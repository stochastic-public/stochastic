function t=triangle(l1,l2,l3)
% t=TRIANGLE(l1,l2,l3)
%
% Checks the triangle inequality, Dahlen and Tromp (1998), Eq. (C.186)
% If t is FALSE then the Wigner3j symbols are zero.
%
% Last modified by fjsimons-at-alum.mit.edu, April 9th, 2004

t=abs(l2-l3)<=l1 & l1<=(l2+l3);


