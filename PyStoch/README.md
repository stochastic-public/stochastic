# PyStoch - a Python based code for SGWB mapping from GW interferometer data

Input is folded stochastic interferometric data(FSID) 

authors: Anirban Ain, Jishnu Suresh, Sudhagar Suyamprakasam and Sanjit Mitra

email: anirban.ain@ligo.org, jishnu.suresh@ligo.org, sudhagar.suyamprakasam@ligo.org, sanjit.mitra@ligo.org


This code creates SGWB anisotropy maps using methods discussed in the paper 'Very fast stochastic gravitational wave background map making using folded data'
arxiv link to the paper: https://arxiv.org/abs/1803.08285, Journal reference: Phys. Rev. D 98, 024001 (2018)

There are 3 steps of running this code:

1. Read gwf frames and collect parameters. (PyStoch/src/read_frames.py)
2. Convert gwf frames into hdf5 format. (PyStoch/src/convert_frames.py)
3. Making maps. (PyStoch/src/pystoch.py)

The parameter file for the calculation is PyStoch/parameters/parameters.ini

The steps are discussed in details in following

# Instructions

### Read gwf frames and collect parameters. (PyStoch/src/read_frames.py)

All the frames you want to process should be kept in subdirectories in PyStoch/frames directory. These subdirectories are framesets
e.g. If you have some FSID frames (gwf format) for O2 run from H1L1 baseline, keep them in a directory PyStoch/frames/O2_H1L1. Now you have a frameset O2_H1L1. 
If you another set of frames for O2 run from H1L1 baseline with some other parameters, you can keep them in a directory PyStoch/frames/O2_H1L1_NoOverlap.
Now you have another frameset O2_H1L1_NoOverlap. You can name framesets by your convenience e.g. O2_H1L1_Albert, O2_H1L1_25September, use_this_one etc.
You can have framesets of different baselines e.g. O2_H1V1, O1_L1V1 etc.
Remember: All frames in a frameset is assumed to have the same parameters (duration, sampling rate, Overlap, etc.).

After coping the frames look to the **frames_location** field in PyStoch/parameters/parameters.ini. This should be the parent of the framesets.
By default it should be ../frames/ but you can change it to wherever you keep the frames.

Next is to run read_frames.py 

It either uses **pycbc** or (**gwpy** and **lalframes**). If you are in a pycbc environment make the **pycbc** field in PyStoch/parameters/parameters.ini True.
Otherwise, you make it False and you need to have done a successful `pip install gwpy lalsuite`.

Run `python read_frames.py` frome PyStoch/src/

After it runs successfully it will create/modify the PyStoch/parameters/framesets.ini file. In this file, all the framesets and their relevant parameters will be written.
If your frames follow standard collaboration and group conventions of channel and file names everything should be fine.
But you should double-check all the parameters.

The PyStoch/parameters/framesets.ini file has a parameter 'process' for all framesets. If this is 'True' then that frameset will be included in map calculation. If it is False that frameset will be ignored.

### Convert gwf frames into hdf5 format. (PyStoch/src/convert_frames.py)

After making sure you have the correct **process** in the PyStoch/parameters/framesets.ini file and the parameters are correct, run PyStoch/src/convert_frames.py.

Run `python convert_frames.py` from PyStoch/src/

This will read the gwf files and create a hdf5 file inside the frameset directory. This will speed up the total process because hdf5 files are faster to read. And you have to do it only once. 

### Making maps. (PyStoch/src/pystoch.py)

Now you should throughly check PyStoch/parameters/parameters.ini and PyStoch/parameters/framesets.ini and

run `python pystoch.py` from PyStoch/src/

The framesets with **process** True will be processed.

Details of some of the fields in PyStoch/parameters/parameters.ini :
**nside**: healpix map size index should be 2^N, 
**multiprocessing**: if true multiple cpu will be used, 
**multi_threads**: number of threads if multiprocessing is true (if left 0 all available cores will be used), 
**f_min** and **f_max**: SGWB maps of this range will be calculated, 
**output_map_dpi**: resolution of output map (png image)


After completing all the 3 steps successfully, you will find a png image of the skymap and a hdf5 file of that skymap data another hdf5 file of all narrowband maps in PyStoch/output.