% flags for optional operations
doFreqMask true
doHighPass1 false
doHighPass2 false

% ifo names
ifo1 H1 
ifo2 H1

% segment duration (sec)
segmentDuration 16

% freq resolution and freq cutoffs for CC statistic sum (Hz)
flow 50
%fhigh 5000
fhigh 1000

% resample rate (Hz)
%resampleRate1 2048
resampleRate1 16384

% ASQ channels
ASQchannel1 LSC-DARM_ERR
ASQchannel2 LSC-STRAIN

% frame type and duration
frameType1 RDS_R_L3
frameType2 H1_RDS_C03_L2
frameDuration1 256
frameDuration2 128

% params for matlab resample routine
nResample1 10
nResample2 10
betaParam1 5
betaParam2 5

% params for high-pass filtering (3db freq in Hz, and filter order) 
highPassFreq1 40
highPassFreq2 40
highPassOrder1 6
highPassOrder2 6

% coherent freqs and number of freq bins to remove if doFreqMask=true;
% NOTE: if an nBin=0, then no bins are removed even if doFreqMask=true
% (coherent freqs are typically harmonics of the power line freq 60Hz
% and the DAQ rate 16Hz)
% Calibration lines are at 46.7 Hz, 393.1 Hz, 1144.3 Hz
%freqsToRemove 52.75,60,108.75,120,148.75,180,194.25,240,265.5,300,344.5,360,376,393.1,420,442.5,480,896.25,1144.3
%nBinsToRemove 15,15,15,15,15,15,15,15,15,15,29,15,15,25,15,15,15,15,25
freqsToRemove 393.1,1144.3
nBinsToRemove 25,25

% calibration filenames
alphaBetaFile1 ../../../../../../sgwb/S5/onasys/input/calibration/H-H1_CAL_FAC_S5_V3_060-843942313-9598080.mat
calCavGainFile1 ../../../../../../sgwb/S5/onasys/input/calibration/H-H1_CAL_REF_CAV_GAIN_DARM_ERR_S5_V3-843942254-999999999.mat
calResponseFile1 ../../../../../../sgwb/S5/onasys/input/calibration/H-H1_CAL_REF_RESPONSE_DARM_ERR_S5_V3-843942254-999999999.mat

alphaBetaFile2 none
calCavGainFile2 none
calResponseFile2 none

% prefix for output filename
outputFilePrefix ../../../../../../sgwb/S5/onasys/output/straincheck/S5_Epoch3_H1_1kHz

