function schumann_psd_combine(params,channel)
% function homestake_daily_psd(params,channel)
% Given a Homestake params struct and channel name, generates PSD and diurnal
% plots for that day
% Routine written by Michael Coughlin.
% Modified: August 17, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdLocation = '/home/mcoughlin/Seismon/Text_Files/PSD/H1_PEM-VAULT_MAG_1030X195Y_COIL_Z_DQ/128';
matFiles = dir([psdLocation '/*.txt']);

data.tt = []; data.ff = []; data.spectra = []; data.fftamp = [];

check_init = 0;
% Loop over frames
for i = 1:length(matFiles)

   data_out = load([psdLocation '/' matFiles(i).name]);

   % Populate array if PSD is non-zero
   if ~isempty(data_out)

      if check_init == 0
         data.ff = data_out(:,1);
      end
      data.spectra = [data.spectra data_out(:,2)];
   end

   if mod(i,100) == 0
      fprintf('Completed %d/%d\n',i,length(matFiles));
   end

end

data.spectra = mean(data.spectra,2);

aLIGOpsdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/schumann/input/aLIGO.txt';
aLIGOpsd = load(aLIGOpsdFile);

ff_interp = aLIGOpsd(:,1);
spectra_interp = interp1(data.ff,data.spectra,ff_interp);
spectra_interp(isnan(spectra_interp)) = 0;


outputLocation = '/home/mcoughlin/matapps/packages/stochastic/trunk/schumann/input';

fid = fopen([outputLocation '/magnetometer.txt'],'w+');
for i=1:length(ff_interp)
   fprintf(fid,'%e %e\n',ff_interp(i),spectra_interp(i));
end
fclose(fid);

