function schumann_fir(params,channel)
% function homestake_daily_fir(params,channel)
% Given a Homestake params struct and channel name, generates FIR plots
% for this day
% Routine written by Michael Coughlin.
% Modified: August 17, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

for i = 1:length(channel(1).frames)
   frame_split = regexp(strrep(channel(1).frames{i},'.','-'),'-','split');
   tt_frames(i) = str2num(frame_split{3});
   dur_frames(i) = str2num(frame_split{4});
end

gps = tt_frames(1):16:tt_frames(end)+dur_frames(end);

check_init = 0;
% Loop over frames
%for f = 1:length(gps)-1
for f = 1:10

   % Retrieve data for this frame
   [data_out,channelHold] = schumann_read_data(params,channel,gps(f),gps(f+1));
   nch = length(data_out);

   % Return to loop if no available data
   if isempty(data_out)
      continue
   end

   % Determine analysis time vector by intersecting time vectors of all channels
   tt = data_out(1).tt;
   for i=1:nch
      tt = intersect(tt,data_out(i).tt);
   end
   if isempty(tt)
      continue
   end

   if params.doPlots && (f == 1)

      % Create plot location
      plotLocation = [params.path '/' strrep(params.channel,':','_') '/fir'];
      homestake_createpath(plotLocation);

      % Timeseries plot
      figure;
      set(gcf, 'PaperSize',[10 6])
      set(gcf, 'PaperPosition', [0 0 10 6])
      clf

      ttStart = min(tt);

      hold all
      for i = 1:length(channel)
         plot(tt - ttStart,data_out(i).samples);
      end
      hold off

      xlabel('Time [s]');
      ylabel(sprintf('Samples [%.0f]',ttStart));

      xlim([min(tt - ttStart) max(tt - ttStart)]);

      hleg1 = legend(data_out.station);
      set(hleg1,'Location','NorthWest')
      set(hleg1,'Interpreter','none')

      print('-dpng',[plotLocation '/timeseries_' num2str(ttStart) '.png']);
      print('-depsc2',[plotLocation '/timeseries_' num2str(ttStart) '.eps']);

      close;
   end

   % Prepare FFT structure 
   if check_init == 0
      data.T = 16;
      data.N = params.filterOrder; %filter order
      data.band = [10 100];

      data.nfft = data.T*channel(1).samplef;
      data.nbin = bitshift(data.nfft,-1);
      data.window = nuttallwin(data.nfft);
      originalSpectra = []; residualSpectra = [];
      data.originalSpectra = []; data.residualSpectra = [];
      data.tt = []; data.ratioSpectra = [];
      data.numSpectra = 1;

      whichChannel = [];
      for i = 1:length(data_out)
         whichChannel = [whichChannel find(strcmpi(data_out(i).station,{channel.station}))];
      end
      channel = channel(whichChannel);

      % Low / bandpass filter coefficients
      if data.band(1) == 0
         [z, p, k] = butter(3,2*data.band(2)/channel(1).samplef,'low');
      elseif data.band(2) == 0
         [z, p, k] = butter(3,2*data.band(1)/channel(1).samplef,'high');
      else
         [z, p, k] = butter(3,2*data.band/channel(1).samplef,'bandpass');
      end
      [sos,g]=zp2sos(z,p,k);

      ttOld = []; samplesOld = [];

      % Determine target channel
      whichChannel = find(strcmpi(params.channel,{channel.station}));
      if isempty(whichChannel)
         continue
      end
      testIndex = setdiff(1:nch,whichChannel); targetIndex = whichChannel;
   end

   % Add data to samples matrix structure
   samples = zeros(length(tt),nch);
   for i=1:nch
      [junk,junk,which_tt] = intersect(tt,data_out(i).tt);
      samples(:,i) = data_out(i).samples(which_tt);
   end

   if ~isempty(samplesOld) && (length(samples(1,:)) ~= length(samplesOld(1,:)))
      continue
   end

   % Add samples from previous frame to the beginning
   tt = [ttOld; tt]; samples = [samplesOld ; samples];   
   ttOld = tt(end - data.N + 1:end); samplesOld = samples(end - data.N + 1:end,:);

   % Low / bandpass samples
   for i = 1:nch
      samples(:,i) = filtfilt(sos, g, samples(:,i));
   end

   if check_init == 0
      % Generate FIR filter coefficients
      MI = samples(:,testIndex); SO = samples(:,targetIndex);
      W = homestake_noiseFilter(MI,SO,data.N);
      check_init = 1;
   else
      % Apply FIR filter to these data
      MI = samples(:,testIndex); SO = samples(:,targetIndex);
      residualSamples = detrend(homestake_subtractFF(W,MI,SO,2*data.band/channel(1).samplef),'constant');
      originalSamples = samples(data.N+1:end,targetIndex);
      tt = tt(data.N+1:end);

      % FFT original and residual spectra
      [originalSpectraHold, ff] = homestake_psd(originalSamples,channel(1).samplef,data.window);
      [residualSpectraHold, ff] = homestake_psd(residualSamples,channel(1).samplef,data.window);
      if isempty(originalSpectraHold) || isempty(residualSpectraHold)
         continue
      end

      if check_init == 1
         data.ff = ff;
         check_init = 2;
      end

      % Populate arrays
      if ~isnan(sum(originalSpectraHold.log_average)) && ~isnan(sum(residualSpectraHold.log_average))
         originalSpectra = [originalSpectra sqrt(originalSpectraHold.log_average)]; 
         residualSpectra = [residualSpectra sqrt(residualSpectraHold.log_average)];
      end

      if ~isempty(originalSpectra) && ~isempty(residualSpectra)
         % Average over spectra
         if length(originalSpectra(1,:)) == data.numSpectra && length(residualSpectra(1,:)) == data.numSpectra
            data.originalSpectra = [data.originalSpectra mean(originalSpectra,2)];
            data.residualSpectra = [data.residualSpectra mean(residualSpectra,2)];
            data.ratioSpectra = [data.ratioSpectra mean(residualSpectra,2)./mean(originalSpectra,2)];
            originalSpectra = []; residualSpectra = [];
            data.tt = [data.tt gps(f)];
         end
      end
   end
end

try data.originalSpectra;
catch
   return;
end

if isempty(data.originalSpectra)
   return
end

if length(find(~isnan(data.originalSpectra(1,:)))) < 2
   return;
end

f_min_index = find(data.ff==params.fmin);
fic = unique(floor(logspace(log10(f_min_index),log10(length(data.ff)),100)));

% Binning parameters
nb = 500;
range_binning = logspace(-30,-20,nb);
data.range_binning = range_binning;

% Determine frequencies to keep
desired_frequencies = ff(fic);
for i=1:length(ff(fic))
   which_freq = find(min(abs(data.ff-ff(fic(i))))==abs(data.ff-ff(fic(i))));
   fic_new(i) = which_freq(1);
end

fic = fic_new;
ff = data.ff(fic);

% Indexes of those frequencies
data.ff = ff; data.fic = fic;

% Reduce fft arrays to correct size
data.originalSpectra = data.originalSpectra(data.fic,:); 
data.residualSpectra = data.residualSpectra(data.fic,:);
data.ratioSpectra = data.ratioSpectra(data.fic,:);

% Determine PSDs to keep
which_spectra = 1:length(data.originalSpectra(1,:));

% Calculate bin histogram of PSDs
data.original_spectral_variation = hist(data.originalSpectra(:,which_spectra)',range_binning)';
data.residual_spectral_variation = hist(data.residualSpectra(:,which_spectra)',range_binning)';
np = sum(data.original_spectral_variation(1,:),2);

% Convert to percentiles
data.original_spectral_variation_norm = data.original_spectral_variation * 100 / np;
data.residual_spectral_variation_norm = data.residual_spectral_variation * 100 / np;

% Initialize arrays
data.original_spectral_variation_norm_1per = [];
data.original_spectral_variation_norm_10per = [];
data.original_spectral_variation_norm_50per = [];
data.original_spectral_variation_norm_90per = [];
data.original_spectral_variation_norm_99per = [];

for i = 1:length(data.ff)
   data.original_spectral_variation_norm_1per(i) = homestake_calculate_percentiles(data.original_spectral_variation_norm(i,:),data.range_binning,1);
   data.original_spectral_variation_norm_10per(i) = homestake_calculate_percentiles(data.original_spectral_variation_norm(i,:),data.range_binning,10);
   data.original_spectral_variation_norm_50per(i) = homestake_calculate_percentiles(data.original_spectral_variation_norm(i,:),data.range_binning,50);
   data.original_spectral_variation_norm_90per(i) = homestake_calculate_percentiles(data.original_spectral_variation_norm(i,:),data.range_binning,90);
   data.original_spectral_variation_norm_99per(i) = homestake_calculate_percentiles(data.original_spectral_variation_norm(i,:),data.range_binning,99);
end

% Initialize arrays
data.residual_spectral_variation_norm_1per = [];
data.residual_spectral_variation_norm_10per = [];
data.residual_spectral_variation_norm_50per = [];
data.residual_spectral_variation_norm_90per = [];
data.residual_spectral_variation_norm_99per = [];

for i = 1:length(data.ff)
   data.residual_spectral_variation_norm_1per(i) = homestake_calculate_percentiles(data.residual_spectral_variation_norm(i,:),data.range_binning,1);
   data.residual_spectral_variation_norm_10per(i) = homestake_calculate_percentiles(data.residual_spectral_variation_norm(i,:),data.range_binning,10);
   data.residual_spectral_variation_norm_50per(i) = homestake_calculate_percentiles(data.residual_spectral_variation_norm(i,:),data.range_binning,50);
   data.residual_spectral_variation_norm_90per(i) = homestake_calculate_percentiles(data.residual_spectral_variation_norm(i,:),data.range_binning,90);
   data.residual_spectral_variation_norm_99per(i) = homestake_calculate_percentiles(data.residual_spectral_variation_norm(i,:),data.range_binning,99);
end

data.W = W;

firLocation = [params.path '/' strrep(params.channel,':','_') '/fir/study'];
homestake_createpath(firLocation);
save([firLocation '/' num2str(params.filterOrder)],'data','channel','-v7.3');

if params.doPlots

   % Create plot location
   plotLocation = [params.path '/' strrep(params.channel,':','_') '/fir'];
   homestake_createpath(plotLocation);

   % Timeseries plot
   figure;
   set(gcf, 'PaperSize',[10 6])
   set(gcf, 'PaperPosition', [0 0 10 6])
   clf

   ttStart = tt(1);
   tt = tt - tt(1);

   hold all
   plot(tt,homestake_normalize(originalSamples));
   plot(tt,homestake_normalize(residualSamples));
   hold off

   xlabel('Time [s]');
   ylabel(sprintf('Samples [%.0f]',ttStart));

   xlim([min(tt) max(tt)]);

   hleg1 = legend('Original','Residual');
   set(hleg1,'Location','NorthWest')
   set(hleg1,'Interpreter','none')

   print('-dpng',[plotLocation '/timeseries.png']);
   print('-depsc2',[plotLocation '/timeseries.eps']);

   close;

   % Original and residual PSD plot
   figure;
   set(gcf, 'PaperSize',[10 6]);
   set(gcf, 'PaperPosition', [0 0 10 6]);
   loglog(data.ff,mean(data.originalSpectra,2),'LineWidth',1);
   hold all
   loglog(data.ff,mean(data.residualSpectra,2),'LineWidth',1);
   hold off
   grid;
   xlabel('Frequency [Hz]');
   ylabel('Spectrum [(m/s)/\surd Hz]');
   hold on
   grid
   set(gca,'Layer','top')
   hold off
   legend('Original','Residual')
   axis([params.fmin params.fmax 1e-30 1e-15])

   print('-dpng',[plotLocation '/psd.png']);
   print('-depsc2',[plotLocation '/psd.eps']);

   close;

   % Binning parameters
   number_of_averages = 100;
   nb_new = floor(nb/number_of_averages);

   indexes_old = 1:length(fic);
   indexes = unique(indexes_old(round(logspace(0,log10(length(indexes_old)),100))));

   data.residual_spectral_variation_norm = data.residual_spectral_variation_norm(indexes,:);
   data.residual_spectral_variation_norm_reduced = zeros(length(indexes),number_of_averages);

   for i=1:number_of_averages
      range(i) = range_binning((i-1) * nb_new + 1);
      for j = 1:length(indexes)
         data.residual_spectral_variation_norm_reduced(j,i) = sum(data.residual_spectral_variation_norm(j,(i-1) * nb_new + 1: i* nb_new));
      end
   end

   [X,Y] = meshgrid(data.ff(indexes),range);

   % Residual variation plot
   data.residual_spectral_variation_norm_reduced(data.residual_spectral_variation_norm_reduced==0) = NaN;

   figure;
   set(gcf, 'PaperSize',[10 8])
   set(gcf, 'PaperPosition', [0 0 10 8])
   clf

   colormap(jet);
   pcolor(X,Y,data.residual_spectral_variation_norm_reduced');

   caxis([0 1])
   set(gcf,'Renderer','zbuffer');

   shading interp
   set(gca,'xscale','log','XLim',[params.fmin params.fmax])
   set(gca,'yscale','log','YLim',[range(1) range(end)])
   set(gca,'clim',[0 5])
   xlabel('Frequency [Hz]')
   ylabel('Spectrum [(m/s)/\surd Hz]')
   grid
   set(gca,'Layer','top')

   hold on
   loglog(data.ff,data.residual_spectral_variation_norm_10per,'w',data.ff,data.residual_spectral_variation_norm_90per,'w',data.ff,data.residual_spectral_variation_norm_50per,'w','LineWidth',2)
   hold off

   print('-dpng',[plotLocation '/specvar.png']);
   print('-depsc2',[plotLocation '/specvar.eps']);
   close;

   tt = (data.tt - data.tt(1)) / (60*60);

   [X,Y] = meshgrid(data.ff(indexes),tt);

   % Frequency-time plot
   figure;
   set(gcf, 'PaperSize',[10 8])
   set(gcf, 'PaperPosition', [0 0 10 8])
   clf

   hC = pcolor(X,Y,log10(data.ratioSpectra(indexes,:))');
   set(gcf,'Renderer','zbuffer');
   colormap(jet)
   shading interp

   t = colorbar('peer',gca);
   set(get(t,'ylabel'),'String','log10(Residual/Original)');
   set(gca,'xscale','log','XLim',[params.fmin params.fmax])
   set(hC,'LineStyle','none');
   grid
   set(gca,'clim',[-1.75 1.75])
   xlabel('Frequency [Hz]')
   ylabel('Time [Hours]');

   print('-dpng',[plotLocation '/tf.png']);
   print('-depsc2',[plotLocation '/tf.eps']);
   close;

   % Residual and original ratio plot
   figure;
   set(gcf, 'PaperSize',[10 6])
   set(gcf, 'PaperPosition', [0 0 10 6])
   clf

   semilogx(ff,log10(mean(data.ratioSpectra,2)));

   xlabel('Frequency [Hz]');
   ylabel('log10(Residual/Original)');

   axis([params.fmin params.fmax -1.75 1.75])

   print('-dpng',[plotLocation '/ratio.png']);
   print('-depsc2',[plotLocation '/ratio.eps']);

   close;

   schumann_fir_page(params)

end
