function homestake_daily_psd_page(params,channel)
% function homestake_daily_psd_page(params,channel)
% Given a Homestake params struct and channel name, generates a html page
% linking to the PSD and diurnal plots for the day
% Routine written by Michael Coughlin.
% Modified: August 17, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

webpageLocation = [params.path '/' channel.station_underscore];
homestake_createpath(webpageLocation);

% Open HTML page for writing
fid=fopen([webpageLocation '/psd.html'],'w+');

% HTML Header
fprintf(fid,'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"\n');
fprintf(fid,'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n');
fprintf(fid,'\n');
fprintf(fid,'<html>\n');
fprintf(fid,'\n');
fprintf(fid,'<head>\n');
fprintf(fid,'<title>PSD and Time Frequency Plots for %s</title>\n',channel.station);
fprintf(fid,'<link rel="stylesheet" type="text/css" href="../../main.css">\n');
fprintf(fid,'<script src="../../sorttable.js"></script>\n');
fprintf(fid,'</head>\n');

% PSD and Time-Frequency plots block
fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">PSD and Time Frequency Plots for %s</span></big></big></big></big><br>\n',channel.station);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>');

% Links to PSD and Time-Frequency plots
fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n')
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="./psd.png"><img alt="" src="./psd.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="./specvar.png"><img alt="" src="./specvar.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="./tf.png"><img alt="" src="./tf.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
%fprintf(fid,'<td style="vertical-align: top;">\n');
%fprintf(fid,'<a href="../psd/%s_all_tf_normal.jpeg"><img alt="" src="../psd/%s_all_tf_normal.jpeg" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',channel_name,channel_name);
%fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>');

fprintf(fid,'</html>\n');
fclose(fid);



