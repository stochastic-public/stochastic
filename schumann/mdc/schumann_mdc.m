function schumann_mdc(paramsFile,jobsFile,jobNumber)

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

params = schumann_readParamsFromFile(paramsFile);

if ~exist(params.framedir)
   schumann_createpath(params.framedir)
end

if params.doPlots
   if ~exist(params.plotdir)
      schumann_createpath(params.plotdir)
   end
end

jobs = load(jobsFile);
job = jobs(jobNumber,:);

frameTimes = job(2):params.frameDuration:job(3); 

if params.doDetectorNoiseSim

   spectra1 = load(params.DetectorNoiseFile1);
   spectra1_f = spectra1(:,1); spectra1_h = spectra1(:,2);
   mc1 = [];
   mc1.transfer(:,1) = spectra1_f;
   mc1.transfer(:,2) = spectra1_h;

   spectra2 = load(params.DetectorNoiseFile2);
   spectra2_f = spectra2(:,1); spectra2_h = spectra2(:,2);
   mc2 = [];
   mc2.transfer(:,1) = spectra2_f;
   mc2.transfer(:,2) = spectra2_h;
    
end

Y1_single_all = []; Y2_single_all = [];

for i = 1:length(frameTimes)-1

   frameStart = frameTimes(i); frameEnd = frameTimes(i+1); frameDur = frameEnd - frameStart;
   t = frameStart:(1/params.sampleRate):frameEnd;
   t = t(1:end-1);
   fs = params.sampleRate; T = frameDur;

   vector1 = zeros(length(t),1); vector2 = zeros(length(t),1);

   if params.doDetectorNoiseSim

      s = RandStream('mcg16807','Seed',params.randomseed);
      RandStream.setGlobalStream(s);

      vector1_noise = schumann_create_noise(fs,T,mc1);
      vector2_noise = schumann_create_noise(fs,T,mc2);

      vector1 = vector1 + vector1_noise'; 
      vector2 = vector2 + vector2_noise';
   end

   if params.doSchumann

      s = RandStream('mcg16807','Seed',0);
      RandStream.setGlobalStream(s);

      [vector1_schumann,vector2_schumann] = schumann_create_schumann(params,fs,T,frameStart);

      vector1 = vector1 + vector1_schumann;
      vector2 = vector2 + vector2_schumann;
   end

   if params.doCBC

      s = RandStream('mcg16807','Seed',0);
      RandStream.setGlobalStream(s);

      [vector1_cbc,vector2_cbc] = schumann_create_cbc(params,fs,T,frameStart);

      vector1 = vector1 + vector1_cbc;
      vector2 = vector2 + vector2_cbc;
   end

   filename = sprintf('%s/%s-%d-%d.gwf',params.framedir,params.framename,frameStart,frameDur);

   rec(1).channel = [params.ifo1 ':' params.ASQchannel1];
   rec(1).data = vector1;
   rec(1).type = 'd';
   rec(1).mode = 'a';

   rec(2).channel = [params.ifo2 ':' params.ASQchannel2];
   rec(2).data = vector2;
   rec(2).type = 'd';
   rec(2).mode = 'a';

   mkframe(filename,rec,'n',1,frameStart);

   L = length(t);
   NFFT = 2^nextpow2(L); % Next power of 2 from length of y
   f = fs/2*linspace(0,1,NFFT/2+1);
   windowconst = 0.375;

   %window = hann(fs*T);
   %vector1 = vector1.*window;
   %vector2 = vector2.*window;
   windowconst = 1;

   Y1 = fft(vector1,NFFT);
   Y1_single = 2*(abs(Y1(1:NFFT/2+1)).^2)/(L*fs*windowconst);
   Y2 = fft(vector2,NFFT);
   Y2_single = 2*(abs(Y2(1:NFFT/2+1)).^2)/(L*fs*windowconst);

   indexes = find(f <= 1024 & f>=1);
   f = f(indexes);
   Y1_single = Y1_single(indexes);
   Y2_single = Y2_single(indexes);

   Y1_single_all = [Y1_single_all; Y1_single'];
   Y2_single_all = [Y2_single_all; Y2_single'];

end

if params.doPlots
   xmin = 5; xmax = 128;
   xmin = 5; xmax = 64;

   timeStart = t(1);

   figure;
   plot(t-timeStart,vector1,'r');
   grid;
   xlabel(sprintf('Timeseries [s] [%d]',timeStart));
   print('-dpng',[params.plotdir '/timeseries1.png'])
   close;

   figure;
   loglog(f,mean(Y1_single_all),'b')
   hold on
   loglog(mc1.transfer(:,1),mc1.transfer(:,2),'r')
   hold off
   grid;
   hleg1 = legend('Noise PSD','LIGO PSD');
   set(hleg1,'Location','NorthEast')
   set(hleg1,'Interpreter','none')
   xlim([xmin,xmax])
   xlabel('Frequency [Hz]');
   ylabel('PSD');
   print('-dpng',[params.plotdir '/psd1.png'])
   close;
  
   figure;
   plot(t-timeStart,vector2,'r');
   grid;
   xlabel(sprintf('Timeseries [s] [%d]',timeStart));
   print('-dpng',[params.plotdir '/timeseries2.png'])
   close;

   [maxVector2,indexMaxVector2] = max(vector2);

   figure;
   plot(t-timeStart,vector2,'r');
   grid;
   xlabel(sprintf('Timeseries [s] [%d]',timeStart));
   xlim([t(indexMaxVector2)-timeStart-2,t(indexMaxVector2)-timeStart+2])
   print('-dpng',[params.plotdir '/timeserieszoom2.png'])
   close;
 
   figure;
   loglog(f,mean(Y2_single_all),'b')
   %semilogy(f,mean(Y2_single_all),'b')
   hold on
   loglog(mc2.transfer(:,1),mc2.transfer(:,2),'r')
   %semilogy(mc2.transfer(:,1),mc2.transfer(:,2),'r')
   hold off
   grid;
   hleg1 = legend('Noise PSD','LIGO PSD');
   set(hleg1,'Location','NorthEast')
   set(hleg1,'Interpreter','none')
   xlim([xmin,xmax])
   xlabel('Frequency [Hz]');
   ylabel('PSD');
   print('-dpng',[params.plotdir '/psd2.png'])
   close;
end 
   
   
   
