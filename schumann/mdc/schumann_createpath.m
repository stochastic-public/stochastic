function schumann_createpath(path)
% function schumann_createpath(path)
% Given a path, creates that path if not already in existence
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Split path by '/'
path_split = regexp(path,'/','split');

% Create path successively adding back pieces
path_construct = '';
for i = 1:length(path_split)
   path_construct = strcat(path_construct,'/',path_split{i});
   if ~exist(path_construct)
      system(['mkdir ' path_construct]);
   end
end

