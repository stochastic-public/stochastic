function schumann_mag_spectra


freqs = 1:16384;
  
fid = fopen('magnetometer.txt','w+');

for i = 1:length(freqs)
   fprintf(fid,'%.10e %.10e\n',freqs(i),1e-3);
end

fclose(fid);
   
   
