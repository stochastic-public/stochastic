% Stochastic pipeline using parallel Matlab on a cluster
% Use on something that has lots of cores like ldas-pcdev5 or ldas-pcdev6

% Use 20 workers for now, this number can be adjusted depending on availability of nodes
% I disable interprocess communication (spmd) because it's not needed and makes
% the jobs more robust.
delete(gcp('nocreate'));
parpool('local', 20, 'SpmdEnabled', false);

stochastic_paths;

paramsFile = 'example_server_all.txt';
jobsFile = 'example_jobs.txt';
ccStatsDir = 'old/output';
outputDir = 'old/output';

[startTimes, jobDurations] = readJobsFile(jobsFile);
numJobs = length(startTimes);

firstJob = 1;
lastJob = numJobs;

% The subset of job numbers to be run. Doesn't need to be sequential or in order
jobs = [firstJob:lastJob];

% Start up the parallel jobs.
% A logfile is written for each one so we can track progress
% Parfor behaviour seems to be that if one job has an error then all jobs are abandoned,
% so I use a try/catch to catch any errors. The job status and error message is saved.
% Need to create at least one instance of the job_status cellarray outside of the parfor
% loop to be passed to the workers.
job_status{firstJob}.succeeded = false;
job_status{firstJob}.msg = '';
parfor k = jobs
  try
    fname = [ ccStatsDir '/out.' num2str(k) ];
    logfile = fopen(fname, 'w');
    fprintf(logfile, '%s Job %d starting\n', datestr(now), k);
    tic;
    stochastic(paramsFile, jobsFile, k);
    jobTime = toc;
    fprintf(logfile, '%s Job %d completed after %f seconds\n', datestr(now), k, jobTime);
    fclose(logfile);
    % Assume job succeeded if we made it this far without an error
    job_status{k}.succeeded = true;
    job_status{k}.msg = '';
  catch me
    % An exception has has occurred so store the message
    job_status{k}.succeeded = false;
    job_status{k}.msg = me.message;
  end;
end;

for k = jobs
  if (job_status{k}.succeeded)
    fprintf('Job %d succeeded\n', k);
  else
    fprintf('Job %d failed with message ''%s''\n', k, job_status{k}.msg);
  end;
end;

