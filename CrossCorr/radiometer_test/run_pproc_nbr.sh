#!/bin/bash

# source seispy
output_pref='nbr/example_'
job1=1
jobN=1
checkpoint_file='nbr/nbr_chkpoint.pkl'
segdur=60
deltaF=0.25
output_mat='nbr/nbr_out.mat'
search_type='nbr'
ndirections=4
direction_names='scox1,gc,sn,random'
output_prefix='nbr/nbr_out_final'

# call pproc

python ../../PostProcessing/run_pproc.py --output-prefix $output_pref\
                       --job1 $job1\
                       --jobN $jobN\
                       --checkpoint-file $checkpoint_file\
                       --segment-duration $segdur\
                       --deltaF $deltaF\
                       --search-type $search_type\
                       --output-matfile $output_mat\
                       --load-checkpoint

python ../../PostProcessing/split_up_results_matfile.py --results-matfile $output_mat \
                                   --ndirections $ndirections \
                                   --direction-names $direction_names \
                                   --output-prefix $output_prefix \
