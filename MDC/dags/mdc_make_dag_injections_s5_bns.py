#!/usr/bin/python

import os, sys, glob, optparse, warnings, datetime, time, matplotlib, math, random
import numpy as np
from pylal import Fr

from glue import iterutils
from glue import pipeline
from glue import lal
from glue.ligolw import lsctables
from glue import segments
import glue.ligolw.utils as utils
import glue.ligolw.utils.segments as ligolw_segments

__author__ = "Michael Coughlin <michael.coughlin@ligo.org>"
__date__ = "2012/8/26"
__version__ = "0.1"

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def parse_commandline():
    """
    Parse the options given on the command-line.
    """
    parser = optparse.OptionParser(usage=__doc__,version=__version__)

    parser.add_option("-s", "--gps_start_time",default=800000000,type=int)
    parser.add_option("-d", "--time_interval",default=1000,type=int)
    parser.add_option("-l", "--segment_duration",default=2048,type=int)
    parser.add_option("-n", "--number_of_segments",default=1,type=int)

    parser.add_option("-v", "--verbose", action="store_true", default=False,
                      help="Run verbosely. (Default: False)")

    opts, args = parser.parse_args()

    # show parameters
    if opts.verbose:
        print >> sys.stderr, ""
        print >> sys.stderr, "running network_eqmon..."
        print >> sys.stderr, "version: %s"%__version__
        print >> sys.stderr, ""
        print >> sys.stderr, "***************** PARAMETERS ********************"
        for o in opts.__dict__.items():
          print >> sys.stderr, o[0]+":"
          print >> sys.stderr, o[1]
        print >> sys.stderr, ""

    return opts


def breakupsegs(seglists, min_segment_length):
        for instrument, seglist in seglists.iteritems():
                newseglist = segments.segmentlist()
                for seg in seglist:
                        if abs(seg) > min_segment_length:
                                newseglist.append(segments.segment(seg))
                seglists[instrument] = newseglist

# =============================================================================
#
#                                    MAIN
#
# =============================================================================

if __name__=="__main__":

    warnings.filterwarnings("ignore")

    # Parse command line
    opts = parse_commandline()

    baseDir = "/home/mcoughlin/Stochastic/MDC/S5_Injection_aLIGO_BNS" 

    MDC_Generation = "/home/mcoughlin/MDC_Generation/MDC_Generation/trunk"
    list2 = "%s/list2"%(MDC_Generation)
    Mdc_ALV = "%s/Mdc_ALV"%(MDC_Generation)

    mdc_create_xml = "/home/mcoughlin/matapps/packages/stochastic/trunk/MDC/mdc_create_xml"
    lalapps_mdc_ninja = "/home/mcoughlin/LAL/master/opt/lscsoft/lalapps/bin/lalapps_mdc_ninja"

    gstlal_fake_aligo_frames = "/usr/bin/gstlal_fake_aligo_frames"
    gstlal_reference_psd = "/usr/bin/gstlal_reference_psd"
    gstlal_recolor_frames = "/usr/bin/gstlal_recolor_frames"
    mdc_combine_frames = "/home/mcoughlin/matapps/packages/stochastic/trunk/MDC/mdc_combine_frames"
   
    ligolw_segments_compat = "/home/mcoughlin/matapps/packages/stochastic/trunk/MDC/ligolw_segments_compat" 
    gstlal_segments_trim = "/usr/bin/gstlal_segments_trim"
    gstlal_cache_to_segments = "/home/mcoughlin/matapps/packages/stochastic/trunk/MDC/gstlal_cache_to_segments"

    gpsStart = 800000000
    gpsEnd = 810494740+2048
    frameDur = 2048
    fs = 4096
    flow = 10
    #flow = 100
    timeIntervalBetweenEvents = 10

    gpsFrames = np.arange(gpsStart,gpsEnd,frameDur)
 
    if not os.path.isdir(baseDir):
        os.mkdir(baseDir)

    noiseFrameDir = os.path.join(baseDir,"noiseframes")
    if not os.path.isdir(noiseFrameDir):
        os.mkdir(noiseFrameDir)
    #injectionFrameDir = os.path.join(baseDir,"injectionframes")
    outputDir = os.path.join(baseDir,"output")
    if not os.path.isdir(outputDir):
        os.mkdir(outputDir)
    frameDir = os.path.join(baseDir,"frames")
    if not os.path.isdir(frameDir):
        os.mkdir(frameDir)
 
    condorDir = os.path.join(baseDir,"condor")
    if not os.path.isdir(condorDir):
        os.mkdir(condorDir)

    if not os.path.isdir(os.path.join(condorDir,"logs")):
        os.mkdir(os.path.join(condorDir,"logs"))

    xmlDir = os.path.join(baseDir,"xml")
    if not os.path.isdir(xmlDir):
        os.mkdir(xmlDir)

    psdDir = os.path.join(baseDir,"psd")
    if not os.path.isdir(psdDir):
        os.mkdir(psdDir)

    ifos = ["H1","H2","L1","V1"]

    for ifo in ifos:
        ifoOutputDir = os.path.join(outputDir,ifo)
        if not os.path.isdir(ifoOutputDir):
            os.mkdir(ifoOutputDir)

    if not os.path.isdir("input"):
        copyInput = "cp -r %s/input ."%(MDC_Generation)
        os.system(copyInput)
    if not os.path.isdir("lists"):
        os.mkdir("lists")

    list2Command = "%s -t %d -n %d -l %d -r %d -f %d -d %d -p 1 -P 0 -q 0 -Q 0 -i 0 -I 0"%(list2,gpsFrames[0],len(gpsFrames),frameDur,fs,flow,timeIntervalBetweenEvents)
    os.system(list2Command)

    rmLists = "rm -r %s/lists"%(baseDir)
    os.system(rmLists)
    rmInput = "rm -r %s/input"%(baseDir)
    os.system(rmInput)

    mvLists = "mv lists %s"%(baseDir)
    os.system(mvLists)
    mvInput = "mv input %s"%(baseDir)
    os.system(mvInput)

    listFile = "%s/lists/list_cbc.txt"%(baseDir)
    injectionFile = "%s/injection.xml"%(baseDir)

    mdc_create_xml_command = "%s -l %s -i %s -f %d"%(mdc_create_xml,listFile,injectionFile,flow)
    #os.system(mdc_create_xml)

    condorFile = os.path.join(condorDir,"condor.dag")
    f = open(condorFile,"w")

    jobNumber = 0

    for i in xrange(len(gpsFrames)-1):

        gpsStartFrame = gpsFrames[i]
        gpsEndFrame = gpsFrames[i+1]
        gpsFrameDur = gpsEndFrame - gpsStartFrame

        jobNumber = jobNumber + 1
        f.write('JOB %d mdc_al.sub\n'%(jobNumber))
        f.write('RETRY %d 3\n'%(jobNumber))
        f.write('VARS %d jobNumber="%d" job="%d"\n'%(jobNumber,jobNumber,i))
        f.write('\n')

    mdcSubFile = os.path.join(condorDir,"mdc_al.sub")
    f = open(mdcSubFile,"w")
    f.write('executable = %s\n'%(Mdc_ALV))
    f.write('arguments = " --verbose --virgo --cbc -s 0 -j $(job) -J 0 -t %d -l %d -r %d -f %d -R %s/ -a 10"\n'%(gpsFrames[0],frameDur,fs,flow,baseDir))
    f.write('request_memory = 128\n')
    f.write('getenv = True\n')
    f.write('log = logs/MDC_frames.log\n')
    f.write('error = logs/$(jobNumber).err\n')
    f.write('output = logs/$(jobNumber).out\n')
    f.write('notification = never\n')
    f.write('queue 1\n')
    f.close()
   
    gstlalSubFile = os.path.join(condorDir,"gstlal_fake_aligo_frames.sub")
    f = open(gstlalSubFile,"w")
    f.write('executable = %s\n'%(gstlal_fake_aligo_frames))
    f.write('arguments = " --gps-start-time $(gpsStart) --gps-end-time $(gpsEnd) --channel-name $(channel_name) --verbose --data-source AdvLIGO --frame-type GAUSSIAN --output-path %s --duration %d --output-channel-name %s"\n'%(noiseFrameDir,frameDur,"STRAIN"))
    f.write('environment = KMP_LIBRARY=serial;MKL_SERIAL=yes\n')
    f.write('request_memory = 128\n')
    f.write('getenv = True\n')
    f.write('log = logs/gstlal_fake_aligo_frames.log\n')
    f.write('error = logs/$(jobNumber).err\n')
    f.write('output = logs/$(jobNumber).out\n')
    f.write('notification = never\n')
    f.write('queue 1\n')
    f.close()

    gstlalSubFile = os.path.join(condorDir,"gstlal_reference_psd.sub")
    f = open(gstlalSubFile,"w")
    f.write('executable = %s\n'%(gstlal_reference_psd))
    f.write('arguments = " --gps-start-time $(gpsStart) --write-psd $(macrowritepsd) --frame-cache $(macroframecache) --gps-end-time $(gpsEnd) --data-source $(macrodatasource) $(macroarguments) "\n')
    f.write('environment = KMP_LIBRARY=serial;MKL_SERIAL=yes\n')
    f.write('request_memory = 128\n')
    f.write('getenv = True\n')
    f.write('log = logs/gstlal_fake_aligo_frames.log\n')
    f.write('error = logs/$(jobNumber).err\n')
    f.write('output = logs/$(jobNumber).out\n')
    f.write('notification = never\n')
    f.write('queue 1\n')
    f.close()

    gstlalSubFile = os.path.join(condorDir,"gstlal_recolor_frames.sub")
    f = open(gstlalSubFile,"w")
    f.write('executable = %s\n'%(gstlal_recolor_frames))
    f.write('arguments = " --reference-psd $(macroreferencepsd) --recolor-psd $(macrorecolorpsd) --data-source $(macrodatasource) --output-channel-name $(macrooutputchannelname) --gps-start-time $(gpsStart) --gps-end-time $(gpsEnd) --frame-cache $(macroframecache) --duration %d --sample-rate %d $(macroarguments) "\n'%(frameDur,fs))
    f.write('environment = KMP_LIBRARY=serial;MKL_SERIAL=yes\n')
    f.write('request_memory = 128\n')
    f.write('getenv = True\n')
    f.write('log = logs/gstlal_recolor_frames.log\n')
    f.write('error = logs/$(jobNumber).err\n')
    f.write('output = logs/$(jobNumber).out\n')
    f.write('notification = never\n')
    f.write('queue 1\n')
    f.close()

    combineSubFile = os.path.join(condorDir,"mdc_combine_frames.sub")
    f = open(combineSubFile,"w")
    f.write('executable = %s\n'%(mdc_combine_frames))
    f.write('arguments = " --gpsStart $(gpsStart) --gpsEnd $(gpsEnd) --baseDir %s"\n'%(baseDir))
    f.write('request_memory = 128\n')
    f.write('getenv = True\n')
    f.write('log = logs/mdc_combine_frames.log\n')
    f.write('error = logs/$(jobNumber).err\n')
    f.write('output = logs/$(jobNumber).out\n')
    f.write('notification = never\n')
    f.write('queue 1\n')
    f.close()
