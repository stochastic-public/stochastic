#!/usr/bin/python

import os, sys, glob, optparse, warnings, datetime, time, matplotlib, math, random
import numpy as np
from pylal import Fr

from glue import iterutils
from glue import pipeline
from glue import lal
from glue.ligolw import lsctables
from glue import segments
import glue.ligolw.utils as utils
import glue.ligolw.utils.segments as ligolw_segments

__author__ = "Michael Coughlin <michael.coughlin@ligo.org>"
__date__ = "2012/8/26"
__version__ = "0.1"

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def parse_commandline():
    """
    Parse the options given on the command-line.
    """
    parser = optparse.OptionParser(usage=__doc__,version=__version__)

    parser.add_option("-s", "--gps_start_time",default=800000000,type=int)
    parser.add_option("-d", "--time_interval",default=1000,type=int)
    parser.add_option("-l", "--segment_duration",default=2048,type=int)
    parser.add_option("-n", "--number_of_segments",default=1,type=int)

    parser.add_option("-v", "--verbose", action="store_true", default=False,
                      help="Run verbosely. (Default: False)")

    opts, args = parser.parse_args()

    # show parameters
    if opts.verbose:
        print >> sys.stderr, ""
        print >> sys.stderr, "running network_eqmon..."
        print >> sys.stderr, "version: %s"%__version__
        print >> sys.stderr, ""
        print >> sys.stderr, "***************** PARAMETERS ********************"
        for o in opts.__dict__.items():
          print >> sys.stderr, o[0]+":"
          print >> sys.stderr, o[1]
        print >> sys.stderr, ""

    return opts


def breakupsegs(seglists, min_segment_length):
        for instrument, seglist in seglists.iteritems():
                newseglist = segments.segmentlist()
                for seg in seglist:
                        if abs(seg) > min_segment_length:
                                newseglist.append(segments.segment(seg))
                seglists[instrument] = newseglist

# =============================================================================
#
#                                    MAIN
#
# =============================================================================

if __name__=="__main__":

    warnings.filterwarnings("ignore")

    # Parse command line
    opts = parse_commandline()

    baseDir = "/home/mcoughlin/Stochastic/MDC/S5_Injection_aLIGO" 

    MDC_Generation = "/home/mcoughlin/MDC_Generation/MDC_Generation/trunk"
    list1 = "%s/list1"%(MDC_Generation)
    list2 = "%s/list2"%(MDC_Generation)
    Mdc_ALV = "%s/Mdc_ALV"%(MDC_Generation)

    gpsStart = 800000000
    gpsEnd = 810494740+2048
    frameDur = 2048
    fs = 4096
    flow = 10
    #flow = 100
    timeIntervalBetweenEvents = 0.1
    MassMin=1.35    # minimal mass
    MassMax=1.45    # maximal mass
    MassMean=1.4    # mean of the mass distribution
    MassSD=0.01    # standart deviation of the mass distribution
    zMin=0    # minimal redshift
    zMax=10    # maximal redshift

    gpsFrames = np.arange(gpsStart,gpsEnd,frameDur)
 
    if not os.path.isdir(baseDir):
        os.mkdir(baseDir)

    outputDir = os.path.join(baseDir,"output")
    if not os.path.isdir(outputDir):
        os.mkdir(outputDir)
 
    condorDir = os.path.join(baseDir,"condor")
    if not os.path.isdir(condorDir):
        os.mkdir(condorDir)

    if not os.path.isdir(os.path.join(condorDir,"logs")):
        os.mkdir(os.path.join(condorDir,"logs"))

    ifos = ["H1","H2","L1","V1"]

    for ifo in ifos:
        ifoOutputDir = os.path.join(outputDir,ifo)
        if not os.path.isdir(ifoOutputDir):
            os.mkdir(ifoOutputDir)

    if not os.path.isdir("input"):
        copyInput = "cp -r %s/input ."%(MDC_Generation)
        os.system(copyInput)
    if not os.path.isdir("lists"):
        os.mkdir("lists")

    list1Command = "%s -t %d -n %d -l %d -r %d -f %d -d %d -m %f -M %f -a %f -b %f -z %f -Z %f"%(list1,gpsFrames[0],len(gpsFrames),frameDur,fs,flow,timeIntervalBetweenEvents,MassMin,MassMax,MassMean,MassSD,zMin,zMax)

    os.system(list1Command)

    rmLists = "rm -rf %s/lists"%(baseDir)
    os.system(rmLists)
    rmInput = "rm -rf %s/input"%(baseDir)
    os.system(rmInput)

    mvLists = "mv lists %s"%(baseDir)
    os.system(mvLists)
    mvInput = "mv input %s"%(baseDir)
    os.system(mvInput)

    listFile = "%s/lists/list_cbc.txt"%(baseDir)

    condorFile = os.path.join(condorDir,"condor.dag")
    f = open(condorFile,"w")

    jobNumber = 0

    for i in xrange(len(gpsFrames)-1):

        gpsStartFrame = gpsFrames[i]
        gpsEndFrame = gpsFrames[i+1]
        gpsFrameDur = gpsEndFrame - gpsStartFrame

        jobNumber = jobNumber + 1
        f.write('JOB %d mdc_al.sub\n'%(jobNumber))
        f.write('RETRY %d 3\n'%(jobNumber))
        f.write('VARS %d jobNumber="%d" job="%d"\n'%(jobNumber,jobNumber,i))
        f.write('\n')

    mdcSubFile = os.path.join(condorDir,"mdc_al.sub")
    f = open(mdcSubFile,"w")
    f.write('executable = %s\n'%(Mdc_ALV))
    f.write('arguments = " --verbose --virgo --colocated --test --cbc -s 0 -j $(job) -J 0 -t %d -l %d -r %d -f %d -R %s/ -a 100"\n'%(gpsFrames[0],frameDur,fs,flow,baseDir))
    f.write('request_memory = 128\n')
    f.write('getenv = True\n')
    f.write('log = logs/MDC_frames.log\n')
    f.write('error = logs/$(jobNumber).err\n')
    f.write('output = logs/$(jobNumber).out\n')
    f.write('notification = never\n')
    f.write('queue 1\n')
    f.close()
   
