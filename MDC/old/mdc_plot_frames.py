#!/usr/bin/python

import matplotlib
matplotlib.use('Agg')
import os, sys, glob
import numpy as np
from pylal import Fr
from matplotlib import pyplot as plt

__author__ = "Michael Coughlin <michael.coughlin@ligo.org>"
__date__ = "2012/8/26"
__version__ = "0.1"

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def read_frames(start_time,end_time,channel,cache):
    time = np.array([])
    data = np.array([])

    #== loop over frames in cache
    for frame in cache:

        frameSplit = frame.replace(".gwf","").split("-")
        gps = float(frameSplit[-2])
        dur = float(frameSplit[-1])

        framegpsStart = gps
        framegpsEnd = gps + dur
        if framegpsStart > end_time or framegpsEnd < start_time:
            continue

        frame_data,data_start,_,dt,_,_ = Fr.frgetvect1d(frame,channel)
        frame_length = float(dt)*len(frame_data)
        frame_time = data_start+dt*np.arange(len(frame_data))

        time = np.concatenate([time,frame_time[(frame_time >= start_time) & (frame_time <= end_time)]])
        data = np.concatenate([data,frame_data[(frame_time >= start_time) & (frame_time <= end_time)]])

    return time,data

# =============================================================================
#
#                                    MAIN
#
# =============================================================================

if __name__=="__main__":

    # NINJA2 Gaussian: 871147452 - 876353468
    # NINJA2: 900000020 - 904759878

    gpsStart = 864000000                # 871147452
    gpsEnd =   gpsStart + 2048 # 876353468 + 4096
    gpsStart = 871147452
    gpsEnd =   876353468

    outputdir = "/home/mcoughlin/Stochastic/MDC/NINJA2_Gaussian"
    if not os.path.isdir(outputdir):
        os.mkdir(outputdir)

    framedir = os.path.join(outputdir,"frames")
    if not os.path.isdir(framedir):
        os.mkdir(framedir)

    plotdir = os.path.join(outputdir,"plot")
    if not os.path.isdir(plotdir):
        os.mkdir(plotdir)

    ifos = ['H1','L1','V1']
    #ifos = ['H1']
    types = {'H1':'H1_NINJA2_G1000176_EARLY_RECOLORED','L1':'L1_NINJA2_G1000176_EARLY_RECOLORED','V1':'V1_NINJA2_G1000176_EARLY_RESCALED_RECOLORED'}
    types={'H1':'H1_NINJA2_GAUSSIAN','L1':'L1_NINJA2_GAUSSIAN','V1':'V1_NINJA2_GAUSSIAN'}
    #types={'H1':'H1_RDS_C03_L2','L1':'L1_RDS_C03_L2','V1':'HrecOnline'}
    channels = {'H1':'H1:LDAS-STRAIN','L1':'L1:LDAS-STRAIN','V1':'V1:h_16384Hz'}
    #channels = {'H1':'H1:LSC-STRAIN','L1':'L1:LSC-STRAIN','V1':'V1:h_16384Hz'}

    injchannels = {'H1':'H1:STRAIN','L1':'L1:STRAIN','V1':'V1:STRAIN'}

    for ifo in ifos:

        ifoframedir = os.path.join(framedir,ifo)
        if not os.path.isdir(ifoframedir):
            os.mkdir(ifoframedir)

        ifoplotdir = os.path.join(plotdir,ifo)
        if not os.path.isdir(ifoplotdir):
            os.mkdir(ifoplotdir)

        frames = glob.glob(os.path.join(ifoframedir,"%s*.gwf"%(ifo[0])))
        time, data = read_frames(gpsStart,gpsEnd,channels[ifo],frames)

        gpsStart = 1e21
        gpsEnd = 0

        for frame in frames:

            frameSplit = frame.replace(".gwf","").split("-")
            gps = float(frameSplit[-2])
            dur = float(frameSplit[-1])

            gpsStart = min(gpsStart,gps)
            gpsEnd = max(gpsEnd,gps+dur)

        plotName = "%s/%s-MDC-%d-%d.png"%(ifoplotdir,ifo[0],gpsStart,gpsEnd)

        plt.figure()
        plt.plot(time, data, 'b')
        plt.xlabel('Time (s)')
        plt.ylabel('Amplitude')
        #plt.xlim([900000990, 900001010])
        #plt.ylim(-4e-21, 4e-21)
        plt.show()
        plt.savefig(plotName, dpi=100)

