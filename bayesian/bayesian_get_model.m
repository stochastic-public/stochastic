function [model,prior] = bayesian_get_model(dataSet,analysisType)

if strcmp(dataSet,'MDC2') || strcmp(dataSet,'MDC3') || strcmp(dataSet,'MDC4') || strcmp(dataSet,'MDC5') || strcmp(dataSet,'none')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -55, -35, 'fixed'; ...
           'k', 'uniform', -6, 2, 'fixed'};
   end
elseif strcmp(dataSet,'MDC0all') || strcmp(dataSet,'MDC2all') || strcmp(dataSet,'MDC3all') || strcmp(dataSet,'MDC4all') || strcmp(dataSet,'MDC5all')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -50, -30, 'fixed'; ...
           'k', 'uniform', -8, 8, 'fixed'};
      %prior = {'a', 'uniform', -39, -38, 'fixed'; ...
      %     'k', 'uniform', -2.5, -2.3, 'fixed'};
   elseif strcmp(analysisType,'powerlawPSD');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -70, -30, 'fixed'; ...
           'k', 'uniform', -8, 8, 'fixed';...
           'A1', 'uniform', -10, 10, 'fixed';...
           'A2', 'uniform', -10, 10, 'fixed'};

      %prior = {'a', 'uniform', -39, -38, 'fixed'; ...
      %     'k', 'uniform', -2.5, -2.3, 'fixed'};
   elseif strcmp(analysisType,'DNSPSD');
      model = @DNS_PSD_model;
      prior = {'M', 'uniform', 0, 10, 'fixed'; ...
           'lam', 'uniform', -8, 8, 'fixed'; ...
           'A1', 'uniform', -10, 10, 'fixed';...
           'A2', 'uniform', -10, 10, 'fixed'};
   end

elseif strcmp(dataSet,'MDC2inj') || strcmp(dataSet,'MDC3inj') || strcmp(dataSet,'MDC4inj') || strcmp(dataSet,'MDC5inj')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -40, -38, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
   end
elseif strcmp(dataSet,'MDC2inj_mid')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -42, -38, 'fixed'; ...
           'k', 'uniform', -4, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
   end
elseif strcmp(dataSet,'MDC2inj_low')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -42, -38, 'fixed'; ...
           'k', 'uniform', -4, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
   end
elseif strcmp(dataSet,'SBnonoise')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -34, -33, 'fixed'; ...
           'k', 'uniform', -3.5 -2.5, 'fixed'};
   end
elseif strcmp(dataSet,'SBnoiseflat')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -34, -33, 'fixed'; ...
           'k', 'uniform', -3.5 -2.5, 'fixed'};
      prior = {'a', 'uniform', -38, -24, 'fixed'; ...
           'k', 'uniform', -5 3, 'fixed'};
      %prior = {'a', 'uniform', -34, -32, 'fixed'; ...
      %     'k', 'uniform', -3.5, -2.5, 'fixed'};
   end
elseif strcmp(dataSet,'SBnoise')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -34, -33, 'fixed'; ...
           'k', 'uniform', -3.5 -2.5, 'fixed'};
      prior = {'a', 'uniform', -38, -24, 'fixed'; ...
           'k', 'uniform', -5 3, 'fixed'};
   end
elseif strcmp(dataSet,'noiseflat')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -40, -32, 'fixed'; ...
           'k', 'uniform', -8 8, 'fixed'};
   end
elseif strcmp(dataSet,'MDC2noise')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -40, -30, 'fixed'; ...
        'k', 'uniform', -5, 5, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
   elseif strcmp(analysisType,'powerlawPSD');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -50, -30, 'fixed'; ...
        'k', 'uniform', -5, 5, 'fixed';...
        'A1', 'uniform', -5, 5, 'fixed';...
        'A2', 'uniform', -5, 5, 'fixed'};
   elseif  strcmp(analysisType,'DNS')
      model = @DNS_PSD_model;
      % ET no detect
      prior = {'M', 'uniform', 0, 10, 'fixed'; ...
           'lam', 'uniform', -8, 8, 'fixed'};
      % 2
      %prior = {'M', 'uniform', 0, 10, 'fixed'; ...
      %     'lam', 'uniform', -7, 0, 'fixed'};
   elseif  strcmp(analysisType,'DNSPSD')
      model = @DNS_PSD_model;
      % ET no detect
      prior = {'M', 'uniform', 0, 10, 'fixed'; ...
           'lam', 'uniform', -8, 8, 'fixed'; ...
           'A1', 'uniform', -5, 5, 'fixed';...
           'A2', 'uniform', -5, 5, 'fixed'};

   elseif strcmp(analysisType,'DNSPSDH');
      model = @DNS_PSD_model;
      prior = {'M', 'uniform', 0, 10, 'fixed'; ...
           'lam', 'uniform', -20, 20, 'fixed'; ...
           'A1', 'uniform', -10, 10, 'fixed';...
           'A2', 'uniform', -10, 10, 'fixed'};

      tmin = 0.02;
      if tmin ==0.02
         load('BC_fit_20.mat');
      else
         load('BC_fit_100.mat');
      end
      sfr = 'h';
      global f_sfr;
      f_sfr = eval(['fit.' sfr]);
      global r_sfr;
      r_sfr = r;

   end
elseif strcmp(dataSet,'MDC2noiseflat')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -40, -30, 'fixed'; ...
           'k', 'uniform', -5, 5, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
   elseif strcmp(analysisType,'powerlawPSD');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -40, -36, 'fixed'; ...
        'k', 'uniform', -5, 5, 'fixed';...
        'A1', 'uniform', -5, 5, 'fixed';...
        'A2', 'uniform', -5, 5, 'fixed'};
   elseif  strcmp(analysisType,'DNS')
      model = @DNS_model;
      % ET no detect
      prior = {'M', 'uniform', 0, 10, 'fixed'; ...
           'lam', 'uniform', -10, -4, 'fixed'};
      % 2
      prior = {'M', 'uniform', 0, 10, 'fixed'; ...
           'lam', 'uniform', -7, 0, 'fixed'};

   end
elseif strcmp(dataSet,'MDC2noise0')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -39, -38, 'fixed'; ...
           'k', 'uniform', -2.5, -2.3, 'fixed'};
      prior = {'a', 'uniform', -41, -38, 'fixed'; ...
           'k', 'uniform', -2.5, 0, 'fixed'};
   elseif strcmp(analysisType,'powerlawPSD');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -40, -36, 'fixed'; ...
        'k', 'uniform', -5, 5, 'fixed';...
        'A1', 'uniform', -5, 5, 'fixed';...
        'A2', 'uniform', -5, 5, 'fixed'};
   end
elseif strcmp(dataSet,'simple')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -39, -38, 'fixed'; ...
           'k', 'uniform', -2.5, -2.3, 'fixed'};
      prior = {'a', 'uniform', -5,5, 'fixed'; ...
           'k', 'uniform', -3,1, 'fixed'};
   elseif strcmp(analysisType,'powerlawPSDpowerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -10, 10, 'fixed'; ...
           'k', 'uniform', -5,5, 'fixed';...
           'A', 'uniform', -10,10, 'fixed'; ...
           'K', 'uniform', -5,5, 'fixed'};
   end
elseif strcmp(dataSet,'simpleSB')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -34, -33, 'fixed'; ...
           'k', 'uniform', -3.5 -2.5, 'fixed'};
      prior = {'a', 'uniform', -40, -24, 'fixed'; ...
           'k', 'uniform', -5 3, 'fixed'};
   elseif strcmp(analysisType,'powerlawPSDpowerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -34, -33, 'fixed'; ...
           'k', 'uniform', -3.5 -2.5, 'fixed'};
      prior = {'a', 'uniform', -50, -30, 'fixed'; ...
           'k', 'uniform', -5 3, 'fixed'; ...
           'A', 'uniform', -50, -30, 'fixed'; ...
           'K', 'uniform', -5 3, 'fixed'};
   end
elseif strcmp(dataSet,'simplePoint')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -34, -33, 'fixed'; ...
           'k', 'uniform', -3.5 -2.5, 'fixed'};
      prior = {'a', 'uniform', -10, 10, 'fixed'; ...
           'k', 'uniform', -5 3, 'fixed'; ...
           'r', 'uniform', 0, 24, 'fixed'; ...
           'd', 'uniform', -90, 90, 'fixed'};
   elseif strcmp(analysisType,'powerlawPSDpowerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -34, -33, 'fixed'; ...
           'k', 'uniform', -3.5 -2.5, 'fixed'};
      prior = {'a', 'uniform', -10, 10, 'fixed'; ...
           'k', 'uniform', -5 3, 'fixed'; ...
           'r', 'uniform', 0, 24, 'fixed'; ...
           'd', 'uniform', -90, 90, 'fixed'; ...
           'A', 'uniform', -10, 10, 'fixed'; ...
           'K', 'uniform', -5 3, 'fixed'};
   end
elseif strcmp(dataSet,'SBpoint')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -34, -33, 'fixed'; ...
           'k', 'uniform', -3.5 -2.5, 'fixed'};
      prior = {'a', 'uniform', -5, 5, 'fixed'; ...
           'k', 'uniform', -5 3, 'fixed'; ...
           'r', 'uniform', 0, 24, 'fixed'; ...
           'd', 'uniform', -90, 90, 'fixed'};
   elseif strcmp(analysisType,'powerlawPSDpowerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -34, -33, 'fixed'; ...
           'k', 'uniform', -3.5 -2.5, 'fixed'};
      prior = {'a', 'uniform', -5, 5, 'fixed'; ...
           'k', 'uniform', -5 3, 'fixed'; ...
           'r', 'uniform', 0, 24, 'fixed'; ...
           'd', 'uniform', -90, 90, 'fixed'; ...
           'A', 'uniform', -5, 5, 'fixed'; ...
           'K', 'uniform', -5 3, 'fixed'};
   end
elseif strcmp(dataSet,'simpleCBC') || strcmp(dataSet,'simpleCBCNagamine')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -34, -33, 'fixed'; ...
           'k', 'uniform', -3.5 -2.5, 'fixed'};
      prior = {'a', 'uniform', -40, -24, 'fixed'; ...
           'k', 'uniform', -5 3, 'fixed'};
   elseif strcmp(analysisType,'powerlawPSDpowerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -34, -33, 'fixed'; ...
           'k', 'uniform', -3.5 -2.5, 'fixed'};
      prior = {'a', 'uniform', -50, -30, 'fixed'; ...
           'k', 'uniform', -5 3, 'fixed'; ...
           'A', 'uniform', -50, -30, 'fixed'; ...
           'K', 'uniform', -5, 3, 'fixed'};
   elseif strcmp(analysisType,'powerlawPSD');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
      model = @powerlaw_model;
      prior = {'a', 'uniform', -34, -33, 'fixed'; ...
           'k', 'uniform', -3.5 -2.5, 'fixed'};
      prior = {'a', 'uniform', -60, -30, 'fixed'; ...
           'k', 'uniform', -5 3, 'fixed'; ...
           'A1', 'uniform', -10, 10, 'fixed'; ...
           'A2', 'uniform', -10, 10, 'fixed'};
   elseif strcmp(analysisType,'DNSPSD');
      model = @DNS_PSD_model;
      prior = {'M', 'uniform', 0, 10, 'fixed'; ...
           'lam', 'uniform', -20, 20, 'fixed'; ...
           'A1', 'uniform', -10, 10, 'fixed';...
           'A2', 'uniform', -10, 10, 'fixed'};

      tmin = 0.02;
      if tmin ==0.02
         load('BC_fit_20.mat');
      else
         load('BC_fit_100.mat');
      end
      sfr = 'h';
      global f_sfr;
      f_sfr = eval(['fit.' sfr]);
      global r_sfr;
      r_sfr = r;
   elseif strcmp(analysisType,'DNSPSDH');
      model = @DNS_PSD_model;
      prior = {'M', 'uniform', 0, 10, 'fixed'; ...
           'lam', 'uniform', -20, 20, 'fixed'; ...
           'A1', 'uniform', -10, 10, 'fixed';...
           'A2', 'uniform', -10, 10, 'fixed'};

      tmin = 0.02;
      if tmin ==0.02
         load('BC_fit_20.mat');
      else
         load('BC_fit_100.mat');
      end
      sfr = 'h';
      global f_sfr;
      f_sfr = eval(['fit.' sfr]);
      global r_sfr;
      r_sfr = r;
   elseif strcmp(analysisType,'DNSPSDF');
      model = @DNS_PSD_model;
      prior = {'M', 'uniform', 0, 10, 'fixed'; ...
           'lam', 'uniform', -20, 20, 'fixed'; ...
           'A1', 'uniform', -10, 10, 'fixed';...
           'A2', 'uniform', -10, 10, 'fixed'};

      tmin = 0.02;
      if tmin ==0.02
         load('BC_fit_20.mat');
      else
         load('BC_fit_100.mat');
      end
      sfr = 'f';
      global f_sfr;
      f_sfr = eval(['fit.' sfr]);
      global r_sfr;
      r_sfr = r;
   elseif strcmp(analysisType,'DNSPSDW');
      model = @DNS_PSD_model;
      prior = {'M', 'uniform', 0, 10, 'fixed'; ...
           'lam', 'uniform', -20, 20, 'fixed'; ...
           'A1', 'uniform', -10, 10, 'fixed';...
           'A2', 'uniform', -10, 10, 'fixed'};

      tmin = 0.02;
      if tmin ==0.02
         load('BC_fit_20.mat');
      else
         load('BC_fit_100.mat');
      end
      sfr = 'w';
      global f_sfr;
      f_sfr = eval(['fit.' sfr]);
      global r_sfr;
      r_sfr = r;
   elseif strcmp(analysisType,'DNSPSDN');
      model = @DNS_PSD_model;
      prior = {'M', 'uniform', 0, 10, 'fixed'; ...
           'lam', 'uniform', -20, 20, 'fixed'; ...
           'A1', 'uniform', -10, 10, 'fixed';...
           'A2', 'uniform', -10, 10, 'fixed'};

      tmin = 0.02;
      if tmin ==0.02
         load('BC_fit_20.mat');
      else
         load('BC_fit_100.mat');
      end
      sfr = 'n';
      global f_sfr;
      f_sfr = eval(['fit.' sfr]);
      global r_sfr;
      r_sfr = r;
   elseif strcmp(analysisType,'DNSPSDS');
      model = @DNS_PSD_model;
      prior = {'M', 'uniform', 0, 10, 'fixed'; ...
           'lam', 'uniform', -20, 20, 'fixed'; ...
           'A1', 'uniform', -10, 10, 'fixed';...
           'A2', 'uniform', -10, 10, 'fixed'};

      tmin = 0.02;
      if tmin ==0.02
         load('BC_fit_20.mat');
      else
         load('BC_fit_100.mat');
      end
      sfr = 's';
      global f_sfr;
      f_sfr = eval(['fit.' sfr]);
      global r_sfr;
      r_sfr = r;
   end
end


