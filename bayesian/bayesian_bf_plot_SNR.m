function mnPE(jobNumber,analysisType,dataSet,injSNRs);

rng(1,'twister');

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_bf';
matDir = [baseplotDir '/' dataSet '/' analysisType '/' num2str(jobNumber)];
plotDir = [baseplotDir '/' dataSet '/' analysisType '/' num2str(jobNumber) '/all'];
createpath(plotDir);

if strcmp(dataSet,'simple');
   trueval1 = 0; trueval2 = 0;
elseif strcmp(dataSet,'simpleSB')
   trueval1 = -36.5; trueval2 = -3;
elseif strcmp(dataSet,'SBnoise') || strcmp(dataSet,'SBnoiseflat')
   trueval2 = -33.38; trueval1 = -3;
elseif strcmp(dataSet,'MDC2nonoise') || strcmp(dataSet,'MDC2nonoiseplus') || strcmp(dataSet,'MDC2noise') || strcmp(dataSet,'MDC2noiseflat') || strcmp(dataSet,'MDC2noise0')
   if strcmp(analysisType,'powerlaw');
      trueval1 = -35.2; trueval2 = 2/3 - 3;
   elseif strcmp(analysisType,'DNS');
      trueval1 = 1.25536; trueval2 = log10(4.7933e-04);
   end
elseif strcmp(dataSet,'simplePoint')
%   trueval1 = 1.25536; trueval2 = log10(4.7933e-04);
end

As = []; ks = [];
AsMarg = []; ksMarg = [];
for i = 1:length(injSNRs)
   injSNR = injSNRs(i);
   matFile = [matDir '/' num2str(injSNR) '/multinest.mat'];
   load(matFile,'prior','lik');

   [C,I] = max(lik(:));
   [I1,I2] = ind2sub(size(lik),I);
   lik_max = lik(I1,I2);

   likhold = lik;
   lik=likhold;
   norm = max(lik(:));
   lik = exp(lik - norm);
   %lik = lik - max(lik(:));
   Vq = lik';

   [junk,index] = min(abs(prior{2,6}-trueval1));
   lik=likhold;
   Vq = lik';
   Vq = Vq(index,:);
   norm = max(Vq);
   Vq = exp(Vq-norm);
   Vq = Vq / sum(Vq);
   As = [As; Vq];

   [junk,index] = min(abs(prior{1,6}-trueval2));
   lik=likhold;
   Vq = lik';
   Vq = Vq(:,index)';
   norm = max(Vq);
   Vq = exp(Vq-norm);
   Vq = Vq / sum(Vq);
   ks = [ks; Vq];

   lik=likhold;
   norm = max(lik(:));
   lik = exp(lik - norm);
   Vq = lik';

   margVq = sum(Vq);
   margVq = margVq /sum(margVq);
   AsMarg = [AsMarg; margVq];
   margVq = sum(Vq');
   margVq = margVq /sum(margVq);
   ksMarg = [ksMarg; margVq];

end

xlab = get_name(prior{1,1});
ylab = get_name(prior{2,1});

Colors = jet;
NoColors = length(Colors);
I = log10(injSNRs);
Ireduced = (I-min(I))/(max(I)-min(I))*(NoColors-1)+1;
RGB = interp1(1:NoColors,Colors,Ireduced);
set(gcf,'DefaultAxesColorOrder',RGB)

[junk,index] = min(abs(prior{2,6}-0));
plotName = [plotDir '/' prior{1,1} '_fixed'];
figure()
hold all
for i = 1:length(I)
   plot(prior{1,6},As(i,:),'color',RGB(i,:));
end
hold off
xlabel(xlab);
cbar = colorbar;
ylabel('Normalized Likelihood');
ylabel(cbar,'log10(SNR)');
caxis([min(log10(injSNRs)) max(log10(injSNRs))])
%ylim([0 1]);
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/' prior{1,1} '_marg'];
figure()
hold all
for i = 1:length(I)
   plot(prior{1,6},AsMarg(i,:),'color',RGB(i,:));
end
plot([trueval1 trueval1],[0 1.1*max(AsMarg(:))],'k--');
hold off
cbar = colorbar;
ylabel(cbar,'log10(SNR)');
caxis([min(log10(injSNRs)) max(log10(injSNRs))])
xlabel(xlab);
ylabel('Marginalized Posteriors');
%ylim([0 0.1]);
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/' prior{1,1} '_marg_exp'];
figure()
hold all
for i = 1:length(I)
   plot(10.^prior{1,6},AsMarg(i,:),'color',RGB(i,:));
end
plot([trueval1 trueval1],[0 1.1*max(AsMarg(:))],'k--');
hold off
cbar = colorbar;
ylabel(cbar,'log10(SNR)');
caxis([min(log10(injSNRs)) max(log10(injSNRs))])
xlabel(xlab);
ylabel('Marginalized Posteriors');
%ylim([0 0.1]);
print('-dpng',plotName);
print('-depsc2',plotName);
close;

[junk,index] = min(abs(prior{1,6}-0));
plotName = [plotDir '/' prior{2,1} '_fixed'];
figure()
hold all
for i = 1:length(I)
   plot(prior{2,6},ks(i,:),'color',RGB(i,:));
end
plot([trueval1 trueval1],[0 1.1*max(AsMarg(:))],'k--');
hold off
xlabel(ylab);
ylabel('Normalized Likelihood');
cbar = colorbar;
ylabel(cbar,'log10(SNR)');
caxis([min(log10(injSNRs)) max(log10(injSNRs))])
%ylim([0 1]);
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/' prior{2,1} '_marg'];
figure()
hold all
for i = 1:length(I)
   plot(prior{2,6},ksMarg(i,:),'color',RGB(i,:));
end
plot([trueval2 trueval2],[0 1.1*max(ksMarg(:))],'k--');
hold off
cbar = colorbar;
ylabel(cbar,'log10(SNR)');
caxis([min(log10(injSNRs)) max(log10(injSNRs))])
xlabel(ylab);
ylabel('Marginalized Posteriors');
%ylim([0 0.1]);
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/' prior{2,1} '_marg_exp'];
figure()
hold all
for i = 1:length(I)
   plot(10.^prior{2,6},ksMarg(i,:),'color',RGB(i,:));
end
plot([trueval2 trueval2],[0 1.1*max(ksMarg(:))],'k--');
hold off
cbar = colorbar;
ylabel(cbar,'log10(SNR)');
caxis([min(log10(injSNRs)) max(log10(injSNRs))])
xlabel(ylab);
ylabel('Marginalized Posteriors');
%ylim([0 0.1]);
print('-dpng',plotName);
print('-depsc2',plotName);
close;
