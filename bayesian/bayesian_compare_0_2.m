function mnPE(jobNumber,analysisType,dataSet,doRun);

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_mn';
dataSet = 'MDC0all';
analysisType = 'powerlaw';
jobNumber = 1;
plotDir1 = [baseplotDir '/' dataSet '/' analysisType '/' num2str(jobNumber)];
matFile1 = [plotDir1 '/multinest.mat'];
data1 = load(matFile1,'prior','lik');

[ff_1,rbartilde1_1,rbartilde2_1,r1r1_1,r1r2_1,r2r1_1,r2r2_1,orf_1,psd1_1,psd2_1] = ... 
   bayesian_get_data(jobNumber,dataSet,plotDir1);

dataSet = 'MDC2all';
analysisType = 'powerlaw';
jobNumber = 1;
plotDir2 = [baseplotDir '/' dataSet '/' analysisType '/' num2str(jobNumber)];
matFile2 = [plotDir1 '/multinest.mat'];
data2 = load(matFile2,'prior','lik');

[ff_2,rbartilde1_2,rbartilde2_2,r1r1_2,r1r2_2,r2r1_2,r2r2_2,orf_2,psd1_2,psd2_2] = ...
   bayesian_get_data(jobNumber,dataSet,plotDir2);


ff = ff_1;
rbartilde1 = rbartilde1_1 - rbartilde1_2;
rbartilde2 = rbartilde2_1 - rbartilde2_2;
orf = orf_1;

plotDir = [baseplotDir '/' 'compare' '/' analysisType '/' num2str(jobNumber)];
createpath(plotDir);

plotName = [plotDir '/r1r2'];
figure;
loglog(ff,abs(r1r2_1),'k--');
hold on
loglog(ff,abs(r1r2_2),'b');
loglog(ff,r1r1_1,'r');
%loglog(ff,psd1_1,'r');
%loglog(ff,psd1_2,'g');
hold off
xlim([0 256]);
xlabel('Frequency [Hz]');
ylabel('r1r2');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/r1r2_semilog'];
figure;
semilogx(ff,r2r1_1,'k--');
hold on
semilogx(ff,r2r1_2,'b');
%loglog(ff,1e-37 * abs(orf),'r');
%loglog(ff,r1r1_1,'r');
%loglog(ff,psd1_2,'g');
hold off
xlim([0 256]);
%ylim([-0.5e-37 0.5e-37]);
xlabel('Frequency [Hz]');
ylabel('r1r2');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

keyboard

if strcmp(dataSet,'MDC0all') || strcmp(dataSet,'MDC2all') || strcmp(dataSet,'MDC3all') || strcmp(dataSet,'MDC4all') || strcmp(dataSet,'MDC5all')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1540;
   deltaFconstant = 10240;
elseif strcmp(dataSet,'MDC2inj') || strcmp(dataSet,'MDC3inj') || strcmp(dataSet,'MDC4inj') || strcmp(dataSet,'MDC5inj') || strcmp(dataSet,'MDC2inj_mid') || strcmp(dataSet,'MDC2inj_low') || strcmp(dataSet,'none')
   windowconst = 0.375;
   L = 2048*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1;
   deltaFconstant = 1;
   deltaFconstant = 1024;
   orf = 1;
else
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1540;
   deltaFconstant = 10240;
end

factor = averages*L*(windowconst/(2*fs*deltaFconstant.^2));
fprintf('%.10f\n',factor);
%keyboard
psd = load(psdfile);
Sn = interp1(psd(:,1),psd(:,2),ff).^2;
Sn = Sn*factor;

omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_2_BNS_nonDet/omega.txt';
omega = load(omegafile);
ff_omega = omega(:,1);
omega_data = omega(:,2);
omega_psd_data = omega(:,3);
omega_psd_data = omega_psd_data*averages*L*(windowconst/(2*fs*deltaFconstant.^2));

params.savePlots = 1;
prior = data1.prior;
lik = data1.lik - data2.lik;

if params.savePlots

   [C,I] = max(lik(:));
   [I1,I2] = ind2sub(size(lik),I);
   lik_max = lik(I1,I2);

   if strcmp(analysisType,'powerlaw');

      a = prior{1,6}(I1);
      k = prior{2,6}(I2);
      powerlaw_data = powerlaw(ff,a,k);

      [junk,index] = max(lik(:));
      fprintf('a: %.5f, k: %.5f, likelihood: %.5e\n',a,k,lik(index));

      plotName = [plotDir '/omega_best'];
      figure;
      %loglog(ff,real(rbartilde1.*rbartilde2),'k--');
      loglog(ff,2*abs(rbartilde1).^2/(L*fs*windowconst*deltaT*deltaT),'k--');
      hold on
      loglog(ff,2*powerlaw_data/(L*fs*windowconst*deltaT*deltaT),'b');
      loglog(ff_omega,omega_psd_data*1e60,'r');
      hold off
      xlim([0 256]);
      ylim([1e5 1e15]);
      xlabel('Frequency [Hz]');
      ylabel('S_{h}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      plotName = [plotDir '/omega_best_mdc'];
      figure;
      %loglog(ff,real(rbartilde1.*rbartilde2),'k--');
      loglog(ff,abs(rbartilde1.*rbartilde2),'k--');
      hold on
      loglog(ff,powerlaw_data,'b');
      loglog(ff,Sn,'g');
      loglog(ff_omega,omega_psd_data,'r');
      hold off
      xlim([0 256]);
      %ylim([1e5 1e15]);
      xlabel('Frequency [Hz]');
      ylabel('S_{h}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

   end

   xlab = get_name(prior{1,1});
   ylab = get_name(prior{2,1});

   %lik = lik-max(lik(:));
   lik = real(lik);
   Ls = 10.^lik;

   %plotName = [plotDir '/contour'];
   %figure()
   %imagesc(prior{1,6},prior{2,6},lik');
   %set(gca,'YDir','normal')
   %set(gca,'XScale','lin');
   %xlabel(xlab);
   %ylabel(ylab);
   %cbar = colorbar;
   %set(get(cbar,'title'),'String','log10(Likelihood)');
   %vmin = max(lik(:))*0.9; vmax = max(lik(:));
   %caxis([vmin vmax]);
   %colorbar();
   %print('-dpng',plotName);
   %print('-depsc2',plotName);
   %close;

   vmax = max(lik(:));
   vmin = 0.9*vmax;
   %vmin = 1.1*vmax; 

   vmin
   vmax

   [Xq, Yq] = meshgrid(prior{1,6},prior{2,6});
   Vq = lik';

   plotName = [plotDir '/loglikelihood'];
   figure()
   pcolor(Xq,Yq,Vq);
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   caxis([vmin vmax]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   norm = max(lik(:));
   lik = exp(lik - norm);
   Vq = lik';

   plotName = [plotDir '/likelihood'];
   figure()
   pcolor(Xq,Yq,Vq);
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   %caxis([vmin vmax]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   zmax = max(lik(:));
   zvals = 0:zmax/2000:zmax;
   znorm = sum(lik(:));
   for ll=1:length(zvals)
      prob(ll) = sum(sum(lik(lik>=zvals(ll))));
   end
   prob = prob/znorm;

   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD') || strcmp(analysisType,'DNS');
      prob = fliplr(prob);
   end
   zvals68 = zvals(abs(prob-0.68)==min(abs(prob-0.68)));
   zvals95 = zvals(abs(prob-0.95)==min(abs(prob-0.95)));
   zvals99 = zvals(abs(prob-0.99)==min(abs(prob-0.99)));
   zcontours = [zvals68(1) zvals95(1) zvals99(1)];
   %[C, h1] = contour(aa, epsilon, lik, zvals95);

   plotName = [plotDir '/likecontour'];
   figure;
   [C, h] = contour(Xq, Yq, Vq, zcontours(1),'k');
   hold on
   [C, h] = contour(Xq, Yq, Vq, zcontours(2),'b');
   [C, h] = contour(Xq, Yq, Vq, zcontours(3),'r');
   hold off
   xlabel(xlab);
   ylabel(ylab);
   %cbar = colorbar;
   %set(get(cbar,'ylabel'),'String','Probability');

   bayesian_get_injplot(analysisType,dataSet);

   legend('68 CL','95 CL','99 CL','True value','Location','northeast','Orientation','horizontal')
   pretty;
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   keyboard
end




