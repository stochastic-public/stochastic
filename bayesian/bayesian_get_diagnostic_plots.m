function bayesian_get_Sn(jobNumber,dataSet,plotDir,injSNR,ff,rbartilde1,rbartilde2,...
      r1r1,r1r2,r2r1,r2r2,psd1,psd2,npsd1,npsd2,orf,Sn1,Sn2);

if strcmp(dataSet,'SBpoint');

   plotName = [plotDir '/fft_point'];
   figure;
   loglog(ff,abs(r1r2(:,1)),'k--');
   %loglog(ff,abs(rbartilde1),'k--');
   hold on
   loglog(ff,r1r1(:,1),'b');
   loglog(ff,r2r2(:,1),'r');
   loglog(ff,Sn1,'g');
   loglog(ff,Sn2,'m');
   %loglog(ff,sqrt(powerlaw(ff,-39,2/3 - 3)/2),'r')
   hold off
   xlim([0 256]);
   %ylim([1e5 1e15]);
   xlabel('Frequency [Hz]');
   ylabel('S_{h}');
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;
end

plotName = [plotDir '/fft'];
figure;
loglog(ff,abs(rbartilde1.*rbartilde2),'k--');
%loglog(ff,abs(rbartilde1),'k--');
hold on
loglog(ff,abs(rbartilde1.*rbartilde1),'b');
loglog(ff,abs(rbartilde2.*rbartilde2),'r');
%loglog(ff,sqrt(powerlaw(ff,-39,2/3 - 3)/2),'r')
loglog(ff,Sn1,'g');
loglog(ff,Sn2,'m');
hold off
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('S_{h}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

%h0 = 0.7;
%omega_to_psd = 3.95364e-37*(h0/0.704)*(h0/0.704)*(1./ff.^3);

plotName = [plotDir '/psd'];
figure;
loglog(ff,Sn1,'k--');
hold on
loglog(ff,Sn2,'b');
%loglog(ff,sqrt(powerlaw(ff,-39,2/3 - 3)/2),'r')
loglog(ff,psd1,'g');
loglog(ff,psd2,'m');
%loglog(ff,DNS(ff,1.4,-8).*omega_to_psd,'c')
hold off
xlim([0 256]);
%if strcmp(dataSet,'MDC2nonoise') || strcmp(dataSet,'MDC2nonoiseplus') || strcmp(dataSet,'MDC2noise') || strcmp(dataSet,'MDC2noiseflat') || strcmp(dataSet,'MDC2noise0')
%   ylim([1e-48 1e-38]);
%end
xlabel('Frequency [Hz]');
ylabel('PSD');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/orf'];
figure;
semilogx(ff,orf,'k--');
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('ORF');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

