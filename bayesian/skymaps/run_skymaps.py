#!/usr/bin/python

"""
% startnight.py

Written by Chris Stubbs and Michael Coughlin (michael.coughlin@ligo.org)

This program runs the nightly Cerro Pachon All-Sky Camera Project camera.

"""

from __future__ import division

import os, sys, pickle, math, optparse, glob, time, subprocess
from datetime import date, datetime
import numpy as np
import scipy.ndimage
import astropy
import aplpy
import ephem

import healpy as hp

import scipy.io
import h5py

import matplotlib
#matplotlib.rc('text', usetex=True)
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 16})
matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from matplotlib.mlab import griddata

import cosmolopy.distance as cd

def healpix(nside,plotDir):

    print "Pixel area: %.4f square degrees" % hp.nside2pixarea(nside, degrees=True) 
    npix = hp.nside2npix(nside)
    theta, phi = hp.pix2ang(nside, np.arange(npix))
    ra = np.rad2deg(phi)
    dec = np.rad2deg(0.5*np.pi - theta)

    matFile = os.path.join(plotDir,"multinest.mat")
    data_out = scipy.io.loadmat(matFile)

    print len(theta), len(phi)

    nest_samples = data_out["nest_samples"]
    a_cat = nest_samples[:,0]
    k_cat = nest_samples[:,1]
    ra_cat = nest_samples[:,2]
    dec_cat = nest_samples[:,3]

    ra_cat = ra_cat * 360.0/24.0
    dec_cat = dec_cat * 1.0

    phi_cat = np.deg2rad(ra_cat)
    theta_cat = 0.5*np.pi - np.deg2rad(dec_cat) 

    indexes_hpix = hp.ang2pix(nside, theta_cat, phi_cat) 

    #indexes_hpix = np.loadtxt(indexesFile).astype(int)
    indexes = np.arange(len(indexes_hpix))
    a_cat = a_cat[indexes]
    k_cat = k_cat[indexes]
    ra_cat = ra_cat[indexes]
    dec_cat = dec_cat[indexes]

    healpix_cat = np.zeros((len(phi),))
    #healpix_cat[indexes_hpix] = healpix_cat[indexes_hpix] + np.ones(indexes_hpix.shape)
    for index in indexes_hpix:
        healpix_cat[index] = healpix_cat[index] + 1
    healpix_cat[indexes_hpix] = healpix_cat[indexes_hpix] / np.sum(healpix_cat[:])
    indexes = np.where(~np.isnan(healpix_cat))
    vmin = np.min(healpix_cat[indexes])
    vmax = np.max(healpix_cat[indexes])
    vmin = vmax*0.01
    indexes = np.where(healpix_cat == 0)[0]
    healpix_cat[indexes] = np.nan

    plotDir = "%s/skymaps/%d"%(plotDir,nside)
    mkdir(plotDir)
    print plotDir

    plotName = os.path.join(plotDir,'mollview_cat.png')
    hp.mollview(healpix_cat, min=vmin,max=vmax,title="")
    hp.projscatter(0.5*np.pi - np.deg2rad(30.0), np.deg2rad(6.0*360.0/24.0), s=200.0, c='k', alpha=0.5, marker = 'x')
    hp.graticule()
    plt.show()
    plt.savefig(plotName,dpi=200)
    plotName = os.path.join(plotDir,'mollview_cat.eps')
    plt.savefig(plotName,dpi=200)
    plotName = os.path.join(plotDir,'mollview_cat.pdf')
    plt.savefig(plotName,dpi=200)
    plt.close('all')


def mkdir(path):
    """@create path (if it does not already exist).

    @param path
        directory path to create
    """

    pathSplit = path.split("/")
    pathAppend = "/"
    for piece in pathSplit:
        if piece == "":
            continue
        pathAppend = os.path.join(pathAppend,piece)
        if not os.path.isdir(pathAppend):
            os.mkdir(pathAppend)

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_mn/simplePoint/powerlaw/1'
baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_mn/simplePoint/powerlawPSDpowerlaw/1'
#baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_mn/SBpoint/powerlaw/1'
plotDirs = glob.glob(os.path.join(baseplotDir,'1000000000*'))
plotDirs = glob.glob(os.path.join(baseplotDir,'*'))
#plotDirs = plotDirs[1:]

nsides = [32,64,128,256,512]
#nsides = [128,512]

for plotDir in plotDirs:
    for nside in nsides:
        healpix(nside,plotDir)

