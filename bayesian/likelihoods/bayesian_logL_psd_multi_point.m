function logL = stamp_logL(data, model, parnames, parvals)

% logL = logL_gaussian(data, model, parnames, parvals)
%
% This function will compute the log likelihood of a multivariate
% gaussian:
%
%     L = 1/sqrt((2 pi)^N det C)
%         exp[-0.5*(y - model(x,params))^T * inv(C) * (y - model(x,params))]
%
% The input parameters are:
%     data - a cell array with three columns
%            { x values, y values, C: covariance matrix }
%     NOTE: if C is a single number, convert to a diag covariance matrix
%     model - the function handle for the signal model.
%     parnames - a cell array listing the names of the model parameters
%     parvals - a cell array containing the values of the parameters given
%         in parnames. These must be in the same order as in parnames. 
%         If parvals is an empty vector the noise-only likelihood will be 
%         calculated.
%
% -------------------------------------------------------------------------
%           This is the format required by nested_sampler.m.
% -------------------------------------------------------------------------

% check whether model is a string or function handle
if ischar(model)
    fmodel = str2func(model);
elseif isa(model, 'function_handle')
    fmodel = model;
else
    error('Error... Expecting a model function!');
end

% get data values from cell array
Sn1 = data{1};
Sn2 = data{2};
r1r1 = data{3};
r1r2 = data{4};
r2r1 = data{5};
r2r2 = data{6};
orf = data{7};
ff = data{8};
 
% evaluate the model
if isempty(parvals)
    % if parvals is not defined get the null likelihood (noise model
    % likelihood)
    Sh = zeros(size(YY));
else
    Sh = feval(fmodel, ff, parnames, parvals);
    
    % if the model returns a NaN then set the likelihood to be zero (e.g. 
    % loglikelihood to be -inf
    if isnan(Sh)
        logL = -inf;
        return;
    end
end

lpn = length(parnames);
lpv = length(parvals);
if lpn ~= lpv
    error('Error: parnames and parvals are not the same length!');
end

nparams = lpn;

% extract parameter values
for ii=1:nparams
  switch parnames{ii}
    case 'k'
      k = parvals{ii};
    case 'a'
      a = parvals{ii};
    case 'r'
      ra = parvals{ii};
    case 'd'
      decl = parvals{ii};
    case 'A'
      A = parvals{ii};
    case 'K'
      K = parvals{ii};
  end
end

Sn1 = powerlaw(ff,A,K);
Sn2 = powerlaw(ff,A,K);

det1 = getdetector('LHO');
det2 = getdetector('LLO');
%gps = 800000000;
%siderealtime=GPStoGreenwichMeanSiderealTime(gps);
%g=orfIntegrandSymbolic(det1,det2,siderealtime,ra,decl);

params.doGPU = 0;
params.gpu.precision = 'double';
Sn1 = gArray(Sn1,params);
Sn2 = gArray(Sn2,params);
Sh = gArray(Sh,params);
orf = gArray(orf,params);

gpss = 0:30:86164;
%gpss = gpss(1:2);
%gpss = gpss(1);
siderealtimes=GPStoGreenwichMeanSiderealTime(gpss);

logLtot = zeros(size(siderealtimes));
%parfor i = 1:length(siderealtimes)
%parfor i = 1:10
%for i = 10
for i = 1:2

   siderealtime = siderealtimes(i);
   g=orfIntegrandSymbolic(det1,det2,siderealtime,ra,decl);

   kappa11= (1/(4*pi))*(g.F1p*g.F1p + g.F1x*g.F1x)*Sh.*exp(2*pi*sqrt(-1)*ff*0);
   kappa12= (1/(4*pi))*(g.F1p*g.F2p + g.F1x*g.F2x)*Sh.*exp(2*pi*sqrt(-1)*ff*g.tau);
   kappa21= (1/(4*pi))*(g.F2p*g.F1p + g.F2x*g.F1x)*Sh.*exp(-2*pi*sqrt(-1)*ff*g.tau);
   kappa22= (1/(4*pi))*(g.F2p*g.F2p + g.F2x*g.F2x)*Sh.*exp(2*pi*sqrt(-1)*ff*0);

   kappa11= gArray(kappa11,params);
   kappa12= gArray(kappa12,params);
   kappa21= gArray(kappa21,params);
   kappa22= gArray(kappa22,params);

   C11 = Sn1 + kappa11;
   C12 = kappa12;
   C21 = kappa21;
   C22 = Sn2 + kappa22;

   detC =C11.*C22 - C12.*C21;
   Cinv11 = C22./detC;
   Cinv12 = -C12./detC;
   Cinv21 = -C21./detC;
   Cinv22 = C11./detC;

   deltaF = ff(2) - ff(1);
   %expTerm = -2*(Cinv11.*r1r1(:,i) + Cinv21.*r2r1(:,i) + Cinv12.*r1r2(:,i) + Cinv22.*r2r2(:,i))*deltaF;
   expTerm = -2*(Cinv11.*r1r1(:,i) + Cinv21.*r2r1(:,i) + Cinv12.*r1r2(:,i) + Cinv22.*r2r2(:,i));
   %expTerm = -2*(Cinv12.*ri1.*rj2);
   % Single sided FFT
   expTerm = real(expTerm);
   %logLike = log((1/(2*pi)^2).*(1./detC)) + expTerm;
   logLike = log((1/(pi/2)^2).*(1./detC)) + expTerm;

   logL = real(sum(logLike));
   logL = gGather(logL,params);
   logLtot(i) = logL;
end
logL = sum(logLtot);

%fprintf('%.5e %.5e\n',sum(Sn),sum(Sh));

if isnan(logL)
   logL = -1e300;
%    error('Error: log likelihood is NaN!');
end

if logL < -1e300
   logL = -1e300;
end

%fprintf('%.2f %.2f %.2f %.5e\n',parvals{1},parvals{2},parvals{3},logL);

%logL = round(logL/10)*10;

%figure;
%semilogx(ff,logLike,'k*');
%print('-dpng','test.png');
%close;

return
