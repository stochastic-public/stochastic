export ARCH=glnxa64
export LD_LIBRARY_PATH=${MATLAB_ROOT}/sys/opengl/lib/${ARCH}:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${MATLAB_ROOT}/sys/java/jre/${ARCH}/jre/lib/amd64:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${MATLAB_ROOT}/sys/java/jre/${ARCH}/jre/lib/amd64/server:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${MATLAB_ROOT}/sys/java/jre/${ARCH}/jre/lib/amd64/native_threads:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${MATLAB_ROOT}/bin/${ARCH}:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${MATLAB_ROOT}/sys/os/${ARCH}:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${MATLAB_ROOT}/runtime/${ARCH}:${LD_LIBRARY_PATH}
export XAPPLRESDIR=${MATLAB_ROOT}/X11/app-defaults

