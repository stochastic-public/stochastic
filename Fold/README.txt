
foldSID folds/compresses Stochastic Intermediate Data (SID) to one sidereal day 
- new product is called Folded Stochastic Intermediate Data (FSID).

Starting from SID frames FSID can be generated is three steps described below.

For detailed mathematics, see LIGO technical document: T0900093



0. Download and Make: Folding code is part of matapps, the codes are in this
        directory in matapps (SVN): packages/stochastic/trunk/Fold/

        NOTE: Makefile to compile MATLAB files and condor .sub files work at the
        CIT cluster. They have to be modified to use on different computers.
        Define MATAPPS_STOCH, may be ${HOME}/matapps/packages/stochastic/trunk/


1. genSIDMetaData.sh: Generates a two column SID metadata file.
	Column 1: GPS start time; Column 2: SID frame file
	The program uses either ligo_data_find or unix find depending on the
	supplied command line info. It automatically excludes the
	empty frame files and prints them on screen.


   Example: to generate SID metadata file, use
	
	./genSIDMetaData.sh SIDMetaData_S5-1wk.txt 844402324 846219804
	(LSCDataFind mode)
        this will generate the metadata file for one week's S5 data

	OR (on CIT cluster)

	./genSIDMetaData.sh SIDMetaData_S5-full.txt /archive/frames/S5/SGWB/LHO
	(Unix find mode)
        this will generate the metadata file for whole S5 data


   Help: run the script (genSIDMetaData.sh) without any argument



2. foldSID.par: Parameter file. The parameters are:

	Compulsory Parameters:

	ifo1:               Interferometer 1 (e.g. H1)
	ifo2:               Interferometer 2 (e.g. L1)
	SIDMetaDataFile:    The file generated in step 1
	segmentDuration:    Length of segments in SID frames


	Optional parameters (default behavior in parenthesis): [No=0,Yes=1]

	FSIDFramePrefix (don't write): File prefix to write FSID frames
	segmentsPerFrame (60):         Number of segments per frame
        FSIDGPSOffset (0):             GPS offset for sidereal times
        FSIDGPSOffsetCorrect (0):      Round off GPS Offset to 0 sidereal time
	version (no tag):              Version tag for frame filenames
	backwardCompatible(false):     stochastic.m compatibility
	identicalNeighbors(false):     Neighboring segments have same PSD
        maxASigma (-1.0 [disable]):    Max abs Sigma to ensure stationarity
        maxDSigma (-1.0 [disable]):    Max Sigma ratio to ensure stationarity
        minDSigma (-1.0 [disable]):    Min Sigma ratio to ensure stationarity
        sigmaCutWeightFile (''):       Extra weights for broadband sigma cuts
                                       First column f, second *sqrt* of weight
                                       The array is linear-interpolated
                                       No extrapolation, 0 outside the range
                                       ** IMPORTANT: frequency notches MUST be
                                       incorporated here, without which
                                       quality cuts are hardly meaningful **
        smoothWinSize4SigmaCut (-1):   # bins to smooth over for sigma cuts
                                       If larger than available # f bins,
                                       cuts are based on f integrated PSDs
                                       If -ve, use f integrated weight/(P_1 P_2)
	badGPSTimesFile                List of bad GPS segments
	FSIDJobFile (don't write):     Write a job file for stochastic.m
	ovlWinCorrection (1):          Overlapping window correction
	siderealDay (86164):           If different from 86164s
	logFile (stdout):              Print messages in this file
	logStep (1000):                Print status after this many steps
	verbose (0):                   Verbose mode


   Example: template.par included in the Fold directory on SVN


3. genDag.pl: Generate DAG file for parallel processing using condor.
	Each process will fold some part of data and optionally the output
	from each process will be combined. It also allows to select
	the part of the SID metadata file to be processed.


   Example: to fold one week's S5 SID, create a param file foldSID-1wk.par & use

        source set_env.sh
	./genDAG.pl fold.dag 10 -1 -1 foldSID.par ../tmp/S5-1wk ../S5-1wk.mat

        ** all the output directories must be created before job submission **

   	Then submit condor job using: condor_submit_dag fold.dag

        This will fold data to the the file S5-1wk.mat and also in frames (if
        the FSIDFramePrefix option is chosen in foldSID.par)


   Help: run the script (genDAG.pl) without any argument

