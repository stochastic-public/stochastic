function writeFSID (foldParams, params, foldedInvCov, foldedStatistic)
%
% Write Folded Stochastic Intermediate Data to frames (.gwf)
%
%
% writeFSID (foldParams, params, foldedInvCov, foldedStatistic);
%
% foldParams      : Structure. Essential elements:
%                    ifo1              : String. Detector 1
%                    ifo2              : String. Detector 2
%                    foldedSegDuration : Real. Duration of folded segments
%                                        This is the foldedSegDuration
%                    nSegment          : Integer. Number of segments to save
%                                        (sidereal day / foldedSegDuration)
%                    segDist           : Integer Array. Number of SID segments
%                                        folded to each sidereal segment
%                    segmentsPerFrame  : Integer. Max # segments per frame
%                                        If -1, put all FSID in one frame
%                    FSIDFramePrefix   : String. Prefix for frame files
%
%                   Optional Elements:
%                    FSIDGPSOffset     : Integer. Offset to be added to GPSStart
%                    startOffset       : Real. Offset added to siderealStart
% params          : Structure. Parameters from the first SID frame and some more
% foldedInvCov    : Real matrix (t,f). Inverse variance of folded statistic
% foldedStatistic : Complex matrix (t,f). Folded statistic
%

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


%% Check arguments
if (nargin < 4 | nargin > 4)
  error(['Argument number mismatch. For usage info: help ' mfilename]);
end


%========= Extract useful parameters from the parameter structures =========%

ifo1              = foldParams.ifo1;
ifo2              = foldParams.ifo2;
nSegment          = foldParams.nSegment;
foldedSegDuration = foldParams.foldedSegDuration;
FSIDFramePrefix   = foldParams.FSIDFramePrefix;
segmentsPerFrame  = foldParams.segmentsPerFrame;
FSIDGPSOffset     = foldParams.FSIDGPSOffset;
if (segmentsPerFrame == -1)
  segmentsPerFrame = nSegment;
end

try
  backwardCompatible = foldParams.backwardCompatible;
  winFactor = params.winFactor;
catch
  backwardCompatible = false;
end

try
  identicalNeighbors = foldParams.identicalNeighbors;
catch
  identicalNeighbors = false;
end

try
  SigmaCut = foldParams.SigmaCut;
catch
  SigmaCut.maxA = -1.0;
  SigmaCut.maxD = -1.0;
  SigmaCut.minD = -1.0;
  SigmaCut.smoothSpan = 0;
  SigmaCut.logfp = 1;
  SigmaCut.applyAS = false;
  SigmaCut.applyDS = false;
end

try
  FSIDJobFilePtr = fopen(foldParams.FSIDJobFile,'w');
  writeFSIDJobFile = true;
catch
  writeFSIDJobFile = false;
end

try
  FSIDGPSOffset = foldParams.FSIDGPSOffset;
catch
  FSIDGPSOffset = 0;
end

try
  siderealDay = foldParams.siderealDay;
catch
  siderealDay = 86164.1;
end

try
  startOffset = foldParams.startOffset;
catch
  startOffset = 0;
end

try
  totNSegments = sum(foldParams.segDist);
catch
  totNSegments = 0;
end

try
  versionString = sprintf('v%d',foldParams.version);
catch
  versionString = '';
end

try
  verbose = foldParams.verbose;
catch
  verbose = false;
end

try
  logfp = foldParams.logfp;
catch
  logfp = 1;
end


% If overlapping window is on, skip half segment to match mid-segment of SIDs
if foldParams.ovlWinCorrection
  startOffset = startOffset + foldedSegDuration/2.0;
end


% Missing segments
missingSegments = find(~foldParams.segDist);
if (length(missingSegments) > 0) & (verbose)
  missingSegments
end


% Entry for job file
jobCount = 0;
jobStart = -1;


%=============== Distribute and write folded data in frames ================%

for iSegment = 1:segmentsPerFrame:nSegment

  % Number of segments in current frame
  nSegment_thisFile = min(nSegment-iSegment+1, segmentsPerFrame);

  % Find missing segments for this file
  missingSegments = ...
    find(~foldParams.segDist(iSegment:iSegment+nSegment_thisFile-1));
  
  % If all segments are missing, write jobfile entry and don't do anything else
  if (length(missingSegments) == nSegment_thisFile)

    % Write jobfile entry
    if (writeFSIDJobFile & (jobStart >= 0))
      jobCount = jobCount + 1;
      fprintf(FSIDJobFilePtr,'%d\t%d\t%d\t%d\n',...
        jobCount, jobStart, jobEnd, jobEnd - jobStart);
      jobStart = -1;      
    end

    if (verbose)
      fprintf(logfp,...
        '\n** No data in sidereal time segment #%d, skipping **\n', iSegment);
    end
    
    continue;
  end
  
  % Start and end sidereal time of data in current frame
  siderealStart = startOffset   + (iSegment-1)*foldedSegDuration;
  siderealEnd   = siderealStart + foldedSegDuration*nSegment_thisFile;
  
  % Everytime there is a break in jobfile, move to next sidereal day
  thisGPSStart  = round(siderealStart + FSIDGPSOffset + jobCount*siderealDay);
  thisGPSEnd    = round(siderealEnd   + FSIDGPSOffset + jobCount*siderealDay);
  
  % To disable moving next contiguous segment to next sidereal day
  %thisGPSStart  = round(siderealStart + FSIDGPSOffset);
  %thisGPSEnd    = round(siderealEnd   + FSIDGPSOffset);
  
  % Entry for job file
  % Keep enough gap before and after a job interval, including buffer
  if writeFSIDJobFile
    if (jobStart < 0)
      jobStart = thisGPSStart - round(3*foldedSegDuration);
    end
    jobEnd = thisGPSEnd + round(3*foldedSegDuration);
  end


  % Frame file name
  frameFile = sprintf('%s%c%c-FSID%s_%s%s_%d%s-%09d-%d.gwf',FSIDFramePrefix,...
    ifo1(1),ifo2(1),versionString,ifo1,ifo2,round(1000*params.deltaF),'mHz',...
    thisGPSStart, thisGPSEnd - thisGPSStart);

  if (verbose)
    fprintf(logfp,'\nWriting segments %d through %d to file %s \n', ...
        iSegment, iSegment+nSegment_thisFile-1,frameFile);
  end


  irec = 0;

    
  %=============================== T-F maps ================================%

  % Folded statistic
  write_buffer_cplx = foldedStatistic(iSegment:iSegment+nSegment_thisFile-1,:)';
  if isreal(write_buffer_cplx)
    write_buffer_cplx = complex(write_buffer_cplx);
  end

  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':ReFoldedStatistic'];
  rec(irec).data    = real(write_buffer_cplx(:));
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';

  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':ImFoldedStatistic'];
  rec(irec).data    = imag(write_buffer_cplx(:));
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';

  clear write_buffer_cplx;
  

  % Folded inverse covariance : diagonal (variance)
  write_buffer   = foldedInvCov.Diag(iSegment:iSegment+nSegment_thisFile-1,:)';
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':FoldedInvVar'];
  rec(irec).data    =  write_buffer(:);
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';
  clear write_buffer;

  % Folded inverse covariance : diagonal - 1 (Cov with previous segment)
  write_buffer   = foldedInvCov.Prev(iSegment:iSegment+nSegment_thisFile-1,:)';
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':FoldedInvCovPrev'];
  rec(irec).data    = write_buffer(:);
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';
  clear write_buffer;

  % Folded inverse covariance : diagonal + 1 (Cov with next segment)
  write_buffer   = foldedInvCov.Next(iSegment:iSegment+nSegment_thisFile-1,:)';
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':FoldedInvCovNext'];
  rec(irec).data    = write_buffer(:);
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';
  clear write_buffer;


  % Make frames directly compatible to isotropic stochastic search, if needed
  if (backwardCompatible)
    
    write_buffer = winFactor ./ (...
      foldedInvCov.Diag(iSegment:iSegment+nSegment_thisFile-1,:) ...
      - foldedInvCov.Prev(iSegment:iSegment+nSegment_thisFile-1,:) ...
      - foldedInvCov.Next(iSegment:iSegment+nSegment_thisFile-1,:))';

    %% If no segment was folded in a given segment, make PSD a large float
    if length(missingSegments) > 0
      write_buffer(:,missingSegments) = realmax;
    end
    
    irec = irec + 1;
    %rec(irec).channel = [ifo1 ':LocalPSD'];
    rec(irec).channel = [ifo1 ':AdjacentPSD'];
    rec(irec).data    =  sqrt(abs(write_buffer(:)));
    rec(irec).type    = 'd';
    rec(irec).mode    = 'a';

    irec = irec + 1;
    %rec(irec).channel = [ifo2 ':LocalPSD'];
    rec(irec).channel = [ifo2 ':AdjacentPSD'];
    rec(irec).data    =  sqrt(abs(write_buffer(:)));
    rec(irec).type    = 'd';
    rec(irec).mode    = 'a';


    %write_buffer_cplx = ...
    %  (foldedStatistic(iSegment:iSegment+nSegment_thisFile-1,:) ./ ...
    %  foldedInvCov.Diag(iSegment:iSegment+nSegment_thisFile-1,:))';
    
    % Divide by the produce of adjact PSDs, so that when the pipeline divides
    % CSD by the product, it effectively processes data in which overlapping
    % window correction has already been acocunted for
    write_buffer_cplx = (write_buffer/winFactor) .* ...
      foldedStatistic(iSegment:iSegment+nSegment_thisFile-1,:)';
    
    %% If no segment was folded in a given segment, make CSD a small float
    if length(missingSegments) > 0
      write_buffer_cplx(:,missingSegments) = realmin;
    end
    if isreal(write_buffer_cplx)
      write_buffer_cplx = complex(write_buffer_cplx);
    end
    
    irec = irec + 1;
    rec(irec).channel = [ifo1 ifo2 ':ReCSD'];
    rec(irec).data    = real(write_buffer_cplx(:));
    rec(irec).type    = 'd';
    rec(irec).mode    = 'a';

    irec = irec + 1;
    rec(irec).channel = [ifo1 ifo2 ':ImCSD'];
    rec(irec).data    = imag(write_buffer_cplx(:));
    rec(irec).type    = 'd';
    rec(irec).mode    = 'a';

    clear write_buffer;
    clear write_buffer_cplx;

  end

  
  %============================== Parameters ===============================%


  % Fake GPS Time
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':GPStime'];
  rec(irec).data    = thisGPSStart;
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';
  
  %% Fake GPS Start
  %irec = irec + 1;
  %rec(irec).channel = [ifo1 ifo2 ':GPSStart'];
  %rec(irec).data    = thisGPSStart;
  %rec(irec).type    = 'd';
  %rec(irec).mode    = 'a';

  %% Fake GPS End
  %irec = irec + 1;
  %rec(irec).channel = [ifo1 ifo2 ':GPSEnd'];
  %rec(irec).data    = thisGPSEnd;
  %rec(irec).type    = 'd';
  %rec(irec).mode    = 'a';

  %% Folded Segment Duration, write as segment duration
  %irec = irec + 1;
  %rec(irec).channel = [ifo1 ifo2 ':segmentDuration'];
  %rec(irec).data    = foldParams.segDuration;
  %rec(irec).type    = 'd';
  %rec(irec).mode    = 'a';

  %% Folded Segment Duration
  %irec = irec + 1;
  %rec(irec).channel = [ifo1 ifo2 ':FoldedSegmentDuration'];
  %rec(irec).data    = foldParams.segDuration;
  %rec(irec).type    = 'd';
  %rec(irec).mode    = 'a';

  % Sidereal Start
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':SiderealStart'];
  rec(irec).data    = siderealStart;
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';

  % Sidereal End
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':SiderealEnd'];
  rec(irec).data    = siderealEnd;
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';

  % GPS offset for folded frames
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':FSIDGPSOffset'];
  rec(irec).data    = FSIDGPSOffset;
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';

  % Sidereal start time offset for folded frames
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':FSIDStartOffset'];
  rec(irec).data    = startOffset;
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';
 
  % Total number of SID frames folded
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':totSIDSegments'];
  rec(irec).data    = int32(totNSegments);
  rec(irec).type    = 'i';
  rec(irec).mode    = 'a';

  % Is it going to work for :iSegment+nSegment_thisFile > 1?
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':nSIDSegment'];
  rec(irec).data    = ...
    int32(foldParams.segDist(iSegment:iSegment+nSegment_thisFile-1));
  rec(irec).type    = 'i';
  rec(irec).mode    = 'a';
  
  % If identical neighbor assumption was used
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':identicalNeighbors'];
  rec(irec).data    = int32(identicalNeighbors);
  rec(irec).type    = 'i';
  rec(irec).mode    = 'a';

  
  % Smoothing bandwidth for Sigma Cuts
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':sigmaCutSmoothSpan'];
  rec(irec).data    = SigmaCut.smoothSpan * params.deltaF;
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';

  % Max AbsSigma value
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':maxASigma'];
  rec(irec).data    = SigmaCut.maxA;
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';

  % Max DeltaSigma value
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':maxDSigma'];
  rec(irec).data    = SigmaCut.maxD;
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';

  % Max DeltaSigma value
  irec = irec + 1;
  rec(irec).channel = [ifo1 ifo2 ':minDSigma'];
  rec(irec).data    = SigmaCut.minD;
  rec(irec).type    = 'd';
  rec(irec).mode    = 'a';


  % Write other parameters explicitly also!
  % original segmentDuration is here
  tt = fieldnames(params);

  for jj = 1:length(tt)

    tmp = getfield(params,tt{jj});
    
    irec = irec + 1;
    rec(irec).channel = [ifo1 ifo2 ':' tt{jj}];
    rec(irec).data    = tmp;
    rec(irec).mode    = 'a';

    if isa(tmp,'double')
      rec(irec).type = 'd';
    elseif isa(tmp,'integer')
      rec(irec).type = 'i';
    else 
      rec(irec).type = 's';
    end

  end

  
  %============================ O B S O L E T E ============================%
  %======= Convert parameter structure to mkframe compatible format ========%
  %
  %% Storing the fold parameters only
  %savedParams.name = [ifo1 ifo2 ':Params'];
  %
  %tt = fieldnames(params);
  %
  %for jj = 1:length(tt)
  %  tmp = getfield(params,tt{jj});
  %
  %  if isnumeric(tmp)    
  %    %if it is a numeric array, needs to be a row 
  %    if (size(tmp,1)>1)
  %      tmp = transpose(tmp);
  %    end
  %    tmp = num2str(tmp); %cet: 10-10
  %  end
  %
  %  savedParams.parameters{jj*2-1}=tt{jj};
  %  savedParams.parameters{jj*2}=tmp;
  %end
  %============================ O B S O L E T E ============================%


  
  %======================= Write to frames, at last! =======================%
  
  mkframe (frameFile, rec, 'n', thisGPSEnd - thisGPSStart, thisGPSStart);

  %============================ O B S O L E T E ============================%
  %  mkframe (frameFile, rec,'n', ...
  %      thisGPSEnd-thisGPSStart, thisGPSStart, savedParams);
  %============================ O B S O L E T E ============================%

end


% Last jobfile entry
if (writeFSIDJobFile)
  if (jobStart >= 0)
    jobCount = jobCount + 1;
    fprintf(FSIDJobFilePtr,'%d\t%d\t%d\t%d\n',...
      jobCount, jobStart, jobEnd, jobEnd - jobStart);
    jobStart = -1; % Redundant
  end
  fclose(FSIDJobFilePtr);
end


return

