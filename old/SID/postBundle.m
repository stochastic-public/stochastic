% First: lsData.pl

f=textread('S5_bunchList.txt','%s');

% Based on sumXFisher.
%   -loop over *bunch2* files instead of SpHSet files
filenames=f;
for kk=1:length(filenames)
  filename=filenames{kk};
  try 
    index = load(filename);
  catch
    fprintf ('%s does not load.\n',filename);
  end
  if kk==1
    x=zeros(size(index.x));
    fisher=zeros(size(index.fisher));
    xP=zeros(size(index.xP));           %mar9
    fisherP=zeros(size(index.fisherP)); %mar9
    coh=zeros(size(index.coh));
    nsegs=0;                            %mar11
  end

  % Sum the projection X and the Fisher matrix for each iteration
  x = x + index.x;
  fisher = fisher + index.fisher;
  coh = coh + index.coh;
  xP = xP + index.xP;                  %mar9
  fisherP = fisherP + index.fisherP;   %mar9
  nsegs = nsegs + index.nsegs;         %mar11
end % for kk=1:length(filenames)

% Regularization syntax: reginv(fisher, regMethod, regCutoff)
[rinvfisher] = reginv(fisher,13,180); %optimized 4 lmax=20
[invfisher,pCovar] = reginv(fisher,2,1); %Keep all eigenvalues

% Save final .mat file
save('S5_final.mat');
