#!/usr/bin/perl

$outpath = "/archive/frames/S5/SGWB/LHO/";
$NNodes=331;
$prefix="HL-SIDv1_H1L1_250mHz";
$user="ethrane";
$MaxMove=500000;

$n=1;
$q=1;
while ($n<=$NNodes) {
    foreach $file (</data/node$n/$user/$prefix*>) {
	if ($q > $MaxMove) {print "Done.\n";   exit;}
	$outfile=$file;
	$outfile =~ s/\/data\/node$n\/$user\///;
	$dir = $outfile;
	$dir =~ s/...\.gwf//;
	$dir =~ s/(....).....$/\1/;
	if (-e $outpath.$dir) {}
	else {system "mkdir $outpath$dir";}
#	else {print "mkdir $outpath$dir\n";}
	if (-e $outpath.$dir."/".$outfile){}
	else {
	    system "cp $file $outpath$dir/$outfile";
#	    print "cp $file $outpath$dir/$outfile\n\n";
	    $q=$q+1;
	}
    }
    $n=$n+1;
}

system "touch ~/cpData.finished";
