#!/usr/bin/perl
#
# Generates S5_fileList.txt for use with bundle.m
# also generates: S5_corrupt.txt

if (-e S5_corrupt.txt) {}
else{system "grep \"corrupt\" logs/*.out > S5_corrupt.txt";}

$outfile=">S5_bunchList.txt";
if (-e $outfile) {}
else {
    $NNodes=331;
#    $prefix="HL-SpHv1_H1L1_250mHz_SpHSet*bunch*";
    $prefix="HL-SpHv1_H1L1_250mHz_SpHSet*bunch2*";
    $user="ethrane";
    $MaxMove=500000;
    open(out,$outfile);
    
    $n=1;
    $q=1;
    while ($n<=$NNodes) {
	foreach $file (</data/node$n/$user/$prefix*>) {
#	print "$file\n";
#	    system "ls -hl $file";
	    print out "$file\n";
	}
	$n=$n+1;
    }
    
    close(out);
}

print "Done.\n";
    
