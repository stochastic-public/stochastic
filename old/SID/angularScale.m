function[] = angularScale(x,fisher,coh);

sanity = 10^-48;
OmegaUL=1.20e-4;                     %Bayesian UL from S4
h0=0.72;                             %Hubbles constant
H0=100*(h0/3.08568E19);              %
fref=100;                            %reference frequency
DeltaP=OmegaUL*(3*H0^2)/(2*pi^2*fref^3*sqrt(4*pi)); %prior

lmax = sqrt(length(x))-1;
for ii=0:1:lmax
  NParms=2*(ii^2+1);
  x0=subX(x,ii);
  fisher0=subFisher(fisher,ii);
  invfisher0=inv(fisher0);
  p0=invfisher0*x0;
  
  chisquared0 = coh - ctranspose(p0)*x0 - ctranspose(x0)*p0 + ...
    real(ctranspose(p0)*fisher0*p0);
  lbayes(ii+1) = -0.5*chisquared0 + 0.5*log(det(invfisher0/sanity^2)) - ...
    NParms*log(DeltaP/sanity);
  lbayes(ii+1) = real(lbayes(ii+1));
  l(ii+1)=ii;
  lbayes_naught(ii+1)=  -0.5*coh;
end


figure(6)
plot(l,lbayes-lbayes(1))
hold on
plot(l,lbayes_naught-lbayes(1),'r--')
xlabel('lmax')
ylabel('log(P)-log(P,lmax=0)')
print('-depsc2','S5_lmax.eps')

