function as = extract_and_combine_channel(filename, ifo, n_fsr)

n_fsr = n_fsr + '0';

as3_i = frextract(filename, [ifo.name ':LSC-AS3_I_' n_fsr 'FSR']);
as3_q = frextract(filename, [ifo.name ':LSC-AS3_Q_' n_fsr 'FSR']);
as4_i = frextract(filename, [ifo.name ':LSC-AS4_I_' n_fsr 'FSR']);
as4_q = frextract(filename, [ifo.name ':LSC-AS4_Q_' n_fsr 'FSR']);

as3 = as3_i * sin(ifo.phi)   + as3_q *  cos(ifo.phi);
as4 = as4_i * sin(ifo.theta) + as4_q *  cos(ifo.theta);

as = (1/(ifo.n3 + ifo.n4)) * (ifo.n3 * as3 + ifo.n4 * as4);
