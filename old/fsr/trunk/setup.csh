#!/bin/csh
unsetenv DISPLAY
setenv MATLAB_ROOT /ldcg/matlab_r2006a

# For 32-bit machines use this line:
#setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${MATLAB_ROOT}/bin/glnx86
# For 64-bit machines use this line:
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${MATLAB_ROOT}/bin/glnxa64

setenv CVS_RSH ssh

