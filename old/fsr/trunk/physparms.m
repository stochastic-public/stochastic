function parameters = physparms();
% $Id: physparms.m,v 1.1 2006-10-18 16:40:25 tobin Exp $

% Time- and frequency-domain signal injections. Set to NaN to disable.

parameters.real_data           = 1;           % boolean
parameters.injection_td        = NaN;         % NaN or scalar
parameters.injection_fd        = NaN;         % NaN or scalar
parameters.timeshift_td        = NaN;
parameters.timeshift_fd        = NaN;

parameters.segs_per_frame      = 2^3;         % integer
parameters.frame_duration      = 2^8;         % seconds
parameters.samprate            = 2^11;        % samples per second

parameters.n_fsr               = 2;

if parameters.n_fsr == 1,
    parameters.het                 = 37504;    % hertz
    parameters.fsr                 = 37520;    % hertz
    parameters.roi_halfwidth       = 200;      % hertz
elseif parameters.n_fsr == 2,
    parameters.het                 = 75008;    % hertz
    parameters.fsr                 = 74848;    % hertz
    parameters.roi_halfwidth       = 334.66;   % hertz
else
    error('Unknown FSR');
end

parameters.cross_normalization = 1;           % unitless

parameters.alpha               = 0.05;        % unitless

parameters.keep_hires          = 0;           % boolean
parameters.run                 = 'S4';        % string

parameters.intnode.delete_input= 1;
% Interferometer parameters

H1.name       = 'H1';
H1.armlength  = 3995.064;          % meters
H1.phi        =  -52.6  * pi/180;  % radians
H1.theta      =    9.00 * pi/180;  % radians
H1.n3         =    1.57;
H1.n4         =    0.856;
H1.cavpole    =   85.6;            % hertz
H1.C          =    2.43e21;        % counts/strain

H1.typical_variance = 11.85;
H1.typical_mean     =  0;
H2.typical_variance =  7.54;
H2.typical_mean     =  0;

H2.name       = 'H2';
H2.armlength  = 2009.12;           % meters
H2.phi        = -172    * pi/180;  % radians
H2.theta      =   23    * pi/180;  % radians
H2.n3         =    1.57;
H2.n4         =    0.856;
H2.cavpole    =  158.5;            % hertz
H2.C          =    1.08e21;        % counts/strain

L1.name       = 'L1';
L1.armlength  = NaN;               % meters
L1.phi        =  -71    * pi/180;  % radians
L1.theta      =  -64    * pi/180;  % radians
L1.n3         = 0.88;
L1.n4         = 1.12;
L1.cavpole    = NaN;
L1.C          = NaN;

parameters.H1 = H1;
parameters.H2 = H2;
parameters.L1 = L1;

parameters.badFrames = {};
