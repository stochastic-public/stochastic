This is a whirlwind tour of the structure of this code, which should help
the reader figure out where to start reading it.

Our code runs in a huge DAG that has two overlapping structures.  Our main
interest is in applying a filter to each frame to generate a statistic, and
then to combine all of these statistics into a final result.  This task is
generally "embarassingly parallel" until the final gathering of the statistics:


    frame1.gwf            frame2.gwf        Frame files
         |                     |
         V                     V
  +--------------+      +--------------+
  | Leafnode     |      | Leafnode     |    Compute FFT, etc
  +--------------+      +--------------+
         |                     |
         V                     V
  +--------------+      +--------------+
  | Rootnode     |      | Rootnode     |    Apply normalizations
  +--------------+      +--------------+
         |                     |
         V                     V
  +--------------+      +--------------+
  | Analysis_opt |      | Analysis_opt |    Apply filters to get statistic
  +--------------+      +--------------+
         |                       |
         |   /-------------------/          All statistics are gathered
         V   V
  +--------------+
  | MkFigures_opt|      Combines all statistics into final result and figures
  +--------------+

  The other thing we want to do is to compute average spectra over the entire
  run.  We do this using a binary tree in which spectra are successively
  combined:

   frame1.gwf      frame2.gwf       frame3.gwf      frame4.gwf
       |               |                |              |
       V               V                V              V
  +----------+    +----------+     +----------+    +----------+
  | Leafnode |    | Leafnode |     | Leafnode |    | Leafnode |
  +----------+    +----------+     +----------+    +----------+
       |                |               |               |
       +------+   +-----+               +------+  +-----+
              |   |                            |  |
              V   V                            V  V
           +----------+                    +----------+
           |  Intnode |                    |  Intnode |
           +----------+                    +----------+
                 |                               |
                 +------------+    +-------------+
                              |    |
                              V    V
                           +----------+
                           |  Intnode |
                           +----------+
                                 |
                                 V
                           +----------+
                           | Rootnode |
                           +----------+
                                 |
                                 V
                           +-----------+
                           | MkFigures |
                           +-----------+



