%% Load the data
vars = load('~/results/2fsr/rootnode.mat');

%% Produce standard variables
parameters = physparms();

n_frames     = length(vars.frameList);
n_segments   = n_frames * parameters.segs_per_frame;
seg_duration = parameters.frame_duration / parameters.segs_per_frame;
seg_samples  = seg_duration * parameters.samprate;

f  = linspace(-parameters.samprate/2, parameters.samprate/2, seg_samples);
f  = f + parameters.het;
x  = find(abs(f - parameters.fsr) <= parameters.roi_halfwidth);

%% Compute instrument response
resp1   = HL(parameters.H1.cavpole, f, parameters.H1.armlength);
resp2   = HL(parameters.H2.cavpole, f, parameters.H2.armlength);
resp_cc = conj(resp1) .* resp2; 

%% Make our optimal filter
Q = (vars.psd1 .* vars.psd2).^-1;
R = abs(resp_cc').^2;
Q = Q .* R;

Q(find(abs(f - parameters.fsr) > parameters.roi_halfwidth)) = 0;

%% Plot the filter over a wide frequency range

x  = find(abs(f - mean(f)) <= 900);
R = R/sum(R(x));
Q = Q/sum(Q(x));
plot(f(x),Q(x),'.',f(x),R(x),'-')
legend('Response^2/P_1P_2','Response^2','Location','Best');

%% Plot the filter over the region of interest

x  = find(abs(f - parameters.fsr) <= parameters.roi_halfwidth);
R = R/sum(R(x));
Q = Q/sum(Q(x));
plot(f(x),Q(x),'.-',f(x),R(x),'-')
legend('Response^2/P_1P_2','Response^2','Location','Best');

save('filter-2fsr.mat','Q');
