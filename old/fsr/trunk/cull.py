#!/usr/bin/python
# $Id: cull.py,v 1.1 2006-10-18 16:40:25 tobin Exp $

# Cull takes a list of frames and a list of time spans of good data and
# computes their intersection, outputting the list of frames that each
# lie totally within a duration of good data.

import re

# I assume that the list of good segments is (1) sorted, and (2) disjoint

def is_inside(seg1, seg2):
    return (seg1[0] >= seg2[0] and seg1[1] <= seg2[1])

def is_in_one_of(seg1, seglist):
    for seg2 in seglist:
        if is_inside(seg1, seg2):
            return True
    return False

f = file('data/framelist.txt', 'r')
g = file('data/selectedsegs.txt', 'r')

splitter = re.compile('\s+')


framefiles = {}
for line in f:
    filename = line.replace('\n','')
    # Note: filename parsing should be made more robust
    start = int(filename[(len(filename)-17):(len(filename)-8)]) 
    end = start + 256
    framefiles[(start, end)] = filename


good_segs = []
for line in g:
    if not line.startswith("#"):
        line = splitter.split(line)
        good_segs.append((int(line[2]),int(line[3])))


good_ranges = filter(lambda item: is_in_one_of(item, good_segs), framefiles.keys())

good_files = [framefiles[x] for x in good_ranges]

for file in good_files:
    print("%s " % file)

