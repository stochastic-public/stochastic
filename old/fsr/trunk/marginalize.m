function [varargout] = marginalize(out_path, varargin)
% marginalize('','1fsr/analysis_opt.csv',10^(-44),1000,0);
% Stefanos Giampanis

% NB! This program is currently intended to be run from within Matlab

% Check that the correct number of command line arguments are given
if (nargin ~= 5),
    error('Usage: marginalize(outpath, input file, prior UL, number of points, doMargCal)');
end

% Convert command line arguments to numbers
for k=2:4,
    if (ischar(varargin{k})),
        varargin{k} = str2num(varargin{k});
    end
end

infile    = varargin{1};  % data for 1 or 2 FSR
priorMax  = varargin{2};  % maximum value of prior PDF (eg. e-44 for 1fsr, e-45 for 2fsr);
n         = varargin{3};  % number of points for prior PDF (flat)
doMargCal = varargin{4};  % boolean (0/1) parameters for marginalization over calibration uncertainty

%% Read data
data = dlmread(infile);

%% Sort the data by gpstime
data = sortrows(data, 5);

%% Pull out the fields of interest
rstat   = data(:,2);
istat   = data(:,3);
sigma   = data(:,4);
gpstime = data(:,5);
alpha1  = data(:,6);
alpha2  = data(:,7);
ps1     = data(:,8);
ps2     = data(:,9);
days = (gpstime - min(gpstime)) / (60 * 60 * 24);

%% Confidence level
confidence = 0.9;

%% Weighted average of Statistics
fprintf('Computing weighted averages:\n');
weighted_mean = @(x, w) (x' * w)/sum(w);
rstat_wmean = weighted_mean(rstat, sigma.^(-2))
istat_wmean = weighted_mean(istat, sigma.^(-2))

sigma_error = sum(sigma.^(-2))^(-1/2)

fprintf('frequentist 90%% upper limit from weighted Statistic:\n')
if rstat_wmean < 0
    UL_f = 1.645 * sigma_error % 90% frequentist upper limit
    else
        UL_f = rstat_wmean + 1.645 * sigma_error
end;

%% Marginalization over angle for final weighted statistic
r0   = linspace(0,priorMax,n); % prior PDF (flat)

y_t  = sqrt(rstat_wmean^2 + istat_wmean^2); % magnitude of weighted Statistic
y_t  = repmat(y_t,1,n);
sigma2 = repmat(sigma_error.^2,1,n);
p_y    = exp(- (y_t.^2 + r0.^2) ./ (2*(sigma2))) .* besseli(0,y_t.*r0./sigma2); % posterior PDF
likelihood_f = p_y / sum(p_y/n); % total likelihood normalized to integrate to 1
fprintf('bayesian 90%% upper limit from weighted Statistic:\n')
UL_f = r0(1,max(find(cumsum(likelihood_f/n) < confidence)))

%% Marginalization over calibration uncertainty
% calibration range, number of points = n/10
if doMargCal
calib_min = 1-0.31;
calib_max = 1+0.37;
calib  = linspace(calib_min, calib_max, n/10);
calib  = repmat(calib',1,n);
y_t    = repmat(y_t,n/10,1);
r0     = repmat(r0,n/10,1);
sigma2 = repmat(sigma2,n/10,1);
p_y_c = (1./calib) .* exp(- ((y_t .* calib).^2 + r0.^2) ./ (2*(sigma2 .* calib.^2))) .* besseli(0,y_t.*r0./(sigma2 .* calib)); % posterior PDF
p_y_c = sum(p_y_c,1);
likelihood_f_c = p_y_c / sum(p_y_c/n);
fprintf('bayesian 90%% upper limit from weighted Statistic after calibration uncertainty marginalization:\n')
UL_f_c = r0(1,max(find(cumsum(likelihood_f_c/n) < confidence)))
end;

%% Marginalization over angle per Statistic
r0 = linspace(0,priorMax,n); % prior PDF (flat)
y  = sqrt(rstat.^2 + istat.^2); % magnitude of j-th Statistic
y  = repmat(y,1,n);

sigma = repmat(sigma,1,n);
r0    = repmat(r0,length(rstat),1);

posterior = exp(- (y.^2 + r0.^2) ./ (2*(sigma.^2))) .* besseli(0,y.*r0./sigma.^2); % posterior PDFs

w_m = mean(log(posterior),2);
w   = log(posterior) - repmat(w_m,1,n);  % logarithm of posterior PDFs

w_t = sum(w,1);
w_t = w_t - max(w_t);         % total logarithm of posterior PDF

likelihood_b = exp(w_t) / sum(exp(w_t)/n); % total likelihood normalized to integrate to 1 

fprintf('bayesian 90%% upper limit from combined (angle-marginalized) Statistics:\n')
UL_b = r0(1,max(find(cumsum(likelihood_b/n) < confidence)))

plot(r0(1,:),likelihood_f,'b',r0(1,:),likelihood_b,'r');grid;
legend('Total likelihood after overall angle-marginalization','Total likelihood after per-frame angle-marginalization');

%% Marginalization over calibration uncertainty
if doMargCal
r0     = linspace(0,priorMax,n);
r0     = repmat(r0,n/10,1);
myfun = @(x,xdata)(x(3)*exp(-((x(1)-xdata).^2)/(2*x(2)^2))); % gaussian used for fitting
xx = linspace(0,1,1000);                                     % x-axis space (r0 prior scaled by 1/priorMax)
fprintf('Marginalization over Calibration uncertainty for combined Statistic (likelihood) \n Fit of total likelihood to a gaussian:\n')
x = lsqcurvefit(myfun,[0;0.2;23],xx,likelihood_b);
A  = x(3);
s  = x(2)*priorMax;
x0 = x(1)*priorMax;
sprintf('from fit, A= %g, x0 = %g, sigma = %g',A,x0,s)
A  = repmat(A,n/10,1000);
x0 = repmat(x0,n/10,1000);
s  = repmat(s,n/10,1000);
xx = repmat(xx*priorMax,n/10,1);
pos_c    = A .* exp(-(x0.*calib-xx).^2 ./ (2*(s.*calib).^2)); % posterior PDF
pos_c    = (1./calib) .* pos_c;  
likelihood_b_c = sum(pos_c,1);                                 % marginalized likelihood
likelihood_b_c = likelihood_b_c / sum(likelihood_b_c/n);       % normalized likelihood
fprintf('bayesian 90%% upper limit from combined (angle-marginalized) Statistics after calibration uncertainty marginalization:\n')
UL_b_c = r0(1,max(find(cumsum(likelihood_b_c/n) < confidence)))
end;
%%
results.UL_f           = UL_f;
results.UL_b           = UL_b;
results.likelihood_f   = likelihood_f;
results.likelihood_b   = likelihood_b;
results.r0             = r0(1,:);
results.stats          = length(rstat);
results.confidence     = confidence*100;
if doMargCal
            results.A  = x(3);
            results.x0 = x(1)*priorMax;
            results.s  = x(2)*priorMax;
results.likelihood_f_c = likelihood_f_c;
results.likelihood_b_c = likelihood_b_c;
        results.UL_f_c = UL_f_c;
        results.UL_b_c = UL_b_c;
end;

if nargout == 1,
    varargout{1} = results;
end

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
likelihood_f   = results.likelihood_f;
likelihood_b   = results.likelihood_b;
likelihood_f_c = results.likelihood_f_c;
likelihood_b_c = results.likelihood_b_c;
r0             = results.r0;
stats          = results.stats;
UL_f           = results.UL_f;
UL_b           = results.UL_b;
confidence     = results.confidence;
UL_f_c         = results.UL_f_c;
UL_b_c         = results.UL_b_c;

% Plots
figure;
plot(r0,likelihood_f,'b',r0,likelihood_b,'r');grid;
title(sprintf('%d %% UL from final weighted Statistic = %g, \n  %d %% UL from combined Statistic = %g',confidence,UL_f,confidence,UL_b));
legend(sprintf('Likelihood of final weighted Statistic \n (after marginalization over angle)'),...
    sprintf('Likelihood of combined (%d Statistics) Statistic \n (after marginalization over angle)',stats));
xlim([0 0.15*10^(-44)]);

figure;
plot(r0,likelihood_f_c,'b',r0,likelihood_b_c,'r');grid;
title(sprintf('%d %% UL from final weighted Statistic = %g, \n  %d %% UL from combined Statistic = %g',confidence,UL_f_c,confidence,UL_b_c));
legend(sprintf('Likelihood of final weighted Statistic \n (after marginalization over angle and over calibration uncertainty)'),...
    sprintf('Likelihood of combined (%d Statistics) Statistic \n (after marginalization over angle and calibration uncertainty)',...
    stats));
xlim([0 0.15*10^(-44)]);



