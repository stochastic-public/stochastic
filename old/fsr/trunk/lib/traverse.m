function result = traverse(root, pattern, predicate, action)
% function traverse(root, pattern, predicate, action)
%
% Do a depth-first search rooted at 'root' for files whose name matches
% 'pattern' and for whom additionally 'predicate' returns true.  Then
% execute 'action' on the file.  Maybe.
%
% Tobin Fricke     2005-11-19   Boston, MA

result = {};

% define a functional-programming list filtering function
filter = @(list, predicate) list(find(map(predicate, list)));

%% FIND THE FILES IN THIS PARTICULAR DIRECTORY

% find all files matching the filename pattern
files = dir([root filesep pattern]);

% expand the filename field to include the complete directory name
for f = 1:length(files),
  files(f).name = [root filesep files(f).name];
end

% apply the supplied predicate to determine which are eligible files
files = files(find(map(predicate, files)));

% Process the eligible files in this directory
for f = 1:length(files),
  result{length(result)+1} = action(files(f));
end

%% FIND THE SUBDIRECTORIES INTO WHICH WE WILL RECURSE
files = dir(root);

subdirs = files(find(horzcat(files.isdir)));

% Remove '.' and '..' from the list of subdirectories to be processed
subdirs = filter(subdirs, @(file) ~(strcmp(file.name, '.')  || strcmp(file.name, '..')));

% Call ourself recursively on any subdirectories other than '.' and '..'
for d = 1:length(subdirs),
  result = cat(2, result, ...
      traverse([root filesep subdirs(d).name], pattern, predicate, action));
end
