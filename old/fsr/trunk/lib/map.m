function results = map(f, list)
results = [];
for i=1:length(list)
 results = [results f(list(i))];
end
