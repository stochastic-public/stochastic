function m = hist2d(xdata, ydata, nbins)

if (length(xdata) ~= length(ydata)),
    error('xdata and ydata must have the same size.');
end

npoints = length(xdata);
%xedges = linspace(min(xdata),max(xdata),nbins+1);
%yedges = linspace(min(ydata),max(ydata),nbins+1);

xrange = max(xdata)-min(xdata);
yrange = max(ydata)-min(ydata);

xbins = round((nbins-1)*(xdata - min(xdata))/xrange)+1;
ybins = round((nbins-1)*(ydata - min(ydata))/yrange)+1;

m = full(sparse(xbins,ybins,ones(npoints,1)));