function plot_circle(center, radius, varargin)
angles = linspace(0,2*pi,1000);
if length(radius)==1,
    radius = [radius radius];
end
if length(center)==1 & ~isreal(center),
    center = [real(center) imag(center)];
end
if nargin==3,
    plotspec = varargin{1};
else
    plotspec = '';
end
plot(radius(1)*cos(angles) + center(1),radius(2)*sin(angles) + center(2),plotspec);
