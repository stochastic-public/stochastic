%%
parameters = physparms();
parameters.real_data = 0;
strainsq_per_countsq = 1/(parameters.H1.C * parameters.H2.C);


injection_amplitude = 10^-47;
n_injections = 10000;

parameters.injection_fd = injection_amplitude / strainsq_per_countsq;

stats =  [];
sigmas = [];

while(1), 
    results = leafnode(parameters,'DUMMY');
    stats   = [stats  results.analysis_opt.stat];
    sigmas  = [sigmas results.analysis_opt.sigma];
   
    stat  = sum(stats .* sigmas.^-2)/sum(sigmas.^-2);
    sigma = sqrt(1/sum(sigmas.^-2));
    
    subplot(1,2,1);
    plot(stats, '.','MarkerSize',1); 
    title(sprintf('%d statistics', length(stats)));
    hold all;
    grid on;
    plot_circle(stat, 1.645 * sigma, '-');
    plot_circle(stat, 1.960 * sigma, '--');
    axis equal;
    line([1 1]*injection_amplitude, get(gca,'YLim'), 'Color',[0 0 0]);
    line([0 0], get(gca,'YLim'), 'Color',[0.5 0.5 0.5]);
    line(get(gca,'XLim'), [0 0], 'Color',[0.5 0.5 0.5]);
    hold off;
    drawnow;
    
    subplot(2,2,2); histfit(real(stats));
    subplot(2,2,4); histfit(imag(stats));
end
