function [f, y_tilde] = stamp_fft(t,y)
% function [f, y_tilde] = stamp_fft(t,y)

T=t(2)-t(1); % sample time
Fs=1/T;
dur = t(end)-t(1);
L = Fs*dur;
NFFT = 2^nextpow2(L);  % calculate nearest power of 2
f = Fs/2*linspace(0, 1, NFFT/2+1)';
y_tilde = fft(y, NFFT)*sqrt(1/L);

return
