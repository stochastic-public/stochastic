function reconstructRadon2(params, map)
% function reconstructRadon2(params, map)
% E. Thrane
% uses radon to find hotspot
% use iradon to reconstruct the best fit track

  % parameterize search angles
  theta = 1:180;

  % square map
  x = size(map.y,1);
  y = size(map.y,2);
  if x>y
    map.y = [map.y zeros(x, x-y) ];
    map.sigma = [map.sigma ones(x, x-y) ];
    map.snr = [map.snr zeros(x, x-y) ];
    map.F = [map.F zeros(x, x-y) ];
    map.sigF = [map.sigF ones(x, x-y) ];
  elseif x<y
    map.y = [map.y ; zeros(y-x, y)];
    map.sigma = [map.sigma ; ones(y-x, y)];
    map.snr = [map.snr ; zeros(y-x, y)];
    map.F = [map.F ; ones(y-x, y)];
    map.sigF = [map.sigF ; zeros(y-x, y)];
  end

  % calculate y_Gamma and sigma_Gamma for every point in Radon space
  if params.fluence
    [tmp1, b] = radon(map.F .* map.sigF.^-2, theta);
    [tmp2, b] = radon(map.sigF.^-2, theta);
    rmap.F = tmp1./tmp2;
    rmap.sigF = tmp2.^-0.5;
    rmap.snr = rmap.F ./ rmap.sigF;
  else 
    [tmp1, b] = radon(map.y .* map.sigma.^-2, theta);
    [tmp2, b] = radon(map.sigma.^-2, theta);
    rmap.y = tmp1./tmp2;
    rmap.sigma = tmp2.^-0.5;
    rmap.snr = rmap.y ./ rmap.sigma;
  end

  rmap.xvals = theta;
  rmap.yvals = b; % impact parameter

  % max Radon SNR
  rmax = max(max(rmap.snr));

  rmap.fit = zeros(size(rmap.snr));
  rmap.fit(rmap.snr==rmax) = rmax;

  % Use iradon to invert.
  map.fit = iradon(rmap.fit, theta);

  % Undo squaring.
  if x>y
    map.fit = map.fit(:,1:y);
    map.snr = map.snr(:,1:y);
  elseif x<y
    map.fit = map.fit(1:x,:);
    map.snr = map.snr(1:x,:);
  end

  % Always save a plot if reconstruction is requested
  printmap(map.fit, map.xvals, map.yvals, 't (s)', 'f (Hz)', ...
    '', [min(min(map.fit)) max(max(map.fit))]);
  print('-dpng','radon_fit.png');
  print('-depsc2','radon_fit.eps');

return
