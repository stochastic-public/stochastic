function DQ_S5_make_mat

% This function creates a mat file containing lists of vetoed segments during S5for both LIGO and Virgo

% S5 H1, H2, and L1 flags were taken from here:
% /home/mabizoua/review_hm/dq/

% VSR1 flags were taken from here:
% https://wwwcascina.virgo.infn.it/DataAnalysis/VDBdoc/BURST/VSR1_DQ_BURST_LIST_20080726.html

DQ = [1 2 3 4];

for i = 1:length(DQ)

   H1_current_veto_file = load(['Segments/S5-H1_cat' num2str(DQ(i)) '.txt']);
   [H1_number_of_flags,H1_number_of_columns] = size(H1_current_veto_file);

   if H1_number_of_columns==2
      H1(i).start_time = H1_current_veto_file(:,1);
      H1(i).end_time = H1_current_veto_file(:,2);
   elseif H1_number_of_columns==4
      H1(i).start_time = H1_current_veto_file(:,2);
      H1(i).end_time = H1_current_veto_file(:,3);
   else
      error('Please provide CAT files with either 2 or 4 columns');
   end

   H2_current_veto_file = load(['Segments/S5-H2_cat' num2str(DQ(i)) '.txt']);
   [H2_number_of_flags,H2_number_of_columns] = size(H2_current_veto_file);

   if H2_number_of_columns==2
      H2(i).start_time = H2_current_veto_file(:,1);
      H2(i).end_time = H2_current_veto_file(:,2);
   elseif H2_number_of_columns==4
      H2(i).start_time = H2_current_veto_file(:,2);
      H2(i).end_time = H2_current_veto_file(:,3);
   else
      error('Please provide CAT files with either 2 or 4 columns');
   end

   L1_current_veto_file = load(['Segments/S5-L1_cat' num2str(DQ(i)) '.txt']);
   [L1_number_of_flags,L1_number_of_columns] = size(L1_current_veto_file);

   if L1_number_of_columns==2
      L1(i).start_time = L1_current_veto_file(:,1);
      L1(i).end_time = L1_current_veto_file(:,2);
   elseif L1_number_of_columns==4
      L1(i).start_time = L1_current_veto_file(:,2);
      L1(i).end_time = L1_current_veto_file(:,3);
   else
      error('Please provide CAT files with either 2 or 4 columns');
   end

   V1_current_veto_file = load(['Segments/S5-V1_cat' num2str(DQ(i)) '.txt']);
   [V1_number_of_flags,V1_number_of_columns] = size(V1_current_veto_file);

   if V1_number_of_columns==2
      V1(i).start_time = V1_current_veto_file(:,1);
      V1(i).end_time = V1_current_veto_file(:,2);
   elseif V1_number_of_columns==4
      V1(i).start_time = V1_current_veto_file(:,2);
      V1(i).end_time = V1_current_veto_file(:,3);
   else
      error('Please provide CAT files with either 2 or 4 columns');
   end


end

save('DQ_flags.mat','H1','H2','L1','V1');

return
