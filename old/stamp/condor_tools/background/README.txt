Instructions:

1.) Create a directory in which you want to run your background studies out of

2.) Edit the params in run_background_sing.m and run the file

3.) Copy your params files, jobfile, and condor dag and sub file to the
directory created (of the form Background_*_*)

4.) Edit the .sub file such that it points to your compiled version of preproc
and a log directory on your account

5.) condor_submit_dag -maxjobs 50 your_pipe_file.dag
