function run_background

randn('state',0);


params.doReal=1;
params.doMC=1;

params.Real_MC=[params.doReal params.doMC];
params.Real_MC_name={'Real','MC'};
params.segmentDurVals=1;
params.glitchCutVals=[0 2];

for i=1:length(params.Real_MC)
 if params.Real_MC(i)==1
   for j=1:length(params.glitchCutVals)
    for k=1:length(params.segmentDurVals)

      data_type=params.Real_MC_name{i};
      glitchCut=params.glitchCutVals(j);
      segmentDur=params.segmentDurVals(k);

      mkdir_command=['mkdir Background_' data_type '_' num2str(glitchCut) ... 
         '_' num2str(segmentDur)];
      system(mkdir_command);

      cp_command=['cp -r /archive/home/mcoughlin/IM/GRB/Document/ADI_Point5/Run_Background_ADI/General/* Background_' data_type '_' ... 
         num2str(glitchCut) '_' num2str(segmentDur)];
      system(cp_command);

      background_params(data_type,glitchCut,segmentDur);
    end
   end
 end
end


