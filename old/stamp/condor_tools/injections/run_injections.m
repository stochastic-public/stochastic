function run_injections(injection_size)

if isstr(injection_size)
   injection_size=str2num(injection_size);
else
   injection_size=injection_size;
end

randn('state',0);

if injection_size>0
  i=0;
  success_count=0;
  injection_dur=10; 

  mkdir_command=['mkdir results/' num2str(injection_size)];
  system(mkdir_command);

  full_job_file=load('S5H1L1_full_run.txt');
  gps_start_times=full_job_file(:,2);
  gps_end_times=full_job_file(:,3);
  gps_duration=full_job_file(:,4);

  fid=fopen(['results/' num2str(injection_size) '/snr_value.txt'],'w+');

  
  while success_count<10
   i=i+1;
   if gps_duration(i)<250
%   try
      inj_params(injection_size,gps_start_times(i),gps_end_times(i),injection_dur,0);
      inj_time=preproc_inj(['results/' num2str(injection_size) '/params.txt'],'S5H1L1_full_run.txt',i)
      inj_params(injection_size,gps_start_times(i),gps_end_times(i),injection_dur,inj_time);

      preproc_out=preproc(['results/' num2str(injection_size) '/params.txt'],'S5H1L1_full_run.txt',i);
      fprintf(fid,'%f %e\n',gps_start_times(i), preproc_out.max_SNR);
      success_count=success_count+1;

%   catch
%   end
   end
  end
  fclose(fid);
end

