function map = freshmap(map)
% function map = freshmap(map)
% E. Thrane - reset some of the ft-maps.  Raw data (such as cross and auto
% power are not reset.  Only reset STAMP estimators are reset.
  map.y = [];
  map.z = [];
  map.sigma = [];
  map.eps_12 = [];
  map.eps_11 = [];
  map.eps_22 = [];
  map.F = [];
  map.sigF = [];
  map.snr = [];
  map.snrz = [];

return;
