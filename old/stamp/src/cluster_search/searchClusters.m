function max_cluster = searchClusters(params, map)
% function max_cluster = searchClusters(params, map)
% clusterstamp Apply density based clustering algorithm developed by R.
% Khan (Class. Quantum Grav. 26 155009)
% INPUT :
%     params.cluster.NN              <- minimum number of pixels in each cluster
%     params.cluster.NR              <- neighbour radius as determined by metric
%     params.cluster.NCR             <- distance between neighbouring clusters
%     params.cluster.pixelThreshold  <- threshold for pixels to be included in 
%                                       the cluster
%     parmas.cluster.tmetric         <- time metric of ft-map
%     parmas.cluster.fmetric         <- frequency metric of ft-map
%     map.xvals                      <- input GPS times array 
%     map.yvals                      <- input frequencies array 
%     map.snr                        <- snr map
%     map.Y                          <- Y map
%     map.sigma                      <- sigma map
%
% OUTPUT : (in the max_cluster struct)
%     Details of the largest cluster
%       -> Y_gamma
%       -> sigma_gamma
%       -> snr_gamma
%       -> varargout{1} = no. of pixels in that cluster
%       
% Routine written by S. kandhasamy (based on burstcluster.m by Peter Kalmus)
% Contact shivaraj@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if params.crunch_map
  warning('crunch_map is on.  <------------------------');
  map = crunch_map(map);
end

% Renaming variables (to short names)
NN = params.cluster.NN;
NR = params.cluster.NR;
NCN = params.cluster.NCN;
NCR = params.cluster.NCR;
pixelThreshold = params.cluster.pixelThreshold;
tmetric = params.cluster.tmetric;
fmetric = params.cluster.fmetric;
t = map.xvals;
f = map.yvals;

%%%%%%%%%%%%%%%%%% clustering starts here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% find all the pixels with snr > pixelThreshold
[rows, cols] = find(map.snr > pixelThreshold);

if(isempty(rows))
  fprintf('No significant pixel, with snr > %.2f, in the ft-map \n',...
           pixelThreshold);
  max_Y_gamma = 0;
  max_sigma_gamma = Inf;
  max_snr_gamma = 0;
  varargout{1} = 0; % max_no_pixels
  return;
end

fprintf('num pixels: %d out of %d with snr > %.2f;  percentage: %1.2f \n', ...
         length(rows), numel(map.snr), pixelThreshold,...
         length(rows)/numel(map.snr)*100);

for ctr=1:length(rows)
  pixels.time(ctr) = t(cols(ctr));
  pixels.frequency(ctr) = f(rows(ctr));
  pixels.snr(ctr) = map.snr(rows(ctr), cols(ctr));
  pixels.y(ctr) = map.y(rows(ctr), cols(ctr));
  pixels.sigma(ctr) = map.sigma(rows(ctr), cols(ctr));
end  

% measure distances
tiles = [pixels.time' pixels.frequency' pixels.snr' pixels.y' pixels.sigma'];

% cluster pixels
seedpoint = clusterPixels(tiles, tmetric, fmetric, NR, NN);

% sort clusters
tempClusters = 0;
for i = 1:size(tiles,1)
    tempClusters(i) = seedpoint{i}.ID;
end
uniqueClusters = sort(unique(tempClusters(tempClusters > 0)));
clusters.numberOfClusters = length(uniqueClusters);

%%%%%%%%%%%%%%%%%%% Reporting clusters and further processing %%%%%%%%%%%%%%%%%
fprintf('Total number of initial clusters: %i \n', clusters.numberOfClusters);

if clusters.numberOfClusters > 0
  % details of all clusters
  for clusterIndex = 1:clusters.numberOfClusters
    % Read time, frequency,and SNR info of each tile
    i = find(tempClusters == uniqueClusters(clusterIndex));
    t = pixels.time(i);
    f = pixels.frequency(i);
    z = pixels.snr(i);

    % Tile indices, total number of tiles in each cluster 
    clusters.indices{clusterIndex} = i;
    clusters.n{clusterIndex} = length(i);
  end
 
  % identifying big clusters
  bigClusters = [];
  for clusterIndex = 1 : length(uniqueClusters)
    indices = find(tempClusters == uniqueClusters(clusterIndex));
    if(length(indices)>= NCN) % requirement on no.of pixels in cluster
      bigClusters = [bigClusters uniqueClusters(clusterIndex)];
    end
  end
  fprintf('Total number of big clusters: %i \n', length(bigClusters));

  % display clustering
  if params.savePlots %-----------------------------------------------
    figure
    axis([min(map.xvals) max(map.xvals) min(map.yvals) max(map.yvals)])
    xlabel('time [sec]');
    ylabel('Frequency [Hz]');
    hold on;

    % it should cycle through markers instead of relying
    % on a huge vector.  use mod...
    markers = {'ro', 'go', 'bo', 'co', 'mo', 'ko', ...
         'rs', 'gs', 'bs', 'cs', 'ms', 'ks', ...
         'rd', 'gd', 'bd', 'cd', 'md', 'kd', ...
         'rv', 'gv', 'bv', 'cv', 'mv', 'kv', ...
         'r^', 'g^', 'b^', 'c^', 'm^', 'k^', ...
         'r<', 'g<', 'b<', 'c<', 'm<', 'k<', ...
         'r>', 'g>', 'b>', 'c>', 'm>', 'k>', ...
         'rp', 'gp', 'bp', 'cp', 'mp', 'kp', ...
         'rh', 'gh', 'bh', 'ch', 'mh', 'kh'};

%eht    for clusterIndex = 1 : length(uniqueClusters)
    for clusterIndex = 1 : length(bigClusters)
%eht      indices = find(tempClusters == uniqueClusters(clusterIndex));
      indices = find(tempClusters == bigClusters(clusterIndex));
      handle = plot(tiles(indices, 1), tiles(indices, 2), ...
               markers{mod(clusterIndex, length(markers))+1 } );
      set(handle, 'MarkerSize', 10);
      set(handle, 'MarkerFaceColor', get(handle, 'Color'));
    end
    title(['All clusters, number of clusters = ' ...
           num2str(length(bigClusters))], 'FontSize', 20)
    hold off;
    pretty;
    print -depsc all_clusters_plot.eps;
    print -dpng all_clusters_plot.png;
  end %-------if params.savePlots--------------------------------------

  % clustering the (big) clusters
  if length(bigClusters) > 0 
    if params.cluster.doCombineCluster 
      indices = [];
      for ii = 1:length(bigClusters)
        indices = [indices find(tempClusters == bigClusters(ii))];
      end
      select_tiles = tiles(indices,:);
      seedpointNew = clusterPixels(select_tiles, tmetric, fmetric, NCR, NN);   
      for i = 1:length(select_tiles(:,1))
        tempClustersNew(i) = seedpointNew{i}.ID;
      end
      uniqueClustersNew = sort(unique(tempClustersNew(tempClustersNew > 0)));
      clusters.numberOfClustersNew = length(uniqueClustersNew);
    end
  else
    clusters.numberOfClustersNew = 0;
  end

  if params.cluster.doCombineCluster & clusters.numberOfClustersNew
    bigClusters = uniqueClustersNew;
    tempClusters = tempClustersNew;
    tiles = select_tiles;
  end

  % calculate Y_Gamma, sigma_Gamma, SNR_Gamma of all (or big) the clusters
  if length(bigClusters) > 0
    for clusterIndex = 1 : length(bigClusters)
      indices = find(tempClusters == bigClusters(clusterIndex));
      Y_gamma(clusterIndex) = ...
         sum(tiles(indices,4)./tiles(indices,5).^2)/sum(1./tiles(indices,5).^2);
      sigma_gamma(clusterIndex) = 1/sqrt(sum(1./tiles(indices,5).^2));
      snr_gamma(clusterIndex) = Y_gamma(clusterIndex)/sigma_gamma(clusterIndex);
      no_pixels(clusterIndex) = length(indices);
    end
    
    % cluster with largest SNR
    [all_snr_gamma,index] = sort(snr_gamma,'descend');
    max_Y_gamma = Y_gamma(index(1));
    max_sigma_gamma = sigma_gamma(index(1));
    max_snr_gamma = snr_gamma(index(1));
    max_no_pixels = no_pixels(index(1));
    
    fprintf('The largest SNR is %.2f with %d number of pixel(s) \n',...
         max_snr_gamma, max_no_pixels);

    % calculate time/freq. range of recovery
    indices = find(tempClusters == bigClusters(index(1)));
    t_vals = tiles(indices,1);
    f_vals = tiles(indices,2);
    t_start = min(t_vals); t_end = max(t_vals);
    gps_start = t_start + map.segstarttime(1);
    gps_end = t_end + map.segstarttime(1);
    f_low = min(f_vals); f_high = max(f_vals);

    fprintf('  Map time range: %.2f - %.2f seconds\n',t_start,t_end);
    fprintf('  GPS time range: %.2f - %.2f seconds\n',gps_start,gps_end);
    fprintf('  Frequency range: %.2f - %.2f Hz\n',f_low,f_high);

    if params.savePlots %-----------------------------------------------
      temp_map = zeros(size(map.snr));
      for ii = 1:length(t_vals)
        cutt = map.xvals==t_vals(ii);
        cutf = map.yvals==f_vals(ii);
        temp_map(cutf,cutt) = map.snr(cutf,cutt);
      end
      printmap(temp_map,map.xvals+0.5,map.yvals+0.5,'time (s)','f (Hz)',...
              'SNR',[-5 5])
      print -depsc large_cluster_plot.eps;
      print -dpng large_cluster_plot.png;
    end
  else
    fprintf('No significant cluster \n');
    max_Y_gamma = 0;
    max_sigma_gamma = Inf;
    max_snr_gamma = 0;
    max_no_pixels = 0;
    t_start = []; t_end = [];
    gps_start = []; gps_end = [];
    f_low = []; f_high = [];
  end
end

% assign values to max_cluster struct
max_cluster.Y_gamma = max_Y_gamma;
max_cluster.sigma_gamma = max_sigma_gamma;
max_cluster.snr_gamma = max_snr_gamma;
max_cluster.NPix = max_no_pixels;
max_cluster.t_start = t_start;
max_cluster.t_end = t_end;
max_cluster.gps_start = gps_start;
max_cluster.gps_end = gps_end;
max_cluster.f_low = f_low;
max_cluster.f_high = f_high;

return;
