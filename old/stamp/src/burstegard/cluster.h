#ifndef CLUSTER_H
#define CLUSTER_H

#include <vector>
using namespace std;

/* PARAMS OBJECT
num_pix       - minimum number of pixels for a cluster to be formed.
rad           - maximum radius for two pixels to be clustered together.
snr_threshold - SNR threshold for a pixel to be identified as a seed.
x_metric      - size of "ellipse" for clustering in x direction.
y_metric      - size of "ellipse" for clustering in y direction. */

class Params {
 public:
  int num_pix;
  double rad;
  double snr_threshold;
  double x_metric;
  double y_metric;
  void set_values (int,double,double,double,double);
};



/* SET VALUES FOR A PARAMS OBJECT. */
inline void Params::set_values (int a, double b, double c, double d, double e) {
  num_pix = a;
  rad = b;
  snr_threshold = c;
  x_metric = d;
  y_metric = e;
}

/* SEED OBJECT
val   - SNR of seed pixel.
num   - seed ID number, counting seeds from left to right, top to bottom in input map.
x_loc - x location of seed in 2D map/array (column in matrix).
y_loc - y location of seed in 2D map/array (row in matrix). */
class Seed {
 public:
  double snr, y, sigma;
  int num, x_loc, y_loc;
};

/* CLUSTER OBJECT.
clusterIndex - cluster ID number.
seedlist     - vector containing the seed number (Seed.num) of all pixels included in the cluster.
nPix         - total number of pixels in the cluster.
snr_gamma    - total cluster SNR. */
class Cluster {
 public:
  int clusterIndex;
  vector<int> seedlist;
  int nPix;
  double snr_gamma, y_gamma, sigma_gamma;
};

#endif
