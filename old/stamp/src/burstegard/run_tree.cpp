#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cmath>
#include <string>
#include "cluster.h"

using namespace std;

// DECLARE FUNCTIONS FROM OTHER .cpp FILES.

void tree(double* input_snr, double* input_y, double* input_sigma, double* output_max, double* output_all, int &nPix, double &snr_gamma, const int size_x, const int size_y, int num_pix, double rad, double snr_threshold, double x_metric, double y_metric, bool save_all_clusters);

// START MAIN FUNCTION
int main (int argc, char *argv[]) {

  // SET UP PARAMS OBJECT.
  // ARGUMENTS FOR .set_values ARE AS FOLLOWS:
  // 1. MIN. # OF PIXELS FOR A CLUSTER.
  // 2. CLUSTERING RADIUS.
  // 3. SNR THRESHOLD.
  // 4. X METRIC.
  // 5. Y METRIC.
  //////////////////////////////////////////
  Params params;
  //params.set_values(80,4/2.4,1.5,1/2.4*2.4,1/0.75*2.4,true); // SAVE
  params.set_values(80,2,0.75,1,1,true); // SAVE
  //params.set_values(80,4,0.75,1/2,1,true); // ERIC
  //params.set_values(10,4,1.5,1/2.4,1/0.75,true); // 1s
  //params.set_values(28,2,0.75,1/1.5,1/0.65,true); // 4s
  //params.set_values(10,3,1,1,1,true); // other

  cout << params.num_pix << " " << params.rad << " " << params.snr_threshold << " " << params.x_metric << " " << params.y_metric << endl;

  string in_file = argv[1];
  string out_file = argv[2];
  const int size_x = atoi(argv[3]);
  const int size_y = atoi(argv[4]);

  // DEFINE ARRAY SIZES.
  double input_2d[size_y][size_x];
  double* input_1d = new double[size_x*size_y];
  double* output_1d = new double[size_x*size_y];

  // GET INPUT MAP FROM FILE.
  int a = 0; int b = 0; // ROW/COLUMN INDICES.
  string temp;
  ifstream file_in;
  file_in.open(in_file.c_str());
  if (file_in.is_open()) {
    while (file_in >> temp) {
      input_2d[a][b] = atof(temp.c_str());
      //cout << temp << " " << input_2d[a][b] << " " << a << " " << b << endl;

      if (b == (size_x - 1)) { b = 0; a++; }
      else { b++; }     
    }
    file_in.close();
  } 
  else {
    cout << "Unable to open file.";
    return -1; // ERROR
  }

  // CONVERT 2D MAP TO 1D ARRAY.
  for (int i = 0; i < size_y; i++) {
    for (int j = 0; j < size_x; j++) {
      input_1d[i*size_x+j] = input_2d[i][j];
    }
  }

  int nPix;
  double snr_gamma;

  tree(input_1d,output_1d,nPix,snr_gamma,size_x,size_y,params.num_pix,params.rad,params.snr_threshold,params.x_metric,params.y_metric,params.save_all_clusters);

  cout << "\tNPix: " << nPix << endl;
  cout << "\tSNR: " << snr_gamma << endl;

  /*
  // WRITE OUTPUT ARRAY FOR TESTING.
  ofstream outfile;
  string filename = "test.dat";
  outfile.open(filename.c_str());

  if (outfile.is_open()) {
    for (int i = 0; i < y; i++) {
      for (int j = 0; j < x; j++) {
	//outfile << reconMap[i][j] << " ";
	outfile << output_1d[i*x+j] << " ";
      }
      outfile << endl;
    }
    outfile << endl;
    outfile.close();
  }
  else {
    cout << "Unable to open output file." << endl;
  }
  */

  delete[] input_1d;

  return 0;
}
