#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <vector>
#include <map>
#include "cluster.h"

using namespace std;

// PURPOSE:
// CALCULATE THE TOTAL SNR (SNR_GAMMA) OF ALL CLUSTERS.
// SET maxCluster TO BE THE CLUSTER WITH THE HIGHEST SNR_GAMMA.
//
// INPUT:
// clusters - vector of Cluster objects.
// seeds    - array of Seed objects.
// params   - Params object.

void analyze_clusters(vector<Cluster> &clusters, Seed* seeds, Params params, Cluster &maxCluster) {

  int max_index;
  double max_snr = 0;
  double yw2, w2; 

  // LOOP OVER CLUSTERS.
  for (int i = 0; i < clusters.size(); i++) {

    yw2 = 0; // y*weight^2 (weight = 1/sigma)
    w2 = 0; // weight^2
    // LOOP OVER SEED PIXELS IN A CLUSTER.
    for (int j = 0; j < clusters[i].nPix; j++) {
      //clusters[i].snr_gamma += seeds[clusters[i].seedlist[j]].snr;
      yw2 += (seeds[clusters[i].seedlist[j]].y)/pow((seeds[clusters[i].seedlist[j]].sigma),2);
      w2 += 1/pow((seeds[clusters[i].seedlist[j]].sigma),2);

    }
    clusters[i].y_gamma = yw2/w2;
    clusters[i].sigma_gamma = 1/sqrt(w2);
    clusters[i].snr_gamma = (clusters[i].y_gamma)/(clusters[i].sigma_gamma);

    if (clusters[i].snr_gamma > max_snr) {
      max_index = i;
      max_snr = clusters[i].snr_gamma;
    }

  }
  
  // SET maxCluster TO BE TO CLUSTER WITH THE HIGHEST SNR_GAMMA.
  maxCluster = clusters[max_index];
  maxCluster.clusterIndex = max_index;

}
