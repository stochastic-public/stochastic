#include <iostream>
#include <cmath>
#include <stdlib.h>
#include "cluster.h"
#include <vector>

using namespace std;

// PURPOSE:
// CALCULATE THE DISTANCE BETWEEN TWO SEED PIXELS.
// DOES THIS USING TWO Seed OBJECTS.
//
// INPUT:
// seed1 - first Seed object.
// seed2 - second Seed object.
// params - Params object.

double calc_dists(Seed seed1, Seed seed2, Params params) {

  // CALCULATE DISTANCES.
  // PARAMS.X_METRIC AND PARAMS.Y_METRIC SPECIFY THE "ELLIPSE" SIZE.
  // EX: PARAMS.X_METRIC = 2 AND PARAMS.Y_METRIC = 1 WOULD MEAN THAT
  // WE WOULD USE AN ELLIPSE (1/4)X^2 + Y^2 <= (PARAMS.RAD)^2 TO CLUSTER.
  double x_dist = (seed1.x_loc - seed2.x_loc)*(1/params.x_metric);
  double y_dist = (seed1.y_loc - seed2.y_loc)*(1/params.y_metric);
  double tot_dist = sqrt( pow(abs(x_dist),2) + pow(abs(y_dist),2) );


  if (-y_dist > params.rad) { return -1; }
  else { return tot_dist; }

}


