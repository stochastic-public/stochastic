function [rmax, bmax, tmax, p, Y_Gamma_max] = searchRadon(params, map)
% function [rmax, bmax, tmax, p] = searchRadon(params, map)
% E. Thrane

  % keep a non-square copy for reconstruction
  map0=map;

  % square off map for Radon search
  x = size(map.y,1);
  y = size(map.y,2);
  map.y=square_map(map.y,0);
  map.sigma=square_map(map.sigma,1);
  map.F=square_map(map.F,0);
  map.sigF=square_map(map.sigF,1e10);

  % parameterize search angles
  theta = 1:180;

  % calculate y_Gamma and sigma_Gamma for every point in Radon space
  if params.fluence
    [tmp1, b] = mylilradon(map.F .* map.sigF.^-2, theta, 1);
    [tmp2, b] = mylilradon(map.sigF.^-2, theta, 2);
    [tmp3, b] = mylilradon(map.sigF.^-2, theta, 1);
    rmap.F = tmp1./tmp3;
    rmap.sigF = (tmp2./tmp3.^2).^0.5;
    rmap.snr = rmap.F ./ rmap.sigF;
  else
    [tmp1, b] = mylilradon(map.y .* map.sigma.^-2, theta, 1);
    [tmp2, b] = mylilradon(map.sigma.^-2, theta, 2);
    [tmp3, b] = mylilradon(map.sigma.^-2, theta, 1);
    rmap.y = tmp1./tmp3;
    rmap.sigma = (tmp2./tmp3.^2).^0.5;
    rmap.snr = rmap.y ./ rmap.sigma;
  end

  rmap.xvals = theta;
  rmap.yvals = b; % impact parameter

  if params.savePlots
    printmap(rmap.snr, rmap.xvals, rmap.yvals, '\theta (deg)', 'b', ...
      'SNR', [-5 5]);
    print('-dpng','radon_snr.png');
    print('-depsc2','radon_snr.eps');
  end

  % max Radon SNR
  rmax = max(max(rmap.snr));

  % record location of maxima (on square map)
  b_array = kron(b',ones(size(theta)));
  t_array = kron(ones(size(b))',theta);
  bmax = b_array(rmap.snr==rmax);
  tmax = t_array(rmap.snr==rmax);

  % use iradon to reconstruct the best fit track
  if params.doRadonReconstruction
    reconstructRadon2(params, map0);
  end

  % nominal p-value (assuming normal distribution)
  p = msnr_prob(rmax, map.nindep)

  Y_Gamma_max = max(max(rmap.y));

  fprintf('rmax = %f\n', rmax);
  fprintf('Y_Gamma_max = %1.1e strain^2/Hz\n',Y_Gamma_max);
  fprintf('nindep = %i\n', map.nindep);
  fprintf('p_nominal = %f\n', p);

return
