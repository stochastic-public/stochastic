function [det1 det2] = ifo2dets(params)
%function [det1 det2] = ifo2dets(params)
  if strcmp(params.ifo1(1),'H'), site1='LHO'; end
  if strcmp(params.ifo1(1),'L'), site1='LLO'; end
  if strcmp(params.ifo1(1),'V'), site1='VIRGO'; end

  if strcmp(params.ifo2(1),'H'), site2='LHO'; end
  if strcmp(params.ifo2(1),'L'), site2='LLO'; end
  if strcmp(params.ifo2(1),'V'), site2='VIRGO'; end

% returns detector locations
  det1 = getdetector(site1);
  det2 = getdetector(site2);

return
