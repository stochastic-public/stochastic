function stoch_out=stochmap(params, jobfile, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function stochmap(params, jobfile, varargin)
% E.g., 
%   params = stampDefaults;
%   jobfile='/archive/home/ethrane/preproc/S5H1L1_full_run.txt';
%   stochmap_dev(params, jobfile, 816088197, 816089507); % job 10
% or,
%   stochmap_dev(params, jobfile, 10); % job 10
% output (depending on what is requested in params struct):
%   diagnostic outputfile: map.mat
%   diagnostic plots: snr.eps, y_map.eps, sig_map.eps + .png versions
%   additional output if requested by search algorithm plug-ins
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% E. Thrane, S. Kandhasamy, S. Dorsher, S. Giampanis, M. Coughlin, 
% T. Prestegard
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;

if(nargin==3)     % job number specified
  params.jobNumber = strassign(varargin{1});
elseif(nargin==4) % params.startGPS and params.endGPS are specified
  params.startGPS = strassign(varargin{1});
  params.endGPS = strassign(varargin{2});
else
  error('Error: the number of arguments should be 3 or 4.');
end

% check values of params for self-consistency
params=paramCheck(params);

% seed random number generator
if exist('params.seed', 'var')
  seed=params.seed;
else
  seed=sum(clock);
end
randn('state',seed);

% For Monte Carlo studies we can call stochmap from within preproc to avoid
% writing to frames.  This happens when params.frompreproc==true.  Normal
% operating mode (reading data from frames) if params.frompreproc==false.
if (params.frompreproc)
  params.startGPS = strassign(varargin{1});
  params.endGPS = strassign(varargin{2});
  nSegs = length(params.pass.GPStimes); % number of segments
else % read in data from frames
  % read in and parse jobfile
  job_details = load(jobfile);
  AllStartTimes = job_details(:,2);
  AllEndTimes = job_details(:,3);
  % The fourth argument of the jobfile, job duration, is not needed.
  % AllJobDurations = job_details(:,4);

  % If there are three arguments, set start/stop GPS with jobfile values.
  if(nargin==3)
    params.startGPS = AllStartTimes(params.jobNumber);
    params.endGPS = AllEndTimes(params.jobNumber);
  end

  % consider only segments between params.startGPS and params.endGPS
  cut = AllEndTimes >= params.startGPS & AllStartTimes <= params.endGPS;
  joblist = 1:length(AllStartTimes);
  kept_jobs = joblist(cut);

  % select the frames needed for this interval (center segment + neighbors)
  k=1; % index for frame files of every segment
  for ii=1:length(kept_jobs)
    IM_CacheFile  = [params.intFrameCachePath 'frameFiles' params.ifo1(1) ...
      params.ifo2(1) '.' num2str(kept_jobs(ii)) '.txt'];
    try
      frameFilesJobs = textread(IM_CacheFile, '%s\n', -1, 'commentstyle', ...
        'matlab');
    catch
      fprintf('%s not found\n', IM_CacheFile);
      error('Missing cachefile.');
    end
    temp_dur = frgetvect(frameFilesJobs{1}, ...
      [params.ifo1 params.ifo2 ':segmentDuration'], 0, 10000);
    params.segmentDuration = temp_dur(1); % getting segment duration
    for j=1:length(frameFilesJobs)
      % record the GPS times
      segGPSstart = frgetvect(frameFilesJobs{j}, ...
        [params.ifo1 params.ifo2 ':GPStime'],0,params.segmentDuration);
      % record the frame names
      if params.bkndstudy
        % if a background study is requested, use whatever frames are in the 
        % cachefiles, no matter if they are consecutive
        frameFiles{k}=frameFilesJobs{j};
        k=k+1;
      else
        if(segGPSstart>=params.startGPS && segGPSstart<=params.endGPS)
          frameFiles{k}=frameFilesJobs{j};
          k=k+1;
        end
      end
    end
  end

  % check for data before continuing
  if(~exist('frameFiles', 'var'))
    error('No data between start and end times.\n');
    return;
  end

  nSegs = length(frameFiles); % number of segments

end % closes loop for case where params.frompreproc==false

for I=1:nSegs
  if (params.frompreproc) % get data from params struct.

    J=params.pass.which_segs(I);

    P1 = params.pass.P1(:,J);           P2 = params.pass.P2(:,J);
    naiP1 = params.pass.naiP1(:,J);     naiP2 = params.pass.naiP2(:,J);
    CC = params.pass.CC(:,J);
    pp.flow = params.pass.ppflow(:,J);  pp.fhigh = params.pass.ppfhigh(:,J);
    pp.deltaF = params.pass.ppdeltaF(:,J);
    pp.w1w2bar = params.pass.ppw1w2bar(:,J);
    pp.w1w2squaredbar = params.pass.ppw1w2squaredbar(:,J);
    pp.w1w2ovlsquaredbar = params.pass.ppw1w2ovlsquaredbar(:,J);
    try
      params.numSegmentsPerInterval = params.pass.nSPI(:,I);
    catch
      params.numSegmentsPerInterval = [];
    end
    segGPSTime = params.pass.GPStimes(I);

  else  % read in data from STAMP frames
    % read in STAMP data      
    inputfile = char(frameFiles{I});
    P1 = frgetvect(inputfile,[params.ifo1 ':AdjacentPSD'], ...
      0, params.segmentDuration);
    P2 = frgetvect(inputfile,[params.ifo2 ':AdjacentPSD'], ...
      0, params.segmentDuration);
    naiP1 = frgetvect(inputfile,[params.ifo1 ':LocalPSD'], ...
      0, params.segmentDuration);
    naiP2 = frgetvect(inputfile,[params.ifo2 ':LocalPSD'], ...
      0, params.segmentDuration);
    CC = frgetvect(inputfile,[params.ifo1 params.ifo2 ':CSD'], ...
      0, params.segmentDuration);
    segGPSTime = frgetvect(inputfile,[params.ifo1 params.ifo2 ':GPStime'], ...
      0, params.segmentDuration);
    
    if I==1 % some variables are read in only once
      pp.flow = frgetvect(inputfile,[params.ifo1 params.ifo2 ':flow'], 0, ...
        params.segmentDuration);
      pp.fhigh = frgetvect(inputfile,[params.ifo1 params.ifo2 ':fhigh'], ...
        0, params.segmentDuration);
      pp.deltaF = frgetvect(inputfile,[params.ifo1 params.ifo2 ':deltaF'], ...
        0, params.segmentDuration);
      pp.w1w2bar = frgetvect(inputfile,[params.ifo1 params.ifo2 ...
        ':w1w2bar'], 0, params.segmentDuration);
      pp.w1w2squaredbar = frgetvect(inputfile, [params.ifo1 ...
        params.ifo2 ':w1w2squaredbar'], 0, params.segmentDuration);
      pp.w1w2ovlsquaredbar = frgetvect(inputfile, [params.ifo1 ...
        params.ifo2 ':w1w2ovlsquaredbar'], 0, params.segmentDuration);
      params.numSegmentsPerInterval = frgetvect(inputfile, ...
        [params.ifo1 params.ifo2 ':numSegmentsPerInterval'], 0, ...
        params.segmentDuration);

      % check that pp and params have reasonable and consistent values
      if (params.fmin<pp.flow || params.fmax>pp.fhigh)
        error('params.fmin and/or params.fmax are out of range of data!');
      end

    else
      % check that metadata and segmentDuruation match I==1 case.
      meta_check(params, pp, inputfile);
    end % closes if I==1 statement

  end % closes loop for params.frompreproc==false (data is read from frames)

  %---STAMP DATA FOR SEGMENT=I HAS NOW BEEN EITHER READ IN FROM FRAME OR---%
  %---PASSED TO STOCHMAP BY PREPROC.  NOW WE PROCESS THE DATA.-------------%
  
  % calculate wingfac <std(SNR)> for effective SNR calculation later
  if I==1, params = wingfac(params, pp); end;

  % calculate adjacent PSD structs for use in sigma calculation
  calPSD1_avg = constructFreqSeries(P1, pp.flow, pp.deltaF, 1);
  calPSD2_avg = constructFreqSeries(P2, pp.flow, pp.deltaF, 1);

  % create frequency mask
  dataFM = constructFreqMask(pp.flow, pp.fhigh, pp.deltaF, ...
  params.StampFreqsToRemove,params.StampnBinsToRemove, params.doStampFreqMask);
  mask = constructFreqSeries(dataFM, pp.flow, pp.deltaF);

  % create arrays of frequency and (empty) overlap factor
  numFreqs = floor((pp.fhigh-pp.flow)/pp.deltaF)+1;
  f = pp.flow + pp.deltaF*transpose(0:numFreqs-1);
  % from loadAuxiliarInput (if params.doDirectional).  Here we set the overlap
  % reduction factor to unity which is appropriate for the radiometer.
  gamma = constructFreqSeries(ones(numFreqs,1), pp.flow, pp.deltaF, 1);

  % prepare to calculate Q filter to establish correspondance with stochastic
  if I==1
    % define H(f)-the expected source spectrum is (always) flat in strain power
    Hf.data = ones(length(f),1);
    Hf.flow = pp.flow;
    Hf.deltaF = pp.deltaF;
    Hf.symmetry = 1;
    %
    % set power-law exponent to [] since we are using Hf instead
    params.alphaExp=[];
    %
    % get detector info & calculate maxCorrelationTimeShift (used for improved
    % computational efficiency)
    [det1 det2] = ifo2dets(params);
    maxCorrelationTimeShift=norm(det1.r-det2.r)/params.c;
    % Keep maxCorrelationTimeShift>0 for co-located analyses
    if maxCorrelationTimeShift==0, maxCorrelationTimeShift=0.010; end;
  end
  %
  % calculate optimal filter, theoretical variance (ccVar), and sensitivity
  % integrand (sensInt) using avg psds
  % For more information on ccVar and sensInt, see: http://tinyurl.com/cglx7au
  [Q, ccVar, sensInt] = calOptimalFilter(params.segmentDuration, gamma, ...
    Hf, params.alphaExp, calPSD1_avg, calPSD2_avg, -1, -1, mask, pp);

  % calculate the CSD, ccStat and ccSpec
  CSD = constructFreqSeries(CC /2 * params.segmentDuration ...
    * pp.w1w2bar, pp.flow, pp.deltaF, 1);

  % apply phaseScramble if requested (will remove coherence for noise studies)
  if params.phaseScramble
    rand_phases = 2*pi*rand(size(CSD.data));
    CSD.data = CSD.data.*exp(i*rand_phases);
  end

  % calculate cross-correlation spectrum, ccSpec
  % ccStat, the broadband statistic, is not going to be used for anything
  [ccStat, ccSpec] = processCSD(CSD, Q, maxCorrelationTimeShift);

  % fill in map data
  map.segstarttime(I) = segGPSTime;
  map.cc(:,I) = ccSpec.data;
  map.sensInt(:,I) = sensInt.data;
  map.ccVar(I) = ccVar;
  map.naiP1(:,I) = naiP1;
  map.naiP2(:,I) = naiP2;
  map.P1(:,I) = P1;
  map.P2(:,I) = P2;

end  % loop over intervals I  

% for background study, issue pretend segment start times
if params.bkndstudy
  map.segstarttime = params.startGPS:params.segmentDuration/2:params.endGPS;
end

% define map metadata
map.f = f;
map.deltaF = pp.deltaF;
map.segDur = params.segmentDuration;

% crop frequency range etc.
[map, params] = finishMapRawData(params, map);

% SF: Allsky compatibility of power injection routine
params.det1=det1;
params.det2=det2;

  %---WE HAVE CONSTRUCTED FT-MAPS OF CROSS AND AUTO-POWER.  NOW CALCULATE--%
  %---STAMP STATISTICS-----------------------------------------------------%

if params.skypatch
  % generate a list of search directions in the area around the source
  source = brutepixel(params.thetamax, params.dtheta, params);
  if params.fastring
    source_start = fastring(det1, det2, params.startGPS, source, params);
    source_end = fastring(det1, det2, params.endGPS, source, params);
    source = union(source_start, source_end, 'rows');
  end
  fprintf('looping over %1.0f sky directions\n', size(source,1));
else
  source = [params.ra params.dec];
end
% the number of searches that have been executed=ss (usually=1)
ss=0;

for kk=1:size(source,1)
  % initialize ft-map struct
  map = freshmap(map);

  % initialize ccSpec and other radiometer variables
  ccSpec.data = map.cc;
  sensInt.data = map.sensInt;
  ccVar = map.ccVar;
  midGPSTimes = map.segstarttime + params.segmentDuration/2;

  % run the radiometer; calculate ft-maps
  [map.y, map.sigma, map.z, eps_12, eps_11, eps_22] = ...
      ccSpecReadout_stamp(det1, det2, midGPSTimes, source(kk,:), ...
			   ccSpec, ccVar, sensInt, params, pp);


  if params.Autopower
    map.eps_11 = repmat(eps_11,size(map.y,1),1);
    map.eps_12 = repmat(eps_12,size(map.y,1),1);
    map.eps_22 = repmat(eps_22,size(map.y,1),1);
  end
  
  % calculate fluence in [ergs/cm^2]

  freq_map = repmat(f,1,length(map.segstarttime));
  map.F = freq_map.^2 .* params.ErgsPerSqCM .* (pi*params.c^3/(4*params.G)) ...
	  .* map.y;
  map.sigF = freq_map.^2 .* params.ErgsPerSqCM .* (pi*params.c^3/ ...
						   (4*params.G)).* map.sigma;

  % check for unexpected NaNs.  NaN is OK if associated with freq mask.
  % Expected NaN's from vetos or missing science data are handled later in 
  % glitchcut and addMisingDatatoMap respectively.
  maskindex = find(mask.data==0);
  map0 = clean_maps(map, maskindex);

  % define SNR map
  map.snr = map.y ./ map.sigma;
  map.snrz = map.z ./map.sigma;
  
  % add missing data etc.
  [map, params] = finishMap(params, map);
  
  % make a copy of the maps with no injection present
  map_copy = map;
  
  % loop over injection trials (one trial if no injections performed)
  for tt=1:params.inj_trials*params.alpha_n
    % add "power injection" if requested %%%% SF: Allsky compatibility
    map = powerinj(params, pp, map_copy, tt, det1, det2); 
    
    % calculate Xi stastic if requested
    map = calXi(params, map);
    
    % apply glitch cut if requested in params struct
    % params.glitchCut can have values of 1 to 11 depending on cut requested
    % params.glitchCut==0 means that no cut is applied
    if params.glitchCut>=1
      [map,stoch_out.ncols(kk)] = glitchCut(params, map);
    end
    
    if params.pixelScramble
      [map.snr,map.y,map.F,map.sigma,map.sigF]=scrambleMap(map.snr,map.y, ...
						  map.F,map.sigma,map.sigF);
    end
    
    % save diagnostic plots, but only for one search direction
    if params.savePlots&&kk==1
      saveplots(params, map); 
    end
    
    % save mat file(s) -- one for each search direction
    if params.saveMat
      save([params.outputfilename '_' num2str(kk) '.mat'], ...
	   'map','params','pp'); 
    end
    
    % broadband radiometer comparison
    if params.doRadiometer
      y_rad = sum(map.y.*map.sigma.^-2, 1) ./ sum(map.sigma.^-2, 1);
      y_sig = sum(map.sigma.^-2, 1).^-0.5;
      fprintf('radiometer=\n');
      fprintf('%1.4e +/- %1.4e\n', y_rad, y_sig);
    end
    
    % STAMP searches ----------------------------------------------------------
    if params.doRadon
      ss=ss+1;
      map0 = clean_maps(map, maskindex);
      warning('NaN values removed from map struct for Radon search.');
      % p, estimated p-value, is currently not used for anything
      % same goes for Y_Gamma_max, the strain power in the loudest cluster
      [max_SNR, p, Y_Gamma_max] = searchRadon(params, map0);
      stoch_out.max_SNR(ss) = max_SNR;
      stoch_out.search{ss} = 'Radon';
    end
    
    if params.doBoxSearch
      ss=ss+1;
      max_SNR = searchBox(params, map);
      stoch_out.max_SNR(ss) = max_SNR;
      stoch_out.search{ss} = 'box';
    end
    
    if params.doLH
      ss=ss+1;
      searchLH(params, map);
      stoch_out.max_SNR(ss) = 1e99;  % temporary assignment of SNR
      stoch_out.search{ss} = 'LH';
    end
    
    if params.doClusterSearch
      ss=ss+1;
      fprintf('cluster search for direction %1.0f\n', kk);
      max_cluster = searchClusters(params, map);
      stoch_out.max_SNR(ss) = max_cluster.snr_gamma;
      stoch_out.search{ss} = ['cluster' num2str(kk)];
      stoch_out.cluster = max_cluster;
    end
    if params.doBurstegard
      ss=ss+1;
      fprintf('burstegard cluster search for direction %1.0f\n', kk);
      max_cluster = run_burstegard(map,params);
      stoch_out.max_SNR(ss) = max_cluster.snr_gamma;
      stoch_out.search{ss} = ['cluster' num2str(kk)];
      stoch_out.cluster = max_cluster;
    end

    if params.loudPixel
      stoch_out.loudPixel.SNR = max(map.snr(:));
      stoch_out.loudPixel.x = map.xvals(find(sum(map.snr==max(map.snr(:)),1)));
      stoch_out.loudPixel.y = map.yvals(find(sum(map.snr==max(map.snr(:)),2)));
    end
  end
end

% store param values
stoch_out.params = params;

% do PEM analysis if requested
if params.stamp_pem
  do_pem(params, map, stoch_out);
end

if params.diagnostic, fprintf('\nCaution: diagnostic tools on.\n\n'); end
fprintf('elapsed_time = %f\n', toc);

return
