function pem_params(date,ifo,frame_size,user,start_time,end_time,channel_name)

%loads channel list

channel_split=regexp(channel_name,'_','split');
channel_split=cellstr(channel_split);

channel_name_short=strcat(channel_split);

channel_name_sort=channel_split{2};
for i=3:length(channel_split);
   channel_name_sort=strcat(channel_name_sort,'_',channel_split{i});
end

channel_name_short=channel_name_sort;
channel_name_pre=channel_split{1};

if (channel_name_pre(1)=='H')
   grav_wave_channel='LSC-DARM_ERR';
   frame_type='H1_RDS_R_L1';
end

if (channel_name_pre(1)=='L')
   grav_wave_channel='LSC-DARM_ERR';
   frame_type='L1_RDS_R_L1';
end

if (channel_name_pre(1)=='V')
   grav_wave_channel='Pr_B1_ACp_4000Hz';
   frame_type='CW_RDS';
end

seis_ind=strfind(channel_name,'SE');
try
   if seis_ind(1)>0;
      sample=2048;
      low_freq=40;
      high_freq=127;
   end
catch
   sample=2048;
   low_freq=65;
   high_freq=115;
end
sample=250;
low_freq=10;
high_freq=100;

grav_wave_pre=[channel_name_pre(1) '1'];

fid=fopen(['files/' channel_name '/' channel_name '.txt'],'w+');

fprintf(fid,'% parameters for stochastic search (name/value pairs)\n');
fprintf(fid,'%\n');
fprintf(fid,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf(fid,'\n');
fprintf(fid,'% flags for optional operations\n');
fprintf(fid,'doFreqMask false\n');
fprintf(fid,'doHighPass1 true\n');
fprintf(fid,'doHighPass2 true\n');
fprintf(fid,'doOverlap true\n');
fprintf(fid,'\n');
fprintf(fid,'doSidereal false\n');
fprintf(fid,'\n');
fprintf(fid,'minDataLoadLength 200\n');
fprintf(fid,' \n');
fprintf(fid,'doBadGPSTimes false\n');
fprintf(fid,'%badGPSTimesFile \n');
fprintf(fid,'maxDSigRatio 1.2\n');
fprintf(fid,'minDSigRatio 0.8\n');
fprintf(fid,'\n');

fprintf(fid,'doShift1 false\n');
fprintf(fid,'ShiftTime1 1\n');
fprintf(fid,'doShift2 false\n');
fprintf(fid,'ShiftTime2 0\n');
fprintf(fid,'\n');

fprintf(fid,'% ifo names\n');
fprintf(fid,'ifo1 %s\n',grav_wave_pre);
fprintf(fid,'ifo2 %s\n',channel_name_pre);
fprintf(fid,'\n');
fprintf(fid,'% segment duration (sec)\n');
fprintf(fid,'segmentDuration %s\n',frame_size);

fprintf(fid,'\n');
fprintf(fid,'% parameters for sliding psd estimation:\n');
fprintf(fid,'% numSegmentsPerInterval should be odd; ignoreMidSegment is a flag \n');
fprintf(fid,'% that allows you to ignore (if true) or include (if false) the \n');
fprintf(fid,'% analysis segment when estimating power spectra\n');
fprintf(fid,'numSegmentsPerInterval 9\n');
fprintf(fid,'ignoreMidSegment true\n');
fprintf(fid,'\n');
fprintf(fid,'% freq resolution and freq cutoffs for CC statistic sum (Hz)\n');
fprintf(fid,'flow 10\n');
fprintf(fid,'fhigh %f\n',(sample/2)-1);
fprintf(fid,'deltaF %f\n',1/str2num(frame_size));
fprintf(fid,'\n');
fprintf(fid,'% params for Omega_gw (power-law exponent and reference freq in Hz)\n');
fprintf(fid,'alphaExp 0\n');
fprintf(fid,'fRef 98\n');
fprintf(fid,'\n');
fprintf(fid,'% resample rate (Hz)\n');
fprintf(fid,'resampleRate1 %f\n',sample);
fprintf(fid,'resampleRate2 %f\n',sample);
fprintf(fid,'\n');
fprintf(fid,'% buffer added to beginning and end of data segment to account for\n');
fprintf(fid,'% filter transients (sec)\n');
fprintf(fid,'bufferSecs1 2\n');
fprintf(fid,'bufferSecs2 2\n');
fprintf(fid,'\n');
fprintf(fid,'% ASQ channel\n');
fprintf(fid,'ASQchannel1 %s\n',grav_wave_channel);
fprintf(fid,'ASQchannel2 %s\n',channel_name_short);
fprintf(fid,'\n');
fprintf(fid,'% frame type and duration\n');
fprintf(fid,'frameType1 %s\n',frame_type);
fprintf(fid,'frameType2 %s\n',frame_type);
fprintf(fid,'frameDuration1 -1\n');
fprintf(fid,'frameDuration2 -1\n');
fprintf(fid,'\n');
fprintf(fid,'% duration of hann portion of tukey window \n');
fprintf(fid,'% (hannDuration = segmentDuration is a pure hann window)\n');
fprintf(fid,'hannDuration1 %s\n',frame_size);
fprintf(fid,'hannDuration2 %s\n',frame_size);
fprintf(fid,'\n');
fprintf(fid,'% params for matlab resample routine\n');
fprintf(fid,'nResample1 10\n');
fprintf(fid,'nResample2 10\n');
fprintf(fid,'betaParam1 5\n');
fprintf(fid,'betaParam2 5\n');
fprintf(fid,'\n');
fprintf(fid,'% params for high-pass filtering (3db freq in Hz, and filter order) \n');
fprintf(fid,'highPassFreq1 32\n');
fprintf(fid,'highPassFreq2 32\n');
fprintf(fid,'highPassOrder1 6\n');
fprintf(fid,'highPassOrder2 6\n');
fprintf(fid,'\n');
fprintf(fid,'% coherent freqs and number of freq bins to remove if doFreqMask=true;\n');
fprintf(fid,'% NOTE: if an nBin=0, then no bins are removed even if doFreqMask=true\n');
fprintf(fid,'% (coherent freqs are typically harmonics of the power line freq 60Hz\n');
fprintf(fid,'% and the DAQ rate 16Hz)\n');
fprintf(fid,'freqsToRemove \n');
fprintf(fid,'nBinsToRemove \n');
fprintf(fid,' \n');
fprintf(fid,'% calibration filenames\n');
fprintf(fid,'alphaBetaFile1 none\n');
fprintf(fid,'alphaBetaFile2 none\n');
fprintf(fid,'calCavGainFile1 none\n');
fprintf(fid,'calCavGainFile2 none\n');
fprintf(fid,'calResponseFile1 none\n');
fprintf(fid,'calResponseFile2 none\n');
fprintf(fid,'\n');
fprintf(fid,'% stochastic test\n');
fprintf(fid,'simOmegaRef 0\n');
fprintf(fid,'heterodyned false\n');

fprintf(fid,'\n');

fprintf(fid,'% path to cache files\n');

fprintf(fid,'gpsTimesPath1 /archive/home/%s/STAMP/STAMP-PEM/%s/daily/%s/%s/files/cache/\n',user,ifo,date,frame_size);
fprintf(fid,'gpsTimesPath2 /archive/home/%s/STAMP/STAMP-PEM/%s/daily/%s/%s/files/cache/\n',user,ifo,date,frame_size);
fprintf(fid,'frameCachePath1 /archive/home/%s/STAMP/STAMP-PEM/%s/daily/%s/%s/files/cache/\n',user,ifo,date,frame_size);
fprintf(fid,'frameCachePath2 /archive/home/%s/STAMP/STAMP-PEM/%s/daily/%s/%s/files/cache/\n',user,ifo,date,frame_size);
fprintf(fid,'\n');

fprintf(fid,'%output filename prefix\n');
fprintf(fid,'outputFilePrefix /archive/home/%s/STAMP/STAMP-PEM/%s/daily/%s/%s/%s/%s\n',user,ifo,date,frame_size,channel_name,channel_name);

fprintf(fid,'% STAMP injection\n');
fprintf(fid,'DoStampInj false\n');
fprintf(fid,'StampInjRA 6\n');
fprintf(fid,'StampInjDECL 30\n');
fprintf(fid,'% StampInjStart coincides with the start of job #3\n');
%fprintf(fid,'StampInjStart 816070756\n');
fprintf(fid,'StampInjStart 0\n');
fprintf(fid,'\n');

fprintf(fid,'% PNS convection toy model\n');
fprintf(fid,'%StampInjType PSD\n');
fprintf(fid,'%StampInjFile files/0/PSD_test_file.dat\n');
fprintf(fid,'%StampInjDur 10\n');
fprintf(fid,'\n');

fprintf(fid,'% 300s sin wave with tapered beginning and end\n');
fprintf(fid,'StampInjType time_series\n');
fprintf(fid,'StampInjFile files/0/time_test_file.dat\n');
fprintf(fid,'\n');

fprintf(fid,'doDetectorNoiseSim false\n');
fprintf(fid,'DetectorNoiseFile LIGOsrdPSD_40Hz.txt\n');
fprintf(fid,'\n');

fprintf(fid,'%output filename prefix\n');
fprintf(fid,'outputFilePrefix files/0/frames/S5H1L1_inj\n');
fprintf(fid,'\n');

fprintf(fid,'stochmap true\n');

fprintf(fid,'fft1dataWindow -1\n');
fprintf(fid,'fft2dataWindow -1\n');

fprintf(fid,'startGPS %.0f\n',start_time);
fprintf(fid,'endGPS %.0f\n',end_time);

fprintf(fid,'% kludge factor (temporary)\n');
fprintf(fid,'kludge 1\n');

fprintf(fid,'% search direction (ra in hours, dec in degrees)\n');
fprintf(fid,'ra 6\n');
fprintf(fid,'dec 30\n');

fprintf(fid,'% frequency range\n');
fprintf(fid,'fmin %.0f\n',low_freq);
fprintf(fid,'fmax %.0f\n',high_freq);

fprintf(fid,'% other\n');
fprintf(fid,'doPolar false\n');
fprintf(fid,'saveMat false\n');
fprintf(fid,'savePlots false\n');
fprintf(fid,'debug false\n');
fprintf(fid,'doRadon false\n');
fprintf(fid,'doBoxSearch false\n');
fprintf(fid,'doLH false\n');
fprintf(fid,'doRadiometer false\n');
fprintf(fid,'fixAntennaFactors true\n');
fprintf(fid,'doClusterSearch false\n');
fprintf(fid,'stamp_pem true\n');
fprintf(fid,'Autopower true\n');

fclose(fid);

end
