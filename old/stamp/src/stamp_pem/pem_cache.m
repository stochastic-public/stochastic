function pem_cache(startGPS,endGPS,ifo)

mkdir_command='mkdir files/';
system(mkdir_command);

mkdir_command='mkdir files/cache/';
system(mkdir_command);

frame_files=['frameFiles' ifo(1) '.1.txt'];
gps_times_file=['gpsTimes' ifo(1) '.1.txt'];

startGPS=str2num(startGPS);
endGPS=str2num(endGPS);

gpsTimes=startGPS:400:endGPS;

frameFilesJobs = textread(frame_files, '%s\n', -1, ...
                              'commentstyle', 'matlab');
frame_split=regexp(frameFilesJobs,'-','split');
gps_times=load(gps_times_file);

frame_gps_times=[];  
for j=1:length(frameFilesJobs)
 if ifo(1)=='V'
    segGPSstart=str2num(frame_split{j}{5});
 else
    segGPSstart=str2num(frame_split{j}{5});
 end
 frame_gps_times=[frame_gps_times segGPSstart];
end

for i=1:length(gpsTimes)-1
 try
  startSegment=gpsTimes(i);
  endSegment=gpsTimes(i+1);

  startSegment_indexes = find(startSegment<=frame_gps_times);
  startSegment_indexes = [startSegment_indexes(1) - 1 startSegment_indexes];

  endSegment_indexes = find(endSegment>=frame_gps_times);
  endSegment_indexes = [endSegment_indexes(end) + 1 endSegment_indexes];
  
  which_segs= intersect(startSegment_indexes,endSegment_indexes);

  which_segs=sort([min(which_segs)-1 which_segs max(which_segs)+1]);

  fid=fopen(['files/cache/frameFiles' ifo(1) '.' num2str(i) '.txt'],'w+');
  fid1=fopen(['files/cache/gpsTimes' ifo(1) '.' num2str(i) '.txt'],'w+');
  for j=1:length(which_segs)
    try
     fprintf(fid,'%s\n',frameFilesJobs{which_segs(j)});
     fprintf(fid1,'%.0f\n',gps_times(which_segs(j)));
    catch
    end
  end
  fclose(fid);
  fclose(fid1);
 catch
 end
end

fid=fopen('jobfile_all.txt','w+');
for i=1:length(gpsTimes)
  startSegment=gpsTimes(i);
  if i==length(gpsTimes);
     endSegment = gpsTimes(i-1) + 400;
  else
     endSegment=gpsTimes(i+1);
  end
  duration=endSegment-startSegment;
  fprintf(fid,'%d %d %d %d\n',i,startSegment,endSegment,duration);
end
fclose(fid);


