function [rmax, time_begin_place, time_end_place] = searchRadon(params, map)
% function [rmax, p] = searchRadon(params, map)
% E. Thrane

  % keep a non-square copy for reconstruction
  map0=map;

  x = size(map.y,1);
  y = size(map.y,2);

  % square off map for Radon search
  map.y=square_map(map.y,0);
  map.sigma=square_map(map.sigma,1);
  map.F=square_map(map.F,0);
  map.sigF=square_map(map.sigF,1);

  % parameterize search angles
  theta = 0:180;

  % calculate y_Gamma and sigma_Gamma for every point in Radon space
  if params.fluence
    PEM_cut = abs(map.F)./map.sigF > 2;
  else
    PEM_cut = abs(map.y)./map.sigma > 2;
  end

  PEM_snr = zeros(size(map.y));
  PEM_snr(PEM_cut) = 1;
  cut_1=find(sum(PEM_snr,1)>floor(length(PEM_snr(:,1))/5));
  PEM_snr(:,cut_1)=0;
  cut_2=find(sum(PEM_snr,2)>floor(length(PEM_snr(1,:))/5));
  PEM_snr(cut_2,:)=0;

  [rmap.snr, b] = radon(PEM_snr,theta);

  rmap.xvals = theta;
  rmap.yvals = b; % impact parameter

   PEM_radon=rmap.snr;
   PEM_length=length(PEM_radon(1,:));
   PEM_length_half=floor(length(PEM_radon(1,:))/2);
   PEM_lines=[7:PEM_length_half-7,PEM_length_half+7:PEM_length-7];
   rmax=max(max(PEM_radon(:,PEM_lines)));
   PEM_cut = find(PEM_radon==rmax);
   PEM_snr = zeros(size(PEM_radon));
   PEM_snr(PEM_cut) = 1;

   rmap.fit = zeros(size(rmap.snr));
   rmap.fit(rmap.snr==rmax) = rmax;

   % Use iradon to invert.
   map.fit = iradon(rmap.fit, theta);

  % Undo squaring.
  if x>y
    map.fit = map.fit(:,1:y);
    map.snr = map.snr(:,1:y);
  elseif x<y
    map.fit = map.fit(1:x,:);
    map.snr = map.snr(1:x,:);
  end

  if rmax>35
    printmap(rmap.snr, rmap.xvals, rmap.yvals, '\theta (deg)', 'b', ...
      'SNR', [-5 5]);
    print('-dpng',[params.which_folder 'radon_snr.png']);
    print('-depsc2',[params.which_folder 'radon_snr.eps']);

    printmap(map.fit, map.xvals, map.yvals, 't (s)', 'f (Hz)', ...
      '', [min(min(map.fit)) max(max(map.fit))]);
    print('-dpng',[params.which_folder 'radon_fit.png']);
    print('-depsc2',[params.which_folder 'radon_fit.eps']);
  end

  PEM_cut = find(map.fit>0);
  PEM_snr = map.fit;
  PEM_snr(PEM_cut) = 1;
  time_find_ave=mean(PEM_snr);
  time_find=time_find_ave>0;
%  time_find(101)=0;
  time_find_all=[];
  for i=1:length(time_find)
     if time_find(i)>0
        time_find_all=[time_find_all i];
     end
  end
  time_begin_place=min(time_find_all);
  time_end_place=max(time_find_all);

return
