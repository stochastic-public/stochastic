function coh = pem_coherence(x,y)

cov_matrix = cov(x,y);

coh = cov_matrix(1,2)/sqrt(var(x)*var(y));

