function map = calXi(params, map)
% function map = calXi(params, map)
% Calculates auto-power consistency stat.
% Based on STAMP conventions,
% 1 = Hanford and 2 = Livingston.
% For additional documentation, see
% https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?docid=71680
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if params.Autopower

  % Calculate Xi (see Eq. 19).
  map.Xi = ((map.naiP1 - map.P1) ./ map.eps_11) - ...
    ((map.naiP2 - map.P2) ./ map.eps_22);

  % Estimator for the variance (see Eq. 21).
  map.sigma_Xi = sqrt( abs( (map.eps_11 .^ (-2)) .* (map.naiP1 .^ 2 + ...
    map.P1 .^ 2) + (map.eps_22 .^ (-2) ) .* (map.naiP2 .^ 2 + ...
    map.P2 .^ 2) - 2*( (map.eps_12 .^ 2) ./ (map.eps_11 .* ...
    map.eps_22 ) ) .* (map.y .^ 2) ) );

  % Normalize map (calculate Xi SNR).
  map.Xi_snr = map.Xi ./ map.sigma_Xi;
end

return;


