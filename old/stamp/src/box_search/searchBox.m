function maxSNR = searchBox(params, map)
% function searchBox(params, map)
% INPUT :
%   params -> structure variable used in stochMap; it contains a lot of 
%             information about the frames and search parameters.
%   map -> structure containing
%     y -> map of y (cross correlation data)
%     sigma -> map containing corresponding sigma (of y map)
%     xvals -> All segment start times (either relative or original GPS times)
%     yvals -> All frequency bin values
% 
% OUTPUT :
%   This code will make plots of, if params.savePlots = true
%      i) ft map of boxes
%      ii) Log histogram of SNR of boxes,
%   And for the box of maximum SNR, it will also  
%     i) print the corresponding box details (freq and time interval),
%     ii) plot the time dependence of y for that particular frequency band 
%         (with params.params.savePlots = true) (OPTIONAL),
%
% Routine written by S. Kandhasamy
% If there are any issues please contact shivaraj@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

AllStartTimes = map.xvals;
freq_list = map.yvals;

% converting absolute GPS segment times to relative times
time_list = AllStartTimes - AllStartTimes(1);

% size of pixels in ft map
deltaF = freq_list(2) - freq_list(1);
deltaT = time_list(2) - time_list(1);

% size of search box
f_box = ceil(params.box.freq_width/deltaF);
t_box = ceil(params.box.time_width/deltaT);

% calculating sum(y/y_sigma^2), sum(1/y_sigma^2) and SNR_y for each box
numerator = map.y./map.sigma.^2;
denominator = 1./map.sigma.^2;
% all_sum.m defined at the end ;
% blkproc.m is a image processing routine which is generally used for reducing
% the size of high resolution image by collabsing pixels in a particular
% manifold (generally non-overlapping boxes) to a single pixel; 
y_num = blkproc(numerator,[f_box t_box],@all_sum); 
y_den = blkproc(denominator,[f_box t_box],@all_sum);
y = y_num./y_den;
sigma_y = 1./sqrt(y_den);

% conditioning the data
cut_y_zero = y == 0; 
sigma_y(cut_y_zero) = Inf; 

% SNR of boxes
SNR_y = y./sigma_y;

if params.savePlots %-----------------------------------------------
  % ft map (SNR) of boxes 
  printmap(SNR_y, [time_list(1) time_list(end)], ...
     [freq_list(1)  freq_list(end)], 'time (s)', 'f (Hz)', 'SNR of boxes',...
     [-5 5]);
  pretty;
  print('-depsc2','box_SNR_map.eps');
  print('-dpng','box_SNR_map.png');
   
  % log histogram of boxes (outliers can be clearly seen in this plot)
  SNR_Plot = abs(SNR_y(~isnan(SNR_y)));
  figure 
  % if SNR of box > 6 it will be displayed in dotted line
  linstep(SNR_Plot,[],1,'b-',6)
  xlabel('box SNR')
  ylabel('No.of bins')
  pretty;
  print('-depsc2','box_histogram_log.eps');
  print('-dpng','box_histogram_log.png');
end %-------if params.savePlots--------------------------------------

% For debugging purposes and for making distance plot; here we look at the 
% exact placees where we injected the signal and see what we recover; 
% useful for testing injections 
if params.box.debug
  debugBox(params,AllStartTimes,freq_list);
end

% details of the box having maximum SNR in the FT map
[nouse, ii] = max(SNR_y);
[maxSNR, jj] = max(max(SNR_y));
row_no = ii(jj);
col_no = jj;

initial_f = freq_list(1) + (row_no-1)*f_box*deltaF;
final_f = initial_f + f_box*deltaF;
if(final_f > freq_list(end))
  final_f = freq_list(end);
end
initial_t = time_list(1)+(col_no-1)*t_box*deltaT;
final_t = initial_t+t_box*deltaT; 
if(final_t > time_list(end))
  final_t = time_list(end);
end

% Printing the details of the box to screen
fprintf('Freq Band %.2f - %.2f Hz and Time interval %d (%d) - %d (%d) \n', ... 
         initial_f,final_f,initial_t+AllStartTimes(1),initial_t, ...
         final_t+AllStartTimes(1),final_t);

% time domain statistic for that particular frequency band
if params.savePlots
  figure
  t = time_list(1):t_box*deltaT:time_list(end);
  errorbar(t-t(1),y(row_no,:),sigma_y(row_no,:),'bo')
  xlabel('time (s)')
  ylabel('y')
  axis([-10 time_list(end) min(y(row_no,:))*5 max(y(row_no,:))*2])
  legend([num2str(initial_f) ' - ' num2str(final_f) ' Hz band']);
  pretty;
  print('-depsc2','y_vs_time_box.eps');
  print('-dpng','y_vs_time_box.png');
end

return;
end % ---------- main function end-----------------------------------------

% function used inside the code
function sum_all = all_sum(x)
  % function sum_all = all_sum(x)
  % Gives sum of all elements in the matrix x 
  sum_all = sum(x(:));
end
