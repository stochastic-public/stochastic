function map_out = powerinj(params, pp, map, tt, det1, det2)
% function map = powerinj(params, pp, map)
% by E. Thrane
% adds injection to the cross- and auto- power maps

% the default is to not add any power to the map
if ~params.powerinj, map_out=map; return; end;

fprintf('performing power injection...');
% load injection file
inj = load(params.injfile);

% make sure the injection data is parsed for the present resolution
if params.segmentDuration~=inj.params.segmentDuration | ...
  pp.deltaF~=inj.pp.deltaF
  error('injfile resolution not matched for data.');
end

% make sure the frequency content is sufficient
if inj.map.f(1)<map.f(1) | inj.map.f(end)>map.f(end)
  fprintf('inj.map.f(1) = %1.3e\n', inj.map.f(1));
  fprintf('map.f(1) = %1.3e\n', map.f(1));
  fprintf('inj.map.f(end) = %1.3e\n', inj.map.f(end));
  fprintf('map.f(end) = %1.3e\n', map.f(end));
  error('injfile does not contain sufficient frequency content.');
end

% crop NaN times from map
y_inj = inj.map.y;
z_inj = inj.map.z;
p1_inj = inj.map.P1;
p2_inj = inj.map.P2;
nai_p1_inj = inj.map.naiP1;
nai_p2_inj = inj.map.naiP2;
eps12_inj = inj.map.eps_12;
eps11_inj = inj.map.eps_11;
eps22_inj = inj.map.eps_22;
p1_inj(:,isnan(y_inj(1,:)))=[];
p2_inj(:,isnan(y_inj(1,:)))=[];
nai_p1_inj(:,isnan(y_inj(1,:)))=[];
nai_p2_inj(:,isnan(y_inj(1,:)))=[];
eps12_inj(:,isnan(y_inj(1,:)))=[];
eps11_inj(:,isnan(y_inj(1,:)))=[];
eps22_inj(:,isnan(y_inj(1,:)))=[];
y_inj(:,isnan(y_inj(1,:)))=[];
z_inj(:,isnan(z_inj(1,:)))=[];

% make sure the map is long enough
if size(map.snr,2) < size(y_inj,2)
  error('map is too short for inj.');
end

% pad injection map if it is too small
if inj.map.yvals(end)<map.yvals(end)
  buffer = zeros(sum(map.yvals>inj.map.yvals(end)), size(y_inj,2));
  buffer1 = ones(sum(map.yvals>inj.map.yvals(end)), size(y_inj,2));
  y_inj = vertcat(y_inj, buffer);
  z_inj = vertcat(z_inj, buffer);
  p1_inj = vertcat(p1_inj, buffer);
  p2_inj = vertcat(p2_inj, buffer);
  nai_p1_inj = vertcat(nai_p1_inj, buffer);
  nai_p2_inj = vertcat(nai_p2_inj, buffer);
  eps12_inj = vertcat(eps12_inj, buffer1);
  eps11_inj = vertcat(eps11_inj, buffer1);
  eps22_inj = vertcat(eps22_inj, buffer1);
  inj.map.yvals = inj.map.yvals:inj.pp.deltaF:map.yvals(end);
end

% crop frequencies if the injection map is too big
fcut = inj.map.yvals>=map.yvals(1) & inj.map.yvals<=map.yvals(end);
y_inj = y_inj(fcut,:);
z_inj = z_inj(fcut,:);
p1_inj = p1_inj(fcut,:);
p2_inj = p2_inj(fcut,:);
nai_p1_inj = nai_p1_inj(fcut,:);
nai_p2_inj = nai_p2_inj(fcut,:);
eps12_inj = eps12_inj(fcut,:);
eps11_inj = eps11_inj(fcut,:);
eps22_inj = eps22_inj(fcut,:);
inj.map.yvals_cut=inj.map.yvals(fcut);

% alpha is the scale factor
% 
zz = mod(tt-1, params.alpha_n)+1;
alpha = (zz-1)*params.alpha_step + params.alpha_min;
fprintf('\nzz=%1.0f, alpha=%1.3e\n', zz, alpha);

% pad injection map with zeros
nzeros = size(map.snr,2) - size(y_inj,2);
% power ~ h^2 ~ alpha
% Thus, h ~ sqrt(alpha)
% Thus, D ~ 1/h ~ 1/sqrt(alpha)
if params.fixedInjectionTime
  lead = 1;
else
  lead = floor(rand*(nzeros+1));
end
%
y_inj2 = zeros(size(map.snr));
y_inj2(:,lead:lead+size(y_inj,2)-1) = alpha*y_inj;
z_inj2 = zeros(size(map.snr));
z_inj2(:,lead:lead+size(z_inj,2)-1) = alpha*z_inj;
p1_inj2 = zeros(size(map.snr));
p1_inj2(:,lead:lead+size(p1_inj,2)-1) = alpha*p1_inj;
p2_inj2 = zeros(size(map.snr));
p2_inj2(:,lead:lead+size(p2_inj,2)-1) = alpha*p2_inj;
%
nai_p1_inj2 = zeros(size(map.snr));
nai_p1_inj2(:,lead:lead+size(nai_p1_inj,2)-1) = alpha*nai_p1_inj;
nai_p2_inj2 = zeros(size(map.snr));
nai_p2_inj2(:,lead:lead+size(nai_p2_inj,2)-1) = alpha*nai_p2_inj;
%
eps12_inj2 = zeros(size(map.snr));
eps12_inj2(:,lead:lead+size(eps12_inj,2)-1) = eps12_inj;
eps11_inj2 = zeros(size(map.snr));
eps11_inj2(:,lead:lead+size(eps11_inj,2)-1) = eps11_inj;
eps22_inj2 = zeros(size(map.snr));
eps22_inj2(:,lead:lead+size(eps22_inj,2)-1) = eps22_inj;

% The injection was processed at one sky location and time, and now we are
% going to analyze it at another time.  Apply a renormalization array to
% account for the different sensitivity.
%%%%%%%%%%%%% S. Franco: All-sky compatibility %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(params.skypatch)
  source_inj=[inj.params.ra,inj.params.dec];
  source_search=[params.ra,params.dec];
  tau = calculate_time_delay(inj.params.det1,inj.params.det2, ...
                            inj.params.stamp_inj_start, source_inj);
  tau_2 = calculate_time_delay(det1,det2,params.startGPS,source_search);
  Y_compl = y_inj2+i*z_inj2;
  for ff=1:size(inj.map.yvals_cut,2)
    Y_compl_2(ff,:) = Y_compl(ff,:) .* ...
      exp(i*2*pi*inj.map.yvals_cut(ff)*(tau_2-tau));
  end
  y_inj2 = real(Y_compl_2);
end

% set zero efficiency to NaN to avoid "divide by zero" warning
eps12_inj2(:,eps12_inj2(1,:)==0)=NaN;
eps11_inj2(:,eps11_inj2(1,:)==0)=NaN;
eps22_inj2(:,eps22_inj2(1,:)==0)=NaN;
%
X12 = abs(map.eps_12 ./ eps12_inj2);
X11 = abs(map.eps_11 ./ eps11_inj2);
X22 = abs(map.eps_22 ./ eps22_inj2);
% set normalization factor to unity where inj efficiency is zero
X12(:,isnan(eps12_inj2(1,:)))=1;
X11(:,isnan(eps11_inj2(1,:)))=1;
X22(:,isnan(eps22_inj2(1,:)))=1;

% rescale injection maps
y_inj2 = X12.*y_inj2;
p1_inj2 = X11.*p1_inj2;
nai_p1_inj2 = X11.*nai_p1_inj2;
p2_inj2 = X22.*p2_inj2;
nai_p2_inj2 = X22.*nai_p2_inj2;

% NOT neighboring signal
h1 = sqrt(nai_p1_inj2) .*  exp(2*pi*i*rand(size(nai_p1_inj2)));
h2 = sqrt(nai_p2_inj2) .*  exp(2*pi*i*rand(size(nai_p2_inj2)));
% NOT neighboring noise
n1 = sqrt(map.naiP1);
n1 = n1 .*  exp(2*pi*i*rand(size(n1)));
n2 = sqrt(map.naiP2);
n2 = n2 .*  exp(2*pi*i*rand(size(n2)));
% neighboring noise--------------------------------------
n1n = sqrt(map.P1) .*  exp(2*pi*i*rand(size(n1)));
n2n = sqrt(map.P2) .*  exp(2*pi*i*rand(size(n2)));
% neighboring signal
h1n = sqrt(p1_inj2) .* exp(2*pi*i*rand(size(p1_inj2)));
h2n = sqrt(p2_inj2) .* exp(2*pi*i*rand(size(p2_inj2)));
%--------------------------------------------------------

% copy over meta data and other info
map_out = map;

% add injections to map
%eht: map_out.y = map.y + y_inj2 + real(conj(h1).*n2 + h2.*conj(n1));
%eht: map_out.P1 = map.P1 + p1_inj2 + real( conj(h1n).*n1n + h1n.*conj(n1n) );
%eht: map_out.P2 = map.P2 + p2_inj2 + real( conj(h2n).*n2n + h2n.*conj(n2n) );
% cross-terms can cause P1 to be *less* than without the injection owing to
% the complexity of h1n and n1n.  Instead, use average of cross-terms.
map_out.y = map.y + y_inj2;
map_out.P1 = map.P1 + p1_inj2;
map_out.P2 = map.P2 + p2_inj2;
map_out.sigma = sqrt(map_out.P1.*map_out.P2) .* ...
  (map.sigma ./ sqrt(map.P1.*map.P2));

% recalculate SNR
map_out.snr = map_out.y./map_out.sigma;

% final calculations for naive sigmas
map_out.naiP1 = map.naiP1 + nai_p1_inj2 + ...
  real( conj(h1).*n1 + h1.*conj(n1) );
map_out.naiP2 = map.naiP2 + nai_p2_inj2 + ...
  real( conj(h2).*n2 + h2.*conj(n2) );

% record scale factor
map_out.alpha=alpha;

fprintf('done.\n');

return
