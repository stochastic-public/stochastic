function tau = calculate_time_delay(det1,det2,GPS,source);

  c = 299792458;

% calculate sidereal time
time=GPStoGreenwichMeanSiderealTime(GPS);

% convert hours into radians: 24 hrs / 2pi rad
w=pi/12;

  psi=w*(time-source(1));
  theta=-pi/2+pi/180*source(2);
  ctheta=cos(theta);
  stheta=sin(theta);
  cpsi=cos(psi);
  spsi=sin(psi);
  Omega1=-cpsi*stheta;
  Omega2=spsi*stheta;
  Omega3=ctheta;

% calculate vector for the separation between sites.
  s = det2.r - det1.r;

  % time delay between detectors
  tau=(Omega1*s(1)+Omega2*s(2)+Omega3*s(3))/c;

return
