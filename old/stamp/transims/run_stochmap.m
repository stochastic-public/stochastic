function run_stochmap(jobfile, jobnumber, varargin)

% STAMP default parameters
params = stampDefaults;

% Optional additional parameters in varargin, of size:
optargin = size(varargin,2);

% Overwriting some of the default parameters
%params.saveMat       = true;
params.freqsToRemove = [];
params.nBinsToRemove = [];
params.savePlots = false;

% Frame generation parameters 
pinj = makePulsarSignalParams();

% --------------------- Search direction ----------------------------------------------------
RAJ  = pinj.RAJ;  % right ascention in "hour:minute:second.xxx"
DECJ = pinj.DECJ; % declination in "deg:min:sec.xx" 

RAJhours = strtok(RAJ,':');
RAJmin   = strtok(RAJ(length(RAJhours)+1:end),':');
RAJsec   = strtok(RAJ(length(RAJhours)+length(RAJmin)+3:end),'.');
RAJsecd  = RAJ(length(RAJhours)+length(RAJmin)+length(RAJsec)+3:end);

% right ascention in hours
params.ra = str2num(RAJhours) + (str2num(RAJmin)/60) + (str2num(RAJsec)+str2num(RAJsecd))/3600;
% right ascention in degrees
%params.ra = params.ra * 360 / 24;

DECJdeg  = strtok(DECJ,':');
DECJmin  = strtok(DECJ(length(DECJdeg)+1:end),':');
DECJsec  = strtok(DECJ(length(DECJdeg)+length(DECJmin)+3:end),'.');
DECJsecd = DECJ(length(DECJdeg)+length(DECJmin)+length(DECJsec)+3:end);

% declination in degrees
params.dec = str2num(DECJdeg) + (str2num(DECJmin)/60) + (str2num(DECJsec)+str2num(DECJsecd))/3600;

% ---------------------- Frequency range of search -----------------------------------------------
params.fmin = 53.5;
params.fmax = 66.5;

% --------------------- Image-specific parameters -----------------------------------------------
params.doBoxSearch = true;
params.box.freq_width = 0.01; % ft box size, (in Hz)  
params.box.time_width = 500; % in sec
params.box.debug = 0;

%params.doClusterSearch = true;
%params = clusterDefaults(params);

%params.doRadon=true;
%params.doRadonReconstruction=true;
%params.fixAntennaFactors=true;
%params.glitchCut=4;
%params.phaseScramble=0;
%params.doPolar=true;
%params.psi=50*(pi/180);
%params.iota=15*(pi/180);
%params.Autopower=1;
%params.iota = 2.0944; 
%params.psi = 0.3;


if optargin == 0
  stoch_out = stochmap(params, jobfile, jobnumber);
elseif optargin == 1
  extraparmsfile = varargin{1};
  try
    extraparms = readParamsFromFile(extraparmsfile);
  catch 
    error('could not parse in extra param file');
  end
  try 
    fn1 = fieldnames(params);
    fn2 = fieldnames(extraparms);
    fn  = [fn1; fn2];
    ufn = length(fn) == unique(length(fn))
    c1  = struct2cell(params);
    c2  = struct2cell(extraparms);
    c   = [c1;c2];
    params = cell2struct(c,fn);
  catch
    error('unable to merge extraparms with params');
  end
  stoch_out = stochmap(params, jobfile, jobnumber);
else
  error('number of extra input arguments has to be either 0 or 1');
end

outvar = stoch_out.max_SNR;

fprintf('stoch_out.max_SNR = %d \n',outvar);

maxSNRfile = [params.intFrameCachePath 'maxSNR.dat'];

save(maxSNRfile,'outvar', '-ASCII')


%%%%%%%%%%%%   Rerun for background estimation %%%%%%%%%%%%%%%%%%
params.fmin = 63.5;
params.fmax = 76.5;

stoch_out2 = stochmap(params, jobfile, jobnumber);

outvar = stoch_out2.max_SNR;

fprintf('stoch_out.max_SNR_background = %d \n',outvar);

maxSNRfile = [params.intFrameCachePath 'maxSNRbackground.dat'];

save(maxSNRfile,'outvar', '-ASCII')


