
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include <getopt.h>

#include <lal/LALStdio.h>
#include <lal/AVFactories.h>
#include <lal/ConfigFile.h>
#include <lal/LALFrameIO.h>
#include <lal/FrameStream.h>
#include <lal/LALDetectors.h>
#include <lal/TimeSeries.h>
#include <lal/Units.h>
#include <lal/LALFrameL.h>


struct CommandLineArgsTag {
    char  *timeseriesfile;
    char  *observatory;
    char  *channel;
    char  *outdirectory;
    UINT4 startTime;
    REAL4 rate;
    REAL4 fMin;
    REAL4 Tobs;
} CLA;


/***************************************************************************/
/* Reads the command line */
int ReadCommandLine(int argc,char *argv[],struct CommandLineArgsTag *CLA);

/************************************* MAIN PROGRAM *************************************/

int main( int argc, char *argv[] )
{

    /* read the command line */
    if (ReadCommandLine(argc,argv,&CLA)) return 1;
      
    /* pointer to input file */
    FILE *pFile1 = NULL;

    /* read in time series files */
    CHAR fileName1[LALNameLength];

    /* parameters for frame generation */
    LIGOTimeGPS gpsStartTime;
    UINT4 Nseg = floor(CLA.Tobs*CLA.rate);

    /* time-series file */
    snprintf(fileName1, LALNameLength,"%s",CLA.timeseriesfile);

    gpsStartTime.gpsSeconds = CLA.startTime;
    gpsStartTime.gpsNanoSeconds = 0.;


    REAL8TimeSeries *series1;
    series1 = XLALCreateREAL8TimeSeries(CLA.channel, &gpsStartTime, CLA.fMin, 1./CLA.rate, &lalStrainUnit, Nseg);  

    /* frame parameters - structure in FrameStream.h */
    FrOutPar frSeries1 = {strcat(CLA.outdirectory,CLA.observatory), CLA.channel, SimDataChannel, 1, 0, 0 };
    

    pFile1 = fopen(fileName1,"r");

    if (pFile1==NULL) {fputs ("File error\n",stderr); exit (1);}
    

    UINT4 i;
    for (i = 0; i < Nseg; i++)
        if (fscanf(pFile1, "%*g %lg", &series1->data->data[i]) == EOF)
            break;


    fclose(pFile1);


    /* define and initialize status pointer */
    static LALStatus status;
    status.statusPtr = NULL;

    /* write output */
    LALFrWriteREAL8TimeSeries(&status,series1,&frSeries1);

    

    return 0;

}



/*******************************************************************************/
int ReadCommandLine(int argc,char *argv[],struct CommandLineArgsTag *CLA)
{
  int errflg = 0;
  optarg = NULL;

  struct option long_options[] = {
    {"time-series-file",          required_argument, NULL,           'f'},
    {"observatory",               required_argument, NULL,           'i'},
    {"channel",                   required_argument, NULL,           'c'},
    {"outdir",                    required_argument, NULL,           'o'},
    {"startTime",                 required_argument, NULL,           's'},
    {"rate",                      required_argument, NULL,           'r'},
    {"fMin",                      required_argument, NULL,           'm'},
    {"Tobs",                      required_argument, NULL,           't'},
    {0, 0, 0, 0}
  };
  char args[] = "hf:i:c:o:s:r:m:t:";

  CLA->timeseriesfile = NULL;  
  CLA->observatory    = NULL;
  CLA->channel        = NULL;
  CLA->outdirectory   = NULL;
  CLA->startTime      = 0;
  CLA->rate           = -1;          
  CLA->fMin           = -1;
  CLA->Tobs           = -1;

 /* Scan through list of command line arguments */
  while ( 1 )
  {
    int option_index = 0; /* getopt_long stores long option here */
    int c;

    c = getopt_long_only( argc, argv, args, long_options, &option_index );
    if ( c == -1 ) /* end of options */
      break;

    switch ( c )
    {

    case 'f':
      /* time series file  */
        CLA->timeseriesfile = optarg;
      break;
    case 'i':
      /* observatory  */
        CLA->observatory = optarg;
      break;
    case 'c':
      /* channel */
        CLA->channel = optarg;
      break;
    case 'o':
        /* output directory */
        CLA->outdirectory = optarg;
        break;
    case 's':
      /* start time  */
        CLA->startTime = atoi(optarg);
      break;
    case 'r':
      /* sampling rate */
        CLA->rate = atof(optarg);
      break;
    case 'm':
      /* lowest detector frequency */
        CLA->fMin = atoi(optarg);
      break;
    case 't':
      /* frame duration (observation time) */
        CLA->Tobs = atof(optarg);
      break;

    case 'h':
      /* print usage/help message */
      fprintf(stdout,"Arguments are :\n");
      fprintf(stdout,"\t--time-series-file (-f)\t\tCHAR\t Time series file to use.\n");
      fprintf(stdout,"\t--observatory (-i)\t\tCHAR\t Observatory (H,L,etc).\n");
      fprintf(stdout,"\t--channel (-c)\t\t\tCHAR\t Channel name (L1:STRAIN, etc).\n");
      fprintf(stdout,"\t--outdir (-o)\t\t\tCHAR\t Output frames directory  name (./frames).\n");
      fprintf(stdout,"\t--startTime (-s)\t\tFLOAT\t Start time.\n");
      fprintf(stdout,"\t--rate (-r)\t\t\tFLOAT\t Time-series sampling frequency.\n");
      fprintf(stdout,"\t--fMin (-m)\t\t\tFLOAT\t Minimum detector frequency.\n");
      fprintf(stdout,"\t--Tobs (-t)\t\t\tFLOAT\t Frame duration.\n");
      exit(0);
      break;
    default:
      /* unrecognized option */
      errflg++;
      fprintf(stderr,"Unrecognized option argument %c\n",c);
      exit(1);
      break;
    }
    }

  if(CLA->timeseriesfile == NULL)
    {
      fprintf(stderr,"No time series file specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->observatory == NULL)
    {
      fprintf(stderr,"No observatory specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->channel == NULL)
    {
      fprintf(stderr,"No channel name specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->outdirectory == NULL)
  {
      fprintf(stderr,"No output frame directory name specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
  }
  if(CLA->startTime == 0)
    {
      fprintf(stderr,"No start time specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->rate == -1)
    {
      fprintf(stderr,"No sampling frequency specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->fMin == -1)
    {
      fprintf(stderr,"No minimum frequency specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->Tobs == -1)
    {
      fprintf(stderr,"No frame duration specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }


  return errflg;
}



/*********************************************************************/
/* Archival code... replaced by fscanf */


    /* char str1[200];*/
    /* char *str1p;*/


    /* Loop over time-series to extract gpstime and h(t) */
/*    UINT4 k = 0;
    while(fgets(str1,sizeof(str1),pFile1) != NULL)
    {

      int len = strlen(str1)-1;
     

      if( str1[len] == '\n' ) 
          { 
              str1p = strtok(str1, " "); // printf("%s\n",str1p);
             
              str1p = strtok(NULL, " "); // printf("%s\n",str1p); 

              series1->data->data[k] += atof(str1p);

              str1[len] = 0;
              
              k++;
          }
    }
*/
