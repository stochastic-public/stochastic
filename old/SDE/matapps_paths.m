function matapps_paths(rootDir,sdeDir)
% MATAPPS_PATH - set path to reference utilities,sims in matapps
%   matapps_paths(rootDir)
%
% rootDir: top level directory for 'matapps' 
%       -- typically ~/no-backup/lscsoft/matapps
%
% - file matapps_paths.txt should have utilities and simulation
%       directories
%
%  $Id: matapps_paths.m,v 1.5 2006/09/06 16:47:39 kathorne Exp $

error(nargchk(2,2,nargin),'struct');
rootDir = char(rootDir);
sdeDir = char(sdeDir);
if (isempty(rootDir) | ~ischar(rootDir))
    msgId = 'matapps_path:badRoot';
    error(msgId,'%s: root directory input is bad\n');
end
if(~isdir(rootDir))
    msgId = 'matapps_path:badRoot';
    error(msgId,'%s: root directory %s not found\n',rootDir);
end
pathFile=sprintf('%s/matapps_paths.txt',sdeDir);
directories=textread(pathFile,'%s','commentstyle','shell');
for i=1:length(directories)
  dir=sprintf('%s/%s',rootDir,directories{i});
  addpath(dir);
end
return
