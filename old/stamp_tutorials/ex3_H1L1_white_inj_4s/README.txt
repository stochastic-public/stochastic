Instructions

0. Compare ex3_params.txt with ../ex2_S5H1L1_MC_Noise_1s/ex2_params.txt.
Notice how this paramfile differ from the ex2 case.  In this example, you will
be injecting a GW signal described in wn_1e-5.dat on top of the existing
strain time series.  It is (very loud) white noise.

1. Look inside frames/.  There should be a bunch of *.gwf files downloaded from
svn.  Now you can try making your own version of these frames.  Open matlab.
Run: preproc('ex3_params.txt', 'S5H1L1_full_run.txt', 1)
This will create a bunch of frames.  Try ls -hl frames/ to see if you made new
ones.

2. Use more to look at the contents of cachefiles/.  These are the cachefiles 
used by the main STAMP code stochmap.  They point to frames created by preproc.
Now create your own copies.  Open stamp*.pl and modify $FrDir to point to your
working directory.  Then run stamp*.pl.  Check to see if the cachefiles 
updated.

3. Now you are ready to run stochmap.  Run: run_stochmap_ex3.m from matlab.
You will create a bunch of .png file plots and a summary output map.mat file.

-------------------------------------------------------------------------------

4. The rest of these instructions are to measure biases assoiated with sigma.
Not everyone will find this exercize useful; it's mostly for the review.  To
perform a diagnostic test measuring the recovered strain power, run: 
test_power.m
