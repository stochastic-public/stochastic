Instructions

0. In this example we inject a loud Accretion Disk Instability (ADI) signal and
measure the isotropic equivalent energy associated with the waveform.  The 
injection file is M10a0.95eps0.2.dat.  This file represents an ADI signal from
a source at 1Mpc.  It is parameterized by astar=0.95 and epsilon=0.2.  The 
former is the dimensionless spin parameter and the latter is the fraction of 
disk mass that clumps.

1. Run: preproc('ex4_EGW_both.txt', 'S5H1L1_full_run.txt', 1)
   * note: we do not include pre-calculated frames/ for this example.
   Please generate them yourself.  You will have to make a directory called
   frames/ to do this.

2. Open stamp*.pl and modify $FrDir to point to your working directory.  Then 
run stamp*.pl.  Check to see if the cachefiles updated.

3. Now you are ready to run stochmap.  Run: run_stochmap_ex4_both.m from 
matlab.

-------------------------------------------------------------------------------

4. The rest of these instructions are to measure the energy associated with the
ADI waveform.  Run test_EGW.m.  This script will use the measured fluence
map.F to calculate the isotropic equivalent energy associated with this 
waveform.
