clear all;

load map.mat;
load radio_out/ex1_ccstatsSkySet1.job1.trial1.mat;

% crop maps to remove zeros at beginning and end of map
ymap = map.y(:,10:end-11);
sigmap = map.sigma(:,10:end-11);

% define w^4 coarse-graining normalization factor
N=4096;
window0 = hann(N);
norm_fac = ( mean(window0.^2)^2/mean(window0.^4) )^(1/2);

% scale sigmap by normalization factor
sigmap=sigmap/norm_fac;

% parse the radiometer data but do not include the final broadband bin
for ii=1:length(Sky)
  yrad(:,ii) = Sky{ii}.data(1:end-1,1);
  sigrad(:,ii) = Sky{ii}.data(1:end-1,2);
end

% radiometer frequencies
radfreqs = Sky{1}.deltaF*[0:Sky{1}.numFreqs-1] + Sky{1}.flow;

% crop radiometer frequencies to match STAMP ft-map
radcut = radfreqs>=map.yvals(1) & radfreqs<=map.yvals(end);
yrad = yrad(radcut,:);
sigrad = sigrad(radcut,:);

ydiff = abs(ymap-yrad)./ymap;
sdiff = abs(sigmap-sigrad)./sigmap;

fprintf('max deviation in y = %1.3e\n', max(ydiff(:)));
fprintf('avg deviation in y = %1.3e\n', mean(ydiff(:)));
fprintf('max deviation in sigma = %1.3e\n', max(sdiff(:)));
fprintf('avg deviation in sigma = %1.3e\n', mean(sdiff(:)));
