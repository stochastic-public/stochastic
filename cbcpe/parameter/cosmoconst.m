%define some cosmology constants.
H       = 73000;  % Hubble Constant in m/(s*Mpc) - note ther weird units.
rhoc    = (3*(H^2))/(8*pi*G);
Om_m    = 0.26;
Om_L    = 0.74;
fEE     = @(z_) sqrt(Om_m*((1+z_).^3) + Om_L);