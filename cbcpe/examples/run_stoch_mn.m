
analysisType = 'powerlaw';
dataSet = '2';
dataSet = '4post';
dataSet = '2';
dataSet = '2ideal';
%dataSet = '4ideal';
%dataSet = '3ideal';
%analysisType = 'powerlaw2';
%analysisType = 'DNS';

doRun = 0;
doRun = 1;

%stoch_mn(analysisType,dataSet,doRun);

dataSets = {'2ideal','3ideal','4ideal','5ideal'};
analysisTypes = {'powerlaw','powerlaw2','DNS','DNS2'};
%analysisTypes = {'powerlaw2'};
%analysisTypes = {'DNS2'};
analysisTypes = {'DNS2'};
dataSets = {'4ideal'};
dataSets = {'3ideal'};
analysisTypes = {'DBH'};
analysisTypes = {'DNSWide'};
%analysisTypes = {'powerlaw2fmax'};
analysisTypes = {'powerlaw'};
%analysisTypes = {'DNSWide'};
%dataSets = {'ET'};
dataSets = {'2ideal'};
dataSets = {'2','3','4','5'};
analysisTypes = {'powerlawPSD'};
%analysisTypes = {'powerlaw'};
%dataSets = {'none'};
%dataSets = {'none','2'};
%analysisTypes = {'DNS'};
%dataSets = {'5ideal'};
%dataSets = {'2ideal'};
dataSets = {'2','3','4'};
%dataSets = {'ETideal_noDetect'};
%dataSets = {'ETideal'};
%dataSets = {'ET','ET_noDetect'};
dataSets = {'4'};
%dataSets = {'2'};
analysisTypes = {'DNS2'};
%analysisTypes = {'DNS'};

for i = 1:length(analysisTypes)
   %for j = 4
   for j = 1:length(dataSets)
      analysisType = analysisTypes{i};
      dataSet = dataSets{j};
      stoch_mn(analysisType,dataSet,doRun);
   end
end

