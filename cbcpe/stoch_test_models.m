%this script is meant to study the stochastic sensitivity for the GW150914
%event

%input from CBC
Mchirp = 36;
LocalRate = 0.016;
LocalRateMin = 0.00065;
LocalRateMax = 0.076;

sfr = 'k';
waveform.form = 'power';
waveform.order = -1;
waveform.tmin = 0.1;
freq = 1:0.25:1000;

RVzero = BC_RatePerVol(sfr,waveform,0)*1e6;

omega = BBH_no_table(freq,Mchirp,LocalRate / RVzero,sfr,waveform,0);
domain = freq;

%repeat for dynamic BBH for GCs, using maximum <N> from Rodriguez paper
Gyr = 60 * 60 * 24 * 365 * 1e9; %in seconds
NN = 3100 / 12 / Gyr; %specify the average number of dynamic BBH per GC per 12 Gyr

omega3 = BBH_dynamic(freq',NN,1);
domain = freq;

baseplotDir = '/home/mcoughlin/Stochastic/MDC/PE/plots_test';

%plotDir = [baseplotDir '/' dataSet '/' analysisType];
plotDir = baseplotDir;
createpath(plotDir);

params.savePlots = 1;

if params.savePlots

      plotName = [plotDir '/omega'];
      figure;
      loglog(freq,omega,'k--');
      hold on
      loglog(freq,omega3,'b');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

end







