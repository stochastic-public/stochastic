function [ff,Y_J_par,sigma_J_par] = load_ptest(ptEstIntFile,sensIntFile,fRef,alphaExp);

data1 = load(ptEstIntFile); data2 = load(sensIntFile);

ff = data1(:,2);
deltaF = ff(2)-ff(1);
sigma_J = 1./sqrt(data2(:,3)*deltaF);
Y_J = 2*deltaF*sum(data2(:,3)*deltaF)*data1(:,3)./(data2(:,3)*deltaF);

% cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
var_J = sigma_J.^2;
cut = isnan(Y_J);
Y_J(cut) = 0;

Y_J_par = Y_J.*(ff/fRef).^alphaExp;
sigma_J_par = sigma_J.*(ff/fRef).^alphaExp;


