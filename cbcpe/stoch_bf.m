function mnPE(analysisType,dataSet,doRun);

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/MDC/PE/plots_bf';
plotDir = [baseplotDir '/' dataSet '/' analysisType];
createpath(plotDir);

[ff,omega_data,sigma_data,fmin,fmax] = get_data(analysisType,dataSet,plotDir);

det1 = getdetector('LHO');
det2 = getdetector('LLO');
gamma = overlapreductionfunction(ff,det1,det2);

psd = load(psdfile);
indexes = find(ff >= fmin & ff <= fmax);
ff = ff(indexes);
omega_data = omega_data(indexes);
%omega_psd_data = omega_psd_data(indexes);
sigma_data = sigma_data(indexes);
psd_data = interp1(psd(:,1),psd(:,2),ff);
psd_data_norm = psd_data./sum(psd_data);
psd_data_norm = ones(size(psd_data_norm));

YY = omega_data;
vv = sigma_data.^2;

[model,prior] = get_model(analysisType);
likelihood = @stoch_logL;

data = {};
data{1} = YY;
%vv = ones(size(YY));
data{2} = vv;
data{3} = ff;
data{4} = psd_data;

matFile = [plotDir '/bf.mat'];

if doRun

   parnames = prior(:,1);
   parvalsmin = cell2mat(prior(:,3));
   parvalsmax = cell2mat(prior(:,4));
   num = 100; 
   %num = 20;
   num1 = 100;
   num2 = 100;
   %num1 = 500;
   %num2 = 500;
 
   %for i = 1:length(parnames)
   %   prior{i,6} = linspace(parvalsmin(i),parvalsmax(i),num+i);
   %end

   prior{1,6} = linspace(parvalsmin(1),parvalsmax(1),num1);
   prior{2,6} = linspace(parvalsmin(2),parvalsmax(2),num2);

   lik = zeros(num1,num2);
   for i = 1:num1
      fprintf('%d\n',i);
      for j = 1:num2
         parvals = {prior{1,6}(i) prior{2,6}(j)};
         logL = stoch_logL(data, model, parnames, parvals);
         lik(i,j) = logL;
      end
   end

   save(matFile);
else
   %load(matFile);
   load(matFile,'prior','lik');
end

params.savePlots = 1;

if params.savePlots

   [C,I] = max(lik(:));
   [I1,I2] = ind2sub(size(lik),I);
   lik_max = lik(I1,I2);

   if strcmp(analysisType,'powerlaw');

      a = prior{1,6}(I1);
      k = prior{2,6}(I2);
      powerlaw_data = powerlaw(ff,a,k);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,powerlaw_data,'b');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(lik(:));
      fprintf('a: %.5f, k: %.5f, likelihood: %.5e\n',a,k,lik(index));

   elseif strcmp(analysisType,'powerlaw2');

      a = median(nest_samples(:,1));
      k = median(nest_samples(:,2));
      b = median(nest_samples(:,3));
      l = median(nest_samples(:,4));
      powerlaw_data_1 = powerlaw(ff,a,k);
      powerlaw_data_2 = powerlaw(ff,b,l);
      powerlaw_data = powerlaw2(ff,a,k,b,l);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,powerlaw_data,'b');
      loglog(ff,powerlaw_data_1,'r');
      loglog(ff,powerlaw_data_2,'g');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;
   elseif strcmp(analysisType,'DNS');
      M = prior{1,6}(I1);
      lam = prior{2,6}(I2);
      DNS_data = DNS(ff,M,lam);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,DNS_data,'b');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(lik(:));
      fprintf('M: %.5f, lam: %.5f, likelihood: %.5e\n',M,lam,lik(index));

   elseif strcmp(analysisType,'BBHNoTable');
      M = prior{1,6}(I1);
      lam = prior{2,6}(I2);

      global sfr;
      global waveform;
      global zinf;

      % calculate y-values
      DBHNoTable_data = BBH_no_table(ff',M,lam,sfr,waveform,zinf)';

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,DBHNoTable_data,'b');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(lik(:));
      fprintf('M: %.5f, lam: %.5f, likelihood: %.5e\n',M,lam,lik(index));

   elseif strcmp(analysisType,'BBHDynamic');
      NN = prior{1,6}(I1);
      flag = prior{2,6}(I2);

      % calculate y-values
      DBHDynamic_data = BBH_dynamic(ff',NN,flag)';

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,DBHDynamic_data,'b');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(lik(:));
      fprintf('NN: %.5f, flag: %.5f, likelihood: %.5e\n',NN,flag,lik(index));

   end

   xlab = get_name(prior{1,1});
   ylab = get_name(prior{2,1});

   %norm = max(lik(:));
   %lik = exp(lik - norm);

   vmin = min(lik(:));
   vmax = max(lik(:));

   vmax = max(lik(:)); 
   vmin = vmax + 100*vmax;

   [Xq, Yq] = meshgrid(prior{1,6},prior{2,6});
   Vq = lik';

   plotName = [plotDir '/loglikelihood'];
   figure()
   %pcolor(Xq,Yq,Vq);
   imagesc(prior{1,6},prior{2,6},Vq);
   set(gca,'YDir','normal');
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   caxis([vmin vmax]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   norm = max(lik(:));
   lik = exp(lik - norm);
   Vq = lik';

   plotName = [plotDir '/likelihood'];
   figure()
   %pcolor(Xq,Yq,Vq);
   imagesc(prior{1,6},prior{2,6},Vq);
   set(gca,'YDir','normal');
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   %caxis([vmin vmax]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   %zmin = min(lik(:));
   %zmax = max(lik(:));
   %zvals = linspace(zmin,zmax,10000);
   %zvals = sort(lik(:),'ascend');
   %prob = 0:1/length(zvals):1;
   %zvals = 0:zmax/2000:zmax;
   %prob = 0:1/length(zvals):1;

   zmax = max(lik(:));
   zvals = 0:zmax/2000:zmax;
   znorm = sum(lik(:));
   for ll=1:length(zvals)
      prob(ll) = sum(sum(lik(lik>=zvals(ll))));
   end
   prob = prob/znorm;

   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD') || strcmp(analysisType,'DNS') || strcmp(analysisType,'DBH') || strcmp(analysisType,'BBHNoTable')
      prob = fliplr(prob);
   end
   zvals68 = zvals(abs(prob-0.68)==min(abs(prob-0.68)));
   zvals68 = zvals(abs(prob-0.32)==min(abs(prob-0.32)));
   zvals95 = zvals(abs(prob-0.05)==min(abs(prob-0.05)));
   zvals99 = zvals(abs(prob-0.01)==min(abs(prob-0.01)));

   zcontours = [zvals68(1) zvals95(1) zvals99(1)];
   %[C, h1] = contour(aa, epsilon, lik, zvals95);

   %zvals68 = zmax*(1+0.66);
   %zvals95 = zmax*(1+0.90);
   %zvals99 = zmax*(1+0.98);

   %zcontours = [zvals68(1) zvals95(1) zvals99(1)];
   %zcontours = [zvals95(1) zvals99(1)];
   %zcontours = zmax*5;

   plotName = [plotDir '/likecontour'];
   figure;
   [C, h] = contour(Xq, Yq, Vq, zcontours(1),'k');
   hold on
   [C, h] = contour(Xq, Yq, Vq, zcontours(2),'b');
   [C, h] = contour(Xq, Yq, Vq, zcontours(3),'r');
   hold off
   xlabel(xlab,'FontSize',24);
   ylabel(ylab,'FontSize',24);
   %'Interpreter','LaTex'
   %cbar = colorbar;
   %set(get(cbar,'ylabel'),'String','Probability');

   get_injplot(analysisType,dataSet);

   leg = legend('68 CL','95 CL','99 CL','True value','Location','northeast','Orientation','horizontal')
   %set(leg,'Interpreter','latex')
   %set(gca,'FontSize',20);

   pretty;
   hgsave(plotName);
   print('-dpdf',plotName);
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   matFile = [plotDir '/bf_contour.mat'];
   save(matFile,'Xq','Yq','Vq','zcontours');  

   param1 = C(1,:);
   param2 = C(2,:);

   if strcmp(analysisType,'powerlaw');

      param1 = param1(param1 < 0);
      param2 = param2(param2 < 2);

      fprintf('a: med: %.5f max: %.5f, min:%.5f\n',median(param1),max(param1),min(param1));
      fprintf('k: med: %.5f max: %.5f, min:%.5f\n',median(param2),max(param2),min(param2));
 
      fprintf('a: %.5e +- %.5e\n',10^median(param1),10^median(param1) * log(10) * abs((median(param1)-min(param1))/median(param1)));
      fprintf('k: %.5f +- %.5f\n',median(param2),median(param2)-min(param2));

 
   elseif strcmp(analysisType,'DNS');

      param2 = param2(param2 < 0);

      param2 = (10.^param2)*0.2e6;

      fprintf('M: med: %.5f max: %.5f, min:%.5f\n',median(param1),max(param1),min(param1));
      %fprintf('lamda: med: %.5e max: %.5e, min:%.5e\n',10^median(param2),10^max(param2),10^min(param2));
      fprintf('lamda: med: %.5e max: %.5e, min:%.5e\n',median(param2),max(param2),min(param2));

   end

end


