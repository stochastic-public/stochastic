function mnPE(dataSet);

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/MDC/PE/plots_gendata';
plotDir = [baseplotDir '/' dataSet ];
createpath(plotDir);

frameDir = [baseplotDir '/' dataSet '/frames'];
createpath(frameDir);

if strcmp(dataSet,'2ideal')
   omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_2_BNS_nonDet/omega.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   omega_psd_data = omega(:,3);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 400;

   %omega_psd_data = omega_psd_data * 1e2;
   % omega_psd_data = omega_psd_data * 50 / 1.1;

   % SNR: 100,000
   omega_psd_data = omega_psd_data * 1e4;
   %omega_psd_data = omega_psd_data * 1e10 * 50 / 1.1;
   %omega_psd_data = omega_psd_data * 100000 * 50 / 1.1;
   %omega_psd_data = omega_psd_data * 100 * 50 / 1.1;
   %omega_psd_data = omega_psd_data * 50 / 1.1;

elseif strcmp(dataSet,'2ideal_mid')
   omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_2_BNS_nonDet/omega.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   omega_psd_data = omega(:,3);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 400;

   %SNR: 1000
   omega_psd_data = omega_psd_data * 1e2;

elseif strcmp(dataSet,'2ideal_low')
   omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_2_BNS_nonDet/omega.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   omega_psd_data = omega(:,3);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 400;
  
   % SNR: 10
   omega_psd_data = omega_psd_data * 1e1;

elseif strcmp(dataSet,'3ideal')
   omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_3_BBH_nonDet/omega.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   omega_psd_data = omega(:,3);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 200;

   omega_psd_data = omega_psd_data * 1e10;

elseif strcmp(dataSet,'4ideal')
   omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_4_BNS_BBH_nonDet/omega_all.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   omega_psd_data = omega(:,3);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 300;

   omega_psd_data = omega_psd_data * 1e10;

elseif strcmp(dataSet,'5ideal')
   omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_5_BNS_BBH_IMBH_High_nonDet/omega_all.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   omega_psd_data = omega(:,3);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 300;

   plotName = [plotDir '/imbh.png'];
   figure;
   loglog(ff,omega_data,'k--');
   print('-dpng',plotName);
   close;

   omega_psd_data = omega_psd_data * 1e10;

elseif strcmp(dataSet,'none')
   ff = 1:1024;
   ff = ff';
   omega_data = zeros(size(ff));
   omega_psd_data = zeros(size(ff));
   sigma_data = zeros(size(ff));
   fmin = 10;
   fmax = 300;

elseif strcmp(dataSet,'noneflat')
   ff = 1:1024;
   ff = ff';
   omega_data = zeros(size(ff));
   omega_psd_data = zeros(size(ff));
   sigma_data = zeros(size(ff));
   fmin = 10;
   fmax = 300;
   
elseif strcmp(dataSet,'SB')
   ff = 1:1024;
   ff = ff';
   omega_data = zeros(size(ff));
   omega_psd_data = zeros(size(ff));
   sigma_data = zeros(size(ff));
   fmin = 10;
   fmax = 300;
end

if strcmp(dataSet,'SB')
   doSimSB = 1;
else
   doSimSB = 0;
end

psd = load(psdfile);
indexes = find(ff >= fmin & ff <= fmax);
ff = ff(indexes);
omega_data = omega_data(indexes);
omega_psd_data = omega_psd_data(indexes);
sigma_data = sigma_data(indexes);
psd_data = interp1(psd(:,1),psd(:,2).^2,ff);
psd_data_norm = psd_data./sum(psd_data);
psd_data_norm = ones(size(psd_data_norm));

if strcmp(dataSet,'noneflat')
   psd_data = psd_data(1) * ones(size(ff));
elseif strcmp(dataSet,'SB')
   psd_data = zeros(size(ff));
end

det1 = getdetector('LHO');
det2 = getdetector('LLO');
gamma = overlapreductionfunction(ff,det1,det2);

h0 = 0.7;

plotName = [plotDir '/omega'];
figure;
loglog(ff,omega_data,'k--');
hold on
%loglog(ff,omega_psd_data,'b');
loglog(ff,powerlaw(ff,-9,2/3),'m--');
%loglog(ff,3.95364e-37*(h0/0.704)*(h0/0.704)*(1./ff.^3).*omega_data,'r');
hold off
xlabel('Frequency [Hz]');
ylabel('\Omega_{GW}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/omega_psd'];
figure;
loglog(ff,omega_psd_data,'k--');
hold on
%loglog(ff,powerlaw(ff,-41,(2/3)-3),'m--');
%loglog(ff,powerlaw(ff,-42.6,(2/3)-3),'m--'); % LOW
%loglog(ff,powerlaw(ff,-41.6,(2/3)-3),'m--'); % MID
loglog(ff,powerlaw(ff,-41.6,(2/3)-3),'m--'); % HIGH
%loglog(ff,3.95364e-37*(h0/0.704)*(h0/0.704)*(1./ff.^3).*omega_data,'r');
hold off
xlabel('Frequency [Hz]');
ylabel('\Omega_{GW}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

%omega_psd_data = powerlaw(ff,6 + -4.4 + -35.6,(2/3)-3);

%omega_psd_data = powerlaw(ff,-35,(2/3)-3);

sampleRate = 512;
duration = 20480;
N = sampleRate * duration;
h = gaussian_noise([ff omega_psd_data], sampleRate, duration);

if doSimSB
   [h1, h2] = stoch_simulateSB(0, 1/sampleRate, 1/sampleRate, N, N,...
                               'const', det1, det2, ...
                               1, 1, 0, 0,...
                               0, 0, NaN, NaN, 1);
   h1 = h1.data; h2 = h2.data;
   h = h1;
else
   h1 = h;
   h2 = h;
end

n1 = gaussian_noise([ff psd_data], sampleRate, duration);
n2 = gaussian_noise([ff psd_data], sampleRate, duration);
t = (0:length(h)-1)/sampleRate;

%h = zeros(size(h));
h = h;
s1 = n1+h1; s2 = n2+h2;

L = length(t);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
f = sampleRate/2*linspace(0,1,NFFT/2+1);

%s1 = fft(s1,NFFT); s2 = fft(s2,NFFT);
%s1 = 2*s1(1:NFFT/2+1); s2 = 2*s2(1:NFFT/2+1);

n1 = fft(n1,NFFT); n2 = fft(n2,NFFT);
n1 = n1(1:NFFT/2+1); n2 = n2(1:NFFT/2+1);

h = fft(h.*hann(length(h)),NFFT);
h = h(1:NFFT/2+1);

windowconst = 0.375;
h_psd = 2*(abs(h).^2)/(L*sampleRate*windowconst);

indexes = find(f >= 10 & f<=256);

f = f(indexes);
h = h(indexes);
n1 = n1(indexes);
n2 = n2(indexes);
h_psd = h_psd(indexes);

plotName = [plotDir '/fft'];
figure;
loglog(f,abs(h),'k');
hold on
loglog(f,abs(n1),'b');
loglog(f,abs(n2),'r');
%loglog(f,powerlaw(f,-3,((2/3)-3)/2),'m--');
hold off
xlabel('Frequency [Hz]');
ylabel('FFT');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/psd'];
figure;
loglog(f,abs(h_psd),'k');
hold on
loglog(ff,omega_psd_data,'b');
loglog(ff,psd_data,'r');
hold off
xlim([10 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('FFT');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

%Y = abs(sum(h));
%sigma = abs(sum(n1));
%fprintf('Y: %.5e, sigma: %.5e, SNR: %.5f\n',Y,sigma,Y/sigma);

indexes = find(ff >= 10 & 256 >= ff);
Y = sum(h_psd(indexes));
sigma = sum(psd_data(indexes));
fprintf('Y: %.5e, sigma: %.5e, SNR: %.5f\n',Y,sigma,Y/sigma);

gpsStart = 900000000;
frameLength = duration;
mode = 'n';
data = [];
data.type = 'd';
data.mode = 'a';

file = sprintf('%s/%s/frames/H1-%d-%d.gwf',baseplotDir,dataSet,gpsStart,duration);
data.channel = 'H1:STRAIN';
data.data = s1;
mkframe(file, data, mode, frameLength, gpsStart);

file = sprintf('%s/%s/frames/L1-%d-%d.gwf',baseplotDir,dataSet,gpsStart,duration);
data.channel = 'L1:STRAIN';
data.data = s2;
mkframe(file, data, mode, frameLength, gpsStart);

file = sprintf('%s/%s/frames/H2-%d-%d.gwf',baseplotDir,dataSet,gpsStart,duration);
data.channel = 'H2:STRAIN';
data.data = s2;
mkframe(file, data, mode, frameLength, gpsStart);

file = sprintf('%s/%s/frames/V1-%d-%d.gwf',baseplotDir,dataSet,gpsStart,duration);
data.channel = 'V1:STRAIN';
data.data = s2;
mkframe(file, data, mode, frameLength, gpsStart);

%S1 = interp1(psd(:,1),psd(:,2),f)'; S2 = interp1(psd(:,1),psd(:,2),f)';
%N = sampleRate*duration;
%indexes = find(f >= 10 & f<=256);

%f = f(indexes);
%S1 = S1(indexes);
%S2 = S2(indexes);
%s1 = s1(indexes);
%s2 = s2(indexes);

%gamma = overlapreductionfunction(f,det1,det2)';
%Q = N*gamma./((f').^3 .* S1 .* S2);
%Q(1) = 0; Q(end) = 0;
%Y = sum(conj(s1).*s2.*Q);
%sigma = sqrt((1/2) * sum(S1.^2.*S2.^2.*abs(Q.^2)));

%fprintf('Y: %.5e, sigma: %.5e, SNR: %.5f\n',Y,sigma,Y/sigma);

