function Omega = Dual_BH (f,mc,lambda)

global f_sfr;
global r_sfr;

lambda = 10^lambda;

constant;
sfr = 'h';

Kb = (G*pi)^(2/3)*(mc*Msolar)^(5/3)/3;

Const = (8*pi*G)/(3*c^2*H0^3)*lambda*Kb/yr/Mpc^3;
 
r = mc/8.7; % ratio of the chirp mass and 8.7 solar mass

fgmin = 0;

v1 = 404/r; %  v1= 404 for mc = 8.7 solar mass
v2 = 807/r;
sigma = 237/r;
fgmax = 1153/r; % per second
zmax = 6;    % dimensionless --max of the redshift

% normalizer of delay time distribution
tmin = 0.1;

%disp ('*************************************************')
%disp ('Beginning BBH Calculation')

Omega = zeros (1,length(f));

indexes = ~((f >=  fgmin) & (f<= fgmax));
zsups = (fgmax./f) - 1;
zsups(zsups>zmax) = zmax;

v = zeros(size(f));
indexes1 = find(f < v1);
v(indexes1) = f(indexes1).^(-1/3);
indexes1 = find(f >= v1 & f<=v2);
v(indexes1) = (f(indexes1).^(2/3))/v1;
indexes1 = find(f > v2);
v(indexes1) = f(indexes1).^2./(1+ 4*(f(indexes1)-v2).^2./sigma.^2).^2/v1/v2^(4/3);

%Omega = Const*f.*v.*BC_integrand(tmin,zsups,sfr);
val = interp1(r_sfr,f_sfr,zsups,'linear','extrap');
Omega = Const*f.*v.*val;
Omega(indexes) = 0;

end
