function y = powerlaw2fmax(freq,a,k,fmax1,b,l,fmax2);

x = freq;
% calculate y-values
y = powerlawfmax(freq,a,k,fmax1) + powerlawfmax(freq,b,l,fmax2);

