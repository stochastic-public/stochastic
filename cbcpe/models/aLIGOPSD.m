function y = aLIGOPSD(freq,a,k);

x = freq;

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';
psd = load(psdfile);
psd_data = interp1(psd(:,1),psd(:,2),x);

% calculate y-values
y = (10.^a)*(psd_data.*(x.^k));

