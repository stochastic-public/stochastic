function simulateData(noiseCurve,seed)
% function simulateData NOISECURVE SEED
%		input: SEED to the random number generator
%		saves output file ogw_measured_SEED.dat.  The format of this file is:
%       freq Omega(f) sigma(f) Omega_true(f)

%==============================================================================
% SIMULATE A MEASURED FRACTIONAL ENERGY DENSITY AND ITS STANDARD DEVIATION
%==============================================================================

% GET ARGUMENTS
seed = strassign(seed);

%------------------------------------------------------------------------------
% USER INPUT
%------------------------------------------------------------------------------
cosmoParams.H0=70;
cosmoParams.Om=0.3;
cosmoParams.Ol=0.6;
cosmoParams.Or=0.0;
cosmoParams.Ok=0.0;
sfrParams.r0=5.0e-12;
sfrParams.W=45.0;
sfrParams.R=3.8;
sfrParams.Q=3.4;
massFuncParams.lambda_bbh = 1*2.2e-3; % []
massFuncParams.mu_bbh = 6.72; % [Msun]
massFuncParams.sigma_bbh = 0.05; % [Msun]
massFuncParams.lambda_bns = 1*2.2e-3; % []
massFuncParams.mu_bns = 1.05; % [Msun]
massFuncParams.sigma_bns = 0.05; % [Msun]

%T = 3.15569e7; % observation time
T = 3.15569e10; % observation time (1000 years for a very loud signal!)
H0_SI = cosmoParams.H0*1000/1e22;

%------------------------------------------------------------------------------
% CALCULATE MEASURED SPECTRUM AND STDEV
%------------------------------------------------------------------------------

psdData = dlmread(noiseCurve); % load strain spectral density
f = psdData(:,1)';
P = psdData(:,2)';
df = f(2)-f(1);

% CALCULATE THE TRUE SPECTRUM
omega_GW_true = omegaGW(f,6.0,sfrParams, cosmoParams,massFuncParams);
% CALCULATE THE STDEV
% EHT: fixed typo
%gamma=abs(overlapreductionfunction(f,getdetector('LLO'),getdetector('H'))); % calculate overlap reduction function
gamma=abs(overlapreductionfunction(f,getdetector('LLO'),getdetector('LHO'))); % calculate overlap reduction function
sigma_GW = (P.^2./((1/5)*gamma)).*(f.^3)*(2.*(pi^2))/(3*(H0_SI^2))*sqrt(1./(T*df)); % calculate stdev

% CALCULATE MEASURED SPECTRUM
omega_GW_hat = randn(size(f)).*sigma_GW + omega_GW_true;

% SAVE DATA TO FILE
% EHT: not the way to save arrays
%save(['ogw_measured_' num2str(seed) '.dat'],'f','omega_GW_hat','sigma_GW','omega_GW_true', '-ascii');
out = [f' omega_GW_hat' sigma_GW' omega_GW_true'];
save(['ogw_measured_' num2str(seed) '.dat'], 'out', '-ASCII');

end
