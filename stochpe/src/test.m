% TEST FUNCTION

clc;
clear;
addpath('./cosmology')
addpath('./sfr')

cosmoParams.H0=70;
cosmoParams.Om=0.3;
cosmoParams.Ol=0.6;
cosmoParams.Or=0.0;
cosmoParams.Ok=0.0;
sfrParams.r0=5.0e-12;
sfrParams.W=45.0;
sfrParams.R=3.8;
sfrParams.Q=3.4;
massFuncParams.lambda_bbh = 1*2.2e-3; % []
massFuncParams.mu_bbh = 6.72; % [Msun]
massFuncParams.sigma_bbh = 0.05; % [Msun]
massFuncParams.lambda_bns = 1*2.2e-3; % []
massFuncParams.mu_bns = 1.05; % [Msun]
massFuncParams.sigma_bns = 0.05; % [Msun]


data = dlmread('ogw_measured.dat');
f = data(1,:);
ogw = omegaGW(f,6.0,sfrParams, cosmoParams,massFuncParams);
ogw_measured = data(2,:);
sgw_measured = data(3,:);

logL = likelihood(ogw, ogw_measured, sgw_measured);

%f = logspace(1,4,50);
%for n = 1:length(f)
	%Ogw(n) = fractionalEnergyDensity(f(n),6.0,sfrParams, cosmoParams, massFuncParams);
%end

%z = linspace(0,1,100);

%starFormationRateDensityObserverFrame(z,sfrParams,cosmoParams)

%m = linspace(0,20,100);

%chirpMassProbability(m,massFuncParams)

%[M,Z] = meshgrid(m,z);

%I = starFormationRateDensityObserverFrame(Z,sfrParams,cosmoParams).*chirpMassProbability(M,massFuncParams);
%sum(I(:))

