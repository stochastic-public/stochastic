function dR_dz = differentialStarFormationRate(z,StarFormationRateParameters,CosmoParams);
% CONVERTING THE STAR FORMATION RATE DENSITY INTO A DIFFERENTIAL STAR FORMATION
% RATE, IE DR/DZ, WHERE R IS THE STAR FORMATION RATE

dVc_dz = comovingVolumeElement(z,CosmoParams);
sfrd = starFormationRateDensityObserverFrame(z,StarFormationRateParameters,CosmoParams);

dR_dz = dVc_dz*sfrd;
