function [Egwf fCut] = singleMergerEnergySpectrum(mc,f,MassFuncParams)
% calculates dE/df up to integration over chirp mass (usually done in another step)
% uses AJITH et. al 2008 for merger, ringdown, cut-off values
% see ZHU et. al 2011, Mandic et al. 2012 for dE/df value.
% -------------------------------------

%-----------------
% CONSTANTS
% see Ajith et. al. 2008
% Table I for 
% parameters for frequency
% calculations.
%-----------------
%
G = 6.67e-11; % [mks]
Msun = 1.99e30;% [kg]

mcs = mc*5e-6; % mass in units of seconds
eta = 0.25; % equal masses for now

sourceClass = MassFuncParams.class;

switch sourceClass
case 'BBH'
	a0 = 2.9740e-1; % merger
	b0 = 4.4810e-2;
	c0 = 9.5560e-2;

	a1 = 5.9411e-1; % ringdown
	b1 = 8.9794e-2;
	c1 = 1.9111e-1;

	a2 = 5.0801e-1; % sigma
	b2 = 7.7515e-2;
	c2 = 2.2369e-2;

	a3 = 8.4845e-1; % cutoff
	b3 = 1.2848e-1;
	c3 = 2.7299e-1;

	%--------------------------------------
	% Calculate frequency cut-offs between
	% different phases of binary using parameters
	% from Ajith et. al. Table I
	% -------------------------------------

	% maskMC = mc > 5; % sets cut off for Inspiral -> IMR
	fMerger = (a0*eta^2+b0*eta+c0)./(pi*mcs.*2.^(6/5)); 
	mask1 = f < fMerger;
	fRing = (a1*eta^2+b1*eta+c1)./(pi*mcs.*2.^(6/5)); 
	mask2 = ~mask1.*(f<fRing);%.*maskMC; 
	sigma = (a2*eta^2+b2*eta+c2)./(pi*mcs.*2.^(6/5)); 
	fCut = (a3*eta^2+b3*eta+c3)./(pi*mcs.*2.^(6/5)); 
	mask3 = ~mask1.*~mask2.*(f<fCut);%.*maskMC; 

	% See Zhu et. al. 2011 equation 5
	freqContent = f.^(-1/3).*mask1 + f.^(2/3).*fMerger.^-1.*mask2 ...
								+ (f./(1+((f - fRing)./(sigma/2)).^2)).^2.*fMerger.^-1.*fRing.^(-4/3).*mask3;
case 'BNS'
	fCut = 1.0./(6.0^(1.5)*pi*mcs*0.25^(-0.6));
	mask1 = f < fCut;
	freqContent = f.^(-1/3).*mask1;
end
            
% put it all together now
Egwf = (G*pi)^(2/3)/3.*chirpMassProbability(mc,MassFuncParams).*mc.^(5/3).*freqContent;
