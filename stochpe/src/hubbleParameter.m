function Ez=hubbleParameter(z,CosmoParams);
% CALCULATING THE HUBBLE PARAMETER
% SEE HOGG (ARXIV:ASTRO-PH/9905116) EQ.(14)

H0 = CosmoParams.H0; % [km/s/Mpc]
Om = CosmoParams.Om;
Ol = CosmoParams.Ol;
Or = CosmoParams.Or;
Ok = 1.-Om-Ol-Or;

Ez = sqrt(Or*(1+z).^4 + Om*(1+z).^3 + Ok*(1+z).^2 + Ol);
