function pMc = chirpMassProbability(mc,MassFuncParams);

% NB: IF THE DISTRIBUTION FOR INDIVIDUAL COMPONENT MASSES ARE THOUGHT TO BE
% GAUSSIAN, THEN THE CHIRP MASS DISTRIBUTION IS NOT A GAUSSIAN

% CALCULATE THE CHIRPMASS DISTRIBUTION AS THE SUM OF THE TWO GAUSSIANS
lambda = MassFuncParams.lambda;
mu = MassFuncParams.mu;
sigma = MassFuncParams.sigma;
pMc = lambda*normpdf(mc,mu,sigma);

end
