function stoch_out = test_clustermap(mode)
% function stoch_out = test_clustermap(mode)
% This is an example wrapper for clustermap.
% updated by Eric Thrane on Feb 13, 2014
%
% mode = run clustermap with different search options
%      1 (default) = deep stochtrack running on 4 GPUs
%      2 = default stochtrack with 4 CPUs
%      3 = default stochtrack with 1 CPU
%      4 = default stochtrack with 1 CPU
%      5 = burstegard

% mode allows you to run with different parameters.  the default value is 1.
try
  mode;
catch
  mode=1;
end

% output file prefix: the wrapper will save the results in ['./' str '.mat']
str ='test';

% initialize stoch_out struct
stoch_out.start=true;

% initialize default parameters
params = stampDefaults;

% jobfile
params.jobsFile = './mcjobs.txt';

% if available, then the location of the files
params.inmats = './input/map';

% search direction
% right ascenion in hours
params.ra = 6.40;
% declination in degrees
params.dec = -39.30;

% frequency range
params.fmin = 100;
params.fmax = 250;

% apply notches
params.doStampFreqMask=true;
params.StampFreqsToRemove = [119 120 121];

% different run modes may be requested by the user
if mode==1
  fprintf('mode: deep stochtrack with 4 GPUs\n');
  params = stochtrackDefaults(params);
  params.stochtrack.T = 10000;
  params.stochtrack.F = 2000;
  params.stochtrack.mindur = 200;
  params.doGPU = true;
  params.doParallel = true;
elseif mode==2
  fprintf('mode: default stochtrack with 4 CPUs\n');
  params = stochtrackDefaults(params);
  params.stochtrack.T = 10000;
  params.stochtrack.F = 200;
  params.stochtrack.mindur = 200;
  params.doParallel = true;
elseif mode==3
  fprintf('mode: default stochtrack with 1 GPU\n');
  params = stochtrackDefaults(params);
  params.stochtrack.T = 10000;
  params.stochtrack.F = 200;
  params.stochtrack.mindur = 200;
  params.doGPU = true;
elseif mode==4
  fprintf('mode: default stochtrack with 1 CPU\n');
  params = stochtrackDefaults(params);
  params.stochtrack.T = 10000;
  params.stochtrack.F = 200;
  params.stochtrack.mindur = 200;
elseif mode==5
  % perform a clustering search search using the fast, seed-based burstegard 
  % algorithm
  fprintf('mode: burstegard\n');
  params = burstegardDefaults(params);
  params.burstegard.rr = 50;
  params.findtrack = true;
else
  error('unrecognized mode');
end

% saving plots
params.savePlots = true;



% need to set params.stampinj to false for some reason...
params.stampinj = false;

% do not apply the glitch identification cut
params.glitch.doCut = false;

% call clustermap
stoch_out=clustermap(params, 1000000000, 1000000300);

% turn off automatic saving of .mat files.
params.saveMat = false; 
% save output files manually (see test.mat)
save(['./' str '.mat'], 'stoch_out');
