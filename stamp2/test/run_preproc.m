function run_preproc
% function run_preproc
% create stamp intermediate data with preproc

% define paramfile
paramfile = 'aligo_params.txt';

% define jobfile
jobfile = 'mcjobs.txt';

% run preproc on the first job of the jobfile
preproc(paramfile, jobfile, 1);
