###############################################################################
DOCUMENTATION FOR STAMP 2
###############################################################################
Thank you for downloading STAMP.  If you have questions about this tagged 
version please contact Eric Thrane (ethrane@physics.umn.edu).

1. GETTING STARTED-------------------------------------------------------------
If you are reading this documentation, you have probably already checked out a
tagged copy of STAMP from the matapps svn:

       svn --username albert.einstein co https://ligo-vcs.phys.uwm.edu/svn/matapps/packages/stochastic/tags/stamp2_v$VERSION stamp2_v$VERSION

The STAMP team recommends performing this checkout on 
ldas-grid.ligo.caltech.edu (or) ldas-pcdev1.ligo.caltech.edu.  These servers 
has been trouble-free when it comes to doing svn commands, and this is where 
STAMP development takes place.

STAMP is a matlab-based package of code.  In order to ensure that you are using
the same copy of matlab as the developers, the STAMP team recommends that you
work on ldas-grid.ligo.caltech.edu or ldas-pcdev1.ligo.caltech.edu.  

Additionally, you must let matlab know about the path to the functions you will
be calling with STAMP.  This is done with a startup.m file.  Most developers
keep their startup.m file in their home directory where it is run automatically
when they open a new matlab session.  This tagged version of STAMP includes an
example startup.m file called stamp2_paths.m.  You can run this function at the
beginning of every matlab session to set your paths.  Or you can copy the
relevant lines into your own startup.m file.  

***Please be sure to edit/check the stamp_install variable to point to the 
correct path.***  By default, it is set to pwd (the current working directory),
which means that ex_startup.m has to be run in the stamp2_v*/ directory.  An
alternative is to hard-code the stamp_install variable.  Both options are fine.

Now you are ready to use STAMP!

2. TESTING---------------------------------------------------------------------
A test script has been included with this tagged copy of STAMP so that you can
run STAMP code and verify that it works.  To run the STAMP test code,
    i.) open a matlab session
    ii.) set paths with startup script
    iii.) cd to the test/ directory
    iv.) from matlab, execute the command: test_clustermap
This command will analyze a small chunk of data (which is included in your 
tagged STAMP download).  It will create diagnostic plots in .eps and .png 
format.
    v.) to run the preprocessing scripts preproc and clustermap, you can run
    the code run_preproc.  This code must be run on the ldas machines since it
    points to cachefiles in ~ethrane/ that cannot be easily moved from machine
    to machine.

3. DOCUMENTATION---------------------------------------------------------------
Documentation for STAMP is available here:
              https://wiki.ligo.org/Main/ReviewPage
              https://wiki.ligo.org/Main/StampProject
