function [source] = getskylocation(params)
% function [source] = getskylocation(params)
% Eric Thrane

% generate a list of search directions in the area around the source 
source = brutepixel(params);
if params.fastring
  [det1 det2] = ifo2dets(params);
  [source_start,phaseR_start] = fastring(det1, det2, params.startGPS, ...
    source, params);
  [source_end,phaseR_end] = fastring(det1, det2, params.endGPS, ...
    source, params);
  source = cat(1,source_start,source_end);
  all_phases = cat(1,phaseR_start,phaseR_end);
  [dummy,idx] = unique(all_phases);
  source = source(idx,:);
end
fprintf('looping over %1.0f sky directions\n', size(source,1));

return
