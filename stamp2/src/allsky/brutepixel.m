function source = brutepixel(params)
% function source = brutepixel(params)
% E. Thrane, T. Prestegard
% params is the parameter struct.
% params.thetamax is the maximum angular separation in degrees.
% params.dtheta is the grid spacing in degrees.
%
% Returns an array of (ra, dec) values that fall inside a search
% cone centered on params.ra and params.dec with a radius of 
% thetamax (degrees) and an angular separation of dtheta (degrees).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Shorthand variable name.
dtheta = params.dtheta;

% Factor for deg. -> rad. conversion.
k = pi/180;

% Initial search direction.
ra0 = params.ra*15; % Convert ra from hours to degrees.
dec0 = params.dec;

% Define a unit vector in the original search direction.
r0 = [-cos(k*dec0)*sin(k*ra0) sin(k*dec0)*sin(k*ra0) cos(k*ra0)];

% Make vector of search directions.
temp_source = combvec((-90:dtheta:90),(0:dtheta:(360-dtheta)))';
ras = temp_source(:,2);
decs = temp_source(:,1);

% Define unit vectors in all search directions.
r = [-sin(k*ras).*cos(k*decs) sin(k*ras).*sin(k*decs) cos(k*ras)];

% Find angular separation of all search directions.
r0 = repmat(r0,size(temp_source,1),1);
angsep = acos(sum((r.*r0),2));

% Store directions that are within 'thetamax' of the original
% search direction.
keep = (angsep <= (params.thetamax*k));
source(:,1) = temp_source(keep,2)/15;
source(:,2) = temp_source(keep,1);

return
