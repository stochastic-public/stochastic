function [t,hp,hc] = cw(f0,fdot,distance,fs);

 dt = 1/fs;
 endt=5000;
 T = endt;     
       t=0:dt:endt;
       f = f0 + fdot*t;

        % ---- Required parameters.
        h_rss = 1/distance;

        % ---- Optional parameters.
        alpha = 0;
        delta = 0;
        iota = 0;

        % ---- Waveform.
        h = 2^0.5*h_rss*sin(2*pi*t.*f);
        hp = 1/2*(1+(cos(iota))^2) * real(h);
        hc = cos(iota) * imag(h);

   taper = linspace(0,1-dt,fs);
   mask = ones(size(t));
   mask(1:fs)=sin(pi/2.*taper).^2;
   mask(end-fs+1:end) = fliplr(mask(1:fs));
   
    hp = hp .* mask;
    hc = hc .* mask;


