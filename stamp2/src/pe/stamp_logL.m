function logL = stamp_logL(data, model, parnames, parvals)

% logL = logL_gaussian(data, model, parnames, parvals)
%
% This function will compute the log likelihood of a multivariate
% gaussian:
%
%     L = 1/sqrt((2 pi)^N det C)
%         exp[-0.5*(y - model(x,params))^T * inv(C) * (y - model(x,params))]
%
% The input parameters are:
%     data - a cell array with three columns
%            { x values, y values, C: covariance matrix }
%     NOTE: if C is a single number, convert to a diag covariance matrix
%     model - the function handle for the signal model.
%     parnames - a cell array listing the names of the model parameters
%     parvals - a cell array containing the values of the parameters given
%         in parnames. These must be in the same order as in parnames. 
%         If parvals is an empty vector the noise-only likelihood will be 
%         calculated.
%
% -------------------------------------------------------------------------
%           This is the format required by nested_sampler.m.
% -------------------------------------------------------------------------

% check whether model is a string or function handle
if ischar(model)
    fmodel = str2func(model);
elseif isa(model, 'function_handle')
    fmodel = model;
else
    error('Error... Expecting a model function!');
end

% get data values from cell array
map = data{1};

% evaluate the model
if isempty(parvals)
    % if parvals is not defined get the null likelihood (noise model
    % likelihood)
    mask = zeros(size(map.y));
else
    mask = feval(fmodel, map, parnames, parvals);
    
    % if the model returns a NaN then set the likelihood to be zero (e.g. 
    % loglikelihood to be -inf
    if isnan(mask)
        logL = -inf;
        return;
    end
end

% calculate the log likelihood
mask = map.snr - (mask ./ map.sigma);

snr_Ls = stamp_snr_dist(map.snr(:));
mask_Ls = stamp_snr_dist(mask(:));

snr_logLs = log(snr_Ls); mask_logLs = log(mask_Ls);

logL = sum(mask_logLs) - sum(snr_logLs);

if isnan(logL)
    error('Error: log likelihood is NaN!');
end

if logL < -1e300
   logL = -1e300;
end

return
