function [map, params, pp] = loadmats(matlist, params)
% function map = loadmats(matlist, params)
% E. Thrane: fills intermediate data in the map struct by reading in mat files
% Also, important metadata is gleaned from the mat files.
% Modifications by Michael Coughlin.

% construct the path to the files from the file prefix parameter
[temp path] = regexp(params.inmats, '(.*)/.*', 'match', 'tokens');
path = path{1};
fprintf('loading data...');

for ii=1:length(matlist)

  % load the data
  M=load(matlist{ii});

  % check that the mat files include the requested band
  if params.fmin < M.params.flow || params.fmax > M.params.fhigh
    error('Requested frequencies out of range of mat files.');
  end

  % use the first mat file to get information about the data
  if ii==1
    % record metadata
    params.deltaF = M.params.deltaF;
    params.segmentDuration = M.params.segmentDuration;
    params.ifo1 = M.params.ifo1;
    params.ifo2 = M.params.ifo2;
    params.numSegmentsPerInterval = M.params.numSegmentsPerInterval;
    params.bufferSecs1 = M.params.bufferSecs1; 
    params.doOverlap = M.params.doOverlap;
    pp = M.pp;
    % initialize new ft-maps with template
    map.segDur = M.map.segDur;
    map.start = M.map.segstarttime(1);
    map.f = params.fmin:params.deltaF:params.fmax;
    map.segstarttime =  params.startGPS:params.segmentDuration/2: ...
      params.endGPS - params.segmentDuration/2;
    T = NaN*ones(length(map.f), length(map.segstarttime));
    map.cc = T;
    map.sensInt = T;
    map.ccVar = NaN*ones(1, length(map.segstarttime));
    map.naiP1 = T;
    map.naiP2 = T;   
    map.P1 = T;
    map.P2 = T;
  end

  % find relevant time indices from new and old ft-maps
  [C, new_t, old_t] = intersect(map.segstarttime, M.map.segstarttime);
  % find relevant frequency indices from new and old ft-maps
  [C, new_f, old_f] = intersect(map.f, M.map.f);
  % check that at least some data is available
  if length(old_f)==0 || length(old_t)==0
    error('Possible timing/frequency mismatch: no data available.');
  end
  % fill new maps;
  map.cc(new_f, new_t) = M.map.cc(old_f, old_t);
  map.sensInt(new_f, new_t) = M.map.sensInt(old_f, old_t);
  map.ccVar(new_t) = M.map.ccVar(old_t);
  map.naiP1(new_f, new_t) = M.map.naiP1(old_f, old_t);
  map.naiP2(new_f, new_t) = M.map.naiP2(old_f, old_t);
  map.P1(new_f, new_t) = M.map.P1(old_f, old_t);
  map.P2(new_f, new_t) = M.map.P2(old_f, old_t);
end
fprintf('done.\n');

return;
