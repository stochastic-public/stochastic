function [path,delta,psi,score,power] = viterbi_colFLT(binnum, obslik)
% VITERBI_COLFLT Find the most-probable (Viterbi) path 
%                through the HMM states (frequency bins).
% 
% I/O Spec
% ========
% Input:
%     binnum     - Max number of bins the sigal could move in each step
%     obslik     - Observation likelihood, e.g. Fstat
%
% Output:
%     path(t)    - The optimal path, where path(1), path(2), ..., path(T) stands  
%                  for the frequency bin index for each step.
%     delta(j,t) - Max probability of the sequence till step t-1 
%                  and then go to state j
%     psi(j,t)   - The best predecessor state, given that it ends up in state j at t
%     score      - Number of standard deviations above the mean value of log 
%                  likelihood of paths to all the states at the final step   
%     power      - Total power of the optimal path 
%
% Authors: L. Sun and the Uni. Melbourne group (2018)

% If binnum is even, increase binnum by 1
if round(binnum/2) == binnum/2
    M = (binnum-1)*2;
else
    M = binnum*2-1;
end

% Q bins, T steps
[Q,T] = size(obslik);

path = zeros(1,T);
delta = zeros(Q,T);
psi = zeros(Q,T);

% Using Matlab function colfilt, which rearranges each Mx1 block
% of delta into a column of a temporary matrix and then applies
% the max or argmax function to this matrix. This function actually
% compares all three possible paths for each step and the operation
% is efficient.

% Set correction term for each bin
cor = (0:Q-1)'; 

% Initialisation 
t = 1;
delta(:,t) = obslik(:,t);

% Forward - Recursion and Termination
for t=2:T
    delta(:,t) = colfilt(delta(:,t-1),[M 1],'sliding',@pmax) + obslik(:,t);
    psi(:,t) = colfilt(delta(:,t-1),[M 1],'sliding',@pargmax) + cor;
end

% Optimal path backtracking
[power, path(T)] = max(delta(:,T));
score = (power-mean(delta(:,T)))/std(delta(:,T));
for t=T-1:-1:1
    path(t) = psi(path(t+1),t+1);
end
