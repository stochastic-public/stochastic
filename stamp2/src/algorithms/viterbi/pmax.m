function val = pmax(L)
% PMAX Find the optimal choices that maximizing L.*p'

[m,n]=size(L);

if round(m/2) == m/2
    p = [zeros(1,m/2-1) ones(1,m/2+1)];
else
    p = [zeros(1,(m-1)/2) ones(1,(m+1)/2)];
end

[val, ~] = max(L.*repmat(p',1,n));
