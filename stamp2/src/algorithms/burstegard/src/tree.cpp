#include <iostream>
#include <stdlib.h>
#include <cmath>
#include <iomanip>
#include <vector>
#include "cluster.h"

using namespace std;

// Declare functions from other .cpp files.
void seed_info(const double* y_vec, const double* sigma_vec,  const int x, const int y, Params params, vector<Seed> &seeds, vector<Row> &rows, int &nSeeds);
//double calc_dists(const Params params, const Seed seed1, const Seed seed2);
void cluster_pixels(const Params params, const vector<Seed> &seeds, vector<Row> &rows, vector<Cluster> &clusterList);
void analyze_clusters(const Params params, const int x, const int y, const vector<Seed> &seeds, vector<Cluster> &clusters, Cluster &maxCluster, double* output_max, double* output_all);

// PURPOSE:
// Cluster pixels in a size_x by size_y 2D map.  This map has
// been converted to a 1D array before being passed to this function.
// Written by Tanner Prestegard.
// Last updated: 10/10/2012
// Contact: prestegard@physics.umn.edu
//
// INPUTS:
// input_y              - input 1D Y map.
// input_sigma          - input 1D sigma map.
// size_x               - size of original 2D array in x direction.
// size_y               - size of original 2D array in y direction.
// params_num_pix       - minimum number of pixels required for a cluster to be saved.
// params_rad           - clustering radius.
// params_snr_threshold - pixels above this threshold will be considered for clustering.
// params_x_metric      - clustering ellipse x-parameter.
// params_y_metric      - clustering ellipse y-parameter.
// params_debug         - option to print debug information.  Can be 0, 1, or 2
//                        based on amount of information desired.
// params_weightedSNR   - true: use STAMP weighted SNR to calculate cluster
//                        statistics.  false: use sum of pixel SNRs.
//
// OUTPUTS:
// nPix          - number of pixels in max. cluster.
// snr_gamma     - STAMP multi-pixel SNR of max. cluster.
// y_gamma       - STAMP multi-pixel Y statistic of max. cluster.
// sigma_gamma   - STAMP multi-pixel sigma statistics of max. cluster.
// output_max    - output ft-map of max. cluster (SNR).
// output_all    - output ft-map of all clusters (labeled by cluster number).
////////////////////////////////////////////////////////////////////////////////////

// Main function.
void tree(const double* input_y, const double* input_sigma, const int size_x, const int size_y, const int params_num_pix, const double params_rad, const double params_snr_threshold, const double params_x_metric, const double params_y_metric, const int params_debug, const bool params_weightedSNR, int &nPix, double &snr_gamma, double &y_gamma, double &sigma_gamma, double* output_max, double* output_all) {

  // SETUP ---------------------------------------------------------------------------------
  // Set values of params object.
  const Params params(params_num_pix,params_rad,params_snr_threshold,params_x_metric,params_y_metric,params_debug,params_weightedSNR);

  // Define vector of Seed objects.
  vector<Seed> all_seeds;
  all_seeds.reserve(size_x*size_y);
  int nSeeds = 0;
  
  // Define and set up vector of Row objects.
  vector<Row> rows;
  rows.reserve(size_y);
  for (int i = 0; i < size_y; i++) {
    Row temp_row;
    temp_row.seedlist.reserve(size_x);
    temp_row.index = i;
    rows.push_back(temp_row);
  }
  // ---------------------------------------------------------------------------------------


  // SEED PROPERTIES -----------------------------------------------------------------------
  seed_info(input_y,input_sigma,size_x,size_y,params,all_seeds,rows,nSeeds);
  // ---------------------------------------------------------------------------------------


  // Print debugging information: seed numbers, locations, SNRs, and numbers within rows.
  if (params.debug >= 1) { cout << "Got seed information." << endl; }
  if (params.debug >= 2) {
    cout << "Seed #\tx\ty\tval\trowNum" << endl;  
    for (int i = 0; i < nSeeds; i++) {
      cout << all_seeds[i].num << "\t" << all_seeds[i].x_loc << "\t" << all_seeds[i].y_loc << "\t" << all_seeds[i].snr << "\t" << all_seeds[i].rowNum<< endl;
    }
  }

  // Define a vector of clusters; max. # of clusters =
  // # of seeds / # of pixels req. for a cluster.
  vector<Cluster> allClusters;
  int nClustersMax = nSeeds/params.num_pix;
  allClusters.reserve(nClustersMax); // Reserve memory for this vector.


  // CLUSTERING ----------------------------------------------------------------------------
  cluster_pixels(params,all_seeds,rows,allClusters);
  if (params.debug >= 1) { cout << "Got clusters." << endl; }

  // If no clusters are found:
  if (allClusters.size() == 0) {
    cout << "No significant clusters found." << endl;
    
    for (int i = 0; i < size_x*size_y; i++) {
      output_max[i] = 0;
      output_all[i] = 0;
    }
    nPix = 0;
    snr_gamma = 0;
    y_gamma = 0;
    sigma_gamma = 1;
    return;
  }
  // ---------------------------------------------------------------------------------------
  

  // ANALYZE -------------------------------------------------------------------------------
  // Define a Cluster for the max. cluster and reserve space. 1000 seems like a reasonable
  // upper limit for number of pixels in a cluster, for realistic signals and
  // reasonable clustering parameters.
  Cluster maxCluster;
  maxCluster.seedlist.reserve(1000);

  // Calculate STAMP statistics for all clusters.
  // Find max. cluster, make output maps for plotting.
  analyze_clusters(params,size_x,size_y,all_seeds,allClusters,maxCluster,output_max,output_all);
  if (params.debug >= 1) { cout << "Clusters analyzed, got max. cluster." << endl; }
  // ---------------------------------------------------------------------------------------


  // OUTPUT --------------------------------------------------------------------------------
  //cout << "Clusters found: " << allClusters.size() << endl;

  // Assign values to other outputs.
  nPix = maxCluster.nPix;
  snr_gamma = maxCluster.snr_gamma;
  y_gamma = maxCluster.y_gamma;
  sigma_gamma = maxCluster.sigma_gamma;

  // Print cluster debugging info.
  if (params.debug >= 1) {
    for (unsigned int i = 0; i < allClusters.size(); i++) {
      Cluster temp1 = allClusters[i];
      cout << "Cluster " << i << ": SNR = " << temp1.snr_gamma << " NPix = " << temp1.nPix << "\n\t";
      for (unsigned int j = 0; j < temp1.seedlist.size(); j++) {
	cout << temp1.seedlist.at(j) << " ";
      }
      cout << endl;
    }
  }
  // ---------------------------------------------------------------------------------------

}
