#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <vector>
#include "cluster.h"

using namespace std;

// PURPOSE:
// Calculate the total snr (snr_gamma) of all clusters.
// Set maxCluster to be the cluster with the highest snr_gamma.
// Create output maps for plotting.
//
// INPUTS:
// params     - Params object.
// x          - 2D map size in x direction.
// y          - 2D map size in y direction.
// seeds      - vector of Seed objects.
// clusters   - vector of Cluster objects.  Also an output since cluster
//              statistics are calculated and stored for each cluster, so
//              it can't be const.
//
// OUTPUTS:
// maxCluster - Cluster object for max. cluster.
// output_max - 1D SNR map containing only max. cluster (output).
// output_all - 1D map containing all clusters (map of cluster number, not SNR).
//////////////////////////////////////////////////////////////////////////////////////

void analyze_clusters(const Params params, const int x, const int y, const vector<Seed> &seeds, vector<Cluster> &clusters, Cluster &maxCluster, double* output_max, double* output_all) {

  // setup
  int max_index = 0;
  double max_snr = 0;
  double yw2, w2, naiveSNR;

  // Set all elements of output_max and output_all to zero.
  for (int i = 0; i < y; i++) {
    for (int j = 0; j < x; j++) {
      output_max[i*x+j] = 0;
      output_all[i*x+j] = 0;
    }
  }

  Seed tempSeed;
  // Loop over clusters.
  // Calculate STAMP multi-pixel statistics for all clusters.
  // Add all clusters to 'output_all' array (all clusters map).
  // Find max. cluster.
  for (unsigned int i = 0; i < clusters.size(); i++) {

    yw2 = 0; // y*weight^2 (weight = 1/sigma)
    w2 = 0; // weight^2
    naiveSNR = 0;
    
    // Loop over pixels in a cluster.
    for (int j = 0; j < clusters[i].nPix; j++) {

      // Temporary seed object representing the jth pixel in cluster i.
      tempSeed = seeds[clusters[i].seedlist[j]];

      // Cluster statistics.
      yw2 += (tempSeed.y)/pow((tempSeed.sigma),2);
      w2 += 1/pow((tempSeed.sigma),2);
      naiveSNR += tempSeed.snr;

      // Store pixel j in the output_all map based on its clusterIndex.
      // Note: clusterIndices start at 0, hence the +1.
      output_all[tempSeed.y_loc*x + tempSeed.x_loc] = clusters[i].clusterIndex + 1;
    }
    clusters[i].y_gamma = yw2/w2;
    clusters[i].sigma_gamma = 1/sqrt(w2);

    // Use weighted cluster SNR or sum of pixel SNRs?
    if (params.weightedSNR) {
      clusters[i].snr_gamma = (clusters[i].y_gamma)/(clusters[i].sigma_gamma);
    } else {
      clusters[i].snr_gamma = naiveSNR;
    }

    // If current cluster's snr_gamma is larger than the max. so far,
    // store it as the max cluster.
    if (clusters[i].snr_gamma > max_snr) {
      max_index = i;
      max_snr = clusters[i].snr_gamma;
    }

  }
  
  // Set maxCluster to be to cluster with the highest snr_gamma.
  maxCluster = clusters[max_index];
  maxCluster.clusterIndex = max_index;

  // Add max. cluster to output_max array.
  for (unsigned int k = 0; k < maxCluster.seedlist.size(); k++) {
    tempSeed = seeds[maxCluster.seedlist[k]];
    output_max[tempSeed.y_loc*x + tempSeed.x_loc] = tempSeed.snr;
  }

}
