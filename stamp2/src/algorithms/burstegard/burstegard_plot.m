function burstegard_plot(cluster,map,params)
% function burstegard_plot(cluster,map)
%
% Makes cluster plots (largest cluster and all clusters).
%
% Arguments:
% cluster - struct containing all information about
%           clusters returned by burstegard code.
% map     - STAMP map struct.
%
% Written by Tanner Prestegard.
% Contact: prestegard@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate xvals.
map.xvals = map.segstarttime - map.segstarttime(1);

% PRINT ALL CLUSTERS
clust_nums = unique(cluster.reconAll(:));
clust_nums = clust_nums(2:end); % Remove 0s.
times = map.xvals;
freqs = map.f;

% FOR injection print a zoomed map around the injection time
if params.stampinj
    tt=map.segstarttime>max(params.pass.stamp.startGPS-0.10*params.pass.stamp.dur,map.segstarttime(1))&...
       map.segstarttime<min(params.pass.stamp.startGPS+params.pass.stamp.dur+0.10*params.pass.stamp.dur,map.segstarttime(end));
    [fmin,fmax]=textread(params.pass.stamp.file, '%*s %d %d %*s %*s\n',1,'headerlines', 1);
    ff=map.f>max(fmin-0.1*(fmax-fmin),map.f(1)) & ...
    map.f<min(fmax+0.1*(fmax-fmin),map.f(end));
end


% Marker array.
markers = {'ro', 'go', 'bo', 'co', 'mo', 'ko', ...
           'rs', 'gs', 'bs', 'cs', 'ms', 'ks', ...
           'rd', 'gd', 'bd', 'cd', 'md', 'kd', ...
           'rv', 'gv', 'bv', 'cv', 'mv', 'kv', ...
           'r^', 'g^', 'b^', 'c^', 'm^', 'k^', ...
           'r<', 'g<', 'b<', 'c<', 'm<', 'k<', ...
           'r>', 'g>', 'b>', 'c>', 'm>', 'k>', ...
           'rp', 'gp', 'bp', 'cp', 'mp', 'kp', ...
           'rh', 'gh', 'bh', 'ch', 'mh', 'kh'};

% Set up figure.
figure;
axis([map.xvals(1) map.xvals(end) map.f(1) map.f(end)]);
xlabel('t (s)');
ylabel('f (Hz)');
hold on;

% Add clusters to plot.
for i = 1:length(clust_nums)
  %fprintf('%i ',i);
  marker_i = markers{ mod((clust_nums(i) - 1), length(markers)) + 1 };
  [r,c] = find(cluster.reconAll == clust_nums(i));
  handle = plot(times(c), freqs(r), marker_i);

  % Add cluster number to plot
  text_color = 'k';
  if strcmp(marker_i,'k'), text_color = 'b'; end
  text(min(times(c)),max(freqs(r)),num2str(i),'Color',text_color);
  set(handle, 'MarkerSize', 10);
  set(handle, 'MarkerFaceColor', get(handle, 'Color'));
end
fprintf('\n');

hold off;

title(['number of clusters = ' num2str(length(clust_nums))], 'FontSize', 20);
pretty;
print('-dpng', [params.plotdir '/all_clusters'])
print('-depsc2', [params.plotdir '/all_clusters'])

% PRINT LARGE CLUSTER
printmap(cluster.reconMax, map.xvals, map.f, 't (s)', ['f' ...
		    ' (Hz)'],'SNR',[-5 5],0,0);
text_title = sprintf('Max. SNR = %.3f',cluster.snr_gamma);
title(text_title);
pretty;
print('-dpng', [params.plotdir '/large_cluster']);
print('-depsc2', [params.plotdir '/large_cluster']);

if params.stampinj
    printmap(cluster.reconMax(ff,tt), map.xvals(tt), map.f(ff), 't (s)', ...
             ['f (Hz)'],'SNR',[-5 5],0,0);
    text_title = sprintf('Max. SNR = %.3f',cluster.snr_gamma);
    title(text_title);
    pretty;
    print('-dpng', [params.plotdir '/large_cluster_zoomed']);
    print('-depsc2', [params.plotdir '/large_cluster_zoomed']);
end

return;
