function params = zebragardDefaults(params)
% function params = zebragardDefaults(params)
% Sets default zebragard parameters and
% sets up burstegard parameters as well.
%
% Routine written by Tanner Prestegard
% Contact: prestegard@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params.doZebragard = true;
params.zebragard.NCN = [20 30 80];
params.zebragard.NR = [2 4 25];
params.zebragard.pixelThreshold = 1;
params = burstegardDefaults(params);

return;
