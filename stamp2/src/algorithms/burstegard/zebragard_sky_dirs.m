function [ra_out, dec_out] = zebragard_sky_dirs(params,nDirs,method)
% function [ra_out, dec_out] = zebragard_sky_dirs(params,nDirs,method)
%
% Inputs:
%   params - STAMP parameters, should have ifos and GPS times set
%   nDirs  - number of sky directions
%   method - how you want to choose the sky directions.
%     Options:
%       1 - span possible time delays using all possible sky positions
%       2 - span possible time delays using only sky positions which
%           correspond to the top 10% most sensitive sky positions.
%       3 - seed random number generator from clock, choose 
%           random sky directions.
%
% Outputs:
%   ra  -  column vector of right ascensions (in hrs)
%   dec -  column vector of declinations (in deg.)
%
% Routine written by Tanner Prestegard.
% Contact: prestegard@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ideas for ways to calculate sky directions
% 1. Maximum time delay spacing (evenly spaced, ignoring epsilon)
% 2. Most sensitive directions (should be reasonably far from each other)
% 3. Maximum time delay spacing (evenly spaced, chosen out of top ~10% of
%    epsilons)

% check inputs
if (nDirs < 1)
  error('nDirs must be >= 1')
end
if (mod(nDirs,1)~=0)
  error('nDirs must be an integer')
end
if (method ~= 1 & method ~= 2 & method ~= 3)
  error('method must be 1, 2, or 3')
end

% random sky directions
if (method == 3)
  rand('state',sum(clock*1e6));
  ra_out = rand(nDirs,1)*24;   % right ascension in hours
  dec_out = (acosd(1-2*rand(nDirs,1))-90); % declination in degrees
  return;
end

% set basic parameters
params.c = 299792458;
if ~isfield(params,'ifo1')
  params.ifo1 = 'H1';
end
if ~isfield(params,'ifo2')
  params.ifo2 = 'L1';
end
[det1 det2] = ifo2dets(params);

% Use mid GPS time of entire ft-map to generate this list.
GPStime = (params.endGPS + params.startGPS)/2;

% generate list of source positions.
% declination needs to be handled so that we don't end up with a bunch of
% repetitive points near the poles.
res = 1; % resolution (in degrees)
ra = 0:1*24/360:23.999;
dec = 0:(res/180):1;
dec = (180/pi)*(acos(1-2*dec)-pi/2);

% make vector of all sky direction pairs and run calF.
source = combvec(ra,dec)';
g = calF(det1, det2, GPStime, source, params);

if (method == 2)
  % calculate epsilon and sort.
  epsilon = (g.F1p .* g.F2p + g.F1x .* g.F2x)/2;
  [eps2 eps_idx] = sort(abs(epsilon));

  % get top 10% of epsilon values (by absolute values)
  % and the corresponding indices
  N = round(numel(eps2)*0.1);
  eps2_idx = (numel(eps2)-N+1):numel(eps2);
  eps2 = eps2(eps2_idx);
  eps_idx = eps_idx(eps2_idx);

  % reduce tau and source arrays to just the elements corresponding to
  % the top 10% of epsilon values.
  g.tau = g.tau(eps_idx);
  source = source(eps_idx,:);
end

% sort time delays and get nDirs time delays, evenly
% spaced.  Get get indices of those time delays
% so we can get the corresponding sky directions.
[no_use tau_idx] = sort(g.tau);
spaced_idx = round(linspace(1,size(source,1),nDirs));
idx = tau_idx(spaced_idx);
%epsilon = epsilon(eps_idx);

% set final sky positions.
ra_out = source(idx,1);
dec_out = source(idx,2);


return;