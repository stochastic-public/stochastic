from __future__ import division
import numpy as np
from scipy.special import gamma, kn
import pdb

def snrdist():
    # converted from stamp_snr_dist by MWC

    N = 8.0
    sigma1 = 1.0
    sigma2 = 1/(1.1809)
    constant  = (N**(2*N))/( (2**(2*N-1)) * (sigma2**(4*N+2)) * (gamma(N))**2 )
    
    zsnr = np.arange(-100, 100.1, 0.1) 
    x = np.arange(0.01, 10.01, 0.01)

    #X = np.repeat(np.reshape(x, (x.size, 1)), zsnr.size, axis=1)
    #Z = np.repeat(np.reshape(zsnr, (1, zsnr.size)), x.size, axis=0)

    # Y = constant*np.abs(X) * np.exp( -np.abs(X*Z/(sigma1)**2)) * kn(0, N*X/(sigma2)**2) * X**(2*N -1)
    
    intvals = np.zeros(zsnr.shape)

    for ii in range(0, zsnr.size):
       y  = constant*np.abs(x) * np.exp( -np.abs(x*zsnr[ii]/(sigma1)**2)) * kn(0, N*x/(sigma2)**2) * x**(2*N -1)
       # pdb.set_trace()
       intvals[ii] = 100*np.trapz(y, x)


    # PDF of SNR
    # intvals = 100*np.trapz(Y, x, axis=0)
    intvals = intvals/np.sum(intvals)

    #c = np.sort(zsnr)
    #p = np.argsort(zsnr)

    #ic = np.digitize(zsnr,  (c[0:-1] + c[1:])/2)

    #ib = (p ==ic)

    #zm   = intvals[ib]

    np.savetxt('snr_dist.txt', (zsnr, intvals))

    plot = False

    if plot:
        plt.plot(zsnr, intvals, '*', label='intvals')
        plt.plot(zsnr, zm,'-', label='zm')
        plt.legend()
        plt.gca().set_xlim(-5, 5)
        plt.savefig('snrdist.png', dpi=150)

    return


if __name__ == "__main__":
    snrdist()
