from __future__ import division
import numpy as np
import matplotlib
matplotlib.use('Agg')
import corner
import matplotlib.pyplot as plt
import sys, os
import json
import pymultinest
import pdb
plt.rc('xtick',labelsize=16)
plt.rc('ytick',labelsize=16)

def ulcontours(outdir, ul):

    '''
    Prints UL Contours:
    
    Input: 
    Outdir: Directory containing multinest posteriors
    ul: The upper limit to be set. 
    '''
    
    # Convert to number
    ul = float(ul)

    if ul>=1:
        raise ValueError('Upper limit should be < 1')

    if outdir[-1] != '/':
        outdir = outdir + '/'

    if not os.path.exists(outdir + 'params.json'):
        sys.stderr.write("Expected the file %sparams.json with the parameter names")

    # List of parameters
    params = json.load(open(outdir+'params.json'))


    # Number of parameters
    npar = len(params)

    # analyse data
    outdata = pymultinest.Analyzer(npar, outputfiles_basename=outdir)

    # These are the posteriors. The first three columns are the values of the parameters,
    # and the last column is the ln(Z)
    multinest_output = outdata.get_equal_weighted_posterior()

    post = multinest_output[:, 0:npar]
    post[:, 1] = post[:, 1]* (post[:, 2])**(1/(1 - post[:, 3]))
    # Prepare 
    f0 = np.arange(30, 4000, 200)
    tau = np.arange(10,2000, 50)

    delf0  = f0[1] - f0[0]
    deltau = tau[1] - tau[0]

    f0 = f0 + delf0/2
    tau = tau + deltau/2


    for ii in range(0, f0.size):
        for jj in range(0, tau.size):
            
            idx1 = np.logical_and((post[:, 1 ] <  f0[ii] + delf0/2) ,  (post[:, 1] >= f0[ii] - delf0/2 )) 
            idx2 = np.logical_and((post[:, 2 ] <  tau[jj] + deltau/2),  (post[:, 2] >= tau[jj] - deltau/2 ))
            
            idx = np.logical_and(idx1, idx2)
            alp_true = post[idx, 0]
         
            pdb.set_trace()
            np.histogram(alp_true)

if __name__ == "__main__":

     if len(sys.argv) != 3:
         raise ValueError('Needed:Path to directory with posteriors and UL value')
     else:
         ulcontours(sys.argv[1], sys.argv[2])
