from __future__ import division
import numpy as np
import pyfftw.interfaces.numpy_fft as npfft
from AntennaFactors import calcAntennaFactors
import pdb
from scipy.interpolate import interp1d as intrp

def gwPower(hp, hx, data, fs):

    '''
    Calcualte and make spectrogram of the intrinsic gravitational wave power for the given 
    hp and hx. This is in principle directly comparable to the estimator spectrogram, y map 
    from stamp
     
    Output: modelY
    '''

    nsegs, segDur, dataFreqs = data['nsegs'], data['segDur'][0,0], data['freqs']
    L = int(segDur*fs)
    # Hann window
    hwindow = np.hanning(L)
    # Hann Window constant
    windconst  = 0.375

    # Make sure that the NFFT is a power of 2 for easier calcualtions
    NFFT = 2*npow2(fs)
    
    # Frequencies for FFT
    fftfreqs = (fs/2)*np.linspace(0, 1,  1 + NFFT/2)
    
    # Zero padding
    zeropadL = int(NFFT - L)
    zpad  = np.zeros(zeropadL)
    
    # Initialize model map
    modelY = np.zeros((dataFreqs.size, nsegs))
    # This is a cross-correlation map
    modelcc = np.zeros((fftfreqs.size, nsegs))
    stamp_corr = 2.66731786731787
    for ii in range(0, nsegs):
        idxmin = int(segDur*ii*fs/2)
        idxmax = int(segDur*(ii/2 + 1)*fs)
        # Calculate FFT
        yp = npfft.fft(np.concatenate((hwindow*hp[idxmin:idxmax], zpad)))/fs
        yx = npfft.fft(np.concatenate((hwindow*hx[idxmin:idxmax], zpad)))/fs

        # We select only positive frequencies for a single sided spectrum and multiply by 2 for it
        modelcc[:, ii] = 2*stamp_corr*( (np.abs(yp[0:int(NFFT/2)+1]))**2  +  (np.abs(yx[0:int(NFFT/2)+1]))**2 + \
                                        2*np.real( yp[0:int(NFFT/2)+1]*np.conj(yx[0:int(NFFT/2)+1] )))/(segDur)

    
    modelY = coarseGrain(modelcc,fftfreqs,dataFreqs)
    return modelY

def crossPower(hp, hx, data, fs):


    '''
    Calculates model cross-power maps for a given GW signal as based on STAMP definitions.
    Much of the code here is taken from getPointSourceData_IM2.m and ccSpecReadout_stamp.m
    Input: hp, hc, data dict

    Output: modelY
    
    '''
    # Extract directions
    ra, decl = data['ra'], data['decl']
    # Extract detector structs
    det1, det2 = data['det1'], data['det2']

    # define det0 for the the center of the earth. Antenna factor for center of earth aren't really used
    det0 = dict([ ('r', np.zeros(det1['r'].shape)) , ('d', det1['d']) ]) 

    # Make time series
    dt  = 1/fs
    duration = int(data['nsegs']*data['delT'])
    tGPS = data['startGPS'] + np.arange(0, hp.size, 1)/float(fs)
    
    # Calcualte antenna factors for each detector
    g1 = calcAntennaFactors(data['ra'], data['decl'], tGPS, det0, det1)
    g2 = calcAntennaFactors(data['ra'], data['decl'], tGPS, det0, det2)

    # ------- Make detector data streams, first for detector 1 ---------------
    h1_tmp = hp*g1['F2p'] + hx*g1['F2x']
    delayedt1 , itimes1 = np.unique(tGPS - g1['tau'] , return_index=True)
    h1_tmp = h1_tmp[itimes1]

    # Next we account for finite sampling
    nsamples = np.around(fs*g1['tau'])
    t1 = tGPS - (dt*nsamples)
    f1 = intrp(delayedt1, h1_tmp,kind='cubic', fill_value='extrapolate')
    h1_tmp = f1(t1)

    h1 = np.zeros(tGPS.shape)
    # We have to shift the times now back to the earth frame. Otherwise things would be messed 
    # when applying the antenna patterns to calcualte Y
    waste, idx_un = np.unique(t1, return_index=True)
    idx1 = np.in1d(t1[idx_un], tGPS)
    idx2 = np.in1d(tGPS, t1[idx_un])
    ha = h1_tmp[idx_un]
    h1[idx2] = h1[idx2] + ha[idx1]


    # Now for detector 2
    h2_tmp = hp*g2['F2p'] + hx*g2['F2x']
    delayedt2 , itimes2 = np.unique(tGPS - g2['tau'] , return_index=True)
    h2_tmp = h2_tmp[itimes2]
    
    # Next we account for finite sampling
    nsamples = np.around(fs*g2['tau']) 
    t2 = tGPS - (dt*nsamples)
    f2 = intrp(delayedt2, h2_tmp,kind='cubic', fill_value='extrapolate')
    h2_tmp = f2(t2)
    h2 = np.zeros(tGPS.shape)
    # We have to shift the times now back to the earth frame. Otherwise things would be messed
    # when applying the antenna patterns to calcualte Y
    waste, idx_un = np.unique(t2, return_index=True)
    idx1 = np.in1d(t2[idx_un], tGPS)
    idx2 = np.in1d(tGPS, t2[idx_un])
    ha = h2_tmp[idx_un]
    h2[idx2] = h2[idx2] + ha[idx1]

    # ------- Now Finally we can do ffts and calculate cross-power --------------

   
    nsegs, segDur, dataFreqs = data['nsegs'], data['segDur'][0,0], data['freqs']
    L = int(segDur*fs)

    # Hann window
    hwindow = np.hanning(L)
    # Hann Window constant
    #windconst  = 0.375
    stamp_corr = 2.66731786731787

    # Make sure that the NFFT is a power of 2 for easier calcualtions
    # We will zero pad each segment by segDur*fs
    NFFT = 2*npow2(fs)
    # Frequencies for FFT
    fftfreqs = (fs/2)*np.linspace(0, 1,  1 + NFFT/2)

    # Zero padding
    zeropadL = int(NFFT - L)
    zpad  = np.zeros(zeropadL)
    
    # Initialize model map
    modelY = np.zeros((dataFreqs.size, nsegs))
    # This is a cross-correlation map
    modelcc = np.zeros((fftfreqs.size, nsegs),dtype='complex')
    # Calculate antenna factors for each segment
    midGPStimes = data['segstartGPS'] + data['delT']
    #gcc = calcAntennaFactors(data['ra'], data['decl'], midGPStimes, det1, det2)
    gcc = data['gcc']
    F1p, F2p, F1x, F2x = gcc['F1p'][0,0], gcc['F2p'][0,0],gcc['F1x'][0,0],gcc['F2x'][0,0]
    taus = gcc['tau'][0, 0]
    # gcc = calcAntennaFactors(data['ra'], data['decl'], midGPStimes, det1, det2)
    gamma0 = F1p*F2p + F1x*F2x
    # taus = np.repeat(taus, dataFreqs.size, axis=0)
    rbar1 = np.zeros(modelcc.shape,dtype='complex')
    rbar2 = np.zeros(modelcc.shape, dtype='complex')

    for ii in range(0, nsegs):
        
        idxmin = int(segDur*ii*fs/2)
        idxmax = int(segDur*(ii/2 + 1)*fs)

        # Calculate FFT
        ytilde1 = np.fft.fft( np.concatenate((hwindow*h1[idxmin:idxmax], zpad)))/fs
        ytilde2 = np.fft.fft( np.concatenate((hwindow*h2[idxmin:idxmax], zpad)))/fs

        # Factor of 2 because one sided. stamp_corr is the hann win
        modelcc[:, ii] = 2*stamp_corr*np.conj(ytilde1[0:int(NFFT/2)+1]) *ytilde2[0:int(NFFT/2)+1]/(segDur)
        
        rbar1[:, ii] = ytilde1[0:int(NFFT/2)+1]
        rbar2[:, ii] = ytilde2[0:int(NFFT/2)+1]

        
    Fmap =  np.repeat(dataFreqs, taus.shape[1], axis=1)
    # Calculate Y from the cross-correlation spectrogram. 
    #emacs  First we will set the flow of modelcc to be half of dataFreqs
    fidx = fftfreqs >= dataFreqs[0]/2
    fftfreqs = fftfreqs[fidx]
    modelcc = modelcc[fidx, :]

 
    #datacc = data['CSD']/(segDur*windconst)
    # Coarsegrain it to data freqs
    modelcc = coarseGrain(modelcc,fftfreqs,dataFreqs)  
    #dataY = np.real( (2/gamma0)*np.exp(- 2*np.pi*1j*Fmap*Tmap)*datacc)
    modelY = np.real( (2/gamma0)*np.exp(- 2*np.pi*1j*taus*Fmap)*modelcc )
    modelY[data['nanidx']] = 0
    
    return modelY



# -------------- Auxiallary function for finding the next power of 2 to the sampling frequency ----------------
def npow2(x):

    pows2 = 2**(np.arange(1, 20, 1))
    idx = np.logical_and( pows2 >= x , 2*x > pows2)
    
    if np.sum(idx) !=1:
        raise ValueError('Something wrong. You should have a singe number here')

    y = pows2[idx]

    return y

# ---------------- Auxillary coarsegraining function ------------------------------------------------------

def coarseGrain(mapx, xFreqs, yFreqs):

    '''
    Coarse grain a finer binned spectrogram into a coarser one. This is a 
    stripped down python copy of coarsegrain.m
    
    Input:
    mapx: fine binned map.
    xFreqs: frequencies of mapx
    yFreqs: desired frequencies of the coarser map
    
    Output:
    mapy: coarse spectrogram
    '''
    
    # define some variables
    delFx = xFreqs[2] - xFreqs[1]
    delFy = yFreqs[2] - yFreqs[1]
    flowx, flowy = xFreqs[0], yFreqs[0]
    Nx, Ny = xFreqs.size, yFreqs.size
    
    if delFy != 2*delFx:
        raise ValueError('deisred coarsegraining is not compatiable with this function')

    ### IMPORTANT NOTE: THIS FUNCTION AS WRITTEN HERE WAS MEANT ONLY FOR USE IN 
    ### crossPower.py. BECAUSE OF THIS I DON'T CHECK FOR THE CONSISTENCY OF THE INPUT
    ### AND OUTPUT FREQUENCIES. IF USED OUTSIDE OF THIS FUNCTION, PLEASE MAKE SURE THAT
    ### THOSE CHECKS ARE MADE. REFER TO coarseGrain.m FOR EXAMPLE
                         
    idxmid = (yFreqs - flowx)/delFx
    idxmin = (-0.5 + yFreqs - flowx)/delFx
    idxmax = (0.5  + yFreqs - flowx)/delFx

    # Convert to ints
    idxmin, idxmax, idxmid = idxmin.astype(int), idxmax.astype(int), idxmid.astype(int)

    fraclow = delFx/delFy
    frachigh = delFx/delFy
    
    # Coarse graining step
    mapy = (delFx/delFy)*(fraclow*mapx[idxmin, :] + frachigh*mapx[idxmax, :] + mapx[idxmid, :])
    
    if mapy.shape[0] != yFreqs.size:
        raise Exception('Coarsegraining output is incompatiable with the the requested frequencies')

    if len(mapy.shape) == 3 and mapy.shape[1] ==1 : 
        mapy = np.reshape(mapy, (mapy.shape[0], mapy.shape[2]))
    
    return mapy
    


# ---------------------- SIMPLE WRAPPER FOR PARSING FUNCTION CALLS ---------------------------------

def modelmap(hp, hx, data, fs, maptype):

    if maptype == 'crosspower':
        modelY = crossPower(hp, hx, data, fs)
    elif maptype == 'gwpower':
        modelY = gwPower(hp, hx, data, fs)
    else:
        raise ValueError('Unknown maptype requested in cross-power')

    return modelY

        
