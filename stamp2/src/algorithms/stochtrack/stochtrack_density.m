function stochtrack_density
% function run_clustermap_noise

params = stochtrackDefaults;
params.doGPU = 0;
M=151; 
M=1776;
N=499;
map.snr = ones(M,N);
aa = 1;
map.segstarttime = (0:N-1)*(1/2);
params.segmentDuration = 1;
map.f = (0:M-1) + 25;

params.plotdir = 'plots';
if ~exist(params.plotdir)
   system(['mkdir -p ' params.plotdir]);
end

% index function: one unique index value for every (x,y) pair
% follow matlab's scheme, which looks like this:
%   tmp = [[1 2 3]' [4 5 6]'];
idx = @(xi, yi) (xi-1)*M + yi;

T = 5000; F = 250;
%F = 1;
%T = 10;
params.stochtrack.T = T;
params.stochtrack.F = F;

map.tracks = zeros(size(map.snr));
% for loop to manage the memory
for aa=1:F
  % obtain an array of stochtracks from stochtrack_points
  [yvals,xvals,H,ttvals,track_data] = stochtrack_points(map, params, aa);

  % take real part to make GPU friendly
  yvals = real(yvals);

  % calculate weights
  indices = find(isnan(yvals));
  weight_up = mod(yvals,1);
  weight_down = 1 - mod(yvals,1);
  % set weights to zero where there is no track
  weight_up(indices) = 0;
  weight_down(indices) = 0;
  yvals(indices) = M/2;
  xvals(indices) = 1;

  % calculate templates
  yvals_up = ceil(yvals);
  yvals_down = floor(yvals);
  templates_up = idx(xvals, yvals_up);
  templates_down = idx(xvals, yvals_down);

  map.tracks(templates_up) = map.tracks(templates_up) + weight_up;
  map.tracks(templates_down) = map.tracks(templates_down) + weight_down;
end

map.tracks = map.tracks ./ max(map.tracks(:));
maxval = max(map.tracks(:));
%minval = maxval*0.90;
minval = 0;
map.xvals=map.segstarttime+params.segmentDuration/4 - map.segstarttime(1);
map.yvals = map.f;
printmap(map.tracks, map.xvals, map.yvals, 't (s)', 'f (Hz)', 'Relative Density', [minval maxval],0,0);
print('-dpng', [params.plotdir '/tracks']);
print('-depsc2', [params.plotdir '/tracks']);


return

