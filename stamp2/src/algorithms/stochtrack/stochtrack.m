function max_cluster = stochtrack(map, params)
% function max_cluster = stochtrack(map, params)
%
% INPUT:
%    map  - STAMP map struct.
%    params - struct containing parameters for STAMP search.
%
% OUTPUT:
%    max_cluster (STRUCT)
%        max_cluster.nPix -> number of pixels in the largest cluster
%        max_cluster.snr_gamma -> SNR of the largest cluster
%        max_cluster.y_gamma -> Y (GW estimator) of largest cluster
%        max_cluster.sigma_gamma -> sigma of the above estimator
%        max_cluster.reconMax -> 2D array containing weight numbers for
%                                pixels contained the largest cluster and 0's
%                                at the location of other pixels
%        max_cluster.tmin
%        max_cluster.tmax
%        max_cluster.fmin
%        max_cluster.fmax
%        max_cluster.numpixels
%        max_cluster.SNRfrac  
% Written by E Thrane and M Coughlin
% Contact: ethrane@ligo.caltech.edu, michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% state if parallelization is on

if params.stochtrack.doSeed
  rng(params.stochtrack.seed,'twister');
  if params.doGPU
    parallel.gpu.rng(params.stochtrack.seed,'Philox4x32-10');
  end
end

if params.doParallel
  fprintf(' (parallel on)...\n');
else
  fprintf(' (parallel off)...\n');
end

% extra feature added by MWC allows the user to turn the map on its side before
% applying stochtrack in order to pick up nearly vertical tracks
if params.stochtrack.doTranspose
   map.snr=map.snr'; map.y=map.y'; map.z=map.z'; map.sigma=map.sigma';
   fprintf('running with transpose...\n');
end

if params.stochtrack.doExponential || params.stochtrack.doRModes
   if params.stochtrack.T ~= size(map.snr,2)
      error('Please run with params.stochtrack.T = %d\n',size(map.snr,2));
   end
end

  
% map of NaNs: zeros where NaNs are present
nanmap0 = ~isnan(map.snr);

% NaNs occur due to missing data, vetoes, and notches.  When a NaN is
% encountered, y and snr are set to zero (so they do not add to the cluster SNR
% and sigma is set to one (much larger than typical strain power) so that the 
% cluster sigma does not depend significantly on the NaN pixels.  Just to be 
% safe, a check is employed to make sure that map.sigma << 1 in case anyone
% uses this with some funny units of strain.
if max(map.sigma(:)) > 1e-10
  error('Surprisingly large value of strain power encountered.');
end
map.snr(isnan(map.snr)) = 0;
map.y(isnan(map.y)) = 0;
map.z(isnan(map.z)) = 0;
map.sigma(isnan(map.sigma)) = 1;
map.sigma(map.sigma==0) = 1;

% map is MxN: M is the # of frequency bins and N is the number of segments
[M,N] = size(map.snr);

%psd_data=load(params.stochtrack.psdFile);
%psd_interp=interp1(psd_data(:,1),psd_data(:,2),map.f);
%for ii = 1:N
%   map.sigma(:,ii) = psd_interp;
%end

% diagnostic tool: write over map with fake noise and a bright monochromatic 
% line
if params.stochtrack.demo
  fprintf('stochtrack demo is on: overwriting map data!\n');
  map.snr = randn(M,N);
  map.snr(floor(M/2),:) = map.snr(floor(M/2),:) + 1;  
end

% index function: one unique index value for every (x,y) pair
% follow matlab's scheme, which looks like this:
%   tmp = [[1 2 3]' [4 5 6]'];
idx = @(xi, yi) (xi-1)*M + yi;

% stochtrack parameters-------------------------------------------------------
% number of trials
T = params.stochtrack.T;
% number of trials in for loop
F = params.stochtrack.F;
%-----------------------------------------------------------------------------

% npix_th is a variable used to intentionally disregard small clusters with
% only a few pixels
try
  npix_th = params.stochtrack.npix_th;
catch
  npix_th = params.stochtrack.mindur;
end

% adjust npix_th for vlong studies
if params.stochtrack.vlong
  npix_th = sqrt(npix_th);
end

% define GPU arrays
map_y = gArray(map.y, params);
map_z = gArray(map.z, params);
map_sigma = gArray(map.sigma, params);
nanmap = gArray(nanmap0, params);

% for use when running with H normalization
if strcmp(params.stochtrack.norm, 'H');
  sigma_median = median(map_sigma,2);
  map_sigma_median = repmat(sigma_median, 1, size(map_sigma,2));
% it must be defined even if it is not used to avoid parfor errors; use dum val
else
  map_sigma_median = NaN;
end

% if stochsky search is performed prepare the necessary variables
% in fact, get them no matter what to prevent parfor errors
% obtain detector structs%
[det1 det2] = ifo2dets(params);

% if stochsky search is performed prepare the necessary variables
if params.stochtrack.stochsky || params.stochtrack.cbc.allsky
  fprintf('running stochsky/allsky\n');
  % create complex-valued map_snr
  map_y_eff = map_y + i*map_z;
  map_snr = (map_y + i*map_z) ./ map.sigma;
else
  fprintf('running stochtrack\n');
  % work with real-valued snr
  map_y_eff = map_y;
  map_snr = map_y ./ map.sigma;
end

% When using amplitude corrections, need to know the effect of our highpass 
% filter on the data
if strcmp(params.stochtrack.norm, 'H');
   fprintf('getting filter...\n');
   map.filter_response = filter_response(map, params);
else
   map.filter_response = ones(size(map.f));
end

% max number of pixels in a track: note that extra_pixels is zero for Bezier
% curves.  it is set non-zero for doCBC
if params.stochtrack.doCBC || params.stochtrack.doECBC
  params.stochtrack.max_pixels = N + params.stochtrack.extra_pixels;
else
  params.stochtrack.max_pixels = N;
end
% for brevity
max_pixels = params.stochtrack.max_pixels;

% initialize mweight arrays (associated with max snr track)
mweight_up = NaN*gZeros(max_pixels , F, params);
mweight_down = NaN*gZeros(max_pixels, F, params);
mtemplates_up = NaN*gZeros(max_pixels ,F, params);
mtemplates_down = NaN*gZeros(max_pixels, F, params);

% optional PixelCut removes very loud pixels which can spoil background
if params.stochtrack.doPixelCut
  map_snr(abs(map_snr) > params.stochtrack.pixel_threshold) = 0;
end

% optional burstegard cut never demonstrated to improve sensitivity
if params.stochtrack.doBurstegardCut
  burstegardCut(params, map);
end

% for loop to manage the memory
for aa=1:F
  % obtain an array of stochtracks from stochtrack_points
  [yvals,xvals,H,ttvals,track_data] = stochtrack_points(map, params, aa);

  % take real part to make GPU friendly
  yvals = real(yvals);

  % calculate weights
  indices = find(isnan(yvals));
  weight_up = mod(yvals,1);
  weight_down = 1 - mod(yvals,1);
  % set weights to zero where there is no track
  weight_up(indices) = 0;
  weight_down(indices) = 0;
  yvals(indices) = M/2;
  xvals(indices) = 1;

  % calculate templates
  yvals_up = ceil(yvals);
  yvals_down = floor(yvals);
  templates_up = idx(xvals, yvals_up);
  templates_down = idx(xvals, yvals_down);

  % if requested, calculate random sky-map phase factors
  if params.stochtrack.stochsky
    % Here, commented out, is the original version of stochsky.  The original 
    % version generates random time delays.  The new version, below, generates
    % random (ra,dec), which better accounts for the rotation of the Earth.
    % The old code is useful for pedagogy; please keep it.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    % begin by calculating maximum time delay between detectors
%    [det1 det2] = ifo2dets(params);
%    tau0 = norm(det1.r - det2.r)/params.c;
%    % randomly chosen time delay
%    tau = tau0*repmat(2*gRand(1,size(weight_up, 2), params) - 1,  ...
%      size(weight_up,1), 1);
%    % frequency array
%    ff = (yvals-1)+params.fmin;
%    % phase factor
%    phi = exp(-i*2*pi*ff.*tau);
%    % create complex-valued map_snr
%    map_y_eff = map_y + i*map_z;
%    map_snr = (map_y + i*map_z) ./ map.sigma;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % generate random search directions
    ra = 24*gRand(T, 1, params);
    dec = acos(2*gRand(T, 1, params) - 1);
    dec = (180/pi)*dec - 90;
    source = [ra dec];

    % if we are looking at very long data, track the antenna factor change from
    % the rotation of the Earth.  epsilon tau does not need a gArray
    % initialization because it is defined in terms of source, which is
    % already a gArray.
    if params.stochtrack.vlong
      epsilon = calF_stochtrack(det1, det2, ttvals, source, params);
      weight_up = epsilon.*weight_up;
      weight_down = epsilon.*weight_down;
      epsilon = [];
    end

    % calculate time delay as a function of segment start time
    % note that caltau is identical to the older function calF except it has 
    % been stripped down to only calculate time delay.  tau does not need a 
    % gArray initialization because it is defined in terms of source, which is
    % already a gArray.
    tau = caltau(det1, det2, ttvals, source, params)';
    % frequency array
    ff = (yvals-1)+params.fmin;
    % phase factor
    phi = exp(-i*2*pi*ff.*tau);
  else
    % otherwise, the search is directed and the phase factor is unity
    phi=1;
  end

  % number of pixel normalization
  % do not include pixels that contains NaN values due to missing data,
  % vetoes, and/or notches
  % the vlong version uses a slightly different npix definition, which may be
  % generalizable to all signals in the future.
  if params.stochtrack.vlong
    npix = sum(weight_up.^2.*nanmap(templates_up),1) + ...
      sum(weight_down.^2.*nanmap(templates_down),1);
  else
    npix = sum(weight_up.*nanmap(templates_up),1) + ...
      sum(weight_down.*nanmap(templates_down),1);
  end

  % perform clustering---------------------------------------------------------
  % Different normalizations of snr are possible.  MWC and ET tested several 
  % options shown here and determined that npix provides the most sensitive
  % statistic (for Bezier curves).
  if strcmp(params.stochtrack.norm, 'H');
    % the default normalization for most applications is
    % params.stochtrack.norm='npix'.  This code is the specialized case for
    % params.stochtrack.norm='H'.  It is used mostly in conjunction with doCBC
    % or when a (very) specific signal model is available.

    %H = H .* ((weight_up.*map_sigma(templates_up) + ...
    %  weight_down.*map_sigma(templates_down))./median(map_sigma(:))).^-1;
    H = H .* ((weight_up.*map_sigma_median(templates_up) + ...
      weight_down.*map_sigma_median(templates_down))./ ...
      median(map_sigma_median(:))).^-1;
    H(isinf(H) | isnan(H) | H > 1e30) = 0;
    sumhoft = sum(H);
    sumhoft = repmat(sumhoft', 1, size(H,1))';
    H = H ./ sumhoft;
    %
    H = H .^ params.stochtrack.alpha;
    H(isinf(H) | isnan(H)) = 0;
    sumhoft = sum(H);
    sumhoft = repmat(sumhoft', 1, size(H,1))';
    H = H ./ sumhoft; 

    % Multiply by the number of pixels in the track
    npix_norm = repmat(npix',1,size(H,1))';
    H = H .* npix_norm;

    if params.stochtrack.cbc.allsky==false
      % standard method for all-sky search (when doCBC all-sky is off)
      snr = real(sum(H.*phi.*weight_up.*map_snr(templates_up),1) + ...
         sum(H.*phi.*weight_down.*map_snr(templates_down),1));
      %snr = sum(phi.*weight_up.*abs(H-map_snr(templates_up)),1) + ...
      %   sum(phi.*weight_down.*abs(H-map_snr(templates_down)),1);
      %snr = 1./snr;
    else
      % when doCBC all-sky is on, it is more efficient to simply loop over
      % ndir sky locations.  ndir=40 has been shown to provide adequate
      % sky coverage for LHO-LLO.
      ndirs = params.stochtrack.cbc.ndirs;
      tau0 = norm(det1.r - det2.r)/params.c;
      % create an array of time delays (search directions) to loop over.
      tau = [1:ndirs]*(2*tau0/(ndirs-1)) - (2*tau0/(ndirs-1)) - tau0;
      % frequency array
      ff = (yvals-1)+params.fmin;
      % loop over search directions (time delays)
      for ii=1:ndirs
        % phase factor
        phi = exp(-i*2*pi*ff.*tau(ii));
        % calculate SNR for each phase offset
        snr(ii,:) = real(sum(H.*phi.*weight_up.*map_snr(templates_up),1) + ...
          sum(H.*phi.*weight_down.*map_snr(templates_down),1));
      end
      % chose the time delay that produces the max SNR (for each template)
      [snr,tau_index] = max(snr, [], 1);
    end
    % normalization-----------------------------------------
    % 'H' applies the optimal statistic weighting; see above
    snr_gamma = snr;
    snr_gamma = snr./sqrt(npix);
    % some tracks will not include any pixels (if they fall in a notch).  in
    % these cases, set SNR=0 to avoid SNR=NaN
    snr_gamma(npix==0) = 0;

  elseif strcmp(params.stochtrack.norm, 'npix') || strcmp(params.stochtrack.norm, 'mean') ...
    || strcmp(params.stochtrack.norm, 'ansatz')
    % Entered the else loop, so we are using standard npix normalization.
    % If the cbc.allsky flag is off, we carry out the standard procedure
    if params.stochtrack.cbc.allsky==false
      % standard method for all-sky search (when doCBC all-sky is off)
      %snr = real(sum(phi.*weight_up.*map_snr(templates_up),1) + ...
      %sum(phi.*weight_down.*map_snr(templates_down),1));
      % 10/28/15 by Ryan Quitzow-James: abs() added around snr calculation to
      % catch (some) linearly polarized signals which can make SNR negative due
      % to the unpolarized filter we use
      % 4/24/17 by PaulSchale: removed abs() because it messed up statistics
      % of SNRs of all clusters. Also any polarized signal at negative SNR is 
      % highly unlikely to be detectable due to poor antenna factors
      snr = real(sum(phi.*weight_up.*map_snr(templates_up),1) + ...
      sum(phi.*weight_down.*map_snr(templates_down),1));
    else
      % when doCBC all-sky is on, it is more efficient to simply loop over
      % ndir sky locations.  ndir=40 has been shown to provide adequate
      % sky coverage for LHO-LLO.
      ndirs = params.stochtrack.cbc.ndirs;
      tau0 = norm(det1.r - det2.r)/params.c;
      % create an array of time delays (search directions) to loop over.
      tau = [1:ndirs]*(2*tau0/(ndirs-1)) - (2*tau0/(ndirs-1)) - tau0;
      % frequency array
      ff = (yvals-1)+params.fmin;
      % loop over search directions (time delays)
      for ii=1:ndirs
        % phase factor
        phi = exp(-i*2*pi*ff.*tau(ii));
        % calculate SNR for each phase offset
        snr(ii,:) = real(sum(phi.*weight_up.*map_snr(templates_up),1) + ...
          sum(phi.*weight_down.*map_snr(templates_down),1));
      end
      snr_all = snr;
      % chose the time delay that produces the max SNR (for each template)
      [snr,tau_index] = max(snr, [], 1);
      maxtau = tau(tau_index);
    end
    % normalization-----------------------------------------
    % 'npix' normalizes SNR by the number of pixels in the track.  This appears
    % to be the best option.
    if strcmp(params.stochtrack.norm, 'npix')
       snr_gamma = snr./sqrt(npix);
    elseif strcmp(params.stochtrack.norm, 'mean')
       snr_gamma = snr./npix;
    elseif strcmp(params.stochtrack.norm, 'ansatz')
       snr_gamma = snr./(npix.^(3/4));
    end
    snr_gamma(npix==0) = 0;

  % other weighting schemes
  else
     fprintf('CAUTION: you have selected a non-standard ST normalization.\n');
    % 'N' normalizes SNR by the map size; all tracks are treated identically.
    if strcmp(params.stochtrack.norm, 'N');
      snr = real(sum(weight_up.*map_snr(templates_up),1) + ...
         sum(weight_down.*map_snr(templates_down),1));
      snr_gamma = snr/sqrt(N);
    % 'sigma' attempts to apply inverse variance weighting.  We expected this
    % to perform the best, but npix appears to be better.
    elseif strcmp(params.stochtrack.norm, 'sigma');
      y_up = ...
      sum( weight_up.*map.y(templates_up).*map.sigma(templates_up).^-2 , 1) ...
      ./ sum( weight_up.*map.sigma(templates_up).^-2, 1);
      y_down = ...
        sum( weight_down.*map.y(templates_down).* ...
        map.sigma(templates_down).^-2 , 1) ...
        ./ sum( weight_down.*map.sigma(templates_down).^-2, 1);
      var_up = sum( weight_up.^2.*map.sigma(templates_up).^-2, 1) ./ ...
        sum( weight_up.*map.sigma(templates_up).^-2 ).^2;
      var_down = sum( weight_down.^2.*map.sigma(templates_down).^-2, 1) ./ ...
        sum( weight_down.*map.sigma(templates_down).^-2 ).^2;
      ytot = (y_up./var_up + y_down./var_down) ./ (var_up.^-1 + var_down.^-1);
      sigtot = (var_up.^-1 + var_down.^-1).^-0.5;
      snr = ytot./sigtot;
      snr_gamma = real(snr);
    % 'dumb' applies no normalization whatsoever
    elseif strcmp(params.stochtrack.norm, 'dumb')
      snr = real(sum(weight_up.*map_snr(templates_up),1) + ...
         sum(weight_down.*map_snr(templates_down),1));
      snr_gamma = real(snr);
    % unrecognized option requested
    else
      error('Unknown normalization requested for stochtrack.');
    end
  end

  % set to zero the snr of any cluster with less than npix_th
  snr_gamma(npix<npix_th) = 0;

  % diagnostic test: check for snr_gamma=NaN
  if any(isnan(snr_gamma))
    fprintf('CAUTION: %i/%i stoch tracks found with SNR=NaN.\n', ...
      sum(isnan(snr_gamma)), T);
  end

  if params.stochtrack.saveMat
     npixs(aa,:) = gather(npix);
     snrs(aa,:) = gather(snr_gamma);

     if params.stochtrack.cbc.allsky==true
        snrs_all(aa,:,:) = gather(snr_all);
        taus(aa,:) = gather(maxtau);
     end
  end

  % find the loudest cluster and the associated trial number (for this time 
  % through the for loop)
  [snr_max0 trial0] = max(snr_gamma);

  % diagnostic cmap plot shows all tracks with SNR>5 on the same plot
  if params.savePlots
    cut = snr_gamma>5;
    cutidx = find(snr_gamma>5);
    templates_up_x = templates_up(:,cut);
    templates_down_x = templates_down(:,cut);
    weight_up_x = weight_up(:,cut);
    weight_down_x = weight_down(:,cut);
    [ctemplates_up tmp] = unique(templates_up_x);
    ctemplates_down = templates_down_x(tmp);
    cweights_up = weight_up_x(tmp);
    cweights_down = weight_down_x(tmp);
  end
  %----------------------------------------------------------------------------

  % keep track of snr_max for each time through the for-loop
  snr_max(aa) = gather(snr_max0);
  trial(aa) = gather(trial0);
  % capital T Trial allows us to assign a unique number to every trial in the
  % for loop
  Trial(aa) = gather(trial0 + (aa-1)*T);

  % if you don't have extra pixels set high enough, the code will fail if you
  % have a waveform that needs it
  K = length(weight_up(:,trial0));
  if K > max_pixels
    error(['Max number of pixels in track exceeded... please raise ' ...
      'params.stochtrack.extra_pixels by at least %d\n'], K - max_pixels);
  end

  % keep the weights associated with the snr_max track
  %mweight_up(:,aa) = gather(weight_up(:,trial0));
  %mweight_down(:,aa) = gather(weight_down(:,trial0));
  %mtemplates_up(:,aa) = gather(templates_up(:,trial0));
  %mtemplates_down(:,aa) = gather(templates_down(:,trial0));

  % adds the templates to the arrays with the correct padding
  mweight_up(:,aa) = padarray(gather(weight_up(:,trial0)), max_pixels-K, ...
    NaN, 'post');
  mweight_down(:,aa) = padarray(gather(weight_down(:,trial0)), ...
    max_pixels-K, NaN, 'post');
  mtemplates_up(:,aa) = padarray(gather(templates_up(:,trial0)), ...
    max_pixels-K, NaN, 'post');
  mtemplates_down(:,aa) = padarray(gather(templates_down(:,trial0)), ...
    max_pixels-K, NaN, 'post');

  % mH keeps track of the amplitude of the track
  if strcmp(params.stochtrack.norm, 'H')
    mH(:,aa) = padarray(gather(H(:,trial0)), max_pixels-K,NaN,'post');
  end

  % record npix value for highest SNR track
  mpix(aa) = gather(npix(trial0));

  if params.stochtrack.stochsky
    % In the first version of stochsky, tau used to be a single number.
    % Now it's an array in time, so we take the mean value.
%    mtau(aa) = gather(tau(trial0));
    mtau(aa) = gather(mean(tau(:,trial0)));
    % with updated code, record the best fit direction
    mra(aa) = gather(ra(trial0));
    mdec(aa) = gather(dec(trial0));
  end

  % if we are running with the doCBC option, record component masses
  if params.stochtrack.doCBC
    m1_max(aa) = gather(track_data.m1(trial0));
    m2_max(aa) = gather(track_data.m2(trial0));
    nstops_max(aa) = gather(track_data.nstops(trial0));
  end

  % if we are running with doECBC option, record eccentricity too
  if params.stochtrack.doECBC
    m1_max(aa) = gather(track_data.m1(trial0));
    m2_max(aa) = gather(track_data.m2(trial0));
    ecc_max(aa) = gather(track_data.eccentricities(trial0));
    nstops_max(aa) = gather(track_data.nstops(trial0));
  end

  % if we are running with cbc.allsky, record time delay
  if params.stochtrack.cbc.allsky
    tau_max(aa) = gather(tau(tau_index(trial0)));
  end

  % if we are running with the doRModes option, record rmodes parameters
  if params.stochtrack.doRModes || params.stochtrack.doExponential
    f0_max(aa) = gather(track_data.f0(trial0));
    alpha_max(aa) = gather(track_data.alpha(trial0));
  end

  if params.stochtrack.doAntiChirp
    alpha_max(aa) = gather(track_data.alpha(trial0));
    fmax_max(aa) = gather(track_data.fmax(trial0));
    f0_max(aa) = gather(track_data.f0(trial0));
  end

  % perodically print aa to stdout to track progress
  if mod(aa, 100)==0
    fprintf('%i\n', aa)
  end
end

% find the max for all times through the for loop
[snr_max tmp_idx] = max(snr_max);
trial = trial(tmp_idx);
Trial = Trial(tmp_idx);

% print the SNR to the screen
fprintf('max SNR = %1.1f, trial=%i/%i, npix=%.0f\n', snr_max, Trial, T*F,mpix(tmp_idx));

% optional mat file with snr and npix
if params.stochtrack.saveMat
  if params.stochtrack.cbc.allsky==true
    save(params.stochtrack.matfile,'npixs','snrs','snrs_all','taus');
  else
    save(params.stochtrack.matfile,'npixs','snrs');
  end
end

% record properties of the loudest cluster
max_cluster.snr_gamma = snr_max;
max_cluster.nPix = mpix(tmp_idx);
if params.stochtrack.stochsky
  max_cluster.tau = mtau(tmp_idx);
  max_cluster.ra = mra(tmp_idx);
  max_cluster.dec = mdec(tmp_idx);
end

% if doCBC option is on, record overall best fit component masses
if params.stochtrack.doCBC
  max_cluster.m1_max = m1_max(tmp_idx);
  max_cluster.m2_max = m2_max(tmp_idx);
  max_cluster.nstops_max = nstops_max(tmp_idx);
  fprintf('Max cluster: m1=%.5f, m2=%.5f, nstop=%d\n', max_cluster.m1_max, max_cluster.m2_max,max_cluster.nstops_max);
end

% if doECBC option is on, record eccentricity too
if params.stochtrack.doECBC
  max_cluster.m1_max = m1_max(tmp_idx);
  max_cluster.m2_max = m2_max(tmp_idx);
  max_cluster.nstops_max = nstops_max(tmp_idx);
  max_cluster.ecc_max = ecc_max(tmp_idx);
  fprintf('Max cluster: m1=%.5f, m2=%.5f, ecc=%.5f, nstop=%d\n', max_cluster.m1_max, max_cluster.m2_max, max_cluster.ecc_max, max_cluster.nstops_max);

end

% record best fit tau for cbc all-sky
if params.stochtrack.cbc.allsky
  max_cluster.tau_max = tau_max(tmp_idx);
end

% record best fit parameters for r-modes
if params.stochtrack.doRModes || params.stochtrack.doExponential
  max_cluster.f0_max = f0_max(tmp_idx);
  max_cluster.alpha_max = alpha_max(tmp_idx);
  fprintf('Max cluster: f0=%.5f, alpha=%.5f\n', max_cluster.f0_max, max_cluster.alpha_max);
end

if params.stochtrack.doRModes || params.stochtrack.doExponential
  max_cluster.f0_max = f0_max(tmp_idx);
  max_cluster.alpha_max = alpha_max(tmp_idx);
  max_cluster.fmax_max = fmax_max(tmp_idx);
  fprintf('Max cluster: f0=%.5f, alpha=%.5f, fmax=%.5f\n', max_cluster.f0_max, max_cluster.alpha_max, max_cluster.fmax_max);
end

% reconstructed track map (shows the loudest track)
rmap = gZeros(size(map.snr,1), size(map.snr,2), params);
[junk,indexes] = unique(mtemplates_up(:,tmp_idx));
indexes_2 = find(~isnan(mtemplates_up(indexes,tmp_idx)));
indexes_up = indexes(indexes_2);
rmap(mtemplates_up(indexes_up,tmp_idx)) = mweight_up(indexes_up,tmp_idx);
[junk,indexes] = unique(mtemplates_down(:,tmp_idx));
indexes_2 = find(~isnan(mtemplates_down(indexes,tmp_idx)));
indexes_down = indexes(indexes_2);
rmap(mtemplates_down(indexes_down,tmp_idx)) = mweight_down(indexes_down,tmp_idx);
rmap = rmap.*nanmap0;

% this plots the actual pixels (with the correct pixel SNR) of the track
smap = gZeros(size(map.snr,1), size(map.snr,2), params);

if params.stochtrack.cbc.allsky
   yvals = flipud(repmat(gColon(1,1,M,params)', 1, N)); 
   ff = (yvals-1)+params.fmin;
   phi = exp(-i*2*pi*ff.*max_cluster.tau_max);
elseif params.stochtrack.stochsky
   yvals = flipud(repmat(gColon(1,1,M,params)', 1, N));
   ff = (yvals-1)+params.fmin;
   phi = exp(-i*2*pi*ff.*max_cluster.tau);
else
   phi = gOnes(size(map.snr,1), size(map.snr,2), params);
end

smap(mtemplates_up(indexes_up,tmp_idx)) = ...
  real(map_snr(mtemplates_up(indexes_up,tmp_idx)) .* mweight_up(indexes_up,tmp_idx) .* phi(mtemplates_up(indexes_up,tmp_idx)));
smap(mtemplates_down(indexes_down,tmp_idx)) = ...
  real(map_snr(mtemplates_down(indexes_down,tmp_idx)) .* mweight_down(indexes_down,tmp_idx) .* phi(mtemplates_down(indexes_down,tmp_idx)));
smap = smap.*nanmap0;

% rmapH plots the amplitude corrected track
if strcmp(params.stochtrack.norm, 'H')
  rmapH = gZeros(size(map.snr,1), size(map.snr,2), params);
  rmapH(mtemplates_up(indexes_up,tmp_idx)) = mH(indexes_up,tmp_idx);
  rmapH(mtemplates_down(indexes_down,tmp_idx)) = mH(indexes_down,tmp_idx);
  rmapH = rmapH.*nanmap0;
end

% un-transpose the rmap if doTranspose is on
if params.stochtrack.doTranspose
  max_cluster.reconMax = rmap';
else
  max_cluster.reconMax = rmap;
end

% calculate Y and sigma
yr = rmap.*map.y;
sr = rmap.*map.sigma;
max_cluster.y_gamma = sum(sum(yr.*sr.^-2)) / sum(sum(sr.^-2));
max_cluster.sigma_gamma = sum(sum(sr.^-2))^-0.5;

% Gather GPU data
max_cluster.reconMax = gather(max_cluster.reconMax);
max_cluster.y_gamma = gather(max_cluster.y_gamma);
max_cluster.sigma_gamma = gather(max_cluster.sigma_gamma);

% Calculate maximum cluster information.                                                 
tvals = map.segstarttime - map.segstarttime(1);
fvals = map.f;
%tvals = sum(max_cluster.reconMax);
%fvals = sum(max_cluster.reconMax');
[r,c] = find(max_cluster.reconMax > 0);
max_cluster.tmin = tvals(min(c));
max_cluster.tmax = tvals(max(c));
max_cluster.gps_min = map.segstarttime(min(c));
max_cluster.gps_max = map.segstarttime(max(c));
max_cluster.fmin = fvals(min(r));
max_cluster.fmax = fvals(max(r));

% Compute fmin and fmax
%fvals = sum(max_cluster.reconMax');
%[junk,maxindex] = max(fvals);

%max_cluster.fmin = fvals(min(find(fvals>0)));
%max_cluster.fmax = fvals(max(find(fvals>0)));
%max_cluster.nPix = max_cluster.nPix;


% calculate SNR frac for all clusters on the map
totalSNR = sum(map.snr(:));
colsSNR=sum(map.snr(:),1)/totalSNR;
temp_idx=find(colsSNR==max(colsSNR));
max_cluster.SNRfrac=colsSNR(temp_idx);
max_cluster.SNRfracTime=map.segstarttime(temp_idx) - ...
    map.segstarttime(1);

idx = find(~isnan(smap));
max_cluster.SNRfrac=max(real(smap(idx)))/sum(real(smap(idx)));

% Gather GPU data
max_cluter.SNRfrac =gather(max_cluster.SNRfrac);

% make a diagnostic plot of the best fit
if params.savePlots
  %index = find(params.stochtrack.triggerGPS == map.segstarttime);
  %rmap(:,index) = -5;

  map.xvals=map.segstarttime+params.segmentDuration/4 - map.segstarttime(1);
  map.yvals = map.f;
  printmap(rmap, map.xvals, map.yvals, 't (s)', 'f (Hz)', 'SNR', [-1 1]);
  title(['max cluster SNR = ' num2str(max_cluster.snr_gamma)])
  print('-dpng', [params.plotdir '/rmap']);
  print('-depsc2', [params.plotdir '/rmap']);

% Eric: smap is temporarily commented out until it can be fixed.  This command
% was causing errors because smap is now a complex quantity since a recent 
% but fix.  The fix is to take the real part of smap.  However, a phase factor
% should first be applied to rotate the map to the right direction.
%  printmap(smap, map.xvals, map.yvals, 't (s)', 'f (Hz)', 'SNR', [-5, 5]);
%  print('-dpng', [params.plotdir '/smap']);
%  print('-depsc2', [params.plotdir '/smap']);
%  close;

  if strcmp(params.stochtrack.norm, 'H')
    printmap(rmapH, map.xvals, map.yvals, 't (s)', 'f (Hz)', 'SNR', ...
      [gather(max(rmapH(:)))/1e3 gather(max(rmapH(:)))]);
    print('-dpng', [params.plotdir '/rmap_H']);
    print('-depsc2', [params.plotdir '/rmap_H']);
    close;

    smap_sum = max(smap); rmapH_sum = max(rmapH);
    smap_sum = smap_sum / sum(smap_sum);
    rmapH_sum = rmapH_sum / sum(rmapH_sum);

% Eric: smap is temporarily commented out until it can be fixed.  This command
% was causing errors because smap is now a complex quantity since a recent 
% but fix.  The fix is to take the real part of smap.  However, a phase factor
% should first be applied to rotate the map to the right direction.    figure;
%    plot(map.xvals,rmapH_sum,'b*');
%    hold on
%    plot(map.xvals,smap_sum,'r*');
%    hold off
%    pretty;
%    xlabel('t (s)'); ylabel('SNR');
%    print('-dpng', [params.plotdir '/rmap_smap_compare']);
%    print('-depsc2', [params.plotdir '/rmap_smap_compare']);
%    close;

  end 

end

return
