function [yvals,xvals,ttvals,track_data] = stochtrack_magnetar(map, params, aa);
% function max_cluster = run_stochtrack(map, params)
%
% INPUT:
%    map  - STAMP map struct.
%    params - struct containing parameters for STAMP search.
%
% OUTPUT:
%    max_cluster (STRUCT)
%        max_cluster.nPix -> number of pixels in the largest cluster
%        max_cluster.snr_gamma -> SNR of the largest cluster
%        max_cluster.y_gamma -> Y (GW estimator) of largest cluster
%        max_cluster.sigma_gamma -> sigma of the above estimator
%        max_cluster.reconMax -> 2D array containing weight numbers for
%                                pixels contained the largest cluster and 0's
%                                at the location of other pixels
% Written by E Thrane and M Coughlin
% Contact: ethrane@ligo.caltech.edu, michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% map is MxN: M is the # of frequency bins and N is the number of segments
[M,N] = size(map.snr);

% define maxdur to be N (the map time dimension) unless otherwise specified
try
   params.stochtrack.maxdur;
catch
   params.stochtrack.maxdur = N;
end

% define maxband to be M (the same as the frequency dimension unless otherwise
% specified
try
  params.stochtrack.maxband;
catch
  params.stochtrack.maxband = M;
end

% stochtrack parameters-------------------------------------------------------
% number of trials
T = params.stochtrack.T;
% number of trials in for loop
F = params.stochtrack.F;
% minimum track duration (IN PIXELS)
mindur = params.stochtrack.mindur;
% maximum track duration (IN PIXELS)
maxdur = params.stochtrack.maxdur;
%-----------------------------------------------------------------------------

% initialize yvals array
yvals = gZeros(N,T,params);
xvals = gColon(1,1,N,params);
ttvals = gArray(map.segstarttime,params); 
d_foft = gOnes(N,T,params);
track_data = [];


%  fprintf('Using magnetar model templates ... \n');

delf =  floor((params.stochtrack.msmagnetar.max_f0 - params.stochtrack.msmagnetar.min_f0)/F);
f0s = gColon(params.stochtrack.msmagnetar.min_f0,delf, params.stochtrack.msmagnetar.max_f0, params)';

min_tau = params.stochtrack.msmagnetar.min_tau;
max_tau = params.stochtrack.msmagnetar.max_tau;
taus = gLogspace(log10(min_tau),log10(max_tau),params.stochtrack.msmagnetar.numTau, params)';


if F > length(f0s)
  f0s = padarray(f0s,F-length(f0s),params.stochtrack.msmagnetar.max_f0,'post');
end

bi_ndex = params.stochtrack.msmagnetar.bi;
nns = 1./(1 - bi_ndex);

thisf0 = f0s(aa);
f0 = repmat(thisf0, 1, size(yvals,1))';

xvalsHold = xvals;

xvalsAll = gZeros(N,T,params); yvalsAll = gZeros(N,T,params);

for bb = 1:length(taus)
  for kk = 1:length(nns)    
    nn = nns(kk);
    
    % Loop over the braking index
    
    tau = taus(bb);  
    
    %keyboard
    t = (gColon(1,1,N,params)' - 1/2) * (params.segmentDuration);
    foft = f0 .*(1 + t./tau).^nn;
    
    
    
    % Convert frequency to map indices
    foft = foft - params.fmin + 1;
    yvals = foft;
    yvals = repmat(yvals, 1, size(yvals,1));

    % Choose coalescense times: the smallest possible stop time is given by
    % minstop.  The largest possible stop time is N = the last pixel in the map.
    if T < size(yvals,1)
      nstops = N.*gRand(T,1,params);
      nstops = round(nstops);
    else
      nstops = gColon(1,1,N,params);
      nstops = padarray(nstops',T-length(nstops),N,'post');
    end
    track_data.nstops = nstops;
    
    nstops = repmat(nstops, 1, size(yvals,1))';

    % Move waveform to correct starting index
    xvals = repmat(xvalsHold', 1, size(yvals,1));
    xvals = mod(xvals + nstops - 1,N) + 1;
    
    % remove values below 0
    yvals(yvals<0) = NaN;

    % Remove any portion of the waveform after the maximum
    xvals_max_index = xvals(end,:);
    xvals_max_index = repmat(xvals_max_index', 1, size(yvals,1))';
    yvals(xvals < xvals_max_index) = NaN;

    
    
    indexes = (bb-1)*T+1:bb*T;
    xvalsAll(:,indexes) = xvals; yvalsAll(:,indexes) = yvals;
    tausAll(indexes) = taus(bb);
    
    

    xvals = xvalsAll; yvals = yvalsAll; tau = tausAll;
    
    track_data.f0 = repmat(thisf0,1,length(tau)*T);
    track_data.tau = tau;
 
  end

 
end

return
