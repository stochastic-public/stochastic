function gM = gLinspace(a, b, n, params)
% function gM = gArray(M, params)

if params.doGPU
  gM = gpuArray.linspace(a,b,n);
else
  gM = linspace(a,b,n);
end

return

