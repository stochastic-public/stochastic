function gM = gColon(a, b, c, params)
% function gM = gColon(a, b, params)

if params.doGPU
  gM = gpuArray.colon(a,b,c);
else 
  gM = a:b:c;
end

return
