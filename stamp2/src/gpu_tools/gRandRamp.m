function gM = gRandRamp(a, b, params)
% function gM = gRandRamp(a, b, params)
% generates random numbers using a ramp distribution (which prefers larger 
% numbers to small numbers).  the output array contains numbers on [0,1].
% in order to run, you will need to define params.fmin, params.fmax.

% the array of uniformly distributed numbers we will need later
if params.doGPU
  gM0 = gpuArray.rand(a,b,params.gpu.precision);
else 
  gM0 = rand(a,b);
end

% is this the first time through
try
  icdf = params.pass.gRandRamp;
catch
  % first time through we must calculate the inverse cdf
  % we begin by guessing a suitable slope and y-intercept
  m = params.fmax/params.fmin;
  b = params.fmin;
  % define frequency array
  f = [params.fmin : params.fmax];
  % define ramp pdf
  pf = m*f + b;
  % normalize
  m = m/sum(pf);
  b = b/sum(pf);
  pf = m*f + b;
  
  % calculate cdf analytically
  f1 = params.fmin;
  f2 = params.fmax;
  cf = (m*f.^2/2 + b*f) - (m*f1.^2/2 + b*f1);
  
  % calculate inverse
  p = [0:0.001:1];
  icdf = interp1(cf, f, p);

  % do not repeat this calculation
  params.pass.gRandRamp = icdf;
end

% interpolate random data
gM = interp1(p, icdf, gM0);

% convert from frequencies into bins
%gM = (params.fmax - params.fmin)*(gM-params.fmin)/(params.fmax-params.fmin) + 1;

% convert distribution to [0,1]
gM = (gM-params.fmin)/(params.fmax-params.fmin);

return
