function [det1 det2] = ifo2dets(params)
% function [det1 det2] = ifo2dets(params)
% by the STAMP working group

% make sure the input is correct
if length(params.ifo1)~=2 | length(params.ifo2)~=2
  error('ifo2dets expects ifo1 = H1 for example');
end

% convert single letter in parameter value into a full site name
if strcmp(params.ifo1(1),'H'), site1='LHO'; end
if strcmp(params.ifo1(1),'L'), site1='LLO'; end
if strcmp(params.ifo1(1),'V'), site1='VIRGO'; end

if strcmp(params.ifo2(1),'H'), site2='LHO'; end
if strcmp(params.ifo2(1),'L'), site2='LLO'; end
if strcmp(params.ifo2(1),'V'), site2='VIRGO'; end

% getdetector returns a struct with details about each site
det1 = getdetector(site1);
det2 = getdetector(site2);

return
