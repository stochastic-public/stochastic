function params = longsegs(params, paths, files)
% function params = longsegs(params)
% Eric Thrane
% this function adjusts start and end GPS to make sure they line up with the 
% segment start times in the STAMP mat files.  If necessary, startGPS is
% decreased and endGPS is increased so that the map is always at least as big
% as initially requested.  This code works by reading in one mat file to learn
% the segment duration and to get a reference time.
%
% If for some reasons, we 
% wish to avoid this, we could in principle pass tref and segmentDuration as
% parameters.  I am avoiding that in this implementation however because it 
% seems like the type of thing that could easily cause problems if someone
% forgets to update those hypothetical parameters after generating new data.

% load the first file to get the segmentDuration and to calculate a reference
% time
M=load([paths{1} '/' files{1}]);
params.segmentDuration = M.params.segmentDuration;
tref = M.map.segstarttime(1);

% check if this option is being left on unnecessarily
if params.segmentDuration==1
  fprintf('You can set longsegs==false when params.segmentDuration=1.\n');
end

% calculate correction factors
t1 = mod(params.startGPS-tref, params.segmentDuration);
t2 = mod(-params.endGPS+tref, params.segmentDuration);

% make params.startGPS and endGPS match up with segment start times
params.startGPS = params.startGPS - t1;
params.endGPS = params.endGPS + t2;

return
