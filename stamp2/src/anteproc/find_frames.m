function [frames,gps,durs] = find_frames(params)
% function [frames,gps,durs] = find_frames(params)
%
% Calls gw_data_find to get paths to GW frame files containing the
% data requested by the user.
%
% Input:
%   params - params struct; need start and end GPS times.
%
% Output:
%   frames - cell array of strings, each element corresponds to the
%            path to a frame file.
%   gps    - array of GPS start times of the data contained in each frame.
%   durs   - array of frame durations.
%
% Useful information on frame types:
% https://wiki.ligo.org/Help/AGuideToFindingGravitationalWaveFrameData
%
% Suggested frame types and channels for each science run/IFO:
%  S5: H1_RDS_C03_L2, L1_RDS_C03_L2. Channel: LSC-STRAIN.
%  S6: H1_LDAS_C02_L2, L1_LDAS_C02_L2. Channel: LDAS-STRAIN.
%  ER8/O1: H1_HOFT_C00, L1_HOFT_C00. Channel: GDS-CALIB_STRAIN.
%  VSR1: HrecV2_16384Hz. Channel: h_16384Hz.
%  VSR2: HrecV3. Channel: h_16384Hz.
%  VSR3: HrecV2. Channel: h_16384Hz.
%  VSR4: HrecOnline. Channel: h_16384Hz.
%
%  Note: Virgo frame types are based on availability at CIT as of
%  October 2015.
%  All channels in the frames have are actually like IFO:CHANNEL
%  (example: H1:LSC-STRAIN), but the STAMP code automatically
%  adds the prefix.
%
% Written by Tanner Prestegard (prestegard@physics.umn.edu) 10/19/2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

setenv('LD_LIBRARY_LOAD','');

% Get GPS times from params.
startGPS = params.job.startGPS;
endGPS = round(params.job.startGPS + params.job.duration + params.largeShiftTime1);
%display(num2str(endGPS))

% Call gw_data_find.
cmd = ['export LD_LIBRARY_PATH="";/usr/bin/gw_data_find -o ' ...
       params.ifo1(1) ' -t ' params.frameType1 ' -s ' ...
       num2str(params.job.startGPS) ' -e ' num2str(endGPS) ' -u file'];
%display(['/usr/bin/gw_data_find -o ' ...
%       params.ifo1(1) ' -t ' params.frameType1 ' -s ' ...
%       num2str(params.job.startGPS) ' -e ' num2str(endGPS) ' -u file'])

[status,frame_string] = system(cmd);

%display(['gw_data_find status: ' num2str(status)])
if status
    error(['find frames error : ' frame_string]);
end

% Split frame_string on newlines into a cell array of strings.
% Each entry corresponds to a frame file.
frame_list = strread(frame_string,'%s','delimiter','\n');

% If no frames are found, throw an error.
if isempty(frame_list)
  fprintf('cmd:%s\n',cmd);
  fprintf('frames:%s\n',frames{:});
  fprintf('frame_string :%s\n',frame_string);
  error(['No frames found. Check your frame type; otherwise, the frames you' ...
	 ' want may not be available on this cluster.']);
end

% Remove file://localhost/ from the beginning of each entry.
frames = regexp(frame_list,'^file://localhost(.*)','tokens','once');
frames = [frames{:}]';

% Get GPS times and durations.
temp = regexp(frames,'-(\d{9,10}(|\.\d+))-','tokens','once');
gps = cellfun(@(x) str2num(x{1}), temp);
temp = regexp(frames,'-(\d+|\d+\.\d+)\.gwf','tokens','once');
durs = cellfun(@(x) str2num(x{1}), temp);

if size(frames,1)==0
  fprintf('Data file not found:\n');
  fprintf('cmd:%s\n',cmd);
  fprintf('frames:%s\n',frames{:});
end
return;
