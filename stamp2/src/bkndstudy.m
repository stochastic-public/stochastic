function map = bkndstudy(map, params)
% function map = bkndstudy(map, params);
% Eric Thrane: removes times with NaN from ft-maps in order to create an
% ft-map with no missing data.  This stitched-together map can be used for
% background estimation studies.

% find the times where there is missing data
flatmap = sum(map.cc,1);
cut = ~isnan(flatmap);

% check with expected map size

try
  N = 2*params.bkndstudydur-1;
catch
  error('Define params.bkndstudydur to check pseudo job lengths.');
end

if N~=sum(cut)
  fprintf('N=%i, sum(cut)=%i\n', N, sum(cut));
  error('Pseudo job does not have the correct length.');
end

% replace segstartime with pretend values as if no gaps are present
map.segstarttime = [0:sum(cut)-1]*map.segDur/2 + map.segstarttime(1);

% remove missing times from ft-maps
map.cc = map.cc(:,cut);
map.sensInt = map.sensInt(:,cut);
map.ccVar = map.ccVar(cut);
map.naiP1 = map.naiP1(:,cut);
map.naiP2 = map.naiP2(:,cut);
map.P1 = map.P1(:,cut);
map.P2 = map.P2(:,cut);

return
