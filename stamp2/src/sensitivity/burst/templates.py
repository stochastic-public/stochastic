
import random
import numpy as np
import pycbc.filter

def bezier(params,delta_t,frequencies):

    # minimum track duration (IN PIXELS)
    mindur = params["stochtrack"]["mindur"]
    # maximum track duration (IN PIXELS)
    maxdur = params["stochtrack"]["maxdur"]
    # maximum track duration (IN PIXELS)
    fmin = params["stochtrack"]["fmin"]
    # maximum track duration (IN PIXELS)
    fmax = params["stochtrack"]["fmax"]
    # maximum track duration (IN PIXELS)
    fdiffmin = params["stochtrack"]["fdiffmin"]
    # maximum track duration (IN PIXELS)
    fdiffmax = params["stochtrack"]["fdiffmax"]

    T = params["stochtrack"]["T"]

    # create track durations (in pixels) between mindur and maxdur=N
    durs = (maxdur-mindur)*random.random() + mindur

    # create track start times between 1 and N-durs
    nstarts = 0.0
    # calculate stop times
    nstops = nstarts + durs

    # create start frequencies between 1 and M
    fstart = (fmax-fmin)*random.random() + fmin
    #fstop = (fmax-fstart-fdiff)*random.random() + fstart
    #fstop = (fmax-fstart-fdiff)*random.random() + fstart

    fdifftot = fmax-fstart 
    if fdifftot < fdiffmax:
        fdiffmax = fdifftot
    fstop = (fdiffmax-fdiffmin)*random.random() + fstart

    # calculate Bezier midpoint to be between fstart and fstop
    fmid = (fstop-fstart)*random.random() + fstart
    tmid = nstarts + durs*random.random()

    # define Bezier parameter on [0,1] with step size 0.01
    p0 = nstarts
    p1 = tmid
    p2 = nstops

    # define evenly spaced Bezier xvals and work backward to get tt
    B1 = np.arange(0,T,delta_t)
    a = p0 - 2*p1 + p2
    b = 2*(p1-p0)
    c = p0 - B1
    tt = (-b + np.sqrt(b**2 - 4*a*c)) / (2*a)

    # calculate Bezier curve B(t) for each trial
    p0 = fstart
    p1 = fmid
    p2 = fstop
    B2 = ((1-tt)**2)*p0 + 2*(1-tt)*tt*p1 + (tt**2)*p2
    # enforce start and stop times
    B2[np.where(tt<0)[0]] = 0.0
    B2[np.where(tt>1)[0]] = 0.0
    yvals = B2

    # Cut yvals outside of map
    yvals[np.where(yvals<fstart)[0]] = 0.0
    yvals[np.where(yvals>fstop)[0]] = 0.0
    yvals[np.isnan(yvals)] = 0.0

    delta_f = frequencies[1] - frequencies[0]
    template = np.histogram(yvals, bins=np.append(frequencies,frequencies[-1]+delta_f))[0]
    template[0] = 0.0
    #template = template / np.sum(template)
    template = template / durs
    template = pycbc.types.FrequencySeries(template,delta_f=delta_f,dtype=pycbc.types.complex128)

    return fstart,durs,yvals,template

