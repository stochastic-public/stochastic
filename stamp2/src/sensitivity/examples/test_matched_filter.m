
params.psdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/LIGOsrdPSD_40Hz.txt';
%params.psdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/LIGOsrdPSD_40Hz.txt';
params.psdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd_10Hz.txt';

params.gps = 1000000000;

params.flow = 40;
params.fhigh = 1000;

% Set frequency
params.fs = 4096;
fs = params.fs;

% PSD file
psd = load(params.psdFile);

% Range of frequency band
flow = params.flow;
fhigh = params.fhigh;

dataStartTime = 946076559.088871717;
sampleRate = 4096; 
params.stamp.mdcdir = '/home/mcoughlin/STAMP/Magnetar/inj_anteproc/1/injframes_new/A/mag-a/H1/';
params.stamp.mdcchannel = 'H1:STRAIN';
params.stamp.alpha = 1;

%params.stamp.mdcdir = '/home/mcoughlin/STAMP/Magnetar/inj_anteproc/1/injframes_new/A/mag-a/L1/';
%params.stamp.mdcchannel = 'L1:STRAIN';

vector = zeros(250*sampleRate,1)';
ht2 = load_mdc(params, vector, dataStartTime, sampleRate);
ht2 = ht2';
ht1 = ht2;

H1SNR=matchedfilter(ht1, ht2, psd, fs, flow, fhigh);
fprintf('H1 SNR: %.2f\n',H1SNR);

params.stamp.mdcdir = '/home/mcoughlin/STAMP/Magnetar/inj_anteproc/1/injframes_new/A/mag-a/L1/';
params.stamp.mdcchannel = 'L1:STRAIN';

vector = zeros(250*sampleRate,1)';
ht2 = load_mdc(params, vector, dataStartTime, sampleRate);
ht2 = ht2';
ht1 = ht2;

L1SNR=matchedfilter(ht1, ht2, psd, fs, flow, fhigh);
fprintf('L1 SNR: %.2f\n',L1SNR);

SNR = sqrt(H1SNR^2 + L1SNR^2);
fprintf('SNR: %.2f\n',SNR);



