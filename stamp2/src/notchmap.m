function x = notchmap( x, params, fmin, fmax)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function map = maskmaps(map, params)
% Eric Thrane: applies user-specified frequency bin notches
% Sharan Banagiri:Added ability to notch a specific number of bins around the specified frequency - adapted in part from constructFreqMask and from maskmaps
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Read in frequencies.txt file and read in notch list for frequencies and number of bins to remove.



if exist([params.searchPath '/tmp/frequencies.txt']) == 2
  fID   = fopen([params.searchPath '/tmp/frequencies.txt']);
else
  fprintf('File for freq mask doesnt exist. No notching done...\n')
  return
end

notchData = textscan(fID, '%s');


% make a list of the notch frequencies and the numbers of bins to notch around each frequency
if length(notchData{1})==4
  params.StampFreqsToRemove = str2double(regexp(cell2mat(notchData{1}(2)), ',', 'split'));
params.StampBinsToRemove =  str2double(regexp(cell2mat(notchData{1}(4)), ',', 'split'));

elseif length(notchData{1}) == 3
  params.StampFreqsToRemove = str2double(regexp(notchData{1}(2), ',', 'split'));
else
   params.StampFreqsToRemove = [];
params.StampBinsToRemove = ones(size(params.StampFreqsToRemove));
end


if isnan(params.StampFreqsToRemove(end))
params.StampFreqsToRemove = params.StampFreqsToRemove(1:end-1);
end

if isnan(params.StampBinsToRemove(end))
params.StampBinsToRemove = params.StampBinsToRemove(1:end-1);
end

if isempty(params.StampFreqsToRemove)
   fprintf('List of freq masks is empty. No notching done...\n')
   return
end

% not sure why this is here, its a carry over 
  if params.segmentDuration < 1
  ff = fmin:(1/params.segmentDuration):fmax;
[params.StampFreqsToRemove, I1, I2] = intersect(ff,params.StampFreqsToRemove);
params.StampBinsToRemove = params.StampBinsToRemove(I2);
end

% an index of all the bins to be nothced
numFreqs = 1+ floor((fmax - fmin)/params.deltaF);
notchIndex = zeros(numFreqs, 1); 

% is notch list is empty, then return to the previous routine
if isempty(params.StampFreqsToRemove)
    return
end

if isempty(params.StampBinsToRemove) % If # of bins is not specified, then it is taken to be one by default for each freqency
params.StampBinsToRemove =  ones(length(params.StampFreqsToRemove));
end


% make sure all the notch frequencies which are not in the allowed freq range
allowedIndex = params.StampFreqsToRemove - params.deltaF*floor((params.StampBinsToRemove - 0.5)/2)>=fmin & params.StampFreqsToRemove+params.deltaF*floor((params.StampBinsToRemove + 0.5)/2)<=fmax;


params.StampBinsToRemove = params.StampBinsToRemove(allowedIndex);
params.StampFreqsToRemove = params.StampFreqsToRemove(allowedIndex);


for  ii = 1:length(params.StampFreqsToRemove)
   index = floor((params.StampFreqsToRemove(ii) - fmin)/params.deltaF) + 1; 
   
   if mod(params.StampBinsToRemove(ii), 2) == 0
      index_low = index - (params.StampBinsToRemove(ii))/2  + 1;
      index_high = index + (params.StampBinsToRemove(ii))/2;
   else
     index_low = index - (params.StampBinsToRemove(ii)-1)/2;
     index_high = index + (params.StampBinsToRemove(ii)-1)/2;
   end

   notchIndex(index_low:index_high) = 1;
end
notchIndex  = logical(notchIndex);



% apply notches
x(notchIndex, :) = NaN;





return
