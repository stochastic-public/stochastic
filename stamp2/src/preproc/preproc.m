function [map, params] = preproc(paramsFile, jobsFile, varargin)
% function [map, params] = preproc(paramsFile, jobsFile, varargin)
%
%  preproc --- main routine for running the stochastic search
%
%  preproc(paramsFile, jobsFile, jobNumber) executes the stochastic
%  search specified by a set of parameters on the chosen job. The jobNumber
%  must be an integer from 0 up to the number of non-commented lines in
%  jobsFile. A jobNumber of 0 is a "dummy" job which can be used to
%  quickly run the executable when compiled under Matlab R14, and check that
%  the parameters are correct.
%
%  The parameter values are written to a file as are any or all of the
%  following:
%
%  - CC statistic values and theoretical sigmas
%  - naive theoretical sigmas (calculated from the analysis segment)
%  - CC spectra
%  - sensitivity integrands (the integrand of 1/theoretical variance)
%  - (complex) coherence between two channels
%  - optimal filter functions
%  - calibrated PSDs
%
%  Simulated stochastic background signals can also be injected into the
%  data if desired.
%
%  Routine written by Joseph D. Romano, John T. Whelan, Martin McHugh,
%  and Vuk Mandic.
%  Contact Joseph.Romano@astro.cf.ac.uk, john.whelan@ligo.org,
%  mmchugh@loyno.edu, and/or vmandic@ligo.caltech.edu
%
%  modifications by Eric Thrane and the STAMP working group.
%  $Id: preproc.m,v 1.4 2008-12-10 20:59:39 ethrane Exp $
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;
ddmmyyyyhhmmss  = datestr(now);

% convert string to numeric input argument (needed for compiled matlab) 
if(nargin == 3) % if job number is specified
  jobNumber = strassign(varargin{1});
  % read in job start time and duration from a file
  [ignore1, startTimes, ignore2, jobDurations] = ...
    textread(jobsFile, '%n %n %n %n', -1, 'commentstyle', 'matlab');

  % Check bounds of jobNumber. If jobNumber is zero,
  % this is a dry run to verify parameters so it is
  % not an error.
  if (jobNumber == 0)
    warning('Dummy job number 0, all parameters verified - exiting');
    return;
  end;
  if (jobNumber < 0 | jobNumber > length(startTimes))
    error(sprintf('Job number %d outside range of available jobs %d', ...
      jobNumber, length(startTimes)));
  end;

  startTime   = startTimes(jobNumber);
  jobDuration = jobDurations(jobNumber);
elseif(nargin == 4) % startGPS and stopGPS specified
  startTime   = strassign(varargin{1});
  jobDuration = strassign(varargin{2}) - strassign(varargin{1});
else
  error('Error: the number of arguments should be 3 or 4.');
end

% read in params structure from a file second argument tells rPFF2 that it is  
% NOT being called by anteproc
params = readParamsFromFile2(paramsFile, false);

% (obsolete on 10/9/14)
%params = readParamsFromFile(paramsFile);

% add additional parameters
params.seedad = varargin{1}; % seed to distinguish between different jobs
params.nargin = nargin; % to determine which cachefile method to use

% store job start time and duration
params.job.startGPS = startTime;
params.job.duration = jobDuration;

% use startGPS for on-the-fly injections if necessary
if params.stampinj && strcmp(params.pass.stamp.inj_type, 'fly')
  params.pass.stamp.startGPS = params.job.startGPS;
end

% check if everything is set and, if not, use default values 
% (obsolete on 10/9/14).  this is now performed by readParamsFromFile2
%params = preprocDefaults(params);

% add few more things to the params structure
params.ddmmyyyyhhmmss = ddmmyyyyhhmmss;
paramsFile = paramsFile;
params.jobsFile = jobsFile;

% if Monte Carlo is requested, read in noise or signal files
mc.init=true;
mc2.init=true;
if params.doDetectorNoiseSim
  mc.transfer = load(params.DetectorNoiseFile);
  if params.doDifferentNoise
     mc2.transfer = load(params.DetectorNoiseFile2);
  else
     mc2 = mc;
  end
end

%determine bad GPS times, if given...
if params.doBadGPSTimes
  if isempty(params.badGPSTimesFile)
    badtimesstart = 9999999999;
    badtimesend = 0;
  else
    [badtimesstart,badtimesend] = textread(params.badGPSTimesFile, ...
      '%f%f\n',-1,'commentstyle','matlab');
  end
end

% set total number of discrete frequencies
numFreqs = floor((params.fhigh-params.flow)/params.deltaF)+1;

% initialize lastLoadedDataEnd variables
lastLoadedDataEnd1 = 0;
lastLoadedDataEnd2 = 0;

% get appropriate detector structure for each ifo
if (isnan(params.azimuth1))
  detector1 = getdetector(params.site1);
else
  detector1 = getdetector(params.site1,params.azimuth1);
end
if (isnan(params.azimuth2))
  detector2 = getdetector(params.site2);
else
  detector2 = getdetector(params.site2,params.azimuth2);
end

% construct filter coefficients for high-pass filtering
if params.doHighPass1
  [b1,a1] = butter(params.highPassOrder1, ...
    params.highPassFreq1/(params.resampleRate1/2), 'high');
end

if params.doHighPass2
  [b2,a2] = butter(params.highPassOrder2, params.highPassFreq2/(params.resampleRate2/2), 'high');
end

% set values for psd estimation (on resampled data, HP filtered data)
psdFFTLength1 = params.resampleRate1*(1/params.deltaF);
psdOverlapLength1 = psdFFTLength1/2; 
psdWindow1 = hann(psdFFTLength1);
psdNFFT1 = psdFFTLength1; 
detrendFlag1  = 'onesided';

psdFFTLength2 = params.resampleRate2*(1/params.deltaF);
psdOverlapLength2 = psdFFTLength2/2;
psdWindow2  = hann(psdFFTLength2); 
psdNFFT2 = psdFFTLength2;
detrendFlag2  = 'onesided';

% set values for data windowing, zero-padding, and FFT
if params.doOverlap
  params.hannDuration1=params.segmentDuration;
  params.hannDuration2=params.segmentDuration;
end;
numPoints1    = params.segmentDuration*params.resampleRate1; 
dataWindow1   = tukeywin(numPoints1, params.hannDuration1/params.segmentDuration);
fftLength1    = 2*numPoints1;

numPoints2    = params.segmentDuration*params.resampleRate2;
dataWindow2   = tukeywin(numPoints2, params.hannDuration2/params.segmentDuration);
fftLength2    = 2*numPoints2;

% construct frequency mask for later use
data = constructFreqMask(params.flow, params.fhigh, params.deltaF, ...
                         params.freqsToRemove, params.nBinsToRemove, params.doFreqMask);
mask = constructFreqSeries(data, params.flow, params.deltaF);

% frequency vector and overlap reduction function
f = params.flow + params.deltaF*transpose([0:numFreqs-1]);
data  = overlapreductionfunction(f, detector1, detector2);
% If one of the data streams is params.heterodyned, set gamma.symmetry to 0
% so that negative frequencies are not included in the normalization.
if params.heterodyned
  gamma = constructFreqSeries(data, params.flow, params.deltaF, 0);
else
  gamma = constructFreqSeries(data, params.flow, params.deltaF, 1);
end

% For STAMP, overlap reduction factor is unity (similar to radiometer)
% and H(f) (= fRef) is flat in strain power
if params.stochmap
  gamma.data = ones(length(gamma.data),1);
  params = rmfield(params,'fRef');
  params.fRef.data = ones(length(gamma.data),1);
  params.fRef.flow = params.flow;
  params.fRef.deltaF = params.deltaF;
end
  
% construct name of gps times files and frame cache files for this job
if(nargin == 3) % if job number is specified
  gpsTimesFile1 = ...
    [params.gpsTimesPath1 'gpsTimes' params.ifo1(1) '.' num2str(jobNumber) '.txt'];
  gpsTimesFile2 = ...
    [params.gpsTimesPath2 'gpsTimes' params.ifo2(1) '.' num2str(jobNumber) '.txt'];
  frameCacheFile1 = ...
    [params.frameCachePath1 'frameFiles' params.ifo1(1) '.' num2str(jobNumber) '.txt'];
  frameCacheFile2 = ...
    [params.frameCachePath2 'frameFiles' params.ifo2(1) '.' num2str(jobNumber) '.txt'];
elseif(nargin == 4) % startGPS and stopGPS specified
  gpsTimesFile1 = []; % just for nameshake
  gpsTimesFile2 = [];
  frameCacheFile1 = [];
  frameCacheFile2 = [];
end
 
% channel names
channelName1 = [params.ifo1 ':' params.ASQchannel1];
channelName2 = [params.ifo2 ':' params.ASQchannel2];

% read in calibration info

if ( ~strncmp(params.alphaBetaFile1,   'none', length(params.alphaBetaFile1))   & ...
     ~strncmp(params.calCavGainFile1,  'none', length(params.calCavGainFile1))  & ...
     ~strncmp(params.calResponseFile1, 'none', length(params.calResponseFile1)) )
[t1, f1, R01, C01, alpha1, gamma1] = ...
  readCalibrationFromFiles(params.alphaBetaFile1, params.calCavGainFile1, params.calResponseFile1);
end;

if ( ~strncmp(params.alphaBetaFile2,   'none', length(params.alphaBetaFile2))   & ...
     ~strncmp(params.calCavGainFile2,  'none', length(params.calCavGainFile2))  & ...
     ~strncmp(params.calResponseFile2, 'none', length(params.calResponseFile2)) )
[t2, f2, R02, C02, alpha2, gamma2] = ...
  readCalibrationFromFiles(params.alphaBetaFile2, params.calCavGainFile2, params.calResponseFile2);
end;

% check that params.numSegmentsPerInterval is odd    
if mod(params.numSegmentsPerInterval,2)==0
  error('params.numSegmentsPerInterval must be odd');
end

% determine number of intervals and segments to analyse
bufferSecsMax = max(params.bufferSecs1,params.bufferSecs2);


if ~params.doSidereal %do not worry about sidereal times
  M = floor( (jobDuration - 2*bufferSecsMax)/params.segmentDuration );

  if params.doOverlap
    if(mod((jobDuration-2*bufferSecsMax)/params.segmentDuration,0.5)==0)
      M = (jobDuration - 2*bufferSecsMax)/params.segmentDuration;
    end
    numSegmentsTotal = 2*M-1;
    numIntervalsTotal = 2*(M - (params.numSegmentsPerInterval-1)) - 1;
    intervalTimeStride = params.segmentDuration/2;
  else 
    numSegmentsTotal = M;
    numIntervalsTotal = M - (params.numSegmentsPerInterval-1);
    intervalTimeStride = params.segmentDuration;
  end

  centeredStartTime = startTime + bufferSecsMax + ...
    floor( (jobDuration - 2*bufferSecsMax - M*params.segmentDuration)/ 2 );

else %calculate parameters compatible with the sidereal time
  srfac = 23.9344696 / 24; %sidereal time conversion factor
  srtime = GPStoGreenwichMeanSiderealTime(startTime) * 3600;
  md = mod(srtime,params.segmentDuration/srfac);
  centeredStartTime = round(startTime + params.segmentDuration - md*srfac); 

  %this is the start time of the first segment in this job that 
  %is compatible with the sidereal timing, but we still have to 
  %check that there is enough time for the preceding buffer

  if centeredStartTime - startTime < bufferSecsMax
    centeredStartTime = centeredStartTime + params.segmentDuration;
  end

  %now we can calculate the remaining bookkeeping variables
  M = floor( (jobDuration - bufferSecsMax - centeredStartTime + ...
              startTime) / params.segmentDuration );

  if params.doOverlap
    if(mod((jobDuration-2*bufferSecsMax)/params.segmentDuration,0.5)==0)
      M = (jobDuration - 2*bufferSecsMax)/params.segmentDuration;
    end
    numIntervalsTotal = 2*(M - (params.numSegmentsPerInterval-1)) - 1;
    intervalTimeStride = params.segmentDuration/2;
  else 
    numIntervalsTotal = M - (params.numSegmentsPerInterval-1);
    intervalTimeStride = params.segmentDuration;
  end
end

if(numIntervalsTotal<1)
  error('Job duration is very small; provide a different start and end GPS times.');
end

isFirstPass=true;
% analyse the data
for I=1:numIntervalsTotal

  badSegmentData = false;
  badResponse = false;

  intervalStartTime = centeredStartTime + (I-1)*intervalTimeStride;

  % check if first pass through the loop
  if isFirstPass

    for J=1:params.numSegmentsPerInterval

      % read in time-series data from frames
      dataStartTime1 = intervalStartTime + (J-1)*params.segmentDuration - params.bufferSecs1;
      dataStartTime2 = intervalStartTime + (J-1)*params.segmentDuration - params.bufferSecs2;

      dataDuration1 = params.segmentDuration + 2*params.bufferSecs1;
      dataDuration2 = params.segmentDuration + 2*params.bufferSecs2;

      if dataStartTime1+dataDuration1 > lastLoadedDataEnd1
        lastLoadedDataEnd1 = min(dataStartTime1+params.minDataLoadLength,startTime+jobDuration);
        lastLoadedDataStart1 = dataStartTime1;
        tmpDuration = lastLoadedDataEnd1 - lastLoadedDataStart1;
        [longadcdata1, data1OK, params] = readTimeSeriesData2(channelName1,...
                              dataStartTime1, tmpDuration,...
                              params.frameType1, params.frameDuration1,...
                              gpsTimesFile1, frameCacheFile1, ...
                              params, mc);
      end


      if dataStartTime2+dataDuration2 > lastLoadedDataEnd2
        lastLoadedDataEnd2 = min(dataStartTime2+params.minDataLoadLength,startTime+jobDuration);
        lastLoadedDataStart2 = dataStartTime2;
        tmpDuration = lastLoadedDataEnd2 - lastLoadedDataStart2;  
        [longadcdata2, data2OK, params] = readTimeSeriesData2(channelName2,...
                                dataStartTime2,tmpDuration,...
                                params.frameType2, params.frameDuration2,...
                                gpsTimesFile2, frameCacheFile2, ...
                                params, mc2);
      end

      startindex = (dataStartTime1 - lastLoadedDataStart1) ...
                      /longadcdata1.deltaT + 1;
      endindex = (dataStartTime1 + dataDuration1 - lastLoadedDataStart1) ...
                      /longadcdata1.deltaT;
      adcdata1.data = longadcdata1.data(startindex:endindex);
      adcdata1.tlow = dataStartTime1;
      adcdata1.deltaT = longadcdata1.deltaT;

      startindex = (dataStartTime2 - lastLoadedDataStart2) ...
                      /longadcdata2.deltaT + 1;
      endindex = (dataStartTime2 + dataDuration2 - lastLoadedDataStart2) ...
                      /longadcdata2.deltaT;
      adcdata2.data = longadcdata2.data(startindex:endindex);
      adcdata2.tlow = dataStartTime2;
      adcdata2.deltaT = longadcdata2.deltaT;

      % if either data stream is bad, set flag and exit loop
      if ( (data1OK==false) | (data2OK==false) )
        badSegmentData = true;
        break
      end

      if (~ isfield(adcdata1,'fbase') )
        adcdata1.fbase = NaN;
      end

      if (~ isfield(adcdata2,'fbase') )
        adcdata2.fbase = NaN;
      end

      if (~ isfield(adcdata1,'phase') )
	adcdata1.phase = NaN;
      end

      if (~ isfield(adcdata2,'phase') )
	adcdata2.phase = NaN;
      end

      % KLUDGE: can override base frequency in parameter file
      if (~ isnan(params.fbase1) )
	adcdata1.fbase = params.fbase1;
      end;
      if (~ isnan(params.fbase2) )
	adcdata2.fbase = params.fbase2;
      end;
      % End KLUDGE

      if ( isnan(adcdata1.fbase) & isnan(adcdata2.fbase) )
	if params.heterodyned
	  error('Trying to do params.heterodyned analysis on non-params.heterodyned data');
	end
      else
	if (~ params.heterodyned)
	  error('Trying to do non-params.heterodyned analysis on params.heterodyned data');
	end
      end

      % downsample the data 
      sampleRate1 = 1/adcdata1.deltaT;
      sampleRate2 = 1/adcdata2.deltaT;
      p1 = 1;  
      p2 = 1;  
      q1 = floor(sampleRate1/params.resampleRate1);
      q2 = floor(sampleRate2/params.resampleRate2);

      deltaT1 = 1/params.resampleRate1;
      deltaT2 = 1/params.resampleRate2;

      if sampleRate1 == params.resampleRate1
        data = adcdata1.data;
      else
        data = resample(adcdata1.data, p1, q1, params.nResample1, params.betaParam1);
      end
      n1(J) = constructTimeSeries(data, adcdata1.tlow, deltaT1, ...
                                  adcdata1.fbase, adcdata1.phase);

      if sampleRate2 == params.resampleRate2
        data = adcdata2.data;
      else
        data = resample(adcdata2.data, p2, q2, params.nResample2, params.betaParam2);
      end
      n2(J) = constructTimeSeries(data, adcdata2.tlow, deltaT2, ...
                                  adcdata2.fbase, adcdata2.phase);

      % free-up some memory
      clear adcdata1; 
      clear adcdata2;

      % calculate response functions from calibration data

      if ( strncmp(params.alphaBetaFile1,   'none', length(params.alphaBetaFile1))   | ...
           strncmp(params.calCavGainFile1,  'none', length(params.calCavGainFile1))  | ...
           strncmp(params.calResponseFile1, 'none', length(params.calResponseFile1)) )

        % the data is already calibrated
        response1(J) = constructFreqSeries(ones(numFreqs,1), params.flow, params.deltaF);
        transfer1(J) = response1(J);

      else
        calibsec1 = dataStartTime1 + params.bufferSecs1;
        [R1, responseOK1] = ...
          calculateResponse(t1, f1, R01, C01, alpha1, gamma1, calibsec1,...
			    params.ASQchannel1);

        % if response function is bad, set flag and exit loop
        if responseOK1==false
          badResponse = true;
          break
        end
  
        % evaluate response function at desired frequencies
        response1(J) = convertResponse(f1, R1, params.flow, params.deltaF, numFreqs, 0, 0);

        % convert to transfer functions (units: counts/strain) 
        transfer1(J) = convertResponse(f1, R1, params.flow, params.deltaF, numFreqs, 1, 0);
      end

      if ( strncmp(params.alphaBetaFile2,   'none', length(params.alphaBetaFile2))   | ...
           strncmp(params.calCavGainFile2,  'none', length(params.calCavGainFile2))  | ...
           strncmp(params.calResponseFile2, 'none', length(params.calResponseFile2)) )

        % the data is already calibrated
        response2(J) = constructFreqSeries(ones(numFreqs,1), params.flow, params.deltaF);
        transfer2(J) = response2(J);

      else
        calibsec2 = dataStartTime2 + params.bufferSecs2;
        [R2, responseOK2] = ...
          calculateResponse(t2, f2, R02, C02, alpha2, gamma2, calibsec2,...
			    params.ASQchannel2);

        % if response function is bad, set flag and exit loop
        if responseOK2==false
          badResponse = true;
          break
        end
   
        % evaluate response function at desired frequencies
        response2(J) = convertResponse(f2, R2, params.flow, params.deltaF, numFreqs, 0, 0);

        % convert to transfer functions (units: counts/strain) 
        transfer2(J) = convertResponse(f2, R2, params.flow, params.deltaF, numFreqs, 1, 0);
      end

    end % loop over segments J

    % if bad data or bad response function for any segment, continue with 
    % next interval
    if (badSegmentData | badResponse)
      continue
    else
      isFirstPass = false;
    end

  else

    % shift data and response functions accordingly
    for J=1:params.numSegmentsPerInterval-1

      if params.doOverlap
        % shift data by half a segment; need to worry about buffer
        N1 = length(n1(J).data);
        N2 = length(n2(J).data);
        bufferOffset1 = params.bufferSecs1/n1(J).deltaT;
        bufferOffset2 = params.bufferSecs2/n2(J).deltaT;

        data  = [n1(J).data(N1/2+1-bufferOffset1:N1-bufferOffset1) ; ...
                 n1(J+1).data(1+bufferOffset1:N1/2+bufferOffset1)];
        tlow  = n1(J).tlow+intervalTimeStride;
        n1(J) = constructTimeSeries(data, tlow, n1(J).deltaT, ...
                                    n1(J).fbase, n1(J).phase);

        data  = [n2(J).data(N2/2+1-bufferOffset2:N2-bufferOffset2) ; ...
                 n2(J+1).data(1+bufferOffset2:N2/2+bufferOffset2)];
        tlow  = n2(J).tlow+intervalTimeStride;
        n2(J) = constructTimeSeries(data, tlow, n2(J).deltaT, ...
                                    n2(J).fbase, n2(J).phase);

        % get response function corresponding to shifted start times

        if ( strncmp(params.alphaBetaFile1,   'none', length(params.alphaBetaFile1))   | ...
             strncmp(params.calCavGainFile1,  'none', length(params.calCavGainFile1))  | ...
             strncmp(params.calResponseFile1, 'none', length(params.calResponseFile1)) )

          % the data is already calibrated
          response1(J) = constructFreqSeries(ones(numFreqs,1), params.flow, params.deltaF);
          transfer1(J) = response1(J);

        else
          calibsec1 = n1(J).tlow + params.bufferSecs1;
          [R1, responseOK1] = ...
            calculateResponse(t1, f1, R01, C01, alpha1, gamma1, calibsec1,...
                            params.ASQchannel1);

          % if response function is bad, set flag and exit loop
          if responseOK1==false
            badResponse = true;
            break
          end
  
          % evaluate response function at desired frequencies
          response1(J) = convertResponse(f1, R1, params.flow, params.deltaF, numFreqs, 0, 0);

          % convert to transfer functions (units: counts/strain) 
          transfer1(J) = convertResponse(f1, R1, params.flow, params.deltaF, numFreqs, 1, 0);
        end

        if ( strncmp(params.alphaBetaFile2,   'none', length(params.alphaBetaFile2))   | ...
             strncmp(params.calCavGainFile2,  'none', length(params.calCavGainFile2))  | ...
             strncmp(params.calResponseFile2, 'none', length(params.calResponseFile2)) )

          % the data is already calibrated
          response2(J) = constructFreqSeries(ones(numFreqs,1), params.flow, params.deltaF);
          transfer2(J) = response2(J);

        else
          calibsec2 = n2(J).tlow + params.bufferSecs2;
          [R2, responseOK2] = ...
            calculateResponse(t2, f2, R02, C02, alpha2, gamma2, calibsec2,...
                            params.ASQchannel2);

          % if response function is bad, set flag and exit loop
          if responseOK2==false
            badResponse = true;
            break
          end
  
          % evaluate response function at desired frequencies
          response2(J) = convertResponse(f2, R2, params.flow, params.deltaF, numFreqs, 0, 0);

          % convert to transfer functions (units: counts/strain) 
          transfer2(J) = convertResponse(f2, R2, params.flow, params.deltaF, numFreqs, 1, 0);
        end

      else
        % simple shift by a full segment
        n1(J)=n1(J+1);
        n2(J)=n2(J+1);
     
        response1(J)=response1(J+1);
        response2(J)=response2(J+1);

        transfer1(J)=transfer1(J+1);
        transfer2(J)=transfer2(J+1);
      end

    end % loop over J

    % if bad response function for any segment, continue with next interval
    if badResponse
      continue
    end
                                                                                
    % read in time-series data for next segment
    dataStartTime1 = intervalStartTime ...
                     + (params.numSegmentsPerInterval-1)*params.segmentDuration ...
                     - params.bufferSecs1;
    dataStartTime2 = intervalStartTime ...
                     + (params.numSegmentsPerInterval-1)*params.segmentDuration ...
                     - params.bufferSecs2;

    dataDuration1  = params.segmentDuration + 2*params.bufferSecs1;
    dataDuration2  = params.segmentDuration + 2*params.bufferSecs2;

    if dataStartTime1+dataDuration1 > lastLoadedDataEnd1
      lastLoadedDataEnd1 = min(dataStartTime1+params.minDataLoadLength,startTime+jobDuration);
      lastLoadedDataStart1 = dataStartTime1;
      tmpDuration = lastLoadedDataEnd1 - lastLoadedDataStart1;
      [longadcdata1, data1OK, params] = readTimeSeriesData2(channelName1,...
                              dataStartTime1, tmpDuration,...
                              params.frameType1, params.frameDuration1,...
                              gpsTimesFile1, frameCacheFile1, ...
                              params, mc);
    end

    if dataStartTime2+dataDuration2 > lastLoadedDataEnd2
      lastLoadedDataEnd2 = min(dataStartTime2+params.minDataLoadLength,startTime+jobDuration);
      lastLoadedDataStart2 = dataStartTime2;
      tmpDuration = lastLoadedDataEnd2 - lastLoadedDataStart2;
      [longadcdata2, data2OK, params] = readTimeSeriesData2(channelName2,...
                                dataStartTime2,tmpDuration,...
                                params.frameType2, params.frameDuration2,...
                                gpsTimesFile2, frameCacheFile2, ...
                                params, mc2);
    end

    startindex = (dataStartTime1 - lastLoadedDataStart1) ...
                      /longadcdata1.deltaT + 1;
    endindex = (dataStartTime1 + dataDuration1 - lastLoadedDataStart1) ...
                      /longadcdata1.deltaT;
    adcdata1.data = longadcdata1.data(startindex:endindex);
    adcdata1.tlow = dataStartTime1;
    adcdata1.deltaT = longadcdata1.deltaT;

    startindex = (dataStartTime2 - lastLoadedDataStart2) ...
                      /longadcdata2.deltaT + 1;
    endindex = (dataStartTime2 + dataDuration2 - lastLoadedDataStart2) ...
                      /longadcdata2.deltaT;
    adcdata2.data = longadcdata2.data(startindex:endindex);
    adcdata2.tlow = dataStartTime2;
    adcdata2.deltaT = longadcdata2.deltaT;

    % if either data stream is bad, set flag and exit loop
    if ( (data1OK==false) | (data2OK==false) )
      badSegmentData = true;
      break
    end

    if (~ isfield(adcdata1,'fbase') )
      adcdata1.fbase = NaN;
    end
                                                                               
    if (~ isfield(adcdata2,'fbase') )
      adcdata2.fbase = NaN;
    end
                                                                                
    if (~ isfield(adcdata1,'phase') )
      adcdata1.phase = NaN;
    end
                                                                                
    if (~ isfield(adcdata2,'phase') )
      adcdata2.phase = NaN;
    end

    % KLUDGE: can override base frequency in parameter file
    if (~ isnan(params.fbase1) )
      adcdata1.fbase = params.fbase1;
    end
    if (~ isnan(params.fbase2) )
      adcdata2.fbase = params.fbase2;
    end
    % End KLUDGE
  
    if ( isnan(adcdata1.fbase) & isnan(adcdata2.fbase) )
      if params.heterodyned
        error('Trying to do params.heterodyned analysis on non-params.heterodyned data');
      end
    else
      if (~ params.heterodyned)
        error('Trying to do non-params.heterodyned analysis on params.heterodyned data');
      end
    end
                                                                                
   % downsample the data 
    sampleRate1 = 1/adcdata1.deltaT;
    sampleRate2 = 1/adcdata2.deltaT;
    p1 = 1;  
    p2 = 1;  
    q1 = floor(sampleRate1/params.resampleRate1);
    q2 = floor(sampleRate2/params.resampleRate2);

    deltaT1 = 1/params.resampleRate1;
    deltaT2 = 1/params.resampleRate2;

    if sampleRate1 == params.resampleRate1
      data = adcdata1.data;
    else
      data = resample(adcdata1.data, p1, q1, params.nResample1, params.betaParam1);
    end
    n1(params.numSegmentsPerInterval) = ...
      constructTimeSeries(data, adcdata1.tlow, deltaT1, ...
                          adcdata1.fbase, adcdata1.phase);

    if sampleRate2 == params.resampleRate2
      data = adcdata2.data;
    else
      data = resample(adcdata2.data, p2, q2, params.nResample2, params.betaParam2);
    end
    n2(params.numSegmentsPerInterval) = ...
      constructTimeSeries(data, adcdata2.tlow, deltaT2, ...
                          adcdata2.fbase, adcdata2.phase);

    % free-up some memory
    clear adcdata1; 
    clear adcdata2;

    % calculate response functions from calibration data

    if ( strncmp(params.alphaBetaFile1,   'none', length(params.alphaBetaFile1))   | ...
	 strncmp(params.calCavGainFile1,  'none', length(params.calCavGainFile1))  | ...
	 strncmp(params.calResponseFile1, 'none', length(params.calResponseFile1)) )

      % the data is already calibrated
      response1(params.numSegmentsPerInterval) = ...
        constructFreqSeries(ones(numFreqs,1), params.flow, params.deltaF);
      transfer1(params.numSegmentsPerInterval) = response1(params.numSegmentsPerInterval);

    else
      calibsec1 = dataStartTime1 + params.bufferSecs1;
      [R1, responseOK1] = ...
        calculateResponse(t1, f1, R01, C01, alpha1, gamma1, calibsec1,...
                            params.ASQchannel1);
    
    % if response function is bad, set flag and exit loop
    if responseOK1==false
      badResponse = true;
      break
    end
    %% evaluate response function at desired frequencies
    response1(params.numSegmentsPerInterval) = ...
      convertResponse(f1, R1, params.flow, params.deltaF, numFreqs, 0, 0);
    
      % convert to transfer functions (units: counts/strain) 
      transfer1(params.numSegmentsPerInterval) = ...
        convertResponse(f1, R1, params.flow, params.deltaF, numFreqs, 1, 0);
    end

    if ( strncmp(params.alphaBetaFile2,   'none', length(params.alphaBetaFile2))   | ...
         strncmp(params.calCavGainFile2,  'none', length(params.calCavGainFile2))  | ...
         strncmp(params.calResponseFile2, 'none', length(params.calResponseFile2)) )
	    
      % the data is already calibrated
      response2(params.numSegmentsPerInterval) = ...
	  constructFreqSeries(ones(numFreqs,1), params.flow, params.deltaF);
      transfer2(params.numSegmentsPerInterval) = response2(params.numSegmentsPerInterval);
      
    else
      calibsec2 = dataStartTime2 + params.bufferSecs2;
      [R2, responseOK2] = ...
        calculateResponse(t2, f2, R02, C02, alpha2, gamma2, calibsec2,...
                            params.ASQchannel2);
      
      % if response function is bad, set flag and exit loop
      if responseOK2==false
        badResponse = true;
        break
      end
  
      % evaluate response function at desired frequencies
      response2(params.numSegmentsPerInterval) = ...
        convertResponse(f2, R2, params.flow, params.deltaF, numFreqs, 0, 0);
    
      % convert to transfer functions (units: counts/strain) 
      transfer2(params.numSegmentsPerInterval) = ...
        convertResponse(f2, R2, params.flow, params.deltaF, numFreqs, 1, 0);
    end

  end % of if isFirstPass ... else ... end

  % if bad data or bad response function for any segment, continue with 
  % next interval
  if (badSegmentData | badResponse)
    continue
  end

  % initialize data array for average psds
  avg_data1 = zeros(numFreqs,1);
  avg_data2 = zeros(numFreqs,1);

  % loop over number of segments
  for J=1:params.numSegmentsPerInterval

    if params.doShift1
      shiftoffset = round(params.ShiftTime1 / n1(J).deltaT);

      if (shiftoffset >= length(n1(J).data))
        error('Time-shift is larger than buffer segments.');
      end

      qtempdata1 = circshift(n1(J).data,shiftoffset);
    else
      qtempdata1 = n1(J).data;
    end
    if params.doShift2
      shiftoffset = round(params.ShiftTime2 / n2(J).deltaT);

      if (shiftoffset >= length(n2(J).data))
        error('Time-shift is larger than buffer segments.');
      end

      qtempdata2 = circshift(n2(J).data,shiftoffset);
    else
      qtempdata2 = n2(J).data;
    end

    o1 = n1(J);
    o2 = n2(J);
    o1.data = qtempdata1;
    o2.data = qtempdata2;

    % high-pass filter the data (optional)
    if params.doHighPass1
      if params.highPassOrder1<=6
        highpassed1 = constructTimeSeries(filtfilt(b1,a1,o1.data), ...
          o1.tlow, o1.deltaT, o1.fbase, o1.phase);
      else
        try
          params.cascade1;
        catch
          params.cascade1=true;
          fprintf('Warning: HP filter1 n>6 requires a cascade filter\n');
        end
        tmp1 = cascadefilter(o1.data, params.highPassOrder1, ...
          params.highPassFreq1, params.resampleRate1);
        highpassed1 = constructTimeSeries(tmp1, o1.tlow, o1.deltaT, ...
          o1.fbase, o1.phase);
      end
    else
      highpassed1 = o1;
    end
%------------------------------------------------------------------------------
    if params.doHighPass2
      if params.highPassOrder2<=6
        highpassed2 = constructTimeSeries(filtfilt(b2,a2,o2.data), ...
          o2.tlow, o2.deltaT, o2.fbase, o2.phase);
      else
        try
          params.cascade2;
        catch
          params.cascade2=true;
          fprintf('Warning: HP filter2 n>6 requires a cascade filter\n');
        end
        tmp2 = cascadefilter(o2.data, params.highPassOrder2, ...
          params.highPassFreq2, params.resampleRate2);
        highpassed2 = constructTimeSeries(tmp2, o2.tlow, o2.deltaT, ...
          o2.fbase, o2.phase);
      end
    else
      highpassed2 = o2;
    end

    % chop-off bad data at start and end of HP filtered, resampled data
    firstIndex1 = 1 + params.bufferSecs1*params.resampleRate1;
    firstIndex2 = 1 + params.bufferSecs2*params.resampleRate2;

    lastIndex1  = length(highpassed1.data)-params.bufferSecs1*params.resampleRate1;
    lastIndex2  = length(highpassed2.data)-params.bufferSecs2*params.resampleRate2;

    r1(J) = constructTimeSeries(highpassed1.data(firstIndex1:lastIndex1), ...
                                highpassed1.tlow + params.bufferSecs1, ...
                                highpassed1.deltaT, ...
				highpassed1.fbase, highpassed1.phase);
    r2(J) = constructTimeSeries(highpassed2.data(firstIndex2:lastIndex2), ...
                                highpassed2.tlow + params.bufferSecs2, ...
                                highpassed2.deltaT, ...
				highpassed2.fbase, highpassed2.phase);

    % estimate power spectra for optimal filter
    [temp1,freqs1] = pwelch(r1(J).data, psdWindow1,...
			    psdOverlapLength1, psdNFFT1, ...
			    1/r1(J).deltaT, detrendFlag1);
    [temp2,freqs2] = pwelch(r2(J).data, psdWindow2, ...
			    psdOverlapLength2, psdNFFT2,...
			    1/r2(J).deltaT, detrendFlag2);

    %%% normalize appropriately 
    temp1 = 1/r1(J).deltaT *1/2. *temp1;
    temp2 = 1/r2(J).deltaT *1/2. *temp2;
					   

    % If all the bins in the PSD are independent, we are dealing with
    % complex params.heterodyned data
    if length(temp1) == psdFFTLength1
      % Account for heterodyning of data.  This appears to be
      % the correct normalization because psd(), unlike pwelch(),
      % calculates the two-sided PSD of both real and complex data.
      freqs1shifted = fftshift(freqs1);
      spec1 = ...
          constructFreqSeries(2*r1(J).deltaT*fftshift(temp1), ...
                              r1(J).fbase + freqs1shifted(1) ...
                              - 1/r1(J).deltaT, ...
                              freqs1(2)-freqs1(1), 0);
    else
      spec1 = ...
	  constructFreqSeries(2*r1(J).deltaT*temp1, freqs1(1), ...
                              freqs1(2)-freqs1(1), 0);
    end

    % If all the bins in the PSD are independent, we are dealing with
    % complex params.heterodyned data
    if length(temp2) == psdFFTLength2
      % Account for heterodyning of data.  This appears to be
      % the correct normalization because psd(), unlike pwelch(),
      % calculates the two-sided PSD of both real and complex data.
      freqs2shifted = fftshift(freqs2);
      spec2 = ...
            constructFreqSeries(2*r2(J).deltaT*fftshift(temp2), ...
      				r2(J).fbase + freqs2shifted(1) ...
      				- 1/r2(J).deltaT, ...
      				freqs2(2)-freqs2(1), 0);
    else
      spec2 = ...
	    constructFreqSeries(2*r2(J).deltaT*temp2, freqs2(1), ...
				freqs2(2)-freqs2(1), 0);
    end
 
    % coarse-grain noise power spectra to desired freqs
    psd1 = coarseGrain(spec1, params.flow, params.deltaF, numFreqs);
    psd2 = coarseGrain(spec2, params.flow, params.deltaF, numFreqs);

    % calibrate the power spectra
    calPSD1 = ...
      constructFreqSeries(psd1.data.*(abs(response1(J).data).^2), ...
                            psd1.flow, psd1.deltaF, psd1.symmetry);
    calPSD2 = ...
      constructFreqSeries(psd2.data.*(abs(response2(J).data).^2), ...
                            psd2.flow, psd2.deltaF, psd2.symmetry);
  
    % calculate avg power spectra, ignoring middle segment if desired
    midSegment = (params.numSegmentsPerInterval+1)/2;
    if ( (params.ignoreMidSegment) & (J==midSegment) )
      % do nothing
      %fprintf('Ignoring middle segment\n');
    else
      avg_data1 = avg_data1 + calPSD1.data;
      avg_data2 = avg_data2 + calPSD2.data;
    end

    if (J==midSegment)
      % This calculates the "naive" theorerical variance, i.e.,
      % that calculated from the current segment without averaging 
      % over the whole interval.
      % This is useful for the stationarity veto which excludes
      % segments for which the naive sigma differs too much from
      % the one calculated with the sliding PSD average.
      [naiQ, naiVar, naiSensInt] ...
	    = calOptimalFilter(params.segmentDuration, gamma, ...
			       params.fRef, params.alphaExp, ... 
			       calPSD1, calPSD2, ...
			       dataWindow1, dataWindow2, mask);
      naiP1 = calPSD1.data;
      naiP2 = calPSD2.data;
    end
 
  end % loop over segments J

  % construct average power spectra
  if params.ignoreMidSegment
    avg_data1 = avg_data1/(params.numSegmentsPerInterval-1);
    avg_data2 = avg_data2/(params.numSegmentsPerInterval-1);
  else
    avg_data1 = avg_data1/params.numSegmentsPerInterval;
    avg_data2 = avg_data2/params.numSegmentsPerInterval;
  end

  calPSD1_avg = constructFreqSeries(avg_data1, params.flow, params.deltaF, 0);
  calPSD2_avg = constructFreqSeries(avg_data2, params.flow, params.deltaF, 0);

  % calculate optimal filter, theoretical variance, and sensitivity 
  % integrand using avg psds
  [Q, ccVar, sensInt] = calOptimalFilter(params.segmentDuration, gamma, ...
                                           params.fRef, params.alphaExp, ... 
                                           calPSD1_avg, calPSD2_avg, ...
                                           dataWindow1, dataWindow2, mask);

  % analyse the middle data segment %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % window, zero-pad and fft the data
  rbartilde1 = windowAndFFT(r1(midSegment), dataWindow1, fftLength1);
  rbartilde2 = windowAndFFT(r2(midSegment), dataWindow2, fftLength2);

  % calculate the value and spectrum of the CC statistic
  [ccStat,CSD] = calCSD(rbartilde1, rbartilde2, Q, ...
                                     response1(midSegment), ...
                                     response2(midSegment));

  % calculate the value and spectrum of the CC statistic
  [fft1,fft2] = calFFT(rbartilde1, rbartilde2, Q, ...
                                     response1(midSegment), ...
                                     response2(midSegment));

  %recover the window parameters, add them to the structure
  [w1w2bar, w1w2squaredbar, w1w2ovlsquaredbar]=windowFactors(dataWindow1,...
                                                  dataWindow2);
  params.w1w2bar = w1w2bar;
  params.w1w2squaredbar = w1w2squaredbar;
  params.w1w2ovlsquaredbar = w1w2ovlsquaredbar;
  params.bias = 1/(2*(params.segmentDuration * params.deltaF * 2 - 1) * (9/11)) + 1;
  params.naivebias = 1/((params.segmentDuration * params.deltaF * 2 - 1) * (9/11)) + 1;
  params.SiderealTime = GPStoGreenwichMeanSiderealTime(r1(midSegment).tlow);
  

  %check if the interval is bad
  if params.doBadGPSTimes %determine if the current interval is bad
    c1 = intervalStartTime - bufferSecsMax < badtimesend;
    c2 = intervalStartTime + 3*params.segmentDuration + bufferSecsMax > badtimesstart;

    if sum(c1&c2) > 0 | sqrt(ccVar/naiVar)>params.maxDSigRatio | sqrt(ccVar/naiVar)<params.minDSigRatio
      badtimesflag = 1;
    else
      badtimesflag = 0;
    end
  else %do not check for bad times
    badtimesflag = 0;
  end

  %finally record the value in the relevant filename, if the bad time flag ok
  if badtimesflag == 0
    gpsstring = num2str(r1(midSegment).tlow);
    halfdur = params.segmentDuration/2;
    filename = [params.outputFilePrefix '-' ...
      gpsstring '-' num2str(halfdur) '.gwf'];
    P1 = calPSD1_avg.data;
    P2 = calPSD2_avg.data;
    CC = 2 * CSD.data / w1w2bar / params.segmentDuration;

    rec(1).channel = [params.ifo1 ':AdjacentPSD'];
    rec(1).data = P1;
    rec(1).type = 'd';
    rec(1).mode = 'a';

    rec(2).channel = [params.ifo2 ':AdjacentPSD'];
    rec(2).data = P2;
    rec(2).type = 'd';
    rec(2).mode = 'a';

    rec(3).channel = [params.ifo1 ':LocalPSD'];
    rec(3).data = naiP1;
    rec(3).type = 'd';
    rec(3).mode = 'a';

    rec(4).channel = [params.ifo2 ':LocalPSD'];
    rec(4).data = naiP2;
    rec(4).type = 'd';
    rec(4).mode = 'a';

    rec(5).channel = [params.ifo1 params.ifo2 ':CSD'];
    rec(5).data = CC;
    rec(5).type = 'dc';
    rec(5).mode = 'a';

%cet-------try to pass parameters as such---------------
    etparams.name = [params.ifo1 params.ifo2 ':Params'];
    if ~params.stochmap
      % the next two lines remove sub-structs that are used for STAMP only
      params0 = rmfield(params, 'pass');
      params0 = rmfield(params0, 'job');
      doublestr = [];
      tt = fieldnames(params0);
      kk=11; %cet
      for jj = 1:length(tt)
        tmp = getfield(params0, tt{jj});
        if ischar(tmp)
          tmpstr = [tt{jj} ' ' tmp];
        else
          if size(tmp,1)>1
            %if it is an array, needs to be a row 
            tmp = transpose(tmp);
          end
            % this if-then statement skips over parameter sub-structs that are
            % used primar
            tmpstr = [tt{jj} ' ' num2str(tmp)];
            tmp = num2str(tmp);
        end
        ttvals{jj}=tmp;
        doublestr = [doublestr double(tmpstr) 10];
      end
      etparams.parameters=horzcat(tt, ttvals');
    end

    rec(6).channel = [params.ifo1 params.ifo2 ':flow'];
    rec(6).data = params.flow;
    rec(6).type = 'd';
    rec(6).mode = 'a';

    rec(7).channel = [params.ifo1 params.ifo2 ':fhigh'];
    rec(7).data = params.fhigh;
    rec(7).type = 'd';
    rec(7).mode = 'a';

    rec(8).channel = [params.ifo1 params.ifo2 ':deltaF'];
    rec(8).data = params.deltaF;
    rec(8).type = 'd';
    rec(8).mode = 'a';

    rec(9).channel = [params.ifo1 params.ifo2 ':GPStime'];
    rec(9).data = r1(midSegment).tlow;
    rec(9).type = 'd';
    rec(9).mode = 'a';

    rec(10).channel = [params.ifo1 params.ifo2 ':params.segmentDuration'];
    rec(10).data = params.segmentDuration;
    rec(10).type = 'd';
    rec(10).mode = 'a';

    rec(11).channel = [params.ifo1 params.ifo2 ':w1w2bar'];
    rec(11).data = params.w1w2bar;
    rec(11).type = 'd';
    rec(11).mode = 'a';

    rec(12).channel = [params.ifo1 params.ifo2 ':w1w2squaredbar'];
    rec(12).data = params.w1w2squaredbar;
    rec(12).type = 'd';
    rec(12).mode = 'a';
				    
    rec(13).channel = [params.ifo1 params.ifo2 ':w1w2ovlsquaredbar'];
    rec(13).data = params.w1w2ovlsquaredbar;
    rec(13).type = 'd';
    rec(13).mode = 'a';

    rec(14).channel = [params.ifo1 params.ifo2 ':bias'];
    rec(14).data = params.bias;
    rec(14).type = 'd';
    rec(14).mode = 'a';
 
    rec(15).channel = [params.ifo1 params.ifo2 ':naivebias'];
    rec(15).data = params.naivebias;
    rec(15).type = 'd';
    rec(15).mode = 'a';
 
    rec(16).channel = [params.ifo1 params.ifo2 ':SiderealTime'];
    rec(16).data = params.SiderealTime;
    rec(16).type = 'd';
    rec(16).mode = 'a';
    rec(17).channel = [params.ifo1 params.ifo2 ':params.numSegmentsPerInterval'];
    rec(17).data = params.numSegmentsPerInterval;
    rec(17).type = 'd';
    rec(17).mode = 'a';

    rec(18).channel = [params.ifo1 params.ifo2 ':params.resampleRate1'];
    rec(18).data = params.resampleRate1;
    rec(18).type = 'd';
    rec(18).mode = 'a';

    rec(19).channel = [params.ifo1 params.ifo2 ':params.resampleRate2'];
    rec(19).data = params.resampleRate2;
    rec(19).type = 'd';
    rec(19).mode = 'a';

    rec(20).channel = [params.ifo1 params.ifo2 ':params.nResample1'];
    rec(20).data = params.nResample1;
    rec(20).type = 'd';
    rec(20).mode = 'a';

    rec(21).channel = [params.ifo1 params.ifo2 ':params.nResample2'];
    rec(21).data = params.nResample2;
    rec(21).type = 'd';
    rec(21).mode = 'a';

    rec(22).channel = [params.ifo1 params.ifo2 ':params.betaParam1'];
    rec(22).data = params.betaParam1;
    rec(22).type = 'd';
    rec(22).mode = 'a';

    rec(23).channel = [params.ifo1 params.ifo2 ':params.betaParam2'];
    rec(23).data = params.betaParam2;
    rec(23).type = 'd';
    rec(23).mode = 'a';

    if ~params.stochmap
      mkframe(filename,rec,'n',params.segmentDuration,r1(midSegment).tlow,etparams);
    else
      map.P1(:,I) = rec(1).data;
      map.P2(:,I) = rec(2).data;
      map.naiP1(:,I) = rec(3).data;
      map.naiP2(:,I) = rec(4).data;
      map.cc(:,I) = CSD.data.*Q.data;
      map.sensInt(:,I) = sensInt.data;
      map.ccVar(I) = ccVar;
      map.segstarttime(:,I) = rec(9).data;
      map.fft1(:,I) = fft1.data;
      map.fft2(:,I) = fft2.data;
      %map.bias(:,I) = rec(14).data;
      %map.naivebias(:,I) = rec(15).data;
    end % params.stochmap if statement

  end % badtimesflag if statement

end % loop over intervals I

if params.stochmap
  % add remaining metdata to map
  map.deltaF = params.deltaF;
  map.segDur = params.segmentDuration;
  map.start = map.segstarttime(1);
  map.f = f;
 
  fprintf('Done with preproc phase...\n');
  if params.storemats 
    fprintf('Storing .mat files from preproc ... \n');
      % if compression is turned on, override batch mode
    if params.compress
      params.startGPS = map.segstarttime(1);
      navg = floor(size(map.P1,2)/params.npixels);
      params.endGPS = map.segstarttime(end) + params.segmentDuration;
      params.batch = false;
    end
    % when params.batch==true we make mat files in batches
    if params.batch
      % the start of the job
      startGPS_job = map.segstarttime(1);
      % the end of the job
      endGPS_job = map.segstarttime(end) + params.segmentDuration;
      % take into account the overlapping segment
      mod_mapsize = params.mapsize - mod(params.mapsize, ...
					 params.segmentDuration/2);
      % the number of mat files to create
      %Boris Goncharov 22/02/2017, commented-out part causes an overlap:
      nmats = ceil( (endGPS_job-startGPS_job) / ...
        (params.mapsize)); %-params.segmentDuration) );
      % create nmats mat files
      for ii=1:nmats
        % warning message
        if params.segmentDuration<1 || params.segmentDuration>100
          warning('This segment duration may cause trouble: talk to Eric.');
          %If segmentDuration<1 then the file names will have a decimal in them
          %That may cause problems with regular expressions in clustermap.
          %If the segmentDuration is >100s then the mat file will have only one
          %segment in it and the requirement of an overlapping segment may
          %cause problems.  -ethrane
        end
        % maps will overlap by one segment
        %Boris Goncharov 22-02-2017 commented out part causes an overlap
        params.startGPS = startGPS_job + ...
          (ii-1)*(mod_mapsize);% - params.segmentDuration);
        params.endGPS = params.startGPS + params.mapsize;
        if params.endGPS>endGPS_job
          params.endGPS=endGPS_job;
        end
        fprintf('creating mat file #%i\n', ii);
        savemap(map, params);
      end
      % when not in batch mode we make one mat file for the entire job or for 
      % some specific time interval given by start and end GPS as input 
      % parameters
    else
      savemap(map, params);
    end
  end
end

fprintf('elapsed_time = %f\n', toc);

return
