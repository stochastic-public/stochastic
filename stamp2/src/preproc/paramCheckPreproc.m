function params = paramCheckPreproc(params)
% function params = paramCheckPreproc(params)
% checks preproc params for correctness and consistency
% E Thrane

% begin with basic variables that must always be defined-----------------------
% make sure ifo variables are set
% these commands used to be carried out in preproc.m
try
  params.site1;
catch
  params.site1 = getsitefromletter(params.ifo1(1));
end
% site2
try
  params.site2;
catch
  params.site2 = getsitefromletter(params.ifo2(1));
end

% make sure standard segment variables are defined
try
  params.segmentDuration;
  params.numSegmentsPerInterval;
  params.ignoreMidSegment;
catch
  error('missing params: segmentDuration, numSegmentsPerInterval, and/or ignoreMidSegment.');
end

% make sure standard frequency variables are defined
try
  params.flow;
  params.fhigh;
catch
  error('missing params: flow and/or fhigh');
end

% data conditioning parameters
try
  params.resampleRate1;
  params.resampleRate2;
  params.bufferSecs1;
  params.bufferSecs2;
  params.nResample1;
  params.nResample2;
  params.betaParam1;
  params.betaParam2;
catch
  error('missing params: resampleRate1/2, nResample1/2, betaParam1/2, and/or bufferSecs1/2');
end

% more data conditioning parameters
try
  params.hannDuration1;
  params.hannDuration2;
  params.doHighPass1;
  params.doHighPass2;
  % technically, these do not need to be defined if doHighPass is on but in 
  % practice, we always high-pass the data.
  params.highPassFreq1;
  params.highPassFreq2;
  params.highPassOrder1;
  params.highPassOrder2;
catch
  error('missing params: doHighPass1/2, hannDuration1/2, highPassFreq1/2, and/or highPassOrder1/2');
end

%%% doLineRemoval option
if params.doLineRemoval

    %line removal parameter
    try
        params.lineFile1;
        params.lineFile2;
        params.respTime1;
        params.respTime2;
    catch
        error('missing params: lineFile1/2 and/or respTime1/2');
    end

    % det 1
    try
        freq = textread(params.lineFile1,...
                        '%f %*s \n', -1,...
                        'commentstyle', 'matlab');
        params.freqsToRemove1 = freq(freq>params.flow & freq<params.fhigh);
    catch
        error([params.lineFile1 ' cannot be read']);
    end

    % det 2
    try
        freq = textread(params.lineFile2,...
                        '%f %*s \n', -1,...
                        'commentstyle', 'matlab');
        params.freqsToRemove2 = freq(freq>params.flow & freq<params.fhigh);
    catch
        error([params.lineFile2 ' cannot be read']);
    end
end


% frame and channel parameters
try
  params.ASQchannel1;
  params.ASQchannel2;
  params.frameType1;
  params.frameType2;
catch
  error('missing params: ASQchannel1/2 and/or frameType1/2');
end

% check that frame cache files are defined
try
  params.frameCachePath1;
  params.frameCachePath2;
catch
  error('missing params: params.frameCachePath1/2');
end

% if requested, turn off frgetvect warnings
if params.suppressFrWarnings
  warning off frgetvect:info;
end

% parameters for stochastic.m only---------------------------------------------
% make sure HfFile is defined if necessary
if params.useSignalSpectrumHfFromFile
  error('params.HfFile parameter not set');
else
  params.HfFile = '';
end

% make sure sph has everything it needs
if params.doSphericalHarmonics
  try
     params.SpHLmax;
     params.gammaLM_coeffsPath;
  catch
    error('missing params For sph: SpHLmax and/or gammaLM_coeffsPath');
  end
end

% copied over from preprocDefaults.m: never write orf files:
params.writeOverlapReductionFunctionToFiles = false;

% make sure doDirectional has everything it needs
if params.doDirectional
  % no way to write the uge amount of data to screen
  params.writeResultsToScreen=false;
  % copied verbatim from old preprocDefaults.m
  try
    useparams.SkyPatternFile=params.useparams.SkyPatternFile;
  catch
    useparams.SkyPatternFile=false;
  end
  % copied verbatim from old preprocDefaults.m
  try
    params.SkyPatternFile = params.SkyPatternFile;
  catch
    if useparams.SkyPatternFile
      error('params.SkyPatternFile parameter not set');
    else
      params.SkyPatternFile = '';
    end
  end
  % SkyPatternRightAscensionNumPoints
  try
    params.SkyPatternRightAscensionNumPoints;
  catch
    error('params.SkyPatternRightAscensionNumPoints parameter not set');
  end
  % SkyPatternDeclinationNumPoints
  try
    params.SkyPatternDeclinationNumPoints;
  catch
    error('params.SkyPatternDeclinationNumPoints parameter not set');
  end
  % maxCorrelationTimeShift
  try
    params.maxCorrelationTimeShift;
  catch
    error('params.maxCorrelationTimeShift parameter not set');
  end
  % EHT: changed on 10/9/14 from warning to error
  if params.UnphysicalTimeShift~=0
    error('params.UnphysicalTimeShift~0 + params.doDirectional = garbage');
  end
% closing doDirectional loop; copied verbatim from old preprocDefaults.m
else
  useparams.SkyPatternFile=0;
  params.SkyPatternFile='';
  params.SkyPatternRightAscensionNumPoints=0;
  params.SkyPatternDeclinationNumPoints=0;
  params.maxCorrelationTimeShift=0;
  params.UnphysicalTimeShift=0;
end

% intermediate data------------------------------------------------------------
if params.intermediate
  try
    params.intFrameCachePath;
  catch
    error('intFrameCachePath not defined');
  end
end

% simulation options-----------------------------------------------------------
if params.doSimulatedPointSource
  % simulationPath
  try
    params.simulationPath;
  catch
    error('params.simulationPath parameter not set');
  end
  % simulatedPointSourcesFile
  try
    params.simulatedPointSourcesFile;
  catch
    error('params.simulatedPointSourcesFile parameter not set');
  end
  % simulatedPointSourcesPowerSpec
  try
    params.simulatedPointSourcesPowerSpec;
  catch
    error('params.simulatedPointSourcesPowerSpec parameter not set');
  end
  % simulatedPointSourcesInterpolateLogarithmic
  try
    params.simulatedPointSourcesInterpolateLogarithmic;
  catch
    params.simulatedPointSourcesInterpolateLogarithmic=true;
  end
  % simulatedPointSourcesBufferDepth
  try
    params.simulatedPointSourcesBufferDepth;
  catch
    error('params.simulatedPointSourcesBufferDepth parameter not set');
  end
  % simulatedPointSourcesHalparams.fRefillLength
  % Eric: copied over verbatim from preprocDefaults...looks weird so commenting
  % out.  do not see this variable used anywhere in preproc code nor in
  % CrossCorr code.
%  try
%    simulatedPointSourcesHalparams.fRefillLength;
%  catch
%   fprintf('ET:Should not this be params.simulatedPointSourcesHalparams??\n');
%    error('simulatedPointSourcesHalparams.fRefillLength parameter not set');
%  end
  % simulatedPointSourcesNoRealData
  try
    params.simulatedPointSourcesNoRealData;
  catch
    params.simulatedPointSourcesNoRealData=false;
  end
  % simulatedPointSourcesMakeIncoherent
  try
    params.simulatedPointSourcesMakeIncoherent;
  catch
    params.simulatedPointSourcesMakeIncoherent=0;
  end
% close doSimulatedPointSource loop
else
  params.simulationPath='';
  params.simulatedPointSourcesFile='';
  params.simulatedPointSourcesPowerSpec='';
  params.simulatedPointSourcesInterpolateLogarithmic=true;
  params.simulatedPointSourcesBufferDepth=0;
  simulatedPointSourcesHalparams.fRefillLength=0;
  params.simulatedPointSourcesNoRealData=false;
  params.simulatedPointSourcesMakeIncoherent=0;
end

% stamp stuff------------------------------------------------------------------
% firstpass is used in conjunction with injections to make sure that the
% injection file is only read in once.  once it is read in, firstpass is set
% to false.
params.pass.stamp.firstpass=true;

% make sure batch mode has what it needs
if ~params.batch
  fprintf('Batch mode is OFF.\n');
  try
    params.startGPS;
    params.endGPS;
  catch
    error('~params.batch: missing fields');
  end
end

% set random number generator.  use pp_seed<0 to randomize with clock.
try
  if params.pp_seed>0
    fprintf('Setting seed to %i.\n', params.pp_seed);
  else
   % This setup should be good until ~year 4294 when seed>2^32-1 
    params.pp_seed =  sum(clock*1000000);
    fprintf('Setting seed using clock.\n');
  end
catch
  fprintf('Setting seed to 1.\n');
  params.pp_seed = 1;
end
randn('state', params.pp_seed);
rand('state', params.pp_seed);

if params.stampmdc
   try
     params.stamp.mdclist; 
     params.stamp.mdcchannel1; 
     params.stamp.mdcchannel2; 
     params.stamp.alpha;
   catch me
      error('missing params For mdc injections: mdclist, mdcchannel1, mdcchannel2, and/or alpha');
   end 
end

% make sure everything is needed for stamp injections
if params.stampinj
  % first, ascertain if there is an inj_type
  try
    params.pass.stamp.inj_type = params.stamp.inj_type;
  catch
    params.pass.stamp.inj_type = 'other';
    params.stamp.iota = 0;
    params.stamp.psi = 0;
  end

  % if the injection is on-the-fly, then we can fill in some dummy params
  if strcmp(params.pass.stamp.inj_type, 'fly')
    params.stamp.file = 'DUMMY_VALUE';
    %params.stamp.alpha = 1;
% this line is done in preproc.m
%    params.pass.stamp.startGPS = params.job.startGPS;
% and we replace it with a dummy value for now
    params.stamp.startGPS = -1;
    % and try defining relevant pass variables
    try
      params.pass.stamp.h0 = params.stamp.h0;
      params.pass.stamp.f0 = params.stamp.f0;
      params.pass.stamp.phi0 = params.stamp.phi0;
      params.pass.stamp.fdot = params.stamp.fdot;
      params.pass.stamp.start = params.stamp.start;
      params.pass.stamp.duration = params.stamp.duration;
    catch
      %error('missing params For OTF injections: h0, phi0, fdot, start, duration and/or f0');
    end

    try
      params.pass.stamp.mass1 = params.stamp.mass1;
      params.pass.stamp.mass2 = params.stamp.mass2;
      params.pass.stamp.start = params.stamp.start;
      params.pass.stamp.duration = params.stamp.duration;
    catch
      %error('missing params For OTF injections: h0, phi0, fdot, start, duration and/or f0');
    end

    try
      params.pass.stamp.alphasat = params.stamp.alphasat;
      params.pass.stamp.f0 = params.stamp.f0;
      params.pass.stamp.start = params.stamp.start;
      params.pass.stamp.duration = params.stamp.duration;
    catch
      %error('missing params For OTF injections: h0, phi0, fdot, start, duration and/or f0');
    end

    try
      params.pass.stamp.eB = params.stamp.eB;
      params.pass.stamp.f0 = params.stamp.f0;
      params.pass.stamp.start = params.stamp.start;
      params.pass.stamp.duration = params.stamp.duration;
    catch
      %error('missing params For OTF injections: h0, phi0, fdot, start, duration and/or f0');
    end

    try
      params.pass.stamp.f0 = params.stamp.f0;
      params.pass.stamp.tau = params.stamp.tau;
      params.pass.stamp.nn = params.stamp.nn;
      params.pass.stamp.epsilon = params.stamp.epsilon;
      params.pass.stamp.duration = params.stamp.duration;
      params.pass.stamp.start = params.stamp.start;
    catch
      %error('missing params For OTF injections: h0, phi0, fdot, start, duration and/or f0');
    end

    try
      params.pass.stamp.cosi = params.stamp.cosi;
    catch
      %error('missing params For OTF injections: h0, phi0, fdot, start, duration and/or f0');
    end

    % see if there is a fly_waveform type defined
    if isfield(params.stamp, 'fly_waveform')
      params.pass.stamp.fly_waveform = params.stamp.fly_waveform;
      % load tau if half sine-Gaussian selected
      if strcmp(params.pass.stamp.fly_waveform, 'half_sg') || strcmp(params.pass.stamp.fly_waveform, 'ringdown')
        try
          params.pass.stamp.tau = params.stamp.tau;
        catch
          error('missing param For OTF half sine-Gaussian injections: tau');
        end
      end
    end
  end

  % define default values (here instead of in paramdef_stamp to avoid 
  % params.stamp complications)
  try
    params.pass.stamp.iota = params.stamp.iota;
  catch
    params.pass.stamp.iota = 0;
  end
  % same thing for psi
  try
    params.pass.stamp.psi = params.stamp.psi;
  catch
    params.pass.stamp.psi = 0;
  end

  % move params.stamp variables to params.pass struct
  try
    params.pass.stamp.ra = params.stamp.ra;
    params.pass.stamp.decl = params.stamp.decl;
    params.pass.stamp.startGPS = params.stamp.startGPS;
    params.pass.stamp.file = params.stamp.file;
    params.pass.stamp.alpha = params.stamp.alpha;
  catch
    error('missing params For stamp inj: ra, decl, iota, psi, startGPS, file, and/or alpha');
  end

  % check ra
  if params.pass.stamp.ra < 0 | params.pass.stamp.ra>24
    error('params.ra must be between 0 and 24.');
  end

  % check dec
  if params.pass.stamp.decl<-90 | params.pass.stamp.decl>90
    error('stamp.decl out of range');
  end
% close stamp_inj loop
end

% by tradition, this is printed to screen
%fprintf('params.stochmap = %i\n', params.stochmap);

% simulate noise for STAMP mat files 
if params.doDetectorNoiseSim
  fprintf('Simulated detector noise.\n');
  % make sure noise file is defined
  try
    params.DetectorNoiseFile = params.DetectorNoiseFile;
  catch
    error('params.DetectorNoiseFile required for doDetectorNoiseSim==1');
  end

  % check sampleRate and give warning message if it is not defined
  try
    params.sampleRate;
  catch
    params.sampleRate = 16384;
    fprintf('sampleRate = %5.0f\n', params.sampleRate); 
  end
end

% for intermediate data--------------------------------------------------------
try
  params.outputFilePrefix = params.outputFilePrefix;
catch
  if ~params.stochmap % writing to intermediate frames (*.gwf)
    error('params.outputFilePrefix parameter not set');
  else
    % if stochmap is on outputFilePrefix (for frames) is not used and
    % hence define a dummy name
    params.outputFilePrefix = 'test';
  end
end

% other miscellaneous commands-------------------------------------------------
% output directory for writing *.mat files after preproc stage (for STAMP use) 
try
  params.outputfiledir = params.outputfiledir;
catch
  params.outputfiledir = './';
end

% variables that should always be defined -------------------------------------
try
  params.c;
catch
  params.c = 299792458; % speed of light in m/s
end

try
  params.doLineRemoval;
catch
  params.doLineRemoval = false;
end

% if using reference GPS time for antenna factors and detector time delay, check that reference time is defined
if params.useReferenceAntennaFactors;
  try
    params.referenceGPSTime;
  catch
    error('Attempted to use empty reference GPS time to set antenna factors and detector time delay for injection')
  end
end

return
