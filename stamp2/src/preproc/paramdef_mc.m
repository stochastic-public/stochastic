function [plist, ptype, pdef, pval, ii] = paramdef_mc(plist, ptype, pdef, pval)
% function [plist, ptype, pdef, pval, ii] = paramdef_mc(plist, ptype, ... 
% pdef, pval)
% support function for paramlist.m.  See that function for more details.
% E Thrane

% set variable counter
ii = length(plist)+1;

plist{ii} = 'doMonteCarlo';
ptype{ii} = 'num';
pdef{ii} = 'simulation option (not often used...use doDetectorNoiseSim instead';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doMCoffset';
ptype{ii} = 'num';
pdef{ii} = 'rarely used';
ii=ii+1;

plist{ii} = 'doSimulatedPointSource';
ptype{ii} = 'num';
pdef{ii} = 'not frequenly tused to generate persistent point source';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doSimulatedSkyMap';
ptype{ii} = 'num';
pdef{ii} = 'carry out radiometer search';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doSimulatedDetectorNoise';
ptype{ii} = 'num';
pdef{ii} = 'NOTE: this option is not used with STAMP. See instead: doDetectorNoiseSim';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'injChannel';
ptype{ii} = 'str2';
pdef{ii} = 'channels to inject into (not in common use)';
ii=ii+1;

plist{ii} = 'injPrefix';
ptype{ii} = 'str2';
pdef{ii} = 'not in common use';
ii=ii+1;

plist{ii} = 'injFrameDuration';
ptype{ii} = 'str2';
pdef{ii} = 'not in common use';
ii=ii+1;

plist{ii} = 'numTrials';
ptype{ii} = 'num';
pdef{ii} = 'number of simulation trials; not in common use';
pval{ii} = 1;
ii=ii+1;

plist{ii} = 'signalType';
ptype{ii} = 'str';
pdef{ii} = 'not in common use';
pval{ii} = 'const';
ii=ii+1;

plist{ii} = 'simulationPath';
ptype{ii} = 'str';
pdef{ii} = 'not in common use';
ii=ii+1;

plist{ii} = 'simulatedPointSourcesFile';
ptype{ii} = 'str';
pdef{ii} = 'not in common use';
ii=ii+1;

plist{ii} = 'simulatedPointSourcesPowerSpec';
ptype{ii} = 'str';
pdef{ii} = 'not in common use';
ii=ii+1;

plist{ii} = 'simulatedPointSourcesInterpolateLogarithmic';
ptype{ii} = 'num';
pdef{ii} = 'interpolation option stochastic injections';
ii=ii+1;

plist{ii} = 'simulatedPointSourcesBufferDepth';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedPointSourcesHalfRefillLength';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedPointSourcesNoRealData';
ptype{ii} = 'num';
pdef{ii} = 'option to simluate point source without reading frames';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'simulatedPointSourcesMakeIncoherent';
ptype{ii} = 'num';
pdef{ii} = 'incoherent point sources';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'simulatedSkyMapFile';
ptype{ii} = 'str';
pdef{ii} = 'path to sky map file';
ii=ii+1;

plist{ii} = 'simulatedSkyMapFileType';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedSkyMapFileNumBins';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedSkyMapInjectAsSpH';
ptype{ii} = 'num';
pdef{ii} = 'inject as sph decomoposition vs pixel basis';
ii=ii+1;

plist{ii} = 'simulatedSkyMapConvertLmax';
ptype{ii} = 'num';
pdef{ii} = 'apply lmax to limit angular scale';
ii=ii+1;

plist{ii} = 'simulatedSkyMapConvertDeg';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedSkyMapInjectTimeDomain';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedSkyMapPowerSpec';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedSkyMapInterpolateLogarithmic';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedSkyMapBufferDepth';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedSkyMapHalfRefillLength';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedSkyMapNoRealData';
ptype{ii} = 'num';
pdef{ii} = 'simulate sky map without reading in frames';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'simulatedDetectorNoisePowerSpec';
ptype{ii} = 'num2';
pdef{ii} = 'simulate detector noise (not the standard stamp noise simulation)';
ii=ii+1;

plist{ii} = 'simulatedDetectorNoiseInterpolateLogarithmic';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedDetectorNoiseBufferDepth';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedDetectorNoiseHalfRefillLength';
ptype{ii} = 'num';
pdef{ii} = 'no comment available';
ii=ii+1;

plist{ii} = 'simulatedDetectorNoiseNoRealData';
ptype{ii} = 'num';
pdef{ii} = 'no comment available; not standard in stamp';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doInjFromFile';
ptype{ii} = 'num2';
pdef{ii} = 'inject from file; not standard in stamp';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'injGPSTimesPath';
ptype{ii} = 'str2';
pdef{ii} = 'pointer to injection GPS cache files; not standard in stamp';
ii=ii+1;

plist{ii} = 'injFrameCachePath';
ptype{ii} = 'str2';
pdef{ii} = 'pointer to injection frame cache files; not standard in stamp';
ii=ii+1;

plist{ii} = 'injScale';
ptype{ii} = 'num2';
pdef{ii} = 'scale injections by this amount; not standard';
ii=ii+1;

return

% use this template for any future additions
plist{ii} = '';
ptype{ii} = '';
pdef{ii} = '';
%pval{ii} = ;
ii=ii+1;
