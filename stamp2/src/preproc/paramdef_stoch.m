function [plist, ptype, pdef, pval, ii] = paramdef_stoch(plist, ptype, ...
  pdef, pval)
% function [plist, ptype, pdef, pval] = paramdef_stoch([plist, ptype, ...
%  pdef, pval])
% support function for paramlist.m.  See that function for more details.
% E Thrane

% set variable counter
ii = length(plist)+1;

plist{ii} = 'doDirectional';
ptype{ii} = 'num';
pdef{ii} = 'carry out radiometer analysis';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doSphericalHarmonics';
ptype{ii} = 'num';
pdef{ii} = 'carry out sph search?';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doNarrowbandRadiometer';
ptype{ii} = 'num';
pdef{ii} = 'carry out narrowband radiometer search?';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doAllSkyComparison';
ptype{ii} = 'num';
pdef{ii} = 'compare isotropic and radiometer results (not often used)';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'SpHFreqIntFlag';
ptype{ii} = 'num';
pdef{ii} = 'not often used: option to integrate in sph search';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'SpHLmax';
ptype{ii} = 'num';
pdef{ii} = 'sph lmax';
ii=ii+1;

plist{ii} = 'useSkyPatternFile';
ptype{ii} = 'num';
pdef{ii} = 'use file to point radiometer';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'SkyPatternFile';
ptype{ii} = 'str';
pdef{ii} = 'path to sky pattern file for radiometer pointing';
ii=ii+1;

plist{ii} = 'SkyPatternRightAscensionNumPoints';
ptype{ii} = 'num';
pdef{ii} = 'number of RA values in sky pattern file';
ii=ii+1;

plist{ii} = 'SkyPatternDeclinationNumPoints';
ptype{ii} = 'num';
pdef{ii} = 'number of dec values in sky pattern file';
ii=ii+1;

plist{ii} = 'maxCorrelationTimeShift';
ptype{ii} = 'num';
pdef{ii} = 'used to optimize radiometer';
ii=ii+1;

plist{ii} = 'gammaLM_coeffsPath';
ptype{ii} = 'str';
pdef{ii} = 'path to gammaLM files used in sph search';
ii=ii+1;

plist{ii} = 'doConstTimeShift';
ptype{ii} = 'num';
pdef{ii} = 'different from ShiftTime1/2 ... usually use those insteadt';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doCombine';
ptype{ii} = 'num';
pdef{ii} = 'combine point estimates from multiple segments in a job';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'writeResultsToScreen';
ptype{ii} = 'num';
pdef{ii} = 'print results to screen';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'writeStatsToFiles';
ptype{ii} = 'num';
pdef{ii} = 'record ccstat files';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'writeNaiveSigmasToFiles';
ptype{ii} = 'num';
pdef{ii} = 'record naive sigmas, useful in delta sigma cut';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'writeSpectraToFiles';
ptype{ii} = 'num';
pdef{ii} = 'record ccspectra to files';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'writeSensIntsToFiles';
ptype{ii} = 'num';
pdef{ii} = 'record sensitivity integrands';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'writeCoherenceToFiles';
ptype{ii} = 'num';
pdef{ii} = 'record coherence to files';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'writeCohFToFiles';
ptype{ii} = 'num';
pdef{ii} = 'not sure';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'writeOptimalFiltersToFiles';
ptype{ii} = 'num';
pdef{ii} = 'record optimal filters in files, unusual to use';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'writeOverlapReductionFunctionToFiles';
ptype{ii} = 'num';
pdef{ii} = 'record gamma(f) in files; unusual: note this is always overrided in paramCheckPreproc';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'writeCalPSD1sToFiles';
ptype{ii} = 'num';
pdef{ii} = 'record calibrated PSD1 to file';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'writeCalPSD2sToFiles';
ptype{ii} = 'num';
pdef{ii} = 'record calibrated PSD2 to file';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doTimingTransientSubtraction';
ptype{ii} = 'num2';
pdef{ii} = 'unusual parameter to remove repeating glitch';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'TimingTransientFile';
ptype{ii} = 'str2';
pdef{ii} = 'used in cojunction with doTimingTransientSubtraction; unusual';
pval{ii} = '';
ii=ii+1;

plist{ii} = 'site';
ptype{ii} = 'str2';
pdef{ii} = 'the site of the detector';
ii=ii+1;

plist{ii} = 'azimuth';
ptype{ii} = 'num2';
pdef{ii} = 'not sure';
pval{ii} = NaN;
ii=ii+1;

plist{ii} = 'alphaExp';
ptype{ii} = 'num';
pdef{ii} = 'alpha exponent characterizing assumed GW spectral shape';
pval{ii} = 0;
ii=ii+1;

plist{ii} = 'fRef';
ptype{ii} = 'num';
pdef{ii} = 'reference frequency given power law GW model; usually 100 Hz';
% EHT: setting default fRef to 100 since it is not actually used in radiometer
% and stamp analyses
pval{ii} = 100;
ii=ii+1;

plist{ii} = 'maxSegmentsPerMatfile';
ptype{ii} = 'num';
pdef{ii} = 'not sure';
pval{ii} = 60;
ii=ii+1;

plist{ii} = 'useSignalSpectrumHfFromFile';
ptype{ii} = 'num';
pdef{ii} = 'describe GW spectrum shape from asci text file';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'HfFile';
ptype{ii} = 'str';
pdef{ii} = 'location of file given useSignalSpectrumHfFromFile';
ii=ii+1;

plist{ii} = 'HfFileInterpolateLogarithmic';
ptype{ii} = 'num';
pdef{ii} = 'interpolate HfFile in log space';
pval{ii} = true;
ii=ii+1;

plist{ii} = 'UnphysicalTimeShift';
ptype{ii} = 'num';
pdef{ii} = 'yet another way to shift data';
pval{ii} = 0;
ii=ii+1;

plist{ii} = 'minMCoff';
ptype{ii} = 'num';
pdef{ii} = 'not sure';
pval{ii} = 0.1;
ii=ii+1;

plist{ii} = 'maxMCoff';
ptype{ii} = 'num';
pdef{ii} = 'not sure';
pval{ii} = 0.6;
ii=ii+1;

plist{ii} = 'ConstTimeShift';
ptype{ii} = 'num';
pdef{ii} = 'value of constant time shift';
pval{ii} = 0;
ii=ii+1;

plist{ii} = 'maxDSigRatio';
ptype{ii} = 'num';
pdef{ii} = 'max allowed value for delta sigma cut';
% copied from preprocDefaults:
pval{ii} = 1000;
ii=ii+1;

plist{ii} = 'minDSigRatio';
ptype{ii} = 'num';
pdef{ii} = 'min allowed value for delta sigma cut';
% copied from preprocDefaults:
pval{ii} = 0;
ii=ii+1;

plist{ii} = 'minDataLoadLength';
ptype{ii} = 'num';
pdef{ii} = 'minimum amount of data per job';
pval{ii} = 200;
ii=ii+1;

plist{ii} = 'fbase';
ptype{ii} = 'num2';
pdef{ii} = 'considered a kludge in old documentation';
pval{ii} = NaN;
ii=ii+1;

plist{ii} = 'alphaBetaFile';
ptype{ii} = 'str2';
pdef{ii} = 'describes gain of uncalibrated data';
% EHT: added on 10/9/14 since this is rarely used anymore
pval{ii} = 'none';
ii=ii+1;

plist{ii} = 'calCavGainFile';
ptype{ii} = 'str2';
pdef{ii} = 'describes cavity gain of uncalibrated data';
pval{ii} = 'none';
ii=ii+1;

plist{ii} = 'calResponseFile';
ptype{ii} = 'str2';
pdef{ii} = 'calibration response of uncalibrated data';
ii=ii+1;

return

% use this template for any future additions
plist{ii} = '';
ptype{ii} = '';
pdef{ii} = '';
%pval{ii} = ;
ii=ii+1;
