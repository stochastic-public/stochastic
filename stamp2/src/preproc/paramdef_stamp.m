function [plist, ptype, pdef, pval, ii] = paramdef_stamp(plist, ptype, ...
  pdef, pval)
% function [plist, ptype, pdef, pval, ii] = paramdef_stamp(plist, ptype, ... 
% pdef, pval)
% support function for paramlist.m.  See that function for more details.
% E Thrane

% set variable counter
ii = length(plist)+1;

plist{ii} = 'outputDirPrefix';
ptype{ii} = 'str';
pdef{ii} = 'location that stamp mat files will be written to';
ii=ii+1;

plist{ii} = 'outputFilePrefix';
ptype{ii} = 'str';
pdef{ii} = 'name of stamp mat files';
ii=ii+1;

% 10/9/14 default to matlab
plist{ii} = 'jobsFileCommentStyle';
ptype{ii} = 'str';
pdef{ii} = '';
pval{ii} = 'matlab';
ii=ii+1;

plist{ii} = 'cacheMat';
ptype{ii} = 'str';
pdef{ii} = 'use mat file as cachefile';
ii=ii+1;

plist{ii} = 'segsMat';
ptype{ii} = 'str';
pdef{ii} = 'use mat file to specify segment list';
ii=ii+1;

plist{ii} = 'verbose';
ptype{ii} = 'num';
pdef{ii} = 'print extra info to screen';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'saveGPS_diff';
ptype{ii} = 'num';
pdef{ii} = 'not sure what this does';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'fft1dataWindow';
ptype{ii} = 'num';
pdef{ii} = 'enter fft1 window by hand';
ii=ii+1;

plist{ii} = 'fft2dataWindow';
ptype{ii} = 'num';
pdef{ii} = 'enter fft2 window by hand';
ii=ii+1;

plist{ii} = 'batch';
ptype{ii} = 'num';
pdef{ii} = 'create multiple mat files per job';
pval{ii} = true;
ii=ii+1;

plist{ii} = 'mapsize';
ptype{ii} = 'num';
pdef{ii} = 'size of each stamp mat file';
ii=ii+1;

plist{ii} = 'pp_seed';
ptype{ii} = 'num';
pdef{ii} = 'random preproc seed';
ii=ii+1;

plist{ii} = 'stampinj';
ptype{ii} = 'num';
pdef{ii} = 'perform an injection with stamp';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'stamp.ra';
ptype{ii} = 'num';
pdef{ii} = 'right ascension of stamp injection';
ii=ii+1;

plist{ii} = 'stamp.decl';
ptype{ii} = 'num';
pdef{ii} = 'declination of stamp injection';
ii=ii+1;

plist{ii} = 'stamp.iota';
ptype{ii} = 'num';
pdef{ii} = 'inclination angle of stamp injection';
ii=ii+1;

plist{ii} = 'stamp.cosi';
ptype{ii} = 'num';
pdef{ii} = 'an alternate field for the cosine of the inclination angle';
ii=ii+1;

plist{ii} = 'stamp.psi';
ptype{ii} = 'num';
pdef{ii} = 'polarization angle of stamp injection';
ii=ii+1;

plist{ii} = 'stamp.startGPS';
ptype{ii} = 'num';
pdef{ii} = 'beginning injection time';
ii=ii+1;

plist{ii} = 'stamp.file';
ptype{ii} = 'str';
pdef{ii} = 'injection file name';
ii=ii+1;

plist{ii} = 'stamp.inj_type';
ptype{ii} = 'str';
pdef{ii} = 'Type of injection, e.g., fly, adi, cw, etc.';

ii=ii+1;

plist{ii} = 'stamp.fly_waveform';
ptype{ii} = 'str';
pdef{ii} = 'Type of waveform if injecting on the fly, e.g., half_sg etc.';

ii=ii+1;

plist{ii} = 'stamp.h0';
ptype{ii} = 'num';
pdef{ii} = 'strain amplitude For injected on-the-fly injections';
ii=ii+1;

plist{ii} = 'stamp.f0';
ptype{ii} = 'num';
pdef{ii} = 'frequency For injected on-the-fly injections';
ii=ii+1;

plist{ii} = 'stamp.phi0';
ptype{ii} = 'num';
pdef{ii} = 'initial phase For injected on-the-fly injections';
ii=ii+1;

plist{ii} = 'stamp.fdot';
ptype{ii} = 'num';
pdef{ii} = 'df/dt for injected on-the-fly injections';
ii=ii+1;

plist{ii} = 'stamp.start';
ptype{ii} = 'num';
pdef{ii} = 'start time for injected on-the-fly injections';
ii=ii+1;

plist{ii} = 'stamp.duration';
ptype{ii} = 'num';
pdef{ii} = 'duration for injected on-the-fly injections';
ii=ii+1;

plist{ii} = 'stamp.mass1';
ptype{ii} = 'num';
pdef{ii} = 'mass 1 For injected on-the-fly injections';
ii=ii+1;

plist{ii} = 'stamp.mass2';
ptype{ii} = 'num';
pdef{ii} = 'mass 2 for injected on-the-fly injections';
ii=ii+1;

plist{ii} = 'stamp.alphasat';
ptype{ii} = 'num';
pdef{ii} = 'saturation amplitude for injected on-the-fly injections';
ii=ii+1;

plist{ii} = 'stamp.eB';
ptype{ii} = 'num';
pdef{ii} = 'epsilon for injected on-the-fly injections';
ii=ii+1;

plist{ii} = 'stamp.alpha';
ptype{ii} = 'num';
pdef{ii} = 'power scaling factor For stamp injections';
ii=ii+1;

plist{ii} = 'stamp.tau';
ptype{ii} = 'num';
pdef{ii} = 'characteristic time for injected on-the-fly half sine-Gaussian injections';
ii=ii+1;

plist{ii} = 'stamp.nn';
ptype{ii} = 'num';
pdef{ii} = 'parames for msmagnetar';
ii=ii+1;

plist{ii} = 'stamp.epsilon';
ptype{ii} = 'num';
pdef{ii} = 'params for msmagnetar';
ii=ii+1;

% used in preproc to add MDC files
plist{ii} = 'stampmdc';
ptype{ii} = 'num';
pdef{ii} = 'perform an mdc injection with stamp';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'stamp.mdclist';
ptype{ii} = 'str';
pdef{ii} = 'mdc frame file list';
ii=ii+1;

plist{ii} = 'stamp.mdcchannel1';
ptype{ii} = 'str';
pdef{ii} = 'mdc channel ifo 1';
ii=ii+1;

plist{ii} = 'stamp.mdcchannel2';
ptype{ii} = 'str';
pdef{ii} = 'mdc channel ifo 2';
ii=ii+1;

% used in preproc for STAMP studies--------------------------------------------
plist{ii} = 'doDetectorNoiseSim';
ptype{ii} = 'num';
pdef{ii} = 'simulated detector noise For stamp';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doDifferentNoise';
ptype{ii} = 'num';
pdef{ii} = 'not sure';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'DetectorNoiseFile';
ptype{ii} = 'str';
pdef{ii} = 'file specifying detector noise';
ii=ii+1;

plist{ii} = 'DetectorNoiseFile2';
ptype{ii} = 'str';
pdef{ii} = 'not sure...maybe For LIGO-Virgo studies?';
ii=ii+1;

plist{ii} = 'sampleRate';
ptype{ii} = 'num';
pdef{ii} = 'sample rate For simulated stamp data';
ii=ii+1;
%------------------------------------------------------------------------------

plist{ii} = 'stochmap';
ptype{ii} = 'num';
pdef{ii} = 'Save data in stamp-style ft-amps';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'startGPS';
ptype{ii} = 'num';
pdef{ii} = 'beginning of ft map to analyze If clustermap is called from preproc';
ii=ii+1;

plist{ii} = 'endGPS';
ptype{ii} = 'num';
pdef{ii} = 'End of ft map to analyze If clustermap is called from preproc';
ii=ii+1;

plist{ii} = 'fixAntennaFactors';
ptype{ii} = 'num';
pdef{ii} = 'use fixed antenna factors to turn off rotation of the earth';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'bestAntennaFactors';
ptype{ii} = 'num';
pdef{ii} = 'use best antenna factors to turn off rotation of the earth';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'storemats';
ptype{ii} = 'num';
pdef{ii} = 'Save mat files (instead of frame files or no files at all)';
pval{ii} = true;
ii=ii+1;

plist{ii} = 'outputfiledir';
ptype{ii} = 'str';
pdef{ii} = 'location of output mat files';
ii=ii+1;

plist{ii} = 'outputfilename';
ptype{ii} = 'str';
pdef{ii} = 'starting name of output mat files';
ii=ii+1;

plist{ii} = 'compress';
ptype{ii} = 'num';
pdef{ii} = 'shall we compress the data in time?';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'npixels';
ptype{ii} = 'num';
pdef{ii} = 'number of (usually overlapping) segments to average to create one very long pixel';
ii=ii+1;

plist{ii} = 'ignoreGaps';
ptype{ii} = 'num';
pdef{ii} = 'shall we ignore data gaps? (useful for long duration maps)';
pval{ii} = false;
ii=ii+1;

return

% use this template for any future additions
plist{ii} = '';
ptype{ii} = '';
pdef{ii} = '';
%pval{ii} = ;
ii=ii+1;
