#include "mex.h"

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

   /* initialize input variables */
   double *in_real;        /* values of input real array */
   double *in_complex;     /* values of input complex array */
   double *in_jlow;        /* values of input begin indices */
   double *in_jhigh;       /* values of input end indices */
   double *in_jlength;     /* length of input index arrays */ 

   /* initialize output variables */
   mxArray *X0, *X1; /* MEX arrays to return to Matlab code */
   double *a, *b;   /* pointers to MEX arrays */

   /* retrieve the input data */
   in_real = mxGetPr(prhs[0]);
   in_complex = mxGetPr(prhs[1]);
   in_jlow = mxGetPr(prhs[2]);
   in_jhigh = mxGetPr(prhs[3]);
   in_jlength = mxGetPr(prhs[4]);

   /* determine matrix dimensions */
   int arrayLength = (int)in_jlength[0];  

   /* preallocate real and complex arrays */
   double real_sum[arrayLength];
   double complex_sum[arrayLength];

   /* set real and complex arrays to 0 */
   for (int count = 0; count < arrayLength; count++) {
      real_sum[count] = 0;
      complex_sum[count] = 0;
   }

   /* populate real and complex arrays with sums */
   for(int ii = 0; ii < arrayLength; ii+=1) {
      int array_start, array_end;
      array_start = in_jlow[ii]-1; array_end = in_jhigh[ii]-1;

      for(int jj = array_start; jj <= array_end; jj+=1) {
         real_sum[ii] = real_sum[ii] + in_real[jj];
         complex_sum[ii] = complex_sum[ii] + in_complex[jj];
      } 
   }

   /* prepare other outputs */
   X0 = plhs[0] = mxCreateDoubleMatrix(arrayLength, 1, mxREAL);
   X1 = plhs[1] = mxCreateDoubleMatrix(arrayLength, 1, mxREAL);

   /* assign pointers */
   a = mxGetPr(X0);
   b = mxGetPr(X1);

   /* assign outputs */
   for (int count = 0; count < arrayLength; count++) {
      a[count] = real_sum[count]; 
      b[count] = complex_sum[count];
   }

}     

