% MPIIR_CORE - returns arrays containing the core iteration algorithm
% output of iwave. This is essentially a running measure of the Laplace
% transform of the data component at the line frequency.
% Ed Daw, 30/08/2011 e.daw@shef.ac.uk
%
% useage: [<data_out_Iphase>, <data_out_Qphase>, <filter_state_out>] =
%            miwave_core(<data_in>, <number of data points>,
%            <filter state in>)
%
% here the arrays data_out_Iphase, data_out_Qphase are double
% precision vectors all of the same number of elements, nelements
% is that number of elements , and filter_state_in and
% filter_state_out are arrays of doubles defining the filter state.
% The first time the filter is called, filter_state_in should have
% previously been initialized using miwave_construct.
