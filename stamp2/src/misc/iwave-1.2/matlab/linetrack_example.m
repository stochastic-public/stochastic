function linetrack_example(nsamples,fstart,srate,tau)
% LINETRACK_EXAMPLE - generate a fluctuating frequency
% useage: freqgen(<number of samples>, <start frequency (Hz)>, ...
%                  <sampling rate (Hz)>, <response time for lowpass>)
%
% for example, try this:
% linetrack_example(163840,100,16384,0.1)
% this generates 10 seconds of
% samples at 16384Hz from a 
% sine wave at an initial frequency of 100Hz,
% which then makes a random walk, and also 
% a linetracker that follows the frequency 
% of this wandering line with reasonable 
% accuracy. This is a rather contrived 
% example, but it shows you how to use the code.
% Ed Daw, 7th May 2015

% single pole lowpass for noisy frequency
lowpassf=zpk([],-1/tau,1);
lowpassd=c2d(lowpassf,1/srate,'tustin');
[bd,ad]=tfdata(lowpassd,'v');
% generate initial noise
indata=randn(nsamples,1);
% filter the data
fdata=filter(bd,ad,indata);
% find the cumulative data
cfdata=fstart+cumsum(fdata);
% generate a sine wave whose frequency wanders
% reflecting cfdata.
phase=mod(cumsum(2*pi*cfdata/srate),2*pi);
inwave=sin(phase);
% initialise iwave filter
sv=miwave_phaselock_linetracker_construct( ...
    srate, ... % sampling rate
    fstart+50, ... % starting frequency
    tau/40, ... % response time
    0);   % when to turn on locking
% run linetracker on input wave data
[phase,amp,freq,filt,sv]= ...
    miwave_phaselock_linetracker_iterate( ...
    inwave,length(inwave),sv);
% create time axis for plot
time=linspace(0,(nsamples-1)/srate,nsamples);
% make the plot
plot(time,cfdata,'-',time,freq,'-');
xlabel('time (s)');
ylabel('frequency (Hz)');
legend(['input data';'freq recon']);

