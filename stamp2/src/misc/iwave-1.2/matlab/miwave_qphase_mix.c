/* wrapper code for MATLAB to access a simple, non frequency tracking */
/* iwave line monitor */
/* Ed Daw, 30th August 2011 */
#include <mex.h>
#include <iwave.h>
#include <stdio.h>

void mexFunction( int nlhs,
		  mxArray *plhs[], 
		  int nrhs, 
		  const mxArray *prhs[]) {
  int ndata;
  double* data_in;
  double* data_out_intermed;
  double* data_out_phase;
  double* data_out_amplitude;
  double* fstate_in;
  double* fstate_out;
  int sc;
  
  /* wire input arguments */
  data_in=(double*)mxGetPr(prhs[0]);
  ndata=(int)(*((double*)mxGetPr(prhs[1])));
  fstate_in=(double*)mxGetPr(prhs[2]);

  /* wire output arguments */
  plhs[0]=mxCreateDoubleMatrix(1,ndata,mxREAL);
  data_out_intermed=mxGetPr(plhs[0]);
  plhs[1]=mxCreateDoubleMatrix(1,ndata,mxREAL);
  data_out_phase=mxGetPr(plhs[1]);
  plhs[2]=mxCreateDoubleMatrix(1,ndata,mxREAL);
  data_out_amplitude=mxGetPr(plhs[2]);
  plhs[3]=mxCreateDoubleMatrix(1,iwave_qphase_size(),mxREAL);
  fstate_out=mxGetPr(plhs[3]);
  
  /* initialize state data */
  for(sc=0;sc<iwave_qphase_size();++sc) {
    fstate_out[sc]=fstate_in[sc];
  }
  /* call iwave library function */
  iwave_qphase_mix(data_in,
		  data_out_intermed,
		  data_out_phase,
		  data_out_amplitude,
		  ndata,
		  fstate_out);
  
  return;
}
