% MPIIR_QPHASE_MIX - monitor the amplitude and phase of a line at
% fixed frequency
% Ed Daw, 31st August 2011
%
% useage: [<product of filter Q phase and input data, unfiltered>,
%          <above product, filtered to remove upper sideband>,
%          <estimate of amplitude of line in data>,
%          <updated filter state data>] =
%                 iwave_qphase_mix( <input data>, 
%                                  <number of input data samples>, 
%                                  <input state data> );
%
