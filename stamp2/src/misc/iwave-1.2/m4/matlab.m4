dnl  ====================================================
dnl             MATLAB library macros
dnl  ====================================================

dnl
dnl  CHECK_MATLAB - find Matlab libraries
dnl  ----------------------------------------------------
AC_DEFUN([CHECK_MATLAB],
[
AC_ARG_WITH(matlab,
[  --with-matlab=DIR	the directory where Matlab is installed ],
MATLAB_DIR=${withval},
MATLAB_DIR=)

if test -n "${MATLAB_DIR}"
then
    AC_MSG_CHECKING(for MATLAB mex command)
    if test -x ${MATLAB_DIR}/bin/mex
    then
	MEXCOMMAND=${MATLAB_DIR}/bin/mex
	AC_MSG_RESULT([${MEXCOMMAND}])
	AC_SUBST(MEXCOMMAND)
	havemex=true
    else
	MEXCOMMAND=`which mex`
	if test -x ${MEXCOMMAND}
	then
	    AC_SUBST(MEXCOMMAND)
	    AC_MSG_RESULT([${MEXCOMMAND}])
	    havemex=true
	else
	    AC_MSG_RESULT([no])
	    havemex=false
	fi
    fi
else
    AC_MSG_RESULT([matlab wrapper will not be built.])
    havemex=false
fi		

])

dnl
dnl  CHECK MEXEXT - figure out the extension for MATLAB mex files
dnl  ------------------------------------------------------------
AC_DEFUN([CHECK_MEXEXT],
[
AC_ARG_WITH(matlab,
[  --with-matlab=DIR    the directory where Matlab is installed ],
MATLAB_DIR=${withval},
MATLAB_DIR=)

if test -n "${MATLAB_DIR}"
then
    AC_MSG_CHECKING(for MATLAB mex extension)
    if test -x ${MATLAB_DIR}/bin/mexext
    then
	MEXEXT=`${MATLAB_DIR}/bin/mexext`
	AC_MSG_RESULT([${MEXEXT}])
	AC_SUBST(MEXEXT)
	havemexext=true
    else
	MEXEXTCMD=`which mexext`
	if test -x ${MEXEXTCMD}
	then
	    MEXEXT=`${MEXEXTCMD}`
	    AC_SUBST(MEXEXT)
	    AC_MSG_RESULT([${MEXEXT}])
	    havemexext=true
	else
	    AC_MSG_RESULT([no])
	    havemexext=false
	fi
    fi
else
    AC_MSG_RESULT([cannot get extension for mex files. Matlab wrapper build disabled.])
    havemexext=false
fi		

])

