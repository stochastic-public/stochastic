/* iwave complex IIR line filter */
/* Ed Daw, 12th August 2011     */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iwave.h>

/* return the size of the state vector for a simple IWAVE filter */
int iwave_notrack_size(void) {
  return (int) STATESIZE;
}

/* return the size of the state vector for a quad phase IWAVE mixer filter */
int iwave_qphase_size(void) {
  return (int) STATESIZE;
}

/* return the size of the state vector for a phaselocked IWAVE filter */
int iwave_phaselock_size(void) {
  return (int) PHASELOCKSTATESIZE;
}

/* initialise a iwave filter specifically for narrow lines */
/* */
int iwave_construct(double fs_in, 
		   double tau_in, 
		   double fline_in,
		   double fstate[]) {
  
  /* useful derived quantities to be calculated */
  double ts;  /* sampling period */
  double delta; /* phase shift of angle variable per sample */
  double w; /* weight of new data in exponential average */
  double phi; /* lag of filter response to exp(-i\delta) excitiation */
  double af; /* modulus of filter response to exp(+i\delta) excitation */
  double ab; /* modulus of filter response to exp(-i\delta) excitation */
  double alpha; /* angular separation between drive and lead of yn over */
                /* ellipse semi major axis */
  double beta; /* argument of ellipse semi major axis */

  /* calculate some useful derived quantities */
  ts = 1/fs_in; 
  w = ts / tau_in;
  delta = 2 * IWAVEPI * fline_in * ts;
  af = 0.5;
  ab = 0.5*w/sqrt(1-2*(1-w)*cos(2*delta)+pow((1-w),2)); /* see lb6 page 97 */
  phi = atan( (2-w)*(tan(delta))/w ); /* see labbook 6 page 97 */ 
  alpha=(delta-phi)/2;
  beta=(delta+phi)/2;
  
  /* calculate filter coefficients for iwave filter. Following usual convention */
  /* that previous outputs times coefficients of those outputs are subtracted from */
  /* the filter sum and inputs times coefficients are added to the filter sum */
  fstate[0] = -cos(delta)*(1-w);
  fstate[1] = +sin(delta)*(1-w);
  fstate[2] = +w*cos(delta);
  fstate[3] = -fstate[1];
  fstate[4] = +fstate[0];
  fstate[5] = +w*sin(delta);

  /* calculate matrix coefficients for I and Q phase production */
  /* see labbook 6 page 118 */
  fstate[6] = cos(alpha)*cos(beta)/(af+ab)-sin(alpha)*sin(beta)/(af-ab);
  fstate[7] = cos(alpha)*sin(beta)/(af+ab)+sin(alpha)*cos(beta)/(af-ab);
  fstate[8] = -sin(alpha)*cos(beta)/(af+ab)-cos(alpha)*sin(beta)/(af-ab);
  fstate[9] = -sin(alpha)*sin(beta)/(af+ab)+cos(alpha)*cos(beta)/(af-ab);

  /* outputs for useful derived quantities */
  fstate[10]=w; 
  fstate[11]=0;
  fstate[12]=0;
  
  return 0;
}

int iwave_core(double data_in[],
	      double data_out_iphase[],
	      double data_out_qphase[],
	      int ndata,
	      double fstate[]) {
  double yprevr, yprevi;
  int dcount;
  double ynewr, ynewi;
  double data_in_val;

  yprevr=fstate[11];
  yprevi=fstate[12];
  /* apply filter to input data array */
  for(dcount=0;dcount<ndata;++dcount) {
    /* if the input data is a nan, set it to zero */
    if(isnan(data_in[dcount])==0) {
      data_in_val=data_in[dcount]; 
    } else {
      data_in_val=0.0;
    }
    /* core iteration formula */
    ynewr= (-fstate[0]*yprevr-fstate[1]*yprevi+fstate[2]*data_in_val);
    ynewi= (+fstate[1]*yprevr-fstate[0]*yprevi+fstate[5]*data_in_val);
    yprevr=ynewr;
    yprevi=ynewi;
    data_out_iphase[dcount]=ynewr;
    data_out_qphase[dcount]=ynewi;
  }
  fstate[11]=ynewr;
  fstate[12]=ynewi;

  return 0;
}


int iwave_track_line(double data_in[],
		    double data_out_iphase[],
		    double data_out_qphase[],
		    int ndata,
		    double fstate[]) {

    double yprevr,yprevi;
    int dcount;
    double ynewr, ynewi;
    double data_in_val;
    double w;
  
    /* extract previous outputs from the state data */
    yprevr=fstate[11];
    yprevi=fstate[12];
    /* apply filter to input data array */
    w=fstate[10];
    for(dcount=0; dcount<ndata; ++dcount) {
      if(isnan(data_in[dcount])==0) {
	data_in_val=data_in[dcount]; 
      } else {
	data_in_val=0.0;
      }
      ynewr = (-fstate[0]*yprevr-fstate[1]*yprevi+fstate[2]*data_in_val);
      ynewi = (-fstate[3]*yprevr-fstate[4]*yprevi+fstate[5]*data_in_val);
      yprevr = ynewr;
      yprevi = ynewi;
      data_out_iphase[dcount]=ynewr;
      data_out_qphase[dcount]=ynewi;
    }  
    fstate[11]=ynewr;
    fstate[12]=ynewi;
    return 0;
  }  

int iwave_oscillator(double data_out[],
		    int ndata,
		    double fstate[]) {
  int dcount;
  double yi;
  double cosdelta, sindelta;
  double input;
  /* get cos and sin delta from the state data */
  cosdelta=fstate[2]/fstate[10];
  sindelta=fstate[5]/fstate[10];
  /* loop over samples feeding in the previous output every time */
  for(dcount=0; dcount<ndata; ++dcount) {
    /* rotate phase of previous output by one sample forward, */
    /* feed real part back to input */
    input=fstate[13]*cosdelta-fstate[14]*sindelta;
    iwave_track_line(&input,data_out+dcount,&yi,1,fstate);
    fstate[13]=data_out[dcount];
    fstate[14]=yi;
    printf("%f\n",fstate[11]);
  }
  return 0;
}

int iwave_filter_line(double data_in[],
		     double data_out[],
		     int ndata,
		     double fstate[]) {
  
  double yprevr,yprevi;
  int dcount;
  double ynewr, ynewi;
  double data_in_val;
  
  yprevr=fstate[11];
  yprevi=fstate[12];
  /* apply filter to input data array */
  for(dcount=0;dcount<ndata;++dcount) {
    if(isnan(data_in[dcount])==0) {
      data_in_val=data_in[dcount]; 
    } else {
      data_in_val=0.0;
    }
    ynewr= (-fstate[0]*yprevr-fstate[1]*yprevi+fstate[2]*data_in_val);
    ynewi= (-fstate[3]*yprevr-fstate[4]*yprevi+fstate[5]*data_in_val);
    yprevr=ynewr;
    yprevi=ynewi;
    data_out[dcount]=fstate[6]*ynewr+fstate[7]*ynewi-data_in[dcount];
  }
  fstate[11]=ynewr;
  fstate[12]=ynewi;

  return 0;
}

int iwave_qphase_detector_construct(double fs_in,
				   double tau_in,
				   double fline_in,
				   double fstate[]) {
  iwave_construct(fs_in, tau_in, fline_in, fstate);
  return 0;
}

int iwave_qphase_mix(double data_in[],
		    double data_intermed[],
		    double qmix_out[],
		    double amp_out[],
		    int ndata,
		    double fstate[]) {
  double yprevr,yprevi;
  int dcount;
  double ynewr, ynewi;
  double iphase, qphase, norm;
  double data_in_val;

  yprevr=fstate[11];
  yprevi=fstate[12];
  /* apply filter to input data array */
  for(dcount=0;dcount<ndata;++dcount) {  
    /* check that input data is not nan */
    if(isnan(data_in[dcount])==0) {
      data_in_val=data_in[dcount]; 
    } else {
      data_in_val=0.0;
    }
    /* run iwave filter over data sample */
    ynewr= (-fstate[0]*yprevr-fstate[1]*yprevi+fstate[2]*data_in_val);
    ynewi= (-fstate[3]*yprevr-fstate[4]*yprevi+fstate[5]*data_in_val);
    yprevr=ynewr;
    yprevi=ynewi;
    /* in phase sine wave sample */
    iphase=fstate[6]*ynewr+fstate[7]*ynewi;
    /* mix sample */
    qphase=fstate[8]*ynewr+fstate[9]*ynewi;
    /* amplitude */
    amp_out[dcount] = iphase*iphase + qphase*qphase;
    /* input data mixed with q phase output */
    data_intermed[dcount] = qphase*data_in[dcount];
    /* subtract product of I and Q phases */
    /*data_intermed[dcount] -= 0.5*qphase*iphase;*/ 
    qmix_out[dcount] = qphase*iphase - data_intermed[dcount];
  }
  fstate[11]=ynewr;
  fstate[12]=ynewi;  
  /* normalize to amplitude of the incoming wave */
  for(dcount=0;dcount<ndata;++dcount) {
    qmix_out[dcount]=2*qmix_out[dcount]/amp_out[dcount];
    amp_out[dcount]=sqrt(amp_out[dcount]);  
  }
  /* add lowpass filter to remove upper sideband on phase here */

  return 0;
}

 
int iwave_phaselock_linetracker_construct(double fs_in,
					 double tau_in,
					 double fline_in,
					 double starttime,
					 double fstate[]) {

  /* pointer to state information associated with the phase locked loop*/
  double* pstate;
  double ts;
  /* time parameters for in-loop feedback filter */
  double t2, t1plust2;
  /* derived servo parameters */
  double gain;

  /* construct phase detector filter */
  iwave_qphase_detector_construct(fs_in, tau_in, fline_in, fstate);
  /* point pstate at the filter state information  */
  pstate=fstate+PHASELOCKSTARTOFFSET;
  /* set the initial frequency of the line */
  pstate[0]=fline_in; /* line frequency */
  pstate[1]=starttime; /* time after start to close feedback loop */
  pstate[2]=fs_in;     /* sampling frequency */
  ts=1/(fs_in);
  gain=(ts/tau_in)*(ts/tau_in);
  t2=tau_in*(sqrt(2)-1);
  /* bilinear transform used to construct feedback filter */
  pstate[3]=(1+2*t2/ts)/3;  /* b0 */
  pstate[4]=(1-2*t2/ts)/3;  /* b1 */
  pstate[5]=-1/3;          /* a1 */
  pstate[6]=0;          /* state buffer for feedback filter          */
  pstate[7]=gain;
  pstate[8]=0;          /* time after start of running (s)           */
  pstate[9]=0;          /* loop closed bit (0=open, 1=closed)        */
  
  pstate[10]=0;         /* lower frequency limit (if limits imposed) */
  pstate[11]=0;         /* upper frequency limit (if limits imposed) */
  pstate[12]=0;
  pstate[13]=0;

  return 0;
}
 
int iwave_phaselock_linetracker_iterate(double data_in[],
				       double qmix_out[], /* phase excursions */
				       double amp_out[], /* wave amplitude */
				       double freq_out[], /* frequency shift */
				       double filtered_out[],
				       int ndata,
				       double fstate[]) {
  double yprevr,yprevi;
  int dcount;
  double ynewr, ynewi;
  double mixed;
  double iphase, qphase, norm;
  double data_in_val;
  double* pstate;
  double ddelta;
  double time;
  double starttime;
  double timeinc;
  double phase_unfiltered;
  feedbackstate plloopstate=loopopen;
  double olpbbuf; /* buffer for out of loop feedback state */
  double newpr, newpi, prevpr, prevpi; /* buffers for the 2f */
                                       /* out of band notch */

  /* point pstate to beginning of phaselock loop state data */
  pstate=fstate + PHASELOCKSTARTOFFSET;
  /* get time per sample */
  timeinc=1/pstate[2];

  yprevr=fstate[11];
  yprevi=fstate[12];
  /* apply filter to input data array */
  for(dcount=0;dcount<ndata;++dcount) {  
    /* check that input data is not nan */
    if(isnan(data_in[dcount])==0) {
      data_in_val=data_in[dcount]; 
    } else {
      data_in_val=0.0;
    }
    /* run iwave filter over data sample */
    ynewr= (-fstate[0]*yprevr-fstate[1]*yprevi+fstate[2]*data_in_val);
    ynewi= (-fstate[3]*yprevr-fstate[4]*yprevi+fstate[5]*data_in_val);
    yprevr=ynewr;
    yprevi=ynewi;
    /* in phase sine wave sample */
    iphase=fstate[6]*ynewr+fstate[7]*ynewi;
    /* mix sample */
    qphase=fstate[8]*ynewr+fstate[9]*ynewi;
    /* amplitude */
    amp_out[dcount] = iphase*iphase + qphase*qphase;
    /* input data mixed with q phase output */
    mixed = qphase*data_in[dcount];
    /* subtract half of I and Q phase product */
    mixed -= iphase*qphase;
    /* subtract I phase from input data */
    filtered_out[dcount]=data_in_val-iphase;
    /* filter the mixed data removing the upper sideband */

    /* normalize the mixed data with the square of the amplitude, as long */
    /* as the amplitude is not zero. Don't normalize if the amplitude is zero */
    /* as this will generate a nan. */
    if(amp_out[dcount] != 0.0) {
      qmix_out[dcount]=-mixed/amp_out[dcount];
    } else {
      qmix_out[dcount]=-mixed;
    }

    /* apply filter to input data array */
    phase_unfiltered = qmix_out[dcount];
   
    /* STATE MACHINE FOR CLOSING THE LOOP */

    /* adjust state and close loop if time exceeds starttime */
    if(pstate[9]<0.5) {
      plloopstate=loopopen;
      time = pstate[8];
      starttime=pstate[1];
      if(time>starttime) {
	pstate[9]=1.0;
	/* close the feedback loop */
	plloopstate=loopclosed;
      } else {
	plloopstate=loopopen;
	pstate[8]+=timeinc;
      }
    } else {
      plloopstate=loopclosed;
    }
    
    /* CALL THE LOOP CODE */

    /* implement feedback if the the loop is closed */
    if(plloopstate==loopclosed) {
      ddelta=phase_unfiltered;
      /* apply correction to state data */
      iwave_phaselock_alter_filters(ddelta, fstate);
      /* reset frequency */
      freq_out[dcount]=pstate[0];
    }

    /* LOOP CODE HAS BEEN CALLED BY HERE IF THE LOOP IS CLOSED */

    /* calculate amplitude by taking square root of amp_out */
    amp_out[dcount]=sqrt(amp_out[dcount]);
  }
  /* update memory buffers for iwave filter */
  fstate[11]=ynewr;
  fstate[12]=ynewi;

  return 0;
}

int iwave_phaselock_alter_filters(double ddelta, double fstate[]) {
  double* pstate;
  double fddelta;
  double dmod;

  /* make pointer to phaselock state data */
  pstate=fstate+PHASELOCKSTARTOFFSET;

  /* phaselock feedback filter */
  dmod = ddelta - pstate[6]*pstate[5];
  fddelta=dmod*pstate[3]+pstate[6]*pstate[4];
  pstate[6] = dmod;

  /* multiply by gain after filter */
  fddelta *= pstate[7];

  /* alter carrier frequency */
  pstate[0]=pstate[0]+fddelta*pstate[2]/(2*( IWAVEPI ));
  /* adjust coefficents for carrier filter */
  iwave_filter_alterstate(fddelta, fstate);
  return 0;
}

int iwave_phaselock_alter_filters_limitf(double ddelta, double fstate[]) {
  double* pstate;
  double fddelta;
  double dmod;

  /* make pointer to phaselock state data */
  pstate=fstate+PHASELOCKSTARTOFFSET;

  /* phaselock feedback filter */
  dmod = ddelta - pstate[6]*pstate[5];
  fddelta=dmod*pstate[3]+pstate[6]*pstate[4];
  pstate[6] = dmod;

  /* multiply by gain after filter */
  fddelta *= pstate[7];

  /* alter carrier frequency */
  pstate[0]=pstate[0]+fddelta*pstate[2]/(2*( IWAVEPI ));
  /* here we apply the limits on the frequency */
  if( pstate[0] < pstate[10] ) {
    pstate[0]=pstate[10]; 
  } else if( pstate[0] > pstate[11] ) {
    pstate[0]=pstate[11];
  }
  /* adjust coefficents for carrier filter */
  iwave_filter_alterstate(fddelta, fstate);
  return 0;
}


int iwave_filter_alterstate(double ddelta, double fstate[]) {

  double* pstate;
  double ffilt;
  double ts;
  double delta;
  double locsin,loccos,locsin2delta,loccos2delta;
  double carry1,carry2,carry3,carry4,ab,phi,alpha,beta,af;
  double w;

  pstate=fstate+PHASELOCKSTARTOFFSET;
  ffilt=pstate[0];
  ts=1/pstate[2];
  delta=2*(IWAVEPI)*ffilt*ts;
  locsin=sin(delta);
  loccos=cos(delta);
  w=fstate[10];
  fstate[0]=(-loccos)*(1-w); 
  fstate[1]=(+locsin)*(1-w);
  fstate[2]=w*loccos;
  fstate[3]=-fstate[1];
  fstate[4]=fstate[0];
  fstate[5]=w*locsin;
  /*locsin2delta=2*locsin*loccos;*/
  loccos2delta=(1-2*locsin*locsin);
  ab=0.5*w/sqrt(1-2*(1-w)*loccos2delta+(1-w)*(1-w));
  af=0.5;
  carry4=(2-w)*(locsin/loccos)/w;
  phi=atan2(carry4,(double)1.0);
  alpha=(delta-phi)/2;
  beta=(delta+phi)/2;
  carry2=sin(alpha);
  carry1=cos(alpha);
  carry4=sin(beta);
  carry3=cos(beta);
  fstate[6]=carry1*carry3/(af+ab)-carry2*carry4/(af-ab);
  fstate[7]=carry1*carry4/(af+ab)+carry2*carry3/(af-ab);
  fstate[8]=-carry2*carry3/(af+ab)-carry1*carry4/(af-ab);
  fstate[9]=-carry2*carry4/(af+ab)+carry1*carry3/(af-ab);


  return 0;
}

int iwave_phaselock_linetracker_limitf_construct(double fs_in,
						double tau_in,
						double fline_in,
						double starttime,
						double f_lowerlimit,
						double f_upperlimit,
						double fstate[]) {
  /* pointer to beginning of state data for feedback */
  double* pstate;
  /* construct a regular linetracker but also set frequency limits */
  iwave_phaselock_linetracker_construct(fs_in, tau_in, fline_in,
				       starttime, fstate);
  pstate=fstate+PHASELOCKSTARTOFFSET;
  /* set frequency limits */
  pstate[10]=f_lowerlimit;
  pstate[11]=f_upperlimit;
  return 0;
}

int iwave_phaselock_linetracker_limitf_iterate(double data_in[],
					      double qmix_out[],
					      double amp_out[],
					      double freq_out[],
					      double filtered_out[],
					      int ndata,
					      double fstate[]) {
  /* this code is a copy of that for an unconstrained line tracker, */
  /* except that for every sample, if the reconstructed frequency is */
  /* outside the bounds [f_lowerlimit, f_upperlimit], the reconstructed */
  /* frequency is set to be equal to the bound. */
  double yprevr,yprevi;
  int dcount;
  double ynewr, ynewi;
  double mixed;
  double iphase, qphase, norm;
  double data_in_val;
  double* pstate;
  double ddelta;
  double time;
  double starttime;
  double timeinc;
  double phase_unfiltered;
  feedbackstate plloopstate=loopopen;
  double olpbbuf; /* buffer for out of loop feedback state */
  double newpr, newpi, prevpr, prevpi; /* buffers for the 2f out of band notch */

  /* point pstate to beginning of phaselock loop state data */
  pstate=fstate + PHASELOCKSTARTOFFSET;
  /* get time per sample */
  timeinc=1/pstate[2];

  yprevr=fstate[11];
  yprevi=fstate[12];
  /* apply filter to input data array */
  for(dcount=0;dcount<ndata;++dcount) {  
    /* check that input data is not nan */
    if(isnan(data_in[dcount])==0) {
      data_in_val=data_in[dcount]; 
    } else {
      data_in_val=0.0;
    }
    /* run iwave filter over data sample */
    ynewr= (-fstate[0]*yprevr-fstate[1]*yprevi+fstate[2]*data_in_val);
    ynewi= (-fstate[3]*yprevr-fstate[4]*yprevi+fstate[5]*data_in_val);
    yprevr=ynewr;
    yprevi=ynewi;
    /* in phase sine wave sample */
    iphase=fstate[6]*ynewr+fstate[7]*ynewi;
    /* mix sample */
    qphase=fstate[8]*ynewr+fstate[9]*ynewi;
    /* amplitude */
    amp_out[dcount] = iphase*iphase + qphase*qphase;
    /* input data mixed with q phase output */
    mixed = qphase*data_in[dcount];
    /* subtract half of I and Q phase product */
    mixed -= iphase*qphase;
    /* subtract I phase from input data */
    filtered_out[dcount]=data_in_val-iphase;
    /* filter the mixed data removing the upper sideband */

    /* normalize the mixed data with the square of the amplitude, as long */
    /* as the amplitude is not zero. Don't normalize if the amplitude is zero */
    /* as this will generate a nan. */
    if(amp_out[dcount] != 0.0) {
      qmix_out[dcount]=-mixed/amp_out[dcount];
    } else {
      qmix_out[dcount]=-mixed;
    }

    /* apply filter to input data array */
    phase_unfiltered = qmix_out[dcount];
   
    /* STATE MACHINE FOR CLOSING THE LOOP */

    /* adjust state and close loop if time exceeds starttime */
    if(pstate[9]<0.5) {
      plloopstate=loopopen;
      time = pstate[8];
      starttime=pstate[1];
      if(time>starttime) {
	pstate[9]=1.0;
	/* close the feedback loop */
	plloopstate=loopclosed;
      } else {
	plloopstate=loopopen;
	pstate[8]+=timeinc;
      }
    } else {
      plloopstate=loopclosed;
    }
    
    /* CALL THE LOOP CODE */

    /* implement feedback if the the loop is closed */
    if(plloopstate==loopclosed) {
      ddelta=phase_unfiltered;
      /* apply correction to state data */
      iwave_phaselock_alter_filters_limitf(ddelta, fstate);
      /* reset frequency */
      freq_out[dcount]=pstate[0];
    }

    /* LOOP CODE HAS BEEN CALLED BY HERE IF THE LOOP IS CLOSED */

    /* calculate amplitude by taking square root of amp_out */
    amp_out[dcount]=sqrt(amp_out[dcount]);
  }
  /* update memory buffers for iwave filter */
  fstate[11]=ynewr;
  fstate[12]=ynewi;

  return 0;
}

