function NE = NullEnergy(map,clust,flag)  
% NullEnergy : compute the null energy of a cluster
%
% DESCRIPTION : 
%   Make the auto power map of each detector. 
%   Define a box using tmin tmax fmin fmax of the cluster or using
%   the whole map. Put all the cluster's pixel to 0 and sum the
%   pixel in the box. Normalize this null energy by dividing with
%   the number of pixel.
%
% SYNTAX :
%   NE = NullEnergy (map,clust,flag)
% 
% INPUT : 
%    map   : struct of array containing map in particular the
%            naiP1&2 and the P1&2 map used for the Auto power
%            map computation 
%    clust : zeros array with 1 on the pixel's cluster
%    flag  : true if use of the cluster box definition false for
%            the whole map
%
% OUTPUT :
%    NE : struct
%          |- ifo1  
%          |  |- brut : non normalized Null energy for ifo1
%          |  |- norm : normalized null energy for ifo1
%          |
%          |- ifo2
%          |  |- brut : non normalized Null energy for ifo2
%          |  |- norm : normalized null energy for ifo2
%          | 
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Fev 2016
%

% define the box
if flag
  [r,c] = find(clust);
  tmin=map.segstarttime(min(c))-map.start;
  tmax=map.segstarttime(max(c))-map.start;
  fmin=map.f(min(r));
  fmax=map.f(max(r));
else
  tmin=min(map.segstarttime)-map.start;
  tmax=max(map.segstarttime)-map.start;
  fmin=min(map.f);
  fmax=max(map.f);
end

tt=map.segstarttime-map.start>=tmin&...
   map.segstarttime-map.start<=tmax;
ff=map.f>=fmin&map.f<=fmax;

% calculate AP for each det
AP1=map.naiP1./map.P1;
AP1(isnan(AP1)|isinf(AP1))=0;

AP2=map.naiP2./map.P2;
AP2(isnan(AP2)|isinf(AP2))=0;

% put all cluster pixel to 0
AP1(clust)=0;
AP2(clust)=0;

% return the null energy struct results
NE.ifo1.brut = sum(sum(AP1(ff,tt)));
NE.ifo1.norm = sum(sum(AP1(ff,tt)))/(sum(tt)*sum(ff));
NE.ifo2.brut = sum(sum(AP2(ff,tt)));
NE.ifo2.norm = sum(sum(AP2(ff,tt)))/(sum(tt)*sum(ff));

return

