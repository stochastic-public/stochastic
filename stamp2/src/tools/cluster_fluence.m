function F = cluster_fluence(map, Gamma, params, verbose)
% function F = cluster_fluence(map, Gamma, params, verbose)
% calculate the fluence from a y-map for cluster Gamma
% The cluster Gamma is an array of SNR values identical in size to map.y.
% Any value that is non-zero is therefore part of the cluster.
% E.g.: stamp_fluence(map, stoch_out.cluster.reconMax)
% To verbose=true in order to see gory detail of how overlapping segments are
% handled.
% Eric Thrane
%
% Note: there are two closely related scripts cal_fluence and cluster_fluence.
% The first calculates the fluence associated with a GW waveform described by 
% a time series.  The second calculates the fluence associated with a ft-map 
% cluster.

% Check if non-essential parameters have been set and use defaults if necessary
try
  params.c;
catch
  params.c = 3e8;
  params.G = 6.67e-11;
end
%
try
  verbose;
catch
  verbose=false;
end

% normfac function accounts for overlapping segments
calnormfac=@(x) ((x+1)/2)/x; 

% make an array of frequencies and an array of times
nsegs = size(map.y, 2);
fmap = repmat(map.f', 1, nsegs);
nfreqs = size(map.y, 1);
tmap = repmat(map.segstarttime, nfreqs, 1);

% make a fluence map (normalization factor handled below)
% See Eq. 7.4 in the stochastic units document:
%   sgwb/trunk/doc/TechNotes/T070045/multipole.pdf
% See also Eqs. 3.12-3.14 in the STAMP method paper.
Fmap = (params.c^3/(16*pi*params.G)) .* (2*pi)^2 * ...
  map.segDur .* fmap.^2 .* map.y;

% SI units of fluence are J/m^2; we convert to ergs/cm^2 
% conversion factor = 1e7 / 100^2 = 1e3
Fmap = 1000 * Fmap;

% find which frequency bins and segments are part of the sum
cut = Gamma~=0;
fcut = any(cut>0, 2);
sumfreqs = map.f(fcut);
tcut = any(cut>0, 1);
sumsegs = map.segstarttime(tcut);

% make maps of F, f and t for just the cluster
tmapcut = tmap(cut);
fmapcut = fmap(cut);
Fmapcut = Fmap(cut);

% initialize the running fluence total
F = 0;

% loop over frequency bins to calculate F
for ii=1:length(sumfreqs)
  % which times are part of the cluster and in this freq bin
  times = tmapcut(fmapcut==sumfreqs(ii));
  if verbose
    fprintf('%1.1f\n', times-times(1));
    fprintf('...\n%i times found in fbin=%i\n', length(times), ii);
  end
  % if there is only one segment in this frequency band, simply add the fluence
  % to the running total
  if length(times)==1
    F = F + Fmapcut(fmapcut==sumfreqs(ii));
    if verbose
      fprintf('  There is only one time.\n');
    end
  % if there is more than one segment, we must check for breaks
  else
    % location of breaks
    breaks = find(times(2:end) - times(1:end-1) ~= params.segmentDuration/2);
    % the number of breaks
    nbreaks = length(breaks);
    % if there are no breaks, the normalization is easy to calculate
    if nbreaks==0
      N = length(times);
      normfac = calnormfac(N);
      if verbose
        fprintf('    There are no breaks.\n');
        fprintf('    normfac=%1.3f\n', normfac);
      end
      F = F +  normfac * sum(Fmapcut(fmapcut==sumfreqs(ii)));
    % if there are breaks we must calculate the normalization for each 
    % continguous span (of which there are nbreaks+1)
    else
      if verbose
        fprintf('    There are %i breaks.\n', nbreaks);
      end
      % define fluence map for just 
      Fmapcuttwice = Fmapcut(fmapcut==sumfreqs(ii));
      % account for the first unbroken stretch
      N = breaks(1);
      normfac = calnormfac(N);
      if verbose
        fprintf('    The first stretch is from 1:%i.\n', breaks(1));
        fprintf('    normfac=%1.3f\n', normfac);
      end
      F = F +  normfac * sum(Fmapcuttwice(1:breaks(1)));
      % account for the final unbroken stretch
      N = length(times) - (breaks(end));
      normfac = calnormfac(N);
      if verbose
        fprintf('    The final stretch is from %i:%i.\n', breaks(end)+1, ...
          length(times));
        fprintf('    normfac=%1.3f\n', normfac);
      end
      F = F +  normfac * sum(Fmapcuttwice(breaks(end)+1:length(times)));
      % loop over additional breaks
      for jj=1:nbreaks-1
        N = breaks(jj+1)-breaks(jj);
        normfac = calnormfac(N);
        if verbose
          fprintf('      Additional stretch from %i:%i.\n', breaks(jj)+1, ...
            breaks(jj+1));
          fprintf('      normfac=%1.3f\n', normfac);
        end
        F = F +  normfac * sum(Fmapcuttwice(breaks(jj)+1:breaks(jj+1)));
       end
    end
  end
end

return
