function grand_stochtrack(parampath, job)
% function grand_stochsky(parampath, job)
% a convenient compile-once, run on many application wrapper for stochtrack

% keep track of time to finish
tic;

% condorize
%job = strassign(job);
job = str2num(job);

% intialize parameters
q = load(parampath);

% check if ofile is defined
if ~isfield(q.params, 'ofile')
  error('Must define params.ofile');
end

% call clustermap
stoch_out = clustermap(q.params, q.params.hstart, q.params.hstop);

% save results
save([q.params.ofile '_' num2str(job) '.mat']);

toc;

return
