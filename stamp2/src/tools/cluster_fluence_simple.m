function F = cluster_fluence_simple(map, Gamma, params)
% function F = cluster_fluence_simple(map, Gamma, params)
% calculate the fluence from a y-map for cluster Gamma
% The cluster Gamma is an array of SNR values identical in size to map.y,
%   any value that is non-zero will be part of the largest cluster.
% E.g.: cluster_fluence_simple(map, stoch_out.cluster.reconMax)
% Eric Thrane, Shivaraj
%
% Note: there are two closely related scripts cal_fluence and 
% cluster_fluence_simple. The first calculates the fluence associated with a 
% GW waveform described by a time series.  The second calculates the fluence 
% associated with a ft-map cluster.

% Check if non-essential parameters have been set and use defaults if necessary
try
  params.c;
catch
  params.c = 3e8;
  params.G = 6.67e-11;
end

% make an array of frequencies and an array of times
nsegs = size(map.y, 2);
fmap = repmat(map.f, 1, nsegs);
nfreqs = size(map.y, 1);
tmap = repmat(map.segstarttime, nfreqs, 1);

% make a fluence map (normalization factor handled below)
% See Eq. 7.4 in the stochastic units document:
%   sgwb/trunk/doc/TechNotes/T070045/multipole.pdf
% See also Eqs. 3.12-3.14 in the STAMP method paper.
df = map.f(2)-map.f(1);
Fmap = (pi*params.c^3/(4*params.G)) * fmap.^2 .* map.y * df;

% SI units of fluence are J/m^2; we convert to ergs/cm^2 
% conversion factor = 1e7 / 100^2 = 1e3
Fmap = 1000 * Fmap;

% find which frequency bins and segments are part of the sum
cut = Gamma~=0;
fcut = any(cut>0, 2);
sumfreqs = map.f(fcut);
tcut = any(cut>0, 1);
sumsegs = map.segstarttime(tcut);
Fmap(~cut)=0; % make the other pixels to zero

% make odd and even segments map
Fmap_odd = Fmap(:,1:2:end);
Fmap_even = Fmap(:,2:2:end);

% Calculate sum of even and odd segments and return the average
F_even = sum(Fmap_even(:));
F_odd = sum(Fmap_odd(:));
%fprintf('F-even %.2f, F-odd %.2f, F-mean %.2f \n', F_even, F_odd, mean([F_even F_odd]));
F = mean([F_even F_odd]);% fluence of the signal (ergs/cm^2)

return
