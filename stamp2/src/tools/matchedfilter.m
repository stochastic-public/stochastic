function SNR=matchedfilter(ht1, ht2, psd, sampleRate, flow, fhigh)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to compute the matchedfilter between 2 time series
% Inputs:
% - ht1 and ht2: 1D vectors containing the 2 time series
% - psd: 2D vector containing freq PSD(f) where PSD is a onesided PSD
% - sampleRate is the sampling rate of h(t)
% - flow and fhigh are the frequency boundaries of the SNR integral.
% The PSD must be defined over the full range [flow fhigh]
%
% Outputs: SNR
% 
% Contact: Marie Anne Bizouard (mabioua@lal.in2p3.fr) 2014
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Inputs
freq=psd(:,1);
psd=psd(:,2);
fmin=freq(1,1);
fmax=freq(end,1);

% Cross-check PSD and integral boundaries
if fmin>flow | fmax<fhigh
  old_values=[num2str(flow) ' ' num2str(fhigh)];
  flow=max(fmin,flow);
  fhigh=min(fmax,fhigh);
  warning(['The SNR integral will computed over [' ...
	   num2str(flow) ' ' num2str(fhigh) '] instead of [' ...	
	   old_values ']']);
end

% FFT 
h1_size=size(ht1,1);
h2_size=size(ht2,1);

fft_size=max(2^nextpow2(h1_size),2^nextpow2(h2_size)) ;
deltaF=sampleRate/fft_size;

% One sided FT
deltaT = 1/sampleRate;
hf1=fft(ht1,fft_size)*deltaT;
hf2=fft(ht2,fft_size)*deltaT;

% Interpolate the onesided PSD. We assume the PSD is a onesided PSD
% The interpolation is done for frequencies of the current PSD 
f = sampleRate./2*linspace(0,1,fft_size/2+1);
ext_freq=find(f>=fmin & f<=fmax);
f=f(ext_freq);
f=transpose(f);

onesided_psd=interp1(freq,psd,f);

% Fix NaN that may happen at the edges. If PSD(f1)==NaN, assign the
% value of the next frequency bin (assuming it is different from NaN)
x=isnan(onesided_psd);
ext=find(x==1);

if size(ext,1)>0
  ext2=ext+1;
  if ext(end)==size(onesided_psd,1)
    ext2(end)=size(onesided_psd,1)-1;
  end
  onesided_psd(ext)=onesided_psd(ext2);
  
  if size(find(isnan(onesided_psd)==1))>0
    display(['PSD is still containing NaNs after correction with next' ...
	     ' values. Please check the PSD'])
    SNR=NaN;
    return;
  end
end

% Compute the integral. First compute the ratio
ratio=4*real((hf1(ext_freq).*conj(hf2(ext_freq)))./onesided_psd);

% The integral is computed over the search frequency range
ext=find(f>=flow&f<=fhigh);

sum_total = sum(ratio(ext));

if sum_total < 0
   SNR = 0;
else
   SNR=sqrt(sum_total*deltaF);
end


