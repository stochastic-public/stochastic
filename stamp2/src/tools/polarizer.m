function [hp hx] = polarizer(Phit,iota,psi,h0)
% [hp hx] = polarizer(Phit,iota,psi,h0)
% Phit is a phase time series, iota and psi are polarization angles
% hp and hx are the plus and cross components of a toy model time series with
% polarization given by iota and psi.


% Defs from Eqs 38-39 of STAMP paper
Ap = (h0/2)*(1+cos(iota)^2);
Ax = h0*cos(iota);

% calculate hp and hx from Eq 41
hp = Ap*cos(2*psi)*cos(Phit) - Ax*sin(2*psi)*sin(Phit);
hx = Ap*sin(2*psi)*cos(Phit) + Ax*cos(2*psi)*sin(Phit);

return
