/* header file for line monitoring code */
/*                                      */
/* Ed Daw, 12th October 2007            */

/* value of pi taken from matlab */
#define PI 3.141592642489793
#define TRUE 1
#define FALSE 0

/* Structure containing line data */
typedef struct linedataset {
  /* The number of elements in the timeseries buffer array. */
  /* Equal to the length of the FFT to obtain the same Fourier */
  /* coefficients */
  unsigned int datasize;
  /* Pointer to the stored timeseries */
  double* tseries;
  /* sampling rate in Hz */
  int srate;
  /* The number of Fourier components to be monitored */
  unsigned int no_of_frequencies;
  /* An array of phase shifts, one for each frequency. */
  /* this is the phase shift per timesample of a sinewave */
  /* at the frequency of this Fourier component, in radians.*/
  double* cosphase;
  double* sinphase;
  /* A counter that increments by one for each time sample */
  /* written to the tseries array. Software will reset this */
  /* counter to zero once it reaches the value datasize */
  unsigned int dcounter; 
  /* Real and imaginary parts of the most recent Fourier */
  /* coefficient for each frequency. */
  double* real;
  double* imag;
  /* Bin offset from current bin to bin where correction applied. */
  /* This is basically half the timeseries length, but be careful where */
  /* the timeseries length is odd. */
  unsigned int correctionoffset;
  /* Real and imaginary parts of phase correction for offset from bin 0 of FFT */
  /* to bin where correction is applied */
  double* coslag;
  double* sinlag;
  /* Flag set to true when all fields are initialized */
  unsigned int initialized;
  /* Flag set by user to indicate that the number of iterations */
  /* has equalled or exceed the length of the timeseries data buffer*/
  unsigned int bufferfull;
} linedata;

/* Calculate the number of entries in the timeseries buffer necessary to */
/* monitor the line with the requested response time and sampling rate. */
unsigned int linewatch_gettsbuffersize(double sampling_rate, 
				       double responsetime);

/* initialize the line monitor                                              */
/* useage - freq: line frequency in Hz                                      */
/*          width: line width in Hz (make wider than line width or wander)  */
/*          sampling_rate: in Hz. Must be an integer.                       */
/*          responsetime: in seconds. Duration of data train used to        */
/*                        estimate Fourier coefficients is twice the lowest */
/*                        integer power of 2 above responsetime             */
int linewatch_constructor(linedata* ldata, double freq, double width,
			  double sampling_rate, double responsetime,
			  double* tseries_buffer);

/* clear the line monitor after final use */
int linewatch_destructor(linedata* ldata);

/* increment monitor by one timesample */
/* timesample is the newest sample from the data channel.    */
/* rp and ip are pointers to arrays of real and imaginary    */
/* parts of the Fourier coefficients. The returned number    */
/* is 2/(sum of real parts of Fourier coefficients). This    */
/* is what should actually be subtracted to remove the line. */
/* The pointer ptsbin is a pointer to the address in memory  */
/* where the timesample from which the correction should be  */
/* subtracted is stored.                                     */
double linewatch_increment(linedata* ldata, double timesample,
			   double* rp, double* ip, double** ptsbin);

/* update timeseries buffer. Use after linewatch_increment */
/* to overwrite the contents of the timeseries buffer with the */
/* latest time sample. This is an external function because */
/* where there is an array of lines monitors, each line monitor */
/* may be using the same ring buffer.*/
void linewatch_nextsample(linedata* ldata, double timesample);
