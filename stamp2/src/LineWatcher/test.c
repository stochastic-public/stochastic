/* program to test line removal library */
/* Ed Daw, 12th November 2007           */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <linewatch.h>

int main(void) {
  linedata ld;
  FILE* infile;
  FILE* outfile;
  double* indata;
  double* outdata;
  unsigned int filedone;
  int ndata;
  int datacount;
  double datadummy;
  double real;
  double imag;
  clock_t start,end;
  double elapsed;
  double fracrealtime;
  double* correctionelement;			      
  double* correction;
  double localcorrection;
  double* tseries_buffer;
  int inputdataavailable;

  printf("PI is %1.15f\n",(double)PI);

  /* allocate memory for timeseries buffer */
  printf("buffer size %u.\n",linewatch_gettsbuffersize(16384,3));
  tseries_buffer=calloc((size_t)linewatch_gettsbuffersize(16384,3),
			sizeof(double));
  linewatch_constructor(&ld,
			(double)50,
			(double)0.5,
			(double)16384,
			(double)3,
			tseries_buffer);

  /* open input data file or manufacture a sine wave data set */
  if((infile=fopen("testdata.txt","r"))==NULL) {
    printf("Input file textdata.txt unavailable; making own test data.\n");
    inputdataavailable = FALSE;
    ndata = 4194304;
  } else { 
    inputdataavailable = TRUE;
    /* find out how many data points in the file */
    filedone=FALSE; ndata=0;
    while(filedone==FALSE) {
      if(fscanf(infile,"%lf",&datadummy)==EOF) {
	filedone=TRUE;
      } else { ++ndata; }
    }
    fclose(infile);
    printf("%d data points in file.\n",ndata);
  }
  
  /* allocate memory for data */
  indata=calloc(ndata,sizeof(double));
  outdata=calloc(ndata,sizeof(double));
  correction=calloc(ndata,sizeof(double));
  /* read or synthesize data */
  if(inputdataavailable == TRUE) {
    infile=fopen("testdata.txt","r");
    for(datacount=0; datacount<ndata; ++datacount) {
      fscanf(infile,"%lf\n",indata+datacount);
    }
    fclose(infile);
  } else {
    for(datacount=0; datacount<ndata; ++datacount) {
      indata[datacount]=10*cos(2*(double)PI*57.2*(double)datacount/(double)16384);
    }
  }
  
  /* run FFT filter on input data */
  printf("Running filter now.\n");
  start=clock();
  for(datacount=0; datacount<ndata; ++datacount) {
    /*    printf("point %d:\n",datacount); */
    correction[datacount]=linewatch_increment(&ld,indata[datacount],
					      &real,&imag,&correctionelement);
    outdata[datacount]=(*correctionelement) - correction[datacount];
    linewatch_nextsample(&ld,indata[datacount]);
  }
  end=clock();
  elapsed=((double)(end-start))/CLOCKS_PER_SEC;
  printf("Finished running filter.\n");
  printf("Time elapsed in processing %d * %d = %d timesamples:\n",
	 ndata,ld.no_of_frequencies,ndata*ld.no_of_frequencies);
  printf("%.3le CPU seconds.\n",elapsed);
  fracrealtime=(elapsed/ld.no_of_frequencies/(ndata/ld.srate));
  printf("Time per second per channel is %e.\n",fracrealtime);	 
  printf("On this computer, %f channels can be processed in real time.\n",
	 1/fracrealtime);

  /* open output file if the input data was from a file */
  if(inputdataavailable == TRUE) {
    outfile=fopen("result.txt","w");
    for(datacount=0; datacount<ndata; ++datacount) {
      fprintf(outfile,"%le\t%le\t%le\n",indata[datacount],correction[datacount],
	      outdata[datacount]);
    }
    fclose(outfile);
  }

  /* clean up after test */
  linewatch_destructor(&ld);
  free(indata);
  free(outdata);
  free(correction);
  free(tseries_buffer);
  return 0;
}
