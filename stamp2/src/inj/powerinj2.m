function [map_out, inj] = powerinj2(params, map, tt, inj)
% function [map_out, inj] = powerinj2(params, map, tt, inj)
% E. Thrane: adds injection to the cross- and auto- power maps
%   v2 is cleaner and more like loadmats.m
%   It also checks to see if the injection file has already been loaded to save
%   time.  It does not apply random phases to calculate n(f)
%
% params: the parameter struct
% map: the struct containing the ft-maps
% tt: an integer that keeps track of the number of injection strengths that
%   have been performed so far
% inj: a struct that include an ft-map of the injection, which is passed back
%   and forth to avoid having to re-read an injection file that has already
%   been processed
% map_out is the map struct with the injection added

% the default is to not add any power to the map
if ~params.powerinj
  map_out=map;
  return;
end

% if this is the first injection, load in and back up the injection file
if tt==1
  fprintf('loading power injection...');
  % load injection maps from file
  M = load(params.injfile);
  fprintf('done.\n');
  % remove times associated with no injection if this has not already been done
  try
    M.map.extractinj;
  catch
    M.map = extractinj(M.map);
  end
  % store injection to avoid reloading data
  inj = M;
else 
  % use pre-loaded injection
  M = inj;
end

% check that the mat files include the requested band
if params.fmin < M.params.fmin | params.fmax > M.params.fmax
  error('Injection: requested frequencies out of range of mat files.');
end

% compare metadata with existing map
if params.deltaF ~= M.params.deltaF | ...
  params.segmentDuration ~= M.params.segmentDuration | ...
  params.ifo1 ~= M.params.ifo1 | params.ifo2 ~= M.params.ifo2
  error('Injection: metadata mismatch.');
end

% create injection times borrowing from map
N = length(M.map.segstarttime);
% aa is the delay between map start and the injection as measured in bins
if params.fixedInjectionTime
  if params.fixedInjectionTimeGPS >= 0
     [column, column_index] = ...
        min(abs(map.segstarttime - params.fixedInjectionTimeGPS));
     % print to screen if the injection time does not match up with a segment
     % start time included in the map
     if min(abs(map.segstarttime - params.fixedInjectionTimeGPS))>0
       fprintf('rounding = %1.1e\n', ...
         min(abs(map.segstarttime - params.fixedInjectionTimeGPS)));
     end
%     aa = column_index - 1 - (params.numSegmentsPerInterval - 1) - ...
%       (params.bufferSecs1 / (params.segmentDuration / 2) + 1);
     aa = column_index - 1;
  else
    % if the GPS time is negative begin injection in first bin
    aa = 0;
  end
else
  A = length(map.segstarttime) - length(M.map.segstarttime);
  aa = round(rand*A);
end
try
  M.map.segstarttime = map.segstarttime(1+aa:N+aa);
catch
  error('error in powerinj: injection duration > map duration');
end

% find relevant frequency indices from two maps
[C, new_f, old_f] = intersect(map.f, M.map.f);

% find relevant time indices from new and old ft-maps
[C, new_t, old_t] = intersect(map.segstarttime, M.map.segstarttime);

% loop over values of alpha.  alpha is the scale factor
% Notes:  power ~ h^2 ~ alpha        h ~ sqrt(alpha)
%         D ~ 1/h ~ 1/sqrt(alpha)    alpha ~ 1/D^2
zz = mod(tt-1, params.alpha_n)+1;
alpha = params.alpha(zz);
fprintf('  zz=%1.0f, alpha=%1.3e\n', zz, alpha);

% The injection was processed at one sky location and time, and now we are
% going to analyze it at another time and sky location.  We must therefore
% rescale the powers.
% cross-power
M.map.y(old_f, old_t) = alpha * M.map.y(old_f, old_t) .* ...
  abs((map.eps_12(new_f, new_t)./M.map.eps_12(old_f, old_t)));
% auto-powers
M.map.P1(old_f, old_t) = alpha * ...
  abs((map.eps_11(new_f, new_t)./M.map.eps_11(old_f, old_t))) .* ...
  M.map.P1(old_f, old_t);
M.map.P2(old_f, old_t) = alpha * ...
  abs((map.eps_22(new_f, new_t)./M.map.eps_22(old_f, old_t))) .* ...
  M.map.P2(old_f, old_t);
% naive auto-powers
M.map.naiP1(old_f, old_t) = alpha * ...
  abs((map.eps_11(new_f, new_t)./M.map.eps_11(old_f, old_t))) .* ...
  M.map.naiP1(old_f, old_t);
M.map.naiP2(old_f, old_t) = alpha * ...
  abs((map.eps_22(new_f, new_t)./M.map.eps_22(old_f, old_t))) .* ...
  M.map.naiP2(old_f, old_t);

% prepare output map by copying over data from starting map
map_out = map;

% add injections to maps
map_out.y(new_f, new_t) = map.y(new_f, new_t) + M.map.y(old_f, old_t);
map_out.P1(new_f, new_t) = map.P1(new_f, new_t) + M.map.P1(old_f, old_t);
map_out.P2(new_f, new_t) = map.P2(new_f, new_t) + M.map.P2(old_f, old_t);
map_out.naiP1(new_f, new_t) = ...
  map.naiP1(new_f, new_t) + M.map.naiP1(old_f, old_t);
map_out.naiP2(new_f, new_t) = ...
  map.naiP2(new_f, new_t) + M.map.naiP2(old_f, old_t);
map_out.sigma = sqrt(map_out.P1.*map_out.P2) .* ...
  (map.sigma ./ sqrt(map.P1.*map.P2));
map_out.snr = map_out.y./map_out.sigma;
fprintf('  injection added to data\n');

return
