function [map_out, inj] = powerinj3(params, map, source, tt, inj)
% function [map_out, inj] = powerinj3(params, map, tt, inj)
% E. Thrane: adds injection to the cross- and auto- power maps
%   v3 includes fluctuation terms and it handles the averages over neighboring
%   segments correctly.  NOTE: several buffer segments are required in order
%   to do the averaging calculation and as of June 5, 2012, there is no check
%   in place that this is done correctly.
%   
%   v2 is cleaner and more like loadmats.m
%   It also checks to see if the injection file has already been loaded to save
%   time.  It does not apply random phases to calculate n(f)
%
% params: the parameter struct
% map: the struct containing the ft-maps
% tt: an integer that keeps track of the number of injection strengths that
%   have been performed so far
% inj: a struct that include an ft-map of the injection, which is passed back
%   and forth to avoid having to re-read an injection file that has already
%   been processed
% map_out is the map struct with the injection added

% if this is the first injection, load in and back up the injection file
if tt==1
  fprintf('loading power injection...');
  % load injection maps from file
  M = load(params.injfile);
  fprintf('done.\n');
  % remove times associated with no injection if this has not already been done
  try
    M.map.extractinj;
  catch
    M.map = extractinj(M.map);
  end

  % store injection to avoid reloading data
  inj = M;
else 
  % use pre-loaded injection
  M = inj;
end

% check that the mat files include the requested band
if params.fmin < M.params.fmin | params.fmax > M.params.fmax
  error('Injection: requested frequencies out of range of mat files.');
end

% compare metadata with existing map
if params.deltaF ~= M.params.deltaF | ...
  params.segmentDuration ~= M.params.segmentDuration | ...
  params.ifo1 ~= M.params.ifo1 | params.ifo2 ~= M.params.ifo2
  error('Injection: metadata mismatch.');
end

% create injection times borrowing from map
N = length(M.map.segstarttime);
% aa is the delay between map start and the injection as measured in bins
if params.fixedInjectionTime
  if params.fixedInjectionTimeGPS >= 0
     [column, column_index] = ...
        min(abs(map.segstarttime - params.fixedInjectionTimeGPS));
     % print to screen if the injection time does not match up with a segment
     % start time included in the map
     if min(abs(map.segstarttime - params.fixedInjectionTimeGPS))>0
       fprintf('rounding = %1.1e\n', ...
         min(abs(map.segstarttime - params.fixedInjectionTimeGPS)));
     end
     aa = column_index - 1;
  else
    % if the GPS time is negative begin injection in first bin
    aa = 0;
  end
else
  % calculate the number of segments that could be the injection start time
  % taking into account overlapping segments used to calculate avg PSDs
  if params.doOverlap
    A = length(map.segstarttime) - length(M.map.segstarttime) - ...
      2*(params.numSegmentsPerInterval-1);
    aa = round((params.numSegmentsPerInterval-1) + rand*A);
  else
    fprintf('Warning: non-overlapping segments power injections in beta.\n');
    A = length(map.segstarttime) - length(M.map.segstarttime) - ...
      (params.numSegmentsPerInterval-1);
    aa = round((params.numSegmentsPerInterval-1)/2 + rand*A);
  end
end
try
  M.map.segstarttime = map.segstarttime(1+aa:N+aa);
catch
  error('error in powerinj: injection duration > map duration');
end

% find relevant frequency indices from two maps
[C, new_f, old_f] = intersect(map.f, M.map.f);

% find relevant time indices from new and old ft-maps
[C, new_t, old_t] = intersect(map.segstarttime, M.map.segstarttime);

% loop over values of alpha.  alpha is the scale factor
% Notes:  power ~ h^2 ~ alpha        h ~ sqrt(alpha)
%         D ~ 1/h ~ 1/sqrt(alpha)    alpha ~ 1/D^2
zz = mod(tt-1, params.alpha_n)+1;
alpha = params.alpha(zz);
fprintf('  zz=%1.0f, alpha=%1.3e\n', zz, alpha);

% Rescale auto-power and cross-power based on desired source sky
% location and search sky location.  Also rescale based on alpha.
M.map = adjust_inj(M.map, map, params, source, alpha);

% prepare output map by copying over data from starting map
map_out = map;

% simulate fluctuation terms using naive PSDs
q = size(M.map.naiP1(old_f, old_t));
% assign random phases to products of ffts
% why: the sum of two uniformly distributed variables is not a uniformly 
% distributed random variable; it becomes slightly Gaussian!
h1 = sqrt(M.map.naiP1(old_f, old_t));
h2 = sqrt(M.map.naiP2(old_f, old_t));
n1 = sqrt(map.naiP1(new_f, new_t));
n2 = sqrt(map.naiP2(new_f, new_t));
% simulate random phases
if params.doOverlap
  % the phases of overlapping segments are correlated and this subtlety is 
  % handled witha simulation by randphases
  phi11 = randphases(q);
  phi22 = randphases(q);
  phi12 = randphases(q);
  phi21 = randphases(q);
else
  % non-overlapping segments yield uncorrelated phases
  phi11 = 2*pi*rand(q);
  phi22 = 2*pi*rand(q);
  phi12 = 2*pi*rand(q);
  phi21 = 2*pi*rand(q);
  fprintf('powerinj: non-overlapping segments\n');
end
% calculate fluctuation terms for cross-power including antenna factor and
% segmentDuration
fluctuations = h1.*n2.*cos(phi12) + h2.*n1.*cos(phi21);
fluctuations = fluctuations ./ abs((map.eps_12(new_f, new_t)));
fluctuations = fluctuations * params.segmentDuration;
% calculate fluctuation terms for auto-power terms
naiN1 = 2*h1.*n1.*cos(phi11);
naiN2 = 2*h2.*n2.*cos(phi22);

% add injections to maps
map_out.y(new_f, new_t) = map.y(new_f, new_t) + M.map.y(old_f, old_t);
map_out.naiP1(new_f, new_t) = ...
  map.naiP1(new_f, new_t) + M.map.naiP1(old_f, old_t);
map_out.naiP2(new_f, new_t) = ...
  map.naiP2(new_f, new_t) + M.map.naiP2(old_f, old_t);
% add fluctuation terms
map_out.y(new_f, new_t) = map_out.y(new_f, new_t) + fluctuations;
map_out.naiP1(new_f, new_t) = map_out.naiP1(new_f, new_t) + naiN1;
map_out.naiP2(new_f, new_t) = map_out.naiP2(new_f, new_t) + naiN2;
% add non-naive fluctuation terms
% If the PSDs are simulated directly, there is too much variability in them.
map.P1x = map.P1;
map.P2x = map.P2;
w = (params.numSegmentsPerInterval-1);
if params.doOverlap
  widx = [-w:2:-2 2:2:w];
else
  fprintf('powerinj: non-overlapping segments\n');
  widx = [-w/2:-1 1:w/2];
end
map.P1x(new_f, new_t) = 0;
map.P2x(new_f, new_t) = 0;
for ii=widx
  map.P1x(new_f, new_t)=map.P1x(new_f, new_t) + map_out.naiP1(new_f, new_t+ii);
  map.P2x(new_f, new_t)=map.P2x(new_f, new_t) + map_out.naiP2(new_f, new_t+ii);
end
map.P1x(new_f, new_t)=map.P1x(new_f, new_t)/(params.numSegmentsPerInterval-1);
map.P2x(new_f, new_t)=map.P2x(new_f, new_t)/(params.numSegmentsPerInterval-1);
map_out.P1 = map.P1x;
map_out.P2 = map.P2x;
%
map_out.sigma = sqrt(map_out.P1.*map_out.P2) .* ...
  (map.sigma ./ sqrt(map.P1.*map.P2));
map_out.snr = map_out.y./map_out.sigma;
fprintf('  injection added to data\n');

return
