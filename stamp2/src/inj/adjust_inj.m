function inj_out = adjust_inj(inj_map, map, params, source, alpha)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function inj_out = adjust_inj(inj_map, map, params, source, alpha)
% Adjusts a power injection so that it has a different source location than
% the one it was preprocessed with.
%
% INPUTS
% inj_map - input injection map struct.
% map     - map struct that the injection will be added to.
% params  - STAMP parameters.
% source  - search sky position.
% alpha   - injection scaling factor.
%
% OUTPUTS
% inj_out = injection map struct
%
% Contact: T. Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set inj_out to inj_map, specific maps will be modified below.
inj_out = inj_map;

% Determine times and frequencies for injection insertion.
[C, new_f, old_f] = intersect(map.f, inj_map.f);
[C, new_t, old_t] = intersect(map.segstarttime, inj_map.segstarttime);

if params.fixedInjectionLocation
  % Get mid-segment GPS times for insertion and from original injection file.
  midGPSTimes = map.segstarttime(new_t) + params.segmentDuration / 2;
  
  % Get detector information.
  [det1 det2] = ifo2dets(params);
  
  % Calculate time delay and antenna factors desired injection sky location and
  % for default injection sky location (from the injection .mat file).
  g_inj = calF(det1, det2, midGPSTimes, [params.inj_ra params.inj_dec], params);
  g_search = calF(det1, det2, midGPSTimes, source, params);
  
  % Calculate detector efficiencies based on filter type.
  if (params.doPolar || params.purePlus || params.pureCross)
    [g_inj, eps_12, eps_11, eps_22] = polarReadout(params, g_inj);
  else
    g_inj.gamma0 = (g_inj.F1p .* g_inj.F2p + g_inj.F1x .* g_inj.F2x)/2;
    eps_12_fixed = g_inj.gamma0;
    eps_11_fixed = (g_inj.F1p .* g_inj.F1p + g_inj.F1x .* g_inj.F1x)/2;
    eps_22_fixed = (g_inj.F2p .* g_inj.F2p + g_inj.F2x .* g_inj.F2x)/2;
  end
  
  % Resize efficiencies, freqs., time delays.
  eps_11_fixed = repmat(eps_11_fixed, size(inj_map.y,1), 1);
  eps_12_fixed = repmat(eps_12_fixed, size(inj_map.y,1), 1);
  eps_22_fixed = repmat(eps_22_fixed, size(inj_map.y,1), 1);
  f = repmat(inj_map.f(old_f)', 1, size(inj_map.segstarttime(old_t),2));
  tau_i = repmat(g_inj.tau, size(inj_map.f(old_f)',1), 1);
  tau_s = repmat(g_search.tau, size(inj_map.f(old_f)',1), 1);

  % Rescale cross-power and auto-powers.
  inj_out.naiP1(old_f, old_t) = alpha * ...
      abs((eps_11_fixed(old_f, old_t) ./ inj_map.eps_11(old_f, old_t))) .* ...
      inj_map.naiP1(old_f, old_t);
  inj_out.naiP2(old_f, old_t) = alpha * ...
      abs((eps_22_fixed(old_f, old_t) ./ inj_map.eps_22(old_f, old_t))) .* ...
      inj_map.naiP2(old_f, old_t);
  inj_out.y(old_f, old_t) = alpha * ...
      real( (inj_map.y(old_f, old_t) - i*inj_map.z(old_f, old_t)) .* ...
	    exp(2*pi*i*f .* (tau_s-tau_i))) ./ ...
      (map.eps_12(new_f, new_t) ./ eps_12_fixed(old_f, old_t));
  % NOTE: We are unsure why y - i*z gives the correct result instead of y +
  % i*z when re-scaling the y-map.  Correct = matches time-series
  % injection. This is under investigation. One possibility is that there may
  % be errors in the time-series injection code and in the filter code.
  % T. Prestegard 7/17/2012
else
  inj_out.naiP1(old_f, old_t) = alpha * ...
      abs((map.eps_11(new_f, new_t) ./ inj_map.eps_11(old_f, old_t))) .* ...
      inj_map.naiP1(old_f, old_t);
  inj_out.naiP2(old_f, old_t) = alpha * ...
      abs((map.eps_22(new_f, new_t) ./ inj_map.eps_22(old_f, old_t))) .* ...
      inj_map.naiP2(old_f, old_t);
  inj_out.y(old_f, old_t) = alpha * inj_map.y(old_f,old_t);
end

return;