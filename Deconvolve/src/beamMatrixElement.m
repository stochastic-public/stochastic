function beamElement = beamMatrixElement (pixelRow, pixelCol, pixOmega, ...
    deltaTime, freqBins, fSpectrum, Gamma, detSep)

% Calculate an UNNORMALIZED element of the beam matrix
%
% beamElement = beamMatrixElement ...
%  (pixelRow, pixelCol, pixOmega, freqBins, fSpectrum, Gamma, detSep)
%
% pixelRow    Integer. Row pixel number of beam matrix
% pixelCol    Integer. Column pixel number of beam matrix
% pixOmega    Real matrix. Direction cosines of the pixels
%             Index 1: pixel number, Index 2: axis index
% deltaTime   Real. Width of time bins.
% freqBins    Real vector. The frequency bin positions
% fSpectrum   Real matrix. Combination of the source & detector PSDs 
%             [GTilde(t,f) in text]. Index-1: Time, Index-2: frequency
% Gamma       Real matrix. Combination of antenna patterns for all directions
%             and sideral times. Index 1: pixel, Index 2: sidereal time
%             ***CAUTION: Gamma is NOT the full overlap reduction function***
% detSep      Real matrix. Detector separation vector at all sideral times
%             Index 1: Vector component, Index 2: sidereal time
%
% beamElement Real. Element of the beam matrix

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


velLight = 2.99792458e10;

deltaFreq = freqBins(2) - freqBins(1);

DeltaOmega = pixOmega(:,pixelCol) - pixOmega(:,pixelRow);


% Ignoring df - the frequency bin size
% should be cancelled by the normalization constant
fSum = 2.0 * deltaFreq * sum ...
   ((cos(2.0*pi*((detSep'*DeltaOmega)*freqBins)/velLight) .* fSpectrum),2);
tSum = deltaTime * (Gamma(pixelCol,:) .* Gamma(pixelRow,:)) * fSum;

beamElement = tSum;


return
