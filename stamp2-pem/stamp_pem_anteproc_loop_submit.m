function stamp_pem_anteproc_loop_submit(paramsFile, channlesListFile, startGPS, endGPS)
% function stamp_pem_anteproc_loop_submit(pemParamsFile, channlesListFile, startGPS, endGPS)
% Given a STAMP-PEM parameter file, channelsListfile, start and
% end GPS times this code creates and submits a condor dag file that
% wraps 10 stamp-pem jobs into one condor job and runs it on the cluster
%
% This routine routine writes dags that use stamp_pem_condor_wrapper.m, which loops
% over several jobs, calling anteproc for each job.  
% 
% ---------------------------------------------
% paramsFile : sets stamp parameters wanted for analysis
% channlesListFile : list of channels to be done for analysis (DARM channel
% 		     is the first one listed. All others will calculate
%		     coherence with the first channel in file.
% startGPS : start GPS time for run
% endGPS : end GPS time for run
%
% The output of this code could be used in conjuction with other stamp codes
% (such as various clustering algorithms) to further analyse the data
% Routine written by Michael Coughlin and Shivaraj Kandhasamy and Patrick meyers.
%
%

% Contact: patrick.meyers@ligo.org,michael.coughlin@ligo.org, shivaraj.kandhasamy@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% To use it as a compiled code
startGPS = strassign(startGPS);
endGPS = strassign(endGPS);

%set threshhold for interesting coherence information
%[gpsStart,gpsEnd]=stamp_pem_seg_check(startGPS, endGPS);

% Extract parameters from input parameter file
params = readParamsFromFilePEM(paramsFile);

% Read in channels list
if exist(channlesListFile)
  [channelNames, channelSampleRates] = ...
      textread(channlesListFile,'%s %f');
else
  error ('No channels list provided \n');
end

% Number of completed channels
completedChannels = 0;

% Number of channels for which completion is desired; It could be less than
% the number of channesl in the channels list file; Curretly we use all
% the channels in the given file
desiredChannels = length(channelNames);

% Indexes of completed channels
whichChannels = [];

% Decide whether to run on condor or not
% create master output directory
params.outputFilePrefix = strrep(params.outputFilePrefix,'usr1','home');
outDir = [params.outputFilePrefix '/' num2str(startGPS) '_' num2str(endGPS) '/' strrep(channelNames{1},':','-')];
if exist(outDir)~=7
  mkdir ([outDir]);
end
numCjobs = round(ceil(length(channelNames)/10));
   dagFilename = ['STAMP-PEM' '_' num2str(startGPS) '_' num2str(endGPS) '.dag'];
   fprintf('Creating dag file %s ...', dagFilename);
   fid = fopen(dagFilename,'w');
        if exist(['logfiles/'])~=7
          mkdir (['logfiles/']);
        end
      % print first line
for ii = 1:numCjobs+1 % self coherence is also calculated
      fprintf(fid,'JOB %d %s\n',ii,'anteproc_loop.sub');
      % print second line
      temp = ['VARS ' num2str(ii) ' jobNumber="' num2str(ii) '" paramsFile="' paramsFile '" channelListFile="' channlesListFile '" startGPS="' num2str(startGPS) '" endGPS="' num2str(endGPS) '" number="' num2str(ii-1) '"'];
      fprintf(fid,'%s\n\n',temp);
end
%  fprintf(fid,'JOB %d %s\n',numCjobs+1,'clean_up_pem_condor.sub');
%  temp = ['VARS ' num2str(numCjobs+1) ' jobNumber="' num2str(numCjobs+1) '" paramsFile="' paramsFile '" channelListFile="' channlesListFile  '" startGPS="' num2str(startGPS) '" endGPS="' num2str(endGPS) '"'];
%  fprintf(fid,'%s\n\n',temp);
  temp2 = ['PARENT 1 CHILD ' num2str(2:numCjobs+1)];
  fprintf(fid, temp2);
  fclose(fid);

  [exeNAvailable, msg] = system(['ls stamp_pem_anteproc_loop']);
  if exeNAvailable
    fprintf('Exe file stamp_pem_anteproc_loop not available, creating it ... \n');
    mcc -R -nodisplay -m -R -singleCompThread stamp_pem_anteproc_loop;
    fprintf(' Done.\n');
  end

%  [exeNAvailable, msg] = system(['ls clean_up_pem_condor']);
%  if exeNAvailable
%    fprintf('Exe file clean_up_pem_condor not available, creating it ... \n');
%    mcc -R -nodisplay -m -R -singleCompThread clean_up_pem_condor;
%    fprintf(' Done.\n');
%  end

  fprintf('Submitting dag file to condor. \n');
  [nouse1, nouse2] = system(['condor_submit_dag -maxjobs 50 ' dagFilename]);
  % estimated wait time; 60 sec data for one channel takes ~30 sec; 180 misc
  % overhead time
  est_time = (endGPS-startGPS)*30/60*max(desiredChannels/100,1)+180;
  fprintf('Results will be available in ~%d sec. If not check the status of the dag file ''%s'' \n', round(est_time), dagFilename);
  fprintf('Results will be available at ''%s'' .\n',[params.outputFilePrefix '/' num2str(startGPS) '_' num2str(endGPS) '/' strrep(channelNames{1},':','-') '/summary.html']);

return;
