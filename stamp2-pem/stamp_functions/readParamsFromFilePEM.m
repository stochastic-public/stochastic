function params = readParamsFromFilePEM(paramsFile)
%
%  readParamsFromFile --- read in search parameters from a file
%
%  readParamsFromFile(paramsFile) reads in search parameters from a
%  file, returning the parameters in a structure.
%
%  Assumes parameters are given by name/value pair.
%
%  Routine adopted from STAMP 
% If you add additional parameters, please add them at the end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% seed random number generator 
rand('state',sum(clock));

%% read in name/value pairs
[names,values] = ...
  textread(paramsFile, '%s %s', -1, 'commentstyle', 'matlab');

%% check that number of names and values are equal
if length(names)~=length(values)
  error('invalid parameter file');
end

%% loop over parameter names, assigning values to structure
for ii=1:length(names)

  switch names{ii}
    case 'doShift1'
      params.doShift1 = str2num(values{ii});

    case 'doShift2'
      params.doShift2 = str2num(values{ii});

    case 'doBadGPSTimes'
      params.doBadGPSTimes = str2num(values{ii});

    case 'doOverlap'
      params.doOverlap = str2num(values{ii});

    case 'ifo1'
      params.ifo1 = values{ii};

    case 'ifo2'
      params.ifo2 = values{ii};

    case 'segmentDuration'
      params.segmentDuration = str2num(values{ii});

    case 'numSegmentsPerInterval'
      params.numSegmentsPerInterval = str2num(values{ii});

    case 'ignoreMidSegment'
      params.ignoreMidSegment = str2num(values{ii});

    case 'deltaF'
      params.deltaF = str2num(values{ii});

    case 'flow'
      params.flow = str2num(values{ii});

    case 'fhigh'
      params.fhigh = str2num(values{ii});

    case 'fRef'
      params.fRef = str2num(values{ii});

    case 'alphaExp'
      params.alphaExp = str2num(values{ii});

    case 'ShiftTime1'
      params.ShiftTime1 = str2num(values{ii});

    case 'ShiftTime2'
      params.ShiftTime2 = str2num(values{ii});

    case 'doSidereal'
      params.doSidereal = str2num(values{ii});

    case 'minDataLoadLength'
      params.minDataLoadLength = str2num(values{ii});

    case 'resampleRate1'
      params.resampleRate1 = str2num(values{ii});

    case 'resampleRate2'
      params.resampleRate2 = str2num(values{ii});

    case 'bufferSecs1'
      params.bufferSecs1 = str2num(values{ii});

    case 'bufferSecs2'
      params.bufferSecs2 = str2num(values{ii});

    case 'hannDuration1'
      params.hannDuration1 = str2num(values{ii});

    case 'hannDuration2'
      params.hannDuration2 = str2num(values{ii});

    case 'nResample1'
      params.nResample1 = str2num(values{ii});

    case 'nResample2'
      params.nResample2 = str2num(values{ii});

    case 'betaParam1'
      params.betaParam1 = str2num(values{ii});

    case 'betaParam2'
      params.betaParam2 = str2num(values{ii});

    case 'doHighPass1'
      params.doHighPass1 = str2num(values{ii});

    case 'doHighPass2'
      params.doHighPass2 = str2num(values{ii});

    case 'highPassFreq1'
      params.highPassFreq1 = str2num(values{ii});

    case 'highPassFreq2'
      params.highPassFreq2 = str2num(values{ii});

    case 'highPassOrder1'
      params.highPassOrder1 = str2num(values{ii});

    case 'highPassOrder2'
      params.highPassOrder2 = str2num(values{ii});

    case 'freqsToRemove'
      params.freqsToRemove = transpose(str2num(values{ii}));

    case 'nBinsToRemove'
      params.nBinsToRemove = transpose(str2num(values{ii}));

    case 'badGPSTimesFile'
      params.badGPSTimesFile = values{ii};

    case 'doFreqMask'
      params.doFreqMask = str2num(values{ii});

    case 'mapsize'
      params.mapsize = str2num(values{ii});

    % used in preproc for STAMP studies
    case 'doDetectorNoiseSim'
      params.doDetectorNoiseSim = str2num(values{ii});

    case 'DetectorNoiseFile'
      params.DetectorNoiseFile = values{ii};
  
    case 'sampleRate'
      params.sampleRate = str2num(values{ii});

    % more params for stochmap addition
    case 'stochmap'
      params.stochmap = str2num(values{ii});

    case 'startGPS'
      params.startGPS = str2num(values{ii});

    case 'endGPS'
      params.endGPS = str2num(values{ii});

    case 'saveMat' % for storing mat files 
      params.saveMat = str2num(values{ii});

    case 'outputfiledir'
      params.outputfiledir = values{ii};

    case 'outputfilename'
      params.outputfilename = values{ii};

    % new cache file method
    case 'cacheFile'
      params.cacheFile = values{ii};

    case 'publicPath'
      params.publicPath = values{ii};

    case 'outputFilePrefix'
      params.outputFilePrefix = values{ii};

    case 'runCondor'
      params.runCondor = str2num(values{ii});

    % make master coherence matrix in log frequency
    case 'cohMatrixdoLog'
      params.cohMatrixdoLog = str2num(values{ii});
 
    case 'subsystem'
      params.subsystem = strsplit(values{ii},',');

    case 'freqsPerBand'
      params.freqsPerBand = str2num(values{ii});

    case 'cohThreshold'
      params.cohThreshold = str2num(values{ii});

    case 'doMaskMap'
      params.doMaskMap = str2num(values{ii});
    
    case 'StampFreqsToRemove'
      params.StampFreqsToRemove = transpose(str2num(values{ii}));
	
    case 'doPlots'
      params.doPlots = str2num(values{ii});

    otherwise
    % do nothing 

    end % switch

end %% loop over parameter names

% Default values for some of the parameters used in the analysis; Generally
% used for regualr STAMP analyses.
% Some constants
params.c = 299792458; %speed of light in m/s
params.savePlots = 1; % save the final plots
params.stochmap = 1; % 1 - Run STAMP; 0 would produce raw frames, useful for
                     % simulations (not recommended at this point) 

%% Misc parameters; For detchar we don't have worry about them. %%%%%%%%%%%%
% Parameters carried over from GW signal analysis
if ~isfield(params,'alphaExp')
  params.alphaExp = 0;
end

if ~isfield(params,'fRef')
  params.fRef = (params.fhigh + params.flow)/2;
end

% Testind and debugging related
params.diagnostic = 0;
params.fixAntennaFactors = 0;
params.bestAntennaFactors = 0;
params.NoTimeDelay = 0;

% Various cuts
params.glitch.doCut = 0;
params.doFreqMask = 0;
params.freqsToRemove = [];
params.nBinsToRemove = [];

% injection related
params.doPolar = 0;
params.purePlus = 0;
params.pureCross = 0;
params.doPolarInjection = 0;
params.alternative_sigma = 0;
params.glitchinj = 0;

% Clustering and parameter estimation
params.doStochtrack = 0;
params.doRadiometer = 0;
params.doBurstegard = 0;
params.doBoxSearch = 0;
params.doClusterSearch = 0;
params.doPE = 0;

% Other plots and about previous data availabilty
params.returnMap = 0;
params.doBicoherence = 0;
params.doFrequencyBins = 0;
params.matavailable = 0;
params.storemats = 0;

return;
