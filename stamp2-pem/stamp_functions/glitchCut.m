function [map, cutcols, detcuts] = glitchCut(params, map)
% function [map, cutcols] = glitchCut(params, map)
% Tanner Prestegard, Michael Coughlin, Shivaraj Kandhasamy, Eric Thrane
% cutcols gives the indices of the cut columns
% For additional documentation see
% https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?docid=71680

% if glitchCut==false then do nothing
if (params.glitch.doCut==false)
  cutcols = [];
  detcuts = [];
  return;
end

% call the Xi cut
[cut_col det_cut] = Xi_cut(map, params);

% record the index number for the cut columns
cutcols = find(cut_col==1); 

% record the detector for each glitch
r = find(det_cut > 0);
detcuts = det_cut(r);

% fill cut columns with NaNs
map.snr(:,cut_col) = NaN;
map.y(:,cut_col) = NaN;
map.sigma(:,cut_col) = NaN;

return
