function [loud_coh] = findLoudCoh(map,params)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% finds loud coherence lines in coherence spectrum.
% To avoid labelling all loud points if there's a broad peak
% one specifies how many frequencies per sub-band they want
% and the funtion finds the maximum of each sub-band and
% if that maximum is large enough it stores the index of that 
% frequency. 
%
% This function is used in saveplotsAndData.m for labelling
% loud frequency spikes in the output of STAMP-PEM. 
%
% Relevant parameters:
%
% params.freqsPerBand ---- number of frequences per sub-band
% params.cohThreshold ---- coherence threshold for what determines 'loud'
% 
% Written by: Patrick Meyers; Jun 6th 2014
% contact:    meyers@physics.umn.edu
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initialize loud coherence vector
loud_coh = [];

% check that proper params are defined. if not, then return empty
% vector for loud coherences.
try 
  params.freqsPerBand;
  params.cohThreshold;
catch
  fprintf('Frequencies per band and/or coherence threshold arent set.\n Loud coherences wont be labelled.\n');
  return
end

% find number of bands based on freqs per band and
% number of frequencies used in analysis
numRegions = ceil(length(map.f)/params.freqsPerBand);

for i = 1:(numRegions-1); % loop of number of bands
  s = (i-1)*params.freqsPerBand+1;% start index
  e = i*params.freqsPerBand;% end index
  if e<map.f(end)% make sure end index isn't too large

    % find max in each band. decide if it's large enough to be interesting
    loud_coh_temp = find(map.coherence(s:e) == max(map.coherence(s:e)) & ...
    max(map.coherence(s:e))/map.theorCoherence(1) > params.cohThreshold)+s-1; 

    % add the result to the loud coherence vector
    loud_coh = [loud_coh loud_coh_temp];
  else
    
    % find max in each band. decide if it's large enough to be interesting
    loud_coh_temp = find(map.coherence(s:end) == max(map.coherence(s:end)) & ... 
    max(map.coherence(s:end))/map.theorCoherence(1) > params.cohThreshold)+s-1;

    % add the result to the loud coherence vector
    loud_coh = [loud_coh loud_coh_temp];
  end% if e<map.f
end% for i = ...

% EOF

