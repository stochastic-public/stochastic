function makeBokehPlotsPEM(coherenceData,output_directory,plot_title);
% CREATEBOKEHPLOTS Description
%       OUT = CREATEBOKEHPLOTS(IN)
%
% Long description
%
coherence = load(coherenceData);
flow = coherence.flow;
fhigh = coherence.fhigh;
clear coherence;
[junk path] = system('echo $STAMPPEM_PATH');
copy_necessary_files(output_directory);
bokeh_cmd = sprintf('python %s/bokeh/stampcoh_html.py -i %s -o %s',...
        path(2:end-1),coherenceData,[output_directory '/' plot_title]);
% call bokeh plotting code
[junk] = system(bokeh_cmd);



function copy_necessary_files(outDir)
[junk path] = system('echo $STAMPPEM_PATH');
system(['cp ' path(1:end-1) 'bokeh/*bokeh* ' outDir]);
system(['cp ' path(1:end-1) 'bokeh/*colorbar* ' outDir]);
system(['cp ' path(1:end-1) 'bokeh/*js* ' outDir]);
