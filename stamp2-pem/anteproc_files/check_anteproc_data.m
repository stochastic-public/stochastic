function [out_params,map1,map2] = check_anteproc_data(map1,map2,params1,params2,params)
% function [out_params,map1,map2] = check_anteproc_data(map1,map2,params1,params2)
%
% Checks parameters associated with the data loaded from both anteproc'd
% .mat files and ensures that they are compatible.  Outputs a single set
% of parameters that contains the user-requested STAMP parameters and
% the parameters used to generate the .mat files.  Also outputs map1
% and map2, with adjusted frequencies (if necessary).  The maps are adjusted
% to only contain frequencies that both maps have data for.
%
% Written by T. Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% CHECK PARAMS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make sure segment duration and deltaF are the same.
if (params1.segmentDuration ~= params2.segmentDuration)
  error('Segment durations are not the same.');
end
if (params1.deltaF ~= params2.deltaF)
  error('Maps have differet deltaFs.');
end

% Make sure frequency ranges overlap.
if ((params1.flow >= params2.fhigh) || (params1.fhigh <= params2.flow))
  error('Frequency ranges do not overlap.');
else
  flow = max(params1.flow,params2.flow);
  fhigh = min(params1.fhigh,params2.fhigh);
end

% Check number of segments per interval.
if (params1.numSegmentsPerInterval ~= params2.numSegmentsPerInterval)
  error('Maps have a different number of segments per interval.');
end

% COPY PARAMS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up output parameters.
out_params = params;

% Get fieldnames.
fn = fieldnames(params1);

% Find fieldnames that contain a "1" (detector-specific).
% Ex: params.ifo1 has corresponding params.ifo2.
% We don't care about params.ifo2 (in fact, it shouldn't
% be specified at all) but we want to be sure not to
% copy it (just in case the user did specify it).
fields1 = regexp(fn,'(.*)1$','tokens','once');
fields1 = fields1(~cellfun('isempty',fields1));
fields1 = [fields1{:}];

% Find fieldnames that don't have a "1" at the end.
fields = regexp(fn,'.*\D$','match','once');
fields = fields(~cellfun('isempty',fields));

% Set params.X1 to be params1.X1 and params.X2 to
% be params2.X1 (based on anteproc params setup).
for ii=1:length(fields1)
  end1 = [fields1{ii} '1'];
  end2 = [fields1{ii} '2'];
  out_params.(end1) = params1.(end1);
  out_params.(end2) = params2.(end1);
end

% Set other params to be params1.  Not perfect because some
% parameters could theoretically differ between params1 and params2
% but hopefully they won't if people are being careful.  We try to check some
% important parameters above.  Very difficult to account for differences like
% this since we would have to rename a lot of STAMP parameters.
for ii=1:length(fields)
  if ~isfield(params,fields{ii})
    out_params.(fields{ii}) = params1.(fields{ii});
  end
end

% Adjusted frequency range.
out_params.flow = flow;
out_params.fhigh = fhigh;

% Make sure the data requested by the user is available.
if (params.fmin < flow || params.fmax > fhigh)
  error('Requested frequency range outside limits of available data.');
end

% CHECK/ADJUST DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check if frequencies of map1 and map2 match up.  If they don't, we need
% to cut them to only the overlapping parts.
%f_diff = sum(abs(map1.f - map2.f));
f_diff = 1;
if (f_diff > 0)
  [f,idx1,idx2] = intersect(round(10000*map1.f)/10000,round(100000*map2.f)/100000);
  [map1.f,map2.f] = deal(f);
  map1.P = map1.P(idx1,:);
%  map1.naiP = map1.naiP(idx1,:);
  map1.fft = map1.fft(idx1,:);
  map2.P = map2.P(idx2,:);
%  map2.naiP = map2.naiP(idx2,:);
  map2.fft = map2.fft(idx2,:);
end

% Compare rbartildes and adjust frequencies as necessary.
% If they have different deltaF -> error.
if (map1.rbtinfo.deltaF ~= map2.rbtinfo.deltaF)
  error('rbartildes have different deltaFs.');
else
  dF = map1.rbtinfo.deltaF;
end

% Calculate frequency bins for each rbartilde.
f1max = ((length(map1.rbartilde)-1)*dF - map1.rbtinfo.flow);
f1 = map1.rbtinfo.flow:dF:f1max;
f2max = ((length(map2.rbartilde)-1)*dF - map2.rbtinfo.flow);
f2 = map2.rbtinfo.flow:dF:f2max;

% If frequencies don't match up, adjust.
%diff = sum(abs(f1 - f2));
diff = 1;
if (diff > 0)
  [f,idx1,idx2] = intersect(round(10000*f1)/10000,round(10000*f2)/10000);
  [map1.rbtinfo.flow,map2.rbtinfo.flow] = deal(min(f));
  map1.rbartilde = map1.rbartilde(idx1,:);
  map2.rbartilde = map2.rbartilde(idx2,:);
end  

% If an injection is present, make sure the parameters
% are consistent.
if (params1.stampinj & params2.stampinj)
  % Both detectors contain an injection
  % Check injection parameters for consistency.

  % Compare injection files.
  if ~strcmp(params1.pass.stamp.file,params2.pass.stamp.file)
    error('Each detector contains a different injection.');
  end

  % Compare injection sky directions.
  if (params1.pass.stamp.ra ~= params2.pass.stamp.ra) 
    error('Injection has different right ascension for each detector.');
  end
  if (params1.pass.stamp.decl ~= params2.pass.stamp.decl) 
    error('Injection has different declination for each detector.');
  end

  % Compare alphas.
  if (params1.pass.stamp.alpha ~= params2.pass.stamp.alpha) 
    error('Injection has different alpha for each detector.');
  end

  % Check injection start time in each detector and make sure
  % it is less than the maximum for earth-based detectors.
  % This will need to be changed if detectors are eventually
  % placed somewhere other than Earth.

  % Calculate maximum time delay for two earth-based detectors.
  % Allow for 2*dt extra to account for rounding off when
  % the time delay is calculated.
  rad_Earth_eq = 6378137; % equatorial radius of Earth in m.
  dt = params1.pass.stamp.t(2) - params1.pass.stamp.t(1);
  max_tau = ((rad_Earth_eq*2)/params1.c)+2*dt;
  actual_tau = abs((params1.pass.stamp.t(1) - params2.pass.stamp.t(1)));

  if (actual_tau > max_tau)
    error('The injection is not coherent for two Earth-based detectors.');
  end

  % Compare injection times at center of earth.
  % This is probably redundant with the above test,
  % but we may as well leave it in for safety.
  if (params1.pass.stamp.startGPS ~= params2.pass.stamp.startGPS) 
    error(['Injection has a different arrival time at the Earth''s center for' ...
	   ' each detector.']);
  end
  
elseif ((params1.stampinj == 1 & params2.stampinj == 0) | ...
	(params1.stampinj == 0 & params2.stampinj == 1))
  % Injection in one detector but not in the other.
  error('One detector contains an injection and the other does not.');
end


return;
