% This is a wrapper script that produces STAMP-PEM coherence results

% Input
channlesListFile = 'input/stamp_pem_L1_channel_list.txt';
pemParamsFile = 'input/stamp_pem_params.txt';

% Running the pipeline
GPSstart = 1096270216;
GPSend = GPSstart+3600;

% call script that runs stamp_pem the proper way
% for easiest condor use:

% run_stamp_pem(start,end,params,channels,doPostProc) is syntax for calling 
% this script
% run stamp pipeline on channel list without postprocessing
run_stamp_pem(GPSstart,GPSend,pemParamsFile,channlesListFile,false);

% To run with post processing.
%run_stamp_pem(GPSstart,GPSend,pemParamsFile,channlesListFile,true);


