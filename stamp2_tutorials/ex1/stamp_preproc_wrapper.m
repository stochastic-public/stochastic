function stamp_preproc_wrapper()

% function stamp_stochmap_wrapper
% Written by M. Coughlin (coughlim@carleton.edu)
%
% preproc wrapper for the STAMP example
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% params file
% Specifies the parameters you want to use to pre-process the data.
% Should be set up to use the 'params.txt' file in this directory as your params
% file.
paramsFile = [getenv('PWD') '/params.txt'];

% jobfile
% Specifies which data segments you want to analyze.  Should be set up to use
% the 'jobfile.txt' file in this directory as your jobfile.
jobsFile=[getenv('PWD') '/jobfile.txt'];
jobNumber = 1;

preproc(paramsFile, jobsFile, jobNumber);

