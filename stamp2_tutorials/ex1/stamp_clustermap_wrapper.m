function stamp_clustermap_wrapper()

% function stamp_clustermap_wrapper
% Written by M. Coughlin (coughlim@carleton.edu)
%
% Clustermap wrapper for the STAMP example
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params = stampDefaults;

jobsFile = [getenv('PWD') '/jobfile.txt'];
params.inmats = [getenv('PWD') '/mats/HL-SID_nspi9_df1_dt1_ts1'];

jobNumber = 1;

% search direction-------------------------------------------------------------
params.ra=6;   %right ascension in hours (for search direction)
params.dec=30; %declination in degrees (for search direction)

% frequency range--------------------------------------------------------------
params.fmin = 100;
params.fmax = 1200;

% override default parameters--------------------------------------------------
params.debug=false;
params.saveMat=true;
params.savePlots=true;
params.doFreqMask=false;
params.Autopower=true;
params.yMapScale = 1e-43;

% Frequency Masking
params.doStampFreqMask = true;
params.StampFreqsToRemove = [118,119,120,121,122,178,179,180,181,182,239,240,241,328,329,330,331,341,342,343,344,345,346,348,349,599,685,686,687,688,689,690,691,692,693,694,695,696,697,959,1028,1029,1030,1031,1032,1033,1034,1035,1036,1039,1040,1041,1043,1044,1045,1143,1144,1145,1146,1150,1151,1152,1153];
params.StampnBinsToRemove = ones(size(params.StampFreqsToRemove));

params.Autopower=true;

% search
params = burstegardDefaults(params);

% fixed sensitivity
params.fixAntennaFactors = true;
fprintf('params.fixAntennaFactors = %i\n', params.fixAntennaFactors);

% Glitch cuts
%params.glitchCut = 8;
%params.DQcut = 1;

jobsFileDataAll = load(jobsFile);
jobsFileData = jobsFileDataAll(jobNumber,:);
jobIndex = jobsFileData(1); jobStart = jobsFileData(2);
jobEnd = jobsFileData(3); jobDuration = jobsFileData(4);

jobStart = jobStart + 6;
jobEnd = min(jobStart+200,jobEnd);

cluster_out=clustermap(params, jobStart, jobEnd);
