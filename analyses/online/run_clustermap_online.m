function run_clustermap(pipe_params)

% keep track of time to finish
tic;

% begin with default parameters
params = stampDefaults;
params.hstart = pipe_params.gpsStart;
params.hstop = pipe_params.gpsEnd;
params.jobNumber=1;
params = default_params(params);
[params,pipe_params] = setup_params(params,pipe_params);

plotDirHold = params.plotdir;
% call clustermap
if params.clustermap.doStochtrackBezier
   params.plotdir = plotDirHold;
   % set clustering params and proper output file paths
   params = setClusteringParams(params,pipe_params,'Bezier');
   params.stochtrack.triggerGPS = pipe_params.triggerGPS;
   %stoch_out = clustermap(params, params.hstart, params.hstop);
   if pipe_params.timeshift == 0
      save([params.plotdir '/map.mat']);
   else
      save([params.plotdir '/map_' num2str(pipe_params.timeshift) '.mat']);
   end
   params.plotdir = [params.plotdir '/lonetrack'];
   createpath(params.plotdir);
   params.stochtrack.lonetrack = false;
   %stoch_out = clustermap(params, params.hstart, params.hstop);
   if pipe_params.timeshift == 0
      save([params.plotdir '/map.mat']);
   else
      save([params.plotdir '/map_' num2str(pipe_params.timeshift) '.mat']);
   end
end
if params.clustermap.doStochtrackCBC
   params.plotdir = plotDirHold;
   params = setClusteringParams(params,pipe_params,'CBC');
   params.stochtrack.triggerGPS = pipe_params.triggerGPS;
   stoch_out = clustermap(params, params.hstart, params.hstop);
   if pipe_params.timeshift == 0
      save([params.plotdir '/map.mat']);
   else
      save([params.plotdir '/map_' num2str(pipe_params.timeshift) '.mat']);
   end
   params.plotdir = [params.plotdir '/lonetrack'];
   createpath(params.plotdir);
   params.stochtrack.lonetrack = false;
   stoch_out = clustermap(params, params.hstart, params.hstop);
   if pipe_params.timeshift == 0
      save([params.plotdir '/map.mat']);
   else
      save([params.plotdir '/map_' num2str(pipe_params.timeshift) '.mat']);
   end
end
if params.clustermap.doStochtrackECBC
   params.plotdir = plotDirHold;
   params = setClusteringParams(params,pipe_params,'ECBC');
   params.stochtrack.triggerGPS = pipe_params.triggerGPS;
   stoch_out = clustermap(params, params.hstart, params.hstop);
   if pipe_params.timeshift == 0
      save([params.plotdir '/map.mat']);
   else
      save([params.plotdir '/map_' num2str(pipe_params.timeshift) '.mat']);
   end
   params.plotdir = [params.plotdir '/lonetrack'];
   createpath(params.plotdir);
   params.stochtrack.lonetrack = false;
   stoch_out = clustermap(params, params.hstart, params.hstop);
   if pipe_params.timeshift == 0
      save([params.plotdir '/map.mat']);
   else
      save([params.plotdir '/map_' num2str(pipe_params.timeshift) '.mat']);
   end
end
if params.clustermap.doBurstegard
   params.plotdir = plotDirHold;
   params.doStochtrack = 0;
   params = setClusteringParams(params,pipe_params,'Burstegard');
   stoch_out = clustermap(params, params.hstart, params.hstop);
   if pipe_params.timeshift == 0
      save([params.plotdir '/map.mat']);
   else
      save([params.plotdir '/map_' num2str(pipe_params.timeshift) '.mat']);
   end
end
% save results

toc;

return
