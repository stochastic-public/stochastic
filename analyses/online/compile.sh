#!/bin/bash

source /home/mcoughlin/matlab_scripts/matlab_script_2013a.sh

mkdir -p ${HOME}/STAMP/online/bin/
cd ${HOME}/STAMP/online/bin/
#rm -rf *
mkdir ${HOME}/matlab
cp ${HOME}/matapps/packages/stochastic/trunk/analyses/online/stamp_startup.m .
/ldcg/matlab_r2013a/bin/matlab <<EOF
 stamp_startup;
 mcc -C -R -nodisplay -m trig_analysis
EOF
# mcc -C -R -nojvm -R -nodisplay -m stamp_pem_run
./trig_analysis 0 0 0 0 0

cd ${HOME}/matapps/packages/stochastic/trunk/analyses/online/

