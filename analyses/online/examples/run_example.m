
outputPath = '/home/mcoughlin/STAMP/ER6/anteproc';

gpsStart = 1074839138;
gpsEnd = gpsStart + 300;
gpsStart = gpsEnd;
gpsEnd = gpsStart + 300;
%gpsStart = gpsEnd;
%gpsEnd = gpsStart + 300;

pipe_params = [];
pipe_params.outputPath = outputPath;
pipe_params.ifos = {'H1','L1','V1'};
pipe_params.channels = {'FAKE-STRAIN','FAKE-STRAIN','FAKE_h_16384Hz_4R'};
pipe_params.frameTypes = {'H1_ER_C00_L1','L1_ER_C00_L1','V1Online'};

pipe_params.ifos = {'H1','L1'};
pipe_params.channels = {'FAKE-STRAIN','FAKE-STRAIN'};
pipe_params.frameTypes = {'H1_ER_C00_L1','L1_ER_C00_L1'};

pipe_params.gpsStart = gpsStart;
pipe_params.gpsEnd = gpsEnd;
pipe_params.triggerGPS = (pipe_params.gpsEnd - pipe_params.gpsStart)/2;

pipe_params.doClustermap = 1;
pipe_params.doBknd = 0;
pipe_params.segmentDuration = 1;

numruns = 1000;

for i = 1:numruns
%run_online(outputPath,gpsStart,gpsEnd);

   fileDir = sprintf('/home/mcoughlin/STAMP/ER6/anteproc/maps/%d-%d',pipe_params.gpsStart,pipe_params.gpsEnd);
   if exist(fileDir)
      continue
   end

   run_pipeline(pipe_params);
   gpsStart = gpsEnd;
   gpsEnd = gpsStart + 300;
   pipe_params.gpsStart = gpsStart;
   pipe_params.gpsEnd = gpsEnd;
   pipe_params.triggerGPS = (pipe_params.gpsEnd - pipe_params.gpsStart)/2;

end

