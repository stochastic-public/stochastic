
import glue.ligolw.utils
import glue.ligolw.table
import glue.ligolw.lsctables
import sys
import numpy as np

inj_xml = "H1L1_all_successful.xml"

xmldoc = glue.ligolw.utils.load_filename(inj_xml, verbose=False)
injs = glue.ligolw.table.get_table(xmldoc,glue.ligolw.lsctables.SimInspiralTable.tableName)

txt_file = "hardwareInj.txt"

f = open(txt_file,"w")

for inj in injs:
    injtime = inj.t_end_time+1.e-9*inj.t_end_time_ns
    dec = inj.latitude * (360 / (2*np.pi))
    ra = inj.longitude * 24.0/360.0
  
    #if injfar > 1e-12:
    #    continue
    f.write("%.0f %.5f %.5f\n"%(injtime,ra,dec))

f.close()


