function run_postprocessing(pipe_params)
% function run_pipeline(pipe_params)
% Given a STAMP online params struct, runs the pipeline. This pipeline
% generates mat files and ft-maps using STAMP, running various search algorithms to
% search for structure. 
% Routine written by Michael Coughlin and Patrick Meyers
% Modified: 6/15/2014
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

createpath(pipe_params.outputPath);
mapsdir = [pipe_params.outputPath '/maps/'];
postdir = [pipe_params.outputPath '/post/'];
createpath(postdir);

for i = 1:length(pipe_params.ifos)
   for j = 1:length(pipe_params.ifos)
      if i>=j
         continue
      end

      pipe_params.ifo1 = pipe_params.ifos{i}; pipe_params.ifo2 = pipe_params.ifos{j};

      fid = fopen([postdir '/' pipe_params.ifo1 pipe_params.ifo2 '.html'],'w+');

      % HTML Header
      fprintf(fid,'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"\n');
      fprintf(fid,'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n');
      fprintf(fid,'\n');
      fprintf(fid,'<html>\n');
      fprintf(fid,'\n');
      fprintf(fid,'<head>\n');
      fprintf(fid,'<title>Summary Page</title>\n');
      fprintf(fid,'<link rel="stylesheet" type="text/css" href="../../main.css">\n');
      fprintf(fid,'<script src="../../sorttable.js"></script>\n');
      fprintf(fid,'</head>\n');

      fprintf(fid,'<table\n');
      fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
      fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
      fprintf(fid,'<tbody>\n');
      fprintf(fid,'<tr>\n');
      fprintf(fid,'<td\n');
      fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
      fprintf(fid,'style="font-weight: bold;">Summary Page</span></big></big></big></big><br>\n');
      fprintf(fid,'</td>\n');
      fprintf(fid,'</tr>\n');
      fprintf(fid,'</tbody>\n');
      fprintf(fid,'</table>\n');
      fprintf(fid,'<br>');

      folders = dir([mapsdir '/*-*']);      
      values = {'CBC'};
      %values = {''};
      for k = 1:length(folders)
         for m = 1:length(values)
         folderName = [mapsdir '/' folders(k).name '/' pipe_params.ifo1 pipe_params.ifo2 '/' values{m}];
         if exist([folderName '/snr.png'])

            map_out = load([folderName '/map.mat']); 

            % HTML table of channel plots
            fprintf(fid,'<table\n');
            fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
            fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
            fprintf(fid,'<tbody>\n');
            fprintf(fid,'<tr>\n');
            fprintf(fid,'<td\n');
            fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
            fprintf(fid,'style="font-weight: bold;">%s, %s, %s%s </span></big></big><br>\n',folders(k).name,values{m},pipe_params.ifo1,pipe_params.ifo2);
            fprintf(fid,'</td>\n');
            fprintf(fid,'<td\n');
            fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
            fprintf(fid,'style="font-weight: bold;">SNR: %.3f </span></big></big><br>\n',map_out.stoch_out.max_SNR);
            fprintf(fid,'</td>\n');
            fprintf(fid,'</tr>\n');
            fprintf(fid,'<tr>\n');
            fprintf(fid,'<td\n');
            fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
            fprintf(fid,'style="font-weight: bold;">Cross-power spectrogram </span></big></big><br>\n');
            fprintf(fid,'</td>\n');
            fprintf(fid,'<td\n');
            fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
            fprintf(fid,'style="font-weight: bold;">Track recovery </span></big></big><br>\n');
            fprintf(fid,'</td>\n');
            fprintf(fid,'</tr>\n');
            fprintf(fid,'<tr>\n');
            fprintf(fid,'<td style="vertical-align: top;">\n');
             
            plotpath = sprintf('../maps/%s/%s%s/%s/snr.png',folders(k).name,pipe_params.ifo1,pipe_params.ifo2,values{m});
            fprintf(fid,'<a href="%s"><img alt="" src="%s" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',plotpath,plotpath);
            

	    if exist([folderName '/large_cluster.png']) 
            plotpath = sprintf('../maps/%s/%s%s/%s/large_cluster.png',folders(k).name,pipe_params.ifo1,pipe_params.ifo2,values{m});
            else 
               plotpath = sprintf('../maps/%s/%s%s/%s/rmap.png',folders(k).name,pipe_params.ifo1,pipe_params.ifo2,values{m});
            end
            fprintf(fid,'</td>');
            fprintf(fid,'<td style="vertical-align: top;">\n');
            fprintf(fid,'<a href="%s"><img alt="" src="%s" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',plotpath,plotpath);
            fprintf(fid,'</td>');
	    fprintf(fid,'</tr>');
            
	    if exist([folderName '/pvalues.png'])
		
		pvaluePlotpath = sprintf('../maps/%s/%s%s/%s/pvalues.png',folders(k).name,pipe_params.ifo1,pipe_params.ifo2,values{m});
                
		fprintf(fid,'<tr>\n');
		fprintf(fid,'<td style="vertical-align: top;">\n');
                fprintf(fid,'<a href="%s"><img alt="" src="%s" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',pvaluePlotpath,pvaluePlotpath);
                fprintf(fid,'</td>');
		fprintf(fid,'</tr>');
	    end	


            fprintf(fid,'</tbody>\n');
            fprintf(fid,'</table>\n');

         end
      end
      end

      % Close HTML
      fprintf(fid,'</html>\n');
      fclose(fid);
   end
end

