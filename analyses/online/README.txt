Summary

STAMP online simplifies the process of generating mat-files and subsequently
running clustering algorithms by setting up the necessary files for you.

Instructions

1. If you only desire to run on some start and end gps time, simply modify
examples/run_example.m with the relevent parameters.

2. If you desire to run on a specific trigger with a gps time and sky
position, please see examples/run_trig_analysis.m.

3. gracedb can be queried with gracedb/run_trig_analysis.py, which given a
start and end gps time, will run the analysis for you on triggers it finds.


