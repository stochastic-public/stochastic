#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Copyright (C) 2014-2015 James Clark <clark@physics.umass.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
"""
import os
import sys

import numpy as np

import lal
import lalsimulation as lalsim

#from pylal import Fr
from glue import lal as gluelal

#import simsig
from pycbc.types import TimeSeries, FrequencySeries, zeros, Array, complex64, float64

import pycbc.filter
import pycbc.filter.matchedfilter
from pycbc.psd import aLIGOZeroDetHighPower

data = np.genfromtxt("magA_tapered.dat",delimiter=" ")

f_low=40
f_high=1000

tlen = len(data)

# Generate the aLIGO ZDHP PSD
delta_f = 4096/float(tlen);
flen = tlen/2 + 1

psd = aLIGOZeroDetHighPower(flen, delta_f, f_low) 
psd.save('psd.txt')

vec1 = np.empty(tlen)
for i in range(tlen):
    vec1[i] = data[i,2]

vec2=TimeSeries(vec1,dtype=float64,delta_t=1.0/4096.)
np.savetxt('vec1.txt',vec1)

snr = pycbc.filter.matchedfilter.sigma(vec2, psd, low_frequency_cutoff=f_low, high_frequency_cutoff=f_high)
print 'SNR=', snr
    
