function [duration,fmin,fmax,hrss,energy] = geninspiral(mass1,mass2,iota,dist);

fmin = 10;
fs = 16384;

Mtot = mass1+mass2;
eta  = mass1*mass2 / (Mtot*Mtot);
LAL_MTSUN_SI = 4.92549095e-6;
totalMass    = Mtot*LAL_MTSUN_SI;
t0   = 5.0/(256.0*eta*(totalMass^(5.0/3.0)) *(pi*fmin)^(8.0/3.0));
t2   = (3715.0 + (4620.0*1.0*eta))/(64512.0*eta*totalMass*(pi*fmin)^(2.0));
t3   = pi/(8.0*eta*(totalMass^(2.0/3.0))*(pi*fmin)^(5.0/3.0));
t4   = (5.0/(128.0*eta*(totalMass^(1.0/3.0))*(pi*fmin)^(4.0/3.0))) * (3058673./1016064. + 5429.0*1.0*eta/1008.0 +617.*1.0*eta*eta/144.0);
t5   = 5.0*(7729.0/252.0 + 1.0*eta)/(256.0*eta*fmin);
duration = t0 + t2 - t3 + t4 - t5;
duration = duration + 1.0; % account for ringdown + delay

if duration > 3600
   duration = 3600;
end

[hp, hx, t, t_insp, phi, amp, freq, tplunge, fisco] = inspiral2pn(mass1,mass2,iota,dist,fs,duration,fmin,0,duration);
[junk,indexmax] = nanmax(freq);
[junk,indexmin] = nanmin(freq);

duration=t_insp(indexmax) - t_insp(indexmin);
fmin = nanmin(freq);
fmax = nanmax(freq);

dt = t_insp(2) - t_insp(1);
hrss =sqrt(sum(hp.^2+hx.^2)*dt);


energy = wvf_energy2(hp, hx, dt, dist*10^6);
