function [hp, hx, t, t_insp, phi, amp, freq, tplunge, fisco] = ...
    inspiral2pn(mass1,mass2,iota,distance,fs,tc,fmin,tstart,tend)
% INSPIRAL2PN - Generate Post-Newtonian inspiral waveform.
%
% This function generates a post-Newtonian inspiral waveform.  It is
% quadrupole in amplitude, but 2PN in phase and frequency.  The waveform cuts
% off abruptly (no smoothing) at the earlier of the coalescence time or the
% time that the phase second derivative becomes negative.
%
% usage:
%
% [hp, hx, t, t_insp, phi, amp, freq, tplunge, fisco] = ...
%    pninspiral(mass1,mass2,iota,distance,fs,ttotal,tc)
%
% mass1         component mass 1 [solar masses]
% mass2     component mass 2 [solar masses]
% iota      inclination angle [rad]
% distance      distance [Mpc]
% fs        sampling rate [s^-1]
% ttotal    waveform duration [s]
% tc        coalescence time [s]
%
% credits: This function is adapted from "inspmergring.m", by an unknown
% author.
% Refs: The phase and frequency evolution are the eq. (7) and (8) of
% http://arxiv.org/abs/gr-qc/9602024 (Blanchet et al. 1996). The propagation
% of tails eq. (5) is neglected also only the first PN amplitude terms (3a)
% and (4a) are used.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Hardwired constants.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

G = 6.673*1e-11;            %-- the gravitational constant in m^3/(kg*s^2)
hplanck = 6.626068e-34;     %-- Planck's constant in m^2*kg/s
c = 299792458;              %-- the speed of light in m/s
pc = 3.0856775807*1e16;     %-- a parsec in m
Mpc = pc*1e6;               %-- a mega-parsec in m
ms = 1.98892*1e30*G/c^3;    %-- a solar mass in s


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Derived parameters.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Mass of the two objects [s]
m1 = mass1*ms;
m2 = mass2*ms;
% ---- Total mass of the system [s].
M = m1+m2;
% ---- Reduced mass of the system [s].
mu = m1*m2/M;
% ---- mass difference of the system [s].
dm = m1 - m2;
% ---- Mass ratio.
eta = mu/M;
% ---- Chirp mass.
Mchirp = mu^(3/5) * M^(2/5);
% % ---- Quality factor of the BH ringdown.
% Q = 2*(1-a)^(-9/20);
% ---- Distance from the source to the detector [s].
r = distance*Mpc/c;

% ---- Time before collapse when separation reaches that of a test particle
%      in the innermost stable circular orbit of Schwarzschild mass M.
tplunge = (405*M/(16*eta));
% ---- ISCO frequency?
fisco = 1/(6^(3/2)*pi*M);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Times and frequencies.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ttotal = tend - tstart;
% ---- Waveform duration in samples.
N = floor(ttotal*fs);
% ---- Sample times.
t = tstart + ([0:N-1].' / fs);

% ---- Assign storage for output waveforms.
hp = zeros(size(t));
hx = zeros(size(t));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Construct inspiral.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Time samples of inspiral portion of waveform.
%index = find(t<tc);  %-- don't want to include t=tc since things diverge.
%if length(index) == 0
%   hp = []; hx = []; t = []; t_insp = [];
%   phi = []; amp = []; freq = []; tplunge = []; fisco = [];  
%   return
%end
%t_insp = t(index);
t_insp = t;

% ---- Rescaled time parameter.
tau = (eta/(5*M)*(tc-t_insp)).^(-1/8);
tau(t_insp>=tc) = NaN;

% ---- GW phase.
Phi0 = 0;  %-- phase at coalescence time
phic0 = -1/eta.*tau.^(-5);
phic2 = -1/eta*(3715/8064+55*eta/96).*tau.^(-3);
phic3 = -1/eta*(-3*pi/4).*tau.^(-2);
phic4 = -1/eta*(9275495/14450688+284875*eta/258048+1855*eta^2/2048).*tau.^(-1);
phi = Phi0 + phic0+phic2+phic3+phic4;

omega0 = tau.^3;
omega2 = (743/2688+11/32*eta)*tau.^5;
omega3 = -3*pi/10*tau.^6;
omega4 = (1855099/14450688+56975/258048*eta+371/2048*eta^2)*tau.^7;
omega = 1/8*(omega0 + omega2 + omega3 + omega4);

freq = [diff(phi);0]/(2*pi/fs);

% ---- Check for non-monotonic frequency evolution and cut off waveform
%      sharply there.
[junk,badEvoln] = max(freq);
if ~isempty(badEvoln)
    tau(badEvoln+1:end) = NaN;
    omega(badEvoln+1:end) = NaN;
    phi(badEvoln+1:end) = NaN;
    freq(badEvoln+1:end) = NaN;
    hp(badEvoln+1:end) = 0;
    hx(badEvoln+1:end) = 0;
end

badEvoln = find(freq < fmin);
if ~isempty(badEvoln)
    tau(badEvoln) = NaN;
    omega(badEvoln) = NaN;
    phi(badEvoln) = NaN;
    freq(badEvoln) = NaN;
    hp(badEvoln) = 0;
    hx(badEvoln) = 0;
end
%t_insp = t_insp - t_insp(1);

% ---- Inspiral GW amplitude.
x = omega.^(2/3);
amp = 2*mu*x/r;

% ---- Construct plus and cross waveforms.
cc=cos(iota);ss=sin(iota);
hp = amp .* (1+cc^2) .* cos(2*phi);
hx = 2 * amp *cc .* sin(2*phi);
hp(isnan(hp)) = 0;
hx(isnan(hx)) = 0;

% ---- Done.
return
