psd=load('aLIGO_O1ASD.txt');
psd(:,2)=psd(:,2).^2;

psd1=load('aLIGO_O1PSD_lineremovedhand.txt');

loglog(psd(:,1),psd(:,2),'b')
hold on
loglog(psd1(:,1),psd1(:,2),'r')

grid

freqs2=logspace(log10(1),log10(8000),350);
psd2=interp1(psd1(:,1),psd1(:,2),freqs2);
ext=find(isnan(psd2(1,:)));
if size(ext,1)>1
  psd2(1,ext(1:end-1))=psd2(1,ext(end-1)+1);
  psd2(1,ext(end))=psd2(1,ext(end)-1);
else
  psd2(1,ext(end))=psd2(1,ext(end)-1);
end

loglog(freqs2,psd2,'g');

bb=load('PSD-H1-ER8-1124096858-1124097258.txt');
loglog(bb(:,1),bb(:,2),'k');
hold off

xlim([1 8000])
xlabel('Frequency [Hz]');
ylabel('PSD');

legend('O1 measured', 'O1 simplified by hand','O1 interp','ER8')

aa=[freqs2;psd3]';
dlmwrite('aLIGO_O1PSD.txt',aa,'delimiter',' ','precision','%8.4g');