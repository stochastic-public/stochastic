psd1=load('LIGOsrdPSD_40Hz.txt');
psd2=load('aLIGOpsd_Early.txt');
psd3=load('aLIGO_O1PSD_6Hz.txt');
psd4=load('O2_psd.txt');
psd5=load('aLIGO_O2PSD.txt');
psd6=load('O2_H1.dat');

loglog(psd1(:,1),psd1(:,2),'b');
hold on
loglog(psd2(:,1),psd2(:,2),'r');
loglog(psd3(:,1),psd3(:,2),'g');
loglog(psd4(:,1),psd4(:,2),'m');
loglog(psd5(:,1),psd5(:,2),'k');
loglog(psd6(:,1),psd6(:,2),'c');
hold off

xlim([1 8000])
grid

legend('iLIGO 40 Hz','aLIGO Early 9Hz','aLIGO O1 6Hz','aLIGO O2', ...
       'aLIGO O2','VF');