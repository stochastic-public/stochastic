MAB 15/09/2015 aLIGO PSDs. 

References: 
[1] https://dcc.ligo.org/LIGO-T1200307/public
[2] see also https://ask.ligo.org/questions/1136/where-can-i-find-a-text-file-of-the-early-aligo-psd

ASD files extracted from [1]
aEarlyPSD.txt

MAB 27/07/2019 aLIGO O2 PSDs.

References:
[1] https://dcc.ligo.org/LIGO-G1801950
[2] https://dcc.ligo.org/LIGO-G1801952

The best "H1" PSD of O2 has been extracted from [1].
The best "L1" PSD of O2 has been extracted from [2].
Then the high frequency content has been simplified (file with the extension modified).
generate_O2PSD.m generates aLIGO_O2PSD.txt
