function generate_O2PSD(itf)

if itf == 'H1'
  asd=load('2017-06-10_DCH_C02_H1_O2_Sensitivity_strain_asd.txt');
  asd1=load('2017-06-10_DCH_C02_H1_O2_Sensitivity_strain_asd_modified.txt');
else
  asd=load('2017-08-06_DCH_C02_L1_O2_Sensitivity_strain_asd.txt');
  asd1=load('2017-08-06_DCH_C02_L1_O2_Sensitivity_strain_asd_modified.txt');
end  

psd=asd;
psd(:,2)=asd(:,2).^2;
psd1=asd1;
psd1(:,2)=asd1(:,2).^2;
  
loglog(psd(:,1),psd(:,2),'b')
hold on
loglog(psd1(:,1),psd1(:,2),'r')

grid

freqs2=logspace(log10(5),log10(8192),300);
psd2=interp1(psd1(:,1),psd1(:,2),freqs2);
ext=find(isnan(psd2(1,:)));
if size(ext,1)>1
  psd2(1,ext(1:end-1))=psd2(1,ext(end-1)+1);
  psd2(1,ext(end))=psd2(1,ext(end)-1);
end

psd4=movmedian(psd2,20);

loglog(freqs2,psd2,'g');
loglog(freqs2,psd4,'k');
hold off

xlim([1 9000])
xlabel('Frequency [Hz]');
ylabel('PSD');

legend('O2 measured', 'O2 simplified by hand','O2 interp',...
       'O2 final')

aa=[freqs2;psd4]';
dlmwrite([itf '_O2PSD.txt'],aa,'delimiter',' ','precision','%8.4g');