#! /usr/bin/env bash

# functions
usage(){
    echo 'Arg 1: run'
    echo 'Arg 2: calibration [C00/C01/C02]'
    echo 'Arg 3: Loudest events list'
    echo 'Arg 4: SNRfracTime [0] or event full time duration [1]'

    echo 'Arg 5: Minimal duration (in second) of the dq segments [0]'
    echo 'Arg 6: shift (in second) applied to the trigger list [0]'
    echo 'Arg 7: SNR threshold (triggers SNR)'
    echo 'Arg 8: remove triggers vetoed by Rveto (1==yes, 0==no)'
    
    exit
}

# main
[[ $# -ne 8 ]] && usage

RUN=$1
CAL=$2
LOUDEST=$3
TIME=$4
DQ_MIN_DURATION=$5
TIMESHIFT=$6
SNR_THRESHOLD=$7
RVETO=$8

if [ ! -d tmp ]; then
    mkdir tmp
else
#    rm -r tmp/*loudest*
    rm -r tmp/*dqlist.table
fi

cp ${LOUDEST} tmp/tmp
if [ ${RVETO} == 1 ]; then
    cat tmp/tmp | awk '{if ($18==0) print $0}' > tmp/tmp1
    mv tmp/tmp1 tmp/tmp
fi

cat tmp/tmp | awk -v thr=${SNR_THRESHOLD} '{if ($2>thr) print $0}' > tmp/tmp1
mv tmp/tmp1 tmp/tmp

# apply vetoes
matlab -nodisplay -r "apply_vetoes tmp/tmp tmp/tmp1 tmp/H1_vetoes.txt 1"
mv tmp/tmp1 tmp/tmp
matlab -nodisplay -r "apply_vetoes tmp/tmp tmp/tmp1 tmp/L1_vetoes.txt 2"
mv tmp/tmp1 tmp/tmp

# Prepare triggers' list
if [ ${TIME} == 0 ]; then
    cat tmp/tmp | awk -v shift=${TIMESHIFT} '{printf("%.2f %.2f\n",$14+shift, $14+3+shift)}' | sort | uniq > tmp/loudest_H1.txt
    cat tmp/tmp | awk -v shift=${TIMESHIFT} '{printf("%.2f %.2f %.2f\n",$14+shift, $14+3+shift,$2)}' | sort -r | uniq -w 27 | sort > tmp/loudest_H1_full.txt

    cat tmp/tmp | awk -v shift=${TIMESHIFT} '{printf("%.2f %.2f\n",$15+shift, $15+3+shift)}' | sort | uniq > tmp/loudest_L1.txt
    cat tmp/tmp | awk -v shift=${TIMESHIFT} '{printf("%.2f %.2f %.2f\n",$15+shift, $15+3+shift,$2)}' | sort -r | uniq -w 27 | sort > tmp/loudest_L1_full.txt
else
    cat tmp/tmp | awk -v shift=${TIMESHIFT} '{printf("%.2f %.2f\n",$3+shift, $3+$5+shift)}' | sort | uniq > tmp/loudest_H1.txt
    cat tmp/tmp | awk -v shift=${TIMESHIFT} '{printf("%.2f %.2f %.2f\n",$3+shift, $3+$5+shift,$2)}' | sort -r | uniq -w 27 | sort > tmp/loudest_H1_full.txt

    cat tmp/tmp | awk -v shift=${TIMESHIFT} '{printf("%.2f %.2f\n",$4+shift, $4+$5+shift)}' | sort | uniq > tmp/loudest_L1.txt
    cat tmp/tmp | awk -v shift=${TIMESHIFT} '{printf("%.2f %.2f %.2f\n",$4+shift, $4+$5+shift,$2)}' | sort -r | uniq -w 27 | sort > tmp/loudest_L1_full.txt
fi

for ifo in 'H1' 'L1'; do
    echo 'performance for ' ${ifo} '...'
    if [ ${RUN} == S5 ]; then
        science_dq_name=${ifo}\:Science
    elif [ ${RUN} == S6 ]; then
        science_dq_name=${ifo}\:DMT-SCIENCE
    elif [ ${RUN} == O1 ]; then
        if [ ${CAL} == "C00" ]; then
            science_dq_name=${ifo}\:DMT-ANALYSIS_READY
        elif [ ${CAL} == "C01" ]; then
            science_dq_name=${ifo}\:DCS-ANALYSIS_READY_C01
        elif [ ${CAL} == "C02" ]; then
            science_dq_name=${ifo}\:DCS-ANALYSIS_READY_C02
        else
            echo 'wrong calibration choice for O1'
            exit 1
        fi
    fi

    science_dq_name=`echo science/${RUN}/${science_dq_name}`

    if [ -f tmp/${ifo}_dqlist.txt ]; then
        rm tmp/${ifo}_dqlist.txt
    fi
       
    DQFiles=`ls allDQs_Science/${RUN}/$ifo*`
    FILENAME=tmp/loudest_${ifo}.txt

    for dq in $DQFiles; do
        INT=`segexpr 'intersection('$FILENAME','$dq')'`

        if [ -n "$INT" ]; then
            echo $dq >> tmp/${ifo}_dqlist.txt
        fi
    done

    rc=`wc -l tmp/${ifo}_dqlist.txt`
    if [ ${#rc} -gt 0 ]; then
        echo 'Found overlapping segments'

        # Set OFFLINE variable for exiting properly from matlab
        export OFFLINE=1

        matlab -nodisplay -r "performance ${ifo} ${science_dq_name} ${DQ_MIN_DURATION}"
    else
        echo 'No coincidence with any segments lists'
    fi

done

FILENAME=`echo ${LOUDEST} | cut -d "." -f 1`

./generate_html.sh ${RUN} DQ_MIN_DURATION ${TIMESHIFT} ${FILENAME}
