#!/usr/bin/env bash

###
# This script generates a combined veto list from a list of flags
# 
# Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
###

# functions
usage(){
    echo 'Missing arguments'
    echo 'Arg 1: run [S5/S6/O1]'
    echo 'Arg 2: calibration [C00/C01/C02]'
    echo 'Arg 3: ifo'
    echo 'Arg 4: input file that contains the selected flags and the minimal duration of segments'
    exit
}

# main
[[ $# -ne 4 ]] && usage

RUN=$1
CAL=$2
IFO=$3
FILENAME=$4

echo 'Generating veto list for ' $RUN

### INPUT LIST
LIST=`cat $FILENAME | cut -d " " -f 1`
TIME=`cat $FILENAME | cut -d " " -f 2`

j=1
for i in $TIME; do
    dur[$j]=$i
    j=$(($j+1))
done

### OUTPUT LIST
OUTPUT=${RUN}'_'${IFO}'_veto.txt'

if [ ${RUN} == S5 ]; then
    science_dq_name=${IFO}\:Science
elif [ ${RUN} == S6 ]; then
    science_dq_name=${IFO}\:DMT-SCIENCE
elif [ ${RUN} == O1 ]; then
    if [ ${CAL} == "C00" ]; then
        science_dq_name=${IFO}\:DMT-ANALYSIS_READY
    elif [ ${CAL} == "C01" ]; then
        science_dq_name=${IFO}\:DCS-ANALYSIS_READY_C01
    elif [ ${CAL} == "C02" ]; then
        science_dq_name=${IFO}\:DCS-ANALYSIS_READY_C02
    else
        echo 'wrong calibration choice for O1'
        exit 1
    fi
fi

science_dq_name=`echo science/${RUN}/${science_dq_name}.txt`
sc_duration=`cat ${science_dq_name} | awk 'BEGIN{sum=0}{sum=sum+$2-$1}END{print sum}'`

i=1
j=1
for dq in $LIST; do
#    echo $dq ' ' $IFO ' ' $j ' ' ${dur[$j]}
    if echo "$dq" | grep -q "$IFO"; then
        dur_cut=${dur[$j]}
        if [ $i == 1 ]; then
            cp allDQs_Science/${RUN}/${dq}.txt tmp1
            cat tmp1 | awk -v dur=${dur_cut} '{if ($2-$1>dur) print $0}' > tmp2
            mv tmp2 tmp1
            i=$(($i+1))
        else
            cp allDQs_Science/${RUN}/${dq}.txt tmp2
            cat tmp2 | awk -v dur=${dur_cut} '{if ($2-$1>dur) print $0}' > tmp3
            mv tmp3 tmp2
            segexpr "union(tmp2,tmp1)" | sort | uniq > tmp3
            mv tmp3 tmp1
        fi

        veto_time=`cat tmp1 | awk 'BEGIN{sum=0}{sum=sum+$2-$1}END{print sum}'`
        dt=`echo "scale=5; $veto_time / $sc_duration * 100 " | bc`
        echo $dq $veto_time $dt    
    else
        echo 'No ' $IFO in $dq
    fi
    j=$(($j+1))
done

segexpr "union(tmp1,tmp1)" | sort > tmp2
sort tmp2 | uniq > $OUTPUT

veto_time=`cat $OUTPUT | awk 'BEGIN{sum=0}{sum=sum+$2-$1}END{print sum}'`
dt=`echo "scale=5; $veto_time / $sc_duration * 100" | bc`
seg_nb=`wc -l $OUTPUT | cut -d " " -f 1`
echo 'Veto duration= '$veto_time 's. Science duration= ' $sc_duration 's. Deadtime=' $dt'% segments nb: ' $seg_nb

rm tmp1
rm tmp2




