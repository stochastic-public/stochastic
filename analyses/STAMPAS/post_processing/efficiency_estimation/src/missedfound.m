function missedfound(searchPath)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% 
%
% M. A. Bizouard (mabzoua@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  import classes.waveforms.dictionary
  import classes.utils.waitbar
  import classes.utils.logfile
  

  %---------------------------------------------------------------------
  %% INIT 
  %---------------------------------------------------------------------
  % 
  % get waveform dictionary
  % create waitbar & logfile
  %


  % 
  %% Get waveform dictionary
  %---------------------------------------------------------------------
  %
  % /!\  load first the dictionary because it use java library that
  % clean all singelton class -> instancied waitbar & logtfile will
  % be delete   
  %
  dic = dictionary.parse([searchPath '/waveforms.txt']);


  waitbar(sprintf('missedfound [init]'));
  
  logfile([searchPath '/logfiles/missedfound.log']);
  logfile('missedfound begin ...');


  figures_folder=[searchPath '/figures'];
  nbInjections = get_nb_injections (searchPath);
  
  %--------------------------------------------------------------
  %% MAIN
  %--------------------------------------------------------------
  %
  for wvf=dic.headers'
    nb_of_alphas=size(wvf.alphas,2);
    
    waitbar(sprintf('missedfound [%3.0f]',100*find(strcmp(dic.id,wvf.id))/dic.nbWvf));
    logfile('***********************************************');
    logfile(['process ' wvf.legend]);

    
    %% INIT RESULTS TABLE
    % --------------------------------------------------------------
    %  Nline : nb injection x nb alphas  
    %  Ncol  : 12
    %
    %  col  1 : alphas
    %  col  2 : INJ GPS start time
    %  col  3 : INJ GPS stop time
    %  col  4 : triggers GPS start time
    %  col  5 : triggers GPS stop time
    %  col  6 : triggers SNR
    %  col  7 : SNR coherent MC
    %  col  8 : SNR coherent
    %  col  9 : SNR ifo1
    %  col 10 : SNR ifo2
    %  col 11 : trigger DQ flaged 
    %  col 12 : trigger glitch cut flaged 
    %
    results=zeros(nbInjections*nb_of_alphas,12);

    for alpha=wvf.alphas
      injName10=[wvf.file '_' num2str(alpha,'%0.10f')];
      injName6=[wvf.file '_' num2str(alpha,'%0.6f')];

      if exist([searchPath '/results/results_merged/injections_' injName10 '_merged'...
            '.mat'])
        load([searchPath '/results/results_merged/injections_' injName10 '_merged'...
              '.mat']);
      elseif exist([searchPath '/results/results_merged/injections_' injName6 '_merged'...
            '.mat'])
        load([searchPath '/results/results_merged/injections_' injName6 '_merged'...
              '.mat']);
      else
         continue
         error(['file :' searchPath '/results/results_merged/injections_' injName10 '_merged' ...
            '.mat']); 
      end

      % fill first column
      %continue
      offset = nbInjections*(find(wvf.alphas==alpha)-1)+1;
      ii = offset:offset+injections.numElements-1;

      results(ii,1)  = repmat(alpha,injections.numElements,1);
      results(ii,2)  = injections.startGPS';
      results(ii,3)  = injections.startGPS'+injections.duration';
      results(ii,7)  = sqrt(injections.SNR1s.^2+injections.SNR2s.^2)';
      results(ii,8)  = sqrt(injections.SNR1.^2+injections.SNR2.^2)';
      results(ii,9)  = injections.SNR1s';
      results(ii,10) = injections.SNR2s';
      

      if exist([searchPath '/results/results_merged/results_' ...
                injName10 '_filtered.mat'])
        load([searchPath '/results/results_merged/results_' ...
              injName10 '_filtered.mat']);
      elseif exist([searchPath '/results/results_merged/results_' ...
                injName6 '_filtered.mat'])
        load([searchPath '/results/results_merged/results_' ...
              injName6 '_filtered.mat']);
      else
        logfile(['results_' injName10 '_filtered.mat not found, probably '...
                 'no injection recovered. Continue']);
        continue
      end

      %% loop over the injection 
      %
      for inj=1:injections.numElements
        idx=offset+inj-1;

        filterTime=data.GPSstart<=results(idx,3)&...
            data.GPSstop>=results(idx,2);

          if sum(filterTime)==0

            balance=abs(results(idx,9)-results(idx,10))/(results(idx,9)+results(idx,10));

            if (results(idx,7)>900 | results(idx,7)>200 & balance<1)
              message=[char(wvf.legend) ...
                       ' missed injection at [' num2str(results(idx,2)) ' ' ...
                       num2str(results(idx,3)) ' ] alpha= ' ...
                       num2str(alpha) ' SNRnet= ' ...
                       num2str(results(idx,7)) '(PSD ' ...
                       num2str(results(idx,8)) ')' ...
                       ' H1 SNR:' num2str(results(idx,9)) ...
                       ' L1 SNR:' num2str(results(idx,10))];
              %waitbar.warning(message);
              logfile(message);
            end

          else
            if sum(filterTime)>1
              logfile(['More than 1 trigger associated to injection ' ...
                       char(wvf.id) ...
                       ' [' num2str(results(idx,2)) ...
                       ' ' num2str(results(idx,3)) '] ' ...  
                       ' for alpha ' num2str(alpha)]);
              %%%% Added to insure only the greatest SNR injection will be used when
              %%%%   there are two or more occurring with the same waveform and alpa.
              %%%%  --- Sean Morriss   contact: sean.morriss@yahoo.com

              [~, ndx]=sort([data(filterTime).SNR],'descend');

              filterTime=find(filterTime>0);
              filterTime=filterTime(ndx(1));
            end

            results(idx,4)=data(filterTime).GPSstart;
            results(idx,5)=data(filterTime).GPSstop;            
            results(idx,6)=sqrt(data(filterTime).SNR);
            results(idx,11)=(data(filterTime).veto.ifo1|data(filterTime).veto.ifo2);
            
            if results(idx,7)>300 & results(idx,6)<40
              logfile([char(wvf.id) ...
                       ' badly reconstructed [' ...
                       num2str(results(idx,2)) ' ' ...
                       num2str(results(idx,3)) ' ] alpha= ' ...
                       num2str(alpha) ' SNRnet= ' ...
                       num2str(results(idx,7)) '(PSD ' ...
                       num2str(results(idx,8)) ')' ...
                       ' H1 SNR:' num2str(results(idx,9)) ...
                       ' L1 SNR:' num2str(results(idx,10)) ...
                       ' reco SNR: ' num2str(results(idx,6))]);
            end
          end % if ... else ... missed inj

          % glitch cut 
          gcut=injections(inj).start.ifo1+ ...
               injections(inj).glitchCut.time{:};
          results(idx,12)=sum(gcut>results(offset+2)&gcut<results(idx,3));
          
      end % loop over inj
    end

    clear data injection;


    missed     = find(results(:,6)==0);
    last_alpha = find(results(:,1)==alpha);
    dq         = find(results(:,11)>0 & results(:,3)>0);
    gcut       = find(results(:,12)~=0);
    color      = colormap(jet(nb_of_alphas));
    
        


    %% SUSPECT TRIGGER 
    % --------------------------------------------------------------------
    % get triggers that are much loud wrt to injected SNR
    % -> probably bad association 
    %
    found_suspect=find(results(:,3)>0 & results(:,8)<40 & results(:,6)>33);    
    for i=1:size(found_suspect,1)
      logfile(['Suspect ' num2str(results(found_suspect(i),2)) ...
               ' alpha= ' ...
               num2str(results(found_suspect(i),1)) ' SNRnet= ' ...
               num2str(results(found_suspect(i),7)) '(PSD ' ...
               num2str(results(found_suspect(i),8)) ')' ...
               ' H1 SNR:' num2str(results(found_suspect(i),9)) ...
               ' L1 SNR:' num2str(results(found_suspect(i),10)) ...
               ' reco SNR: ' num2str(results(found_suspect(i),6))]);
    end
    close all 

    figure
    plot(results(:,2),results(:,8),'+');
    hold all
    plot(results(missed,2),results(missed,8),'+r');
    plot(results(dq,2),results(dq,8),'xg');
    plot(results(gcut,2),results(gcut,8),'ysq');  
    hold off
    
    set(gca,'YScale','log');
    try
        set(gca,'XLim',[min(results(results(:,2)~=0,2))-100000 max(results(:,2))+100000]);
    catch
    end

    leg={'Found injections'};
    if length(missed)>0; leg=[leg 'Missed'];   end
    if length(dq)>0;     leg=[leg 'Vetoed'];   end
    if length(gcut)>0;   leg=[leg 'glitchCut'];end

    legend(leg,'Location','Best');

    xlabel('GPS','fontsize',15)
    ylabel('Injection network SNR','fontsize',15)  
    title(wvf.legend,'Fontsize',15)
    make_png([figures_folder '/' wvf.group '/' wvf.id], 'missedfound');
    
    % SNR reconstruction vs matched filtering SNR 
    distance    = zeros(1,nb_of_alphas);
    ave         = zeros(1,nb_of_alphas);
    sigma       = zeros(1,nb_of_alphas);  
    ave2        = zeros(1,nb_of_alphas);
    sigma2      = zeros(1,nb_of_alphas);
    recosnr_ave = zeros(1,nb_of_alphas);
    simusnr_ave = zeros(1,nb_of_alphas);  

    for al=1:nb_of_alphas %% loop over alpha factors
      if strcmp(wvf.xlabel,'distance')
        distance(1,al)=wvf.distance/sqrt(wvf.alphas(al));
        xlabel_text='Distance [Mpc]';
      else
        distance(1,al)=wvf.hrss*sqrt(wvf.alphas(al));
        xlabel_text='hrss';      
      end

      ext = find(results(:,1)==wvf.alphas(al) & isnan(results(:,7))==0);

      ave(1,al)         = mean(results(ext,6)./results(ext,8));
      sigma(1,al)       = std(results(ext,6)./results(ext,8));
      ave2(1,al)        = mean(results(ext,6)./results(ext,7));
      sigma2(1,al)      = std(results(ext,6)./results(ext,7));
      recosnr_ave(1,al) = mean(results(ext,6));
      simusnr_ave(1,al) = mean(results(ext,8));    
    end
    
    figure
    hold on
    errorbar(distance,ave,sigma,'LineWidth', 2, 'Color','g');
    errorbar(distance,ave2,sigma2,'LineWidth', 2, 'Color','r');
    hold off

    leg=legend('Real data PSD', 'iLIGO PSD');
    set(leg,'Fontsize', 15)

    set(gca,'XScale','log')
    set(gca,'XMinorGrid','on');
    set(gca,'YMinorGrid','on');
    
    if strcmp(wvf.xlabel,'distance')
      X=wvf.distance/sqrt(results(:,2));
    else
      X=wvf.hrss*sqrt(results(:,2));
    end
    xmin=10^(log10(min(X))-0.1*(log10(max(X))-log10(min(X))));
    xmax=10^(log10(max(X))+0.1*(log10(max(X))-log10(min(X))));
      
    set(gca,'YLim',[0 max(1,max(max(ave(1,:)+sigma(1,:),max(ave2(1,:)+sigma2(1,:)))))]);
    try
        set(gca,'XLim',[xmin xmax]);
    catch
    end

    xlabel(xlabel_text,'Fontsize',15);
    ylabel('Stampas amplitude SNR / sqrt(SNR_{IFO1}^2+SNR_{IFO2}^2)', 'Fontsize',15);
    title(wvf.legend,'Fontsize',15)

    make_png([figures_folder '/' wvf.group '/' wvf.id], 'ratioSNR_vs_distance');

    
    figure
    hold on
    errorbar(simusnr_ave,ave,sigma,'LineWidth', 2, 'Color','g');
    errorbar(simusnr_ave,ave2,sigma2,'LineWidth', 2, 'Color','r');
    hold off

    leg=legend('Real data PSD', 'iLIGO PSD');
    set(leg,'Fontsize', 15)

    set(gca,'xscale','log')
    set(gca,'XMinorGrid','on');
    set(gca,'YMinorGrid','on');

    try
    set(gca,'YLim',[0 max(max(ave(1,:)+sigma(1,:),max(ave2(1,:)+sigma2(1,:))))]);
    catch
    end

    set(gca,'XLim',[1 20000]);

    xlabel('<Simu. SNR>','Fontsize',15);
    ylabel('Stampas amplitude SNR / sqrt(SNR_{IFO1}^2+SNR_{IFO2}^2)', 'Fontsize',15);
    title(wvf.legend,'Fontsize',15)

    make_png([figures_folder '/' wvf.group '/' wvf.id],'ratioSNR_vs_simuSNR');    
  end

  waitbar('missedfound [done]');
  logfile('missedfound done without errors');
  
  delete(waitbar);
  delete(logfile);

end
