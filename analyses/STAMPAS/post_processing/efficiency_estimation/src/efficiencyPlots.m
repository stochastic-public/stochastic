function efficiencyPlots(searchPath)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This MATLAB script plots efficiency curves from pre-existing
% efficiency files created by the efficiencyCalc.m script.
%
% Input:
% - Plots: you can put it either a list of the files you
%   which to use for the plotting, or if they are part of the same
%   injection study, you can directly put the (fileName) you gave
%   them. The script will use the (fileName).list file to look for
%   all the efficiency files.
% - X_full: the values of the X axis, energy, distance, alpha factor, in 
%   the order given by efficiencyCalc.m. All given for a single alpha
%   factor (use X_part if you need to repeat several times the same
%           list)
% - Label: label of the X axis
% - Units: units of this label
% - Title: title of the plot
% - logScale: if set to 1, the X axis will use a log scale
% - graphName: neame of the output graph
% - interp: if set to true, the script will return for each curve
%   the X value associated with a 50% efficiency when it exists.
%
% Legend has to be changed by the user at the end of the file
%
% M. A. Bizouard (mabizoua@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  import classes.waveforms.dictionary
  import classes.utils.waitbar
  import classes.utils.logfile


  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  % 
  % get waveform dictionary
  % create waitbar & logfile
  %

  % 
  % Get waveform dictionary
  %---------------------------------------------------------------------
  % /!\  load first the dictionary because it use java library that
  % clean all singelton class -> instancied waitbar & logtfile will
  % be delete   
  %
  dic = dictionary.parse([searchPath '/waveforms.txt']);


  waitbar(sprintf('efficiency plot [init]'));

  logfile([searchPath '/logfiles/efficiencyPlots.log']);
  logfile('efficiency Plots begin ...');

  %---------------------------------------------------------------------
  %% MAIN 
  %---------------------------------------------------------------------
  % 
  % loop over the group then loop over the wvf 
  % for each waveform plot the efficiency for various cut 
  % for each group compare the wvf efficiency
  % 
  step=0;  
  for grp=dic.group
    close all;
    for wvf=dic.headers(grp)'
      step=step+1;
      waitbar(sprintf('efficiency plot [%3.0f]',100*step/dic.nbWvf));
      load([searchPath '/figures/eff_' wvf.id '.mat']);

      
      % 
      % cut efficiency plot
      %------------------------------------------------------------
      % For each cut plot the efficiency of the waveform group
      % this plot is usefl to compare the cut influence on some
      % waveform group 
      % to do this plot we assign a number to the figure and we
      % simply draw the efficiency of the corresponding cut
      % this plot is filled along the loop over the waveform 
      %

      for c=1:numel(cut)
        figure(c);
        hold all
        errorbar(eff(:,2),eff(:,2+2*c),eff(:,3+2*c),...
                 'LineWidth', 2);
        hold off
      end

      % 
      % waveform efficiency plot
      %------------------------------------------------------------
      % plot the waveform efficiency for various cut 
      % this cut is usefull to understand what are the influence of
      % the cut over on specific waveform
      %

      figure()
      hg=hggroup();
      hold all
      p=[];
      for c=1:numel(cut)
        p=[p errorbar(eff(:,2),eff(:,2*c+2),eff(:,2*c+3),'LineWidth', 2,'Parent',hg)];
        errorbar_tick(p(end),180000);
      end
      hold off
      xlabel('h_{rss} [1/sqrt(Hz)]','Fontsize',15);
      ylabel('Efficiency', 'Fontsize', 15)
      
      l=legend(p,decode(cut));
      set(l,'Location','NorthWest');
      set(l,'Visible','off')

      set(gca,'Box','on');
      set(gca,'XScale','log');

      set(gca,'XMinorGrid','on');
      set(gca,'YMinorGrid','on');
      
      set(gca,'YLim',[0 1.1]);
      xmin=10^(log10(min(eff(:,2)))-0.1*(log10(max(eff(:,2)))-log10(min(eff(:,2)))));
      xmax=10^(log10(max(eff(:,2)))+0.1*(log10(max(eff(:,2)))-log10(min(eff(:,2)))));
      set(gca,'XLim',[xmin xmax]);
      

      title(wvf.legend,'Fontsize',15);
      make_png([searchPath '/figures/' grp{:} '/' wvf.id],['eff_' ...
                          wvf.id '_hrss']);
      
      % EFF vs ENERGY
      % keep same plot but xlabel with energy
      cg=get(hg,'Children');
      for i=1:numel(cg)
        set(cg(i),'XData',eff(:,3));
      end
      xmin=10^(log10(min(eff(:,3)))-0.1*(log10(max(eff(:,3)))-log10(min(eff(:,3)))));
      xmax=10^(log10(max(eff(:,3)))+0.1*(log10(max(eff(:,3)))-log10(min(eff(:,3)))));      
      set(gca,'XLim',[xmin xmax]);
      xlabel('dE/dA [M_sun c^{2} / m^{2}]');

      make_png([searchPath '/figures/' grp{:} '/' wvf.id],['eff_' wvf.id '_energy']);

      % EFF vs Distance
      % if physical wvf, plot with distance x label
      % keep same plot but xlabel with distance 
      if strcmp(wvf.xlabel,'distance')
        cg=get(hg,'Children');
        for i=1:numel(cg)
          set(cg(i),'XData',eff(:,1));
        end
        xmin=10^(log10(min(eff(:,1)))-0.1*(log10(max(eff(:,1)))-log10(min(eff(:,1)))));
        xmax=10^(log10(max(eff(:,1)))+0.1*(log10(max(eff(:,1)))-log10(min(eff(:,1)))));      
        set(gca,'XLim',[xmin xmax]);
        xlabel(['Distance [Mpc]'])
        make_png([searchPath '/figures/' grp{:} '/' wvf.id],['eff_' wvf.id '_distance']);
        
      end        

      % trick to get legend
      set(gca,'Visible','off');
      set(hg,'Visible','off');
      set(l,'Visible','on');
      make_png([searchPath '/figures/' grp{:} '/' wvf.id],['eff_' wvf.id '_legend']);

    end  
    


    % 
    % Styling the cut efficiency plot 
    %------------------------------------------------------------
    % Came back on the cut efficiency plot  to add style over
    % label, legend and title.
    %

    for c=1:numel(cut)
      figure(c);
      
      xlabel('h_{rss} [1/sqrt{Hz}]','Fontsize',15);
      ylabel('Efficiency', 'Fontsize', 15)
      
      l=legend({dic.headers(grp).legend});
      set(l,'Location','NorthWest');
      
      set(gca,'Box','on');
      
      set(gca,'YLim',[0 1.1]);
      set(gca,'XScale','log');

      set(gca,'XMinorGrid','on');
      set(gca,'YMinorGrid','on');
      
      title(decode(cut{c}),'Fontsize',15);
      make_png([searchPath '/figures/' grp{:}],['eff_' grp{:} '_' ...
                          cut{c} '_hrss']);
      
      % EFF vs ENERGY
      % keep same plot but xlabel with energy change
      cg= get(gca,'Children');
      for i=1:numel(cg)
        set(cg(i),'XData',eff(:,3));
      end
      xmin=10^(log10(min(eff(:,3)))-0.1*(log10(max(eff(:,3)))-log10(min(eff(:,3)))));
      xmax=10^(log10(max(eff(:,3)))+0.1*(log10(max(eff(:,3)))-log10(min(eff(:,3)))));      
      set(gca,'XLim',[xmin xmax]);
      xlabel('dE/dA [M_{sun} c^{2} / m^{2}]');
      make_png([searchPath '/figures/' grp{:}],['eff_' grp{:} '_' ...
                          cut{c} '_energy']);

      % EFF vs Distance
      % if physical wvf, plot with distance x label
      % keep same plot but xlabel with distance 
      if strcmp(wvf.xlabel,'distance')
        cg=get(gca,'Children');
        for i=1:numel(cg)
          set(cg(i),'XData',eff(:,1));
        end
        xmin=10^(log10(min(eff(:,1)))-0.1*(log10(max(eff(:,1)))-log10(min(eff(:,1)))));
        xmax=10^(log10(max(eff(:,1)))+0.1*(log10(max(eff(:,1)))-log10(min(eff(:,1)))));      
        set(gca,'XLim',[xmin xmax]);
        xlabel(['Distance [Mpc]'])
        
        set(l,'Location','SouthWest');

        make_png([searchPath '/figures/' grp{:}],['eff_' grp{:} '_' ...
                            cut{c} '_distance']);
        
      end        

    end
      
  end


  %
  %---------------------------------------------------------------------
  %% CLOSE FUNCTION
  %---------------------------------------------------------------------
  %
  % display end message on waitbar & logfile
  % delete instancied singelton object and close
  %


  %% Make continuum injections plots. 
  if exist([searchPath, '/ContWaveforms']) == 7
     nbinj = get_nb_injections(searchPath);
     wvfname = grp{:};
     contEff(searchPath, wvfname, nbinj, 0.68);
  end


  logfile('efficiency done without error');
  waitbar('efficiency plot [done]');
  
  delete(logfile);
  delete(waitbar);
  
end
