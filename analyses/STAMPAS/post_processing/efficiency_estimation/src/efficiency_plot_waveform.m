function efficiency_plot_waveform(family_name, amp_variable, ind, waveform_names, colors, ...
				  legend_label, figures_folder, ...
                                  SNRCut, SNRfracCut)

ext={'all', 'DQ', 'SNRfrac', 'SNR', 'SNRfrac+SNR', 'DQ+SNRfrac', ...
     'DQ+SNRfrac+SNR'};

ext2={'all', 'DQ', ['SNRfrac<' num2str(SNRfracCut)], ['SNR>' num2str(SNRCut)], ...
      ['SNRfrac<' num2str(SNRfracCut) '+SNR>' num2str(SNRCut)], ...
      ['DQ+SNRfrac<' num2str(SNRfracCut)], ['DQ+SNRfrac<' ...
                    num2str(SNRfracCut) '+SNR>' num2str(SNRCut)]};

% 3 4   all
% 5 6   DQ
% 7 8  SNRfrac
% 9 10 SNR
% 11 12 SNRfrac+SNR
% 13 14 DQ+SNRfrac
% 15 16 DQ+SNRfrac+SNR

warning off
if strcmp(amp_variable,'distance')==1
  amp_index=1;
  %  xscale=[.1 100];
  %  xscale=[1 500];
  xlabelname='Distance (Mpc)';
else
  amp_index=2;
  %  xscale=[1e-22 2e-18];
  %  xscale=[1e-23 1e-20];
  xlabelname='hrss';
end

% 1 plot for each type (all, DQ, ...)
for type=1:6
  xscale_min=Inf;
  xscale_max=-Inf;
  display (['Plot ' char(waveform_names(ind(1))) '-->' ...
	    char(waveform_names(ind(end))) ' ' char(ext(type))])
  
  first=1;
  figure(type)
  
  leg=[];
  for i=ind(1):ind(end)
      filename=[figures_folder '/eff_' char(waveform_names(i)) '.txt'];
      data = load(filename);
      h=errorbar(data(:,amp_index),data(:,1+2*type),data(:,2+2*type), 'Color', colors(i+1-ind(1),:), 'LineWidth', 2);
      if first==1
          first=2;
          hold on
      end
      set(gca,'XScale','log');
      errorbar_tick(h,180000);
      if amp_index==1
          xscale_min=min(xscale_min,data(1,amp_index));
          xscale_max=1.2*max(xscale_max,data(end,amp_index));
      else
          xscale_min=0.8*min(xscale_min,data(1,amp_index));
          xscale_max=max(xscale_max,data(end,amp_index));
      end
  end
  hold off
  grid on
  if amp_index==2
    l=legend(legend_label);
    set(l,'Location','NorthWest');
  else
    l=legend(legend_label);
    set(l,'Location','NorthEast');
  end
  if xscale_min<xscale_max
    xscale=[xscale_min xscale_max];
  else
    xscale=[xscale_max xscale_min];
  end
  xlim(xscale);
  ylim([0 1.1])
  xlabel(xlabelname, 'Fontsize', 15);
  ylabel('Efficiency', 'Fontsize', 15);
  
  if type==4 | type ==6
    title_name=[char(ext(type)) '>' num2str(SNRCut)];
    title(title_name,'Fontsize',15);
  else
    title(char(ext(type)),'Fontsize',15);
  end
  
  make_png(figures_folder, ['eff_' family_name '_' char(ext(type))])
end


% 1 plot for each injection
for i=ind(1):ind(end)
  display (['Plot ' char(waveform_names(i))]);
  first=1;
  leg=[];
  figure(10+i)
  filename=[figures_folder '/eff_' char(waveform_names(i)) '.txt'];
  data = load(filename);
  for type=1:6
    leg=[leg; ext2(type)];
    h=errorbar(data(:,amp_index),data(:,1+2*type),data(:,2+2*type), 'Color', colors(type,:), 'LineWidth', 2);
    set(gca,'XScale','log');
    errorbar_tick(h,180000)
    if first==1
      first=2;
      hold on
    end
  end
 
  if amp_index==1
      xscale_min=min(xscale_min,data(1,amp_index));
      xscale_max=1.2*max(xscale_max,data(end,amp_index));
  else
      xscale_min=0.8*min(xscale_min,data(1,amp_index));
      xscale_max=max(xscale_max,data(end,amp_index));
  end  
  hold off
  
  grid on
  if amp_index==2
    l=legend(leg);
    set(l,'Location','NorthWest');
  else
    l=legend(leg);
    set(l,'Location','SouthWest');
  end

  if xscale_min<xscale_max
    xscale=[xscale_min xscale_max];
  else
    xscale=[xscale_max xscale_min];
  end 
  xlim(xscale)
  ylim([0 1.1])
  xlabel(xlabelname, 'Fontsize', 15)
  ylabel('Efficiency', 'Fontsize', 15)
  
  str_name=strrep(char(waveform_names(i)),'.dat','');
  str_name=strrep(str_name,'_tapered','');  
  str_name=strrep(str_name,'_','');
  title(str_name,'Fontsize',15);
  make_png(figures_folder, ['eff_' char(waveform_names(i))])


  figure(100+i)
  h=errorbar(data(:,amp_index),data(:,1+2*5),data(:,2+2*5), 'Color', colors(5,:), 'LineWidth', 2);
  xlim(xscale)
  xlabel(xlabelname, 'Fontsize', 15)
  ylabel('Efficiency', 'Fontsize', 15)
  title(str_name,'Fontsize',15);
  set(gca,'XScale','log');
  errorbar_tick(h,180000)
  make_png(figures_folder, ['eff_all_' char(waveform_names(i))])
  
end


return
