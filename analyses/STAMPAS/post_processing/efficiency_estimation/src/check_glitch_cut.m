function rc=check_glitch_cut(searchPath,injName,injections)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% The function checks if a column contained during the time of an
% injection has been fired for any sky position. It may happen that
% the glitch cut was activated for a given sky position but not all
% In this case, the reason why the signal is not seen may be not
% the glitch cut
%
% Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

inj_nb=size(injections,1);
rc=zeros(inj_nb,1);

%display(['missed injections: ' num2str(inj_nb)])
filename=['../../' searchPath '/results/glitchCut_merged/glitchCut_' injName '.txt'];
if exist(filename,'file')==0
    return
end
gcut=readtext(filename, '[,\t]', '%', '', 'textual');
n=size(gcut,1);
gcut_times=zeros(n,1);

for i=1:n
    a=cell2mat(cellfun(@(x) str2num(x),gcut(i,1),'UniformOutput',0)) + ...
      cell2mat(cellfun(@(x) str2num(x),gcut(i,2),'UniformOutput',0));
    gcut_times(i,1)=a;
end

for i=1:inj_nb
    ext=gcut_times(:)>=injections(i,1) & gcut_times(:)<=injections(i,2);
    if sum(ext)>0
        %    display('Injection  has been glitch cut');
        rc(i,1)=1;
    end
end
