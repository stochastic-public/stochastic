function [] = RvetoDiagnostic(run,searchPath)  
% RvetoDiagnostic : plot Rveto diagnostic plot
% DESCRIPTION :
%   plot various graph to verify that Rveto is well tuned
%   
% SYNTAX :
%   RvetoDiagnostic (searchPath)
% 
% INPUT : 
%    searchPath : the dag path
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : May 2016
%

  import classes.waveforms.dictionary
  import classes.utils.waitbar

  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  % 
  % get waveform dictionary
  % create waitbar & logfile
  %


  % 
  % Get waveform dictionary 
  %---------------------------------------------------------------------
  % /!\  load first the dictionary because it use java library that
  % clean all singelton class -> instancied waitbar & logtfile will
  % be delete   
  %
  dic = dictionary.parse([searchPath '/waveforms.txt']);

  waitbar(sprintf('Rveto diagnostic plot [init]'));

  folder = [searchPath '/figures'];


  for wvf=dic.headers'
    % Get data
    %--------------------------------------------------
    % store all data in a cell
    % each element of the cell correspond to an alphas
    % to plot data we simply use the cellfun function
    %
    waitbar(sprintf('Rveto diagnostic plot [%10s] : %30s', ...
                    wvf.legend,'get data'));

    cc=colormap(jet(length(wvf.alphas)));
    for al=1:numel(wvf.alphas)
      injName=strcat(wvf.file,'_', num2str(wvf.alphas(al),'%0.10f'));
      if exist([searchPath '/results/results_merged/results_' ...
                injName  '_filtered.mat'])
        m=matfile([searchPath '/results/results_merged/results_' ...
                   injName '_filtered.mat']);

        data{al}=m.data;
        color{al}=[cc(al,:)];
	lline{al}='none';
      end
    end

    color(cellfun(@isempty,data))=[];
    data(cellfun(@isempty,data))=[];

    color(cellfun(@(x) x.numTriggers == 0,data))=[];
    data(cellfun(@(x) x.numTriggers == 0,data))=[];

    data=fliplr(data);
    color=fliplr(color);

    % Get COLORBAR TICK
    %--------------------------------------------------
    % compute the distance & hrssfor the colorbar tick
    %
    distance_max=wvf.distance/sqrt(wvf.alphas(1));
    distance_min=wvf.distance/sqrt(wvf.alphas(end));

    hrss_min=wvf.hrss*sqrt(min(wvf.alphas))*1e20;
    hrss_max=wvf.hrss*sqrt(max(wvf.alphas))*1e20;

    if strcmp(wvf.xlabel,'distance')
      % adi case (distance)
      ytick=linspace(distance_min,distance_max,length(wvf.alphas));
      ytick2=wvf.distance./sqrt(wvf.alphas);
      yticklabel=cell(1,length(wvf.alphas));
      yticklabel(:) = {''};
      for i=1:length(wvf.alphas)
        a=sprintf('%.1f',ytick2(i));
        yticklabel(1,i)={[a ' Mpc']};
      end
      if distance_min==distance_max
        distance_min=distance_min-10;
        distance_max=distance_max+10;
      end
      clim_interval=[distance_min distance_max];
    else
      % other waveforms (hrss)
      ytick=linspace(hrss_min,hrss_max,length(wvf.alphas));
      ytick2=wvf.hrss.*sqrt(wvf.alphas);
      yticklabel=cell(1,length(wvf.alphas));
      yticklabel(:)={''};
      for i=1:length(wvf.alphas)
        a=sprintf('%.3g',ytick2(i));
        yticklabel(1,i)={a};
      end
      if hrss_min==hrss_max
        hrss_min=hrss_min-10;
        hrss_max=hrss_max+10;
      end
      clim_interval=[hrss_min hrss_max];
    end


    det={'H1','L1'};

    close all

    %------------------------------------------------------------
    %% SNR DISTRIBUTION
    %------------------------------------------------------------
    %
    waitbar(sprintf('Rveto Diagnostic : %30s','SNR distribution'));

    figure
    ax=axes('CLim',clim_interval);

    hold all
%    for i=1:16
%      color{i}
%    end    
    f=@(x,c,l) plot(x.SNR('ifo1'),x.SNR('ifo2'),...
                  'Color',c, 'marker', '+', 'LineStyle', 'none');
    cellfun(f, data, color,'UniformOutput', false);

    [names,values]=textread(['../../' run '/Rveto.txt'], '%s %s\n',-1,'commentstyle','matlab');

    for ii=1:length(names)
      switch names{ii}
       case 'R1'
        R1 = str2num(values{ii});
       case 'R2'
        R2 = str2num(values{ii});
       case 'THR1'
        x1 = str2num(values{ii});
       case 'THR2'
        x2 = str2num(values{ii});
      end
    end

    THR1=10^x1;
    THR2=10^x2;

%    line ([1 1e4],[2.9 2.9e4],'Color','black')
%    line ([1 2.9e4],[0.345 1e4],'Color','black')
%    line ([1e4 1e4],[2.9e4 1e8],'Color','black')
%    line ([2.9e4 1e8],[1e4 1e4],'Color','black')

    line ([100 THR2],[100*R2 THR2*R2],'Color','black')
    line ([THR2 THR2],[R2*THR2 1e8],'Color','black')
    line ([100*R1 R1*THR1],[100 THR1],'Color','black')
    line ([R1*THR1 1e8],[THR1 THR1],'Color','black')

    hold off

    xlabel('SNR_{ifo1}')
    ylabel('SNR_{ifo2}')
    title('SNR distribution')

    set(gca(),'YScale','log')
    set(gca(),'XScale','log')
    set(gca(),'XLim',[1e2 1e8])
    set(gca(),'YLim',[1e2 1e8])
    set(gca(),'XMinorGrid','on');
    set(gca(),'YMinorGrid','on');

    cc = colorbar('YTick',ytick,'YTickLabel',yticklabel);
    if verLessThan('matlab','8.2')
      set(get(cc,'Ylabel'),'String','Distance');
    else
      set(get(cc,'Label'),'String','Distance');
    end
    set(get(cc,'Title'),'FontSize',15);
    
    make_png([folder '/' wvf.group '/' wvf.id],['RvetoSNR']);
    


    %------------------------------------------------------------
    %% SNRratio time distribution
    %------------------------------------------------------------
    %
    waitbar(sprintf('Rveto Diagnostic : %30s','SNRratio vs time'));

    for ifo=1:2
      figure
      ax=axes('CLim',clim_interval);
      hold all
      f=@(x,c) plot(x.GPSstart(['ifo' num2str(ifo)]),x.SNR('ifo1')./ ...
                    x.SNR('ifo2'),'Color', c, 'marker', '+', ...
		    'LineStyle', 'none');
      cellfun(f, data, color,'UniformOutput', false);
      hold off

      xlabel('GPS time [s]')
      ylabel('SNR_{ifo1}/SNR_{ifo2}')
      title('SNRratio time distribution')

      set(gca(),'XMinorGrid','on');
      set(gca(),'YGrid','on');
      set(gca(),'YMinorGrid','on');
      set(gca(),'YScale','log')
      set(gca(),'YLim',[1e-6 1e6])

      cc = colorbar('YTick',ytick,'YTickLabel',yticklabel);
      if verLessThan('matlab','8.2')
        set(get(cc,'Ylabel'),'String','Distance');
      else
        set(get(cc,'Label'),'String','Distance');
      end
      set(get(cc,'Title'),'FontSize',15);

      make_png([folder '/' wvf.group '/' wvf.id],['RvetoSNRratioGPS' det{ifo}]);
    end


    %------------------------------------------------------------
    %% Nb Triggers Rveto cut
    %------------------------------------------------------------
    % 
    waitbar(sprintf('Rveto Diagnostic : %30s','SNRratio distribution'));

    Rmax=max(cellfun(@(x) max(x.SNR('ifo1')./x.SNR('ifo2')),data));
    Rmin=min(cellfun(@(x) min(x.SNR('ifo1')./x.SNR('ifo2')),data));

    Rx=logspace(log10(0.9*Rmin),log10(1.1*Rmax),100);

    f = @(x) hist(x.SNR('ifo1')./x.SNR('ifo2'),Rx);
    [n] = cellfun(f, data, 'UniformOutput', false);

    Y = max(cellfun(@max,n));
    figure
    axis([log10(Rmin) log10(Rmax) 0 1.1*log10(Y)]);
    ax = gca();
    hold all
    for al=1:size(n,2)
      hold on
      s=stairs(log10(Rx),log10(n{al}));
      set (s, 'Color', color{al});
    end    

    cc = colorbar('YTick',ytick,'YTickLabel',yticklabel);
    if verLessThan('matlab','8.2')
      set(get(cc,'Ylabel'),'String','Distance');
    else
      set(get(cc,'Label'),'String','Distance');
    end

    axis off
    ax1 = axes('position', get(ax, 'position'), ...
               'color', 'none');

    xlabel('SNR_{ifo1}/SNR_{ifo2} cut [s]')
    ylabel('#')
    title('Histogram of the ratio')

    set(get(cc,'Title'),'FontSize',15);
    
    set(ax1,'YLim',[0, 1.1*max(Y)]);
    set(ax1,'XLim',[Rmin, Rmax]);
    set(ax1,'Box','on');
    set(ax1,'YScale','log');
    set(ax1,'XScale','log');
    set(ax1,'XMinorGrid','on');
    set(ax1,'YMinorGrid','on');

    make_png([folder '/' wvf.group '/' wvf.id],['RvetoHist']);


    %------------------------------------------------------------
    %% Nb Triggers Rveto cut for each windows 
    %------------------------------------------------------------
    %
    waitbar(sprintf('Rveto Diagnostic : %30s',['SNRratio distribution by windows ']));

    R  = cell2mat(cellfun(@(x) x.SNR('ifo1')'./x.SNR('ifo2')',data,'UniformOutput',false));
    ww = cell2mat(cellfun(@(x) x.windows.idx',data,'UniformOutput',false));
    X  = unique(cell2mat(cellfun(@(x) x.windows.start.ifo1',data,'UniformOutput',false)));

    Rmax=max(R);
    Rmin=min(R);
    
    Rx=logspace(log10(0.9*Rmin),1.1*log10(0.9*Rmax),101);
    wid=unique(ww);
    
    MAP=zeros(length(Rx),length(wid));

    for i=1:length(ww)
      [~,x]=min(abs(ww(i)-wid));
      if ww(i)-wid<0;x=x-1;end

      [~,y]=min(abs(R(i)-Rx));
      if R(i)-Rx<0;y=y-1;end
      
      MAP(y,x)=MAP(y,x)+1;    
    end

    figure;
    surf(X,Rx,zeros(size(MAP)),'CData',log10(MAP), 'Linestyle','none');
    view(0,90);
    colorbar

    set(gca(),'Box','on');
    set(gca(),'YLim',[min(Rx) max(Rx)]);
    set(gca(),'XLim',[min(X) max(X)]);
    set(gca(),'YScale','log');
    xlabel('GPS time [s]')
    ylabel('SNR_{ifo1}/SNR_{ifo2}')
    title('SNR ratio triggers distribution by windows')

    make_png([folder '/' wvf.group '/'  wvf.id],['RvetoHist_Win']);
  end
  
  %------------------------------------------------------------
  %% CLOSE FUNCTION
  %------------------------------------------------------------
  %
  waitbar(sprintf('Rveto Diagnostic [done] %30s',''));
  delete(waitbar);

end





