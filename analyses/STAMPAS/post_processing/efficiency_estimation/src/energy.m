function egw = energy(hrss,r,f)  
% energy :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = energy ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : MAB
% CONTACT : mabizoua@lal.in2p3.fr
% VERSION :
% DATE : 
%

  % hrss in 1/sqrtHz
  % r in pc
  % f in Hz

  msun=1.988e30;        % kg
  msunc2=msun*(3e8)^2;  % kg m^2 s^-2
  c=2.99e8;             % m s^-1
  G=6.67e-11;           % kg m^3 s^-2

  egw=(hrss*r*3.08e16*pi*f)^2;
  egw=egw*c^3/G;
  egw=egw/msunc2;

end

