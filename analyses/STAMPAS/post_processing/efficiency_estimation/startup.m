% Designed to set up the matlab environment for running the
% STAMP AS postprocessing functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath('./src');
addpath('./src/html');
addpath('../../functions')
addpath('../..')
addpath('../')
addpath(genpath('../../../../stamp2'));

[no_use,h] = system('hostname -d');
h = h(1:(end-1)); % remove endline
  
if (strcmpi(h,'ligo.caltech.edu') | strcmpi(h,'ligo-wa.caltech.edu') | ...
    strcmpi(h,'ligo-la.caltech.edu'))
  addpath(genpath('/usr/share/ligotools/matlab/'));
elseif strcmpi(h,'atlas.aei.uni-hannover.de')
  addpath('/opt/lscsoft/ligotools/matlab');
end

