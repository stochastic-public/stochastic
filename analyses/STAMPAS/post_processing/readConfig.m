function config = readConfig(file)  
% readConfig :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = readConfig ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
  fid=fopen(file,'r');
  l=fgetl(fid);
  
  while ischar(l)
    if regexp(l,'^%','match','once')
      l=fgetl(fid);
      continue
    end
    
    [kv]=strsplit(l);
    kv(cellfun(@isempty,kv))=[];
    if length(kv)==2
      if regexp(kv{2},'\D*','once')
        config.(kv{1})=kv{2};        
      else
        config.(kv{1})=str2num(kv{2});
      end
    end
    l=fgetl(fid);
  end
  
end
