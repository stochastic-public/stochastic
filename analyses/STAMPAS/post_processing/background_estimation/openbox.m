%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% openbox : launch all openbox post-processing step that analyse
% triggers of Zero Lag and report results as a HTML page
%
% DESCRIPTION :
%   plot several distribution and diagnostic figure for various
%   cut. compute Far and generated HTML page.
% 
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Avr 2016
%

import classes.utils.waitbar
import classes.utils.logfile

%------------------------------------------------------------------
%% DEFINE CUT
%------------------------------------------------------------------
% get the cut using the variable definie
cut={'noCut'};

if doTFveto
  cut=[cut, 'TFveto'];
end

if doRveto
  cut=[cut, 'Rveto'];
end

if ~isempty(SNRfracCut)
  cut=[cut, ['SNRfrac:' num2str(SNRfracCut)]];
end

if doDQFlags
  cut=[cut, 'DQFlags'];
end


%------------------------------------------------------------------
%% INIT
%------------------------------------------------------------------
% 
if ~exist([searchPath '/figures'])
  system(['mkdir -p ' searchPath '/figures']);
end

config = readConfig([searchPath '/config_bkg.txt']);
start  = str2num(regexprep(searchPath, '.*Start-([0-9]*)_.*','$1'));
stop   = str2num(regexprep(searchPath, '.*Stop-([0-9]*)_.*','$1'));

config.searchPath=searchPath;

%------------------------------------------------------------------
%% get data
%------------------------------------------------------------------
if ~exist([config.searchPath '/results/results_merged/results_filtered.mat'])
  error(['file : ' config.searchPath '/results/results_merged/' ...
         'results_filtered.mat not found']);
end
load([config.searchPath '/results/results_merged/results_filtered.mat']);

%------------------------------------------------------------------
%% MAIN
%------------------------------------------------------------------
%

%% DIAGNOSTIC PLOTS
%------------------------------------------------------------------
%
fprintf('\nopenbox [diagnostic]\n');
diagnostic(config.searchPath,data,'Cut',cut);

%% FAR PLOTS
%------------------------------------------------------------------
%
if config.doZebra
  fprintf('openbox [FAR]\n');
  FAR(config.searchPath,data,'Cut',cut,'MCpath',mcPath);

  fprintf('openbox [FAR plots]');
  FARplots_openbox([config.searchPath '/figures'],cut,bkgPath,mcPath);
end

if config.doLonetrack
  %% jobs
  jobs=load([config.searchPath '/tmp/joblistTrue.txt']);
  windows_nb=length(jobs);

  %% Lonetrack data
  %------------------------------------------------------------------
  lonetrackPath = [config.searchPath '/Lonetrack'];
  trigger_filename=[lonetrackPath '/bknd/stage1_triggers.mat'];    
  snrhisto_filename=[lonetrackPath '/bknd/stage1_far.mat'];
  filename=[lonetrackPath '/bknd/stage1.mat'];

  if ~exist(trigger_filename)
    error([trigger_filename ' not found. Please run extract.sh first'])
  else
    fprintf('\nLoading already generated merged files ...\n');
    load(trigger_filename);
    load(snrhisto_filename);
    %load(filename);
  end
  fprintf('openbox [Lonetrack FAR]\n');

  ZLTotTime=load([config.searchPath '/tmp/TTimeZeroLag.txt']);
  pproc_FAR(snrmax_zero_histo,edges,ZLTotTime,ZLTotTime,[config.searchPath '/figures/'])

  fprintf('openbox [Lonetrack FAR plots]');  
  pproc_FARplots_openbox([config.searchPath '/figures'],cut,bkgPath,mcPath,ZLTotTime);  
end

%% LOUDEST
%------------------------------------------------------------------
% comput the loudest cut : cut apply in the list of loudest
% only TFveto & Rveto & SNRfrac & DQflags if exist
%

lcut={'noCut'};
if sum(strcmp(cut,'TFveto'))
  lcut=[lcut,'TFveto'];
end
if sum(strcmp(cut,'Rveto'))
  lcut=[lcut,'Rveto'];
end
if ~isempty(SNRfracCut)
  lcut=[lcut, ['SNRfrac:' num2str(SNRfracCut)]];
end
if doDQFlags
  lcut=[lcut, 'DQFlags'];
end

if config.doLonetrack
  jobs=load([config.searchPath '/tmp/joblistTrue.txt']);
  ind = find(pass_zero);
  lonetrack_get_loudest_ZL([config.searchPath '/figures/loudest.txt'],...
                           snrmax_zero(ind),AP,snrtimes_zero(ind),...
                           timedelay_zero(ind),data,id,lag,jobs,10,0,...
                           load([bkgPath '/figures/pproc_FAR_clean.mat']),...
                           ZLTotTime);
else
  far=load([bkgPath '/figures/FAR_clean.mat']);
  get_loudest_ZL(config.searchPath,data,far,'Cut',lcut);
end

if config.doZebra
  %% CORRELOGRAM PLOTS
  %------------------------------------------------------------------
  %
  correlogram(config.searchPath,data,[config.searchPath '/figures'])
  
  %% WINDOWS OVERLAP PLOTS
  %------------------------------------------------------------------
  %
  window_overlap(config.searchPath,data,'Folder',[config.searchPath '/figures'],...
  		 'Overlap',config.overlap,'Size',config.window);
  
  
  %% TIMEBET PLOTS
  %------------------------------------------------------------------
  %
  timebet(config.searchPath,data,'Folder',[config.searchPath '/figures'])
  
  %% Rveto DIAGNOSTIC PLOTS
  %------------------------------------------------------------------
  % only if Rveto is used
  if sum(cell2mat(strfind(cut,'Rveto')))
    RvetoDiagnostic(data,[config.searchPath '/figures/'],searchPath);
  end
 
  %% SNRfrac DIAGNOSTIC PLOTS
  %------------------------------------------------------------------
  % only if SNRfrac is used
  if sum(cell2mat(strfind(cut,'SNRfrac')))
    SNRfracDiagnostic(data,[config.searchPath '/figures/']);
  end
end  

%% PSD
%------------------------------------------------------------------
fprintf('\nPSD estimation\n');
PSDplots([config.searchPath '/tmp/jobfile_bknd.txt'],...
         'Start',start,...
        'Stop',stop,...
         'HPFreq',22,...
         'Fmin',config.fmin,...
         'Fmax',config.fmax,...
         'Folder',[config.searchPath '/figures']);


%% GENERATE HTML WEB PAGE
%------------------------------------------------------------------
%
generate_html(config.searchPath,'Cut',cut,'MC',mcPath,'BKG',bkgPath,'WEB',webPath);

exit
