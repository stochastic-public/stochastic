#!/usr/bin/python

import os, sys, glob, optparse, warnings, numpy, datetime, time, matplotlib, math
from subprocess import Popen, PIPE, STDOUT
import numpy as np


# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def parse_commandline():
    """
    Parse the options given on the command-line.
    """
    parser = optparse.OptionParser(usage=__doc__)

    parser.add_option("-i", "--input", help="input file", default ="dq.txt")

    opts, args = parser.parse_args()

    return opts



# =============================================================================
#
#                                    MAIN
#
# =============================================================================

if __name__=="__main__":

    warnings.filterwarnings("ignore")

    # Parse command line
    opts = parse_commandline()

    dqFile = opts.input
    entries = [line.strip() for line in open(dqFile)]
    firstlines=entries.pop(0)
    firstlines=entries.pop(0)
    firstlines=entries.pop(0)

    s=0
    for line in entries:
        if s==1:
            name=line
            s=s+2
        if s==2:
            active=line.replace("segment(","").replace(")","")
            s=s+2
        if line == "Flag name:":
            s=1
        if line == "Active segments:":
            s=2
        if s==4:
            print '{0:40}    {1:25}'.format(name, active)
            

