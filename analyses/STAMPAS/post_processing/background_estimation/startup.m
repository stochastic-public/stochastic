% Designed to set up the matlab environment for running the
% STAMP AS postprocessing functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------
%% set path 
%-----------------------------------------------------------------
%

addpath('./src');
addpath('./src/html');
addpath('./src/lonetrack');
addpath('../');
addpath('../target');
addpath('../../');
addpath('../../functions');
addpath(genpath('../../../../stamp2'));

[no_use,h] = system('hostname -d');
h = h(1:(end-1)); % remove endline
  
if (strcmpi(h,'ligo.caltech.edu') | strcmpi(h,'ligo-wa.caltech.edu') | ...
    strcmpi(h,'ligo-la.caltech.edu'))
  addpath(genpath('/usr/share/ligotools/matlab/'));
elseif strcmpi(h,'atlas.local')
  addpath('/opt/lscsoft/ligotools/matlab');
end

%-----------------------------------------------------------------
%% set root graphics default style 
%-----------------------------------------------------------------
%

set(0,'defaultAxesFontName','Helvetica');
set(0,'defaultAxesFontSize',15);
set(0,'defaultAxesBox','on');
