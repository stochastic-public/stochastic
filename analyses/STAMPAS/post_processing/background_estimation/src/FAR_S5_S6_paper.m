%function FAR_S5_S6_paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Macro to generate S5+S6 zerolag plot for the s5 paper
%
% M.-A. Bizouard (mabizoua@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
verbose=1;
run1='S5';
run2='S6';
ID_ZL1=100;
ID_ZL2=100;
ID_BKG1=101;
ID_BKG2=101;
ID_mc=4;
mc=1;
addpath('../../../stamp2/src/misc/ezyfit')
figures_folder=['figures_paper'];

x1=load(['figures_' run1 '_ID' num2str(ID_BKG1) '/FAR_SNRfrac.txt']);
x2=load(['figures_' run1 '_ID' num2str(ID_ZL1) '/FAR_SNRfrac.txt']);
x3=load(['figures_' run2 '_ID' num2str(ID_BKG2) '/FAR_SNRfrac.txt']);
x4=load(['figures_' run2 '_ID' num2str(ID_ZL2) '/FAR_SNRfrac.txt']);

if mc==1
  x5=load(['figures_' run1 '_ID' num2str(ID_mc) '/FAR_SNRfrac.txt']);
  if verbose==1
    display(['MC:' num2str(x5(1,2))]);
  end
end

fig=figure();
ax1=axes('Parent', fig, ...
	 'XScale', 'lin', ...
	 'YScale', 'log', ...	 
	 'XMinorTick','on',...
	 'Layer','top');
% xlim does not work properly with -nodisplay option
xmin=22;      
x1=x1(x1(:,1)>xmin,:);
x2=x2(x2(:,1)>xmin,:);
x3=x3(x3(:,1)>xmin,:);
x4=x4(x4(:,1)>xmin,:);
xlim([xmin 34]);
ylim([1e-10 4e-5]);

png_name = ['S5_S6_ZL_FAR'];

grid(ax1,'on')
hold(ax1,'all')

%set(gcf, 'Renderer', 'zbuffer');

plot(x1(:,1),x1(:,2),'LineStyle','-','color','k');
plot(x3(:,1),x3(:,2),'LineStyle','--','color',[1 0 0]);

dx=x1(2,1)-x1(1,1);

if mc==1
  plot(x5(:,1)-dx/2,x5(:,2), 'Color', [0 1 1],'LineStyle','-','LineWidth',2);
end

x2=flipud(x2);
[c,ia,ic]=unique(x2(:,2));
x2=x2(ia,:);
x2=flipud(x2);
plot(x2(:,1)-dx/2,x2(:,2),'Marker','o',...
     'MarkerFaceColor',[0 0 0],...
     'MarkerEdgeColor',[0 0 0],'MarkerSize',6,...
     'LineStyle','none');

x4=flipud(x4);
[c,ia,ic]=unique(x4(:,2));
x4=x4(ia,:);
x4=flipud(x4);
plot(x4(:,1)-dx/2,x4(:,2),'Marker','^',...
     'MarkerFaceColor',[1 0 0],...
     'MarkerEdgeColor',[1 0 0],'MarkerSize',7,...
     'LineStyle','none');

plot(x1(:,1),x1(:,2),'LineStyle','-','color','k','LineWidth',2);
plot(x3(:,1),x3(:,2),'LineStyle','--','color',[1 0 0],'LineWidth',2);

box

xlabel('SNR_{\Gamma}','fontsize',15);
ylabel('False alarm rate [Hz]','fontsize',15);
hold off



if mc==0
  leg=legend('S5 background', 'S6 background',...
	     'S5 triggers', 'S6 triggers');
else
  leg=legend('S5 background', 'S6 background', 'Gaussian noise',...
	     'S5 triggers','S6 triggers');
end

set(leg,'FontSize', 13)
  


