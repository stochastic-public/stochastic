function plot_rveto(run)

[names,values]=textread(['../../' run '/Rveto.txt'], '%s %s\n',-1,'commentstyle','matlab');

for ii=1:length(names)
  switch names{ii}
   case 'R1'
    R1 = str2num(values{ii});
   case 'R2'
    R2 = str2num(values{ii});
   case 'THR1'
    x1 = str2num(values{ii});
   case 'THR2'
    x2 = str2num(values{ii});
  end
end

THR1=10^x1;
THR2=10^x2;

%    line ([1 1e4],[2.9 2.9e4],'Color','black')
%    line ([1 2.9e4],[0.345 1e4],'Color','black')
%    line ([1e4 1e4],[2.9e4 1e8],'Color','black')
%    line ([2.9e4 1e8],[1e4 1e4],'Color','black')

line ([100 THR2],[100*R2 THR2*R2],'Color','black')
line ([THR2 THR2],[R2*THR2 1e8],'Color','green')
line ([100*R1 R1*THR1],[100 THR1],'Color','red')
line ([R1*THR1 1e8],[THR1 THR1],'Color','blue')

end
