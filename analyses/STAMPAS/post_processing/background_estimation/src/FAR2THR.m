function [THR] = FAR2THR(FARPath,FAR)  
% FAR2THR : get the SNR correspondant to a FAR wrt a specific
% distribution
%
% DESCRIPTION :
%   get the clean FAR and try to estimate the SNR of a specific
%   _FAR value
%
% SYNTAX :
%   _[THR] = FAR2THR (FARPath,FAR)
% 
% INPUT : 
%    FARPath : path to the FAR dirstribution file
%    FAR: calue of the FAR [1/yr]
%
% OUTPUT :
%   THR: SNR correspondant to the FAR 

% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : APR 2017
%
  FAR    = FAR/(365*24*3600); % [s]
  
  D=load(FARPath);

  idx=find(D.far<FAR & D.far>0);

  if isempty(idx)
    THR=NaN;
    return
  end

  y1 = D.far(idx(1)-1);
  y2 = D.far(idx(1));
  x1 = D.snr(idx(1)-1);
  x2 = D.snr(idx(1));

  THR=(FAR-y1)/(y2-y1)*(x2-x1)+x1
end

