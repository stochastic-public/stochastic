function myh = algo_correlogram2 (evl1, deltat, binnb)

myh=zeros(binnb+1,1);
ev1nb=length(evl1);
delta = deltat*binnb + 1;

if ~issorted(evl1)
    evl1=sort(evl1);
end

for (i=1:ev1nb-1)
    if (mod(i,100000)==0)
        disp(['rate ' num2str(i) '/' num2str(ev1nb)]);
    end
    
    dist=[];
    for (j=i+1:ev1nb)
        dt=evl1(j)-evl1(i);
        if dt<delta
            dist = [dist; dt];
        else
            myh = addToH(dist,myh,deltat);
            break;
        end
    end
    if j==ev1nb
        myh = addToH(dist,myh,deltat);
    end
end


function his = addToH(distance,myhis,dtime)
hnb = length(myhis);
idx=ceil(distance/dtime)+1;
his=myhis;
if (idx<=hnb)
    his(idx) = his(idx)+1;
end
