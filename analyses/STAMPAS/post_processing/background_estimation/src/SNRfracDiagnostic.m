function SNRfracDiagnostic(data,folder)  
% SNrfracDiagnostic :
% DESCRIPTION :
% 
% SYNTAX :
%   SNrfracDiagnostic(data, folder)
% 
% INPUT : 
%    data : STAMPAS results object
%    folder : figure folder
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : May 2016
%

  import classes.utils.waitbar

  waitbar('SNRfrac Diagnostic [init]');

  data.sort('SNR.coherent','ascend');
  
  %------------------------------------------------------------
  %% SNR-SNRfrac scatter 
  %------------------------------------------------------------
  %
  waitbar(sprintf('SNRfrac Diagnostic : %30s','SNR-SNRfrac scatter'));

  figure
  scatter(data.SNRfrac('ifo1'),data.SNRfrac('ifo2'),[],min(data.SNR,50),'+');

  xlabel('SNRfrac_{ifo1}')
  ylabel('SNRfrac_{ifo2}')
  title('SNRfrac distribution')

  set(gca(),'XLim',[0 1])
  set(gca(),'YLim',[0 1])
  set(gca(),'CLim',[0 50]);
  set(gca(),'XMinorGrid','on');
  set(gca(),'YMinorGrid','on');

  cc = colorbar();
  if verLessThan('matlab','8.2')
    set(get(cc,'Ylabel'),'String','SNR');
  else
    set(get(cc,'Label'),'String','SNR');
  end
  set(get(cc,'Title'),'FontSize',15);

  make_png([folder '/SNRfrac'],'SNR-SNRfrac_scatter');


  %------------------------------------------------------------
  %% SNRfrac-SNRfrac scatter 
  %------------------------------------------------------------
  %
  waitbar(sprintf('SNRfrac Diagnostic : %30s','SNRfrac-SNRfrac scatter'));

  figure
  scatter(data.SNRfrac('ifo1'),data.SNRfrac('ifo2'),[],data.SNRfrac,'+');

  xlabel('SNRfrac_{ifo1}')
  ylabel('SNRfrac_{ifo2}')
  title('SNRfrac distribution')

  set(gca(),'XLim',[0 1])
  set(gca(),'YLim',[0 1])
  set(gca(),'CLim',[0 1]);
  set(gca(),'XMinorGrid','on');
  set(gca(),'YMinorGrid','on');

  cc = colorbar();
  if verLessThan('matlab','8.2')
    set(get(cc,'Ylabel'),'String','SNRfrac');
  else
    set(get(cc,'Label'),'String','SNRfrac');
  end
  set(get(cc,'Title'),'FontSize',15);
  make_png([folder '/SNRfrac'],'SNRfrac-SNRfrac_scatter');

  %------------------------------------------------------------
  %% CLOSE FUNCTION
  %------------------------------------------------------------
  %
  waitbar(sprintf('SNRfrac Diagnostic [done] %30s',' '));
  delete(waitbar);

end

