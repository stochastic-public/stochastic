function pproc_FAR (snrmax_histo,edges,TotTime,TotTimeZL,folder)
% pproc_FAR : 
% DESCRIPTION :
%   compute and plot the FAR for Lonetrack triggers
%   
% SYNTAX :
%   pproc_FAR (snrmax_histo,edges,folder)
% 
% INPUT : 
%    snrmax_histo : 
%    edges :
%    TotTime :
%    folder : figures folder
%
% AUTHOR : Marie Anne Bizouaard
% CONTACT : mabizoua@lal.in2p3.fr
% VERSION : 1.0
% DATE : January 2017
%

import classes.utils.waitbar

waitbar('FAR [start]');

% Check snrmax hiist is not empty
if sum(snrmax_histo)==0
  error('snrmax_histo is empty. Please check Lonetrack files merging.');
end

N_cum=cumsum(snrmax_histo);
N_cum_max=N_cum(end);
for i=1:length(N_cum)
  N_cum_inv(i,1)=N_cum_max-N_cum(i);
end

far=N_cum_inv/TotTime;
snr=edges';

% define 5-sigma line and 4sigma line
s5 = 1/1744278/TotTimeZL;
s4 = 1/15787/TotTimeZL;
s3 = 1/370/TotTimeZL;

% Save FAR in file
save([folder '/pproc_FAR_clean.mat'],'snr','far');

% Plots

cc=flipud(colormap(gray(1+2)));
cc=cc(2:end,:); % remove the white color

figure
ax=gca();
set(ax,'XScale','log');
set(ax,'YScale','log');
set(ax,'XLim',[min(snr) 30])
set(ax,'XGrid','on');
set(ax,'YGrid','on');
set(get(ax,'XLabel'),'String','SNR');
set(get(ax,'YLabel'),'String','FAR [Hz]');
set(ax,'Box','on');
hold all

ymin=10^(log10(min(far(far~=0)))*1.1);
ymax=10^(log10(max(far))*0.9);
h1=bar(snr,far,...
       'FaceColor', cc(1,:), ...
       'EdgeColor', cc(1,:), ...
       'BarWidth', 1, ...
       'basevalue', ymin,'Parent',ax);

dx=snr(2)-snr(1);
stairs(snr-dx/2,far, 'Color', [0 0 0],'Parent',ax)

ylim([ymin ymax]);
xl = xlim();
xlim([min(snr(snr~=0)) xl(2)]);

h2 = plot([min(snr(snr~=0)) xl(2)], [s5 s5], 'k--','LineWidth',3);
plot([min(snr(snr~=0)) xl(2)], [s4 s4], 'k--','LineWidth',3);
plot([min(snr(snr~=0)) xl(2)], [s3 s3], 'k--','LineWidth',3);


legend([h1,h2],{'All bkg triggers','3,4,5-sigma'});

make_png(folder,'pproc_FAR_all');


waitbar('FAR [done ]');
fprintf('\n');
