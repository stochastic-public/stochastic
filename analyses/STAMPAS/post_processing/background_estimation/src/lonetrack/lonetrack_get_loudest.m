function [] = lonetrack_get_loudest(searchPath,File,snrmax,AP,win_idx,timedelay,id,lag,jobs,gpstime,MaxTriggers,ThSNR,far);  

% pproc_et_loudest :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = pproc_get_loudest ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Marie Anne Bizouard
% CONTACT : 
% VERSION :
% DATE : 
%

  import classes.utils.waitbar

  waitbar('loudest [init]');
  
  if MaxTriggers == -1
    MaxTriggers=10;
  end

  if ThSNR == -1
    ThSNR=0;
  end
  
  % Selection
  [snrmax_sort,index]=sort(snrmax,'descend');
  snrmax=snrmax_sort(snrmax_sort>ThSNR);
  sv=min(length(snrmax),MaxTriggers);

  win_idx   = win_idx(1,index);
  win_idx   = win_idx(1,1:sv);
  gpstime   = gpstime(1,index);
  gpstime   = gpstime(1,1:sv);
  timedelay = timedelay(1,index);
  timedelay = timedelay(1,1:sv);
  snrmax    = snrmax(1,1:sv);
  id = id(index);
  id = id(1:sv);

  lag = lag(index,:);
  lag = lag(1:sv,:);  

  % Conversion map index --> GPStime
  snrtimes=jobs(win_idx',2)';
  snrtimes_ifo1 = abs(id);
  snrtimes_ifo1(id>0)=gpstime(id>0);

  snrtimes_ifo2 = abs(id);
  snrtimes_ifo2(id<0)=gpstime(id<0);

 
  %% IDENTIFICATION
  ifo={'ifo1','ifo2'};
  snrifo1  = [];
  snrifo2  = [];
  fmin     = [];
  fmax     = [];
  duration = [];
  SNRfrac  = [];
  veto1    = zeros(1,length(id));
  veto2    = zeros(1,length(id));
  for i=1:length(id)
    trigger=load([searchPath '/Lonetrack/lonetrack_0/lonetrack_' ...
          num2str(snrtimes(i)) '.mat']);

    if id(i)<0
      fmin = [fmin,trigger.stoch_out.lonetrack.out2.fmin];
      fmax = [fmax,trigger.stoch_out.lonetrack.out2.fmax];
      duration = [duration,trigger.stoch_out.lonetrack.out2.tmax-...
                  trigger.stoch_out.lonetrack.out2.tmin];
      SNRfrac = [SNRfrac,trigger.stoch_out.lonetrack.out2.SNRfrac];

      snrifo1 = [snrifo1,AP(mod(win_idx(i)+lag(i,1),size(jobs,1)),1)];
      snrifo2 = [snrifo2,AP(win_idx(i),2)];

    else
      fmin = [fmin,trigger.stoch_out.lonetrack.out1.fmin];
      fmax = [fmax,trigger.stoch_out.lonetrack.out1.fmax];
      duration = [duration,trigger.stoch_out.lonetrack.out1.tmax-...
                  trigger.stoch_out.lonetrack.out1.tmin];
      SNRfrac = [SNRfrac,trigger.stoch_out.lonetrack.out1.SNRfrac];

      snrifo1 = [snrifo1,AP(win_idx(i),1)];
      snrifo2 = [snrifo2,AP(mod(win_idx(i)+lag(i,1),size(jobs,1)),2)];
    end
                 
    clear trigger;
  end

  %---------------------------------------------------------------------
  %% MAIN
  %---------------------------------------------------------------------
  % 
  % get interresting carateristic of the loudNb first trigger
  %
  loudNb = sv;
  D=[...
      1:loudNb;...                          % trigger idx
      get_far(snrmax(1,1:loudNb),far);...   % P_value
      snrmax(1,1:loudNb);...                % SNR 
      snrifo1(1,1:loudNb);...               % SNR IFO 1
      snrifo2(1,1:loudNb);...               % SNR IFO 2
      snrtimes_ifo1(1,1:loudNb);...         % GPS start ifo1
      snrtimes_ifo2(1,1:loudNb);...         % GPS start ifo2
      duration(1,1:loudNb);...              % duration
      lag(1:loudNb,1)';...                   % lag number
      lag(1:loudNb,2)';...                   % mini lag number
      fmin(1,1:loudNb);...                  % fmin
      fmax(1,1:loudNb);...                  % fmax
      1000*timedelay(1,1:loudNb);...        % timedelay
      SNRfrac(1,1:loudNb);...		    % SNRfrac
      veto1(1,1:loudNb);...                 % DQFlags ifo1	  
      veto2(1,1:loudNb);...	            % DQFlags ifo2	  
    ];

  % write the loudest triggers property in a text file   
  fid = fopen(File, 'w');
  fprintf(fid,['%d %g %7.2f %7.2f %7.2f %12.2f %12.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %d %d\n'],D);
  fclose(fid);
  
  waitbar('loudest [done]');
  
  delete(waitbar);



