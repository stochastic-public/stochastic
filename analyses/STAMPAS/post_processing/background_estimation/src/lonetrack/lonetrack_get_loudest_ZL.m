function [] = lonetrack_get_loudest_ZL(File,snrmax,AP,win_idx,timedelay,data,id,lag,jobs,MaxTriggers,ThSNR,far,livetime);  

% pproc_et_loudest :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = pproc_get_loudest ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Marie Anne Bizouard
% CONTACT : 
% VERSION :
% DATE : 
%

  import classes.utils.waitbar

  waitbar('loudest [init]');
  
  if MaxTriggers == -1
    MaxTriggers=10;
  end

  if ThSNR == -1
    ThSNR=0;
  end
  
  % Selection
  [snrmax_sort,index]=sort(snrmax,'descend');
  snrmax=snrmax_sort(snrmax_sort>ThSNR);
  sv=min(length(snrmax_sort),MaxTriggers);
 
  win_idx=win_idx(1,index);
  win_idx=win_idx(1,1:sv);
  timedelay=timedelay(1,index);
  timedelay=timedelay(1,1:sv);
  snrmax=snrmax(1,1:sv);
  id = id(index);
  id = id(1:sv);

  lag = lag(index,:);
  lag = lag(1:sv,:);  

  % Conversion map index --> GPStime
  snrtimes=jobs(win_idx',2)'
  snrtimes_ifo1 = snrtimes;
  snrtimes_ifo2 = snrtimes;
 
  %% IDENTIFICATION
  ifo={'ifo1','ifo2'};
  dd=[];
  snrifo1=[];
  snrifo2=[];
  for i=1:length(id)
    if id(i)<0
      snrifo1 = [snrifo1,AP(mod(win_idx(i)+lag(i,1),size(jobs,1)),1)];
      snrifo2 = [snrifo2,AP(win_idx(i),2)];
    else
      snrifo1 = [snrifo1,AP(win_idx(i),1)];
      snrifo2 = [snrifo2,AP(mod(win_idx(i)+lag(i,1),size(jobs,1)),2)];
    end
                 

    if ~isempty(dd)
      dd=dd+data(data.windows.start.(ifo{1+(id(i)<0)})==abs(id(i)));
    else
      dd = data(data.windows.start.(ifo{1+(id(i)<0)})==abs(id(i)));
    end
  end

  %---------------------------------------------------------------------
  %% MAIN
  %---------------------------------------------------------------------
  % 
  % get interresting carateristic of the loudNb first trigger
  %
  loudNb = MaxTriggers;
  D=[...
      1:loudNb;...                          % trigger idx
      get_far(snrmax(1,1:loudNb),far);...   % P_value
      p_value(snrmax(1,1:loudNb),far,livetime);...   % P_value
      snrmax(1,1:loudNb);...                % SNR 
      snrifo1(1,1:loudNb);...               % SNR IFO 1
      snrifo2(1,1:loudNb);...               % SNR IFO 2
      snrtimes_ifo1(1,1:loudNb);...         % GPS start ifo1
      snrtimes_ifo2(1,1:loudNb);...         % GPS start ifo2
      dd(1:loudNb).duration()';...            % duration
      lag(1:loudNb,1)';...                  % lag number
      lag(1:loudNb,2)';...                  % mini lag number
      dd(1:loudNb).fmin()';...                % fmin
      dd(1:loudNb).fmax()';...                % fmax
      1000*timedelay(1,1:loudNb);...        % timedelay
      dd(1:loudNb).SNRfrac()';...		    % SNRfrac ifo1	  
      dd(1:loudNb).veto.ifo1()';...           % DQFlags ifo1	  
      dd(1:loudNb).veto.ifo2()';...	    % DQFlags ifo2	  
    ];

  % write the loudest triggers property in a text file   
  fid = fopen(File, 'w');
  fprintf(fid,['%d %g %g %7.2f %7.2f %7.2f %12.2f %12.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f ' ...
               '%d %d\n'],D);
  fclose(fid);
  
  waitbar('loudest [done]');
  
  delete(waitbar);



