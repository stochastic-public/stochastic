function pproc_FARplots_openbox(zl,cut,bkg,mc,TotTimeZL)  
% pproc_FARplots_openbox :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = pproc_FARplots_openbox ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%

  import classes.utils.waitbar

  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  %
  % parse input & get data
  %
  if nargin>5 | nargin<1
      error('wrong number of input')
  end
  
  if mc
    if exist([mc '/figures/FAR_clean.mat'])==2
      dataMC=load([mc '/figures/FAR_clean.mat']);
    else
      waitbar.warning(['Warning: No MC FAR file has been found in ' ...
                       mc '/figures/FAR_clean.mat']);
    end
  end
  
  if bkg
    if exist([bkg '/figures/pproc_FAR_clean.mat'])==2
      dataBKG=load([bkg '/figures/pproc_FAR_clean.mat']);
    else
      waitbar.warning(['Warning: No BKG FAR file has been found in ' ...
                       bkg '/figures/pproc_FAR_clean.mat']);
    end
  end
  
  % define 5-sigma line and 4sigma line
  s5 = 1/1744278/TotTimeZL;
  s4 = 1/15787/TotTimeZL;
  s3 = 1/370/TotTimeZL;

  %---------------------------------------------------------------------
  %% MAIN
  %---------------------------------------------------------------------
  % 
  %

  
  all=load([zl '/pproc_FAR_clean.mat']);
  leg = {};
  
  % define boudary of the plot
  if bkg
    if max(dataBKG.snr) > 30
      xmax = min(100*ceil(max(dataBKG.snr(dataBKG.far~=0))*1.1/100),500);
      xmin = max(min(dataBKG.snr),10);
    else
      xmax = min(100*ceil(max(dataBKG.snr(dataBKG.far~=0))*1.1/100),30);
      xmin = min(dataBKG.snr);
    end
    ymin=10^(log10(min(dataBKG.far(dataBKG.far~=0)))*1.1);
    ymax=10^(log10(max(dataBKG.far))*0.9);
  else
    if max(all.snr) > 30
      xmax = min(100*ceil(max(all.snr(all.far~=0))*1.1/100),500);
      xmin = max(min(all.snr),10);
    else
      xmax = min(100*ceil(max(all.snr(all.far~=0))*1.1/100),30);
      xmin = min(all.snr);
    end
    ymin=10^(log10(min(all.far(all.far~=0)))*1.1);
    ymax=10^(log10(max(all.far))*0.9);
  end

  figure;
  ax=gca();
  hg=hggroup;
  set(ax,'XLim',[xmin xmax]);
  set(ax,'YLim',[ymin ymax]);
  set(ax,'XScale','log');
  set(ax,'YScale','log');
  set(ax,'XGrid','on');
  set(ax,'YGrid','on');
  set(get(ax,'XLabel'),'String','SNR');
  set(get(ax,'YLabel'),'String','FAR');
  set(ax,'Box','on');
  
  hold all
  ho=[];
  if bkg
    nsall=dataBKG.snr(dataBKG.snr<xmax&dataBKG.snr>xmin);
    nfall=min(max(dataBKG.far(dataBKG.snr<xmax&dataBKG.snr>xmin),ymin),ymax);

    h=bar(nsall,nfall, ...
          'FaceColor', [.7 .7 .7], ...
          'EdgeColor', [.7 .7 .7], ...
          'BarWidth', 1, ...
          'basevalue', ymin,'Parent',ax);
    ho=[ho,h];

    dx=nsall(2)-nsall(1);
    stairs(nsall-dx/2,nfall, 'Color','k','Parent',hg)

    leg=[leg,'background'];
  else
    nsall=all.snr(all.snr<xmax&all.snr>xmin);
    nfall=min(max(all.far(all.snr<xmax&all.snr>xmin),ymin),ymax);
    
    dx=nsall(2)-nsall(1);
    
  end
  
  if mc
    h=stairs(dataMC.snr-dx/2,dataMC.far, 'Color','c','Parent',ax)
    ho=[ho,h];
    leg=[leg,'MC'];
  end

  h=plot(all.snr-dx/2,all.far,'*r');
  ho=[ho,h];
  leg=[leg,'Zero Lag'];

  h2 = plot([min(nsall) max(nsall)], [s5 s5], 'k--','LineWidth',3);
  plot([min(nsall) max(nsall)], [s4 s4], 'k--','LineWidth',3);
  plot([min(nsall) max(nsall)], [s3 s3], 'k--','LineWidth',3);
  
  hold off
  legend([ho h2],[leg,'3,4,5-sigma']);
  title('FAR');
  make_png(zl, 'FAR_ZL');

end

