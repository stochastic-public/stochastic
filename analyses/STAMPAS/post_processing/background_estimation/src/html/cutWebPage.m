function cutWebPage(cut,leg,params)  
% cutWebPage : generate html document cut specific
% DESCRIPTION :
%   generate html document that display cut information and
%   diagnostic plots
%
%   <h1> STAMPAS report page </h1>
%
%   <h2>FAR</h2>
%   <div class='plots'>
%      * table of FAR plots showing effect of the cut
%
%   <h2>Diagnostic plots</h2>
%   <div class='plots'>
%      * plots to check good effects of the cut
% 
% SYNTAX :
%   cutWebPage (cut, leg, params)
% 
% INPUT : 
%   cut : the cut name
%   leg : the cut legend
%   params : html parameters
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%


  % ------------------------------------------------------------------
  %% INIT
  % ------------------------------------------------------------------
  %

  % create a new html document
  doc = html.document;
  doc.setStyle('style.css');
  doc.setScript('main.js');

  % <h1> title
  doc.createNode('h1','STAMPAS report page');

  % add a ariane string for navigation
  d=doc.createNode('div','',...
                   'Attribute',struct('class','ariane'));
%  if params.doLonetrack
%    doc.createNode('a','main',...
%                   'Attribute',struct('href','../../'), ...
%                   'Parent',d);
%    d.appendChild(doc.createTextNode([' > ']));
%    doc.createNode('a',regexprep(params.webpath,'.*/',''),...
%                   'Attribute',struct('href','../'), ...
%                   'Parent',d);
%  else
    doc.createNode('a','main',...
                   'Attribute',struct('href','../'), ...
                   'Parent',d);
%  end
  d.appendChild(doc.createTextNode([' > ' cut]));


  % ------------------------------------------------------------------
  %% FAR (not for Lonetrack)
  % ------------------------------------------------------------------
  %
  % <h2> title


  if params.doLonetrack==0
    doc.createNode('h2','FAR');
    d=doc.createNode('div','Attribute',struct('class','plots'));
  
    t=doc.createNode('table','Parent',d);
    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th','FAR','Attribute',struct('colspan','3'), ...
		   'Parent',tr);
    switch cut
     case {'noCut','allCut','clean'}
      tr=doc.createNode('tr','Parent',t);
      addplot(doc,['figures/FAR_all.png'],tr);
      addplot(doc,['figures/FAR_clean.png'],tr);
      
     otherwise
      tr=doc.createNode('tr','Parent',t);
      addplot(doc,['figures/FAR_all.png'],tr);
      addplot(doc,['figures/FAR.png'],tr);
      addplot(doc,['figures/FAR_clean.png'],tr);
      
    end
  end


  % ------------------------------------------------------------------
  %% Diagplot plots
  % ------------------------------------------------------------------
  %
  
  % <h2> title
  doc.createNode('h2','Diagnostic plots');
  d=doc.createNode('div','Attribute',struct('class','plots'));

  t=doc.createNode('table','Parent',d);
  tr=doc.createNode('tr','Parent',t);

  doc.createNode('th','Triggers distribution plot','Attribute', ...
                 struct('colspan','4'),'Parent',tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/GPSH1_SNR.png'],tr);
  addplot(doc,['figures/GPSL1_SNR.png'],tr);
  addplot(doc,['figures/GPSH1_Duration.png'],tr);
  addplot(doc,['figures/GPSL1_Duration.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/GPSH1_Fmin_SNR_scatter.png'],tr);
  addplot(doc,['figures/GPSL1_Fmin_SNR_scatter.png'],tr);
  addplot(doc,['figures/GPSH1_Fmax_SNR_scatter.png'],tr);
  addplot(doc,['figures/GPSL1_Fmax_SNR_scatter.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/SNRfrac_Duration.png'],tr);
  addplot(doc,['figures/SNRfrac_SNR.png'],tr);
  addplot(doc,['figures/SNRfracH1_SNRH1.png'],tr);
  addplot(doc,['figures/SNRfracL1_SNRL1.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/SNRfracH1_SNRfracL1.png'],tr);
  addplot(doc,['figures/GPSH1_SNRfrac.png'],tr);
  addplot(doc,['figures/GPSL1_SNRfrac.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/SNR1_SNR2.png'],tr);
  addplot(doc,['figures/Duration_SNR.png'],tr);
  addplot(doc,['figures/MinFreq_SNR.png'],tr);
  addplot(doc,['figures/MaxFreq_SNR.png'],tr);


  % CUT Diag plot
  if strcmp(cut,'Rveto')
    t=doc.createNode('table',...
                     'Attribute',struct('class','plots'),...
                     'Parent',d);
    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th','Rveto diagnostic plot',...
                   'Attribute',struct('colspan','3'),...
                   'Parent',tr);
    
    tr=doc.createNode('tr','Parent',t);
    addplot(doc,['figures/RvetoSNR.png'],tr);
    addplot(doc,['figures/RvetoSNRratioGPSH1.png'],tr);
    addplot(doc,['figures/RvetoSNRratioGPSL1.png'],tr);

    tr=doc.createNode('tr','Parent',t);
    addplot(doc,['figures/RvetoHist.png'],tr);
    addplot(doc,['figures/RvetoHist_Win.png'],tr)
    addplot(doc,['figures/RvetoSNR_Lag.png'],tr);

  elseif ~isempty(regexp(cut,'frac:[0-9]*','match','once'))
    t=doc.createNode('table',...
                     'Attribute',struct('class','plots'),...
                     'Parent',d);
    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th','SNRfrac diagnostic plot',...
                   'Attribute',struct('colspan','3'),...
                   'Parent',tr);

    tr=doc.createNode('tr','Parent',t);
    addplot(doc,['figures/SNR-SNRfrac_scatter.png'],tr);
    addplot(doc,['figures/SNRfrac-SNRfrac_scatter.png'],tr);
  end

  % -------------------------------------------------------------------------
  %% Write Web Page
  % -------------------------------------------------------------------------
  %
  params.webpath;
  params.figuresPath;

  %display(cut)
  if ~exist([params.webpath '/' cut])
    system(['mkdir ' params.webpath '/' cut ]);
    system(['mkdir ' params.webpath '/' cut '/figures']);
  end

  system(['cp -r ' params.figuresPath '/' cut '/* ' ...
          params.webpath '/' cut '/figures']);

  if ~isempty(regexp(cut,'frac:[0-9]*','match','once'))
    system(['cp -r ' params.figuresPath '/SNRfrac/* ' ...
          params.webpath '/' cut '/figures']);
  end

  if exist([params.searchPath '/figures/FAR_clean.png'])
    system(['cp -pr ' params.searchPath '/figures/FAR_clean*.png ' ...
	    params.webpath '/' cut '/figures']);
  end
  if exist([params.searchPath '/figures/FAR_all.png'])
    system(['cp -pr ' params.searchPath '/figures/FAR_all*.png ' ...
	    params.webpath '/' cut '/figures']);
  end
  system(['cp -r  ./template/style.css ' params.webpath '/' cut '/style.css']);
  
  doc.write([params.webpath '/' cut '/index.html']);
    
end

