function generate_html(SearchPath,varargin)  
% generate_html : generate html web report page to display
% bkg & zl search
%
% DESCRIPTION :
%    generate main page that summarize global information
%    generate one page by cut used
%    generate one page for STAMPAS diagnostic
%    generate one page for windows statistic
%
% SYNTAX :
%   generate_html (searchPath, options)
% 
% INPUT : 
%    searchPath : path to the BKG or ZL dag
%    options : keys- values argument :
%        MC : path to the mc dag
%        BKG : path to the BKG dag (for ZL only)
%        Cut : cut used
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Aug 2016
%

  import classes.utils.waitbar
  import classes.utils.logfile

  %---------------------------------------------------------------------
  %% INPUT PARSER
  %---------------------------------------------------------------------
  % 
  % parse input
  %
  p=inputParser;
  addRequired(p,'SearchPath');
  if verLessThan('matlab','8.2')
    addParamValue(p,'MC','');
    addParamValue(p,'BKG','');
    addParamValue(p,'Cut',{'noCut'});
    addParamValue(p,'WEB','');
  else
    addParameter(p,'MC','');
    addParameter(p,'BKG','');
    addParameter(p,'Cut',{'noCut'});
    addParameter(p,'WEB','');
  end
  parse(p,SearchPath,varargin{:});

  searchPath = p.Results.SearchPath;
  cut        = p.Results.Cut;
  mc         = p.Results.MC;
  bkg        = p.Results.BKG;
  web        = p.Results.WEB;

  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  % 
  % create waitbar & logfile
  % and html web page
  %
  params = HTMLparams(searchPath);

  params.bkg = bkg;
  params.mc = mc;
  params.cut = cut;

  waitbar(sprintf('generate html [init]'));
  
  logfile([searchPath '/logfiles/generate_html.log']);
  logfile('generate HTML begin ...');

  if ~isempty(web)
    params.webpath = [params.host '/' web];
  end
  

  %---------------------------------------------------------------------
  %% Main Web Page
  %---------------------------------------------------------------------
  %
  mainWebPage(params);

  %---------------------------------------------------------------------
  %% Cut Web Page
  %---------------------------------------------------------------------
  %
  leg=decode(cut);
  cellfun(@(c,l) cutWebPage(c,l,params), cut,leg);
  if numel(cut)~=1
    cutWebPage('allCut','All Cut',params);
    cutWebPage('clean','Clean',params);
  end  
  
  %% diagnostic plot
  %---------------------------------------------------------------------
  %
  diagnosticWebPage(params);

  %% Windows statistic plot
  %---------------------------------------------------------------------
  %
  if params.config.doZebra
    windowsStatisticWebPage(params);
  end


  %
  %---------------------------------------------------------------------
  %% CLOSE FUNCTION
  %---------------------------------------------------------------------
  %
  logfile('generate_html done without errors');
  waitbar(sprintf('generate html [done]'));
  
  delete(logfile);
  delete(waitbar);
  
  
end

