function mainWebPage(params)  
% mainWebPage : generate html report [age including global information
%
% DESCRIPTION :
%    generate html document containing main information such as
%    FAR, PSD, etc ...
% 
%   <h1> STAMPAS report page </h1>
%
%   <h2> dag information </h2>
%   <div class='info'> 
%     <table>
%       * global search information like tstart, tend, run etc...
%     </table>
%   </div>
%
%  <h2>FAR</h2>
%    * global FAR plot 
%
%  <h2>LOUDEST</h2>
%    * table of loudest triggers
%    * SEE addLoudestTable for more informations
%
%  <h2>PSD</h2>
%    * table of PSD plot 
%
%
% SYNTAX :
%   [] = mainWebPage (params)
% 
% INPUT : 
%    params : struct of HTML params
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%

  import classes.utils.logfile
  
  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  %

  % create a new html document
  doc = html.document;
  doc.setStyle('style.css');
  doc.setScript('main.js');
  
  % <h1> title
  doc.createNode('h1','STAMPAS report page');
  
  if params.doLonetrack
    d=doc.createNode('div','',...
                     'Attribute',struct('class','ariane'));
    doc.createNode('a','main',...
                   'Attribute',struct('href','../'), ...
                   'Parent',d);
    d.appendChild(doc.createTextNode([' > ' regexprep(params.webpath,'.*/','')]));    
  end

  %---------------------------------------------------------------------
  %% INFORMATION SECTION
  %---------------------------------------------------------------------
  %
  doc.createNode('h2','dag information');

  d=doc.createNode('div','','Attribute',struct('class','info'));
  t=doc.createNode('table','Parent',d);

  % search info 
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Search : ','Parent',tr);
  doc.createNode('td',params.search,'Parent',tr);

  % run info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Run : ','Parent',tr);
  doc.createNode('td',params.run,'Parent',tr);

  % type info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Type : ','Parent',tr);
  doc.createNode('td',params.type,'Parent',tr);

  % BKG (if ZL)
  if params.config.doZeroLag && ~isempty(params.bkg)
    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th','BKG : ','Parent',tr);
    td=doc.createNode('td','Parent',tr);
    doc.createNode('a',regexprep(params.bkg,'.*BKG/(.*)','$1'), ...
                   'Attribute',struct('href',[regexprep(params.bkg,'BKG/','') '/background_estimation']),...
                   'Parent',td);
  end

  % MC (if MC)
  if params.mc
    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th','MC : ','Parent',tr);
    td=doc.createNode('td','Parent',tr);
    doc.createNode('a',regexprep(params.mc,'.*BKG/(.*)','$1'),'Attribute', ...
                   struct('href',[regexprep(params.mc,'BKG/','') '/background_estimation']),'Parent',td);
  end

  % GPS start info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','GPS start : ','Parent',tr);
  doc.createNode('td',num2str(params.start),'Parent',tr);

  % GPS stop info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','GPS stop : ','Parent',tr);
  doc.createNode('td',num2str(params.stop),'Parent',tr);

  % id info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Id : ','Parent',tr);
  doc.createNode('td',num2str(params.id),'Parent',tr);

  % config file info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Config file : ','Parent',tr);
  td=doc.createNode('td','Parent',tr);
  doc.createNode('a','see file','Attribute',...
                 struct('href',['info/config.txt']),...
                 'Parent',td);

  % freqNotch
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Frequency Notches : ','Parent',tr);
  td=doc.createNode('td','Parent',tr);
  doc.createNode('a','see file','Attribute',...
                 struct('href',['info/frequencies.txt']),...
                 'Parent',td);

  % number of lag
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Number of lag : ','Parent',tr);
  if params.config.doZebra
    doc.createNode('td',num2str(params.config.NTShifts),'Parent',tr);
  else
    doc.createNode('td',num2str(params.config.lonetrackNSlides),'Parent',tr);
  end

  % FAR THR
  if ~params.config.doZeroLag
    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th',sprintf('SNR Threshold (1/%.0f): ',1/params.far2thr),'Parent',tr);
    doc.createNode('td',num2str(FAR2THR(params.FARpath,params.far2thr)),'Parent',tr);
  end

  % time live
  if params.config.doZeroLag
    TTime=load([params.searchPath '/tmp/TTimeZeroLag.txt']);
  else
    TTime=load([params.searchPath '/tmp/TTime.txt']);
  end
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Total livetimes : ','Parent',tr);
  doc.createNode('td',num2str(TTime),'Parent',tr);

  % lonetrack th ifo
  if params.doLonetrack
    st1 = load([params.searchPath '/Lonetrack/bknd/stage1.mat']);

    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th','Th1 : ','Parent',tr);
    doc.createNode('td',num2str(st1.th1),'Parent',tr);

    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th','Th2 : ','Parent',tr);
    doc.createNode('td',num2str(st1.th2),'Parent',tr);
  end

  % Cut info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Cut  : ','Parent',tr);
  td=doc.createNode('td','Parent',tr);
  doc.createNode('a',decode(params.cut{1}),...
                 'Attribute',struct('href',['./' params.cut{1}]),...
                 'Parent',td);
  if numel(params.cut)~=1
    for c={params.cut{2:end}}
      tr=doc.createNode('tr','Parent',t);
      doc.createNode('th','','Parent',tr);
      td=doc.createNode('td','Parent',tr);
      doc.createNode('a',decode(c{:}),...
                     'Attribute',struct('href',['./' c{:}]),...
                     'Parent',td);
    end
    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th','','Parent',tr);
    td=doc.createNode('td','Parent',tr);
    doc.createNode('a','All Cut','Attribute',struct('href','allCut'),'Parent',td);
    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th','','Parent',tr);
    td=doc.createNode('td','Parent',tr);
    doc.createNode('a','Clean',...
                   'Attribute',struct('href','clean'),...
                   'Parent',td);
  end

  % pipeline diag plot & WIn stat
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Pipeline information  : ','Parent',tr);
  td=doc.createNode('td','Parent',tr);
  doc.createNode('a','diagnostic plots',...
                 'Attribute',struct('href','diagnostic'),'Parent',td);

  if params.config.doZebra  
    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th','','Parent',tr);
    td=doc.createNode('td','Parent',tr);
    doc.createNode('a','Windows statistic',...
		   'Attribute',struct('href','winstat'),...
		   'Parent',td);
  end
  %---------------------------------------------------------------------
  %% FAR SECTION
  %---------------------------------------------------------------------
  %

  if params.config.doZeroLag
    if params.bkg
      doc.createNode('h2','BKG FAR');

      d=doc.createNode('div','Attribute',struct('class','plots'));
      t=doc.createNode('table','Parent',d);

      tr=doc.createNode('tr','Parent',t);
      if params.doLonetrack
	addplot(doc,['BKG/pproc_FAR_all.png'],tr);
      else
	addplot(doc,['BKG/FAR_all.png'],tr);
	addplot(doc,['BKG/FAR_cumulative.png'],tr);
	addplot(doc,['BKG/FAR_clean.png'],tr);
      end

    end

    % ZL FAR 
    doc.createNode('h2','ZL FAR');

    d=doc.createNode('div','Attribute',struct('class','plots'));
    t=doc.createNode('table','Parent',d);

    tr=doc.createNode('tr','Parent',t);
    addplot(doc,['figures/FAR_ZL.png'],tr);

    % ZL FAR detail
    if ~params.doLonetrack
      doc.createNode('h2','ZL FAR (detail)');
      d=doc.createNode('div','Attribute',struct('class','plots'));
      t=doc.createNode('table','Parent',d);
      
      tr=doc.createNode('tr','Parent',t);
      addplot(doc,['figures/FAR_all.png'],tr);
      addplot(doc,['figures/FAR_cumulative.png'],tr);
      addplot(doc,['figures/FAR_clean.png'],tr);
    end

  else
    doc.createNode('h2','FAR');
    d=doc.createNode('div','Attribute',struct('class','plots'));
    
    t=doc.createNode('table','Parent',d);
    tr=doc.createNode('tr','Parent',t);

    if params.doLonetrack
      addplot(doc,['figures/pproc_FAR_all.png'],tr);
    else
      addplot(doc,['figures/FAR_all.png'],tr);
      addplot(doc,['figures/FAR_cumulative.png'],tr);
      addplot(doc,['figures/FAR_clean.png'],tr);
    end
  end


  %---------------------------------------------------------------------
  %% LOUDEST SECTION
  %---------------------------------------------------------------------
  %
  if params.config.doZebra
    addLoudestTable(doc,[params.searchPath '/figures/loudest.txt'],params);
  else
    addLoudestTableLonetrack(doc,[params.searchPath '/figures/loudest.txt'],params);
  end

  %---------------------------------------------------------------------
  %% PSD SECTION
  %---------------------------------------------------------------------
  %
  doc.createNode('h2','PSD');
  d=doc.createNode('div','Attribute',struct('class','plots'));

  t=doc.createNode('table','Parent',d);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/PSD-H1.png'],tr);
  addplot(doc,['figures/PSD-L1.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/PSD-H1_zoom.png'],tr);
  addplot(doc,['figures/PSD-L1_zoom.png'],tr);

  
  %---------------------------------------------------------------------
  %% Write HTML page
  %---------------------------------------------------------------------
  %
  if ~exist(params.webpath) || ~exist([params.webpath '/figures']);
    logfile(['make path : ' params.webpath]);
    system(['mkdir -p ' params.webpath]);
    system(['mkdir -p ' params.webpath '/figures']);
  end
  
  % move file
  if ~exist([params.webpath '/info']);
    system(['mkdir ' params.webpath '/info']);
  end
  system(['cp -r ' params.searchPath '/figures/*FAR*.png ' ...
          params.webpath '/figures']);
  system(['cp -r ' params.searchPath '/figures/PSD/* ' params.webpath '/figures']);
  system(['cp -r ' params.searchPath '/config_bkg.txt ' params.webpath ...
          '/info/config.txt']);
  system(['cp -r ' params.searchPath '/tmp/frequencies.txt ' params.webpath ...
          '/info/frequencies.txt']);
  system(['cp -r  ./template/style.css ' params.webpath '/style.css']);
  
  if params.config.doZeroLag && ~isempty(params.bkg)
    system(['cp -r ' params.bkg '/figures ' params.webpath '/BKG']);
  end

  doc.write([params.webpath '/index.html'])

end

