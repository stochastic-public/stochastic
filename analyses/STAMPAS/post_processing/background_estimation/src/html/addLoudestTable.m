function [] = addLoudestTable(doc,file,params)  
% addLoudestTable : create html table showint loudest triggers
% DESCRIPTION :
%    generate a html table showing loudest triggers characteristic
%
%  <h2>LOUDEST</h2>
%  <div class='triggers'>
%    <table>
%      <tr>
%        <th> * header to define triggers characteristic * </th>
%      <tr>
%      <tr>
%        <td> * triggers's information * </td>
%      </tr>
%    </table>
%  </div>
%
% SYNTAX :
%   addLoudestTable (doc, file, params)
% 
% INPUT : 
%    doc : html document
%    file : .txt containing the loudest triggers information
%    params : html parameters
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%

  triggers=load(file);
  
  % ---------------------------------------------------------------
  %% INIT
  % ---------------------------------------------------------------
  % 
  % <h2> Loudest triggers </h2>
  % <div class='' id='trigger'>
  %   <table></table>
  % </div>
  %
  doc.createNode('h2','Loudest triggers');
  d=doc.createNode('div','Attribute',struct('class','triggers'));
  t=doc.createNode('table','Parent',d);

  % ---------------------------------------------------------------
  %% HEADER
  % ---------------------------------------------------------------
  % 
  % <tr>
  %   <th rowspan="2">Index</th>
  %   <th rowspan="2">FAR</th>
  %   <th rowspan="2">SNR</th>
  %   <th rowspan="2">GPS start</th>
  %   <th rowspan="2">Duration</th>
  %   <th rowspan="2">Lag</th>
  %   <th rowspan="2">Fmin</th>
  %   <th rowspan="2">Fmax</th>
  %   <th rowspan="2">Ra</th>
  %   <th rowspan="2">Dec</th>
  %   <th rowspan="2">Fmax</th>
  %   <th colspan="3">SNRfrac</th>
  %   <th colspan="3">veto</th>
  %   <th rowspan="2">FTmap</th>
  %   <th colspan="3">OmegaScan</th>
  % </tr>
  % <tr>
  %   <th>ifo1</th>
  %   <th>ifo2</th>
  %   <th>coherent</th>
  %   <th>ifo1</th>
  %   <th>ifo2</th>
  %   <th>ifo1</th>
  %   <th>ifo2</th>
  % </tr>
  %
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Index',...
                 'Attribute',struct('rowspan','2'), ...
                 'Parent',tr);
  doc.createNode('th','FAR',...
                 'Attribute',struct('rowspan','2'), ...
                 'Parent',tr);
  doc.createNode('th','SNR',...
                 'Attribute',struct('rowspan','2'), ...
                 'Parent',tr);
  doc.createNode('th','GPS start time',...
                 'Attribute',struct('colspan','2'),...
                 'Parent',tr);
  doc.createNode('th','Duration',...
                 'Attribute',struct('rowspan','2'),...
                 'Parent',tr);
  doc.createNode('th','Lag',...
                 'Attribute',struct('rowspan','2'), ...
                 'Parent',tr);
  doc.createNode('th','Fmin',...
                 'Attribute',struct('rowspan','2'), ...
                 'Parent',tr);
  doc.createNode('th','Fmax',...
                 'Attribute',struct('rowspan','2'), ...
                 'Parent',tr);
  doc.createNode('th','Ra',...
                 'Attribute',struct('rowspan','2'), ...
                 'Parent',tr);
  doc.createNode('th','Dec',...
                 'Attribute',struct('rowspan','2'), ...
                 'Parent',tr);
  doc.createNode('th','Tau [ms]',...
                 'Attribute',struct('rowspan','2'), ...
                 'Parent',tr);
  doc.createNode('th','SNRfrac',...
                 'Attribute',struct('colspan','3'),...
                 'Parent',tr);
  doc.createNode('th','veto',...
                 'Attribute',struct('colspan','2'), ...
                 'Parent',tr);
  doc.createNode('th','FTmap',...
                 'Attribute',struct('rowspan','2'), ...
                 'Parent',tr);
  doc.createNode('th','Omega scan',...
                 'Attribute',struct('colspan','2'),...
                 'Parent',tr);

  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','ifo 1','Parent',tr);
  doc.createNode('th','ifo 2','Parent',tr);
  doc.createNode('th','coherent','Parent',tr);
  doc.createNode('th','ifo 1','Parent',tr);
  doc.createNode('th','ifo 2','Parent',tr);
  doc.createNode('th','ifo 1','Parent',tr);
  doc.createNode('th','ifo 2','Parent',tr);
  doc.createNode('th','ifo 1','Parent',tr);
  doc.createNode('th','ifo 2','Parent',tr);


  
  % ---------------------------------------------------------------
  %% MAIN
  % ---------------------------------------------------------------
  % loop over the loudest event to fill the cell
  %
  for i=1:size(triggers,1)
    tr=doc.createNode('tr','Parent',t);
    for j=1:size(triggers,2)
      doc.createNode('td',num2str(triggers(i,j)),'Parent',tr);
    end
    
    % FTMAPS
    td=doc.createNode('td','Parent',tr);
    doc.createNode('a','ftmap',...
                   'Attribute',struct('href',['FTMAPS/ftmap_' ...
                        num2str(i)]),...
                   'Parent',td);
    
    % OMEGA SCAN
    td=doc.createNode('td','Parent',tr);

    ifo1_gps=triggers(i,4);
    ifo2_gps=triggers(i,5);

    doc.createNode('a','omega',...
                   'Attribute',struct('href',[params.href_omega ...
                        '/OMEGA/H1/' sprintf('%.3f',ifo1_gps)]), ...
                   'Parent',td);
    td.appendChild(doc.createTextNode(' | '));
    doc.createNode('a','omega',...
                   'Attribute',struct('href',[params.href_omega ...
                        '/OMEGA_HOFT/H1/' sprintf('%.3f',ifo1_gps)]),...
                   'Parent',td);

    td=doc.createNode('td','Parent',tr);
    doc.createNode('a','omega',...
                   'Attribute',struct('href',[params.href_omega ...
                        '/OMEGA/L1/' sprintf('%.3f',ifo2_gps)]), ...
                   'Parent',td);
    td.appendChild(doc.createTextNode(' | '));
    doc.createNode('a','omega',...
                   'Attribute',struct('href',[params.href_omega ...
                        '/OMEGA_HOFT/L1/' sprintf('%.3f',ifo2_gps)]),...
                   'Parent',td);

    % Omega scan script generation (not CIT case)
    if params.cit==0
      fprintf(params.fid1,...
              './launchOmegaScan.sh H %.3f %.3f p %s/LOUDEST\n',...
              ifo1_gps,ifo1_gps,params.search);
      fprintf(params.fid2,...
              './launchOmegaScan.sh L %.3f %.3f p %s/LOUDEST\n',...
              ifo2_gps,ifo2_gps,params.search);
    end
  end

end

