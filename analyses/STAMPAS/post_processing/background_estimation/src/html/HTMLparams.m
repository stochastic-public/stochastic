function params = HTMLparams(searchPath)  
% HTMLparams : define parameters use in web page generation
% DESCRIPTION :
%   get all parameters from the background search 
% 
% SYNTAX :
%   [params] = HTMLparams (searchPath)
% 
% INPUT : 
%   searchPath : path to the search
%  
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%
  params.far2thr = 1/1; %% thr of the FAR
  params.searchPath = searchPath;
  params.start   = str2num(regexprep(params.searchPath, '.*Start-([0-9]*)_.*','$1'));
  params.stop    = str2num(regexprep(params.searchPath, '.*Stop-([0-9]*)_.*','$1'));
  params.id      = str2num(regexprep(searchPath, '.*ID-([0-9]*).*','$1'));
  params.type    = regexprep(searchPath,'.*/(BKG|INJ)/.*', '$1');
  params.run     = get_run_from_GPStimes(params.start, params.stop);
  params.search  = [lower(params.run) '-allsky'];
  params.config  = readConfig([params.searchPath '/config_' lower(params.type) '.txt']);

  params.figuresPath = [searchPath '/figures'];

  % test if zero lag
  if params.config.doZeroLag
    params.type='Zero Lag';
  end
  
  % test if seedless folder
  if params.config.doLonetrack || params.config.doLonetrackPProc
    params.doLonetrack=true;
  else
    params.doLonetrack=false;
  end

  % FAR path  needed to FAR2THR function
  if params.doLonetrack
    params.FARpath = [params.searchPath '/figures/pproc_FAR_clean.mat'];
  else
    params.FARpath = [params.searchPath '/figures/FAR_clean.mat'];
  end
  
  % web direction definition
  [~,h] = system('hostname -d');
  user=getenv('USER');
  h=h(1:end-1); % remove last char

  params.cit=0;
  params.href_omega=['https://ldas-jobs.ligo.caltech.edu/~' user '/' params.search ...
              '/LOUDEST/'];

  if (strcmpi(h,'ligo.caltech.edu') | strcmpi(h,'ligo-wa.caltech.edu') ...
      | strcmpi(h,'ligo-la.caltech.edu'))
    params.host=['/home/' user '/public_html'];
    params.cit=1;
  elseif strcmpi(h,'atlas.local')
    params.host=['/home/' user '/WWW/LSC'];
  else
    params.host=['/home/' user '/public_html'];
  end

  if params.cit==0
    params.fid1=fopen('omegascan_H1.sh','w');
    params.fid2=fopen('omegascan_L1.sh','w');
  end  

  % make path
  params.webpath = sprintf('%s/%s/Start-%d_Stop-%d_ID-%d/background_estimation/',...
                           params.host,params.search,params.start,params.stop,params.id);

  

end

