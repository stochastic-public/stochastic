function PSDplots(jobFile,varargin)  
% PSDplots : plot PSD of a given run 
% DESCRIPTION :
%    divide the run into 10 segments. Make the PSD of each segment. 
%    PSD is estimate using the pwelch method. a high pass filter is
%    use for the data before estimate the PSD. then plot the mean
%    PSD over the 10 segment and add error bar using the std of the
%    PSD over these segment. 
%
% SYNTAX :
%   [] = PSDplots (searchPath,options)
% 
% INPUT : 
%    searchPath : the path of the search folder
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.2
% DATE : Mar 2016
%
  
  %setenv('LD_LIBRARY_PATH','');
  import classes.utils.waitbar

  %--------------------------------------------------------------
  %% INPUT PARSER
  %--------------------------------------------------------------
  %
  %
  p=inputParser;

  addRequired(p,'jobFile',@exist); 
  if verLessThan('matlab','8.2')
    addParamValue(p,'Det',{'H1','L1'});
    addParamValue(p,'Folder','./figures');
    addParamValue(p,'Nave',20,@isnumeric);
    addParamValue(p,'Delta',0.05,@isnumeric);
    addParamValue(p,'HPFreq',0,@isnumeric);
    addParamValue(p,'MC',0,@isnumeric);
    addParamValue(p,'DetectorNoiseFileLIGO');
    addParamValue(p,'Start',0,@isnumeric);
    addParamValue(p,'Stop',0,@isnumeric);
    addParamValue(p,'Fmin',0,@isnumeric);
    addParamValue(p,'Fmax',0,@isnumeric);
    addParamValue(p,'Ntime',10,@isnumeric);
    addParamValue(p,'THR',-22,@isnumeric);    
  else  
    addParameter(p,'Det',{'H1','L1'});
    addParameter(p,'Folder','./figures');
    addParameter(p,'Nave',20,@isnumeric);
    addParameter(p,'Delta',0.05,@isnumeric);
    addParameter(p,'HPFreq',0,@isnumeric);
    addParameter(p,'MC',0,@isnumeric);
    addParameter(p,'DetectorNoiseFile','../../PSDs/H1_O2PSD.txt');
    addParameter(p,'Start',0,@isnumeric);
    addParameter(p,'Stop',0,@isnumeric);
    addParameter(p,'Fmin',0,@isnumeric);
    addParameter(p,'Fmax',0,@isnumeric);
    addParameter(p,'Ntime',10,@isnumeric);
    addParameter(p,'THR',-22,@isnumeric);    
  end
  parse(p,jobFile,varargin{:});
  
  segments     = load(p.Results.jobFile);
  det          = p.Results.Det;
  ff           = p.Results.Folder;
  Nave         = p.Results.Nave;
  delta        = p.Results.Delta;
  highPassFreq = p.Results.HPFreq;
  MC           = p.Results.MC;
  DetectorNoiseFile = p.Results.DetectorNoiseFile;
  thr          = p.Results.THR;

  
  if p.Results.Start == 0;
    start      = segments(1,2);
  else
    start      = p.Results.Start;
  end

  if p.Results.Stop == 0;
    stop        = segments(end,3);
  else
    stop        = p.Results.Stop;
  end

  fmin          = p.Results.Fmin;
  fmax          = p.Results.Fmax;
  Ntime         = p.Results.Ntime+1;


  %--------------------------------------------------------------
  %% INIT
  %--------------------------------------------------------------
  %
  % Series of plot for windows statistic
  %

  fsamp    = 16384;
  run      = get_run_from_GPStimes(start, stop);
  T        = ceil(Nave/delta);

  segments = segments(segments(:,2)>=start&segments(:,3)<=stop,:);
  segs=[];

  for i=1:size(segments,1)
    sub=segments(segments(:,5)==i,:);
    if size(sub,1)>0
      segs=[segs;i,sub(1,2),sub(end,3),sub(end,3)-sub(1,2),i];
    end
  end
  segments = segs(segs(:,4)>2*T,:);

  total=sum(segments(:,4));
  times = round(linspace(1,total-1,Ntime));
  X=cumsum(segments(:,4));
  X=[0;X];
  PSDtimes=zeros(Ntime,1);
  for i=1:Ntime
    index=find(X(:,1)<times(i));
    PSDtimes(i)=segments(index(end),2);
  end

  display(['The PSDs will be estimated at the following times: ' num2str(PSDtimes')]);
%  printf("\n")

  %--------------------------------------------------------------
  %% MAIN
  %--------------------------------------------------------------
  %
  % plot a PSD each 10% of the jobfile
  %
            
  % loop over the ifo
  for ifo=1:2
    
    % allocate memory for speed up
    Yall  = zeros(fsamp/(2*delta)+1,length(PSDtimes)-1);
    Yhall = zeros(fsamp/(2*delta)+1,length(PSDtimes)-1);

    % get the frames
    %------------------------------------------------------
    params.job.startGPS    = start;
    params.job.duration    = stop-start;
    params.largeShiftTime1 = 1;
    params.ifo1            = det{ifo};
    params.frameType1      = frames(run,det{ifo});
    which find_frames
    [frames,gps,durs] = find_frames(params);

    
    % loop over the PSD time
    for idx=1:numel(PSDtimes)-1
      waitbar(sprintf('psd [%3.0f\%]',100*((ifo-1)*numel(PSDtimes)+idx)/(2*numel(PSDtimes))));

      GPStime = PSDtimes(idx);

      % Get the coincident segments
      %------------------------------------------------------
      if size(segments,1)<1
        error('No segments found');
      end

      segment  = segments(segments(:,2)<=GPStime&...
                          segments(:,3)>=GPStime+T,:);


      if size(segment,1)==0
        error(['No segment found at the requested time. '...
	       num2str(GPStime) ' Will take the closest segment']);
      end

      % get the good frames
      %------------------------------------------------------
      index=[];
      for i=1:length(gps)-1
        if gps(i)<GPStime & gps(i+1)>=GPStime
          index=[index; i];
        end
        if gps(i)<GPStime+T & gps(i+1)>=GPStime+T
          index=[index; i];
        end
      end
      index=unique(index);

      goodtimes  = gps(index);
      goodframes = frames(index);

      % Get data
      %------------------------------------------------------
      if MC == 0
	[vector,tsamp]=...
	    frgetvect(goodframes{1},channel(run,det{ifo}), GPStime,T);
	fs=1./(tsamp(2)-tsamp(1));
      else
	psd = load(['../../' DetectorNoiseFile]);
	N=T*fsamp;
	fs=fsamp;
	% prepare for FFT
	if ( mod(N,2)== 0 )
	  numFreqs = N/2 - 1;
	else
	  numFreqs = (N-1)/2;
	end
	deltaF = 1./T;
	f = deltaF*[1:1:numFreqs]';  %'
	flow = deltaF;
	% Next power of 2 from length of y
	NFFT = 2^nextpow2(N);
	amp_values = psd(:,2);
	f_transfer1 = psd(:,1);
	Pf1 = 10.^interp1(f_transfer1, log10(amp_values), f, 'linear', 'extrap');
	%
	deltaT = 1./fsamp;
	norm1 = sqrt(N/(2*deltaT)) * sqrt(Pf1);
	%
	re1 = norm1*sqrt(1/2).* randn(numFreqs, 1);
	im1 = norm1*sqrt(1/2).* randn(numFreqs, 1);
	z1  = re1 + 1i*im1;

	htilde1 = z1;
	% convolve data with instrument transfer function
	otilde1 = htilde1*1;
	% set DC and Nyquist = 0, then add negative freq parts in proper order 
	if ( mod(N,2)==0 )
	  % note that most negative frequency is -f_Nyquist when N=even
	  otilde1 = [ 0; otilde1; 0; flipud(conj(otilde1)) ];
	else
	  % no Nyquist frequency when N=odd
	  otilde1 = [ 0; otilde1; flipud(conj(otilde1)) ];
	end
	% fourier transform back to time domain
	data = ifft(otilde1);
	% take real part (imag part = 0 to round-off)
	data = real(data);
	vector = data';  %'
	size(vector)
	vector(1,100)
      end



      % Make PSD using pwelch method
      %------------------------------------------------------
      [Y,f] = pwelch(vector, hann(fs),...
                     [],fs/delta, ...
                     fs, 'onesided');
      Yall(:,idx)=log10(sqrt(Y));

      clear Y;
      
      % High Passed Data
      %------------------------------------------------------
      if highPassFreq ~= 0
        % High pass filter setting
        highPassOrder = 6;
        highPassFreqNorm = highPassFreq/(fs/2);
        
        [b,a] = butter(highPassOrder, highPassFreqNorm, 'high');
        vector = filtfilt(b,a,vector);
        
        [Yh,fh] = pwelch(vector, hann(fs),...
                         [], fs/delta, ...
                         fs, 'onesided');

        Yhall(:,idx)=log10(sqrt(Yh));
        clear Yh fh;
      end
    end
    
    % Define and remove outlier 
    %------------------------------------------------------
    %
    out = mean(Yall(f>310&f<311,:)) < thr;
    Yall = Yall(:,out); 
    if  highPassFreq ~= 0
      out = mean(Yhall(f>310&f<311,:)) < thr;
      Yhall = Yhall(:,out); 
    end

    % convert PSD to loglog PSD
    %------------------------------------------------------
    %
    % remove all zeros element to avoid inf when we log
    % the array
    %
    Yall = Yall(f~=0,:);
    
    ym   = mean(Yall,2);
    fl   = log10(f(f~=0));

    if  highPassFreq ~= 0
      Yhall = Yhall(f~=0,:);
      yhm   = mean(Yhall,2);
      yerr  =  std(Yhall,0,2);
    else
      yerr = std(Yall,0,2);
    end
    clear Yhall Yall;
    

    % PLOT PSD
    psd2plot(10, 5000);
    title([run '-' det{ifo} '. Average of ' num2str(Ntime) ' PSD over ' num2str(start) ...
           '-' num2str(stop)], 'Fontsize',15);

    make_png([ff '/PSD'], ['PSD-' det{ifo}]);
    
    % PLOT ZOOMED PSD
    if fmin ==0 
      xmin = 10;
    else
      xmin = fmin;      
    end
    
    if fmax ==0
      xmax = 5000;
    else
      xmax = fmax;
    end

    psd2plot(xmin,xmax);
    title([run '-' det{ifo} ':' num2str(start) ...
           '-' num2str(stop)], 'Fontsize',15);

    make_png([ff '/PSD'], ['PSD-' det{ifo} '_zoom']);
  end

  %--------------------------------------------------------------
  %% CLOSE FUNCTION
  %--------------------------------------------------------------
  %
  %
  waitbar(sprintf('psd [done]'));
  delete(waitbar);
  
  %--------------------------------------------------------------
  %% TOOLS FUNCTION
  %--------------------------------------------------------------
  %
  % plots computed psd 

  function psd2plot(xmin, xmax)
  %%|  plot PSD

    if xmax-xmin<50
      useLinScale=true;
    else
      useLinScale=false;
    end

    % get boundary
    Ylim = ym(f>xmin&f<xmax);
    xmin = log10(xmin);
    xmax = log10(xmax);
    ymin = min(Ylim)-0.1*(max(Ylim)-min(Ylim));
    ymax = max(Ylim)+0.1*(max(Ylim)-min(Ylim));

    figure()
    hold all
    axis([xmin xmax ymin ymax]);
    ax=gca();            
    
    %%% matlab r2013 have problem for clip area to axes
    %%% so we cut data
    cut = fl>xmin&fl<xmax;
    X=[fl(cut);flipud(fl(cut))];
    if highPassFreq~=0
      Y=[max(ymin,yhm(cut)-yerr(cut));...
         min(ymax,flipud(yhm(cut)+yerr(cut)))];
    else
      Y=[max(ymin,ym(cut)-yerr(cut));...
         min(ymax,flipud(ym(cut)+yerr(cut)))];
    end
    
    %%% plot area and PSDmean
    hh=fill(X, Y,'c');
    set(hh, 'FaceColor', [0.0 0.9 0.9]);
    set(hh, 'EdgeColor', [0.0 0.9 0.9]);
    
    h1=plot(fl(cut), ym(cut), 'r');
    
    leg={'std psd', 'mean psd'};                
    hleg=[hh, h1];
    
    if highPassFreq~=0
      h2=plot(fl(cut), yhm(cut), 'b');
      leg = [leg 'high passed'];
      hleg=[hleg, h2];
    end
    
    %%% add log axis
    axis off
    ax1 = axes('position', get(ax, 'position'), ...
               'color', 'none');
    
    Ymin = 10^ymin;
    Ymax = 10^ymax;
    Xmin = 10^xmin;
    Xmax = 10^xmax;

    set(ax1,'YLim',[Ymin, Ymax]);
    set(ax1,'XLim',[Xmin, Xmax]);

    set(ax1, 'YScale', 'log')
    if useLinScale
      set(ax1, 'XScale', 'lin')
    else
      set(ax1, 'XScale', 'log')
    end

    %%% legend
    l=legend(hleg,leg);
    set(l,'Location','best')
    set(l,'Interpreter','none')                
    
    hold off
    
    %%% label, title ...
    xlabel('Frequency [Hz]','Fontsize',15);
    ylabel('ASD [strain\surd(Hz)]','Fontsize',15);
    grid minor
    box
  end
end


function ch=channel(run,det)
%%% Get channel
  if strcmp(run, 'S6')
    ch = [det ':LDAS-STRAIN'];
  elseif strcmp(run, 'ER7')
    ch = [det ':GDS-CALIB_STRAIN'];
  elseif strcmp(run, 'ER8')
    ch = [det ':GDS-CALIB_STRAIN'];
  elseif strcmp(run, 'O1')
    ch = [det ':DCS-CALIB_STRAIN_C02'];
  elseif strcmp(run, 'O2')
    ch = [det ':DCH-CLEAN_STRAIN_C02'];
  elseif strcmp(run, 'O3')
    ch = [det ':DCS-CALIB_STRAIN_CLEAN_C01'];
  else
    error(['Channel name not defined for this run:' run]);
  end
end

function ch=frames(run,det)
%%% Get frames
  if strcmp(run, 'S6')
    fr = [det '_LDAS_C02_L2'];
  elseif strcmp(run, 'ER7')|| strcmp(run, 'ER8')|| strcmp(run, 'O1')
    ch = [det '_HOFT_C02'];
  elseif strcmp(run, 'O2')
    ch = [det '_CLEANED_HOFT_C02'];
  elseif strcmp(run, 'O3')
    ch = [det '_HOFT_C01'];
  else
    error(['Channel name not defined for this run:' run]);
  end
end
