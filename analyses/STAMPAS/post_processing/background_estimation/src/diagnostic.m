function diagnostic(SearchPath,Data,varargin)  
% plots : make series of plot 
% DESCRIPTION :
%    plot the following graph:
%     - GSP1/2 vs SNR (f1)            [+ with various cut]
%     - GPS1/2 vs SNRfrac (f2)        [+ with various cut]
%     - GPS1/2 vs Dur (f3)            [+ with various cut]
%     - Dur vs SNR  (f4)              [+ with various cut]
%     - SNRfrac vs SNR (f5)           [+ with various cut]
%     - SNRfrac vs Dur (f6)           [+ with various cut]
%     - Fmin vs SNR (f7)              [+ with various cut]
%     - Fmax vs SNR (f8)              [+ with various cut]
%     - SNRfrac1/2 vs SNR1/2 (f9)     [+ with various cut]
%     - SNR1 vs SNR2 (f10)            [+ with various cut]
%     - GPS1/2 vs Fmin vs SNR (f11)   [plot only triggers that pass cut]
%     - GPS1/2 vs Fmax vs SNR (f12)   [plot only triggers that pass cut]
%     - SNRfrac1 vs SNRfrac2 (f13)    [+ with various cut]

%     - GPS1/2 vs SNR density          page diagnostic
%     - GPS1/2 vs Fmin density         page diagnostic
%     - GPS1/2 vs duration  density    page diagnostic
%     - Fmin vs duration density       page diagnostic
%     - Fmin vs SNR (SNR<100) density  page diagnostic
%     - Fmax vs SNR (SNR<100) density  page diagnostic
%     - lag vs duration density        page diagnostic

%     - Dur histo                 page diagnostic 
%     - ra histo                  page diagnostic
%     - dec histo                 page diagnostic

%     - Events Nb vs Lag          not used
%     - Events Nb vs Window       not used
%     - EventsNb vs window histo  not used
%     - windows GPS time histo (SNR > thr) not used
%
% SYNTAX :
%   plots (SearchPath,Data,options)
% 
% INPUT : 
%    SearchPath : the path of the search folder
%    Data : STAMPAS data object
%    options : keys values input
%        Folder : figures folder by default searchPath/figures
%                 directory
%        Cut : cell array of cut, by default noCut 
% 
% AUTHOR : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.2
% DATE : Mar 2016
%

  import classes.utils.waitbar
  import classes.utils.logfile

  %---------------------------------------------------------------------
  %% INPUT PARSER
  %---------------------------------------------------------------------
  % 
  p=inputParser;
  addRequired(p,'SearchPath');
  addRequired(p,'Data');
  if verLessThan('matlab','8.2')
    addParamValue(p,'Folder','');
    addParamValue(p,'Cut',{'noCut'});
  else
    addParameter(p,'Folder','');
    addParameter(p,'Cut',{'noCut'});
  end
  parse(p,SearchPath,Data,varargin{:});
  
  data = p.Results.Data;
  
  searchPath = p.Results.SearchPath;
  if isempty(p.Results.Folder)
    ff = [searchPath '/figures'];
  else
    ff = p.Results.Folder;
  end
  cut = p.Results.Cut; 
  
  % if multiple cut add a clean cut that combine all the cut 
  if length(cut)~=1
    allCut=strcat(cut,'+');
    allCut=strcat(allCut{:});
  end  

  config=readConfig([searchPath '/config_bkg.txt']);
  
  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  % 
  % check input
  % if cut is not pass as an input take no cut 
  % if seedless data, split data into coherent, ifo1, ifo2 data and
  % run diagnostic function for each of them
  % 
  waitbar('diagnostic [init]');
  logfile([searchPath '/logfiles/diagnostic.log']);
  logfile('diagnostic plot begin ...');
  
  % | order trigger by SNR
  if isstruct(data.format.SNR)
    data.sort('SNR.coherent','ascend');
  else
    data.sort('SNR','ascend');
  end
  SNR = data.SNR;

  % define snrfrac 
  SNRfrac=data.SNRfrac;
  
  % define variable
  det = {'H1','L1'};
  SNR_max     = max(SNR);
  SNRfrac_max = max(SNRfrac);
  
  % Frequency Notch
  freqNotch = load([searchPath '/tmp/frequencies.txt']);
  idx  = [0,find(diff(freqNotch)>1)];
  freqNotchBand = [freqNotch(idx(1:end -1)+1);...
                   freqNotch(idx(2:end))];
  
  %---------------------------------------------------------------------
  %% MAIN
  %---------------------------------------------------------------------
  % 
  % make various plot. 
  % in order to increase speed we define function and we use
  % cellfun for each cut
  % 
  close all

  SNR_THR=25;

  
  %%  GPS vs SNR
  %---------------------------------------------------------------------
  % plot GPS time vs SNR for each ifo and cut
  %
  waitbar(sprintf('diagnostic f1: %30s','GPS vs SNR'));
  logfile('plot : GPS vs SNR');

 
  function f1(cut,leg,idx)
    for ifo=1:2
      figure()
      hold all
      switch cut
        case 'noCut'
          plot(data.GPSstart,SNR,'+b');      
          ll={leg};          
        
        case 'clean'
          idx=idx&(data.SNR>SNR_THR);
          x=find(idx==1);
          plot(data(x).GPSstart,SNR(x),'+b');      
          ll={leg};          
        
        otherwise
          plot(data.GPSstart,SNR,'+b');      
          x=find(idx==0);
          plot(data(x).GPSstart,SNR(x),'*r');      
  	  ll={'All triggers',strrep(leg,'<','>')};
      end
      hold off

      set(gca(),'YScale','log');
      set(gca(),'Box','on');
      set(get(gca(),'XLabel'),'String',[det{ifo} ' GPS']);
      set(get(gca(),'XLabel'),'Fontsize',15);
      set(get(gca(),'YLabel'),'String','SNR');
      set(get(gca(),'YLabel'),'Fontsize',15);
    
      l = legend(ll);
      set(l, 'Location', 'NorthWest');

      SNR_max=max(SNR(idx));
      SNR_min=min(SNR(idx));
      SNR_max=50;
      if strcmp(cut,'clean')
        ylim([SNR_THR 2*SNR_max]);
      else
        ylim([0.9*SNR_min 2*SNR_max]);
      end
      xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
            max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);
      make_png([ff '/' cut], ['GPS' det{ifo} '_SNR']);
      close;
    end 
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f1,cut,leg,idx);
  
  % allCut & clean version
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f1('clean','Remaining triggers',idx);
    f1('allCut','All Cut',idx);
  end


  %%  GPS vs SNRfrac
  %---------------------------------------------------------------------
  % plot GPS vs SNRfrac for each ifo and for various cut
  %
  waitbar(sprintf('diagnostic f2: %30s','GPS vs SNRfrac'));
  logfile('plot : GPS vs SNRfrac');

  function f2(cut,leg,idx)
    for ifo=1:2
      figure()
      hold on
      switch cut
        case 'noCut'
          plot(data.GPSstart(['ifo' num2str(ifo)]),SNRfrac,'+b');
          ll=leg;
        
        case 'clean'
          idx=idx&(data.SNR>SNR_THR);
          x=find(idx==1);
          plot(data(x).GPSstart(['ifo' num2str(ifo)]),SNRfrac(x),'+b');
          ll=leg;
        
        otherwise
          plot(data.GPSstart(['ifo' num2str(ifo)]),SNRfrac,'+b');
          x=find(idx==0);
          plot(data(x).GPSstart(['ifo' num2str(ifo)]),SNRfrac(x),'*r');
          ll={'All triggers',strrep(leg,'<','>')};
      end        
      hold off

      set(gca(),'YScale','log');
      set(gca(),'Box','on');
      set(get(gca(),'XLabel'),'String',[det{ifo} ' GPS']);
      set(get(gca(),'XLabel'),'Fontsize',15);
      set(get(gca(),'YLabel'),'String','SNRfrac');
      set(get(gca(),'YLabel'),'Fontsize',15);
    
      l = legend(ll);
      set(l, 'Location', 'NorthWest');
    
      ylim([0 1]);
      xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
            max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);
      make_png([ff '/' cut], ['GPS' det{ifo} '_SNRfrac'])
      close;
    end
  end  
  
  [leg,m,idx]=decode(cut,data);
  cellfun(@f2,cut,leg,idx);

  % noCut & clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f2('clean','Remaining triggers',idx);
    f2('allCut','All Cut',idx);
  end
  

  %% GPS vs Duration
  %---------------------------------------------------------------------
  % plot duration vs GPS for each ifo and for various cut
  %
  waitbar(sprintf('diagnostic f3: %30s','GPS vs duration'));
  logfile('plot : GPS vs duration');

  function f3(cut,leg,idx)
    for ifo=1:2
      figure()
      hold all
      switch cut
        case 'noCut'
          plot(data.GPSstart(['ifo' num2str(ifo)]), data.duration,'+b');
          ll=leg;
          
        case 'clean'
          idx=idx&(data.SNR>SNR_THR);
          x=find(idx==1);
          plot(data(x).GPSstart(['ifo' num2str(ifo)]), data(x).duration,'+b');
          ll=leg;
          
        otherwise
          plot(data.GPSstart(['ifo' num2str(ifo)]), data.duration,'+b');
          x=find(idx==0);
          plot(data(x).GPSstart(['ifo' num2str(ifo)]), data(x).duration,'*r');
          ll={'All triggers',strrep(leg,'<','>')};
      end
      hold off

      set(gca(),'Box','on');
      set(get(gca(),'XLabel'),'String',[det{ifo} ' GPS']);
      set(get(gca(),'XLabel'),'Fontsize',15);
      set(get(gca(),'YLabel'),'String','Duration [s]');
      set(get(gca(),'YLabel'),'Fontsize',15);
      
      l = legend(ll);
      set(l, 'Location', 'NorthWest');

      xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
            max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);

      make_png([ff '/' cut], ['GPS' det{ifo} '_Duration'])
      close;
    end
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f3,cut,leg,idx);
  
  % allCut & clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f3('clean','Remaining triggers',idx);
    f3('allCut','All Cut',idx);
  end
    


  %% Duration vs SNR
  %---------------------------------------------------------------------
  % plot duration vs SNR for various cut
  %
  waitbar(sprintf('diagnostic f4: %30s','Duration vs SNR'));
  logfile('plot : Duration vs SNR');

  function f4(cut,leg,idx)
    figure()
    hold on
    switch cut
      case 'noCut'
        plot(data.duration,SNR,'+b');
        ll=leg;

      case 'clean'
        idx=idx&(data.SNR>SNR_THR);
        x=find(idx==1);
        plot(data(x).duration,SNR(x),'+b');
        ll=leg;
        
      otherwise
        plot(data.duration,SNR,'+b');
        x=find(idx==0);
        plot(data(x).duration,SNR(x),'*r');
        ll={'All triggers', strrep(leg,'<','>')};
    end
    hold off


    SNR_max=max(SNR(idx));
    if SNR_max<100
      set(gca(),'Xscale','lin');
      set(gca(),'YScale','lin');
    else
      set(gca(),'Xscale','log');
      set(gca(),'YScale','log');
    end
    set(gca(),'Box','on');
    set(get(gca(),'XLabel'),'String','Duration [s]');
    set(get(gca(),'XLabel'),'Fontsize',15);
    set(get(gca(),'YLabel'),'String','SNR');
    set(get(gca(),'YLabel'),'Fontsize',15);

    SNR_max=max(SNR(idx));

    if strcmp(cut,'clean')
      ylim([SNR_THR 2*SNR_max]);
    else
      ylim([5 2*SNR_max]);
    end

    l = legend(ll);
    set(l, 'Location', 'NorthWest');
    
    make_png([ff '/' cut], ['Duration_SNR']);
    close;
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f4,cut,leg,idx);

  % allCut & clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f4('clean','Remaining triggers',idx);
    f4('allCut','All Cut',idx);
  end



  %% SNRfrac vs SNR
  %---------------------------------------------------------------------
  % plot SNRfrac vs SNR for various cut
  %
  waitbar(sprintf('diagnostic f5: %30s','SNRfrac vs SNR'));
  logfile('plot : SNRfrac vs SNR');

  function f5(cut,leg,idx)
    figure()
    hold on
    switch cut
      case 'noCut'
        plot(SNRfrac,SNR,'+b');
        ll=leg;

      case 'clean'
        idx=idx&(data.SNR>SNR_THR);
        x=find(idx==1);
        plot(SNRfrac(x),SNR(x),'+b');
        ll=leg;
        
      otherwise
        plot(SNRfrac,SNR,'+b');
        x=find(idx==0);
        plot(SNRfrac(x),SNR(x),'*r');
        ll={'All triggers', strrep(leg,'<','>')};
    end
    hold off
    
    set(gca(),'YScale','log');
    set(gca(),'Box','on');
    set(gca(),'XGrid','on');
    set(gca(),'YGrid','on');
    set(get(gca(),'XLabel'),'String','SNRfrac');
    set(get(gca(),'XLabel'),'Fontsize',15);
    set(get(gca(),'YLabel'),'String','SNR');
    set(get(gca(),'YLabel'),'Fontsize',15);

    SNR_max=max(SNR(idx));

    xlim([0 1.1])

    if strcmp(cut,'clean')
      ylim([SNR_THR 2*SNR_max]);
    else
      ylim([5 2*SNR_max]);
    end
    
    l = legend(ll);
    set(l, 'Location', 'SouthEast');
    
    make_png([ff '/' cut], ['SNRfrac_SNR']);
    close;
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f5,cut,leg,idx);

  % clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f5('clean','Remaning triggers',idx);
    f5('allCut','All Cut',idx);
  end
  

  %% SNRfrac vs Dur
  %---------------------------------------------------------------------
  % plot SNRfrac vs duration for various cut
  %
  waitbar(sprintf('diagnostic f6: %30s','SNRfrac vs Duration'));
  logfile('plot : SNRfrac vs Duration');

  function f6(cut,leg,idx)
    figure()
    hold on
    switch cut
      case 'noCut'
        plot(SNRfrac,data.duration,'+b');
        ll = leg;

      case 'clean'
        idx=idx&(data.SNR>SNR_THR);
        x=find(idx==1);
        plot(SNRfrac(x),data(x).duration,'+b');
        ll = leg;
        
      otherwise
        plot(SNRfrac,data.duration,'+b');
        x=find(idx==0);
        plot(SNRfrac(x),data(x).duration,'*r');
        ll={'All triggers', strrep(leg,'<','>')};
    end
    hold off

    set(gca(),'YScale','log');
    set(gca(),'Box','on');
    set(get(gca(),'XLabel'),'String','SNRfrac');
    set(get(gca(),'XLabel'),'Fontsize',15);
    set(get(gca(),'YLabel'),'String','Duration [s]');
    set(get(gca(),'YLabel'),'Fontsize',15);
    
    xlim([0 1.1])
    
    l = legend(ll);
    set(l, 'Location', 'SouthWest');
    
    make_png([ff '/' cut], ['SNRfrac_Duration'])
    close;
  end  

  [leg,m,idx]=decode(cut,data);
  cellfun(@f6,cut,leg,idx);
  
  % allCut & clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f6('allCut','All Cut',idx);
    f6('clean','Remaining triggers',idx);
  end
                

  %% Fmin vs SNR
  %---------------------------------------------------------------------
  % plot Fmin vs SNR for various cut
  %
  waitbar(sprintf('diagnostic f7: %30s','Fmin vs SNR'));
  logfile('plot : Fmin vs SNR');

  function f7(cut,leg,idx)  
    figure
    hold on
    switch cut
      case 'noCut'
        plot(data.fmin,SNR,'+b');
        ll={leg};

      case 'clean'
        idx=idx&(data.SNR>SNR_THR);
        x=find(idx==1);
        plot(data(x).fmin,SNR(x),'+b');
        ll={leg};
        
      otherwise
        plot(data.fmin,SNR,'+b');
        x=find(idx==0);
        plot(data(x).fmin,SNR(x),'*r');
        ll={'All triggers',strrep(leg,'<','>')};
    end
    

    SNR_max=max(SNR(idx));

    xlimit=get(gca, 'xlim');
    for fq=1:size(freqNotchBand,2)
      h=rectangle('position', ...
                  [freqNotchBand(1,fq) 5 ...
                   max(diff(freqNotchBand(:,fq)),1) 2*SNR_max-5], ...
                  'FaceColor','g', 'EdgeColor','g' );
      uistack(h,'bottom');
    end
    hold off

    %trick for the rectangle legend
    line(0, 0,'LineWidth',5 ,'Color','g');
    
    set(gca(),'YScale','log');
    set(gca(),'Box','on');
    set(get(gca(),'XLabel'),'String','Minimal frequency [Hz]');
    set(get(gca(),'XLabel'),'Fontsize',15);
    set(get(gca(),'YLabel'),'String','SNR');
    set(get(gca(),'YLabel'),'Fontsize',15);

    if strcmp(cut,'clean')
      ylim([SNR_THR 2*SNR_max]);
    else
      ylim([5 2*SNR_max]);
    end
      
    xlim(xlimit);
    
    l=legend([ll 'Frequency notch']);
    set(l, 'Location', 'NorthEast');
    
    make_png([ff '/' cut], ['MinFreq_SNR'])
    close;
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f7,cut,leg,idx);
  
  % clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f7('allCut','All Cut',idx);
    f7('clean','Remaining triggers',idx);
  end

  %% Fmax vs SNR
  %---------------------------------------------------------------------
  % plot Fmax vs SNR plot various cut
  %
  waitbar(sprintf('diagnostic f8: %30s','Fmax vs SNR'));
  logfile('plot : Fmax vs SNR');

  function f8(cut,leg,idx)
    figure()
    hold on
    switch cut
      case 'noCut'
        plot(data.fmax,SNR,'+b');
        ll={leg};
        
      case 'clean'
        idx=idx&(data.SNR>SNR_THR);
        x=find(idx==1);
        plot(data(x).fmax,SNR(x),'+b');
        ll={leg};
        
      otherwise
        plot(data.fmax,SNR,'+b');
        x=find(idx==0);
        plot(data(x).fmax,SNR(x),'*r');
        ll={'All triggers', strrep(leg,'<','>')};
    end

    ylimit=get(gca,'ylim');
    xlimit=get(gca, 'xlim');
    for fq=1:size(freqNotchBand,2)
      h=rectangle('position', ...
                  [freqNotchBand(1,fq) 5 ...
                   max(diff(freqNotchBand(:,fq)),1) 2*ylimit(2)-5], ...
                  'FaceColor','g', 'EdgeColor','g' );
      uistack(h,'bottom');
    end
    hold off

    %trick for the rectangle legend
    line(0, 0,'LineWidth',5 ,'Color','g');

    set(gca(),'YScale','log');
    set(gca(),'Box','on');
    set(get(gca(),'XLabel'),'String','Maximal frequency [Hz]');
    set(get(gca(),'XLabel'),'Fontsize',15);
    set(get(gca(),'YLabel'),'String','SNR');
    set(get(gca(),'YLabel'),'Fontsize',15);

    if strcmp(cut,'clean')
      ylim([SNR_THR 2*ylimit(2)]);
    else
      ylim([5 2*ylimit(2)]);
    end

    xlim(xlimit);

    l=legend([ll 'Frequency notch']);
    set(l, 'Location', 'NorthEast');
    
    make_png([ff '/' cut], ['MaxFreq_SNR'])
    close;
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f8,cut,leg,idx);
  
  % clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f8('allCut','All Cut',idx);
    f8('clean','Remaining triggers',idx);
  end


  %% SNRfrac1/2 vs SNR1/2
  %---------------------------------------------------------------------
  % plot SNR of one ifo vs the SNR of the other
  %
  waitbar(sprintf('diagnostic f9: %30s','SNRfrac1/2 vs SNR1/2'));
  logfile('plot : SNRfrac1/2 vs SNR1/2');
  
  function f9(cut,leg,idx)
    for ifo=1:2
      figure()
      hold all
      switch cut
        case 'noCut'
	  scatter(data.SNRfrac(['ifo' num2str(ifo)]),data.SNR(['ifo' num2str(ifo)]),[],min(SNR,50),'+');
          ll={leg};          
         
        case 'clean'
          idx=idx&(data.SNR>SNR_THR);
          x=find(idx==1);
          scatter(data(x).SNRfrac(['ifo' num2str(ifo)]),data(x).SNR(['ifo' num2str(ifo)]),[],min(SNR(x),50),'+');
          ll={leg};          
         
        otherwise
          scatter(data.SNRfrac(['ifo' num2str(ifo)]),data.SNR(['ifo' num2str(ifo)]),[],min(SNR,50),'+');
          x=find(idx==0);
          scatter(data(x).SNRfrac(['ifo' num2str(ifo)]),data(x).SNR(['ifo' num2str(ifo)]),[],[1 0 0],'r+');
          ll={'All triggers',strrep(leg,'<','>')};
      end
      hold off

      cc = colorbar();
      if verLessThan('matlab','8.2')
	set(get(cc,'Ylabel'),'String','SNR');
      else
	set(get(cc,'Label'),'String','SNR');
      end

      set(gca(),'XScale','lin');
      set(gca(),'YScale','log');
      set(gca(),'Box','on');
      set(gca(),'XLim',[0 1])
      set(gca(),'YLim',[1e0 1e6])
      set(gca(),'CLim',[0 50]);
      set(gca(),'XMinorGrid','on');
      set(gca(),'YMinorGrid','on');

      set(get(gca(),'XLabel'),'String',['SNRfrac ' det{ifo}]);
      set(get(gca(),'XLabel'),'Fontsize',15);
      set(get(gca(),'YLabel'),'String',['SNR ' det{ifo}]);
      set(get(gca(),'YLabel'),'Fontsize',15);
    
      l = legend(ll);
      set(l, 'Location', 'NorthWest');

      make_png([ff '/' cut], ['SNRfrac' det{ifo} '_SNR' det{ifo}]);
      close;
    end
  end

  if ~strcmp(class(data.format),'classes.format.seedless')
 % if ~isprop(data,'lonetrack')
    [leg,m,idx]=decode(cut,data);
    cellfun(@f9,cut,leg,idx);
  
    % allCut & clean version
    if length(cut)~=1
      [~,~,idx]=decode(allCut,data);
      f9('clean','Remaining triggers',idx);
      f9('allCut','All Cut',idx);
    end
  end


  %% SNR1 vs SNR2
  %---------------------------------------------------------------------
  % plot SNR of one ifo vs the SNR of the other
  %
  waitbar(sprintf('diagnostic f10: %30s','SNR1 vs SNR2'));
  logfile('plot : SNR1 vs SNR2');

  function f10(cut,leg,idx)
    figure()
    hold all
    switch cut
      case 'noCut'
        scatter(data.SNR('ifo1'),data.SNR('ifo2'),[],min(SNR,50),'+');
        ll={leg};          
         
      case 'clean'
        idx=idx&(data.SNR>SNR_THR);
        x=find(idx==1);
        scatter(data(x).SNR('ifo1'),data(x).SNR('ifo2'),[],min(SNR(x),50),'+');
        ll={leg};          
         
      otherwise
        scatter(data.SNR('ifo1'),data.SNR('ifo2'),[],min(SNR,50),'+');
        x=find(idx==0);
        scatter(data(x).SNR('ifo1'),data(x).SNR('ifo2'),[],[1 0 0],'r+');
        ll={'All triggers',strrep(leg,'<','>')};
    end

    RvetoPlot('O3')

    hold off

    cc = colorbar();
    if verLessThan('matlab','8.2')
      set(get(cc,'Ylabel'),'String','SNR');
    else
      set(get(cc,'Label'),'String','SNR');
    end

    set(gca(),'YScale','log');
    set(gca(),'XScale','log');
    set(gca(),'Box','on');
    set(gca(),'XLim',[1e2 1e6])
    set(gca(),'YLim',[1e2 1e6])
    set(gca(),'CLim',[0 50]);
    set(gca(),'XMinorGrid','on');
    set(gca(),'YMinorGrid','on');

    set(get(gca(),'XLabel'),'String',['SNR' det{1}]);
    set(get(gca(),'XLabel'),'Fontsize',15);
    set(get(gca(),'YLabel'),'String',['SNR' det{2}]);
    set(get(gca(),'YLabel'),'Fontsize',15);
    
    l = legend(ll);
    set(l, 'Location', 'NorthWest');

    make_png([ff '/' cut], ['SNR1_SNR2']);
    close;
  end

  if ~strcmp(class(data.format),'classes.format.seedless')
%  if ~isprop(data,'lonetrack')
    [leg,m,idx]=decode(cut,data);
    cellfun(@f10,cut,leg,idx);
    
    % allCut & clean version
    if length(cut)~=1
      [~,~,idx]=decode(allCut,data);
      f10('clean','Remaining triggers',idx);
      f10('allCut','All Cut',idx);
    end
  end
  

  %% GPS vs Fmin vs SNR all triggers
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic f11: %30s','GPS vs Fmin'));
  logfile('plot : GPS vs Fmin ');

  function f11(cut,leg,idx)
    for ifo=1:2
      figure();
      idx=idx&(data.SNR>SNR_THR);
%      scatter(data(idx).GPSstart(['ifo' num2str(ifo)]), ...
%              data(idx).fmin, [], min(SNR(idx),50), '+');
      scatter(data(idx).GPSstart(['ifo' num2str(ifo)]), ...
              data(idx).fmin, [], SNR(idx), '+');

      set(gca(),'Box','on');
      set(get(gca(),'XLabel'),'String',[det{ifo} ' GPS']);
      set(get(gca(),'XLabel'),'Fontsize',15);
      set(get(gca(),'YLabel'),'String','Minimal frequency [Hz]');
      set(get(gca(),'YLabel'),'Fontsize',15);

      xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
            max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);

      c = colorbar;
      title(c, 'SNR');

      make_png([ff '/' cut], ['GPS' det{ifo} '_Fmin_SNR_scatter'])
      close;
    end
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f11,cut,leg,idx);
  
  % clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f11('allCut','All Cut',idx);
    f11('clean','Remaining triggers',idx);
  end


  %% GPS vs Fmax vs SNR all triggers
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic f12: %30s','GPS vs Fmax'));
  logfile('plot : GPS vs Fmax');

  function f12(cut,leg,idx)
    for ifo=1:2
      figure();
      idx=idx&(data.SNR>SNR_THR);
      x=find(idx==1);
      scatter(data(x).GPSstart(['ifo' num2str(ifo)]), ...
              data(x).fmax, [], min(SNR(x),50), '+');

      set(gca(),'Box','on');
      set(get(gca(),'XLabel'),'String',[det{ifo} ' GPS']);
      set(get(gca(),'XLabel'),'Fontsize',15);
      set(get(gca(),'YLabel'),'String','Maximal frequency [Hz]');
      set(get(gca(),'YLabel'),'Fontsize',15);

      xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
            max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);

      c = colorbar;
      title(c, 'SNR');

      make_png([ff '/' cut], ['GPS' det{ifo} '_Fmax_SNR_scatter'])
      close;
    end
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f12,cut,leg,idx);
  
  % clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f12('allCut','All Cut',idx);
    f12('clean','Remaining triggers',idx);
  end




  %% SNRfrac1 vs SNRfrac2
  %---------------------------------------------------------------------
  % plot SNRfrac of one ifo vs the SNRfrac of the other
  %
  waitbar(sprintf('diagnostic 13: %30s','SNRfrac1 vs SNRfrac2'));
  logfile('plot : SNRfrac1 vs SNRfrac2');

  function f13(cut,leg,idx)
    figure()
    hold all
    switch cut
      case 'noCut'
        scatter(data.SNRfrac('ifo1'),data.SNRfrac('ifo2'),[],min(SNR,50),'+');
        ll={leg};          
         
      case 'clean'
        idx=idx&(SNR>SNR_THR);
        x=find(idx==1);
        scatter(data(x).SNRfrac('ifo1'),data(x).SNRfrac('ifo2'),[],min(SNR(x),50),'+');
        ll={leg};          
         
      otherwise
        scatter(data.SNRfrac('ifo1'),data.SNRfrac('ifo2'),[],min(SNR,50),'+');
        x=find(idx==0);
        scatter(data(x).SNRfrac('ifo1'),data(x).SNRfrac('ifo2'),[],[1 0 0],'r+');
        ll={'All triggers',strrep(leg,'<','>')};
    end
    hold off

    cc = colorbar();
    if verLessThan('matlab','8.2')
      set(get(cc,'Ylabel'),'String','SNR');
    else
      set(get(cc,'Label'),'String','SNR');
    end

    set(gca(),'YScale','lin');
    set(gca(),'XScale','lin');
    set(gca(),'Box','on');
    set(gca(),'XLim',[0 1])
    set(gca(),'YLim',[0 1])

    if strcmp(cut,'clean')
      set(gca(),'CLim',[SNR_THR 50]);
    else
      set(gca(),'CLim',[0 50]);
    end

    set(gca(),'XMinorGrid','on');
    set(gca(),'YMinorGrid','on');

    set(get(gca(),'XLabel'),'String',['SNRfrac' det{1}]);
    set(get(gca(),'XLabel'),'Fontsize',15);
    set(get(gca(),'YLabel'),'String',['SNRfrac' det{2}]);
    set(get(gca(),'YLabel'),'Fontsize',15);
    
    l = legend(ll);
    set(l, 'Location', 'NorthWest');

    make_png([ff '/' cut], ['SNRfrac' det{1} '_SNRfrac' det{2}]);
    close;
  end

  if ~strcmp(class(data.format),'classes.format.seedless')
%  if ~isprop(data,'lonetrack')
    [leg,m,idx]=decode(cut,data);
    cellfun(@f13,cut,leg,idx);
  
    % allCut & clean version
    if length(cut)~=1
      [~,~,idx]=decode(allCut,data);
      f13('clean','Remaining triggers SNR>25',idx);
      f13('allCut','All Cut',idx);
    end
  end





  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GENERAL DIAGNOSTIC PLOTS %%%%%%%%%%%%%%

  %% GPS vs SNR
  %---------------------------------------------------------------------
  % plot GPS vs SNR distribution
  %
  waitbar(sprintf('diagnostic : %30s','GPS vs SNR'));
  logfile('plot : GPS vs SNR');

  for ifo=1:2
    figure()
    hist33 (data.GPSstart(['ifo' num2str(ifo)]), ...
            log10(SNR),100,100)
    xlabel([det{ifo} ' GPS'], 'Fontsize', 15);
    ylabel('log_{10}(SNR)', 'Fontsize', 15);
    xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
          max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);
    make_png([ff '/diagnostic'], ['GPS' det{ifo} '_SNR_density'])
    close;
  end
  


  %% GPS vs Fmin
  %---------------------------------------------------------------------
  % plot GPS vs Fmin distribution
  %
  waitbar(sprintf('diagnostic : %30s','GPS vs Fmin'));
  logfile('plot : GPS vs Fmin');

  for ifo=1:2
    figure()
    hist33 (data.GPSstart(['ifo' num2str(ifo)]), ...
            data.fmin,100,100)
    xlabel([det{ifo} ' GPS'], 'Fontsize', 15);
    ylabel('Minimal Frequency [Hz]', 'Fontsize', 15);
    xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
          max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);
    make_png([ff '/diagnostic'], ['GPS' det{ifo} '_Freq_density'])
    close;
  end

  %% GPS vs Dur
  %---------------------------------------------------------------------
  % plot SNR vs duration distribution
  %
  waitbar(sprintf('diagnostic : %30s','GPS vs Duration'));
  logfile('plot : GPS vs Duration');

  for ifo=1:2
    figure()
    hist33 (data.GPSstart(['ifo' num2str(ifo)]), ...
            data.duration,100,50)
    xlabel([det{ifo} ' GPS'], 'Fontsize', 15);
    ylabel('Duration [s]', 'Fontsize', 15);
    xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
          max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);
    make_png([ff '/diagnostic'], ['GPS' det{ifo} '_Duration_density'])
  end


  %% Fmin vs Dur
  %---------------------------------------------------------------------
  % plot Fmin vs duration distribution
  %
  waitbar(sprintf('diagnostic : %30s','Fmin vs Duration'));
  logfile('plot : Fmin vs Duration');

  figure()

  hist33 (data.fmin, data.duration,100,100, 'intbinsy')
  xlabel('Minimal frequency [Hz]', 'Fontsize', 15);
  ylabel('Duration', 'Fontsize', 15);
  make_png([ff '/diagnostic'], 'Freq_Duration_density')
  close;

  %% Fmin vs SNR
  %---------------------------------------------------------------------
  % plot Fmin vd SNR distribution
  %
  waitbar(sprintf('diagnostic : %30s','Fmin vs SNR'));
  logfile('plot : Fmin vs SNR');

  figure()


  if strcmp(class(data.format),'classes.format.seedless')
%  if isprop(data,'lonetrack')
    hist33 (data.fmin, data.SNR,10,10)
  else
    idx=data.SNR<100;
    hist33 (data(idx).fmin, data(idx).SNR,100,100)
  end
  xlabel('Minimal frequency [Hz]', 'Fontsize', 15);
  ylabel('SNR', 'Fontsize', 15);
  make_png([ff '/diagnostic'], 'Fmin_SNR_density')
  close;


  %% Fmax vs SNR
  %---------------------------------------------------------------------
  % plot Fmax vd SNR distribution
  %
  waitbar(sprintf('diagnostic : %30s','Fmax vs SNR'));
  logfile('plot : Fmax vs SNR');

  figure()
  if strcmp(class(data.format),'classes.format.seedless')
  %if isprop(data,'lonetrack')
    hist33 (data.fmax, SNR,10,10)
  else
    idx=data.SNR<100;
    hist33 (data(idx).fmax, SNR(idx),100,100)
  end
  xlabel('Max frequency [Hz]', 'Fontsize', 15);
  ylabel('SNR', 'Fontsize', 15);
  make_png([ff '/diagnostic'], 'Fmax_SNR_density')
  close;

  

  %% lag vs Dur
  %---------------------------------------------------------------------
  % plot lag vs duration distribution
  if config.doZebra==1
    waitbar(sprintf('diagnostic : %30s','lag vs Duration'));
    logfile('plot : lag vs Duration');

    figure()
    hist33(data.lag,data.duration,200,50)
    xlabel('Lag #', 'Fontsize', 15);
    ylabel('Duration [s]', 'Fontsize', 15);
    make_png([ff '/diagnostic'], 'Lag_Duration_density')
    close;
  end




  %% Duration histo
  %---------------------------------------------------------------------
  % plot duration distribution
  waitbar(sprintf('diagnostic : %30s','Duration histo'));
  logfile('plot : Duration histo');

  figure()

  if config.doZebra==1
    xmax=60;
    x=0:.5:xmax;
    outsider=data.duration>xmax;
  else
    xmax=250;
    x=0:1:xmax;
    outsider=data.duration>xmax;
  end

  hist(data(~outsider).duration,x);
  h = findobj(gca,'Type','patch');
  set(h(1),'FaceColor','none','EdgeColor','k');
  xlim([0 xmax+5]);
  grid
  xlabel('Duration [s]', 'Fontsize', 15);
  ylabel('Events nb', 'Fontsize', 15);
  legend(['Overflow (>' num2str(xmax) ') : ' ...
         num2str(sum(outsider)) '[' ...
         num2str(round(1000*sum(outsider)/data.numTriggers)/10) '%]'])
  make_png([ff '/diagnostic'], 'Duration_histo');
  close;

  %% RA
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic : %30s','Ra histo'));
  logfile('plot : Ra histo');

  figure()
  
  [N]=hist(data.ra,24);
  hist(data.ra,24);
  xlabel('RA [h]', 'fontsize', 15);
  ylabel('Counts', 'fontsize', 15)
  xlim([-1 25])

  average = mean(N);
  sigma   = std(N);

  line([0 24], [average average],'Color','red')
  line([0 24], [average-sigma average-sigma],'Color',...
       'red','LineStyle','--');
  line([0 24], [average+sigma average+sigma],'Color',...
       'red','LineStyle','--');
  legend('All triggers', ...
         ['mean: ' num2str(average,'%6.1f')], ...
         ['sigma:' num2str(sigma,'%6.1f')],'Location','SouthEast')
  make_png([ff '/diagnostic'], 'RA')
  close;


  %% Dec
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic : %30s','Dec histo'));
  logfile('plot : Dec histo');

  figure()
  [N]=hist(cos(data.dec*pi/180+pi/2),90);
  hist(cos(data.dec*pi/180+pi/2),90);
  xlabel('cos(dec+\pi/2) [deg]', 'fontsize', 15);
  ylabel('Counts', 'fontsize', 15)
  xlim([-1.05 1.05])
  
  average=mean(N);
  sigma=std(N);

  line([-1 1], [average average],'Color','red')
  line([-1 1], [average-sigma average-sigma],'Color',...
       'red','LineStyle','--');
  line([-1 1], [average+sigma average+sigma],'Color',...
       'red','LineStyle','--');
  legend('All triggers', ...
         ['mean: ' num2str(average,'%6.1f')], ...
         ['sigma:' num2str(sigma,'%6.1f')],'Location','SouthEast')
  make_png([ff '/diagnostic'], 'dec')
  close;





  %% EventsNb vs Lag
  %---------------------------------------------------------------------
  % 
  waitbar(sprintf('diagnostic : %30s','Nb events vs lag'));
  logfile('plot : Nb events vs lag');

  figure()
  lag_max=max(data.lag);
  lag_min=min(data.lag);
  a=zeros(lag_max-lag_min+1,3);
  lags = lag_min:lag_max;
  for i=1:numel(lags)
    ext=data.lag==lags(i);
    a(i,1)=lags(i);
    a(i,2)=sum(ext,1);
    a(i,3)=mean(SNR(ext));
  end

  average=mean(a(:,2));
  sigma=std(a(:,2));

  bar(a(:,1), a(:,2), 'BaseValue', average*.9);
  xlim([lag_min-1 lag_max+1])
  ylim([average*.9 average*1.1])
  line([lag_min lag_max], [average average],'Color','red')
  line([lag_min lag_max], [average-sigma average-sigma],'Color', ...
       'red','LineStyle','--')
  line([lag_min lag_max], [average+sigma average+sigma],'Color', ...
       'red','LineStyle','--')

  legend('data', ...
         ['mean: ' num2str(average,'%6.1f')], ...
         ['sigma:' num2str(sigma,'%6.1f')])

  xlabel('Lag #', 'Fontsize', 15);
  ylabel('Events nb', 'Fontsize', 15);
  make_png([ff '/diagnostic'], 'EventsNb_Lag')
  close;
  clear a;


  %% EventsNb vs windowIdx
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic : %30s','Nb events vs windows idx'));
  logfile('plot : Nb events vs windows idx');

  figure()
  windows=unique(data.windows.idx);
  window_nb=size(windows,1);
  window_max=max(data.windows.idx);

  tab=zeros(window_max,2);
  x=1:1:window_max;

  [N,BIN]=histc(data.windows.idx,x);
  bar(1:window_max, N, 'b')
  xlim([windows(1)-10 windows(end)+10])

  average = mean(N);
  sigma   = std(N);
  line([1 window_max], [average average],'Color','red')
  line([1 window_max], [average-sigma average-sigma],'Color','red', ...
       'LineStyle','--')
  line([1 window_max], [average+sigma average+sigma],'Color','red', ...
       'LineStyle','--')
  legend('All triggers', ...
         ['mean :' num2str(average,'%6.1f')], ...
         ['sigma :' num2str(sigma,'%6.1f')], ...
         'Location','SouthEast');
  xlabel('Window #','Fontsize', 15)
  ylabel('Events nb / window','Fontsize', 15)
  make_png([ff '/diagnostic'], 'EventsNb_window')
  close;

  %% EventsNb vs windows idx histo 
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic : %30s','Nb events vs windows idx histo'));
  logfile('plot : Nb events vs windows idx histo');

  try
    figure()
    histlog(N,1,'b');
    xlabel('Events nb / window','Fontsize', 15)
    ylabel('Counts','Fontsize', 15)
    [max_nb,id]=max(N);
    xlim([0 1.05*max_nb]);
    ylim_sup=get(gca,'ylim');
    line([average average], [ylim_sup(1) ylim_sup(2)], 'Color','red')
    line([average-sigma average-sigma], [ylim_sup(1) ylim_sup(2)], ...
         'Color','red','LineStyle','--')
    line([average+sigma average+sigma], [ylim_sup(1) ylim_sup(2)], ...
         'Color','red','LineStyle','--')
    legend('All triggers', ...
         ['mean :' num2str(average,'%6.1f')], ...
         ['sigma :' num2str(sigma,'%6.1f')], ...
         'Location','Best');
    make_png([ff '/diagnostic'], 'EventsNb_window_histo')
    close;
  catch
  end







  %---------------------------------------------------------------------
  %% Close Function 
  %---------------------------------------------------------------------
  % display end message and delete waitbar & logfile
  %
  logfile('Diagnostic end without errors');
  waitbar(sprintf('diagnostic : [done] %30s',''));
  
  delete(logfile);
  delete(waitbar);


end
