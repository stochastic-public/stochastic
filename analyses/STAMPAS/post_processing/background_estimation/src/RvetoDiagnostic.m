function RvetoDiagnostic(data,folder,searchPath)  
% RvetoDiagnostic : plot Rveto diagnostic plot
% DESCRIPTION :
%   plot various graph to verify that Rveto is well tuned
%   
% SYNTAX :
%   RvetoDiagnostic (data,folder)
% 
% INPUT : 
%    data : SATMAPS triggers object
%    folder : figures folder
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : May 2016
%
  import classes.utils.waitbar

  waitbar('Rveto Diagnostic [init]');

  det={'H1','L1'};

  if isfield(data.veto,'TFveto')
    data=data(~data.veto.TFveto);
  end
  
  data.sort('SNR.coherent','ascend');

  %------------------------------------------------------------
  %% SNR DISTRICUTION
  %------------------------------------------------------------
  %
  waitbar(sprintf('Rveto Diagnostic : %30s','SNR distribution'));

  figure
  scatter(data.SNR('ifo1'),data.SNR('ifo2'),[],min(data.SNR,50),'+');

  xlabel('SNR_{ifo1}')
  ylabel('SNR_{ifo2}')
  title('SNR distribution')

  set(gca(),'YScale','log')
  set(gca(),'XScale','log')
  set(gca(),'XLim',[1e2 1e6])
  set(gca(),'YLim',[1e2 1e6])
  set(gca(),'CLim',[0 50]);
  set(gca(),'XMinorGrid','on');
  set(gca(),'YMinorGrid','on');

  cc = colorbar();
  if verLessThan('matlab','8.2')
    set(get(cc,'Ylabel'),'String','SNR');
  else
    set(get(cc,'Label'),'String','SNR');
  end
  set(get(cc,'Title'),'FontSize',15);
  
  make_png([folder '/Rveto'],'RvetoSNR');
  


  %------------------------------------------------------------
  %% SNRratio time distribution
  %------------------------------------------------------------
  %
  waitbar(sprintf('Rveto Diagnostic : %30s','SNRratio vs time'));

  for ifo=1:2
    figure
    scatter(data.GPSstart(['ifo' num2str(ifo)]),data.SNR('ifo1')./data.SNR('ifo2'),[],min(data.SNR,50));

    xlabel('GPS time [s]')
    ylabel('SNR_{ifo1}/SNR_{ifo2}')
    title('SNRratio time distribution')

    set(gca(),'XMinorGrid','on');
    set(gca(),'YGrid','on');
    set(gca(),'YMinorGrid','on');
    set(gca(),'YScale','log')
    set(gca(),'YLim',[1e-6 1e6])
    set(gca(),'CLim',[0 50]);

    cc = colorbar();
    if verLessThan('matlab','8.2')
      set(get(cc,'Ylabel'),'String','SNR');
    else
      set(get(cc,'Label'),'String','SNR');
    end
    set(get(cc,'Title'),'FontSize',15);

    make_png([folder 'Rveto'],['RvetoSNRratioGPS' det{ifo}]);
  end


  %------------------------------------------------------------
  %% Nb Triggers Rveto cut
  %------------------------------------------------------------
  % 
  waitbar(sprintf('Rveto Diagnostic : %30s','SNRratio distribution'));

  Rmax=max(data.SNR('ifo1')./data.SNR('ifo2'));
  Rmin=min(data.SNR('ifo1')./data.SNR('ifo2'));

  Rx=logspace(log10(0.9*Rmin),log10(1.1*Rmax),100);

  Y=hist(data.SNR('ifo1')./data.SNR('ifo2'),Rx);
  figure
  axis([log10(Rmin) log10(Rmax) 0 1.1*log10(max(Y))]);
  ax = gca();
  hold all
  bar(ax,log10(Rx),log10(Y),...
      'BarWidth', 1, ...
      'basevalue', 0);
  hold off
  
  axis off
  ax1 = axes('position', get(ax, 'position'), ...
             'color', 'none');

  xlabel('SNR_{ifo1}/SNR_{ifo2} cut [s]')
  ylabel('#')
  title('Nb Triggers passing Rveto')
  
  set(ax1,'YLim',[0, 1.1*max(Y)]);
  set(ax1,'XLim',[Rmin, Rmax]);
  set(ax1,'Box','on');
  set(ax1,'YScale','log');
  set(ax1,'XScale','log');
  set(ax1,'XMinorGrid','on');
  set(ax1,'YMinorGrid','on');

  make_png([folder '/Rveto'],['RvetoHist']);


  %------------------------------------------------------------
  %% Nb Triggers Rveto cut for each windows 
  %------------------------------------------------------------
  %
  waitbar(sprintf('Rveto Diagnostic : %30s',['SNRratio distribution by windows ']));

  Rmax=max(data.SNR('ifo1')./data.SNR('ifo2'));
  Rmin=min(data.SNR('ifo1')./data.SNR('ifo2'));
  
  Rx=logspace(log10(0.9*Rmin),1.1*log10(0.9*Rmax),101);
  wid=unique(data.windows.idx);
  
  MAP=zeros(length(Rx),length(wid));

  ww=data.windows.idx;
  rr=data.SNR('ifo1')./data.SNR('ifo2');  

  for i=1:length(ww)
    [~,x]=min(abs(ww(i)-wid));
    if ww(i)-wid<0;x=x-1;end

    [~,y]=min(abs(rr(i)-Rx));
    if rr(i)-Rx<0;y=y-1;end
    
    MAP(y,x)=MAP(y,x)+1;    
  end
  X=unique(data.windows.start.ifo1);


  figure;
  surf(X,Rx,zeros(size(MAP)),'CData',log10(MAP), 'Linestyle','none');
  view(0,90);
  colorbar

  set(gca(),'Box','on');
  set(gca(),'YLim',[min(Rx) max(Rx)]);
  set(gca(),'XLim',[min(X) max(X)]);
  set(gca(),'YScale','log');
  xlabel('GPS time [s]')
  ylabel('SNR_{ifo1}/SNR_{ifo2}')
  title('SNR ratio triggers distribution by windows')

  make_png([folder '/Rveto'],['RvetoHist_Win']);

  %------------------------------------------------------------
  %% Nb Triggers Rveto cut for each Lag 
  %------------------------------------------------------------
  %

  config=readConfig([searchPath '/config_bkg.txt']);
  if config.doZeroLag==0
    waitbar(sprintf('Rveto Diagnostic : %30s',['SNRratio vs lag']));

    Rmax=max(data.SNR('ifo1')./data.SNR('ifo2'));
    Rmin=min(data.SNR('ifo1')./data.SNR('ifo2'));
    
    Rx=logspace(log10(0.9*Rmin),1.1*log10(0.9*Rmax),101);
    lag=unique(data.lag);
  
    MAP=zeros(length(Rx),length(lag));
  
    ll=data.lag;
    rr=data.SNR('ifo1')./data.SNR('ifo2');  

    for i=1:length(ll)
      [~,x]=min(abs(ll(i)-lag));
      if ll(i)-lag<0;x=x-1;end
      
      [~,y]=min(abs(rr(i)-Rx));
      if rr(i)-Rx<0;y=y-1;end
      
      MAP(y,x)=MAP(y,x)+1;    
    end
  
    figure;
    surf(lag,Rx,zeros(size(MAP)),'CData',log10(MAP), 'Linestyle','none');
    view(0,90);
    colorbar
    
    set(gca(),'Box','on');
    set(gca(),'YLim',[min(Rx) max(Rx)]);
    set(gca(),'XLim',[min(lag) max(lag)]);
    set(gca(),'YScale','log');
    xlabel('Lag number')
    ylabel('SNR_{ifo1}/SNR_{ifo2}')
    title('SNR ratio triggers distribution by lag')
    
    make_png([folder '/Revto'],['RvetoHist4']);
  end 

  %------------------------------------------------------------
  %% Nb Triggers Rveto cut for each Lag 
  %------------------------------------------------------------
  %
  if config.doZeroLag==0
    waitbar(sprintf('Rveto Diagnostic : %30s',['SNRratio distribution by lag']));
    
    Rmax=max(data.SNR('ifo1')./data.SNR('ifo2'));
    Rmin=min(data.SNR('ifo1')./data.SNR('ifo2'));
    
    figure;
    scatter(data.lag,data.SNR('ifo1')./data.SNR('ifo2'),[],min(data.SNR,50),'+');
    colorbar
    
    set(gca(),'Box','on');
    set(gca(),'YLim',[min(Rx) max(Rx)]);
    set(gca(),'XLim',[min(lag) max(lag)]);
    set(gca(),'YScale','log');
    xlabel('Lag number')
    ylabel('SNR_{ifo1}/SNR_{ifo2}')
    title('SNR ratio by lag')
    
    make_png([folder '/Rveto'],['RvetoSNR_Lag']);
  end

  %------------------------------------------------------------
  %% CLOSE FUNCTION
  %------------------------------------------------------------
  %
  waitbar(sprintf('Rveto Diagnostic [done] %30s',''));
  delete(waitbar);

end

