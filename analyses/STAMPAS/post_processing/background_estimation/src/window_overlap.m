function [] = window_overlap(SearchPath,Data,varargin)  
% window_overlap :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = window_overlap ()
% 
% INPUT : 
%   overlap [s] between 2 windows (1s default)
%   window_size: size of the windows in seconds (500s default)
%
% OUTPUT :
%
% AUTHOR : 
%   MAB
%   Valentin FREY    
%
% CONTACT : 
%   mabizoua@lal.in2p3.fr
%   valentin.frey@gmail.com
%
% VERSION : 1.2
% DATE : Mar 2016
%

  import classes.utils.waitbar
  import classes.utils.logfile

  %--------------------------------------------------------------
  %% INPUT PARSER
  %--------------------------------------------------------------
  %
  p=inputParser;
  addRequired(p,'SearchPath');
  addRequired(p,'Data');
  if verLessThan('matlab','8.2')
    addParamValue(p,'Folder','');
    addParamValue(p,'Overlap',250,@isnumeric);
    addParamValue(p,'Size',500,@isnumeric);
  else
    addParameter(p,'Folder','');
    addParameter(p,'Overlap',250,@isnumeric);
    addParameter(p,'Size',500,@isnumeric);
  end
  parse(p,SearchPath,Data,varargin{:});
  
  data        = p.Results.Data;
  searchPath  = p.Results.SearchPath;
  if isempty(p.Results.Folder)
    ff        = [searchPath '/figures'];
  else
    ff        = p.Results.Folder;
  end
  overlap     = p.Results.Overlap;
  window_size = p.Results.Size;

  %--------------------------------------------------------------
  %% INIT
  %--------------------------------------------------------------
  %
  % get windows information for each triggers
  %
  waitbar('windows overlap [init]');
  logfile([searchPath '/logfiles/windows_overlap.log']);
  logfile('windows overlap begin ...');

  windows                = data.windows.idx;
  windows_group          = data.windows.group.id;
  windows_group_nb       = data.windows.group.nbWindows;
  windows_group_start    = data.windows.group.start;
  windows_index_in_group = data.windows.group.index;

  offset=0;

  %--------------------------------------------------------------
  %% MAIN
  %--------------------------------------------------------------
  %
  % Series of plot for windows statistic
  %


  %
  %% Tstart 10 Windows 
  %--------------------------------------------------------------
  %
  % select 10 consecutif windows and plot the distribution of time
  % between the start time of the trigger and the start time of the
  % group windows
  %
  waitbar(sprintf('windows overlap : %30s','Tstart 10 Windows'));
  logfile('plots : Tstart 10 Windows');

  % At least 10 consecutive windows
  Nwind = 10;
  sel   = windows_group_nb>Nwind;

  % At least 2 consecutive windows & variable windows activated
  sel1 = windows_group_nb>Nwind & windows_index_in_group==1;
  sel2 = windows_group_nb>Nwind & windows_index_in_group==2;
  sel3 = windows_group_nb>Nwind & windows_index_in_group==3;

  close all
  figure(offset+1)
  x=0:0.5:1000;
  x=[x Inf];
  hold on

  if sum(sel)~=0
    [n]=histc(data(sel).GPSstart-windows_group_start(sel), x);
    s=stairs(x,n);
    ylim([-5 1.3*max(n(1:2000))])
  end
  if sum(sel1)~=0
    [n1]=histc(data(sel1).GPSstart-windows_group_start(sel1), x);
    s1=stairs(x,n1);
    set (s1, 'Color', 'r');
  end
  if sum(sel2)~=0
    [n2]=histc(data(sel2).GPSstart-windows_group_start(sel2), x);
    s2=stairs(x,n2);
    set (s2, 'Color', 'g');
  end
  if sum(sel3)~=0
    [n3]=histc(data(sel3).GPSstart-windows_group_start(sel3), x);
    s3=stairs(x,n3);
    set (s3, 'Color', 'k');
  end

  hold off
  grid

  xlabel('t_{start} - t_{start segment}','fontsize', 15);
  ylabel('Events nb','fontsize', 15);
  title('At least 10 consecutive windows', 'fontsize', 15)
  legend('All triggers', 'triggers from the first window', ...
         'triggers from the second window', 'triggers from the third window')
  make_png([ff '/winstat'],'Tstart_10W_histo')

  xlim([window_size-100, window_size+100])
  make_png([ff '/winstat'], 'Tstart_10W_histo_zoom');


  %
  %% Tstop 10 Windows 
  %--------------------------------------------------------------
  %
  % select 10 consecutif windows and pot the distribution of time
  % between the start time of the trigger and the stop time of the
  % group windows
  %
  waitbar(sprintf('windows overlap : %30s','Tstop 10 Windows'));
  logfile('plots : Tstop 10 Windows');

  figure(offset+2)
  x=0:0.5:1000;
  x=[x Inf];
  hold on

  if sum(sel)~=0
    [n]=histc(data(sel).GPSstop-windows_group_start(sel), x);
    s=stairs(x,n);
    ylim([-5 1.3*max(n(1:2000))])
  end
  if sum(sel1)~=0
    [n1]=histc(data(sel1).GPSstop-windows_group_start(sel1), x);
    s1=stairs(x,n1);
    set (s1, 'Color', 'r');
  end
  if sum(sel2)~=0
    [n2]=histc(data(sel2).GPSstop-windows_group_start(sel2), x);
    s2=stairs(x,n2);
    set (s2, 'Color', 'g');
  end
  if sum(sel3)~=0
    [n3]=histc(data(sel3).GPSstop-windows_group_start(sel3), x);
    s3=stairs(x,n3);
    set (s3, 'Color', 'k');
  end

  hold off

  grid
  xlim([-10 1010])
  xlabel('t_{end} - t_{start segment}','fontsize', 15);
  ylabel('Events nb','fontsize', 15);
  title('At least 10 consecutive windows', 'fontsize', 15)
  legend('All triggers', 'triggers from the first window', ...
         'triggers from the second window', 'triggers from the third window')
  make_png([ff '/winstat'],'Tend_10W_histo')

  xlim([window_size-100 window_size+100])
  make_png([ff '/winstat'], 'Tend_10W_histo_zoom');


  %
  %% Tstart & Tstop 1 Windows 
  %--------------------------------------------------------------
  %
  % select 1 consecutif windows and pot the distribution of time
  % between the start time of the trigger and the start/stop time
  % of the group windows
  %
  waitbar(sprintf('windows overlap : %30s','Tstart&Tstop 1 Windows'));
  logfile('plots : Tstart&Tstop 1 Windows');
  
  % Only 1 consecutive windows
  sel = windows_group_nb(:,1)==1;
  
  if sum(sel)~=1

    % Tstart
    figure(offset+3)
    x=0:0.5:500;
    x=[x Inf];

    [n]=histc(data(sel).GPSstart-windows_group_start(sel), x);
    s=stairs(x,n);

    grid
    xlim([-10 510])
    xlabel('t_{start} - t_{start segment}','fontsize', 15);
    ylabel('Events nb','fontsize', 15);
    title('1 isolated window', 'fontsize', 15)
    legend('All triggers')
    make_png([ff '/winstat'],'Tstart_1W_histo')

    % Tstop
    figure(offset+4)
    x=0:0.5:500;
    x=[x Inf];

    [n]=histc(data(sel).GPSstart-windows_group_start(sel), x);
    s=stairs(x,n);

    grid
    xlim([-10 510])
    xlabel('t_{end} - t_{start segment}','fontsize', 15);
    ylabel('Events nb','fontsize', 15);
    title('1 isolated window', 'fontsize', 15)
    legend('All triggers')
    make_png([ff '/winstat'],'Tend_1W_histo')
  end


  %
  %% Tstart & Tstop 1 Windows 
  %--------------------------------------------------------------
  %
  % Fractional timing histogram
  % No selection. One takes all triggers in all windows. But one
  % excludes the N last windows a[ff '/winstat']ected by non overlapping windows in IFO
  % due to the N shifts. 
  %
  waitbar(sprintf('windows overlap : %30s','Tstart All Windows'));
  logfile('plots : Tstart All Windows');

  figure(offset+5)
  x=0:0.008:1;
  x=[-Inf x Inf];
  n_sum=zeros(1,size(x,2));

  shift=unique(data.lag);
  for i=1:size(shift,1)
    sel=data.lag==shift(i) & ...
        data.GPSstart < windows_group_start+...
        window_size+(window_size-overlap)*(windows_group_nb-1)-...
        shift(i)*window_size;
    if sum(sel)>1
      [n]=histc((data(sel).GPSstart-windows_group_start(sel))./...
                ((window_size-overlap)*(windows_group_nb(sel)-1)-...
                 (shift(i)-1)*window_size),x);
      n_sum=n_sum+n';
    else
      waitbar.warning(['Tstart All windows : Not enough data in this lag: ' num2str(i)])
    end
  end

  stairs(x,n_sum);

  xlim([-.05 1.05])
  xlabel('(t_{start} - t_{start\_seg})/seg\_duration','fontsize', 15);
  ylabel('Events nb','fontsize', 15);
  title('All segments', 'fontsize', 15)
  grid
  
  n_sum   = n_sum(n_sum~=0);
  average = mean(n_sum);
  sigma   = std(n_sum);
  
  line([0 1], [average average],'Color','red')
  line([0 1], [average-sigma average-sigma],'Color','red','LineStyle','--')
  line([0 1], [average+sigma average+sigma],'Color','red','LineStyle','--')
  
  legend('data', ...
         ['mean: ' num2str(average,'%6.1f')], ...
         ['sigma:' num2str(sigma,'%6.1f')], 'Location', 'SouthEast')

  make_png([ff '/winstat'], 'Tstart_allW_frac');



  %
  %% Tstart vs SNR/duration for All Windows 
  %--------------------------------------------------------------
  %
  % Density plots relative time
  % No selection. One takes all triggers in all windows. But one
  % excludes the N last windows a[ff '/winstat']ected by non overlapping windows in IFO
  % due to the N shifts. 
  %

  shift=unique(data.lag);
  evts=[];
  for i=1:size(shift,2)
    sel=data.lag==shift(i) & ...
        data.GPSstart < windows_group_start+...
        window_size+(window_size-overlap)*(windows_group_nb-1)-...
        shift(i)*window_size;
    
    SNR=data(sel).SNR;

    evts=[evts;
          (data(sel).GPSstart-windows_group_start(sel))./ ...
          ((window_size-overlap)*(windows_group_nb(sel)-1)-...
           (shift(i)-1)*window_size) SNR data(sel).duration];
  end


  % Tstart - SNR
  waitbar(sprintf('windows overlap : %30s','Tstart vs SNR All Windows'));
  logfile('plots : Tstart vs SNR All Windows');

  figure(offset+6)
  hist33 (evts(:,1), evts(:,2),1000,100)
  xlabel('(t_{start} - t_{start\_seg})/seg\_duration','fontsize', 15);
  ylabel('SNR','fontsize', 15);
  title('All segments', 'fontsize', 15)
  make_png([ff '/winstat'], 'Tstart_SNR_allW_frac_density');

  % Tstart - duration
  waitbar(sprintf('windows overlap : %30s','Tstart vs Duration All Windows'));
  logfile('plots : Tstart vs Duration All Windows');

  figure(offset+7)
  hist33 (evts(:,1), evts(:,3),1000,100)
  xlabel('(t_{start} - t_{start\_seg})/seg\_duration','fontsize', 15);
  ylabel('Duration [s]','fontsize', 15);
  title('All segments', 'fontsize', 15)
  make_png([ff '/winstat'], 'Tstart_Duration_allW_frac_density');


  %
  %% Tstart vs SNR/duration for All Windows 
  %--------------------------------------------------------------
  %
  % Density plots relative time
  % No selection. One takes all triggers in all windows. But one
  % excludes the N last windows a[ff '/winstat']ected by non overlapping windows in IFO
  % due to the N shifts. 
  %
  evts=[];
  for i=1:size(shift,2)
    sel=data.lag==shift(i) & ...
        data.GPSstop < windows_group_start+...
        window_size+(window_size-overlap)*(windows_group_nb-1)-...
        shift(i)*window_size;
    
    SNR=data(sel).SNR;

    evts=[evts;
          (data(sel).GPSstop-windows_group_start(sel))./ ...
          ((window_size-overlap)*(windows_group_nb(sel,1)-1)-...
           (shift(i)-1)*window_size), ...
          SNR data(sel).duration];
  end


  % Tstop - SNR
  waitbar(sprintf('windows overlap : %30s','Tstop vs SNR All Windows'));
  logfile('plots : Tstop vs SNR All Windows');

  figure(offset+8)
  hist33 (evts(:,1), evts(:,2),1000,100)
  xlabel('(t_{end} - t_{start\_seg})/seg\_duration','fontsize', 15);
  ylabel('SNR','fontsize', 15);
  title('All segments', 'fontsize', 15)
  make_png([ff '/winstat'], 'Tend_SNR_allW_frac_density');

  % Tstop - duration
  waitbar(sprintf('windows overlap : %30s','Tstop vs Duration All Windows'));
  logfile('plots : Tstop vs Duration All Windows');
  
  figure(offset+9)
  hist33 (evts(:,1), evts(:,3),1000,100)
  xlabel('(t_{end} - t_{start\_seg})/seg\_duration','fontsize', 15);
  ylabel('Duration','fontsize', 15);
  title('All segments', 'fontsize', 15)
  make_png([ff '/winstat'], 'Tend_Duration_allW_frac_density');




  
  %
  %% Density plots absolute time
  %--------------------------------------------------------------
  %
  sel=data.duration<50 & windows_group_nb>8 & ...
      data.GPSstart-windows_group_start<2000;

  if sum(sel)~=0

    % Tstart - duration
    waitbar(sprintf('windows overlap : %30s','Tstart vs Duration 8 Windows'));
    logfile('plots : Tstart vs Duration 8 Windows');

    figure(offset+10)
    hist33 (data(sel).GPSstart-windows_group_start(sel),...
            data(sel).duration,1000,49)
    xlabel('t_{start} - t_{start\_seg}','fontsize', 15);
    ylabel('Duration','fontsize', 15);
    title('At least 8 consecutive windows', 'fontsize', 15)
    make_png([ff '/winstat'], 'Tstart_Duration_8W_density');

    xlim([window_size-overlap-100 window_size-overlap+100])
    make_png([ff '/winstat'], 'Tstart_Duration_8W_density_zoom');


    % Tstop - duration
    waitbar(sprintf('windows overlap : %30s','Tstop vs Duration 8 Windows'));
    logfile('plots : Tstop vs Duration 8 Windows');
    
    figure(offset+11)
    hist33 (data(sel).GPSstop-windows_group_start(sel), ...
            data(sel).duration,1000,49)
    xlabel('t_{end} - t_{start\_seg}','fontsize', 15);
    ylabel('Duration','fontsize', 15);
    title('At least 8 consecutive windows', 'fontsize', 15)
    make_png([ff '/winstat'], 'Tend_Duration_8W_density');

    xlim([window_size-overlap-100 window_size-overlap+100])
    make_png([ff '/winstat'], 'Tend_Duration_8W_density_zoom');
  end



  %
  %% SNR density plot
  %--------------------------------------------------------------
  %
  S=data.SNR;

  sel=S<30 & windows_group_nb>8 & ...
      data.GPSstart-windows_group_start<2000;

  if sum(sel)~=0
    % Tstop - duration
    waitbar(sprintf('windows overlap : %30s','Tstart vs SNR 8 Windows'));
    logfile('plots : Tstart vs SNR 8 Windows');

    figure(offset+12)
    hist33 (data(sel).GPSstart-windows_group_start(sel), S(sel),1000,98)
    xlabel('t_{start} - t_{start\_seg}','fontsize', 15);
    ylabel('SNR','fontsize', 15);
    title('At least 8 consecutive windows', 'fontsize', 15)
    make_png([ff '/winstat'], 'Tstart_SNR_8W_density');

    xlim([window_size-overlap-100 window_size-overlap+100])
    make_png([ff '/winstat'], 'Tstart_SNR_8W_density_zoom');
    
    
    % Tstop - duration
    waitbar(sprintf('windows overlap : %30s','Tstop vs SNR 8 Windows'));
    logfile('plots : Tstop vs SNR 8 Windows');

    figure(offset+13)
    hist33 (data(sel).GPSstop-windows_group_start(sel), S(sel),1000,98)
    xlabel('t_{end} - t_{start\_seg}','fontsize', 15);
    ylabel('SNR','fontsize', 15);
    title('At least 3 consecutive windows', 'fontsize', 15)
    make_png([ff '/winstat'], 'Tend_SNR_8W_density');

    xlim([window_size-overlap-100 window_size-overlap+100])
    make_png([ff '/winstat'], 'Tend_SNR_8W_density_zoom');
  end


  %--------------------------------------------------------------
  %% CLOSE FUCNTION
  %--------------------------------------------------------------
  %
  waitbar(sprintf('windows overlap [done] %30s',' '));
  logfile('windows overlap done without errors');
  
  delete(waitbar);
  delete(logfile);
end


