function [] = extract(searchPath,varargin)  
% run : run all the extract triggers process for a STAMPAS dag
% DESCRIPTION :
%    run the different step needed to get cleaned triggers use for
%    the post-processing.
%    - merge all triggers created by run_clustermap into a
%    results_merged file. 
%    - remove double triggers due to overlap. 
%    - for injection associated triggers to an injection
%    - apply DQFlags
%    - apply TFveto (if requested)
%    - apply Rveto (for zebragard)
%
%    - merge stage2 lonetrack files created by run_lonetrackpproc
%
% SYNTAX :
%   extract(searchPath,TFveto)
% 
% INPUT : 
%    searchPath : Path to the dag
%                 eg. BKG/Start-1126073342_Stop-1137283217_ID-2
%    TFveto [OPTIONAL] : TFveto file 
%    
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Mar 2016
%

  IFO1='H1';
  IFO2='L1';


  %----------------------------------------------------------------------
  %% INIT
  %----------------------------------------------------------------------
  % define all varibles needed 
  start   = str2num(regexprep(searchPath, '.*Start-([0-9]*)_.*','$1'));
  stop    = str2num(regexprep(searchPath, '.*Stop-([0-9]*)_.*','$1'));
  id      = str2num(regexprep(searchPath, '.*ID-([0-9]*)_.*','$1'));
  type    = regexprep(searchPath,'(BKG|INJ)/.*', '$1');
  run     = get_run_from_GPStimes(start, stop);
  m_path  = ['../../' searchPath '/results/results_merged'];
  config  = readConfig(['../../' searchPath '/config_' lower(type) '.txt']);
  veto_filename_ifo1 = ['../../' upper(run) '/' IFO1 '_DQveto.txt'];
  veto_filename_ifo2 = ['../../' upper(run) '/' IFO2 '_DQveto.txt'];
  veto    = get_dqflags(veto_filename_ifo1,veto_filename_ifo2);
  if strcmp(type,'INJ')
    [dt,df] = get_resolution(['../../' searchPath '/tmp/params_inj_1_1_1.txt']);
  else
    [dt,df] = get_resolution(['../../' searchPath '/tmp/params_H1.txt']);
  end

  config.searchPath=searchPath;

  %% FILE DEFINITION
  % for BKG file = results
  % for INJ one file for each wvf & alphas
  if strcmp(type, 'BKG')
    folder={'results'};
    
  elseif strcmp(type, 'INJ')
    params  = get_waveform_parameters(searchPath);
    
    if config.nullWaveform
      folder={'results/results_noWaveform'};
    else
      folder={};
    end

    for wvf=1:params.nb_wvfs
      for al=1:numel(params.alphas{wvf})
        injName= ...
            char(strcat(params.waveform_names(wvf),'_', ...
                        num2str(params.alphas{wvf}(al),'%0.10f')));
        folder=[folder,['results/results_' injName]];
      end
    end
  else
    error([ type 'must be INJ or BKG']);
  end

  %----------------------------------------------------------------------
  %% Check consistency for Lonetrack
  %----------------------------------------------------------------------
  if config.doSeedless & config.inj==0
    if (config.doLonetrackPProc & config.doZeroLag==1) | (config.doLonetrack & config.doZeroLag==0)
      error('zerolag dag with doLonetrackPProc or background with doLonetrack');
    end
  end


  %----------------------------------------------------------------------
  %% MAIN
  %----------------------------------------------------------------------
  for ii=folder

    %% init
    %------------------------------------------------------------------
    MFile  = [regexprep(ii{:},'(results[/]*)+','results') '_merged.mat'];
    RDFile = [regexprep(ii{:},'(results[/]*)+','results') '_NODBL.mat'];
    FFile  = [regexprep(ii{:},'(results[/]*)+','results') '_filtered.mat'];

    fprintf('processing file : %s\n', ii{:});

    % get files 
    rfiles = getFiles(['../../' searchPath '/' ii{:}],'results_[0-9]*.mat');
    ifiles = getFiles(['../../' searchPath '/' ii{:}],'injections_[0-9]*.mat');
    
    % if no file available don't extract triggers
    if isempty(rfiles);continue;end

    %% MERGE STEP
    %------------------------------------------------------------------
    if exist([m_path '/' MFile]) == 0
      if exist(m_path) == 0
        system(['mkdir ' m_path]);
      end
      
      % merge
      if ~isempty(rfiles);
	data = merge(rfiles,searchPath);
      end

      if strcmp(type,'INJ')&&~strcmp(ii{:},'results/results_noWaveform')
        if ~isempty(ifiles);
	  injections = merge(ifiles,searchPath);
	end
        save([m_path '/' regexprep(MFile,'results','injections')],'injections')
      end

      data.save([m_path '/' MFile])
      
    else
      display(['Merged folder already exists. Moving to next step...']);
    end


    %% REMOVE DOUBLE STEP
    %------------------------------------------------------------------
    if exist([m_path '/' RDFile])==2
      display(['NODBL files already exists.  Moving to next step...']);
    else
      try 
        data;
      catch
        if exist([m_path '/' MFile ])
          load([m_path '/' MFile]);
        else
          error('You should merge before remove double');
        end
      end
      
      %if lonetrack do not remove double
      if config.doSeedless
        display('lonetrack study : pass to the next file');
      else
        remove_double(data,'dt',dt,'df',df);
      end
      
      data.save([m_path '/' RDFile]);
    end


    %% ASSOCIATE INJECTION STEP
    %------------------------------------------------------------------
    % don't associate injection if nowaveform run or BKG run
    if ~strcmp(ii{:},'results/results_noWaveform')&&strcmp(type,'INJ')
      try
        data;
      catch
        if exist([m_path '/' RDFile])
          load([m_path '/' RDFile]);
        else
          error('You should remove double before using DQFlags');
        end
      end

      try
        injections;
      catch
        if exist([m_path '/' strrep(MFile,'results_','injections_')])
          load([m_path '/' strrep(MFile,'results_','injections_')]);
        else
          error('You should remove double before using DQFlags');
        end
      end

      args={data,injections,config.injectionSeparation};
      
      % if nullwaveform option set get the noWaveform results
      if config.nullWaveform
        if exist([m_path '/results_noWaveform_filtered.mat']) ~= 2
          error('please process the nowaveform analysis first');
        else
          args={args{:},getfield(matfile([m_path '/results_noWaveform_filtered.mat']),'data')};
        end
      end

      % associated injection
      if data.numTriggers ~= 0
        if exist([m_path '/' FFile])
          system(['rm ' m_path '/' FFile]);
        end
        
        associate_injections(args{:});
      end
      data.save([m_path '/' FFile]);
    end
    

    %% DQFlags STEP
    %------------------------------------------------------------------
    try
      data;
    catch
      if exist([m_path '/' FFile])&&strcmp(type,'INJ')
        load([m_path '/' FFile]);
      elseif strcmp(type,'BKG')&&exist([m_path '/' RDFile])
        load([m_path '/' RDFile]);
      else
        error('You should remove double before using DQFlags');
      end
    end

    DQFlags(data,veto);
    data.save([m_path '/' FFile]);


   
    if config.doSeedless
      display('lonetrack study : pass to the next file');
    else
      %% TFveto STEP
      %------------------------------------------------------------------
      if nargin==2 && ~isempty(varargin{1})
	try
	  data;
	catch
	  if exist([m_path '/' FFile])
	    load([m_path '/' FFile]);
	  elseif exist([m_path '/' RDile])
	    load([m_path '/' RDFile]);
	  else
	    error('You should remove double before using TFveto');
	  end
	end
	
	TFveto(data,varargin{1});
	data.save([m_path '/' FFile]);
      end
      

      %% Rveto STEP
      %------------------------------------------------------------------
      if true %nargin==2 && ~isempty(varargin{1})
	try
	  data;
	catch
	  if exist([m_path '/' FFile])
	    load([m_path '/' FFile]);
	  elseif exist([m_path '/' RDile])
	    load([m_path '/' RDFile]);
	  else
	    error('You should remove double before using Rveto');
	  end
	end
      
        Rveto(run,data);
	data.save([m_path '/' FFile]);
      end
    end

    
    %% Clear Step
    %------------------------------------------------------------------
    clear data injections  
  end

  % Lonetrack specific functions
  if config.doLonetrackPProc & config.inj==0
    lonetrackPath = ['../../' searchPath '/LonetrackPProc/'];
    bkndoutputDir = [lonetrackPath '/bknd'];
    stage2_triggers = [bkndoutputDir '/stage2_triggers.mat'];
    stage2_far = [bkndoutputDir '/stage2_far.mat'];

    if ~exist(stage2_triggers) || ~exist(stage2_far)
       merge_lonetrackpproc(['../../' searchPath])
    end
  end

  if (config.doLonetrack || config.doLonetrackPProc) & config.inj==0
    zerolonetrackPath = ['../../' searchPath '/Lonetrack/'];
    zerobkndoutputDir = [zerolonetrackPath '/bknd'];

    stage1_triggers = [zerobkndoutputDir '/stage1_triggers.mat'];
    stage1_far = [zerobkndoutputDir '/stage1_far.mat'];

    if ~exist(stage1_triggers) || ~exist(stage1_far)
      merge_lonetrack(['../../' searchPath])
    end
  end





