function DQFlags(data,veto)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This script reads the filtered triggers, apply each IFO DQ vetoes
%% and fills column 15 and 16 with 0 (not vetoed) and 1 (vetoed)
%%
%% Contact: Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
%% Modified by Valentin FREY (frey@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  import classes.utils.waitbar
 
  if isprop(data,'lonetrack') 
    disp('IFO 1 : ');
    if data.lonetrack.ifo1.numTriggers >0
      DQFlags(data.lonetrack.ifo1,veto);
    end

    disp('IFO 2 :');
    if data.lonetrack.ifo1.numTriggers >0
      DQFlags(data.lonetrack.ifo2,veto);
    end

    disp('COHERENT : ');
  end


  waitbar(sprintf('DQFlags [init]')); 
  
  w1  = zeros(data.numTriggers,1);
  w2  = zeros(data.numTriggers,1);
  fraction1  = zeros(data.numTriggers,1);
  fraction2  = zeros(data.numTriggers,1);
  
  if ~isempty(veto.ifo1)
    %%% veto ifo 1
    w1(arrayfun(@(ts,te) logical(sum(ts<veto.ifo1(:,2) & te>veto.ifo1(:,1))), ...
                data.GPSstart('ifo1'),data.GPSstop('ifo1')))=1;
    %               data.windows.start.ifo1+data.SNRfracTime,data.windows.start.ifo1+data.SNRfracTime+3))=1;    
%    ext=find(w1>0);
%    for i=1:size(ext,1)
%      idx=find(data(ext(i)).GPSstart('ifo1')<veto.ifo1(:,2) & data(ext(i)).GPSstop('ifo1')>veto.ifo1(:,1));
%      if size(idx,1)>0
%	fraction1(ext(i))=(veto.ifo1(idx(1),2)-veto.ifo1(idx(1),1))/(data(ext(i)).GPSstop('ifo1')-data(ext(i)).GPSstart('ifo1'));
%	display(['fraction:' num2str(data(ext(i)).GPSstart('ifo1')) ...
%		 ' ' num2str(data(ext(i)).GPSstop('ifo1')) ' ' num2str(fraction1(ext(i)))]);
%      end
%    end
  end
  
  if  ~isempty(veto.ifo2)       
    %%% veto ifo 2    
    w2(arrayfun(@(ts,te) logical(sum(ts<veto.ifo2(:,2) & te>veto.ifo2(:,1))), ...
                data.GPSstart('ifo2'),data.GPSstop('ifo2')))=1;
    %                data.windows.start.ifo2+data.SNRfracTime,data.windows.start.ifo2+data.SNRfracTime+3))=1;    

%    ext=find(w2>0);
%    for i=1:size(ext,1)
%      idx=find(data(ext(i)).GPSstart('ifo2')<veto.ifo2(:,2) & data(ext(i)).GPSstop('ifo2')>veto.ifo2(:,1));
%      if size(idx,1)>0
%	fraction2(ext(i))=(veto.ifo2(idx(1),2)-veto.ifo2(idx(1),1))/(data(ext(i)).GPSstop('ifo2')-data(ext(i)).GPSstart('ifo2'));
%	display(['fraction:' num2str(data(ext(i)).GPSstart('ifo2')) ...
%		 ' ' num2str(data(ext(i)).GPSstop('ifo2')) ' ' num2str(fraction2(ext(i)))]);
%      end
%    end

  end
  
  %%% fill the veto fields 
  data.addFields('veto.ifo1',w1);
  data.addFields('veto.ifo2',w2);
%  data.addFields('veto.frac1',fraction1)
%  data.addFields('veto.frac2',fraction2)
  
%  waitbar(sprintf('DQFlags [done]'));
  
  %%% count the number of triggers vetoed
  veto1=data.veto.ifo1;
  veto2=data.veto.ifo2;
  
  %%% Print results to screen.
  waitbar.warning(sprintf(['number of DQ vetoed triggers ' ...
                      ': [%i %i]/%i\n\n'], sum(veto1), ...
                          sum(veto2), data.numTriggers));
  
  %%% delete waitbar
  delete(waitbar);
end
