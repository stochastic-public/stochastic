function out=merge(files,searchPath)
% load all the results files
%
% DESCRIPTION :
%   Load every data in the files and meged then into one
%   output variable. 
%
% SYNTAX :
%   [out]=merge(files)
%
% INPUT :
%   files : list of files to merged
%
% OUTPUT :
%   data : triggers merged 
%
% AUTHOR : 
%   Tanner Prestegard
%   Valentin FREY (modified by) 
%   
% CONTACT :
%   prestegard@physics.umn.edu 
%   frey@lal.in2p3.fr
%
  import classes.utils.waitbar
  
  dict_ci_path = [searchPath, '/ContWaveforms/dic_ci.yml'];
  
  % If continuum injection run then, we will load the temporarily generated dictionary instead
  if exist(dict_ci_path)==2
    dic=classes.waveforms.dictionary(dict_ci_path);
  else 
    dic = classes.waveforms.dictionary;
  end

  %---------------------------------------------------
  %% INIT
  %---------------------------------------------------
  % init the waitbar to display the progress of the 
  % merging
  %
  %fprintf('Nb of files %f\n',numel(files))  
  waitbar(sprintf('merge [init]'));
  
  % initialize waitbar progesse
  i=0;
  

  %---------------------------------------------------
  %% MAIN
  %---------------------------------------------------
  % loop over the file to merge. save the content of the .mat file
  % in a tmp variable. 
  %

  for ff=files
    % increment
    i=i+1;

    % Progress bar
    waitbar(sprintf('merge [%3.0f]', 100*i/numel(files)));
    %fprintf('merge [%3.0f]', 100*i/numel(files));
    %fprintf('\t%s\n',ff{:})

    % get data from matfile
    m=matfile(ff{:});
    k=fieldnames(m);
    data=m.(k{end});

    if ~isa(data,'classes.data.dataInterface')
      if isstruct(data)
        if isfield(data,'lonetrack')

	  for I=1:numel(data)
	    data(I).windows.start.ifo1=data(I).start.ifo1;
            data(I).windows.start.ifo2=data(I).start.ifo2;
          end
	  data=classes.data.triggers(data,'lonetrack');

        elseif isfield(data,'hrss')
          wvf=dic.waveforms(regexprep(data.name,'waveforms/',''));

          data.fmin=wvf.fmin;
          data.fmax=wvf.fmax;
          data.duration=wvf.duration;
          data.alpha=str2num(regexprep(ff{:},'.*results/results.*_([0-9]*.[0-9]*)/.*','$1'));

          data=classes.data.injections(data); 
        else
          waitbar.warning(sprintf('unable to read file %s',ff{:}));
          %fprintf('unable to read file %s',ff{:});
          continue
        end
      else
        waitbar.warning(sprintf('bad file : %s, merging skip it',ff{:}))
        %fprintf('bad file : %s, merging skip it',ff{:})
        continue;
      end
    end

    if isprop(data,'lonetrack') && exist('SNR_IFO_THR.txt')
      [~,snr_thr_cut_ifo1] = system(['grep IFO1 SNR_IFO_THR.txt| '...
                          'awk ''{print $2}''']);
      [~,snr_thr_cut_ifo2] = system(['grep IFO2 SNR_IFO_THR.txt| ' ...
                          'awk ''{print $2}''']);
      snr_thr_cut_ifo1 = str2num(snr_thr_cut_ifo1);
      snr_thr_cut_ifo2 = str2num(snr_thr_cut_ifo2);
      data = data(data.lonetrack.ifo1.SNR>=snr_thr_cut_ifo1|...
                  data.lonetrack.ifo2.SNR>=snr_thr_cut_ifo2);
    else
      if ~exist('SNR_IFO_THR.txt')
	waitbar.warning('SNR_IFO_THR.txt does not exist')
      end
    end

    if isempty(data)||data.numElements==0
      continue;
    else

      if exist('out') ~= 1
        if strcmp(class(data.format),'classes.format.seedless')
          out = classes.data.triggers('seedless');
         
          %if isprop(data,'lonetrack')
          %  rmprops(data,'lonetrack')
          %end

        else
          out = classes.data.triggers('zebragard');
        end
      end

      try
        if isprop(data,'inj_type') || isprop(data,'lonetrack')
          if isprop(data,'lonetrack')
            rmprops(data,'lonetrack')
          end
          out=out+data;
	else
	  out=out+data(data.SNR>=0);
	end
      catch
        waitbar.warning(fprintf('cannot merge data from file %s\n',ff{:}))
        %fprintf('cannot merge data from file %s\n',ff{:})
      end
    end

    clear data;
  end   

  %---------------------------------------------------
  %% CLOSE FUNCTION
  %---------------------------------------------------
  % display end message and delete waitbar
  %
  waitbar(sprintf('merge [done]'));
  %fprintf('merge [done]');
  delete(waitbar);
  
end


