function Rveto(run,data)  
% Rveto : compare SNR of each ifo an veto all triggers that have an
% asymetric SNR 
%
% DESCRIPTION :
%   Rveto is using the following cut :
%   ~(log10(SNR1/SNR2) > R1 | log10(SNR2/SNR1) > R2) |
%   (log10(SNR1) > THR1 & log10(SNR2) > THR2)
%   
%   R1   : 0.4
%   R2   : 0.4
%   THR1 : 3
%   THR1 : 3
%
% in the future R1, R2, THR1, THR2 will be give as argument
% 
% SYNTAX :
%   Rveto (data)
% 
% INPUT : 
%    data : classes.data.triggers object
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : May 2016
%
  import classes.utils.waitbar

  %if ~isstruct(data.SNR);return;end

  waitbar('Rveto [init]');

  [names,values]=textread(['../../' run '/Rveto.txt'], '%s %s\n',-1,'commentstyle','matlab');

  for ii=1:length(names)
    switch names{ii}
      case 'R1'
        R1 = str2num(values{ii});
      case 'R2'
        R2 = str2num(values{ii});
      case 'THR1'
        THR1 = str2num(values{ii});
      case 'THR2'
        THR2 = str2num(values{ii});
    end
  end

%  display('Rveto applied with the following parameters:');
%  display('Data pass if:');
%  display(['log10(SNR_ifo1) >' num2str(THR1) ' & log10(SNR_ifo2) >' ...
%	   num2str(THR2)]);
%  display('or')
%  display(['SNR_ifo1/SNR_ifo2 <' num2str(R1) ' & SNR_ifo2/SNR_ifo1 <' ...
%	   num2str(R2)])


  cut=~(data.SNR('ifo1')./data.SNR('ifo2')>R1|...
        data.SNR('ifo2')./data.SNR('ifo1')>R2);
  cut=cut| (log10(data.SNR('ifo1'))>THR1& ...
            log10(data.SNR('ifo2'))>THR2);
  data.addFields('veto.Rveto',~cut);



  waitbar.warning(sprintf('Nb triggers vetoed : %d/%d',sum(~cut),data.numTriggers));
  waitbar('Rveto [done]');

  delete(waitbar);
end

