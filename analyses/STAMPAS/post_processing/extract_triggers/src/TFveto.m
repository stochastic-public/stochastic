function TFveto(data,file)
% TFveto : apply some veto define in a text file 
% DESCRIPTION :
%   Veto define in a .txt file in the format of [ifo tmin tmax fmin
%   fmax]. If more than 10% of the triggers is on the veto zone
%   rectangle, then flag the triggers. 
% 
% SYNTAX :
%   TFveto (data,file)
% 
% INPUT : 
%    data : triggers dataInterface object
%    file : the .txt veto definition 
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.1
% DATE : Oct 2015
%

  import classes.utils.waitbar
    
  waitbar(sprintf('TFveto [init]'));
  
  thr = 0.1;
  v   = false(data.numTriggers,1);
  
  % read veto file
  [veto.ifo, veto.start, veto.stop, veto.fmin, veto.fmax] = ...
      textread(file, '%s %f %f %f %f', 'commentstyle', 'matlab');
  
  for ii=1:numel(veto.start)
    waitbar(sprintf('TFveto [%3.0f]',100*ii/numel(veto.start)));

    % select only triggers that overlap in time with the veto
    if strcmp(veto.ifo(ii),'H1')
      sub=data.GPSstart>=veto.start(ii) & ...
          data.GPSstop<=veto.stop(ii);
      start=data.GPSstart;
      stop =data.GPSstop; 
    else
      sub=data.GPSstart('ifo2')>=veto.start(ii) & ...
          data.GPSstop('ifo2')<=veto.stop(ii);
      start=data.GPSstart('ifo2');
      stop =data.GPSstop('ifo2'); 
    end

    c1=find(sub>0);
    if sum(sub)~=0
      sub_data=[ ...
          start(sub), ...
          stop(sub),...
          data.fmin(sub), ...
          data.fmax(sub), ...
               ];
      
      olap_t = min(sub_data(:,2),veto.stop(ii))-...
               max(sub_data(:,1),veto.start(ii));
      olap_f = min(sub_data(:,4),veto.fmax(ii))-...
               max(sub_data(:,3),veto.fmin(ii));
      t_area = (sub_data(:,2)-sub_data(:,1)).*...
               (sub_data(:,4)-sub_data(:,3));
      ratio = (olap_t .* olap_f)./t_area;
      
      c1(ratio<thr)=[];
    end
    v(c1)=true;


%%%% 
if 0
    waitbar.warning(sprintf(['veto %2d [%.1f %.1f %4.1f %4.1f] ' ...
                        ': Nb triggers vetoed %i/%i'], ...
                            ii, veto.start(ii), veto.stop(ii), ...
                            veto.fmin(ii), veto.fmax(ii), numel(c1), ...
                            data.numTriggers));
end
%%%% 




  end
  
  % fill TFveto field
  data.addFields('veto.TFveto',v);

  % display end message & close waitbar
  waitbar(sprintf('TFveto [done]'));  
  delete(waitbar);
end

