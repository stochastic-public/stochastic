function associate_injections(data,injections,sep,varargin)  
% associate_injections : associate triggers to injections  
% DESCRIPTION :
%   associate triggers to a list of injection. association is made
%   using overlap in time & frequency
%
% SYNTAX :
%   associate_injections (data,injections,option)
% 
% INPUT : 
%   data : triggers dataInterface object 
%   injections : injections dataInterface object
%   options :
%     bkg : dataInterface object containing bkg triggers
%
% AUTHOR : 
%   M. A. Bizouard
%   Valentin FREY (Modified by)
%   
% CONTACT : 
%   mabzoua@lal.in2p3.fr
%   frey@lal.in2p3.fr
%
% VERSION : 1.2
% DATE : Fev 2016
%

%  import classes.utils.waitbar
  
%  waitbar(sprintf('associate injections %s','[init]'));
%  waitbar.warning(['Total nb of triggers:' num2str(data.numTriggers)]);  
  display(sprintf('associate injections %s','[init]'));
  display(['Total nb of triggers:' num2str(data.numTriggers)]);  
 
  if nargin==4;
    BKG=varargin{1};
  end

  injectionsTmp = unique(injections.startGPS);

  % ------------------------------------------------------------
  %% INIT
  % ------------------------------------------------------------
  % Keep only triggers with the good frequency range 
  % IE the expected frequency range of the injections
  % Selection done also on the trigger list with no
  % injections if available
  %

  if isa(data.format,'classes.format.seedless')
    filterFreq=true(data.numTriggers,1);
  else
    filterFreq=data.fmin<=injections.fmax&data.fmax>=injections.fmin;
  end

  triggers=data(filterFreq);
  data.removeElements(~filterFreq);
  rmTriggers=false(data.numTriggers,1);

%  waitbar.warning(['Total nb of triggers in inj freq boundaries:' num2str(data.numTriggers)]);  
  display(['Total nb of triggers in inj freq boundaries:' num2str(data.numTriggers)]);  

  if nargin==4
    filterFreqBkg = BKG.fmin<=injections.fmax &...
                    BKG.fmax>=injections.fmin;
    BKG=BKG(filterFreqBkg);
    %  waitbar.warning(['Total nb of triggers in nowaveform:' num2str(BKG.numTriggers)]);    
    display(['Total nb of triggers in nowaveform:' num2str(BKG.numTriggers)]);
  end
  %
  % For each injection window, keep only the signals with start and
  % end times compatible with the injection.
  %
%  injectionsTmp=[injectionsTmp injectionsTmp+injections.duration];
  injectionsTmp=[injectionsTmp injectionsTmp+sep];

  for inj=1:size(injectionsTmp,1)
%    waitbar(sprintf('associate injection [%3.0f]',100*inj/length(injectionsTmp)));
%    display(sprintf('associate injection [%3.0f]',100*inj/length(injectionsTmp)));
    
    if triggers.numTriggers ~= 0
      c1=triggers.GPSstart<=injectionsTmp(inj,2)&...
         triggers.GPSstop>=injectionsTmp(inj,1);
    else
      c1=[];
    end

    if isempty(c1)
      display('no trigger in time window')
      continue
    end
    triggersTmp=triggers(c1);
    c1i=find(c1==1);
    % Filtering done on the list without injections
    if nargin==4
      if BKG.numTriggers ~= 0
        c1b=BKG.GPSstart<=injectionsTmp(inj,2) ...
            &BKG.GPSstop>=injectionsTmp(inj,1);
        BKGTmp=BKG(c1b);
      else
        BKGTmp=[];
      end
      %% A trigger found even in the absence of injection is rejected.
      %% A bkg trigger and a inj trigger is identical if
      %% starting/ending time and frequency are strictly identical
      %% Consequence: an weak injection done inside a glitch will
      %% be declared "seen", but a signal amplified by a glitch
      %% will be detected in real life.
      if ~isempty(BKGTmp)& triggersTmp.numTriggers~=0
        B=[BKGTmp.fmin ,BKGTmp.fmax, ...
           BKGTmp.tmin, BKGTmp.tmax];
        A=[triggersTmp.fmin, triggersTmp.fmax,...
           triggersTmp.tmin, triggersTmp.tmax];
        falseAlarms=ismember(A,B,'rows');

        if sum(falseAlarms)>0	  
          for kk=1:size(B,1)
            S=triggersTmp.SNR;
            BS=BKGTmp.SNR;

            deltaSNR_overSNR=abs(S(falseAlarms)-...
                                 BS(kk))/ ...
                min(S(falseAlarms),BS(kk));
%            if deltaSNR_overSNR>1.1
%              waitbar.warning(['Found a bkg trigger associated to an ' ...
%                               'injection with a large SNR difference: '...
%                               'deltaSNR/SNR=' num2str(deltaSNR_overSNR)]);
%              waitbar.warning(num2str([triggersTmp(falseAlarms).fmin, ...
%                                  triggersTmp(falseAlarms).fmax,...
%                                  triggersTmp(falseAlarms).tmin, ...
%                                  triggersTmp(falseAlarms).tmax]));
%             %{
%              waitbar.warning(num2str([BKGTmp(falseAlarms).fmin, ...
%                                  BKGTmp(falseAlarms).fmax, ...
%                                  BKGTmp(falseAlarms).tmin, ...
%                                  BKGTmp(falseAlarms).tmax]));
%              %}
%            end
          end
          triggersTmp=triggersTmp(~falseAlarms);
          c1i=c1i(~falseAlarms);
        end
      end
    end
    %% If the injection has been recovered in several directions, we keep
    %% only the loudest trigger
    if triggersTmp.numTriggers ~= 0
      [~,ind]=max(triggersTmp.SNR);
      triggersTmp=triggersTmp(ind(1));
      c1i=c1i(ind(1));
    end
    rmTriggers(c1i)=true;
  end
  data.removeElements(~rmTriggers);

%  waitbar.warning(['Associated ' num2str(sum(rmTriggers)) ' triggers to injections']);
%  waitbar.warning(['Associated ' num2str(sum(~rmTriggers)) ' triggers to noise']);
  display(['Associated ' num2str(sum(rmTriggers)) ' triggers to injections']);
  display(['Associated ' num2str(sum(~rmTriggers)) ' triggers to noise']);


  % display end message & close waitbar
%  waitbar('associate injection [done]');
%  delete(waitbar);

end


