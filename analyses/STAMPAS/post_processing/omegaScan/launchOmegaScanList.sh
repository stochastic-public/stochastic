#!/usr/bin/env bash

usage(){
    echo 'Missing parameters:'
    echo '1: web folder name'
    echo '2: loud events list file'
    echo '3: ifo1(1), ifo2(2) or both(0)'
    exit
}


# main
[[ $# -ne 3 ]] && usage

folder=$1
loudest_file=$2
ifo_choice=$3

evts_nb=`wc -l ${loudest_file} | awk '{print $1}'`

for i in `seq 1 ${evts_nb}`
#for i in `seq 1 2`
do
    timeH1=`cat ${loudest_file} | head -$i | tail -1 | awk '{printf("%.3f\n",$4)}'`
    timeL1=`cat ${loudest_file} | head -$i | tail -1 | awk '{printf("%.3f\n",$5)}'`
    #time_s=`cat ${loudest_file} | head -$i | tail -1 | awk '{print $5}' | sed  '/\./s/\.*0*$//g'`
    #time_e=`cat ${loudest_file} | head -$i | tail -1 | awk '{print $6}' | sed  '/\./s/\.*0*$//g'`

    #deltaT=` echo "scale=2;$time_s/2 + $time_e/2" | bc | sed  '/\./s/\.*0*$//g'`
    #timeH1=` echo "scale=2;$timeH1 + $deltaT" | bc | sed  '/\./s/\.*0*$//g'`
    #timeL1=`echo "scale=2;$timeL1 + $deltaT" | bc | sed  '/\./s/\.*0*$//g'`
    #timeH1=` echo "scale=2;$timeH1 + $time_s" | bc | sed  '/\./s/\.*0*$//g'`
    #timeL1=`echo "scale=2;$timeL1 + $time_s" | bc | sed  '/\./s/\.*0*$//g'`

    echo $i ' ' $timeH1 ' ' $timeL1 ' IFO' ${ifo_choice}

    if [ ${ifo_choice} == "0" ] || [ ${ifo_choice} == "1" ]; then
	if [ -f /home/${USER}/public_html/${folder}/H1_scandone.txt ]; then
     	    rc=`cat /home/${USER}/public_html/${folder}/H1_scandone.txt | awk -v gps=$timeH1 'function abs(x){return ((x < 0.0) ? -x : x)} {if (abs($1-gps)<2) print "done"}'`
#	    echo $rc
     	    if [ ${#rc} -gt 0 ]; then
     		echo 'wscan for time ' $timeH1 ' already done'
     	    else
     		${PWD}/launchOmegaScan.sh H1 $timeH1 $timeH1 p $folder
#     		${PWD}/launchOmegaScanHOFT.sh H1 $timeH1 $timeH1 $folder
     		echo $timeH1 >> /home/${USER}/public_html/${folder}/H1_scandone.txt
     	    fi
	else
     	    ${PWD}/launchOmegaScan.sh H1 $timeH1 $timeH1 p $folder
#     	    ${PWD}/launchOmegaScanHOFT.sh H1 $timeH1 $timeH1 $folder
     	    echo $timeH1 >> /home/${USER}/public_html/${folder}/H1_scandone.txt
	fi
    fi
    if [ ${ifo_choice} == "0" ] || [ ${ifo_choice} == "2" ]; then
	if [ -f /home/${USER}/public_html/${folder}/L1_scandone.txt ]; then
     	    rc=`cat /home/${USER}/public_html/${folder}/L1_scandone.txt | awk -v gps=$timeL1 'function abs(x){return ((x < 0.0) ? -x : x)} {if (abs($1-gps)<2) print "done"}'`
#            echo $rc
     	    if [ ${#rc} -gt 0 ]; then
     		echo 'wscan for time ' $timeL1 ' already done'
     	    else
    		${PWD}/launchOmegaScan.sh L1 $timeL1 $timeL1 p $folder
#     		${PWD}/launchOmegaScanHOFT.sh L1 $timeL1 $timeL1 $folder
     		echo $timeL1 >> /home/${USER}/public_html/${folder}/L1_scandone.txt
     	    fi
	else
     	    ${PWD}/launchOmegaScan.sh L1 $timeL1 $timeL1 p $folder
#     	    ${PWD}/launchOmegaScanHOFT.sh L1 $timeL1 $timeL1 $folder
     	    echo $timeL1 >> /home/${USER}/public_html/${folder}/L1_scandone.txt
	fi
    fi
    # if [ -f L1_scandone_rough.txt ]; then
    # 	rc=`cat L1_scandone_rough.txt | awk -v gps=$timeL1 'function abs(x){return ((x < 0.0) ? -x : x)} {if (abs($1-gps)<10) print "done"}'`
    # 	if [ ${#rc} -gt 0 ]; then
    # 	    echo 'wscan for time ' $timeL1 ' already done'
    # 	else
    # 	    ./launchOmegaScan.sh L $timeL1 $timeL1 r
    # 	    echo $timeL1 >> L1_scandone_rough.txt
    # 	fi
    # else
    # 	./launchOmegaScan.sh L $timeL1 $timeL1 r
    # 	echo $timeL1 >> L1_scandone_rough.txt
    # fi

done


