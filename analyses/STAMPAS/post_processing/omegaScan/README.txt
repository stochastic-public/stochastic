1. For each run, one needs to generate a configuration file. 

To get the location of the frame file:
gw_data_find --observatory L --gps-start-time 1121200000 --gps-end-time 1121600000 --type L1_HOFT_C00

To generate the configuration file:
./wconfigure.sh L1_ER7config.txt /data/node197/frames/ER7/hoft/H1/H-H1_HOFT_C00-11176/H-H1_HOFT_C00-1117626368-4096.gwf

Another way is to get the already generated omega scan configuration files in:
/home/detchar/etc/omega/

2. To launch an omegascan, use the launOmegaScanList.sh and launchOmegaScan.sh scripts

Example:
./launchOmegaScan.sh L 1128284924 1128284924  p o1-allsky/LOUDEST


Marie Anne Bizouard
23/02/2016
