# Q Scan configuration file
# Automatically generated with wconfigure.sh
# by user marieanne.bizouard on 2020-08-03 07:44:57 PDT
# from sample frame files:
#   /archive/frames/O3/hoft_C01/L1/L-L1_HOFT_C01-12378/L-L1_HOFT_C01-1237827584-4096.gwf

[Context,Context]

[Parameters,Parameter Estimation]

[Notes,Notes]

[L1:DCS,L1:DCS]

{
  channelName:                 'L1:DCS-CALIB_STRAIN_C01'
  frameType:                   'L1_HOFT_C01'
  sampleFrequency:             4096
  searchTimeRange:             64
  searchFrequencyRange:        [0 Inf]
  searchQRange:                [4 64]
  searchMaximumEnergyLoss:     0.2
  whiteNoiseFalseRate:         1e-3
  searchWindowDuration:        0.5
  plotTimeRanges:              [1 4 16]
  plotFrequencyRange:          []
  plotNormalizedEnergyRange:   [0 25.5]
  alwaysPlotFlag:              0
}

{
  channelName:                 'L1:DCS-CALIB_STRAIN_CLEAN_C01'
  frameType:                   'L1_HOFT_C01'
  sampleFrequency:             4096
  searchTimeRange:             64
  searchFrequencyRange:        [0 Inf]
  searchQRange:                [4 64]
  searchMaximumEnergyLoss:     0.2
  whiteNoiseFalseRate:         1e-3
  searchWindowDuration:        0.5
  plotTimeRanges:              [1 4 16]
  plotFrequencyRange:          []
  plotNormalizedEnergyRange:   [0 25.5]
  alwaysPlotFlag:              0
}

