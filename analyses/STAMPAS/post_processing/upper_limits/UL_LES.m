function [hrss, ULs] = UL_LES(doSNRfracStudy, doFADranking, varargin)
% function [hrss, ULs] = UL_LES(doSNRfracStudy, doFADranking, varargin)
% function [hrss, ULs] = UL_LES(doSNRfracStudy, doFADranking)
% function [hrss, ULs] = UL_LES(doSNRfracStudy, doFADranking, SNRfrac_thresh)
%
% Loudest event statistic:
% Calculates 90% rate upper limit for each waveform using the loudest event
% statistic.  Calls calc_LES.m for each search and then combines the results
% into an overall upper limit.  The upper limit for each waveform is a
% function of hrss.
%
% Inputs:
%   doSNRfracStudy - specifies whether you are doing a study to tune the
%                    SNRfrac threshold.  If so, the background data is
%                    used to calculate the upper limit rather than the
%                    zero-lag, and only the loudest trigger is used.
%   doFADranking   - specifies if the user wants to use FAD rankings to
%                    determine the loudest event from multiple searches.
%                    If false, the loudest event is determined by SNR.
%   SNRfrac_thresh - specifies the SNRfrac threshold to use.  This
%                    parameter is optional, but if specified, will
%                    overwrite the SNRfrac thresholds specified in the
%                    params struct.
% Outputs:
%   hrss - cell array of numeric vectors of hrss.  Each cell corresponds to a
%          waveform.
%   ULs  - cell array of LES upper limits, each cell corresponds to a
%          waveform's upper limits as a function of hrss.
%
% Author: Tanner Prestegard (prestegard@physics.umn.edu)
%         Frey Valentin (frey@lal.in2p3.fr) Modified by
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load parameters for upper limit calculation.
params = UL_params();
nSearches = numel(params.search_label);

prepareWebReport(params);

% If a 2nd argument is specified, it should be SNRfrac.
% Then we override the SNRfrac values in the params struct
if (nargin == 3)
  for ii=1:numel(params.SNRfracCut)
    params.SNRfracCut{ii} = varargin{1};
  end
end

% If doing an SNRfrac study, set the zero-lag to be
% the background.
if (doSNRfracStudy)
  params.ID_zl = params.ID_bkg;
end

% Set up params struct for each search.
par = split_params(params);

% Loop over searches.
zl_max = 0;
for ii=1:nSearches
  
  % Assign information to search struct.
  search(ii).GPS_start = params.GPS_start{ii};
  search(ii).GPS_end = params.GPS_end{ii};
  search(ii).ID_inj = params.ID_inj{ii};
  search(ii).ID_zl = params.ID_zl{ii};

  % Set up search paths.
  search(ii).searchPath_inj = generate_searchPath('INJ',params.GPS_start{ii}, ...
						  params.GPS_end{ii}, ...
						  params.ID_inj{ii});
  search(ii).searchPath_zl = generate_searchPath('BKG',params.GPS_start{ii}, ...
						 params.GPS_end{ii}, ...
						 params.ID_zl{ii});

  % Handle zero-lag data.
  % Check if ZL filtered file exists.
  fprintf('Loading %s zero-lag data.\n',params.search_label{ii});
  zl_mat = ['../../' search(ii).searchPath_zl '/results/results_merged/results_filtered.mat'];
  if (exist(zl_mat) ~= 2)
    error(['Filtered zero-lag file ' zl_mat ' does not exist.  Run apply_DQFlags.m' ...
	   ' to produce it.']);
  else
    % Load zero-lag data.
    search(ii).zl = load(zl_mat);
  end

  % Get zero-lag observation time.
  time_file = ['../../' search(ii).searchPath_zl '/tmp/TTimeZeroLag.txt'];
  search(ii).zl.T_obs = load(time_file);

  % Remove ZL triggers based on SNRfrac and DQ flags.
  search(ii).zl.data = clean_data(search(ii).zl.data,par(ii));

  % Find ZL event with largest SNR.
  search(ii).zl.snr_max = max(search(ii).zl.data.SNR);

  % Get overall loudest ZL event (largest SNR).
  zl_max = max(zl_max,search(ii).zl.snr_max);

end

% If the user wants to use FAD rankings of events to determine
% the loudest event, we use the trigger from all searches with
% the lowest overall FAD instead of the highest SNR.
if doFADranking
  for ii=1:nSearches

    % FAD .mat file.
    inj_mat = ['../../' search(ii).searchPath_inj '/results/results_merged/' ...
	       'results_FAD.mat'];

    if (exist(inj_mat) ~= 2 | doSNRfracStudy)

      % Load bkg data.
      search(ii).searchPath_bkg = generate_searchPath('BKG',params.GPS_start{ii}, ...
						      params.GPS_end{ii}, ...
						      params.ID_bkg{ii});
      fprintf('Loading %s background data.\n',params.search_label{ii});
      bkg_mat = ['../../' search(ii).searchPath_bkg '/results/results_merged/' ...
		 'results_filtered.mat'];
      if (exist(bkg_mat) ~= 2)
	error(['Filtered bkg file ' bkg_mat ' does not exist.  Run extract.sh' ...
	       ' to produce it.']);
      else
	search(ii).bkg = load(bkg_mat);
      end

      % Clean background data.
      search(ii).bkg.data = clean_data(search(ii).bkg.data,par(ii));

      % Get background observation time.
      bkg_time_file = ['../../' search(ii).searchPath_bkg '/tmp/TTime.txt'];
      search(ii).bkg.T_obs = load(bkg_time_file);

      % Calculate false alarm density for all waveforms.
      fprintf('Running calc_FAD.m to get FAD data for %s.\n', ...
	      params.search_label{ii});

      inj = calc_FAD(par(ii),doSNRfracStudy,search(ii).bkg);
      % Save FAD .mat file for future use if not running an SNRfrac study.
      if (~doSNRfracStudy)
	save(inj_mat,'inj');
      end
    else
      % Otherwise, load injection FAD and visible volume data.
      fprintf('Loading saved %s FAD data.\n',params.search_label{ii});
      load(inj_mat);
    end

    % Set up inj struct and waveform names.
    search(ii).inj = inj;
    search(ii).wf_names = {inj.wf_name};
  end

  % Loop over waveforms; for each, get FAD of overall
  % loudest ZL event from all searches.
  % (event with lowest overall FAD)
  fprintf('Finding loudest overall ZL event using FAD ranking.\n');
  comb = get_loudest_FAD(params,search,doSNRfracStudy);
  zl_max = comb.snr;
end

% Get efficiency information for all waveforms.
[search,tot] = calc_LES(params,zl_max,search);

% Loop over waveforms and calculate upper limits.
for ii=1:numel(search(1).wf_names)
  % Calculate 90% rate upper limit.
  tot(ii).R90 = 2.3./tot(ii).eff_time;
end

% Set up outputs.
for ii=1:numel(search(1).wf_names)
  ULs{ii} = tot(ii).R90';
  hrss{ii} = tot(ii).hrss';
end

% Make plots
if (params.doPlots & ~doSNRfracStudy)
  fprintf('Making plots.\n');
  wf_names = strrep(search(1).wf_names,'_','\_');
  for ii=1:numel(search(1).wf_names)
    close all;
    figure;
    loglog(hrss{ii},ULs{ii},'-b','LineWidth',2);
    xlabel('h_{rss} (strain/Hz^{1/2})');
    ylabel('Rate upper limit (#/yr)');
    title([wf_names{ii} ' LES upper limit vs.h_{rss}']);
    largePlot(15,12);
    pretty();
    for jj=1:nSearches
      print('-dpng',[params.webFolder{jj} '/les/figures/' search(1).wf_names{ii} '_LES_upper_limits.png']);
    end
    close all;
  end

end

for ii=1:nSearches
  fid=fopen([params.webFolder{ii} '/les.txt'],'w');
  fclose(fid);
end

return;

% End of file