function ULs = UL_FAD(doSNRfracStudy, varargin)
% function ULs = UL_FAD(doSNRfracStudy, varargin)
% function ULs = UL_FAD(doSNRfracStudy)
% function ULs = UL_FAD(doSNRfracStudy, SNRfrac_thresh)
%
% Calculates 90% rate upper limit using the false alarm density statistic.
% Calls calc_FAD.m to calculate visible volume and FAD for each search
% specified by the params struct.  Then adds the results of the searches
% together to produce overall upper limits on each waveform.
%
% Inputs:
%   doSNRfracStudy - specifies whether you are doing a study to tune the
%                    SNRfrac threshold.  If so, the background data is
%                    used to calculate the upper limit rather than the
%                    zero-lag, and only the loudest trigger is used.
%   SNRfrac_thresh - specifies the SNRfrac threshold to use.  This
%                    parameter is optional, but if specified, will
%                    overwrite the SNRfrac thresholds specified in the
%                    params struct.
%
% Outputs:
%   ULs - 90% rate upper limits (#/Mpc^3/yr). One number
%         per waveform.
%
% Author: Tanner Prestegard (prestegard@physics.umn.edu)
%         Frey Valentin (frey@lal.in2p3.fr) Modified by
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load parameters for upper limit calculation.
params = UL_params();

% Get number of searches.
nSearches = numel(params.search_label);

% If a 2nd argument is specified, it should be SNRfrac.
% Then we override the SNRfrac values in the params struct.
if (nargin == 2)
  for ii=1:numel(params.SNRfracCut)
    params.SNRfracCut{ii} = varargin{1};
  end
end

% create web folder to save plots
prepareWebReport(params);

% Set up params struct for each search.
par = split_params(params);

% Loop over searches.
for ii=1:nSearches

  % Assign information to search struct.
  search(ii).GPS_start = params.GPS_start{ii};
  search(ii).GPS_end = params.GPS_end{ii};
  search(ii).ID_inj = params.ID_inj{ii};
  search(ii).ID_zl = params.ID_zl{ii};

  % Set up search paths.
  search(ii).searchPath_inj = generate_searchPath('INJ',params.GPS_start{ii}, ...
						  params.GPS_end{ii}, ...
						  params.ID_inj{ii});
  search(ii).searchPath_bkg = generate_searchPath('BKG',params.GPS_start{ii}, ...
						 params.GPS_end{ii}, ...
						 params.ID_bkg{ii});
  search(ii).searchPath_zl = generate_searchPath('BKG',params.GPS_start{ii}, ...
						 params.GPS_end{ii}, ...
						 params.ID_zl{ii});

  % Load bkg data.
  fprintf('Loading %s background data.\n',params.search_label{ii});
  bkg_mat = ['../../' search(ii).searchPath_bkg '/results/results_merged/results_filtered.mat'];
  if (exist(bkg_mat) ~= 2)
    error(['Filtered bkg file ' bkg_mat ' does not exist.  Run extract.m' ...
	   ' to produce it.']);
  else
    search(ii).bkg = load(bkg_mat);
  end

  % Check if ZL filtered file exists and load zero-lag data.
  % If we are doing an SNRfrac study, we use the background
  % results instead of the zero-lag.
  fprintf('Loading %s zero-lag data.\n',params.search_label{ii});
  if (doSNRfracStudy)
    search(ii).zl = search(ii).bkg;
  else
    zl_mat = ['../../' search(ii).searchPath_zl '/results/results_merged/results_filtered.mat'];
    if (exist(zl_mat) ~= 2)
      error(['Filtered zero-lag file ' zl_mat ' does not exist.  Run extract.m' ...
	     ' to produce it.']);
    else
      search(ii).zl = load(zl_mat);
    end
  end

  % Get background observation time.
  bkg_time_file = ['../../' search(ii).searchPath_bkg '/tmp/TTime.txt'];
  search(ii).bkg.T_obs = load(bkg_time_file);

  % Get zero-lag observation time.
  if (doSNRfracStudy)
    zl_time_file = ['../../' search(ii).searchPath_bkg '/tmp/TTimeZeroLag.txt'];
  else
    zl_time_file = ['../../' search(ii).searchPath_zl '/tmp/TTimeZeroLag.txt'];
  end
  search(ii).zl.T_obs = load(zl_time_file);

  % Remove bkg and ZL events based on SNRfrac and DQ flags.
  search(ii).bkg.data = clean_data(search(ii).bkg.data,par(ii));
  search(ii).zl.data = clean_data(search(ii).zl.data,par(ii));

  % Find ZL event with largest SNR.
  if isstruct(search(ii).zl.data.SNR)
    search(ii).zl.snr_max = max(search(ii).zl.data.SNR.coherent);
  else
    search(ii).zl.snr_max = max(search(ii).zl.data.SNR);
  end
    
  % Get injection data.
  % It can be useful to save the FAD and visible volume results
  % since it can be time-consuming to do the calculation.  But users
  % might also want to run on the fly for tuning SNRfrac.
  % Here we handle the different cases.
  inj_mat = ['../../' search(ii).searchPath_inj '/results/results_merged/results_FAD.mat'];
  
  % If .mat file doesn't exist or if we are doing an SNRfrac tuning study,
  % we run calc_FAD.m to get the false alarm density and visible volume.
  if (exist(inj_mat) ~= 2 | doSNRfracStudy)
    fprintf('Running calc_FAD.m to get %s FAD data.\n',params.search_label{ii});
  
    % Calculate false alarm density for all waveforms.
    inj = calc_FAD(par(ii),doSNRfracStudy,search(ii).bkg);
    
    % Save FAD .mat file for future use if not running an SNRfrac study.
    if (~doSNRfracStudy)
      FAD_params = par(ii);
      save(inj_mat,'inj','FAD_params');
    end
  else
    % Otherwise, load injection FAD and visible volume data.
    fprintf('Loading saved %s FAD data.\n',params.search_label{ii});
    load(inj_mat);

    % Check if saved parameters are equal to those specified in
    % UL_params. compare_params.m will throw an error if not.
    compare_params(par(ii),FAD_params);

  end

  % Set up inj struct and waveform names.
  search(ii).inj = inj;
  search(ii).wf_names = {inj.wf_name};
  
  % Get waveform names and check search compatibility by comparing the names
  % of the waveforms used in each search (if using multiple searches).
  if (ii > 1)
    % Compare with wf_names from search(1) to make sure they are the same.

    for jj=1:numel(search(ii).wf_names)
      if ~strcmp(search(1).wf_names(jj),search(ii).wf_names(jj))
	error(['Waveforms from search ' num2str(ii) ' don''t match.  Check' ...
	       ' your waveforms.txt files.']);
      end
    end
  end

  % Calculate FAR and FAP of loudest trigger from each search.
  % Units for FAR are #/yr.
  [search(ii).zl.FAR_max, search(ii).zl.FAP_max] = ...
      calc_FARP(par(ii),search(ii));
  fprintf(['Loudest zero-lag trigger for search %s has a FAR of %f/year' ...
	  ' and FAP %f.\n'], ...
	  par(ii).search_label,search(ii).zl.FAR_max,search(ii).zl.FAP_max);
end

% Loop over waveforms; for each, get FAD of overall
% loudest ZL event from all searches.
% (event with lowest overall FAD)
fprintf('Finding loudest overall ZL event.\n');
comb = get_loudest_FAD(params,search,doSNRfracStudy);

% Calculate time-volume product corresponding to
% the lowest FAD for each search and waveform.
% T-V product is in units of Mpc^3*yr.
fprintf('Calculating time-volume product.\n');
for ii=1:nSearches
  for jj=1:numel(search(1).wf_names)
    if (doSNRfracStudy)
      v_vis(ii,jj) = search(ii).inj(jj).v_vis;
      dv_tot(ii,jj) = search(ii).inj(jj).dv_tot;
    else
      v_vis(ii,jj) = interp1(search(ii).inj(jj).FAD, ...
			     search(ii).inj(jj).v_vis,comb.FAD(jj), ...
			     params.interp_method);
      dv_tot(ii,jj) = interp1(search(ii).inj(jj).FAD, ...
			      search(ii).inj(jj).dv_tot,comb.FAD(jj), ...
			      params.interp_method);
    end
  end
  nu(ii,:) = v_vis(ii,:)*(search(ii).zl.T_obs/(86400*365.25));
  dnu(ii,:) = dv_tot(ii,:)*(search(ii).zl.T_obs/(86400*365.25));
end

% Combine results from multiple searches to
% get overall T-V productivity. Combined uncertainty
% is calculated by adding uncertainties in quadrature.
comb.nu = sum(nu,1);
comb.dnu = sqrt(sum(dnu.^2,1));

% Calculate upper limit with Bayesian method (see bayes_UL.m
% for more information).
fprintf('Calculating upper limits.\n');
for ii=1:numel(search(1).wf_names)
  if (comb.nu(ii) > 0)
    ULs(ii) = bayes_UL(comb.nu(ii),comb.dnu(ii),params.doPlots, ...
		       search(1).wf_names{ii},...
                       [params.webFolder{1} '/fad/' search(1).wf_names{ii} ...
                       '/figures/']);
  else
    ULs(ii) = Inf;
  end

  % Print results to screen.
  fprintf('\tWaveform %s: %.4g per year per Mpc^3.\n', ...
	  search(1).wf_names{ii},ULs(ii));
end

% Make plots.
if (params.doPlots & ~doSNRfracStudy)
  fprintf('Making plots.\n');
  FAD_plots(search,params);
end

for ii=1:nSearches
  fid=fopen([params.webFolder{ii} '/fad.txt'],'w');
  fclose(fid);
end
return;

% End of file