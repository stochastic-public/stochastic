function [FAR, FAP] = calc_FARP(params,search)
% function [FAR, FAP] = calc_FARP(params,search)
%
% Calculates the false alarm rate (FAR) and false alarm
% probability (FAP) for the loudest zero-lag trigger
% from the given search.
%
% Input:
%   params - params struct for upper limits (should correspond
%            to a single search).
%   search - search struct, contains information about background
%            and zero-lag data.
%
% Output:
%   FAR - false alarm rate of loudest zero-lag trigger (#/year).
%   FAP - false alarm probability of loudest zero-lag trigger.
%
% Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% SNR list
snr_list = sort(search.bkg.data.SNR);

% FAR list (#/yr)
N = 1:numel(snr_list);
far_list = (86400*365.25/search.bkg.T_obs)* ...
    (numel(snr_list) - N);

% Get only unique SNRs so we can interpolate.
[snr_list,idx] = unique(sort(snr_list));
far_list = far_list(idx);

% Interpolate to get FAR for loudest ZL trigger.
FAR = interp1(snr_list,far_list,search.zl.snr_max,params.interp_method);

% Calculate false alarm probability.
% mu is the expected number of background triggers.
mu = (search.zl.T_obs/(86400*365.25))*FAR;
FAP = 1 - exp(-mu);

return;