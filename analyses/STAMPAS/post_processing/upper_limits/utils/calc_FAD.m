function inj = calc_FAD(params, doSNRfracStudy, bkg)
% function inj = calc_FAD(params, doSNRfracStudy, bkg)
%
% Calculates false alarm density (FAD), visible volume, and visible
% volume uncertainty as a function of SNR. Results are saved in a
% Matlab file (results_FAD.mat) in the results/results_merged
% subdirectory of the injection study directory. The .mat file is
% not saved if doing an SNRfrac study.
%
% Background data (bkg.data) should have already been cleaned
% before being passed to this script.
%
% Inputs:
%   params         - struct specifying params for handling the data.
%   doSNRfracStudy - if true, means we are doing a study to optimize
%                    the SNRfrac threshold for a search.  In this case
%                    we only do the calculation for the loudest trigger.
%   bkg            - struct containing background triggers and other
%                    information, including background livetime.
%
% Outputs:
%  inj - an array of structs.  Each struct corresponds to a particular
%        waveform and contains visible volume, its uncertainty, and
%        the FAD as a function of SNR.
%
% Author: Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import classes.waveforms.dictionary

% Set up search path to injection data.
searchPath_inj = generate_searchPath('INJ',params.GPS_start,params.GPS_end, ...
				     params.ID_inj);

% Get number of injections performed.
[names,values] = textread(['../../' searchPath_inj '/config_inj.txt'], '%s %s\n',-1,'commentstyle','matlab');
Ninj = values(find(strcmp(names,'NumberOfInjections')));
Ninj = str2num(cell2mat(Ninj)); % Convert from cell -> string -> number.

% Get waveform names, alphas, distances, hrss.
dic = dictionary.parse(['../../' searchPath_inj '/waveforms.txt']);

% Take only unique SNRs and record number of triggers for each SNR.
% If we are doing an SNRfrac study, we are only interested in
% the event with the largest SNR.
% Note: N_trigs calculation assumes that idx corresponds to data that is
% sorted by SNR! So even though unique() sorts the data, we need it to
% be sorted before we pass it to unique() and get the indices.
% Thus we can't do unique() only, we have to do unique(sort()).
[uniq_snr,idx] = unique(sort(bkg.data.SNR));
[idx,uniq_snr]=hist(uniq_snr,1000);

if (doSNRfracStudy)
  uniq_snr = max(uniq_snr);
  N_trigs = 1;
else
  N_trigs = diff(idx);
  N_trigs(end+1) = (bkg.data.numTriggers - idx(end) + 1);
  N_trigs = N_trigs';
  N_trigs=idx;
end

% Estimate time to completion.
t_est = (7e-4*numel(uniq_snr)/60)*numel(dic.nbWvf);
fprintf('Estimated time to completion: %.3f minutes.\n',t_est);

% Loop over waveforms and calculate visible volume
% and FAD for each as a function of SNR.

ii=0;
for wvf=dic.headers'
  ii=ii+1;

  % Print message for user.
  fprintf('Processing results for waveform %s (%d/%d).\n',wvf.id,ii,dic.nbWvf);

  % Load injection study results.
  % Data is organized in a struct ('wf') with substructs.
  % Each substruct corresponds to a particular alpha value
  % for that waveformn and contains the results for that
  % alpha and some metadata.
  wf = get_inj_data(searchPath_inj,params,wvf);
  fprintf('data load...')

  % Add number of injections to wf struct.
  [wf.Ninj] = deal(Ninj);

  % Calculate visible volume.
  result = calc_vvis(wf,uniq_snr,params);

  % OLD FAD CALCULATION %%%%%%%%%%%%%%%%%%%%
  % FAD(SNR_i) = sum(1/V_vis(SNR_i)) where the sum is over all background events
  % with SNR > SNR_i.  FAD is in units of Mpc^(-3)yr^(-1)
  %vvis_inv = 1./result.v_vis;
  %result.FAD = (86400*365.25/bkg.T_obs)*(fliplr(cumsum(fliplr(vvis_inv.*N_trigs))) - vvis_inv);

  % Calculate FAR (units of #/yr).
  result.FAR = (86400*365.25/bkg.T_obs)*(sum(N_trigs) - cumsum(N_trigs));

  % Classical FAD calculation (DCC T1300869, Eq. 4)
  result.FAD = result.FAR ./ result.v_vis;

  % Convert to monotonous FAD (same DCC document, Eq. 6).
  % Need to add a small amount of "jitter" - otherwise there are
  % cases where the FAD is the same and the interpolation doesn't
  % work then.  So we add jitter at the level at 1e-10 to prevent this.
  % Compared results where we do manual (local) interpolation with no
  % jitter and also add jitter and use Matlab interp1 function, and
  % there is no difference.
  jitter = 1e-10;
  for jj=2:numel(result.FAD)
    if (result.FAD(jj) >= result.FAD(jj-1))
      result.FAD(jj) = result.FAD(jj-1)*(1-jitter);
    end
  end

  % Add visible volume, uncertainty, and FAD to output struct.
  inj(ii).v_vis = result.v_vis';
  inj(ii).dv_tot = result.dv_tot';
  inj(ii).FAD = result.FAD';

  % Add waveform name to output struct.
  inj(ii).wf_name = wvf.id;

  % Add SNR to output struct.
  inj(ii).snr = uniq_snr;

end

return;