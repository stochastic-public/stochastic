function comb = get_loudest_FAD(params,search,doSNRfracStudy)
% function comb = get_loudest_FAD(params,search,doSNRfracStudy)
% 
% Loops over results from multiple searches and finds the 
% background trigger with the lowest FAD.
%
% Inputs:
%   params         - upper limit parameters.
%   search         - array of search structs.
%   doSNRfracStudy - if true, means we are doing a
%                    study to set the SNRfrac threshold.
%
% Outputs:
%   comb - struct containing FAD and snr values of trigger
%          with lowest FAD.  Each waveform has a different
%          FAD value.
%
% Author: Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Loop over waveforms
for ii=1:numel(search(1).wf_names)
  % Set up comb struct.
  comb.FAD(ii) = max(search(1).inj(ii).FAD);
  comb.snr(ii) = min(search(1).inj(ii).snr);

  % Loop over searches.
  for jj=1:numel(search)
    
    % Make sure loudest ZL event is not louder than loudest
    % background event.
    if (search(jj).zl.snr_max > max(search(jj).inj(ii).snr))
      error(['Your most significant zero-lag event is louder than any' ...
	     ' background event.  There is either a mistake or a possible' ...
	     ' detection...']);
    end

    % Get FAD corresponding to loudest ZL event.
    if (doSNRfracStudy)
      temp_FAD = search(jj).inj(ii).FAD;
    else
      temp_FAD = interp1(search(jj).inj(ii).snr,search(jj).inj(ii).FAD, ...
			 search(jj).zl.snr_max,params.interp_method);
    end
    
    % Find lowest overall FAD (and corresponding SNR).
    if (temp_FAD < comb.FAD(ii))
      comb.FAD(ii) = temp_FAD;
      comb.snr(ii) = search(jj).zl.snr_max;
    end
    
  end
end
