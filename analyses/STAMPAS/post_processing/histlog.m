function [xout2, n2, underflow, overflow] = histlog (input, dx, color)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AUTHOR: MAB
% DATE: 12/03/2006
% SUBJECT: histogram with log scale, with stair style
% LAST RELEASE:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if size(input,1)==1 & size(input,2)>1
  input=input';
end

if size(dx) == [1,1]
  x_min=min(input(:,1));
  x_max=max(input(:,1));
  intervals=(x_max - x_min)/dx;
  ceil(intervals);
  if intervals == ceil(intervals)
    nbins=intervals+1;
  else
    nbins=ceil(intervals);
  end

  % To be sure bins are ending correctly, one redefines x_max with nbins
  xmax=x_min+nbins*dx;
  bins=x_min:dx:x_max;
else
  bins=dx;
  nbins=size(bins,2);
end

edges=[-Inf bins Inf];  

[nn1]=histc(input,edges);
n1=nn1';
if size(n1,1)==0
  warning('Not enough data in the histogram');
  return;
end
xout2=zeros(1,nbins+4);
n2=zeros(1,nbins+4);
if isempty(find(n1(1,:)>0))
  min_n=1;
else
  min_n=min(n1(1,find(n1(1,:)>0)));
  if min_n/10>1
    min_n=1;
  end
end
%display([num2str(n1(1,1)) ' ' num2str(n1(1,2)) ' '...
%	 num2str(n1(1,3)) ' ' num2str(n1(1,4)) ' '...
%	 num2str(n1(1,end-3)) ' ' num2str(n1(1,end-2)) ' '...
%	 num2str(n1(1,end-1)) ' ' num2str(n1(1,end))])

n2(1,1)=min_n/10;
n2(1,2:nbins+3)=n1(1,:);
n2(1,nbins+4)=min_n/10;
n2(1,find(n2(1,:)==0))=min_n/10;

xout2(1,3:nbins+2)=bins(1,1:nbins);

if (nbins>1)
  delta = bins(2)-bins(1);
else 
  delta = 0;
end

xout2(1,1) = xout2(1,3)-1.1*delta;
xout2(1,2) = xout2(1,3)-delta;    
xout2(1,nbins+3) = xout2(1,nbins+2)+delta;
xout2(1,nbins+4) = xout2(1,nbins+2)+1.1*delta;    

%display([num2str(n2(1,1)) ' ' num2str(n2(1,2)) ' '...
%	 num2str(n2(1,3)) ' ' num2str(n2(1,4)) ' '...
%	 num2str(n2(1,end-3)) ' ' num2str(n2(1,end-2)) ' '...
%	 num2str(n2(1,end-1)) ' ' num2str(n2(1,end))])


s=stairs(xout2,n2);
h = get( gcf, 'CurrentAxes');
set (h,'YScale','log');
%%set (h,'XScale','log');
set (h, 'FontSize', 12);
set (s, 'Color', color);
set (h, 'YLimMode', 'manual');

overflow=n1(1,end);
underflow=n1(1,1);
