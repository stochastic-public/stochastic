function [] = targetZebragard(searchPath,trigger,folder)  
% targetZebragard :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = targetZebragard ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Mar 2017
%

  params = targetParams(searchPath);

  params.ra  = trigger(10);
  params.dec = trigger(11);

  params.anteproc.jobfile = [searchPath '/tmp/jobfile_bknd.txt'];
  params.pFile       = [searchPath '/tmp/p_H1.txt'];
  
  doc=html.document;
  doc.setStyle('style.css');

  doc.createNode('h1','STAMPAS TARGET ANALYSIS');
  idoc = doc.createNode('div','Attribute',struct('id','info'));
  pdoc = doc.createNode('div','Attribute',struct('id','plots'));
  
  % make folder
  if exist([folder '/FTMAPS/plots_' num2str(trigger(1))])~=7
    system(['mkdir -p ' folder '/FTMAPS/ftmap_' ...
            num2str(trigger(1))]);
    system(['cp template/style.css ' ...
            folder '/FTMAPS/ftmap_' num2str(trigger(1))...
            '/style.css']);
  end

  plotdir = [folder '/FTMAPS/ftmap_' num2str(trigger(1))];
  params.returnMap = true;
  
  % get the window corresponding to the given gps time
  jobs = load(params.anteproc.jobfile);
  idx  = jobs(:,2)<=trigger(4) & ...
         jobs(:,3)>=trigger(4)+trigger(6);

  t_start = jobs(idx,2);
  t_end   = jobs(idx,3);
  winIdx  = jobs(idx,1);
  
  if sum(idx)>1
    t_start = t_start(1);
    t_end = t_end(1);
    winIdx = winIdx(1);
  end

  currentLag=mod(winIdx+trigger(7),size(jobs,1));

  if currentLag==0
    currentLag=size(jobs,1);
  end

  params.anteproc.jobNum1 = winIdx;
  params.anteproc.jobNum2 = currentLag;

  rng(jobs(winIdx,2)+jobs(currentLag,2),'twister');

  % run clustermap
  stoch_out=clustermap(params, t_start, t_end)
  save('bkp.mat');
  % save plot
  targetZebragardPlots(stoch_out,plotdir,trigger);
  targetZebragard_plots2html(doc);
  
  % include info
  targetZebragardInfo(stoch_out,trigger,doc);


  clear stoch_out;

  doc.write([folder '/FTMAPS/ftmap_' num2str(trigger(1)) '/index.html'])

end

