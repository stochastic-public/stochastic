function [d] = targetInfo(stoch_out,aa,doc)  
% targetInfo : get info from triggers
% DESCRIPTION :
%   get detailled info of a targeted triggers
%
% SYNTAX :
%   [] = targetInfo (stoch_out,aa,doc)
% 
% INPUT : 
%   stoch_out : struct return by clustermap
%   aa : triggers 
%   doc : html.document object
% 
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Fev 2016
%

  % get snr ifo 1&2
  AP1 = stoch_out.map.naiP1./stoch_out.map.P1;
  AP2 = stoch_out.map.naiP2./stoch_out.map.P2;
  snrifo1=sum(sum(AP1(stoch_out.cluster.reconMax~=0)));
  snrifo2=sum(sum(AP2(stoch_out.cluster.reconMax~=0)));

  n_snrifo1=stoch_out.map.naiP1./repmat(median(stoch_out.map.naiP1,2),1,size(stoch_out.map.naiP1,2));
  n_snrifo2=stoch_out.map.naiP2./repmat(median(stoch_out.map.naiP2,2),1,size(stoch_out.map.naiP2,2));
  n_snrifo1=sum(sum(n_snrifo1(stoch_out.cluster.reconMax~=0)));
  n_snrifo2=sum(sum(n_snrifo2(stoch_out.cluster.reconMax~=0)));

  % null energie
  tt=stoch_out.map.segstarttime>aa(4)&stoch_out.map.segstarttime<aa(4)+aa(6);
  ff=stoch_out.map.f>aa(8)&stoch_out.map.f<aa(9);

  AP1(stoch_out.cluster.reconMax~=0)=0;
  AP2(stoch_out.cluster.reconMax~=0)=0;
  AP1(isnan(AP1))=0;
  AP2(isnan(AP2))=0;
  nullE1=sum(sum(AP1(ff,tt)));
  nullE2=sum(sum(AP2(ff,tt)));
  nullE1_norm=sum(sum(AP1(ff,tt)))/(sum(ff)*sum(tt));
  nullE2_norm=sum(sum(AP2(ff,tt)))/(sum(ff)*sum(tt));

  AP1=stoch_out.map.naiP1./repmat(median(stoch_out.map.naiP1,2),1,size(stoch_out.map.naiP1,2));
  AP2=stoch_out.map.naiP2./repmat(median(stoch_out.map.naiP2,2),1,size(stoch_out.map.naiP2,2));
  AP1(stoch_out.cluster.reconMax~=0)=0;
  AP2(stoch_out.cluster.reconMax~=0)=0;
  AP1(isnan(AP1))=0;
  AP2(isnan(AP2))=0;
  nullE1_med=sum(sum(AP1(ff,tt)));
  nullE2_med=sum(sum(AP2(ff,tt)));
  nullE1_med_norm=sum(sum(AP1(ff,tt)))/(sum(ff)*sum(tt));
  nullE2_med_norm=sum(sum(AP2(ff,tt)))/(sum(ff)*sum(tt));

  % F1&2
  a.c=3e8;
  g=calF(getdetector('LHO'), getdetector('LLO'), aa(4), [aa(10) ...
                      aa(11)], a);
  F1=g.F1p^2+g.F1x^2;
  F2=g.F2p^2+g.F2x^2;

  d=doc.getElementById('info');
  doc.createNode('h2','Summary table','Parent',d);
  div=doc.createNode('div','Attribute',struct('class','info'),'Parent',d);
  table=doc.createNode('table','Parent',div);

  % gps start time
  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','GPS start time',...
                 'Attribute',struct('colspan','3','class','tsection'),...
                 'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','ifo 1','Parent',tr);
  doc.createNode('td',num2str(aa(4)),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','ifo 2','Parent',tr);
  doc.createNode('td',num2str(aa(5)),'Parent',tr);

  % SNR
  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','Energie',...
                 'Attribute',struct('colspan','3','class','tsection'),...
                 'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','SNR','Parent',tr);
  doc.createNode('td','coherent','Parent',tr);
  doc.createNode('td',num2str(aa(3)),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','ifo 1','Parent',tr);
  doc.createNode('td',num2str(snrifo1),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','ifo 2','Parent',tr);
  doc.createNode('td',num2str(snrifo2),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','median ifo 1','Parent',tr);
  doc.createNode('td',num2str(n_snrifo1),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','median ifo 2','Parent',tr);
  doc.createNode('td',num2str(n_snrifo2),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','SNR frac','Parent',tr);
  doc.createNode('td','coherent','Parent',tr);
  doc.createNode('td',num2str(aa(12)),'Parent',tr);


  % Null Energie
  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','Null Energie',...
                 'Attribute',struct('colspan','3','class','tsection'),...
                 'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','ifo 1','Parent',tr);
  doc.createNode('td',num2str(nullE1),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','ifo 2','Parent',tr);
  doc.createNode('td',num2str(nullE2),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','Normalized','Parent',tr);
  doc.createNode('td','ifo 1','Parent',tr);
  doc.createNode('td',num2str(nullE1_norm),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','ifo 2','Parent',tr);
  doc.createNode('td',num2str(nullE2_norm),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','Median sigma estimation','Parent',tr);
  doc.createNode('td','ifo 1','Parent',tr);
  doc.createNode('td',num2str(nullE1_med),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','ifo 2','Parent',tr);
  doc.createNode('td',num2str(nullE2_med),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','Normalized & ','Parent',tr);
  doc.createNode('td','ifo 1','Parent',tr);
  doc.createNode('td',num2str(nullE1_med_norm),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','Median sigma estimation','Parent',tr);
  doc.createNode('td','ifo 2','Parent',tr);
  doc.createNode('td',num2str(nullE2_med_norm),'Parent',tr);


  % ifo antenna factor
  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','IFO RESPONSE',...
                 'Attribute',struct('colspan','3','class','tsection'),...
                 'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','F','Parent',tr);
  doc.createNode('td','ifo 1','Parent',tr);
  doc.createNode('td',num2str(F1),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','ifo 2','Parent',tr);
  doc.createNode('td',num2str(F2),'Parent',tr);

  % frequency
  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','FREQUENCY',...
                 'Attribute',struct('colspan','3','class','tsection'),...
                 'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','Fmin','Parent',tr);
  doc.createNode('td',num2str(aa(8)),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','Fmax','Parent',tr);
  doc.createNode('td',num2str(aa(9)),'Parent',tr);

  % additionnal
  tr=doc.createNode('tr','Parent',table);
  doc.createNode('th','ADDITIONAL INFORMATION',...
                 'Attribute',struct('colspan','3','class','tsection'),...
                 'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','duration','Parent',tr);
  doc.createNode('td',num2str(aa(6)),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','lag #','Parent',tr);
  doc.createNode('td',num2str(aa(7)),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','Ra','Parent',tr);
  doc.createNode('td',num2str(aa(10)),'Parent',tr);

  tr=doc.createNode('tr','Parent',table);
  doc.createNode('td','Parent',tr);
  doc.createNode('td','Dec','Parent',tr);
  doc.createNode('td',num2str(aa(11)),'Parent',tr);

end


