function make_png (FOLDER, NAME)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AUTHOR: Marie-Anne Bizouard (mabizoua@lal.in2p3.fr)
%
% DATE: 28/05/2008
% SUBJECT: generates .png of the current figure
%
% LAST RELEASE:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ImageFile = [FOLDER '/' NAME '.png'];
ThumbnailFile = [FOLDER '/' NAME '_thumbnail.png'];

if (exist(FOLDER, 'dir') == 0)
  mkdir (FOLDER)
end
set(gcf,'Renderer','painters');
print('-dpng','-r120', ImageFile);
[a,b] = unix(['convert -resize 350x ' ImageFile ' -depth 8 -colors 256 ' ThumbnailFile]);

