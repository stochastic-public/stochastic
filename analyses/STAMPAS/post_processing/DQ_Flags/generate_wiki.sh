#! /usr/bin/env bash

# functions
usage(){
    echo 'Arg 1: RUN'
    echo 'Arg 2: ID'
    echo 'Arg 3: IFO1'
    echo 'Arg 4: IFO2'
    echo 'Arg 5: threshold on eff/deadtime'
    echo 'Arg 6: threshold on sigma nb'
    echo 'Arg 7: threshold on 1000*eff/segment#'

    exit
}


insert_DQ_table(){
    RUN=$1
    ID=$2
    IFO=$3

    sed "/<!-- ${IFO}-DQ-TABLE -->/ {
         h
         r ${RUN}_ID${ID}/${RUN}_${IFO}.table
         g
         N
     }" index.wiki > tmp.wiki
    mv tmp.wiki index.wiki
}

insert_loudest_table(){
    RUN=$1
    ID=$2

    sed "/<!-- LOUDEST-TABLE -->/ {
         h
         r ${RUN}_ID${ID}/loudest_events.table
         g
         N
     }" index.wiki > tmp.wiki
    mv tmp.wiki index.wiki
}

# main
[[ $# -ne 7 ]] && usage

RUN=$1
ID=$2
IFO1=$3
IFO2=$4
THR_ED=$5
THR_SIG=$6
THR_ESN=$7

# Prepare the wiki page
cp template_dqflags.wiki index.wiki

echo 'Generating report page for run ' $RUN $IFO1 $IFO2

FOLDER=${RUN}_ID${ID}

${PWD}/downselect_flags.sh ${RUN} ${ID} ${IFO1} ${THR_ED} ${THR_SIG} ${THR_ESN}
${PWD}/downselect_flags.sh ${RUN} ${ID} ${IFO2} ${THR_ED} ${THR_SIG} ${THR_ESN}

echo 'Creating veto list for ' ${IFO1} ' ...'
${PWD}/create_veto_list.sh ${RUN} ${IFO1} $FOLDER/${RUN}_${IFO1}_DQ_1.txt file_veto1   # all DQ flags
${PWD}/create_veto_list.sh ${RUN} ${IFO1} $FOLDER/${RUN}_${IFO1}_DQ_2.txt file_veto2   # DQ flags that pass criteria 1
${PWD}/create_veto_list.sh ${RUN} ${IFO1} $FOLDER/${RUN}_${IFO1}_DQ_3.txt file_veto3   # DQ flags that pass criteria 1 & 2
${PWD}/create_veto_list.sh ${RUN} ${IFO1} $FOLDER/${RUN}_${IFO1}_DQ_4.txt file_veto4   # DQ flags that pass criteria 1 & 2 & 3

mv ${RUN}_${IFO1}_veto.txt $FOLDER/${IFO1}_veto.txt

IFO_SCIENCE=`cat file_veto1 | cut -d " " -f 2`
IFO_VETO_DUR1=`cat file_veto1 | cut -d " " -f 1`
IFO_DEADTIME1=`cat file_veto1 | cut -d " " -f 3`
IFO_SEGNB1=`cat file_veto1 | cut -d " " -f 4`
IFO_VETO_DUR2=`cat file_veto2 | cut -d " " -f 1`
IFO_DEADTIME2=`cat file_veto2 | cut -d " " -f 3`
IFO_SEGNB2=`cat file_veto2 | cut -d " " -f 4`
IFO_VETO_DUR3=`cat file_veto3 | cut -d " " -f 1`
IFO_DEADTIME3=`cat file_veto3 | cut -d " " -f 3`
IFO_SEGNB3=`cat file_veto3 | cut -d " " -f 4`
IFO_VETO_DUR4=`cat file_veto4 | cut -d " " -f 1`
IFO_DEADTIME4=`cat file_veto4 | cut -d " " -f 3`
IFO_SEGNB4=`cat file_veto4 | cut -d " " -f 4`

#echo ${IFO_SEGNB1} " " ${IFO_SEGNB2} " " ${IFO_SEGNB3} " " ${IFO_SEGNB4}

sed -e "s|\[${IFO1}_SCIENCE\]|${IFO_SCIENCE}|g" \
    -e "s|\[${IFO1}_VETO_DUR1\]|${IFO_VETO_DUR1}|g" \
    -e "s|\[${IFO1}_DEADTIME1\]|${IFO_DEADTIME1}|g" \
    -e "s|\[${IFO1}_SEGNB1\]|${IFO_SEGNB1}|g" \
    -e "s|\[${IFO1}_VETO_DUR2\]|${IFO_VETO_DUR2}|g" \
    -e "s|\[${IFO1}_DEADTIME2\]|${IFO_DEADTIME2}|g" \
    -e "s|\[${IFO1}_SEGNB2\]|${IFO_SEGNB2}|g" \
    -e "s|\[${IFO1}_VETO_DUR3\]|${IFO_VETO_DUR3}|g" \
    -e "s|\[${IFO1}_DEADTIME3\]|${IFO_DEADTIME3}|g" \
    -e "s|\[${IFO1}_SEGNB3\]|${IFO_SEGNB3}|g" \
    -e "s|\[${IFO1}_VETO_DUR4\]|${IFO_VETO_DUR4}|g" \
    -e "s|\[${IFO1}_DEADTIME4\]|${IFO_DEADTIME4}|g" \
    -e "s|\[${IFO1}_SEGNB4\]|${IFO_SEGNB4}|g" \
    index.wiki > tmp.wiki
mv tmp.wiki index.wiki

echo 'Creating veto list for ' ${IFO2} ' ...'
${PWD}/create_veto_list.sh ${RUN} ${IFO2} $FOLDER/${RUN}_${IFO2}_DQ_1.txt file_veto1
${PWD}/create_veto_list.sh ${RUN} ${IFO2} $FOLDER/${RUN}_${IFO2}_DQ_2.txt file_veto2
${PWD}/create_veto_list.sh ${RUN} ${IFO2} $FOLDER/${RUN}_${IFO2}_DQ_3.txt file_veto3
${PWD}/create_veto_list.sh ${RUN} ${IFO2} $FOLDER/${RUN}_${IFO2}_DQ_4.txt file_veto4

mv ${RUN}_${IFO2}_veto.txt $FOLDER/${IFO2}_veto.txt

IFO_SCIENCE=`cat file_veto1 | cut -d " " -f 2`
IFO_VETO_DUR1=`cat file_veto1 | cut -d " " -f 1`
IFO_DEADTIME1=`cat file_veto1 | cut -d " " -f 3`
IFO_SEGNB1=`cat file_veto1 | cut -d " " -f 4`
IFO_VETO_DUR2=`cat file_veto2 | cut -d " " -f 1`
IFO_DEADTIME2=`cat file_veto2 | cut -d " " -f 3`
IFO_SEGNB2=`cat file_veto2 | cut -d " " -f 4`
IFO_VETO_DUR3=`cat file_veto3 | cut -d " " -f 1`
IFO_DEADTIME3=`cat file_veto3 | cut -d " " -f 3`
IFO_SEGNB3=`cat file_veto3 | cut -d " " -f 4`
IFO_VETO_DUR4=`cat file_veto4 | cut -d " " -f 1`
IFO_DEADTIME4=`cat file_veto4 | cut -d " " -f 3`
IFO_SEGNB4=`cat file_veto4 | cut -d " " -f 4`

sed -e "s|\[${IFO2}_SCIENCE\]|${IFO_SCIENCE}|g" \
    -e "s|\[${IFO2}_VETO_DUR1\]|${IFO_VETO_DUR1}|g" \
    -e "s|\[${IFO2}_DEADTIME1\]|${IFO_DEADTIME1}|g" \
    -e "s|\[${IFO2}_SEGNB1\]|${IFO_SEGNB1}|g" \
    -e "s|\[${IFO2}_VETO_DUR2\]|${IFO_VETO_DUR2}|g" \
    -e "s|\[${IFO2}_DEADTIME2\]|${IFO_DEADTIME2}|g" \
    -e "s|\[${IFO2}_SEGNB2\]|${IFO_SEGNB2}|g" \
    -e "s|\[${IFO2}_VETO_DUR3\]|${IFO_VETO_DUR3}|g" \
    -e "s|\[${IFO2}_DEADTIME3\]|${IFO_DEADTIME3}|g" \
    -e "s|\[${IFO2}_SEGNB3\]|${IFO_SEGNB3}|g" \
    -e "s|\[${IFO2}_VETO_DUR4\]|${IFO_VETO_DUR4}|g" \
    -e "s|\[${IFO2}_DEADTIME4\]|${IFO_DEADTIME4}|g" \
    -e "s|\[${IFO2}_SEGNB4\]|${IFO_SEGNB4}|g" \
    index.wiki > tmp.wiki
mv tmp.wiki index.wiki

rm file_veto*

echo 'Generating the loudest event table ...'
${PWD}/generate_loudest_table.sh ${RUN} ${ID}

sed -e "s|\[RUN\]|${RUN}|g" \
    -e "s|\[THR_ED\]|${THR_ED}|g" \
    -e "s|\[THR_SIG\]|${THR_SIG}|g" \
    -e "s|\[THR_ESN\]|${THR_ESN}|g" \
    -e "s|\[ID\]|${ID}|g" \
    -e "s|\[IFO1\]|${IFO1}|g" \
    -e "s|\[IFO2\]|${IFO2}|g" \
    index.wiki > tmp.wiki
mv tmp.wiki index.wiki

insert_DQ_table $RUN $ID $IFO1
insert_DQ_table $RUN $ID $IFO2

insert_loudest_table $RUN $ID
