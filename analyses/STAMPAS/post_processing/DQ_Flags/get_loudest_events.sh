#! /usr/bin/env bash

# functions
usage(){
    echo 'Arg 1: GPS_START'
    echo 'Arg 2: GPS_END'
    echo 'Arg 3: ID'
    echo 'Arg 4: loudest events nb'
    exit
}

# main
[[ $# -ne 4 ]] && usage

gps_start=$1
gps_start=$2
id=$3
loudest_evts_nb=$4

if [ ${gps_start} -lt 900000000 ]; then
    run="S5"
elif [ ${gps_start} -lt 1000000000 ]; then
    run="S6"
else
    run="O1"
fi

#LOCAL_PATH=`echo $PWD | awk '{match($0,"DQ_Flags"); print substr($0,1,RSTART-2)}'`
#export MATLABPATH=${LOCAL_PATH}/background_estimation

matlab -nodisplay -r "get_loudest_events ${gps_start} ${gps_end} ${id} ${loudest_evts_nb} ${run}"
