#! /usr/bin/env bash

##########################
# This script needs to be copied on the machines at LLO and LHO where UPV vetoed are available
# Modify the ifo name and UPV path if needed
# All UPV veto lists (Hierarchical ones) are copied in a folder UPV
#
# 29/04/2018 Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
##########################


if [ ! -d UPV ]; then
    mkdir UPV
fi

path=$PWD

ifo='H1:'

cd /home/detchar/public_html/NewUPV/O2/results

folders=`ls | grep H`

for folder in $folders
do
    echo $folder

    #folder='O2-H-1183161618-1183766418'
    timestamp=`echo $folder | cut -d "-" -f 3-`

    files=`ls $folder/*veto.txt | grep -v up_to_`
    for i in $files
    do
	filename1=`echo $i | cut -d "/" -f 2`
	filename2=`echo $filename1 | sed "s/veto/${timestamp}_veto/g"`
	cp $i $path"/UPV/"$ifo$filename2
    done
done
