---++ Introduction

---+++++ Command

Command used to generate the vetoes:
<blockquote>
cd post_processing/DQFlags/

./generate_wiki.sh Start-1164067217_Stop-1187733618_ID-5 [RUN] [ID] [IFO1] [IFO2] [THR_ED] [THR_SIG] [THR_ESN]
</blockquote>

---+++++ Selection

Flags must follow:

1. coincident loudest/flags (shift==0) - 1 sigma &gt; <coincident loudest/flags (shift!= 0)>

2. efficiency / deadtime &gt; [THR_ED]

3. sigma nb &gt; [THR_SIG]

4. 1000 * efficiency / segments nb &gt; [THR_ESN]

Remarks:
   * Criteria 1 is illustrated with [[https://ldas-jobs.ligo.caltech.edu/~mabizoua/o2-allsky/studyFlags/][plots]] for all flags
   * Criteria 2. is useful to eliminate flags that are not efficient enough wrt the flag's deadtime.
   * Criteria 3. is similar to 1. but the threshold on the number of sigmas is increased
   * Criteria 4. is downgrading DQ flags with a huge number of small segments. This is equivalent to downselecting flags that have a low coincidence significance. The coincidence significance is the number of coincident loudest/flags (Shift==0) in terms of number of sigmas.
   * Red flags dont pass criteria 2. Blue flags dont pass criteria 3. Green flags dont pass criteria 4. Black flags pass all criteria.

---++ Resuts

---+++++ H1 DQ selected flags

<!-- H1-DQ-TABLE -->

---+++++ L1 DQ selected flags

<!-- L1-DQ-TABLE -->

---+++++ Veto deadtime

|                   |                        | *Passed criteria 1*     | *Passed criteria 2*            | *Passed criteria 3*            | *Passed criteria 4*            |
| *Veto file*       | *IFO science time [s]* | *Veto duration [s] (deadtime/seg#)* | *Veto duration [s] (deadtime/seg#)* | *Veto duration [s] (deadtime/seg#)* | *Veto duration [s] (deadtime/seg#)* |
| [RUN]_H1_veto.txt | [H1_SCIENCE] | [H1_VETO_DUR1] ([H1_DEADTIME1]) [H1_SEGNB1] | [H1_VETO_DUR2] ([H1_DEADTIME2]) [H1_SEGNB2] | [H1_VETO_DUR3] ([H1_DEADTIME3]) [H1_SEGNB3] | [H1_VETO_DUR4] ([H1_DEADTIME4]) [H1_SEGNB4] |
| [RUN]_L1_veto.txt | [L1_SCIENCE] | [L1_VETO_DUR1] ([L1_DEADTIME1]) [L1_SEGNB1] | [L1_VETO_DUR2] ([L1_DEADTIME2]) [L1_SEGNB2] | [L1_VETO_DUR3] ([L1_DEADTIME3]) [L1_SEGNB3] | [L1_VETO_DUR4] ([L1_DEADTIME4]) [L1_SEGNB4] |


---+++++ Selected DQ safety


---+++++ Loudest events results

<!-- LOUDEST-TABLE -->