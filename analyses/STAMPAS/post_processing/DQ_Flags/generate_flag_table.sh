#! /usr/bin/env bash

# functions
usage(){
    echo 'Arg 1: GPS_START'
    echo 'Arg 2: GPS_END'
    echo 'Arg 3: ID'
    echo 'Arg 4: IFO1'
    echo 'Arg 5: IFO2'
    echo 'Arg 6: folder suffix [<run>_ID<ID>_<loudest triggers nb>]'
    exit
}

[[ $# -ne 6 ]] && usage

GPS_START=$1
GPS_END=$2
ID=$3
IFO1=$4
IFO2=$5
folder_suffix=$6

if [ ${GPS_START} -lt 900000000 ]; then
    RUN="S5"
    search='s5-allsky'
elif [ ${GPS_START} -lt 1000000000 ]; then
    RUN="S6"
    search='s6-allsky'
else
    RUN="O1"
    search='o1-allsky'
fi

cat ${folder_suffix}/${folder_suffix}.txt  | awk '{printf("%d %7.2f %12.2f %12.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %5.3f\n",NR,$6, $1+($7+$8)/2.,$12+($7+$8)/2,$8-$7,$13,$4,$5,$2,$3,$9)}' > tmp.txt

cat ${folder_suffix}/${folder_suffix}.txt | awk '{printf("%f %f \n", $1+$7, $1+$8)}' > H1_times.txt
cat ${folder_suffix}/${folder_suffix}.txt | awk '{printf("%f %f \n", $12+$7, $12+$8)}' > L1_times.txt

echo '| *ID* | *SNR* | *GPS H1* | *GPS L1* | *Dur. [s]* | *Lag #* | *f_min [Hz]* | *f_max [Hz]* | *RA [hr]* | *dec [deg.]* | *SNR_frac* | *Plots* | *Omega Scan (H1)* | *Omega Scan (L1)* | *DQ H1* | *DQ L1* |' > ${folder_suffix}_table.txt

cat tmp.txt | awk '{print "|"$1 "|"$2 "|"$3 "|"$4 "|"$5 "|"$6 "|"$7 "|"$8 "|"$9 "|"$10 "|"$11 "|[[https://ldas-jobs.ligo.caltech.edu/~prestegard/S6_bg/plots_v1/"NR"/][link]] |[[https://ldas-jobs.ligo.caltech.edu/~mabizoua/OMEGA/H1/"int($3)"][scan]] | [[https://ldas-jobs.ligo.caltech.edu/~mabizoua/OMEGA/L1/"int($4)"][scan]]|["$3"DQH1]|["$4"DQL1]|"}' >> ${folder_suffix}_table.txt 

mv ${folder_suffix}_table.txt table_tmp1

echo 'extracting DQ information for H1 ...'
for i in {1..100}; do
    cat H1_times.txt | awk -v ind=$i '{if (NR==ind) print $0}' > tmp
    gps=`cat tmp | awk '{printf("%10.2f\n", ($1+$2)/2)}'`
    a=`segexpr 'intersection(tmp, ${RUN}_H1_veto.txt)'`
    
    if [ ${#a} -gt 0 ]; then
	echo $gps 'YES'
	sed -e "s|\[${gps}DQH1\]|YES|g" table_tmp1 > table_tmp2
	cp table_tmp2 table_tmp1
    else
	echo $gps ' '
	sed -e "s|\[${gps}DQH1\]| |g" table_tmp1 > table_tmp2
	cp table_tmp2 table_tmp1
    fi
done

echo 'extracting DQ information for L1 ...'
for i in {1..100}; do
    cat L1_times.txt | awk -v ind=$i '{if (NR==ind) print $0}' > tmp
    gps=`cat tmp | awk '{printf("%10.2f\n", ($1+$2)/2)}'`
    a=`segexpr 'intersection(tmp, ${RUN}_L1_veto.txt)'`

    if [ ${#a} -gt 0 ]; then
	echo $gps ' YES'
	sed -e "s|\[${gps}DQL1\]|YES|g" table_tmp1 > table_tmp2
	cp table_tmp2 table_tmp1
    else
	echo $gps ' '
	sed -e "s|\[${gps}DQL1\]| |g" table_tmp1 > table_tmp2
	cp table_tmp2 table_tmp1
    fi
done


mv table_tmp1 ${folder_suffix}.table
rm table_tmp2
rm tmp.txt
rm tmp



