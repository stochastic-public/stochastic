function extractGPSfromTriggers (ifo1, ifo2, filename_prefix)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script extract the GPS start and end times in each detector
% from a list of triggers. This list will be used to create shifted
% lists for the DQ study.
%
% The output list can ordered chronologically or in the same
% order than the input list.
%
% Written by: S. Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Input list
l=load([filename_prefix '.txt']);

%% Ordering
chrono=1; %% Set to 1 to order the ouput list chronologically, 
          %% to 0 elsewhise.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
o_gpsI1=[l(:,3) l(:,3)+l(:,5)];
o_gpsI2=[l(:,4) l(:,4)+l(:,5)];

%% List ordered
if chrono
   o_gpsI1=sortrows(o_gpsI1);
   o_gpsI2=sortrows(o_gpsI2);
   suffix='chrono';
else
   suffix='ranked';
end


gpsI1=unique(o_gpsI1,'rows');
gpsI2=unique(o_gpsI2,'rows');

name=[filename_prefix '_' suffix];

dlmwrite([name '_' ifo1 '.txt'],gpsI1,'delimiter',' ','precision','%.1f');
dlmwrite([name '_' ifo2 '.txt'],gpsI2,'delimiter',' ','precision','%.1f');

tshifts=[-400,-350,-300,-250,0,250,300,350,400];

for TS=1:length(tshifts)
   l=gpsI1+tshifts(TS);
   dlmwrite([filename_prefix '_' ifo1 '_' num2str(tshifts(TS)) '.txt'],l,'delimiter',' ','precision','%.1f');
   l=gpsI2+tshifts(TS);
   dlmwrite([filename_prefix '_' ifo2 '_' num2str(tshifts(TS)) '.txt'],l,'delimiter',' ','precision','%.1f');
end

exit
