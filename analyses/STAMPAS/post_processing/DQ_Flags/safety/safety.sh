LIST=`cat ../S5_ID12_100/S5_H1_DQ_1.txt`

echo 'Checking safety ... output is empty if all flags are safe'

if [ -f H1.out ]; then
    rm H1.out
fi

if [ -f L1.out ]; then
    rm L1.out
fi

for dq in $LIST; do
    ans=`awk -v channel=$dq '{
	if ($1==channel) print $2 ;
	    }' CBC_HIGHMASS.txt CBC_LOWMASS.txt CBC_LOWMASS2.txt CBC_RINGDOWN.txt CBC_RINGDOWN2.txt`
    echo $dq $ans >> H1.out
done


LIST=`cat ../S5_ID12_100/S5_L1_DQ_1.txt`

for dq in $LIST; do
    ans=`awk -v channel=$dq '{
	if ($1==channel) print $2 ;
	    }' CBC_HIGHMASS.txt CBC_LOWMASS.txt CBC_LOWMASS2.txt CBC_RINGDOWN.txt CBC_RINGDOWN2.txt`
    echo $dq $ans >> L1.out
done

cat H1.out | awk '{if (NF==1) print $1 " is unsafe?"}'
cat L1.out | awk '{if (NF==1) print $1 " is unsafe?"}'

