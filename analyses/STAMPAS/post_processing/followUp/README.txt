Last edited: 28/03/2014
Contact: S. Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The plotTriggers.m script is done to re-process a ft-map inside which
a trigger of interest has been found. By giving the references of the search
and the (GPS,RA,DECL,Lag) of the trigger of interest, the script recovers
all relevant informations and process the corresponding ft-map,
including the waveform to use to re-process an injection. This is useful
for follow up purposes. Full all-sky search can be also done.

NOTE: As long as the GPS and the lag given has been processed by during your search,
the map will be re-processed even if the RA,DEC coordinates are different or if no
trigger was inside the investigated ft-map. This can be useful in particular to
understand why an injection has been recovered.

Just fill the fields in plotTriggers.m and launch the run script.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Options:
   - altSigma: if you want to use altSigma calculation
   - allSky: if you want to run a full all-sky search for the time segment
              you are interested in.
   - altInjection: Set true if you used altInjection during your search

   - params.saveMat: set true if you want to save the map matfiles
   - params.savePlots: set true if you want to save the diagnostic plots
