function hrss=get_hrss(hp,hx,sampleRate,alpha)

% 
% Functions computes the hrss of a waveform
% Inputs: hp: 1D vect of h+ time series
%         hx: 1D vect of hx time series
%         sampleRate: sampling frequency of the time series
%         alpha: sqrt(alpha) is the amplitude factor applied to the
%         waveform
%
% Contact: Marie Anne Bizouard (mabizoua@lal.in2p3.fr) 2015
%


if sampleRate<=0
  warning(['Sampling rate cannot be null: ' num2str(sampleRate)]);
  hrss=0;
  return;
else
  deltaT=1./sampleRate;
end

sum=0;
if size(hp,1) ~= size(hx,1)
  error(['hp vector size ' num2str(size(hp,1)) ...
	 ' is different from hx vector size ' ...
	 num2str(size(hx,1))]);
  hrss=0;
  return;
end

for i=1:size(hp,1)
  sum=sum+hp(i)*hp(i)+hx(i)*hx(i);
end

hrss=sqrt(sum*deltaT)*sqrt(alpha);
