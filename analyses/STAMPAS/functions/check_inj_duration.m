function check_inj_duration(new_job_list,DurInj,NInj,coeffInj)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function check_inj_duration()
%
% This function checks that the GPS start and end times
% correspond to period large enough to contain a number 
% of STAMPAS jobs equal to the number of injections 
% required. It stops the processing if it is not the case.
%
%Written by: Samuel Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


effective_time=floor(new_job_list(:,4)/DurInj)*DurInj;
total_effective_time=sum(effective_time);

if total_effective_time/DurInj<NInj*coeffInj
   error(['You need more GPS time to make your injections. ' num2str(NInj*DurInj*coeffInj-total_effective_time) ' seconds of data needed.'])
end

