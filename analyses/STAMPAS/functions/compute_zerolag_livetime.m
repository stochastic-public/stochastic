function compute_zerolag_livetime (searchPath, list_of_jobs, overlap)

%
% Zero-lag lifetime calculation
%


timeTotZL=0;
group_min=min(list_of_jobs(:,5));
group_max=max(list_of_jobs(:,5));
for zlGr=group_min:group_max
  tmpZL=list_of_jobs(list_of_jobs(:,5)==zlGr,:);
  nbJobs=size(tmpZL,1);
  timeTotZL=timeTotZL+sum(tmpZL(:,4))-(nbJobs-1)*overlap;
end

display(['Total zero-lag lifetime: ' num2str(timeTotZL) ' (s)']);
dlmwrite([searchPath '/tmp/TTimeZeroLag.txt'],timeTotZL,'precision','%.0f');
DaysZL = timeTotZL/86400;
display(['Total zero-lag lifetime: ' num2str(DaysZL) ' (days)']);

