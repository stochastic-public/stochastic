function [I] = intersect_joblist(A,B)  
% intersect_joblist : intersect two jobfile
% DESCRIPTION :
%   make intersection between two jobfiles
%
% SYNTAX :
%   [I] = intersect_joblist (A,B)
% 
% INPUT : 
%    A : jobfile 1
%    B : jobfile 2
%
% OUTPUT :
%    I : jobfile intersected
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : APR 2017
%

[ra,ca]=size(A);
[rb,cb]=size(B);

if ra>rb  
  jobfile1 = B;
  jobfile2 = A;
else
  jobfile1 = A;
  jobfile2 = B;
end

I=[];
for nj=1:size(jobfile1,1)
 ii=jobfile1(nj,3)>=jobfile2(:,2)&jobfile1(nj,2)<=jobfile2(:,3);
 if all(~ii);continue;end
 tmp = jobfile2(ii,:);
 tmp(1,2) = max(tmp(1,2),jobfile1(nj,2));
 tmp(1,2) = diff(tmp(1,2:3));
 tmp(end,3) = min(tmp(end,3),jobfile1(nj,3));
 tmp(end,2) = diff(tmp(end,2:3));

 I = [I;tmp];
end
I(:,1) = (1:size(I,1))';
end