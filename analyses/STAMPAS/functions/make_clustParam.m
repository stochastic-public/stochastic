function make_clustParam(searchPath,pair,fmin,fmax,doInj,doZebra,doSeedless,doSeedlessAllsky,seedlessParams,doLonetrack,doGPU,doParallel,doDgPlots,doVariableWindows,overlap,altSigma,noStitching,segDur,matfilesPath,matfilesName,bufferSegments,doFixedPol,windowLimit,doTFNotch,TFNotchesFile,doStampFreqMask,fixedSkyPosition,fixedRa,fixedDec);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function creates the clustParam.mat file
% read by run_clustermap.m. Contains parameters
% identical for each job of the search.
%
% Written by: S. Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fmin=num2str(fmin);
fmax=num2str(fmax);
doInj=num2str(doInj);
doZebra=num2str(doZebra);
doSeedless = num2str(doSeedless);
doSeedlessAllsky = num2str(doSeedlessAllsky);
seedlessParams = seedlessParams;
doLonetrack = num2str(doLonetrack);
doGPU = num2str(doGPU);
doParallel = num2str(doParallel);
doDgPlots=num2str(doDgPlots);
doVariableWindows=num2str(doVariableWindows);
overlap=num2str(overlap);
segDur=num2str(segDur);
doStampFreqMask = num2str(doStampFreqMask);
fSave=[searchPath '/tmp/clustParam.mat'];
save(fSave,'pair','fmin','fmax','doInj','doZebra','doSeedless','doSeedlessAllsky',...
     'seedlessParams','doLonetrack','doGPU','doParallel','doDgPlots', ...
     'doVariableWindows','overlap','altSigma','noStitching','segDur', ...
     'matfilesPath','matfilesName','bufferSegments','doFixedPol', ...
     'windowLimit','doTFNotch','TFNotchesFile','doStampFreqMask',...
     'fixedSkyPosition','fixedRa','fixedDec');

return;
