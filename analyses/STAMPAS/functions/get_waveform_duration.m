function [duration]=get_waveform_duration(waveform_path, waveform)

%
% Function to determine the duration of a waveform. 
% In case of a "folder" type waveform, it assumes that all
% waveforms in the same folder has the same duration
%
% Marie Anne Bizouard (mabizua@lal.in2p3.fr)
%

% The waveform is actually a folder
onthefly = 0;
ontheflytypes = {'sinusoid','inspiral','rmodes','magnetar'};
for ii = 1:length(ontheflytypes)
   if findstr(waveform, ontheflytypes{ii})
      onthefly = 1;
   end
end
if ~isempty(regexp(waveform,'.*/$'))
  temp = dir([waveform_path waveform]);
  temp = regexpi({temp.name},'(.*\.dat.*|.*\.txt.*)', 'tokens','once');
  temp = vertcat(temp{:});
  ht=load([waveform_path '/' waveform temp{1}]);
  duration=ht(end,1)-ht(1,1)
elseif onthefly
  ht=load([waveform_path '/' waveform]);
  duration=ht.duration;
else
  ht=load([waveform_path '/' waveform]);
  duration=ht(end,1)-ht(1,1);
end


