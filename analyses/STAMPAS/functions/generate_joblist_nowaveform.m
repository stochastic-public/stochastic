function [joblist]=generate_joblist_nowaveform(searchPath,waveform_path,waveforms)

%
% function make_dag_inj()
%
% This function generates dag file for STAMPAS injection 
% studies. One job corresponds to one injection.
%
% Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
%

dic = classes.waveforms.dictionary;

njobs=0;

jobs=load([searchPath '/tmp/joblist_injections.txt']);
injections=load([searchPath '/tmp/radec.txt']);
Nbwvf=size(waveforms,1);
final_joblist=[];

for j=1:Nbwvf % Loop over each waveform
  wheader = dic.headers(waveforms{j});
  duration = wheader.duration;

  for i=1:size(injections,1);
    t_start=injections(i,2+j);
    t_stop=injections(i,2+j)+duration;
    ext=find(t_stop>jobs(:,2) & t_start<jobs(:,3));
    if size(ext,1)>0
      selected_jobs=jobs(ext,:);
    else
      display([num2str(i) ' ' num2str(t_start) ' ' num2str(t_stop) ...
	       ' ' num2str(j)]);
      error('All foreseen injections should be found in at least 1 job')
    end


    % add, if they exist, the job before and the next jobs
    ext1=find(jobs(:,5)==selected_jobs(1,5));
    if size(ext1,1)>0
      prev_job=find(jobs(ext1(:,1),1)==selected_jobs(1,1)-1);
      if size(prev_job,1)>0
	final_joblist=[final_joblist; jobs(ext1(prev_job),:)];
      end
    
      final_joblist=[final_joblist;selected_jobs];
      
      next_job=find(jobs(ext1(:,1),1)==selected_jobs(end,1)+1);
      if size(next_job,1)>0
	final_joblist=[final_joblist; jobs(ext1(next_job),:)];
      end
    else
      final_joblist=[final_joblist;selected_jobs];
    end
    
  end
end

final_joblist=sortrows(final_joblist,1);
joblist=unique(final_joblist,'rows');

