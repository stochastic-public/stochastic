function LIST = shorten_jobs_list_overlap(LIST,segDur)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function shorten_jobs_list_overlap()
%
% This functions creates, from a source joblist, a joblist with jobs 
% of duration *duration*. An overlap between the jobs can be included.
% If *fixed_duration* is set to 0, jobs longer than *duration* but not
% long enough to be splited into two overlapping jobs are kept unique.
% If *fixed_duration* is set to 1, their duration is shorten to *duration*
% If add_group is 1, a fifth column is added in the file, identifying 
% all the jobs issued from a single job of the original joblist. 
%
%Written by: Samuel Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if segDur <= 1
    return 
end

gpss = LIST(1,2):segDur:LIST(end,3);

for i=1:length(LIST(:,1)) %% loop on each science segment

  gpsStart = LIST(i,2); gpsEnd = LIST(i,3);

  indexes = find(gpss >= gpsStart);
  index = indexes(1);
  LIST(i,2) = gpss(index);

  indexes = find(gpss <= gpsEnd);
  index = indexes(end);
  LIST(i,3) = gpss(index);

  LIST(i,4) = LIST(i,3) - LIST(i,2);

end


