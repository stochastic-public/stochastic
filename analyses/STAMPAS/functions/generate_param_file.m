function generate_param_file(fileName,parameters,searchPath)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function generate_param_file()
%
% This function writes down the parameter files needed to
% perform bkg and inj STAMPAS analysis. It copies the
% params_0.txt file, containing parameters which cannot
% be set by the user using STAMPAS, the new file being
% named fileName. Then it completes the file by appending
% the appropriate lines.
%
%Written by: Samuel Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% current interactive mode assumes you are working 2 directories
% below the main STAMPAS directory.
fname = [searchPath '/../../params_0.txt'];

% copy params_0.txt to search directory
system(['cp ' fname ' ' fileName]);

fid = fopen(fileName,'a+');

%% Parameters regarding the use of MC data
if parameters.doDetectorNoiseSim
  fprintf(fid,'doDetectorNoiseSim true\n');
  %   fprintf(fid,'doDetectorNoiseSim true" >> ' fileName]);
  if parameters.pair=='H1L1'
    fprintf(fid,'doDifferentNoise false\n');
    fprintf(fid,'DetectorNoiseFile %s\n',parameters.DetectorNoiseFileLIGO);
  elseif parameters.pair=='H1V1'
    fprintf(fid,'doDifferentNoise true\n');
    fprintf(fid,'DetectorNoiseFile %s\n',parameters.DetectorNoiseFileLIGO);
    fprintf(fid,'DetectorNoiseFile2 %s\n',parameters.DetectorNoiseFileVirgo);
  elseif parameters.pair=='V1L1'
    fprintf(fid,'doDifferentNoise true\n');
    fprintf(fid,'DetectorNoiseFile2 %s\n',parameters.DetectorNoiseFileLIGO);
    fprintf(fid,'DetectorNoiseFile %s\n',parameters.DetectorNoiseFileVirgo);
  end
else
  fprintf(fid,'doDetectorNoiseSim false\n');
end


%% Parameters regarding the storage of pre-processed data
if parameters.doAnteproc %% Bkg studies
  fprintf(fid,'storemats true\n');
  fprintf(fid,'outputfiledir %s\n',parameters.outputfiledir);
  fprintf(fid,'outputfilename %s\n',parameters.outputfilename);
  fprintf(fid,'outputfilepath %s\n',parameters.outputfiledir);
  fprintf(fid,'anteproc.timeShift1 %s\n', parameters.tShift1);
else %% Injections
  fprintf(fid,'storemats false\n');
end

fprintf(fid,'anteproc.jobFileTimeShift false\n');
if parameters.largeShift
  fprintf(fid,'anteproc.timeShift2 %s\n', parameters.largeShiftTime2);
end

%% Set seed
fprintf(fid,'pp_seed %s\n',parameters.ppseed);

%% Enables the largeShift option, only used for injections
if parameters.largeShift
  fprintf(fid,'largeShift true\n');
  fprintf(fid,'largeShiftTime2 %s\n',parameters.largeShiftTime2);
else
  fprintf(fid,'largeShift false\n');
end

%% parameters for mdc injection
if parameters.stampmdc
  fprintf(fid,'stampmdc true\n');
  fprintf(fid,'stamp.mdclist %s\n',parameters.mdclist);
  fprintf(fid,'stamp.mdcchannel1 %s\n',parameters.mdcchannel1);
  fprintf(fid,'stamp.mdcchannel2 %s\n',parameters.mdcchannel2);
end

%% Relevant parameters of an injection
if parameters.stampinj
  fprintf(fid,'stampinj true\n');
end
if parameters.stampinj || parameters.stampmdc
  fprintf(fid,'stamp.ra %s\n',parameters.ra);
  fprintf(fid,'stamp.decl %s\n',parameters.decl);
  fprintf(fid,'stamp.startGPS %s\n',parameters.startGPS);
  fprintf(fid,'stamp.file %s\n',parameters.injfile);
  fprintf(fid,'stamp.alpha %s\n',parameters.alpha);

  if regexp(parameters.injfile, 'sinusoid')
     fprintf(fid,'stamp.inj_type fly\n');
     fprintf(fid,'stamp.fly_waveform sinusoid\n');
     ht=load(parameters.injfile);
     fprintf(fid,'stamp.h0 %.10e\n',ht.h0);
     fprintf(fid,'stamp.f0 %.5f\n',ht.f0);
     fprintf(fid,'stamp.phi0 %.5f\n',ht.phi0);
     fprintf(fid,'stamp.fdot %.10e\n',ht.fdot);
     fprintf(fid,'stamp.start %.5f\n',str2num(parameters.startGPS));
     fprintf(fid,'stamp.duration %.5f\n',ht.duration);
  elseif regexp(parameters.injfile, 'inspiral')
     fprintf(fid,'stamp.inj_type fly\n');
     fprintf(fid,'stamp.fly_waveform inspiral\n');
     ht=load(parameters.injfile);
     fprintf(fid,'stamp.mass1 %.5f\n',ht.mass1);
     fprintf(fid,'stamp.mass2 %.5f\n',ht.mass2);
     fprintf(fid,'stamp.start %.5f\n',str2num(parameters.startGPS));
     fprintf(fid,'stamp.duration %.5f\n',ht.duration);
  elseif regexp(parameters.injfile, 'gw170817')
     fprintf(fid,'stamp.inj_type fly\n');
     fprintf(fid,'stamp.fly_waveform inspiral\n');
     ht=load(parameters.injfile);
     fprintf(fid,'stamp.mass1 %.5f\n',ht.mass1);
     fprintf(fid,'stamp.mass2 %.5f\n',ht.mass2);
     fprintf(fid,'stamp.start %.5f\n',str2num(parameters.startGPS));
     fprintf(fid,'stamp.duration %.5f\n',ht.duration);
  elseif regexp(parameters.injfile, '^(waveforms/)?rmodes')
     fprintf(fid,'stamp.inj_type fly\n');
     fprintf(fid,'stamp.fly_waveform rmodes\n');
     ht=load(parameters.injfile);
     fprintf(fid,'stamp.alphasat %.5f\n',ht.alpha);
     fprintf(fid,'stamp.f0 %.5f\n',ht.f0);
     fprintf(fid,'stamp.start %.5f\n',str2num(parameters.startGPS));
     fprintf(fid,'stamp.duration %.5f\n',ht.duration);
  elseif regexp(parameters.injfile, 'msmagnetar')
     fprintf(fid,'stamp.inj_type fly\n');
     fprintf(fid,'stamp.fly_waveform msmagnetar\n');
     ht=load(parameters.injfile);
     fprintf(fid,'stamp.f0 %.5f\n',ht.f0);
     fprintf(fid,'stamp.tau %.5f\n',ht.tau);
     fprintf(fid,'stamp.nn %.5f\n',ht.nn);
     fprintf(fid,'stamp.epsilon %.5f\n',ht.epsilon);
     fprintf(fid,'stamp.start %.5f\n',str2num(parameters.startGPS));
     fprintf(fid,'stamp.duration %.5f\n',ht.duration);
     try
       fprintf(fid,'stamp.cosi %.5f\n',ht.cosi);
     catch
     end 
 elseif regexp(parameters.injfile, 'magnetar')
     fprintf(fid,'stamp.inj_type fly\n');
     fprintf(fid,'stamp.fly_waveform magnetar\n');
     ht=load(parameters.injfile);
     fprintf(fid,'stamp.eB %.5f\n',ht.eB);
     fprintf(fid,'stamp.f0 %.5f\n',ht.f0);
     fprintf(fid,'stamp.start %.5f\n',str2num(parameters.startGPS));
     fprintf(fid,'stamp.duration %.5f\n',ht.duration);
  else
     fprintf(fid,'stamp.inj_type adi\n');
  end

  if parameters.doRandPol&(~parameters.fixedPol)
    fprintf(fid,'stamp.iota acosd(1-2*rand)\n');
    fprintf(fid,'stamp.psi rand*360\n');
  elseif parameters.doRandPol&(parameters.fixedPol)
    fprintf(fid,'stamp.iota %s\n',parameters.iota);
    fprintf(fid,'stamp.psi %s\n',parameters.psi);
  end
else
  fprintf(fid,'stampinj false\n');
end

%% Shift only used in injections.
if parameters.doShift
  fprintf(fid,'shiftTime1 %s\n',parameters.shiftTime');
else
  fprintf(fid,'shiftTime1 0\n');
end

%% 'Set names of the frames channel
%% to use depending on the pair of interferometers.
if parameters.doAnteproc %% Background case
  if parameters.detector==1
    det=parameters.pair(1:2);
  elseif parameters.detector==2
    det=parameters.pair(3:4);
  else
    error('parameters.detector should be 1 or 2')
  end
  fprintf(fid,'ifo1 %s\n',det);
  if parameters.pair=='H1L1'
    fprintf(fid,'cacheFile %scache_%s_%s%s.mat\n',parameters.cachePath,parameters.pair,parameters.run,parameters.cacheSuffix);
    if strcmp(parameters.run, 'S6')
      fprintf(fid,'ASQchannel1 LDAS-STRAIN\n');
      fprintf(fid,'frameType1 %s_LDAS_C02_L2\n',det);
    elseif strcmp(parameters.run, 'ER7') || ...
	  strcmp(parameters.run, 'ER8') || ...
	  strcmp(parameters.run, 'O1') || ...
          strcmp(parameters.run, 'O2') || ...
          strcmp(parameters.run, 'O3')
      % C00
      %fprintf(fid,'ASQchannel1 GDS-CALIB_STRAIN\n');
      %fprintf(fid,'frameType1 %s_HOFT_C00\n',det);

      % C00 CLEANED
      %fprintf(fid,'ASQchannel1 GDS-CALIB_STRAIN_CLEAN\n');
      %fprintf(fid,'frameType1 %s_HOFT_C00\n',det);

      % CO1
      %fprintf(fid,'ASQchannel1 DCS-CALIB_STRAIN_C01\n');
      %fprintf(fid,'frameType1 %s_HOFT_C01\n',det);

      % C01 CLEANED
      fprintf(fid,'ASQchannel1 DCS-CALIB_STRAIN_CLEAN_C01\n');
      fprintf(fid,'frameType1 %s_HOFT_C01\n',det);

      % CO2
      %      fprintf(fid,'ASQchannel1 DCH-CLEAN_STRAIN_C02\n');
      %      fprintf(fid,'frameType1 %s_CLEANED_HOFT_C02\n',det);
    else
      fprintf(fid,'ASQchannel1 LSC-STRAIN\n');
      fprintf(fid,'frameType1 %s_RDS_C03_L2\n',det);
    end
  elseif parameters.pair=='H1V1';
    fprintf(fid,'cacheFile %scache_%s_%s%s.mat\n',parameters.cachePath,parameters.pair,parameters.run,parameters.cacheSuffix);
    if det=='H1'
      fprintf(fid,'ASQchannel1 LDAS-STRAIN\n');
      fprintf(fid,'frameType1 H1_LDAS_C02_L2\n');
    elseif det=='V1'
      fprintf(fid,'frameType1 HrecOnline\n');
      fprintf(fid,'ASQchannel1 h_16384Hz\n');
    else
      error('Problem with det definition')
    end
  elseif parameters.pair=='V1L1';
    fprintf(fid,'cacheFile %scache_%s_%s%s.mat\n',parameters.cachePath,parameters.pair,parameters.run,parameters.cacheSuffix);
    if det=='L1'
      fprintf(fid,'ASQchannel1 LDAS-STRAIN\n');
      fprintf(fid,'frameType1 H1_LDAS_C02_L2\n');
    elseif det=='V1'
      fprintf(fid,'frameType1 HrecOnline\n');
      fprintf(fid,'ASQchannel1 h_16384Hz\n');
    else
      error('Problem with det definition')
    end
  else
    error('Error with pair definition')
  end  
else
  if parameters.pair=='H1L1'
    fprintf(fid,'cacheFile %scache_%s_%s%s.mat\n',parameters.cachePath,parameters.pair,parameters.run,parameters.cacheSuffix);
    fprintf(fid,'ifo1 H1\n');
    fprintf(fid,'ifo2 L1\n');
    if strcmp(parameters.run, 'S6')
      fprintf(fid,'ASQchannel1 LDAS-STRAIN\n');
      fprintf(fid,'ASQchannel2 LDAS-STRAIN\n');
      fprintf(fid,'frameType1 H1_LDAS_C02_L2\n');
      fprintf(fid,'frameType2 L1_LDAS_C02_L2\n');
    elseif strcmp(parameters.run, 'ER7') || ...
            strcmp(parameters.run, 'ER8') || ...
            strcmp(parameters.run, 'O1')

% C00
%      fprintf(fid,'ASQchannel1 GDS-CALIB_STRAIN\n');
%      fprintf(fid,'ASQchannel2 GDS-CALIB_STRAIN\n');
%      fprintf(fid,'frameType1 H1_HOFT_C00\n');
%      fprintf(fid,'frameType2 L1_HOFT_C00\n');
% C01
      fprintf(fid,'ASQchannel1 DCS-CALIB_STRAIN_C01\n');
      fprintf(fid,'ASQchannel2 DCS-CALIB_STRAIN_C01\n');
      fprintf(fid,'frameType1 H1_HOFT_C01\n');
      fprintf(fid,'frameType2 L1_HOFT_C01\n');
    elseif strcmp(parameters.run, 'O2') %C02
      fprintf(fid,'ASQchannel1 DCH-CLEAN_STRAIN_C02\n');
      fprintf(fid,'ASQchannel2 DCH-CLEAN_STRAIN_C02\n');
      fprintf(fid,'frameType1 H1_CLEANED_HOFT_C02\n');
      fprintf(fid,'frameType2 L1_CLEANED_HOFT_C02\n');
    elseif strcmp(parameters.run, 'O3') % C01
      fprintf(fid,'ASQchannel1 DCS-CALIB_STRAIN_CLEAN_C01\n');
      fprintf(fid,'ASQchannel2 DCS-CALIB_STRAIN_CLEAN_C01\n');
      fprintf(fid,'frameType1 H1_HOFT_C01\n');
      fprintf(fid,'frameType2 L1_HOFT_C01\n');
    else
      fprintf(fid,'ASQchannel1 LSC-STRAIN\n');
      fprintf(fid,'ASQchannel2 LSC-STRAIN\n');
      fprintf(fid,'frameType1 H1_RDS_C03_L2\n');
      fprintf(fid,'frameType2 L1_RDS_C03_L2\n');
    end
  elseif parameters.pair=='H1V1'
    fprintf(fid,'cacheFile %scache_%s_%s%s.mat\n',parameters.cachePath,parameters.pair,parameters.run,parameters.cacheSuffix);
    fprintf(fid,'ifo1 H1\n');
    fprintf(fid,'ifo2 V1\n');
    fprintf(fid,'ASQchannel1 LDAS-STRAIN\n');
    fprintf(fid,'ASQchannel2 h_16384Hz\n');
    fprintf(fid,'frameType1 H1_LDAS_C02_L2\n');
    fprintf(fid,'frameType2 HrecOnline\n');
  elseif parameters.pair=='V1L1'
    fprintf(fid,'cacheFile %scache_%s_%s%s.mat\n',parameters.cachePath,parameters.pair,parameters.run,parameters.cacheSuffix);
    fprintf(fid,'ifo2 L1\n');
    fprintf(fid,'ifo1 V1\n');
    fprintf(fid,'ASQchannel2 LDAS-STRAIN\n');
    fprintf(fid,'ASQchannel1 h_16384Hz\n');
    fprintf(fid,'frameType2 H1_LDAS_C02_L2\n');
    fprintf(fid,'frameType1 HrecOnline\n');
  else
    error('The pair of ifos you chose does not exist')
  end
end

fclose(fid);

return
