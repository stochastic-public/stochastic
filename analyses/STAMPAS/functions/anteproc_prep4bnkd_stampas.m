function joblist=anteproc_prep4bnkd_stampas(searchPath, startGPS,endGPS, overlap,inmats1,inmats2,job_time,segmentDur,noStitching,joblist)
% function anteproc_prep4bknd_stampas()
%
% Creates a jobfile for doing a background study using
% data pre-processed by anteproc.  Searches the specified
% directories for all available data and creates a new
% containing jobs of a user-specified length. Assumes that the user
% will set params.anteproc.bkndstudy = true when doing
% the background study in order to remove gaps in the
% data.
%
% The user needs to specify the start and end GPS times
% of the available data set, the segment duration, the
% locations of the data for the two detectors, and the
% desired length of the jobs in the new jobfile.
%
% This script outputs a new jobfile called 'bknd_jobfile.txt'.
%
% Written by T. Prestegard (prestegard@physics.umn.edu)
% modified by S. Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Users - modify these parameters. %%%%%%%%%%%%%%%%%%%%%%%
% Start and end GPS times to find data between.
params.startGPS = startGPS;
params.endGPS = endGPS;

% Segment duration of available data.
params.segmentDuration = segmentDur;

useCachefiles = exist([searchPath '/anteproc_cache.mat'])==2;

% Location of data.
params.anteproc.inmats1 = inmats1;
params.anteproc.inmats2 = inmats2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long g
% Useful quantities.
segDur = params.segmentDuration/2;
nSegsPerJob = job_time/segDur - 1;
nSegsOverlap = overlap/segDur;
nSegsReduced = nSegsPerJob - nSegsOverlap;

if segDur > 1
   joblistDur = 1;
else
   joblistDur = segDur;
end

% Don't change these parameters!
params.anteproc.jobFileTimeShift = false;
params.anteproc.timeShift1 = 0;
params.anteproc.timeShift2 = 0;
if ~useCachefiles
  params.anteproc.useCache = false;
else
  params.anteproc.useCache = true;
  params.anteproc.cacheFile= [searchPath '/anteproc_cache.mat'];
end

% Find all anteproc'd .mat files with GPS times
% within the specified start and end times.
matlist = find_anteproc_mats(params);

% Loop over mat files corresponding to detector one.  Calculate data
% available based on GPS start time and total length (determined from file
% names).  Make a list (gps1) of all segment START times available.
gps1 = [];
gps1bis=[];
for ii=1:length(matlist.ifo1)
  gps_start = regexp(matlist.ifo1{ii},'-(\d{9,10}(|\.\d+))-','tokens','once');
  gps_start = str2num(gps_start{:});
  dur = regexp(matlist.ifo1{ii},'-(\d+|\d+\.\d+)\.mat','tokens','once');
  dur = str2num(dur{:});
  gps1 = [gps1 gps_start:segDur:(gps_start + dur - params.segmentDuration)];
  if noStitching
     gps1tmp=[gps_start:segDur:(gps_start + dur - params.segmentDuration)];
     gps1bis= [gps1bis gps1tmp gps1(end)+segDur];
  end
end

% Do same calculation for detector 2.
gps2 = [];
gps2bis=[];
for ii=1:length(matlist.ifo2)
  gps_start = regexp(matlist.ifo2{ii},'-(\d{9,10}(|\.\d+))-','tokens','once');
  gps_start = str2num(gps_start{:});
  dur = regexp(matlist.ifo2{ii},'-(\d+|\d+\.\d+)\.mat','tokens','once');
  dur = str2num(dur{:});
  gps2 = [gps2 gps_start:segDur:(gps_start + dur - params.segmentDuration)];
  if noStitching
    gps2tmp=[gps_start:segDur:(gps_start + dur - params.segmentDuration)];
    gps2bis= [gps2bis gps2tmp gps2(end)+segDur];
  end
end

% Remove any duplicate GPS times that may occur due to possible
% overlap of .mat files.
gps1 = sort(unique(gps1));
gps2 = sort(unique(gps2));

if noStitching
  gps1bis = sort(unique(gps1bis));
  gps2bis = sort(unique(gps2bis));
end

% Find available coincident data for the two detectors of interest.
overlaps = sort(intersect(gps1,gps2));

if noStitching
  overlapsBis = sort(intersect(gps1bis,gps2bis));
  if length(overlapsBis)==0
    error('No coincident time found between the 2 detectors');
  end
end

% Do same calculation for joblist.
gpslist = [];
gpslistbis=[];
for ii=1:size(joblist,1)
  gps_start = joblist(ii,2); gps_end = joblist(ii,3);
  gpslist = [gpslist gps_start:joblistDur:(gps_end - params.segmentDuration)];
  if noStitching
    gpslisttmp=[gps_start:joblistDur:(gps_end - params.segmentDuration)];
    gpslistbis= [gpslistbis gpslisttmp gpslist(end)+joblistDur];
  end
end
overlaps = sort(intersect(overlaps,gpslist));
overlapsBis = sort(intersect(overlapsBis,gpslistbis));

if ~noStitching

   % Print background study jobfile.
   if overlap>0
     nJobs = ceil(1+(numel(overlaps)-nSegsPerJob)/nSegsReduced); 
   else
     nJobs = ceil(numel(overlaps)/nSegsPerJob);
   end
   fid = fopen([searchPath '/tmp/jobfile_bknd.txt'],'w+');
   for ii=1:nJobs
     if overlap>0
       idx1 = 1+(ii-1)*nSegsReduced;
       idxN = ii*nSegsPerJob-(ii-1)*nSegsOverlap+1;
     else
       idx1 = 1+(ii-1)*nSegsPerJob;
       idxN = ii*nSegsPerJob+1;
     end

     if (ii == nJobs)
       idxN = length(overlaps);
       temp = idxN - idx1;
    
       % If some extra segments left over, skip for now.
       % Not sure of best way to handle this.  Probably OK to skip.
       if (temp < nSegsPerJob)
         fprintf('\tWARNING: skipping last job since it is too short.\n');
         continue;
       end
     end

     startGPS = overlaps(idx1);
     endGPS = overlaps(idxN);

     fprintf(fid,'%i  %.1f  %.1f  %.1f %i\n',ii,startGPS,endGPS,job_time,ceil(ii/(2000/job_time))); %%create groups of about 2000s
   end
   fclose(fid);
   joblist=[];
else
   %% find the gaps within the data
   gaps=find(diff(overlapsBis)>segDur);
   segstart=overlapsBis(1);
   currentJob=1;
   joblist=[];
   if isempty(gaps)
      % No gaps!
      joblist = [currentJob segstart overlapsBis(end) overlapsBis(end)-segstart+segDur];
   else
      %% Creating the joblist (saved later on).
      for gap=1:size(gaps,2)
         line=[currentJob segstart overlapsBis(gaps(gap)) overlapsBis(gaps(gap))-segstart+segDur];
         joblist=[joblist; line];
         currentJob=currentJob+1;
         if gap~=size(gaps,2)
            segstart=overlapsBis(gaps(gap)+1);
         end
      end
   end
end
  
return;
