function make_dag_anteproc(searchPath,pair,paramsFile0,jobsFile,retry,scratchPath)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function make_dag_anteproc()
%
% This function generates the dagfile for the .mat anteproc
% files processing.
%
%Written by: Samuel Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Jobfile used for preprocessing. No overlap, no specified 
%% window duration...
fid=fopen([searchPath '/stamp_allsky_anteproc.dag'],'w+');


job_number=0;
l=load(jobsFile);
l_data=size(l,1);

for PAIR=0:2:2 %%for each ifo in the pair
   paramsFile=[paramsFile0 pair(PAIR+1:PAIR+2) '.txt'];
   for i=1:l_data
      job_number=job_number+1;
      fprintf(fid,'JOB %d stamp_allsky_anteproc.sub\n',job_number);
      fprintf(fid,'RETRY %d %d\n',job_number,retry);
      fprintf(fid,'VARS %d jobnumb="%d" searchPath="%s" paramsFile="%s" jobsFile="%s" jobNumber="%d" scratchPath="%s"', job_number,job_number,searchPath,paramsFile,jobsFile, i,scratchPath);
      fprintf(fid,'\n\n');
   end
end



%fprintf(fid,'\nPARENT 1 CHILD ');
%for k=2:job_number
%   fprintf(fid,' %d ',k);
%end




fclose(fid);
