% BKG analysis
inj 0

% The pair of ifos you choose. Choices are 'H1L1','H1V1' and 'V1L1'
pair H1L1

% Path to the real data cachefiles
%%%%
cachePath cachefiles/

% Absolute path to anteproc matfiles (write default to choose search directory)
matfilesLocation /home/stampas/matfiles/er8

% Anteproc matfiles name
matfilesName STAMP_dt1_df1_nspi17

% Generate a cachefile
genCachefiles true

% Get anteproc matfiles on the nodes
doNodes 0

% Anteproc matfilpath on nodes should end with '/'
nodes_dir matfiles/

% Zebragard search. Set to 0 or 1.
doZebra 0

% Diagnostic plots. Set to 0 or 1.
doDgPlots 0

% Seedless search. Set to 0 or 1.
doSeedless 1

% Seedless all-sky search. Set to 0 or 1.
doSeedlessAllsky 1

% Seedless parameter file
seedlessParams seedless_params.txt

% Seedless search (Lonetrack). Set to 0 or 1.
doLonetrack 0

% Seedless search (Lonetrack post-processing). Set to 0 or 1.
doLonetrackPProc 0

% Seedless search (Lonetrack post-processing FAP). Set to < 1.
lonetrackFAP 0

% Turn on the use of GPUs. Set to 0 or 1
doGPU 0

% Turn on the use of parallel toolbox. Set to 0 or 1
doParallel 0

% Job memory size
jobMemory 2400

% Monte-Carlo data. Set to 0 or 1.
% if set to 1, check that doNodes is set to 0
doMC 0

% PSD Files, for LIGO and Virgo.
DetectorNoiseFileLIGO PSDs/LIGOsrdPSD_40Hz.txt
DetectorNoiseFileVirgo PSDs/VirgoDesign_PSD_only.txt

% FT-map size (Duration, and frequency limits)
window 500
fmin 30
fmax 2000

% Overlap between two consecutive ft-map
overlap 250

% File listing frequencies to notch during analysis
FrequenciesToNotch freqs_ER8H1L1_1s_1Hz.txt

% alternative_sigma option
alternative_sigma 0

% Zero-lag analysis. Set to 0 or 1.
doZeroLag 0	  

% Number of timeshifts (excluding zero-lag)
NTShifts 2

% The NTShifts timeshifts will start at TSOffset
TSOffset 0

% dense DAG option: makes DAG jobs longer but reduce their number
denseDAG 1

% Number of groups analyzed per Condor job
nbGroupsPerJob 5

% Variable windows option: the size of the analysis window depends on
% the presence or not of triggers at the end of the previous window.
% Need denseDAG option!
doVariableWindows 0

%% Limit size of the windows (s)
windowLimit 600

% noStitching option
noStitching 1

% Number of retries for each Condor job
retry 1

% ignore gaps between segments
ignoreGaps 0
