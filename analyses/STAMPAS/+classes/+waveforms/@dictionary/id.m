function ii = id(obj,w)  
% id : return the id of the waveforms
% DESCRIPTION :
%   return the id of all the waveform present in the dictionary or
%   for some specified waveform
% SYNTAX :
%   [id] = id (obj,w)
% 
% INPUT : 
%    obj : the dictionary
%    w [OPTIONAL] : the waveform group or file eq adi or
%                   adi_A_tapered.dat
%
% OUTPUT :
%   id : the id of the waveform
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.2
% DATE : Jun 2016
%
  if nargin == 1
    ii={obj.data.id};
  else
    if sum(strcmp(obj.group,w))
      ii = {obj.data(strcmp({obj.data.group},w)).id};
    elseif sum(strcmp(obj.fileInfo,w))
      ii = {obj.data(strcmp({obj.data.file},w)).id};
    else
      error([w 'not found in the dictionary. Try waveform group input or a ' ...
             'waveform file input'])
    end
  end
end