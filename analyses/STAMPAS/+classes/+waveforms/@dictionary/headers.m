function [h] = headers(obj,w)  
% headers : return the waveform's headers 
% DESCRIPTION :
%   get the header of all the waveform present in the dictionary or
%   for some specific waveform. the header returned are object from
%   the class classes.data.waveform.headers
%
% SYNTAX :
%   [h] = headers (obj,w)
% 
% INPUT : 
%    obj : the dictionary
%    w [OPTIONAL] : waveform name, id, or file
%
% OUTPUT :
%   h : waveforms headers
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jun 2016
%
  if nargin==1
    h=obj.data;
  else
    if sum(strcmp(obj.group,w))
      h = obj.data(strcmp({obj.data.group},w));
    elseif sum(strcmp(obj.fileInfo,w))
      h = obj.data(strcmp({obj.data.file},w));
    elseif sum(strcmp(obj.id,w))
      h = obj.data(strcmp({obj.data.id},w));
    else
      error([w 'not found in the dictionary. Try waveform group input or a ' ...
             'waveform file input'])
    end
  end
end

