classdef header<dynamicprops
% header : waveforms header obj
% DESCRIPTION :
%   class whos manage waveform headers
%   
% SYNTAX :
%   [h] = header (s)
%
% INPUT : 
%   s : structure containing waveform information
%       the minimal structur should be :
%       s -+
%          +- id
%          +- group
%          +- file
%          +- type    
%          +- legend
%
% OUTPUT :
%   h : headers information
%
% PROPERTIES:
%   id
%   group
%   file
%   type    
%   legend
%
% METHODS:
%   h = parse(s) 
%
% AUTHOR : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jun 2016
%
  properties    
    id
    group
    file
    type    
    legend
    energy
  end
  
  methods
    function obj=header(s)
      if nargin>0
        obj.parse(s);
      end
    end    
  end
end

