function parseASCII(obj)  
% parseASCII : parse ascii file to get hx and hp
% DESCRIPTION :
%    parse the waveform information using ascii file
%
% SYNTAX :
%   parseASCII (obj)
%   
% INPUT : 
%   obj : waveform class
% 
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jun 2016
%
  import classes.config
  
  % ---------------------------------------------------------------------------
  %% CHECK if the file exist
  % ---------------------------------------------------------------------------
  %
  if exist(obj.file)
    file = file;
  elseif exist([config.WVF_FOLDER '/' obj.file])
    file = [config.WVF_FOLDER '/' obj.file];
  else
    error(['file : ' obj.file 'not found. try to change file or change the ' ...
           'default FOLDER parameters'])
  end
  
  % ---------------------------------------------------------------------------
  %% READ data from the ascii file
  % ---------------------------------------------------------------------------
  %
  try
    fid=fopen(file);
    l=fgetl(fid);
    while ischar(l)
      if ~isempty(regexp(l,'^%','match','once'))
        l=fgetl(fid);
        continue
      end
      data=fscanf(fid, '%f %f %f\n');
      break
    end
    data=reshape(data,3,ceil(length(data)/3))';    

  catch ME
    error(['problem when read file : ' file ' due to ' ME.message])
  end

  % ---------------------------------------------------------------------------
  %% PARSE waveform using data 
  % ---------------------------------------------------------------------------
  %
  obj.hp       = data(:,2);
  obj.hx       = data(:,3);
  if isempty(obj.deltaT)
    obj.deltaT   = data(2,1)-data(1,1);
  end
  if isempty(obj.duration)
    obj.duration = data(end,1);
  end
  
end
