function parseOnTheFly(obj)  
% parseOnTheFly : parse waveform on the fly
% DESCRIPTION :
%    parse waveform hp & hx properties uning on the fly funtion
%
% SYNTAX :
%   parseOnTheFly (obj)
% 
% INPUT : 
%   obj : waveform class
% 
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jun 2016
%
  import classes.config
  
  % ---------------------------------------------------------------
  %% CHECK file exist
  % ---------------------------------------------------------------
  %
  if exist(obj.file)
    file = obj.file;
  elseif exist([config.WVF_FOLDER '/' obj.file])
    file =[config.WVF_FOLDER '/' obj.file];
  else
    error(['file : ' obj.file 'not found']);
  end

  % ---------------------------------------------------------------
  %% CHECK constructor is define
  % ---------------------------------------------------------------
  %
  try
    obj.constructor;
  catch
    error('missing parameters constructor');
  end

  % ---------------------------------------------------------------
  %% fill params for constructor
  % ---------------------------------------------------------------
  %
  s.pass.stamp              = load(file); 
  if ~isempty(strfind(obj.type,'rmodes'))
    s.pass.stamp.alphasat = s.pass.stamp.alpha;
  end
  s.pass.stamp.start        = 0;
  s.pass.stamp.dataStartGPS = 0;
  s.pass.stamp.dataEndGPS   = obj.duration;
  s.flow                    = -Inf;

  % ---------------------------------------------------------------
  %% eval the constructor
  % ---------------------------------------------------------------
  %
  if obj.deltaT~=0
    fs=1/deltaT;
  else
    fs=config.SAMPLE_RATE;
  end
  data=feval(obj.constructor,s,fs);

  % ---------------------------------------------------------------
  %% PARSE waveform using the constructor
  % ---------------------------------------------------------------
  %
  obj.hp       = data(:,2);
  obj.hx       = data(:,3);
  obj.deltaT   = data(2,1)-data(1,1);
  obj.duration = data(end,1);

end