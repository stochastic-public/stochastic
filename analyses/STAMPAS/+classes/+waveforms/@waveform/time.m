function [t] = time(obj)  
% time :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = time ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%
  t=[linspace(0,obj.duration,length(obj.hp))]';

end

