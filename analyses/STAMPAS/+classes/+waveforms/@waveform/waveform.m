classdef waveform<classes.waveforms.header
% waveform : waveform class
% DESCRIPTION :
%   class representing waveform. subclass of headers class. 
%   the waveform class contain all the header and time series hx &
%   hp
%
% SYNTAX :
%   [wvf] = waveform (h)
%
% INPUT : 
%   h : waveform header
%  
% OUTPUT :
%   wvf : waveform class
%
% PROPERTIES :
%   header properties
%   hx
%   hp
%   deltaT
%   duration
%
% METHODS :
%   parseASCII
%   parseOnTheFly
%
% AUTHOR : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Ju 2016
%
  properties
    hx
    hp
    deltaT
    duration
  end
  
  methods
    
    function obj=waveform(s)
      obj@classes.waveforms.header(s);
      switch s.type
        case 'ascii'
          obj.parseASCII;          
          
        case 'onthefly'
          obj.parseOnTheFly;
          
        otherwise
          error(['type : ' obj.type ' unknow']);
      end
      
    end
  end

end

