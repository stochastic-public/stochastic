classdef (Abstract) config
% config : define default configuration for STAMPAS 
% DESCRIPTION :
%   define some default value & vairable for the STAMPAS package
%   config is only composed of static fucntion. there is no
%   constructor for this class
%
% METHODS:
%   S = STAMPAS()
%   S = WVF_FOLDER()
%
% AUTHOR : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jun 2016
%
  properties (Constant)
    SAMPLE_RATE = 16384
  end

  methods (Static)

    function S=STAMPAS()
      S = fileparts(mfilename('fullpath'));
      S = regexprep(S,'(.*/STAMPAS).*','$1');
    end
    
    function S = WVF_FOLDER()
      S = fileparts(mfilename('fullpath'));
      S = [regexprep(S,'(.*/STAMPAS).*','$1') '/waveforms'];
    end

  end
end

