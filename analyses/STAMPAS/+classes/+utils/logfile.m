classdef (Sealed) logfile < handle
% logfile :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = logfile ()
% 
% INPUT : 
%    
% OUTPUT :
%
% PROPERTIES:
%
% METHODS:
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
  properties(Access = private)
    fid
  end

  methods

    function obj=logfile(in)
      persistent log
      if isempty(log) || ~isvalid(log)
        if ~exist(regexprep(in,'(.*)/.*','$1'));
          error(['cannot create the ' in ' log file']);
        end
        log = obj;
        obj.fid=fopen(in,'w');
        return
      end
      obj = log;
      
      if nargin==1
        obj.write(in);
      end
    end

    function delete(obj)
      if obj.fid
        fclose(obj.fid);
      end
    end
  end

  methods(Access = private)
    function write(obj,msg)
      c=clock;
      str='';
      for s=strsplit(msg);
        if length(str)+length(s)<60
          str=[str ' ' s{:}];
        else
          fprintf(obj.fid,'[%s %2d:%2d:%2.0f] %s\n',date,c(4),c(5),c(6),str);
          str=s{:};
        end
      end
      fprintf(obj.fid,'[%s %2d:%2d:%2.0f] %s\n',date,c(4),c(5),c(6),str);
    end
  end

end

