function [tau] = tau(obj)  
% tau :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = tau ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jan 2017
%
  if ~isprop(obj.format,'tau')
    tau= arrayfun(@(t,r,d) caltau(getdetector('LHO'), getdetector('LLO'),t,[r,d],struct('c',299792458)),obj.GPSstart,obj.ra,obj.dec);
  else
    tau =  obj.data(:,obj.format.tau{1});
  end
end
