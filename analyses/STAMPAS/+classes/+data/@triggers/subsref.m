function [varargout] = subsref(obj,S)  
% subsref :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = subsref ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%

switch S(1).type
  
  case {'.','{}'}
    [varargout{1:nargout}] = subsref@classes.data.dataInterface(obj,S);

  case '()'
    if numel([S.subs])>1&& ~strcmp(S(1).type,'()')
      [varargout{1:nargout}] = subsref@classes.data.dataInterface(obj,S);

    elseif  numel([S.subs])>1 && length(obj)==1
      d=obj.getInstance;
      idx = find(S(1).subs{1}>0);
      d.data=obj.data(S(1).subs{1}(idx),:);
      d.cell=obj.cell(S(1).subs{1}(idx),:);
      d.numElements=sum(S(1).subs{1}>0);

      if isprop(obj,'lonetrack')
        d.lonetrack.ifo1.data = obj.lonetrack.ifo1.data(S(1).subs{1}(idx),:);
        d.lonetrack.ifo1.cell = obj.lonetrack.ifo1.cell(S(1).subs{1}(idx),:);
        d.lonetrack.ifo2.data = obj.lonetrack.ifo2.data(S(1).subs{1}(idx),:);
        d.lonetrack.ifo2.cell = obj.lonetrack.ifo2.cell(S(1).subs{1}(idx),:);        
      end

      try
        [varargout{1:nargout}] = subsref@classes.data.dataInterface(d,S(2:end));
      catch, rethrow(lasterror); end

    elseif  numel([S.subs])>1
      d=obj(S(1).subs{1});
      try
        [varargout{1:nargout}] = builtin('subsref',d,S(2:end));
      catch, rethrow(lasterror); end

    elseif length(obj)~=1
      varargout{1}=obj(S.subs{1});

    else
      varargout{1}=obj.getInstance;
      varargout{1}.data=obj.data(S.subs{1},:);
      varargout{1}.cell=obj.cell(S.subs{1},:);

      if isprop(obj,'lonetrack')
        varargout{1}.lonetrack.ifo1.data = obj.lonetrack.ifo1.data(S(1).subs{1},:);
        varargout{1}.lonetrack.ifo1.cell = obj.lonetrack.ifo1.cell(S(1).subs{1},:);
        varargout{1}.lonetrack.ifo2.data = obj.lonetrack.ifo2.data(S(1).subs{1},:);
        varargout{1}.lonetrack.ifo2.cell = obj.lonetrack.ifo2.cell(S(1).subs{1},:);        
      end

      varargout{1}.numElements=sum(S.subs{1}>0);


    end


end

