function obj = loadobj(s)  
% loadobj :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = loadobj ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

  obj = loadobj@classes.data.dataInterface(s);
  obj.name     = s.name;
  obj.inj_type = s.inj_type;
  obj.fmin     = s.fmin;
  obj.fmax     = s.fmax;
  obj.duration = s.duration;
  obj.alpha    = s.alpha;
  
end

