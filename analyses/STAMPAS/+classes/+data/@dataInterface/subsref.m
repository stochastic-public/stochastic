function [varargout] = subsref(obj,S)  
% subsref :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = subsref ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Fev 2016
%

switch S(1).type
  case '.'
    try
      [varargout{1:nargout}] = builtin('subsref',obj,S);
    catch, rethrow(lasterror); end
    
  case '{}'
    try
      [varargout{1:nargout}] = builtin('subsref',obj,S);
    catch, rethrow(lasterror); end
    
  case '()'
    if numel([S.subs])>1&& ~strcmp(S(1).type,'()')
      try
        [varargout{1:nargout}] = builtin('subsref',obj,S);
      catch, rethrow(lasterror); end

    elseif  numel([S.subs])>1 && length(obj)==1
      d=obj.getInstance;
      idx = find(S(1).subs{1}>0);
      d.data=obj.data(S(1).subs{1}(idx),:);
      d.cell=obj.cell(S(1).subs{1}(idx),:);
      d.numElements=sum(S(1).subs{1}>0);
      try
        [varargout{1:nargout}] = builtin('subsref',d,S(2:end));
      catch, rethrow(lasterror); end

    elseif  numel([S.subs])>1
      d=obj(S(1).subs{1});
      try
        [varargout{1:nargout}] = builtin('subsref',d,S(2:end));
      catch, rethrow(lasterror); end

    elseif length(obj)~=1
      varargout{1}=obj(S.subs{1});

    else
      varargout{1}=obj.getInstance;
      varargout{1}.data=obj.data(S.subs{1},:);
      varargout{1}.cell=obj.cell(S.subs{1},:);

      varargout{1}.numElements=sum(S.subs{1}>0);
    end
    
    return
end


