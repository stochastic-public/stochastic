function [] = removeElements(obj,n)  
% removeElements :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = removeElements (n)
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Dec 2015
%

  if (islogical(n) && numel(n) == obj.numElements) || numel(n)<= ...
        obj.numElements || sum(n)~=0 || ~isempty(n)
    obj.data(n,:) = [];
    obj.cell(n,:) = [];
    obj.numElements=obj.numElements-numel(find(n~=0));
  else
    warning('bad entry, please check your input');
  end
  
end
