function addElements(obj,s,varargin)  
% addElements :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = addElements (n,offset)
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Jan 2016
%
  
  if nargin==2
    h=obj.format;
    obj.data = [obj.data;zeros(numel(s),obj.format.maxIdx('num'))];
    obj.cell = cat(1,obj.cell,cell(numel(s),obj.format.maxIdx('cell')));

  elseif nargin==3
    h=varargin{1};
    
  else
    error('wrong number of input');
  end  
  
  
  for i=fieldnames(h)'
    if ~isfield(s,i{:});[s.(i{:})]=deal([]);end
    if isstruct(h.(i{:}))
      obj.addElements([s.(i{:})],h.(i{:}))
    else
      idx=obj.numElements+1:obj.numElements+numel(s);
      if strcmp(h.(i{:}){2},'cell')
        obj.cell(idx,h.(i{:}){1})={s.(i{:})}';
        
      elseif strcmp(h.(i{:}){2},'num')
        if ~isempty([s.(i{:})])
          obj.data(idx,h.(i{:}){1})=[s.(i{:})]';
        end
      else
        error('wrong format');
      end
    end
  end

  if nargin==2
    obj.numElements = obj.numElements+numel(s);
  end

end
