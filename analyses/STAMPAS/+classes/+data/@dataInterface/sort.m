function sort(obj,field,varargin)  
% sort :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = sort ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

if nargin<3
    mode='ascend';
else
    mode=varargin{1};
end

    idx=obj.format;
    f=strsplit(field,'.');
    
    if ~isprop(obj.format,f{1})
        error('wrong fields use for sort');
    end
    for i=1:numel(f)
        idx=idx.(f{i});
    end

    switch idx{2}
      case 'cell'
        [~,ii]=sort(obj.cell{:,idx{1}},mode);

      case 'num'
        [~,ii]=sort(obj.data(:,idx{1}),mode);
    end

    obj.data=obj.data(ii,:);
    obj.cell=obj.cell(ii,:);

return

