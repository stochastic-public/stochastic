function [v]=getProp(obj,idx)  
% getProp :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = getProp ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
if isstruct(idx)
  for i=fieldnames(idx)'
    v.(i{:})=obj.getProp(idx.(i{:}));
  end
else
  if strcmp(idx{2},'num')
    v=obj.data(:,idx{1});
  elseif strcmp(idx{2},'cell')
    v=obj.cell(:,idx{1});
  end
end

return

