function [out] = split(obj,node)  
% split :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = split ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

  import classes.data.triggers
  import classes.format.formatImpl
  
  field=strsplit(node,'.');

  frmt=getfield(obj.format,field{:});

  format = formatImpl(frmt);
  format.reshape;

  out = triggers(format);
  out.data = zeros(size(obj.data,1),size(out.data,2));
  out.cell = cell(size(obj.cell,1),size(out.cell,2));
  
  cellfun(@(x) fill(out.format.(x),frmt.(x)),fieldnames(frmt));
  
  function fill (n,o)
    if isstruct(n)
      cellfun(@(x) fill(n.(x),o.(x)),fieldnames(o));
      return
    end

    if strcmp(n{2},'cell')
      out.cell(:,n{1})=obj.cell(:,o{1});
    elseif strcmp(n{2},'num')
      out.data(:,n{1})=obj.data(:,o{1});
    end
  end
end
