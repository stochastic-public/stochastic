classdef formatImpl<classes.format.formatInterface&dynamicprops
% formatImpl :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = formatImpl ()
% 
% INPUT : 
%    
% OUTPUT :
%
% PROPERTIES:
%
% METHODS:
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
   methods
     
     function obj=formatImpl(s)
       for i=fieldnames(s)'
         addprop(obj,i{:});
         obj.(i{:})=s.(i{:});
       end
       
     end

   end
end

