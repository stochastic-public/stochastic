% Designed to set up the matlab environment for running the
% STAMP AS postprocessing functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

STAMPAS = regexprep(pwd,'(.*/STAMPAS)/.*','$1');
stamp2  = regexprep(pwd,'(.*)/STAMPAS/.*','$1/stamp2/src/');

addpath(STAMPAS);
addpath(genpath(stamp2));
addpath([STAMPAS '/functions']);
addpath([STAMPAS '/post_processing/']);
addpath([STAMPAS '/post_processing/background_estimation/src']);

[~,h]=system('hostname -d');
h=h(1:end-1);
if (strcmpi(h,'ligo.caltech.edu') | strcmpi(h,'ligo-wa.caltech.edu') | ...
    strcmpi(h,'ligo-la.caltech.edu'))
  addpath('/ligotools/matlab');
elseif strcmpi(h,'atlas.local')
  addpath('/opt/lscsoft/ligotools/matlab');
end

