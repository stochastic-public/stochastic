function [] = plot2html(webFolder,wvfId,injIdx)  
% plot2html :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = plot2html ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

addpath(regexprep(pwd,'(.*/STAMPAS)/.*','$1'))

%%% get the waveform parameters
wvfs=get_waveform_parameters('./');
alphas=wvfs.alphas{wvfId};

%%% get the time of the injections
radec=load(['./tmp/radec.txt']);
jobs=load(['./tmp/joblist_injections.txt']);
injStart=radec(injIdx,wvfId+2);
injStop=radec(injIdx,wvfId+2)+wvfs.duration(wvfId);
ext=injStop>jobs(:,2) & injStart<jobs(:,3);
joblist=jobs(ext,:);

%%% get html doc
doc=html.document();
doc.setStyle('style.css');
doc.setScript('main.js');
body=doc.getElementsByTagName('body');
body=body.item(0);

%%% title
doc.createNode('h1',['Waveform : ' ...
                    wvfs.waveform_names{wvfId} ...
                    '| Injection time : ' num2str(injStart)],...
               'Parent',doc.body);

%%}| init toc
doc.createNode('h2','Toc','Parent',doc.body);
nav = doc.createNode('nav','Parent',doc.body);
ul  = doc.createNode('ul','Parent',nav);

%%% MAIN Loop
for AA=1:length(alphas);
    
    %%% add title 
    doc.createNode('h2',['Alpha of the injection: ' ...
                        num2str(alphas(AA),'%.6f')],...
                      'Attribute',struct('id', num2str(alphas(AA),'%.6f')),...
                      'Parent',doc.body);
    
    %%% Fill the ToC
    li=doc.createNode('li','Parent',ul);
    doc.createNode('a',num2str(alphas(AA),'%.6f'),'Attribute', ...
                   struct('href',['#' num2str(alphas(AA),'%.6f')]),...
                   'Parent',li);
    
    %%% Get the Results data
    if exist(['results/results_merged/results_' ...
              wvfs.waveform_names{wvfId} '_' num2str(alphas(AA),'%0.10f') ...
              '_filtered.mat']) == 2
        filtered=getfield(matfile(['results/results_merged/results_' ...
                            wvfs.waveform_names{wvfId} '_' ...
                            num2str(alphas(AA),'%0.10f') ...
                            '_filtered.mat']),'data');
    else
      filtered=[];
    end
    
    folder=['results/results_' wvfs.waveform_names{wvfId} '_' num2str(alphas(AA),'%.10f') ...
            '/results_' num2str(joblist(1,2)) '.mat'];
    load(folder);
    
    for job=1:size(joblist,1)
        
      if ~isempty(filtered)
        fdata=filtered(filtered.GPSstart==joblist(job,2)); 
      else
        fdata=[];
      end
      if ~isempty(data)
        dataJob=data(data.GPSstart==joblist(job,2));
      else
        dataJob=[];
      end
        
        %%% add Sub title 
        doc.createNode('h3',[' GPS time of the windows: ',...
                            num2str(joblist(job,2))],...
                          'Parent',doc.body);

        %%% add the change plot button
        div=doc.createNode('div','Plots : ','Parent',doc.body);
        doc.createNode('a','Global',...
                       'Attribute',struct('href','#',...
                                          'onclick','changePlot(this);return false;'),...
                       'Parent',div);        
        doc.createNode('a','Zoom',...
                       'Attribute',struct('href','#',...
                                          'onclick','changePlot(this);return false;'),...
                       'Parent',div);
        
        folder=['plots_' num2str(injStart) ...
                '/plots_' num2str(alphas(AA),'%.6f') ...
                '/plots_' num2str(joblist(job,2))];
        
        if exist([webFolder '/figures/' folder]) ~= 2
            system(['mkdir -p ' webFolder '/figures/' folder]);
        end
        system(['cp DiagPlots/plots_' wvfs.waveform_names{wvfId} '/' ...
                folder '/* ' webFolder '/figures/' folder]);
        
        %%% Fill the plot Table
        table=doc.createNode('table','Attribute',struct('class','plots'),'Parent',doc.body);
        tr=doc.createNode('tr','Parent',table);
        doc.createNode('th','Xi snr map','Parent',tr);
        doc.createNode('th','Reconstructed Clusters','Parent',tr);
        doc.createNode('th','Loudest Reconstructed cluster','Parent',tr);
        doc.createNode('th','Sigma map','Parent',tr);
        doc.createNode('th','SNR map','Parent',tr);
        doc.createNode('th','Y map','Parent',tr);

        tr=doc.createNode('tr','Parent',table);
        doc.createNode('img','Attribute',struct('src',['figures/' folder '/Xi_snr_map.png']),'Parent',doc.createNode('td','Parent',tr));
        doc.createNode('img','Attribute',struct('src',['figures/' folder '/all_clusters.png']),'Parent',doc.createNode('td','Parent',tr));
        doc.createNode('img','Attribute',struct('src',['figures/' folder '/large_cluster.png']),'Parent',doc.createNode('td','Parent',tr));
        doc.createNode('img','Attribute',struct('src',['figures/' folder '/sig_map.png']),'Parent',doc.createNode('td','Parent',tr));
        doc.createNode('img','Attribute',struct('src',['figures/' folder '/snr.png']),'Parent',doc.createNode('td','Parent',tr));
        doc.createNode('img','Attribute',struct('src',['figures/' folder '/y_map.png']),'Parent',doc.createNode('td','Parent',tr));
        
        %%% Fill the result Table
        table=doc.createNode('table','Parent',doc.body);
        tr=doc.createNode('tr','Parent',table);
        doc.createNode('th','RA','Parent',tr);
        doc.createNode('th','DEC','Parent',tr);
        doc.createNode('th','tStart','Parent',tr);
        doc.createNode('th','tStop','Parent',tr);
        doc.createNode('th','fmin','Parent',tr);
        doc.createNode('th','fmax','Parent',tr);
        doc.createNode('th','SNR','Parent',tr);
        doc.createNode('th','SNRfrac','Parent',tr);
        
        fidx=[];
        for tt=1:dataJob.numTriggers
            if ~isempty(fdata) & sum(abs(dataJob(tt).SNR.coherent-fdata.SNR.coherent)<0.01)
                tr=doc.createNode('tr','Attribute',struct('class','filtered'),'Parent',table);
            else
                tr=doc.createNode('tr','Parent',table);            
            end
            doc.createNode('td',sprintf('%.2f',dataJob(tt).ra),'Parent',tr);
            doc.createNode('td',sprintf('%.2f',dataJob(tt).dec),'Parent',tr);
            doc.createNode('td',sprintf('%.1f',dataJob(tt).GPSstart),'Parent',tr);
            doc.createNode('td',sprintf('%.1f',dataJob(tt).GPSstop('ifo2')),'Parent',tr);
            doc.createNode('td',sprintf('%.1f',dataJob(tt).fmin),'Parent',tr);
            doc.createNode('td',sprintf('%.1f',dataJob(tt).fmax),'Parent',tr);
            doc.createNode('td',sprintf('%.2f',dataJob(tt).SNR.coherent),'Parent',tr);
            doc.createNode('td',sprintf('%.2f',dataJob(tt).SNRfrac.coherent),'Parent',tr);            
        end
    end
end


doc.write([webFolder '/index.html']);



return

