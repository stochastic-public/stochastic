function [] = write(obj,f)
% write :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = write (f)
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%    
    di=obj.getImplementation();

    % open file f
    os=java.io.FileOutputStream(java.io.File(f));
    
    % create Load-Save Output
    ls=di.createLSOutput();
    ls.setByteStream(os);

    % create Load-Save Serializer
    s=di.createLSSerializer();

    % write 
    s.write(obj.getChildNodes,ls);

    % close file
    os.close;
end



