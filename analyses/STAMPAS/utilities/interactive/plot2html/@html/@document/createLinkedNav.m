function nav=createLinkedNav(obj,data)  
% createLinkedNav :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = createLinkedNav ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%

nav=obj.createElement('nav');
ul=obj.createElement('ul');
for ii=1:numel(data)
    li=obj.createElement('li');
    fields=fieldnames(data(ii).attributes);
    for aa=1:numel(fields)
        li.setAttribute(fields{aa},sprintf('%s',data(ii).attributes.(fields{aa})));
    end
    a=obj.createElement('a');
    a.setAttribute('href', sprintf('%s',data(ii).link));
    a.appendChild(obj.createTextNode(sprintf('%s',data(ii).name)));
    li.appendChild(a);
    ul.appendChild(li);
end
nav.appendChild(ul);

return

