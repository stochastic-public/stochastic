function nav = getNavElement(obj)  
% getNavElement :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = getNavElement ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%

nav=struct('name','','link','','attribute',struct('class',''));

return

