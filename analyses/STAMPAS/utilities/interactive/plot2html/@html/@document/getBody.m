function element = getBody(obj,v,p)  
% body :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = body ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

element=obj.getElementsByTagName('body');
element=element.item(0);

end

