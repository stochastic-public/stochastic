#! /usr/bin/env bash
# make web page from DiagPlots folder
# -----------------------------------------------------------------
# Syntax:
#   ./plot2html/plot2html.sh [user_web_folder] [waveform name/time slide] [gps windows]
# ------------------------------------------------------------------
# Commands:
#   user_web_folder    Web pages will be put in the s5-allsky/Start-* 
#                      _Stop-*_ID-*/DiagPlots/user_web_folder
#                      this folder must exist
# ------------------------------------------------------------------
# Purpose:
#     make table of plot from the DiagPlots folder obtain by 
#     run_clustermap. make table of trigger caracteristic from result
#     folder obtain by run_clustermap. use index.html template and a 
#     style.css file. the result will be put in s5-allsky/Start-*
#    _Stop-*_ID*/DiagPlots/user_web_folder
#
# ------------------------------------------------------------------
# author:                         FREY valentin
# EMail Address:                  valentin.frey@gmail.com
# version:                        1.0
# Comment:                        the plot2html folder need to be in
#                                 your Start-*_Stop-*_ID* folder.
#                                 run this script in this folder 
# ------------------------------------------------------------------

if [ "$#" -ne 4 ]; then
    echo "Problem in plot2html number of argument invalide, please check"
    echo "[1] type of the run [INJ/BKG]"
    echo "[2] waveform name / time slides"
    echo "[3] injection gpsTime/ "
    echo "[4] web folder"
    exit
fi

type=$1
wvf_lag=$2
gpsWindows=$3
webFolder=$4

if [[ -n ${interactive} ]];then
    ff=$interactive
else
    ff='.'
fi

### Check the folder exist
if ! [ -d $webFolder ]; then
    echo "$webFolder doesn't exist"
    exit 1
fi 


### create header of the index.html
cp $ff/plot2html/index.html header.txt || exit $?
if [[ ${type} == 'INJ' ]];then 
    sed "s/<title>/<title> Waveform : ${wvf_lag} Study/" header.txt > tmpH.txt
    sed "s/<h1>/<h1> Waveform : ${wvf_lag} | Injection GPS time : ${gpsWindows} /" tmpH.txt > header.txt
elif [[ ${type} == 'BKG' ]];then
    sed "s/<title>/<title> Lag : ${wvf_lag} Study/" header.txt > tmpH.txt
    sed "s/<h1>/<h1> LAG : ${wvf_lag} analysis GPS : ${gpsWindows} /" tmpH.txt > header.txt
fi
rm tmpH.txt

### create the Toc of the web page
echo -e "<h2> ToC </h2>" > toc.txt|| exit $? 
echo -e "\t<nav>\n\t\t<ul>" >> toc.txt || exit $? 


### make the figure folder in the web repository
if ! [ -d $webFolder/figures ]; then
    mkdir $webFolder/figures
fi


list=(`cd DiagPlots/; ls *${wvf_lag}*/*${gpsWindows}`) || exit $?
for file in ${list[@]};do
    ### make figure/$file folder in the web directory
    if ! [ -d $webFolder/figures/$file ]; then
	mkdir $webFolder/figures/$file
    fi
    
    ### for inj get the list of alphas 
    ### for bkg get the list of jobNumber
    alphaJob=`echo ${file} | sed -e 's/.*_//'` || exit $?
    sub_list=(`cd DiagPlots/*${wvf_lag}*/*${gpsWindows}*/$file; ls`) || exit $?
    if [[ "$type" == "INJ" ]]; then
	echo -e "<h2 id='${alphaJob}'> Alpha of the injection: $alphaJob</h2>" >> tmp.txt || exit $?
	
        ### Toc
	echo -e "\t\t\t<li><a href=\"\#${alphaJob}\">${alphaJob}</a></li>" >> toc.txt || exit $?
    fi

    for sub_file in ${sub_list[@]}; do
	echo $sub_file
	### make figure/$file/$sub_file folder in the web directory
	if ! [ -d $webFolder/figures/$file/$sub_file ]; then
	    mkdir $webFolder/figures/$file/$sub_file || exit $?
	fi

	### for both inj & bkg get the GSP time 
	GPStime=`echo ${sub_file} | sed -e 's/.*_//'`;
	plot_list=(`cd DiagPlots/*${wvf_lag}*/*${gpsWindows}*/$file/$sub_file/;ls *.png`);
	
	echo -e "\t <h3 id='${GPStime}'> beginning GPS time of the window: $GPStime</h3>">> tmp.txt
	echo -e "<table class='plots'>" >> tmp.txt

        ### Toc
	if [[ ${type} == 'BKG' ]];then
	    echo -e "\t\t\t<li><a href="\#${GPStime}">${GPStime}</a></li>" >> toc.txt || exit $?
	fi
	
	### Legend of the plot table
	echo -e "\t <tr>" >> tmp.txt
	for plot in ${plot_list[@]}; do
	    legend=`echo ${plot} | sed -e 's/.png//'`;
	    echo -e "\t\t <td>$legend</td>" >> tmp.txt
	done    
	echo -e "\t </tr>\n" >> tmp.txt
	
	### Table of plot
	echo -e "\t <tr>" >> tmp.txt
	for plot in ${plot_list[@]}; do
	    
	    # Copy plot into the Web folder
	    cp DiagPlots/*${wvf_lag}*/*${gpsWindows}*/$file/$sub_file/$plot $webFolder/figures/$file/$sub_file/ || exit $?
	    path=figures/$file/$sub_file 
	    
	    echo -e "\t\t <td class='plots'><a href='$path/$plot'><img src='$path/$plot'/></a></td>" >> tmp.txt
        done
	echo -e "\t</tr>\n" >> tmp.txt
	echo -e "</table>" >> tmp.txt	
    done
done
echo "</html>">>tmp.txt

### end toc
echo -e "\t\t</ul>\n\t</nav>" >> toc.txt

### cat heater toc & tmp to create the index.html
cat header.txt toc.txt tmp.txt > ./index.html || exit $?
cp $ff/plot2html/style.css $webFolder/style.css || exit $?
rm tmp.txt || exit $?
rm toc.txt || exit $?
rm header.txt || exit $?