Perl scripts for coyping STAMP pre-processed files to the node
filesystems at CIT.

Procedure:
1. Modify parameters in spray_data.pl and run it.  This will
   copy the data to the node file systems.
2. Modify parameters in check_nodes_data.pl and run it.  This will
   compare the file sizes of the data in the original location and
   on the nodes. This is a simple check to make sure the copying
   didn't encounter any problems.

##############################################
Tanner Prestegard (prestegard@physics.umn.edu)
July 30, 2015
