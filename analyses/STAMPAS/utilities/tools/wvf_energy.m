function [E] = wvf_energy(wvf_name,distance)  
% wvf_energy : compute the energy of a giver waveform
% DESCRIPTION :
%   energy is define as following
%   E = r^2 1/4 c^3/G <dh+^2 dhx^2>
%   
%   dh+ : dh+ / dt
%   dhx : dhx / dt
%
% SYNTAX :
%   [E] = wvf_energy (wvfId, distance)
% 
% INPUT : 
%    wvfId : the waveform ID
%    distance : source distance [pc]
%
% OUTPUT :
%    E : energy
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jul 2016
%
  import classes.waveforms.dictionary

  msun   = 1.988e30;   % kg
  c      = 299792458;  % m s^-1
  G      = 6.67e-11;   %
  msunc2 = msun*c^2;   % kg m^2 s^-2
  pc     = 3.08e16;    % m
  
  dic = dictionary;
  try
    wvf = dic.waveforms(wvf_name);
  catch M
    M.message
    error('waveform not found')
  end
  
  
  if nargin==1
    r = wvf.distance*pc;
  else
    r = distance*pc;
  end

  dhp = (diff(wvf.hp)/wvf.deltaT);
  dhx = (diff(wvf.hx)/wvf.deltaT);

  E=r^2*c^3/G*1/4*sum((9/16*dhp.^2+1/2*dhx.^2)*wvf.deltaT);
  fprintf('[%s] ENERGY : %g\n',wvf_name,E/msunc2);

end

