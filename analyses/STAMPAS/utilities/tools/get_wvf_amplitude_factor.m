function wvf_factor=get_wvf_amplitude_factor(nb_points,a,b,norm_method,ampl_norm)

% This function generates nb_points values between a and b that are
% logarithmically spaced. Once generated, just copy wvf_factor in
% the waveforms.txt file.
%
% INPUTS nb_points: number of points
%        a: lower bound value
%        b: upper bound value
%        norm_method: 'hrss' or 'distance'
%        ampl_norm: nominal value of the original waveform (ex:
%        hrss=1e-20 / distance=1 Mpc)
% OUTPUT wvf_factor: factors with which the orginal waveform will
%        be multiply according: h(t)=h_orig(t)*sqrt(wvf_factor)
%
% AUTHOR: Marie Anne Bizouard (mabizoua@lal.in2p3.fr)

log_min=log10(a);
log_max=log10(b);
  
A=logspace(log_min,log_max,nb_points);
A=A./ampl_norm;
    
if strcmp(norm_method,'distance')
  wvf_factor=flipud((1./(A.*A))');
else
  wvf_factor=(A.*A)';
end

display(['Factors:' num2str(sprintf('%.2g ',wvf_factor))])
if strcmp(norm_method,'distance')
  display(['Distances:' num2str(sprintf('%.2f ',A*ampl_norm))])
else
  display('hrss:')
  display(num2str(sprintf('%.2g ',A*ampl_norm)))
end