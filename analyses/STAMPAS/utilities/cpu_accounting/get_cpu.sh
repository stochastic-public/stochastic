#! /usr/bin/env bash

usage(){
    echo 'Arg 1: DAG path [ex: BKG/Start-812345678_Stop-912345678_ID1]'
    echo 'Arg 2: type [inj or bkg or ante]'
    exit
}

# main
if [[ $# -lt 2 ]]; then
    usage
fi

folder='../../'$1
type=$2

#folder='/home/mabizoua/s6-allsky/STAMPAS/INJ/Start-931589668_Stop-971614700_ID-6/'

total_sec=0
total_sec_norm=0
for i in {1..100000} ; do
    if [ $(( $i % 1000 )) == 0 ]; then
	echo 'looping ... ' $i '/100000'
    fi
   if [ ! -e ${folder}/logs/err_allsky_${type}.${i} ]; then
	echo 'File index ' $i ' does not exist.'
	break
    fi
    cpu=`cat ${folder}/logs/err_allsky_${type}.${i} | grep cpu | awk '{print $6}'`
    node=`cat ${folder}/logs/out_allsky_${type}.${i} | grep model | cut -d ":" -f 2 | cut -d " " -f 5`
    if [ ${#node} -eq 0 ]; then
	echo 'No node type found in '${folder}/logs/out_allsky_${type}.${i}
	echo 'Stop. Please check the job log file'
	exit 1
    fi
    factor=`grep ${node} cores.txt | awk '{print $NF}'`
    if [ ${#factor} -eq 0 ]; then
	echo 'Node ' $node ' of out_allsky_'${type}'.'${i}' not found in the cores.txt file.'
	echo 'Stop. Please check the file'
	exit 1
    fi
#    echo ${node} " " $factor
    if [ ${#cpu} -gt 0 ]; then
	mn_nb=`echo $cpu | cut -d "m" -f 1 | bc`
	sec_nb=`echo $cpu | cut -d "m" -f 2 | cut -d "s" -f 1 | cut -d "." -f 1 | bc`
	total_sec=$(( ${total_sec} + ${mn_nb}*60 ))
	total_sec=$(( ${total_sec} + ${sec_nb} ))
	total_sec_norm=`echo ${total_sec_norm} " " ${mn_nb} " " ${sec_nb} " " ${factor} | awk '{printf ("%10.1f\n",$1+($2*60.0+$3)*$4)}'`
    fi
done

#echo ${total_sec}
#echo ${total_sec_norm}

total_hr=`echo ${total_sec} | awk '{printf("%10.1f\n",$1/3600)}'`
total_hr_norm=`echo ${total_sec_norm} | awk '{printf("%10.1f\n",$1/3600)}'`

if [ $type == 'bkg' ] ; then
    livetime=`cat ${folder}/tmp/TTime.txt | bc` 

    SU_1000s_100TS=`echo $total_hr " " $livetime | awk '{print $1/$2*1000*100}'`
    SU_1000s_100TS_norm=`echo $total_hr_norm " " $livetime | awk '{print $1/$2*1000*100}'`
    
    echo 'Ave. CPU hour for 1000s and 100 TS: ' ${SU_1000s_100TS}
    echo 'Normalized SU for 1000s and 100 TS: ' ${SU_1000s_100TS_norm}
elif [ $type == 'ante' ]; then
    livetime=`cat  ${folder}/tmp/joblist.txt | awk '{x=x+$4}END{print x/3600}'`
    echo 'Livetime (hours): ' ${livetime}
    echo 'CPU hour for preprocessing: ' ${total_hr}
    echo 'Normalized SU for preprocessing: ' ${total_hr_norm}
elif [ $ type == 'inj' ]; then
    wvf_nb=`grep -v "%%" ${folder}/waveforms.txt | wc -l `
    alphaxwvf_nb=`grep -v "%%" ${folder}/waveforms.txt | wc -w `
    alpha_nb=`echo ${alphaxwvf_nb} " " ${wvf_nb} | awk '{printf("%d\n", $1/$2-1)}'`

    inj_nb=`wc -l ${folder}/tmp/radec.txt | cut -d " " -f 1`
    echo $alpha_nb " " $wvf_nb " " $inj_nb

    SU=`echo $total_hr " " $alpha_nb " " $wvf_nb " " $inj_nb | awk '{print $1/($2*$3*$4)}'`
    SU_norm=`echo $total_hr_norm " " $alpha_nb " " $wvf_nb " " $inj_nb | awk '{print $1/($2*$3*$4)}'`

    echo 'Ave. CPU hour / wvf / amplitude factor / injection: ' ${SU}
    echo 'Normalized SU / wvf / amplitude factor / injection: ' ${SU_norm}
fi
