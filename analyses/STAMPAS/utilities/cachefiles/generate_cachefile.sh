#/usr/bin/env bash

# Generate cache file
# Options
usage(){
    echo 'Usage ./generate_cachefile.sh 1116700672 1118331000'
    echo 'Arg 1: GPS_START'
    echo 'Arg 2: GPS_END'

    exit
}

# main
[[ $# -ne 2 ]] && usage

GPS_START=$1
GPS_END=$2


IFOS=('H1' 'L1')
NB=${#IFOS[@]}

if [ $GPS_END -lt 999999999 ]; then
    CHANNEL_SUFFIX="LDAS_C02_L2"
else
    CHANNEL_SUFFIX="HOFT_C00"
fi

if [ ! -d cache ]; then
    mkdir cache
fi

for i in `seq 0 $(( ${NB} -1 ))` ;  do
    obs=`echo ${IFOS[$i]} | cut -b -1`
    
    gw_data_find --observatory ${obs} --gps-start-time ${GPS_START} --gps-end-time ${GPS_END} --type ${IFOS[$i]}_${CHANNEL_SUFFIX} -u file > cache/${IFOS[$i]}.cache
    python run_cache.py --ifo ${IFOS[$i]}
    for j in `seq $(( $i + 1 )) $(( $NB-1 ))` ; do
#	echo ${IFOS[$i]} " " ${IFOS[$j]} " " $NB
	matlab -nodisplay -r "masterCache ${IFOS[$i]} ${IFOS[$j]}"
    done
done


### CHECK ###
### check if the last frame gps times is not too far from GPS_STOP
lastGpsTime0=`tail -n1 cache/gpsTimes$(echo ${IFOS[0]} | cut -b1 ).$(echo ${IFOS[0]} | cut -b2 ).txt`
lastGpsTime1=`tail -n1 cache/gpsTimes$(echo ${IFOS[1]} | cut -b1 ).$(echo ${IFOS[1]} | cut -b2 ).txt`

if [[ ${lastGpsTime0}+1000 -lt ${GPS_END} ]];then
    echo -e "\n\e[0;33mWarning data from ifo "${IFOS[0]}" stop at "${lastGpsTime0} "("`tconvert ${lastGpsTime0}`")\e[0m"
    echo "Check data availability at https://segments.ligo.org/dqsegdb_web/"
fi

if [[ ${lastGpsTime1}+1000 -lt ${GPS_END} ]];then
    echo -e "\n\e[0;33mWarning data from ifo "${IFOS[1]}" stop at "${lastGpsTime1} "("`tconvert ${lastGpsTime1}`")\e[0m"
    echo "Check data availability at https://segments.ligo.org/dqsegdb_web/"
fi

if [[ ${lastGpsTime0} -ne ${lastGpsTime1} ]];then
    echo -e "\n\e[0;33mWarning data from ifo "${IFOS[0]}" stop at "${lastGpsTime0} "("`tconvert ${lastGpsTime0}`")\e[0m"
    echo -e "\e[0;33mWarning data from ifo "${IFOS[1]}" stop at "${lastGpsTime1} "("`tconvert ${lastGpsTime1}`")\e[0m"
fi

### display the frames times available
time0=`cat cache/frameFiles$(echo ${IFOS[0]} | cut -b1 ).$(echo ${IFOS[0]} | cut -b2 ).txt | sed 's/.*-//' | sed 's/.gwf//' | awk 'BEGIN{sum=0}{sum+=$0}END{print sum}'`
time1=`cat cache/frameFiles$(echo ${IFOS[1]} | cut -b1 ).$(echo ${IFOS[1]} | cut -b2 ).txt | sed 's/.*-//' | sed 's/.gwf//' | awk 'BEGIN{sum=0}{sum+=$0}END{print sum}'`
ratio0=`echo "scale=0; 100*${time0}/(${GPS_END}-${GPS_START})" | bc`
ratio1=`echo "scale=0; 100*${time1}/(${GPS_END}-${GPS_START})" | bc`

echo -e "\nframes files coverage for "${IFOS[0]} ${time0} "s [" ${ratio0} "% ]"
echo "frames files coverage for "${IFOS[1]} ${time1} "s [" ${ratio1} "% ]"

