segexpr "intersection(stoch/science/H1:Science.txt, not(stoch_H1.veto))" > tmp
sort tmp > H1_science_after_cat1.txt
segexpr "intersection(stoch/science/L1:Science.txt, not(stoch_L1.veto))" > tmp
sort tmp > L1_science_after_cat1.txt

echo 'Coincidence SCIENCE H1L1 ...'
# coincidence SCIENCE H1 L1
segexpr "intersection(stoch/science/H1:Science.txt, stoch/science/L1:Science.txt)" > tmp
segexpr "intersection(tmp, boundaries.txt)" > H1L1.txt

echo 'Coincidence SCIENCE H1L1 after cat1 ...'
# coincidence H1 L1 after cat1
segexpr "intersection(H1_science_after_cat1.txt, L1_science_after_cat1.txt)" > tmp
segexpr "intersection(tmp, boundaries.txt)" > H1L1_aftercat1.txt

echo 'Live time computation ...'
echo ' ' 
# livetime
cat stoch_H1.veto | awk 'BEGIN{x=0}{x=x+$2-$1}END{print "H1 veto: " x}'
cat stoch_L1.veto | awk 'BEGIN{x=0}{x=x+$2-$1}END{print "L1 veto: " x}'

cat H1L1.txt | awk '{if ($2-$1>500) x=x+$2-$1}END{print x " s (" x/86400 " days)"}'
cat H1L1_aftercat1.txt | awk '{if ($2-$1>500) x=x+$2-$1}END{print x " s (" x/86400 " days)"}'

if [ -e tmp ]; then
    rm tmp
fi

