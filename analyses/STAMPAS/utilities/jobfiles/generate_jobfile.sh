#! /usr/bin/env bash

# Options
usage(){
    echo 'Usage ./generate_jobfile.sh ER7 1116700672 1118331000 C00 NO'
    echo 'Usage ./generate_jobfile.sh O1 1126051217 1137254417 C02 NO'
    echo 'Usage ./generate_jobfile.sh O2 1164067217 1187733618 C02 NO'
    echo 'Usage ./generate_jobfile.sh O3 1235433618 9999999999 C00 NO'
    echo 'Arg 1: RUN'
    echo 'Arg 2: GPS_START'
    echo 'Arg 3: GPS_END'
    echo 'Arg 4: CALIBRATION (C00, C01, C02, ...)'
    echo 'Arg 5: include BURST HW injections [YES/NO]'
    exit
}


# main
[[ $# -ne 5 ]] && usage

RUN=$1
GPS_START=$2
GPS_END=$3
CAL=$4
HWINJ=$5

IFOS=('H1' 'L1' 'V1')
NB=${#IFOS[@]}

case $RUN in
    'O1')
	for i in `seq 0 $(( ${NB} -1 ))` ;  do
	    #IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":DMT-ANALYSIS_READY"    # ONLINE
	    IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":DCS-ANALYSIS_READY_"${CAL}     # C01
	done
	;;
    'O2')
	for i in `seq 0 $(( ${NB} -1 ))` ;  do
#	    IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":GDS-CALIB_STRAIN"     # C00
#	    IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":DMT-ANALYSIS_READY:1"      # C00
#	    IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":DCS-ANALYSIS_READY_C02"     # C02
	    IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":DCH-CLEAN_SCIENCE_C02"     # C02 cleaned
	done
	;;
    'O3')
	for i in `seq 0 $(( ${NB} -1 ))` ;  do
#	    IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":DMT-GRD_ISC_LOCK_NOMINAL"
#	    IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":GDS-CALIB_STRAIN"     # C00
#	    IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":DMT-ANALYSIS_READY:1"      # C00
	    IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":DCS-ANALYSIS_READY_C01:1"  # C01
#	    IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":DCS-ANALYSIS_READY_C02"    # C02
#	    IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":DCH-CLEAN_SCIENCE_C02"     # C02 cleaned
	    if [ ${IFOS[$i]} == "V1" ]; then
		IFOS_SCIENCE_FLAG[i]=${IFOS[$i]}":ITF_SCIENCE:2"  # C01 
	    fi
	    echo $i ${IFOS[$i]} ${IFOS_SCIENCE_FLAG[i]}	    
	done
	;;
esac



# Get the segments
#export X509_USER_CERT=/home/${USER}/.globus/detcharcert.pem
#export X509_USER_KEY=/home/${USER}/.globus/detcharkey.pem

if [ ! -d jobfiles ]; then
    mkdir jobfiles
else
    rm jobfiles/*
fi

cd jobfiles

echo ${GPS_START} " " ${GPS_END} > boundaries.txt

for i in `seq 0 $(( ${NB}-1 ))` ;  do
    ligolw_segment_query_dqsegdb --segment-url=https://segments.ligo.org --query-segments --include-segments ${IFOS_SCIENCE_FLAG[$i]} --gps-start-time ${GPS_START} --gps-end-time ${GPS_END} | ligolw_print -t segment:table -c start_time -c end_time -d " " > ${IFOS[$i]}-SCIENCE.txt
    echo "1132013797  1132090332" > veto_20nov.txt
    segexpr "intersection(${IFOS[$i]}-SCIENCE.txt,not(veto_20nov.txt))" | sort > tmp.txt
    mv tmp.txt ${IFOS[$i]}-SCIENCE.txt
done


# Get the CAT1 segments
if [ ${RUN} == "ER7" ]; then
    DEFINER_FILE_BURST="../H1L1-HOFT_C00_ER8A_BURST.xml"
elif [ ${RUN} == "ER8" ]; then
    ligolw_add ../H1L1-HOFT_C00_ER8A_BURST.xml ../H1L1-HOFT_C00_ER8B_BURST.xml -o ../H1L1-HOFT_C00_ER8AB_BURST.xml
    DEFINER_FILE_BURST="../H1L1-HOFT_C00_ER8AB_BURST.xml"
elif [ ${RUN} == "O1" ]; then
    DEFINER_FILE_BURST="../H1L1-HOFT_"${CAL}"_O1_BURST.xml"
elif [ ${RUN} == "O2" ]; then
    DEFINER_FILE_BURST="../veto-definitions/burst/O2/H1L1-HOFT_"${CAL}"_O2_BURST.xml"
elif [ ${RUN} == "O3" ]; then
    DEFINER_FILE_BURST="../veto-definitions/burst/O3/H1L1V1-HOFT_"${CAL}"_V1ONLINE_O3_BURST.xml"
else
    echo 'Only ER7, ER8, O1 and O2 and O3 definer file defined. Please update the script.'
    exit 1
fi

echo 'Definer file: ' $DEFINER_FILE_BURST

ligolw_segments_from_cats_dqsegdb -s ${GPS_START} -e ${GPS_END} -t https://segments.ligo.org -p -v ${DEFINER_FILE_BURST}

# Substract CAT1 & CAT4 (burst detchar injections) to science segments
for i in `seq 0 $(( ${NB}-1 ))` ;  do
    ligolw_print ${IFOS[$i]}-VETOTIME_CAT1-${GPS_START}-*.xml -t segment -c start_time -c end_time -d " " | sort > ${IFOS[$i]}-VETOTIME_CAT1.txt
    ligolw_print ${IFOS[$i]}-VETOTIME_CAT4-${GPS_START}-*.xml -t segment -c start_time -c end_time -d " " | sort > ${IFOS[$i]}-VETOTIME_CAT4.txt
    if [ -e ../${IFOS[$i]}-CAT0.txt ]; then
	segexpr "intersection(../${IFOS[$i]}-CAT0.txt, boundaries.txt)" | sort > tmp
	mv tmp ${IFOS[$i]}-CAT0.txt
	segexpr "union(${IFOS[$i]}-VETOTIME_CAT1.txt,${IFOS[$i]}-CAT0.txt)" | sort > tmp
	mv tmp ${IFOS[$i]}-VETOTIME_CAT1.txt
    fi
    segexpr "union(${IFOS[$i]}-VETOTIME_CAT1.txt,${IFOS[$i]}-VETOTIME_CAT4.txt)" | sort > ${IFOS[$i]}-VETOTIME_CAT14.txt
    segexpr "intersection(${IFOS[$i]}-SCIENCE.txt,not(${IFOS[$i]}-VETOTIME_CAT1.txt))" | sort > ${IFOS[$i]}-SCIENCE_aftercat1.txt
    segexpr "intersection(${IFOS[$i]}-SCIENCE.txt,not(${IFOS[$i]}-VETOTIME_CAT14.txt))" | sort > ${IFOS[$i]}-SCIENCE_aftercat14.txt
    a=`segsum ${IFOS[$i]}-VETOTIME_CAT1.txt`
    e=`segsum ${IFOS[$i]}-VETOTIME_CAT4.txt`
    b=`segsum ${IFOS[$i]}-SCIENCE.txt`
    c=`segsum ${IFOS[$i]}-SCIENCE_aftercat1.txt`
    d=`segsum ${IFOS[$i]}-SCIENCE_aftercat14.txt`
    cat1_dt=`echo "scale=3;($b - $c)/$b*100" | bc` 
    cat14_dt=`echo "scale=3;($b - $d)/$b*100" | bc` 
    echo ${IFOS[$i]} ' cat1 segments duration' $a ' s'
    echo ${IFOS[$i]} ' cat4 segments duration' $e ' s'
    echo ${IFOS[$i]} ' science segments duration' $b ' s'
    echo ${IFOS[$i]} ' science segments after cat1 duration' $c ' s (dt: ' $cat1_dt '%)'
    echo ${IFOS[$i]} ' science segments after cat14 duration' $d ' s (dt: ' $cat14_dt '%)'
done

echo ' '
# Get CAT2 veto files
for i in `seq 0 $(( ${NB}-1 ))` ;  do
    ligolw_print ${IFOS[$i]}-VETOTIME_CAT2-${GPS_START}-*.xml -t segment -c start_time -c end_time -d " " > ${IFOS[$i]}-VETOTIME_CAT2.txt
    segexpr "intersection(${IFOS[$i]}-SCIENCE.txt,${IFOS[$i]}-VETOTIME_CAT2.txt)" | sort > ${IFOS[$i]}-VETOTIME_CAT2_afterSCIENCE.txt
    a=`segsum ${IFOS[$i]}-VETOTIME_CAT2_afterSCIENCE.txt`
    b=`segsum ${IFOS[$i]}-SCIENCE.txt`
    cat2_dt=`echo "scale=3;$a/$b*100" | bc` 
    echo ${IFOS[$i]} ' cat2 segments duration' $a ' s'
    echo ${IFOS[$i]} ' science segments duration' $b ' s'
    echo ${IFOS[$i]} ' cat2 deadtime:' $a ' s (dt: ' $cat2_dt '%)'

    # Merge cat1&cat2 to apply newly defined cat1 flafs.
    segexpr "intersection(${IFOS[$i]}-SCIENCE.txt,${IFOS[$i]}-VETOTIME_CAT1.txt)" | sort > ${IFOS[$i]}-VETOTIME_CAT1_afterSCIENCE.txt
    segexpr "union(${IFOS[$i]}-VETOTIME_CAT1_afterSCIENCE.txt, ${IFOS[$i]}-VETOTIME_CAT2_afterSCIENCE.txt)" | sort > ${IFOS[$i]}-VETOTIME_CAT12_afterSCIENCE.txt

done

echo ' '
# Get all pairs' intersection
for i in `seq 0 $(( $NB-1 ))` ; do
    for j in `seq $(( $i + 1 )) $(( $NB-1 ))` ; do
	echo ${IFOS[$i]} ${IFOS[$j]}
	if [ ${HWINJ} == "YES" ]; then
	    echo 'Transient hardware injections are included in segments'
	    segexpr "intersection(${IFOS[$i]}-SCIENCE_aftercat1.txt,${IFOS[$j]}-SCIENCE_aftercat1.txt)" > ${IFOS[$i]}${IFOS[$j]}.txt
	else
	    echo 'Transient hardware injections are excluded in segments'
	    segexpr "intersection(${IFOS[$i]}-SCIENCE_aftercat14.txt,${IFOS[$j]}-SCIENCE_aftercat14.txt)" > ${IFOS[$i]}${IFOS[$j]}.txt
	fi
	segexpr "intersection(${IFOS[$i]}-SCIENCE.txt,${IFOS[$j]}-SCIENCE.txt)" > ${IFOS[$i]}${IFOS[$j]}_beforecat1.txt
	cat ${IFOS[$i]}${IFOS[$j]}.txt | awk '{if ($2-$1 > 200) printf("1 %d %d %d\n", $1, $2, $2-$1)}' > jobs${IFOS[$i]}${IFOS[$j]}.txt
	a=`segsum ${IFOS[$i]}${IFOS[$j]}.txt`
	b=`segsum ${IFOS[$i]}${IFOS[$j]}_beforecat1.txt`
	cat14_dt=`echo "scale=3;($b - $a)/$b*100" | bc`
	echo ${IFOS[$i]} ${IFOS[$j]} ' science segments duration' $b ' s'
	if [ ${HWINJ} == "YES" ]; then
	    echo ${IFOS[$i]} ${IFOS[$j]} ' science segments after cat1 duration' $a ' s (dt: ' $cat14_dt '%)'
	else
	    echo ${IFOS[$i]} ${IFOS[$j]} ' science segments after cat1&cat4 duration' $a ' s (dt: ' $cat14_dt '%)'
	fi
	rm ${IFOS[$i]}${IFOS[$j]}.txt
	rm ${IFOS[$i]}${IFOS[$j]}_beforecat1.txt
    done
done

#rm *VETOTIME*

#rm *SCIENCE*
