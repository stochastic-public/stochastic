segexpr 'union( H1-VETOTIME_CAT1_A.txt,  H1-VETOTIME_CAT4_A.txt)' | sort > H1-VETOTIME_A.txt
segexpr 'union( H1-VETOTIME_CAT1_B.txt,  H1-VETOTIME_CAT4_B.txt)' | sort > H1-VETOTIME_B.txt
segexpr 'union( H1-VETOTIME_CAT1_C.txt,  H1-VETOTIME_CAT4_C.txt)' | sort > H1-VETOTIME_C.txt
segexpr 'union( H1-VETOTIME_CAT1_D.txt,  H1-VETOTIME_CAT4_D.txt)' | sort > H1-VETOTIME_D.txt

segexpr 'union( L1-VETOTIME_CAT1_A.txt,  L1-VETOTIME_CAT4_A.txt)' | sort > L1-VETOTIME_A.txt
segexpr 'union( L1-VETOTIME_CAT1_B.txt,  L1-VETOTIME_CAT4_B.txt)' | sort > L1-VETOTIME_B.txt
segexpr 'union( L1-VETOTIME_CAT1_C.txt,  L1-VETOTIME_CAT4_C.txt)' | sort > L1-VETOTIME_C.txt
segexpr 'union( L1-VETOTIME_CAT1_D.txt,  L1-VETOTIME_CAT4_D.txt)' | sort > L1-VETOTIME_D.txt



segexpr 'intersection(H1-SCIENCE_A.txt,not(H1-VETOTIME_A.txt))' | sort > H1-SCIENCE_A_afterveto.txt
segexpr 'intersection(H1-SCIENCE_B.txt,not(H1-VETOTIME_B.txt))' | sort > H1-SCIENCE_B_afterveto.txt
segexpr 'intersection(H1-SCIENCE_C.txt,not(H1-VETOTIME_C.txt))' | sort > H1-SCIENCE_C_afterveto.txt
segexpr 'intersection(H1-SCIENCE_D.txt,not(H1-VETOTIME_D.txt))' | sort > H1-SCIENCE_D_afterveto.txt

segexpr 'intersection(L1-SCIENCE_A.txt,not(L1-VETOTIME_A.txt))' | sort > L1-SCIENCE_A_afterveto.txt
segexpr 'intersection(L1-SCIENCE_B.txt,not(L1-VETOTIME_B.txt))' | sort > L1-SCIENCE_B_afterveto.txt
segexpr 'intersection(L1-SCIENCE_C.txt,not(L1-VETOTIME_C.txt))' | sort > L1-SCIENCE_C_afterveto.txt
segexpr 'intersection(L1-SCIENCE_D.txt,not(L1-VETOTIME_D.txt))' | sort > L1-SCIENCE_D_afterveto.txt


segexpr 'intersection(H1-SCIENCE_A_afterveto.txt,L1-SCIENCE_A_afterveto.txt)' > H1L1_A_ini.txt
segexpr 'intersection(H1-SCIENCE_B_afterveto.txt,L1-SCIENCE_B_afterveto.txt)' > H1L1_B_ini.txt
segexpr 'intersection(H1-SCIENCE_C_afterveto.txt,L1-SCIENCE_C_afterveto.txt)' > H1L1_C_ini.txt
segexpr 'intersection(H1-SCIENCE_D_afterveto.txt,L1-SCIENCE_D_afterveto.txt)' > H1L1_D_ini.txt

segexpr 'intersection(H1L1_A_ini.txt,not(../ByHand_vetoes.txt))' | awk '{if ($2-$1>200) print $1,$2,$2-$1}' > H1L1_A.txt
segexpr 'intersection(H1L1_B_ini.txt,not(../ByHand_vetoes.txt))' | awk '{if ($2-$1>200) print $1,$2,$2-$1}' > H1L1_B.txt
segexpr 'intersection(H1L1_C_ini.txt,not(../ByHand_vetoes.txt))' | awk '{if ($2-$1>200) print $1,$2,$2-$1}' > H1L1_C.txt
segexpr 'intersection(H1L1_D_ini.txt,not(../ByHand_vetoes.txt))' | awk '{if ($2-$1>200) print $1,$2,$2-$1}' > H1L1_D.txt

segexpr 'union(H1L1_A.txt,H1L1_B.txt)' > toto1.txt
segexpr 'union(H1L1_C.txt,H1L1_D.txt)' > toto2.txt

segexpr 'union(toto1.txt,toto2.txt)' | awk '{print "1 " $1, $2, $2-$1}'> jobsH1L1.txt

rm toto1.txt
rm toto2.txt
rm H1-VETOTIME_A.txt  H1-VETOTIME_B.txt  H1-VETOTIME_C.txt  H1-VETOTIME_D.txt
rm L1-VETOTIME_A.txt  L1-VETOTIME_B.txt  L1-VETOTIME_C.txt  L1-VETOTIME_D.txt
rm H1-SCIENCE_A_afterveto.txt H1-SCIENCE_B_afterveto.txt H1-SCIENCE_C_afterveto.txt H1-SCIENCE_D_afterveto.txt
rm L1-SCIENCE_A_afterveto.txt L1-SCIENCE_B_afterveto.txt L1-SCIENCE_C_afterveto.txt L1-SCIENCE_D_afterveto.txt
rm H1L1_A_ini.txt H1L1_B_ini.txt H1L1_C_ini.txt H1L1_D_ini.txt
rm H1L1_A.txt H1L1_B.txt H1L1_C.txt H1L1_D.txt