# Options
usage(){
    echo 'Usage ./get_veto_definer.sh O2'
    echo 'Arg 1: RUN name'

    exit
}

# main
[[ $# -ne 1 ]] && usage

RUN=$1


#should we get the master version of the tagged version?

case $RUN in
'O1')
	#wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/blob/master/cbc/O1/H1L1-HOFT_C00_O1_CBC.xml
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/burst/ER8/H1L1-HOFT_C00_ER8A_BURST.xml
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/burst/ER8/H1L1-HOFT_C00_ER8B_BURST.xml
	
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/burst/O1/H1L1-HOFT_C00_O1_BURST.xml
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/burst/O1/H1L1-HOFT_C01_O1_BURST.xml
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/burst/O1/H1L1-HOFT_C02_O1_BURST.xml
	
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/cbc/ER7/H1L1V1-ER7_CBC_OFFLINE.xml
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/cbc/ER8/H1L1-HOFT_C00_ER8A_CBC.xml
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/cbc/ER8/H1L1-HOFT_C00_ER8B_CBC.xml
	
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/cbc/O1/H1L1-HOFT_C00_O1_CBC.xml
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/cbc/O1/H1L1-HOFT_C01_O1_CBC.xml
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/cbc/O1/H1L1-CBC_VETO_DEFINER_C00_O1_1126051217-11203200.xml
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/cbc/O1/H1L1-CBC_VETO_DEFINER_C01_O1_1126051217-11203200.xml
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/cbc/O1/H1L1-CBC_VETO_DEFINER_C02_O1_1126051217-11203200.xml
	wget --no-check-certificate https://code.pycbc.phy.syr.edu/detchar/veto-definitions/download/master/cbc/O1/H1L1-IMBH_CBC_VETO_DEFINER_C02_O1_1126051217-11203200.xml
;;
'O2' | 'O3')
	if [ -e veto-definitions ]; then
	    cd veto-definitions
	    git pull
	else
	    git clone git@git.ligo.org:detchar/veto-definitions.git
#	    cp veto-definitions/burst/${RUN}/*.xml test/.
	fi
;;
esac
