O1 jobfiles:

jobsH1L1_1.txt
Sep 12 --> Jan 12 + cat1+cat4 removed + DCH-ANALYSIS_READY_C01
./generate_jobfile.sh O1 1126073342  1136649617
H1  cat1 segments duration 275229  s
H1  cat4 segments duration 2785  s
H1  science segments duration 6558753  s
H1  science segments after cat1 duration 6401628  s (dt:  2.300 %)
H1  science segments after cat14 duration 6400519  s (dt:  2.400 %)
L1  cat1 segments duration 126364  s
L1  cat4 segments duration 1463  s
L1  science segments duration 5684430  s
L1  science segments after cat1 duration 5625530  s (dt:  1.000 %)
L1  science segments after cat14 duration 5624374  s (dt:  1.000 %)
 
H1  cat2 segments duration 16846  s
H1  science segments duration 6558753  s
H1  cat2 deadtime: 16846  s (dt:  .200 %)
L1  cat2 segments duration 2353  s
L1  science segments duration 5684430  s
L1  cat2 deadtime: 2353  s (dt:  0 %)
 
H1 L1
H1  science segments duration 4372994  s
H1  science segments after cat1&cat4 duration 4231465  s (dt:  3.200 %)

jobsH1L1_2.txt
Sep 12 --> Jan 19 + cat1+cat4 removed + DCH-ANALYSIS_READY_C01
./generate_jobfile.sh O1 1126073342  1137254417
H1  cat1 segments duration 550518  s
H1  cat4 segments duration 7984  s
H1  science segments duration 6828870  s
H1  science segments after cat1 duration 6671745  s (dt:  2.300 %)
H1  science segments after cat14 duration 6668381  s (dt:  2.300 %)
L1  cat1 segments duration 254808  s
L1  cat4 segments duration 6023  s
L1  science segments duration 5817457  s
L1  science segments after cat1 duration 5758557  s (dt:  1.000 %)
L1  science segments after cat14 duration 5754649  s (dt:  1.000 %)
 
H1  cat2 segments duration 17217  s
H1  science segments duration 6828870  s
H1  cat2 deadtime: 17217  s (dt:  .200 %)
L1  cat2 segments duration 2400  s
L1  science segments duration 5817457  s
L1  cat2 deadtime: 2400  s (dt:  0 %)
 
H1 L1
H1  science segments duration 4453747  s
H1  science segments after cat1&cat4 duration 4309976  s (dt:  3.200 %)


jobsH1L1_3.txt
Sep 12 --> Jan 12 + cat1 removed + DCH-ANALYSIS_READY_C01

[mabizoua@ldas-pcdev1 jobfiles]$ ./generate_jobfile.sh O1 1126073342  1136649617
Traceback (most recent call last):
  File "/usr/bin/ligolw_print", line 209, in <module>
    xmldoc = utils.load_url(url, verbose = options.verbose, contenthandler = ContentHandler)
  File "/usr/lib64/python2.6/site-packages/glue/ligolw/utils/__init__.py", line 409, in load_url
    fileobj = open(path)
IOError: [Errno 2] No such file or directory: 'H1-VETOTIME_CAT4-1126073342-*.xml'
H1  cat1 segments duration 275229  s
H1  cat4 segments duration 0  s
H1  science segments duration 6558753  s
H1  science segments after cat1 duration 6401628  s (dt:  2.300 %)
H1  science segments after cat14 duration 6401628  s (dt:  2.300 %)
Traceback (most recent call last):
  File "/usr/bin/ligolw_print", line 209, in <module>
    xmldoc = utils.load_url(url, verbose = options.verbose, contenthandler = ContentHandler)
  File "/usr/lib64/python2.6/site-packages/glue/ligolw/utils/__init__.py", line 409, in load_url
    fileobj = open(path)
IOError: [Errno 2] No such file or directory: 'L1-VETOTIME_CAT4-1126073342-*.xml'
L1  cat1 segments duration 126364  s
L1  cat4 segments duration 0  s
L1  science segments duration 5684430  s
L1  science segments after cat1 duration 5625530  s (dt:  1.000 %)
L1  science segments after cat14 duration 5625530  s (dt:  1.000 %)
 
H1  cat2 segments duration 16846  s
H1  science segments duration 6558753  s
H1  cat2 deadtime: 16846  s (dt:  .200 %)
L1  cat2 segments duration 2353  s
L1  science segments duration 5684430  s
L1  cat2 deadtime: 2353  s (dt:  0 %)
 
H1 L1
H1  science segments duration 4372994  s
H1  science segments after cat1&cat4 duration 4232579  s (dt:  3.200 %)


jobsH1L1_4.txt
Sep 12 --> Jan 19 + cat1 removed + DCH-ANALYSIS_READY_C01
[mabizoua@ldas-pcdev1 jobfiles]$ ./generate_jobfile.sh O1 1126073342  1137254417
Traceback (most recent call last):
  File "/usr/bin/ligolw_print", line 209, in <module>
    xmldoc = utils.load_url(url, verbose = options.verbose, contenthandler = ContentHandler)
  File "/usr/lib64/python2.6/site-packages/glue/ligolw/utils/__init__.py", line 409, in load_url
    fileobj = open(path)
IOError: [Errno 2] No such file or directory: 'H1-VETOTIME_CAT4-1126073342-*.xml'
H1  cat1 segments duration 275289  s
H1  cat4 segments duration 0  s
H1  science segments duration 6828870  s
H1  science segments after cat1 duration 6671745  s (dt:  2.300 %)
H1  science segments after cat14 duration 6671745  s (dt:  2.300 %)
Traceback (most recent call last):
  File "/usr/bin/ligolw_print", line 209, in <module>
    xmldoc = utils.load_url(url, verbose = options.verbose, contenthandler = ContentHandler)
  File "/usr/lib64/python2.6/site-packages/glue/ligolw/utils/__init__.py", line 409, in load_url
    fileobj = open(path)
IOError: [Errno 2] No such file or directory: 'L1-VETOTIME_CAT4-1126073342-*.xml'
L1  cat1 segments duration 128444  s
L1  cat4 segments duration 0  s
L1  science segments duration 5817457  s
L1  science segments after cat1 duration 5758557  s (dt:  1.000 %)
L1  science segments after cat14 duration 5758557  s (dt:  1.000 %)
 
H1  cat2 segments duration 17107  s
H1  science segments duration 6828870  s
H1  cat2 deadtime: 17107  s (dt:  .200 %)
L1  cat2 segments duration 2398  s
L1  science segments duration 5817457  s
L1  cat2 deadtime: 2398  s (dt:  0 %)
 
H1 L1
H1  science segments duration 4453747  s
H1  science segments after cat1&cat4 duration 4313332  s (dt:  3.100 %)
