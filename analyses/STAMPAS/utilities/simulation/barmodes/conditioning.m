function conditioning(name,out,letter)

theta = 0;

x=load(name);

fs = 4096;
t = 0:1/fs:max(x(:,1));

[junk,idx] = unique(x(:,1));
x = x(idx,:);
h = interp1(x(:,1),x(:,3),t);
f = interp1(x(:,1),x(:,2),t);
h(isnan(h)) = 0;
f(isnan(f)) = 0;
dt = 1/fs;
%phi = 2*pi*cumsum(f)*dt;
phi = 2*pi*cumtrapz(t,f);

taper = linspace(0,1-dt,fs);
N = length(taper);
mask = ones(length(t),1);
mask(1:N,1)=sin(pi/2.*taper).^2;
mask(end-N+1:end,1) = flipud(mask(1:N,1));

clear taper
y=zeros(length(t),3);
y(:,1) = t;
y(:,2) = 1e-21 * -h .* cos(phi) * ((1+cos(theta)^2)/2) .* mask';
y(:,3) = 1e-21 * -h .* sin(phi) * cos(theta) .* mask';

dlmwrite(out,y,'delimiter','\t','precision','%.8g');
clear mask

msun   = 1.988e30;   % kg
c      = 299792458;  % m s^-1
G      = 6.67e-11;   %
msunc2 = msun*c^2;   % kg m^2 s^-2
pc     = 3.08e16;    % m
r      = 100e6*pc;

dhp    = (diff(y(:,2))*fs);
dhx    = (diff(y(:,3))*fs);
E =  r^2*c^3/G*1/4*sum((9/16*dhp.^2+1/2*dhx.^2)/fs)/msunc2;
hrss =sqrt(sum(y(:,2).^2+y(:,3).^2)*dt);


display(' ')
display(['legend: Bar modes ' letter])
display(['file: ' out ])
display(['energy: ' num2str(E)])
display(['fmin: ' num2str(min(x(:,2)))])
display(['fmax: ' num2str(max(x(:,2)))])
display(['duration: ' num2str(y(end,1)-y(1,1))])
display(['hrss: ' num2str(hrss)])
display('distance: 100')
display('type: ascii')






