function burstegard(eB, f0, duration,letter)

eB = 0.1; % in unit of 10^14 G
f0 = 1800; % Hz
duration = 450; %s

taper_size=2;
fmin=10;
fs = 4096;

T0 = 0;
tstart = 0;
tend = duration;

[hp, hc, t, freq] = magnetar(eB,f0,duration,fs,T0,tstart,tend);

fmin = floor(nanmin(freq));
fmax = ceil(max(freq));
dt = t(2) - t(1);
hrss =sqrt(sum(hp.^2+hc.^2)*dt);

fprintf('%.5f %.5f %.5f %.5e\n',duration,fmin,fmax,hrss);

% save hp hc
fid=fopen(['magnetar' letter '.dat'],'w');
fprintf(fid,'%% fmin fmax dist(Mpc) hrss \n');
fprintf(fid,'%% %d %d 1 %.3g\n', fmin, fmax, hrss);
fprintf(fid,'%% \n');
fprintf(fid,'%% t h+ hx \n');
fprintf(fid,'0 0.0 0.0 \n');

for i=1:length(hp)
  fprintf(fid, '%.8f %.8e %.8e\n', i*dt, hp(i),hc(i));
end
fclose(fid);

% appy a taper window
hp_t=apply_taper(hp,taper_size,dt);
hc_t=apply_taper(hc,taper_size,dt);

% save hp hc
fid=fopen(['magnetar' letter '_tapered.dat'],'w');
fprintf(fid,'%% fmin fmax dist(Mpc) hrss \n');
fprintf(fid,'%% %d %d 1 %.3g\n', fmin, fmax, hrss);
fprintf(fid,'%% \n');
fprintf(fid,'%% t h+ hx \n');
fprintf(fid,'0 0.0 0.0 \n');
for i=1:length(hp)
  fprintf(fid, '%.8f %.8e %.8e\n', i*dt, hp_t(i),hc_t(i));
end

return


function [h_t]=apply_taper(h,taper_size,dt)

n_taper=round(taper_size/dt);
window=hanning(n_taper);

h_t=h;
wvf_len=length(h);

for i=1:n_taper/2
  h_t(i)=h_t(i)*window(i);
end

for i=n_taper/2:n_taper
  h_t(wvf_len-n_taper+i)=h_t(wvf_len-n_taper+i)*window(i);
end

return