function burstsg(f0,tau)
% function burstsg()
%
% Produces a sine-Gaussian waveform with the parameters
% specified below.
%
% Adapted from burstsg.m,v 1.2 2006-02-14 15:46:45 kathorne
% by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-- Default parameters
fs = 4096;          %-- Sampling rate (Hz).
hrssNorm = 1e-20;   %-- Default h_rss normalization.
%wf_dur = 250.0;     %-- Length of waveform (seconds).
%f0 = 405;           %-- Minimum frequency (Hz).
%tau = wf_dur/5;     %-- Gaussian decay constant (seconds).
wf_dur=5*tau;
iota = 0*(pi/180);  %-- iota in degrees
psi = 0*(pi/180);   %-- psi in degrees
offset_frac = 0.5;  %-- Offset within output time series (as part of block)

% Calculate quality factor Q from f0, tau of Gaussian.
Q = tau * (sqrt(2) * pi * f0);
fprintf('f0 = %.3g, Q = %.3g, tau = %.3g, duration = %.3g\n',f0,Q,tau,wf_dur);

% Calculate length of time series in samples
t = 0:1/fs:wf_dur;

% Calculate hp and hx.
Phit = 2*pi*(f0*t);
Ap = (1+cos(iota)^2)/2;
Ax = cos(iota);
hp = Ap*cos(2*psi)*cos(Phit) - Ax*sin(2*psi)*sin(Phit);
hx = Ap*sin(2*psi)*cos(Phit) + Ax*cos(2*psi)*sin(Phit);

% Calculate Gaussian window.
tOffset = wf_dur * offset_frac;
tg = t - tOffset;
gauss = exp(-(tg/tau).^2);

% Multiply hp and hx by Gaussian
hp = hp .* gauss;
hx = hx .* gauss;

% Taper the waveform
hp = apply_window(hp,fs,1,'both');
hx = apply_window(hx,fs,1,'both');

% Normalize hp and hx such that the total
% hrss is equal to hrssNorm.
hrss = sqrt(trapz(t,(hp.^2 + hx.^2)));
normFactor = hrssNorm/hrss;
hp = hp .* normFactor;
hx = hx .* normFactor;

% Set up wf array.
wf = [t; hp; hx;];

% Write to file.
fname = 'sineGaussian.dat';
fid = fopen(fname,'w+');
fprintf(fid,'%% fmin fmax dist(Mpc) hrss\n');
fprintf(fid,'%% %d  %d  0 %.2e\n',f0-3,f0+3,hrssNorm);
fprintf(fid,'%% \n');
fprintf(fid,'%% t h+ hx\n');
fprintf(fid,'%.12e\t%.8e\t%.8e\n',wf);
fclose(fid);


return
