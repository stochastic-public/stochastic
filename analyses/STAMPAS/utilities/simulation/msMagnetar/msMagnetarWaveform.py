'''
Paul Lasky:  paul.lasky@ligo.org

Code for caluclating gravitational waveforms for the millisecond magnetar model
with arbitrary braking index (see Lasky, Leris, Rowlinson and Glampedakis 2017 for details)
This assumes spindown is a power law with braking index nn, and gravitational-wave
emission is at twice the spin frequency.
'''

import numpy as np

import astropy.constants as cc
import astropy.units as uu

import pdb

def fgw(time, f0, tau, nn):
    ### gravitational-wave frequency as function of time:
    ### f(t) = f0 (1 + t/tau)^[1/(1-n)]
    # time: time array [s]
    # f0: initial gravitational-wave frequency [Hz]
    # tau: spindown timescale [s]
    # nn: braking index [dimensionless]

    return f0 * (1.+ time / tau)**(1./(1-nn))

def h0(time, f0, tau, nn, eps, dd, II = 1e45):
    ### gravitational-wave amplitude as function of time:
    ### h(t)=4\pi^2 G II eps fgw(t) / (c^4 dd)
    # time: time array [s]
    # f0: initial gravitational-wave frequency [Hz]
    # tau: spindown timescale [s]
    # nn: braking index [dimensionless]
    # eps: ellipticity of the star [dimensionless]
    # dd: distance [Mpc]
    # II: principal moment of inertia [g cm^2]

    f_gw = fgw(time, f0, tau, nn)  # create the frequency array

    # get the units right:
    f_gw = f_gw * uu.Hz   
    II = II * uu.g * uu.cm**2 
    dd = dd * uu.Mpc

    consts = 4. * np.pi**2 * cc.G / cc.c**4

    # calculate the strain, and convert to cgs units
    # should be dimensionless, so this is a sanity check
    hh = (consts * II * eps * f_gw**2 / dd).cgs

    return hh, f_gw


def ht(time, f0, tau, nn, eps, dd, iota, II = 1e45, Phi0 = 0.):
    ### gravitational-wave strain time series as a function of time
    # time: time array [s]
    # f0: initial gravitational-wave frequency [Hz]
    # tau: spindown timescale [s]
    # nn: braking index [dimensionless]
    # eps: ellipticity of the star [dimensionless]
    # dd: distance [Mpc]
    # iota: inclination angle [degrees]
    # II: principal moment of inertia [g cm^2]
    # Phi0: phase at time = 0

    h_0, f_gw = h0(time, f0, tau, nn, eps, dd) # Calc GW strain array and frequency

    # phase (fgw comes with units of Hz.  need to get rid of them)
    term1 = 2.* np.pi * tau * f0 * (1.-nn) / (2.-nn)
    term2 = (1.+time / tau)**((2.-nn) / (1.-nn)) - 1
    Phi = Phi0 + term1 * term2

    hp = h_0 * (1. + np.cos(iota)**2) / 2. * np.cos(Phi)  # h_plus
    hx = h_0 * np.cos(iota) * np.sin(Phi)    # h_cross

    return hp, hx
