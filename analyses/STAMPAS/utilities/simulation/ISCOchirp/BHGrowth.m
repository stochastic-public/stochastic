%----------------------------------------------------------------------------
%  
%	======================================================================
%	Authors:	Maurice H.P.M. van Putten
%	======================================================================
%	Code name:                   BHGrowth.m
%	Language:                    MatLab 8.5.0 (R2015a)
%	Code tested by compilers/OS: n/a
%	Description of input data:	 none
%	Description of output data:  figures (eps,jpg,pdf) and ascii output
%	System requirements:		 none
%	Calls to external routines:  LaneEmden.m,Lz.m,rms.m,zF.m,fisco.m
%	Dependencies:				 none
%	Additional comments:		 none
% =======================================================================
%  The AAS gives permission to anyone who wishes to use these subroutines
%  to run their own calculations. Permission to republish or reuse these
%  routines should be directed to permissions@aas.org. Note that the AAS 
%  does not take responsibility for the content of the source code. 
%  Potential users should be wary of applying the code to conditions that 
%  the code was not written to model and the accuracy of the code may be 
%  affected when compiled and executed on different systems.
% ========================================================================
%
%   DESCRIPTION AND BACKGROUND
%    
%  This MatLab program calculates three phases in black hole evolution in 
%  core-collapse of a progenitor in uniform rotation with a Lane-Emden mass 
%  distribution. All results are normalized to the mass of the progenitor
%  at the onset of collapse. Calculated are the subsequent phases of:
%
%  I. Direct infall of angular momentum poor fall back matter [8];
%  II. Idealized Bardeen accretion, neglecting outflows [2,9,15];
%  III. Spin down against the ISCO [4-8,14] at subcritical accretion [13].
%
%  These phases show, respectively, rapid growth in mass with a decrease in 
%  the dimensionless spin parameter a/M [1]; growth to a high mass near-
%  extremal black hole, provided the progenitor is sufficiently massive at
%  the given spin rate; spin down to a slowly rotating black hole remnant.
%  Equations solved are: Direct infall (jISCO < jISCO(M,J)[4]); Bardeen
%  integral zM^2=const.[2]; suspended accretion [6,12].
%
%  For Phase III, a model light curve of the baryon-poor jet (BPJ) is
%  calculated along an open flux tube along the black hole spin axis
%  in a split topology of poloidal magnetic flux [8,14], suspended by
%  the event horizon at a finite half-opening angle in its lowest energy
%  state. The evolution of the black hole is governed by a major output 
%  to matter at the ISCO, mediated by Alfven waves in an inner torus 
%  magnetosphere [4-6,12] at subcritical accretion rates [13]. Matter at 
%  the ISCO hereby catalyzes most of the spin energy unseen into 
%  gravitational radiation, MeV neutrinos and magnetic winds [6-8]. 
%  The light curve in electromagnetic radiation produced by dissipation 
%  downstream of the BPJ thus defined can be seen in normalized light 
%  curves of the BATSE catalogue [11,12,14].
%  
%  Selected references: 
%    1. Kerr, R.P., 1963, Phys. Rev. Lett., 11, 237
%    2. Bardeen, J.M., 1970, Nature, 226, 64
%    3. Bardeen, J.M., Press, W.H., & Teukolsky, S.A., 1972, 
%       Phys. Rev. D, 178, 347
%    4. van Putten, M.H.P.M., Science, 1999, 284, 115
%    5. van Putten, M.H.P.M., & Ostriker, E., 2001, ApJ, 552, L31
%    6. van Putten, M.H.P.M., 2001, Phys. Rev. Lett., 87, 091101
%    7. van Putten, M.H.P.M., 2002, ApJ, 575, L71
%    8. van Putten, M.H.P.M., & Levinson, A., 2003, ApJ, 584, 937
%    9. van Putten, M.H.P.M., 2004, ApJ, 611,L81
%    10. van Putten, M.H.P.M., 2008, ApJ, 684, L91
%    11. van Putten, M.H.P.M., & Gupta, A., 2009, 394, 2238
%    12. van Putten, M.H.P.M., 2012, Prog. Theor. Phys., 127, 331
%    13. Globus, N., & Levinson, A., 2014, ApJ, 796, 26
%    14. van Putten, M.H.P.M., 2015, ApJ, 810, 7
%    15. van Putten, M. H. P. M. 2016, Zenodo, doi: 10.5281/zenodo.45298
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%  (c)2016 Maurice H.P.M. van Putten
%	This program is distributed under the GNU General Public Licence
%   <http://www.gnu.org/licenses/>
%--------------------------------------------------------------------------
clear all;
%--------------------------------------------------------------------------
%   LANE-EMDEN MASS DISTRIBUTION OF THE PROGENITOR 
%--------------------------------------------------------------------------
clear line1;N=1000;i_a=0;
tmx=8;n=3;eps=0.001;
  c0=1-eps^2/6;c1=-eps^3/3;c2=4*pi/3*eps^3;c3=(3/5)*eps^2*c2;
  options=odeset('RelTol',1e-9,'Refine',4); 
  [t,y0]=ode23('LaneEmden_ode',[eps tmx],[c0;c1;c2;c3],options,n);
  M=length(t);for i=1:M,if(imag(y0(i,4))==0)M=i;end;end
  y(1:M,:)=y0(1:M,:);k=M;for i=1:M-1,if(y(i+1,1)*y(i,1)<0)k=i;end;end
figure(1)
  subplot(221)
    plot(t(1:k),y(1:k,1).^n);grid on;
    xlabel('r','FontSize',16);ylabel('\rho','FontSize',18);legend('Lane-Emden')
  subplot(222)
    plot(t(1:k),y(1:k,3));grid on;
    xlabel('r','FontSize',16);ylabel('M[<r]','FontSize',14)
  subplot(223)
    plot(t(1:k),y(1:k,4));grid on;
    xlabel('r','FontSize',16);ylabel('J[<r]','FontSize',14)
  subplot(224)
    A=y(1:k,4)./y(1:k,3).^2;loglog(t(5:k),A(5:k));grid on;
    xlabel('r','FontSize',16);ylabel('(J/M^2)[<r]','FontSize',14);
    legend('normalized (\beta=1)');axis([0 8 0.01 100])

%--------------------------------------------------------------------------
%   Part I. DIRECT ACCRETION FOLLOWING BLACK HOLE BIRTH  
%--------------------------------------------------------------------------
kb=2/3;
G=1;c=1;rhoc=1;
min_a=min(y(10:k,4)./y(10:k,3).^2);
beta_max=1/kb/min_a;
  Mass=y(:,3); %true M
  J_H =y(:,4); %true J = beta*J_H
for i_select=1:1
    
  clear A_i B_i p_i q_i i_ic P Pp;i_p=0;i_P=0;
  for ic=1:N  %Apply ISCO and Kerr condition for a range of beta values
   
    beta      = beta_max*(0.25+0.85*(ic-1)/N);
    omega     = beta*rhoc*t(k);
    beta_c(ic)= beta;
    P_c(ic)   = 2*pi/omega/3600/24;

    i_counter=0;j_counter=0;
    for i=2:M
      a_i(i)=min(kb*beta*J_H(i)/Mass(i)^2,1);
      if(a_i(i)==1) i_a(i)=1;else i_a(i)=0;end; %flag sum(i_a)>=1
      A_i(i)= beta*t(i)^2/Mass(i) - Lz(zF(a_i(i))); %Lz(rms(a_i(i)));
      B_i(i)= kb*beta*J_H(i)/Mass(i)^2;   
    end
    for i=2:M-1
      if(A_i(i+1)*A_i(i)<0)         %zero-crossings A
        i_counter=i_counter+1;p_i(i_counter)=i;
      end
    end
    i_ic(ic)=i_counter;
  
  for i=1:i_counter                 %zero-crossings A
    p=p_i(i);
    if(i==1)
       s=p+1;r1=[t(p) B_i(p) Mass(p)/Mass(k) Mass(k) p];r2=[t(s) B_i(s) Mass(s)/Mass(k) Mass(k) s];
       x1_ic(ic,:)=0.5*(r1+r2);
    elseif(i==2)
       s=p+1;r1=[t(p) B_i(p) Mass(p)/Mass(k) Mass(k) p];r2=[t(s) B_i(s) Mass(s)/Mass(k) Mass(k) s];
       x2_ic(ic,:)=0.5*(r1+r2);
       kdiv=2;
       if(floor(ic/kdiv)*kdiv==ic)
         i_p=i_p+1;
         Pp(i_p,:)=[ic p_i(1:2) y(p_i(1:2),3)'/Mass(k) B_i(p_i(1:2)) beta];
       end
       Kx=600; %up to max i_ic=2
       K=Kx;
       if(floor(ic/K)*K==ic)
         figure(6)
         i_P=i_P+1;
         P(i_P,:)=[ic p_i(1:2) y(p_i(1:2),3)'/Mass(k) B_i(p_i(1:2)) beta];
         time = t(p_i(1):p_i(2)).^(3/2);
         m    = y(p_i(1):p_i(2),3)/Mass(k);
         a    = B_i(p_i(1):p_i(2));max_a=max(a)
         Fi   = 1./m./(zF(a').^(3/2)+a');
         l    = length(time);
         B    = [time(l) y(p_i(2),3)/Mass(k) a(l) Fi(l)];
         subplot(311);
           semilogx(time(1),y(p_i(1),3)/Mass(k),'*b','MarkerSize',10);hold on;
           semilogx(time,y(p_i(1):p_i(2),3)/Mass(k),'b--');box on;grid on;  
         subplot(312) 
           semilogx(time(1),a(1),'*b','MarkerSize',10);hold on;
           semilogx(time,a,'b--');box on;grid on;
         subplot(313)
           semilogx(time(1),Fi(1),'*b','MarkerSize',10);hold on;
           semilogx(time,Fi,'b--');box on;grid on; 
         if(ic==Kx & i_select==1) 
            line1=[time a' Fi m];
            P1=P;
         end
       end
    end
  end %for i=1:i_counter
  
  end %for ic=1:N
  Pp1=Pp;

  N1=N;N2=1;
  for i=2:N,
    if(N1==N),if(i_ic(i)==2)N1=i;NN1=i;end;end
    if(i_ic(i-1)==2),if(i_ic(i)<2)N2=i-1;end;end
  end
  Nm=N1;Nx=N2;
  figure(2);
  NL=1;
  Nx=min(length(x1_ic),length(x2_ic));
  subplot(211);
    u2=1./beta_c(Nm:Nx);
    ua2_i=x2_ic(Nm:Nx,2);um2_i=x2_ic(Nm:Nx,3);
    ua1_i=x1_ic(Nm:Nx,2);um1_i=x1_ic(Nm:Nx,3);
    u2Erot_i=2*sin(asin(ua2_i)/4).^2.*x2_ic(Nm:Nx,3);
    u1Erot_i=2*sin(asin(ua1_i)/4).^2.*x1_ic(Nm:Nx,3);
    Ip=1:50:length(u2);
    semilogy(u2(Ip),u2Erot_i(Ip),'bo-','LineWidth',1);hold on;
    semilogy(u2(Ip),u1Erot_i(Ip),'k*--','LineWidth',1);grid on;
    semilogy(u2(Ip),um2_i(Ip),'bo-','LineWidth',1);           
    semilogy(u2(Ip),um1_i(Ip),'k*--','LineWidth',1);        
    axis([0 0.4 1e-4 1]);
  subplot(212);
    plot(u2(Ip),ua2_i(Ip),'bo-','LineWidth',1);hold on;  %final a/M
    plot(u2(Ip),ua1_i(Ip),'k*--','LineWidth',1);grid on;  %initial a/M
    annotation('arrow',[0.68 0.68],[0.38 0.30],'linewidth',3);
    xlabel('1/\beta','fontsize',16);ylabel('a/M','fontsize',16);
    axis([0 0.4 1e-4 1]);
  subplot(211);
    text(0.06,0.30,'M/M_0 ','FontSize',16);
    text(0.06,0.03,'E_{rot}/M_0','FontSize',16);
    legend('Disk first forms','Black hole at birth','Location','southwest')
    annotation('arrow',[0.63 0.63],[0.72 0.78],'linewidth',3);
    annotation('arrow',[0.74 0.74],[0.76 0.90],'linewidth',3);
    xlabel('1/\beta','fontsize',16);
    title('Direct accretion','FontSize',14);
end
%--------------------------------------------------------------------------
%   Part II. IDEALIZED LIMIT OF BARDEEN ACCRETION
%--------------------------------------------------------------------------
N=5000;clear A1 a_i M_i Z_i i_k A_i U_i Mk F_i;
for i=1:N
   a_i(i)=(i-1)/N;z_i(i)=zF(a_i(i));
end
b_i=1./(z_i.^(3/2)+a_i);
for k=1:3
   n0=1+(k-1)*2200;
   z0=z_i(n0);M0=4;M1=120;
   for i=1:N
     M_i(i)=M0+(M1-M0)*i/N;
     Z_i(i)=z0*(M0/M_i(i)).^2;  %Bardeen integral
     if(Z_i(i)>1)i_k(k)=i;end
     j0=0;
     for j=1:N,if(z_i(j)>Z_i(i))j0=j;end;end
     A_i(i)=a_i(j0);
     U_i(i)=z_i(j0);
   end
   Ik=1:i_k(k);
   Omega_ISCO = 1./M_i./(Z_i.^(3/2)+A_i);
   h_i        = 1./(2*pi).*Omega_ISCO;
   m_i(k)     = max(2*pi*M_i(Ik).*h_i(Ik));
   Mk(k)      = max(M_i(i_k(k)));
   F_i        = h_i*3e10/1.5e5;
   lambda     = asin(A_i);
   OmegaH_i   = 0.5./M_i.*tan(lambda/2);
  
   clear out;out(:,1)=M_i(Ik)';out(:,2)=A_i(Ik)';out(:,3)=Z_i(Ik)';out(:,4)=F_i(Ik)';
   save BHPhaseII.txt out -ascii;

    if(k==1)
      figure(32);
      subplot(211);
        plot(A_i(Ik),M_i(Ik).*Omega_ISCO(Ik));
        axis([0 1 0 0.5]);grid on;hold on;
        m_1=max(M_i.*Omega_ISCO);
        plot(A_i(Ik),M_i(Ik).*OmegaH_i(Ik),'linewidth',4);grid on;hold on;
        xlabel('a/M','FontSize',14);
        ylabel('M\Omega','FontSize',14);
        legend('M\Omega_{ISCO}','M\Omega_H',2);
        title('Bardeen accretion','FontSize',14);
    end
    if(k>0)
      figure(31)
      subplot(211);
        plot(M_i(Ik),A_i(Ik));axis([0 10 0 1]);grid on;hold on;
        plot(M_i(1),A_i(1),'ob','MarkerSize',10);
        plot(M_i(i_k(k)),A_i(i_k(k)),'^b','MarkerSize',10);
        xlabel('M [{Solar}]','FontSize',14);
        ylabel('a/M','FontSize',14);
        title('Bardeen accretion','FontSize',14);
        axis([4 10 0 1.2]);
      subplot(212);
        plot(M_i(Ik),2*F_i(Ik));axis([0 10 0 3e3]);grid on;hold on;
        plot(M_i(1),2*F_i(1),'ob','MarkerSize',10);
        plot(M_i(i_k(k)),2*F_i(i_k(k)),'^b','MarkerSize',10);
        xlabel('M [{Solar}]','FontSize',14);
        ylabel('f_{GW}^{ISCO} [Hz]','FontSize',14);
        axis([4 10 0 6000]);
      figure(32);subplot(212);
        plot(M_i(Ik),M_i(Ik).*Z_i(Ik));hold on;grid on;
        xlabel('M [{Solar}]','FontSize',14);
        ylabel('r_{ISCO}','FontSize',14);
    end
end
%--------------------------------------------------------------------------
%   Part III. SPIN DOWN AGAINST MATTER AT THE ISCO
%--------------------------------------------------------------------------
t0=t;
%SCALING OF MASS AND DURATION
fM=10;        %Initial black hole mass set to 10 MSolar
fC=0.5;fz=1.0;%Scaling of feedback (fz also in fisco.m) and R_Torus=R_ISCO

%ISCO FORMULAS
a=0.0001:0.0001:0.9999;la=asin(a);
ZZ1=1+(1-a.^2).^(1/3).*((1+a).^(1/3)+(1-a).^(1/3));ZZ2=(3*a.^2+ZZ1.^2).^0.5;
z1=3+ZZ2-((3-ZZ1).*(3+ZZ1+2*ZZ2)).^0.5;z2=3+ZZ2+((3-ZZ1).*(3+ZZ1+2*ZZ2)).^0.5;
z1=fz*z1;z2=fz*z2;

%TEMPLATES a/M in the range (0.36,1)
figure(40*fM+1)
kkmx=60;
for kk=1:kkmx
   
   %MATLAB ODE45 INTEGRATION (M-INIT=1MSolar) 
   clear T Y;ak=0.359999+0.64*(kk-1)/(kkmx-1);a_k(kk)=ak;a_initial=ak
   inc=[1,ak];eps=1e-12;tol=eps;          
   options = odeset('RelTol',eps,'AbsTol',tol);
   [T,Y]=ode45('fisco',[0 40/fC],inc,options);L=length(T);
   t=T;
   %OUTPUT PARAMETERS 
   Egw(kk)=1-Y(length(T),1);M=Y(:,1);J=Y(:,2);aa=J./M.^2;
     Z1=1+(1-aa.^2).^(1/3).*((1+aa).^(1/3)+(1-aa).^(1/3));Z2=(3*aa.^2+Z1.^2).^0.5;
     z=3+Z2-((3-Z1).*(3+Z1+2*Z2)).^0.5;E=sqrt(1-2/3./z);C1=fC*E;                    
     lambda=asin(aa);OmegaH =tan(lambda/2)/2./M;OmegaT =1./M./(z.^(3/2)+aa);
     C=3*C1.*(M.*z.*OmegaT).^2; 
   eta=OmegaT./OmegaH;
     Erot=2*M.*sin(lambda/4).^2;Erot_k(kk)=Erot(1); 
     A=C.*eta.*(1-eta).*OmegaH.^2;  
     f=OmegaT/pi*3e10/(1.5e5*fM);       %f scaling by fM
     EkTorus=C1.*(M.*z.*OmegaT).^2;Lj=(OmegaH.*z).^2.*EkTorus;
   
   %PLOT RESULTS (SCALING BY fM WHEN NEEDED)
   f0(kk)=f(1);f1(kk)=f(L);dT(1)=T(1);df(1)=f(2)-f(1);
   dM(1)=M(2)-M(1);mf(1)=f(1);
   L=length(T);
   for i=2:L-1
       dT(i)=(T(i+1)-T(i-1))/2;df(i)=(f(i+1)-f(i-1))/2;
       dM(i)=(M(i+1)-M(i-1))/2;dE(i)=dM(i)/df(i);
       mf(i)=(f(i+1)+f(i-1))/2;
   end;
   dT(L)=T(L)-T(L-1);df(L)=f(L)-f(L-1);
   dM(L)=M(L)-M(L-1);                   %mf(L)=f(L);
   N_Phi(kk)=sum(f(1:L).*transpose(dT(1:L)));%total nr of periods
   dk=kkmx/10;
   if(kk==kkmx)
      IP=1:100:L;l=length(IP),IP(l+1)=L;
      FS=14;
      subplot(211);
        plot(T(IP),Y(IP,1),'b','LineWidth',1);hold on;grid on;
        plot(T(IP),Y(IP,2)./Y(IP,1).^2,'b--','LineWidth',1);
        plot(T,1./eta,'b-.');
        
        plot(1,Y(1,1),'b^','MarkerSize',FS);
        plot(T(L),Y(L,1),'bs','MarkerSize',10);
        plot(T(L),Y(L,2)/Y(L,1)^2,'bs','MarkerSize',10);
        plot(T(L),1/eta(L),'bs','MarkerSize',10);
    
        xlabel('time [s]','fontsize',FS);
        legend('M','a/M','\Omega_H/\Omega_{ISCO}','Location','southwest');
        axis([0 80 0 1.5]);
        title('Spin down against matter at the ISCO','fontsize',FS)
      subplot(212);
        y=Egw./Erot_k;x=a_k;plot(x,y);grid on;
        ylabel('{\Delta E /E_{rot}(0)}','FontSize',FS);
        ylabel('Efficiency','FontSize',FS)
        xlabel('initial a/M','FontSize',FS);
        axis([0 1 0 0.7]);
        clear dEdf;dEdf(:,1)=f;dEdf(:,2)=(fM*dM)./df; %dEdf scaled by fM
      if(kk==kkmx)
        print -djpeg PhaseIIIa.jpg;
        print -depsc PhaseIIIa.eps;
        print -dpdf PhaseIIIa.pdf;
      end
   end
  
end

%PLOT BPJ LIGHT CURVE CHARACTERISTICS
figure(40*fM+2);
subplot(221);
  Lj=Lj;Lj=Lj/max(Lj);[p,q]=max(Lj);
  plot(T,Lj);grid on; hold on;box on;plot(T(q),Lj(q),'o');
  %legend(['M_H=' num2str(fM) 'M_{sun}'],['a/M = ' num2str(aa(q),4)]);
  legend(['a/M = ' num2str(aa(q),4)]);
  ylabel('L_{BPJ} (a.u.)','FontSize',14);xlabel('time [s]','FontSize',14);
subplot(222);
  thetaH=sqrt(z);thetaH=thetaH/max(thetaH);
  plot(T,thetaH);grid on;hold on;box on;ylabel('\theta_H/max(\theta_H)','FontSize',14);xlabel('time [s]','FontSize',14);
  axis([0 80 0.2 1]);
subplot(223);
  plot(aa,Lj);grid on;hold on;xlabel('a/M','FontSize',14);ylabel('L_{BPJ} (a.u.)','FontSize',14);
subplot(224);
  plot(thetaH,Lj);grid on;hold on;xlabel('(\theta_H/max(\theta_H)','FontSize',14);ylabel('L_{BPJ} (a.u.)','FontSize',14);
  print -djpeg PhaseIIIb.jpg;
  print -depsc PhaseIIIb.eps;
  print -dpdf PhaseIIIb.pdf

%NORMALIZED OUTPUT (M-INIT=1MSolar)
clear out;out(:,1)=T;out(:,2)=M;out(:,3)=J;out(:,4)=A;out(:,5)=f*fM;out(:,6)=Lj;
save fort.990 out -ascii;
save fort.102 out -ascii;

%SCALING SN2015L

  figure(50)

  l=length(t);u=1.5*(t-12);V=-23.75-2.5*log10(Lj);
  I=500:l;J=1:500;La=Lj(l);
  plot(u(I),V(I),'b','LineWidth',4);grid on;hold on;
  plot(u(J),V(J),'b--','LineWidth',2);
  dm=2.5*log10(1/La);
  axis ij;axis([-40 90 -24.2 -20.0]);
  legend(['\Deltam = ' num2str(dm,4)]);
  
  clear out;out(:,1)=t;out(:,2)=Lj;out(:,3)=u;out(:,4)=V;
  save Lj.txt out -ascii;
  
  print -djpeg PhaseIIIc.jpg;print -depsc PhaseIIIc.eps;print -dpdf PhaseIIIc.pdf;

t=t0;
load fort.102;out=fort; 
  T3=out(:,1);
  M3=out(:,2);J3=out(:,3);
  L3=out(:,4);f3=out(:,5);
  a3=J3./M3.^2;L3=length(T3);
%--------------------------------------------------------------------------
%   MERGE PART I-III
%--------------------------------------------------------------------------
clear Tp1 ap1 Fp1 Tp2 ap2 Fp2;
for k=1:length(Pp1(:,1))
  if(Pp1(k,1)<=max(P1(:,1)))
     M0=Pp1(k,5);a0=Pp1(k,7);i0=0;for i=1:N,if(a_i(i)<a0)i0=i;end;end
     a2=a_i(i0:N);z2=z_i(i0:N);M2=M0*sqrt(z_i(i0)./z2);
     N1=0;for i=1:length(M2),if(M2(i)<1)N1=i;end;end;
     a2=a2(1:N1);M2=M2(1:N1);
       l=length(M2);T0=t(Pp1(k,3)).^(3/2);T=T0+20*(M2-M2(1)); 
       fs=M2(1)/M0;F=fs./M2./(zF(a2).^(3/2)+a2);
       Tp1(k)=max(T);ap1(k)=max(a2);Fp1(k)=max(F);Mp1(k)=max(M2);
  end
end   
figure(6);
for k=1:length(P1(:,1))
  M0=P1(k,5);a0=P1(k,7);i0=0;for i=1:N,if(a_i(i)<a0)i0=i;end;end
  a2=a_i(i0:N);z2=z_i(i0:N);M2=M0*sqrt(z_i(i0)./z2);
  N1=0;for i=1:length(M2),if(M2(i)<1)N1=i;end;end;
  a2=a2(1:N1);M2=M2(1:N1);
    i0=0;for i=1:L3,if(a3(i)>max(a2))i0=i;end;end;i0=i0+1;
    l=length(M2);T0=t(P1(k,3)).^(3/2);T=T0+20*(M2-M2(1));
    fs=M2(1)/M0;F2=fs./M2./(zF(a2).^(3/2)+a2);Fx=max(F2);
  subplot(311);plot(T,M2,'b');
  subplot(312);plot(T,a2,'b');
  subplot(313);plot(T,F2,'b');
    L=length(line1);line1(L+1:L+l,:)=[T' a2' F2' M2'];
    T0=T(l);l=length(i0:L3);
    T=T0+T3(i0:L3)-T3(i0);A3=a3(i0:L3);m3=M3(i0:L3)/M3(i0)*max(M2);
    F3=1./M3(i0:L3)./(zF(a3(i0:L3)).^(3/2)+a3(i0:L3));F3=F3*Fx/F3(1);
    S =[T(1) m3(1) A3(1) F3(1)];
  subplot(311);plot(T,m3,'b','linewidth',3);plot(T(l),m3(l),'bs','MarkerSize',10);
  subplot(312);plot(T,A3,'b','linewidth',3);plot(T(l),A3(l),'bs','MarkerSize',10);
  subplot(313);plot(T,F3,'b','linewidth',3);plot(T(l),F3(l),'bs','MarkerSize',10);
    L=length(line1);line1(L+1:L+l,:)=[T A3 F3 m3];
end
subplot(311);axis([1 100 0 1]);ylabel('M/M_0','FontSize',16);
subplot(312);axis([1 100 0.2 1]);ylabel('a/M','FontSize',16); 
subplot(313);axis([1 100 0.05 1.2]);ylabel('M_0\Omega_{ISCO}','FontSize',16); 
xlabel('time (a.u.)','FontSize',16);grid on;
subplot(311);legend('Black hole birth','Direct accretion','Bardeen accretion','Spin down against ISCO','Black hole remnant','Location','southeast');
subplot(311);
  semilogx(B(1),B(2),'ob','MarkerSize',10);
  semilogx(S(1),S(2),'^b','MarkerSize',10);
subplot(312);
  semilogx(B(1),B(3),'ob','MarkerSize',10);
  semilogx(S(1),S(3),'^b','MarkerSize',10);
subplot(313);
  semilogx(B(1),B(4),'ob','MarkerSize',10);
  semilogx(S(1),S(4),'^b','MarkerSize',10);
  text(1.5,0.95,'f_{GW,0}','fontsize',16);text(  3,0.50,'f_{GW,1}','fontsize',16);
  text(  9,0.75,'f_{GW,2}','fontsize',16);text( 50,0.30,'f_{GW,3}','fontsize',16);

figure(1);print -djpeg BHGrowtha.jpg;print -depsc BHGrowtha.eps;
  print -dpdf  BHGrowtha.pdf;
figure(2);print -djpeg BHGrowthb.jpg;print -depsc BHGrowthb.eps;
  print -dpdf  BHGrowthb.pdf;
figure(31);print -djpeg BHGrowthc.jpg;print -depsc BHGrowthc.eps;
  print -dpdf  BHGrowthc.pdf;
figure(32);print -djpeg BHGrowthd.jpg;print -depsc BHGrowthd.eps;
  print -dpdf  BHGrowthd.pdf;
figure(6);print -djpeg BHGrowthe.jpg;print -depsc BHGrowthe.eps;
  print -dpdf  BHGrowthe.pdf;
  
save line1.txt line1 -ascii;

keyboard
