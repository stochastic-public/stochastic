#! /bin/bash

#
# Purpose: check that the background jobs have successfully run
# Author: mabizoua@lal.in2p3.fr
#

cd ~jialun.luo/stamp_as_v3/STAMPAS/BKG/Start-931081116_Stop-971614699_ID-1
#cd /home/prestegard/stamp_as_v3/STAMPAS/BKG/Start-816065659_Stop-877591542_ID-1/

job_nb=`tail -8 stamp_allsky_bkg.dag | grep JOB | awk '{print $2}'`
echo $job_nb

cd logs
err_missing=0
out_missing=0
err_nb=0
out_nb=0

for i in `seq 1 ${job_nb}`; do
    n=$((i%1000))
    if [ $n -eq 0 ]; then
	echo "Job checked " $i " / " ${job_nb}
    fi
    file_err=err_allsky_bkg.${i}
    file_out=out_allsky_bkg.${i}

    if [ ! -f ${file_err} ]; then
	echo "file " ${file_err}" does not exist"
	rc=$((err_missing++))
    else
	file_err_lines_nb=`wc -l ${file_err} | awk '{print $1}'`
	if [ ${file_err_lines_nb} -gt 2 ]; then
#	    echo $file_err " is larger than expected"
	    err_sum=`grep -i Error ${file_err}`
	    if [ ${#err_sum} -gt 0 ]; then
		echo $file_err " contains error messages"
		rc=$((err_nb++))
		cat $file_err
	    fi
	fi

    fi

    if [ ! -f ${file_out} ]; then
	echo "file " ${file_out}" does not exist"
	rc=$((out_missing++))
    else
	out_sum=`grep -i Error ${file_out}`
	if [ ${#out_sum} -gt 0 ]; then
	    echo $file_out " contains error messages"
	    rc=$((out_nb++))
	    cat $file_out
	fi

	a=`grep subJob $file_out | awk '{print $2}' `
	subjob_nb=`echo $a | awk '{print $NF}'`

	subjob_nb_exp=`grep $i ../stamp_allsky_bkg.dag.dagman.out | grep submitting | grep "'$i -a mem" | cut -d "=" -f 15 | cut -d "-" -f 1 | cut -b 4- | tail -1`

	if [ $subjob_nb -lt $subjob_nb_exp ]; then
	    echo ' The number of subjobs is less than it should be: ' $subjob_nb '/' $subjob_nb_exp
	fi

	
    fi

done

echo "Number of error in err files: " ${err_nb}
echo "Number of error in out files: " ${out_nb}
echo "Number of missing err files: " ${err_missing}
echo "Number of missing out files: " ${out_missing}
