function quick_vp_calc2
% function quick_vp_calc2

% GW emission frequency
%f = 200;
%f = 162;

% convert 1 Msun to ergs using E=mc^2 and (1e7 ergs/J) and msun=2e30 kg
msun = 2e30 * (3e8)^2 * 1e7;

% vanPutten says that about 0.1 msun is converted into GWs.
% factor of 2.5 beaming for face-on emission (see note by Patrick Sutton)
% http://tinyurl.com/boeqat5
EVP = 2.5 * 0.1 * msun;

% best fluence limit in ergs/cm^2
% March 25, 2013 (GRB070611) adi-b including calibration error
F0 = 3.53;

% reference frequency: if this is different from f (GW emission frequency 
% defined above), then the fluence will be scaled by R to show how the 
% sensitivity is affected by the detector's noise spectrum.  There is an 
% an additional factor of (f/fref)^2 to take into account how strain varies 
% with frequency for a fixed energy budget. 
fref = 162;

% strain power sensitivity ratio
g = load('LIGOsrdPSD.txt');
gf = g(:, 1);
gP = g(:, 2);

% R compares the detector strain power sensitivity at f to that at fref
R =@(f) interp1(gf, gP, f) / interp1(gf, gP, fref);

% convert to ergs/Mpc^2
Mpc2cm = 3.09e24;
F0 = F0 * Mpc2cm^2;

% scale fluence by sensitivity of detector and according to frequency
% dependence of fluence
F =@(f) F0 * (f/fref).^2 .* R(f);

% Calculate expected detection distance
%   F = E/4*pi*D^2 implies
%   D = sqrt(E/4*pi*F)
D =@(f) sqrt(EVP./(4*pi*F(f)));

% print results to screen
fprintf('predicted detection distance: D=%1.1e Mpc.\n', D(fref));

% plot
ff = 100:1200;
figure;
loglog(ff, D(ff));
xlabel('f (Hz)');
ylabel('D^{90%} (Mpc)');
axis([ff(1) ff(end) 1 100]);
grid on;
pretty;
print('-dpng', 'quick_vp_calc2');
print('-depsc2', 'quick_vp_calc2');

return
