
* s5vsr1_swift_lgrb.txt provided by Peter Raffai from S5-VSR1 burst analysis.

* Run find_triggers to generate astro, job, and trigger files.  See the 
beginning of find_triggers for additional details.

* The triggers created by this script are the "new" set of triggers.  This list
includes all of the old triggers plus some new ones.  The old trigger files are
here

        ../upperlimits/jobfile_s5_swift_1000s_v2.txt
        ../triggers_s5_swift_1000s_v2.txt

The old list is smaller because we applied different cuts on epsilon.  In order
to go between the old and new lists, there is a script in upperlimits/ called
relocate which maps the the two lists.

* extra_H2L1_triggers.pl finds GRB triggers in H2L1 jobfile that are not 
already part of the H1L1 trigger list.

* extra_H2L1_triggers.out is the output list from this script.

* extra_H2L1_followup is then used to determine the R value for the H2L1
triggers.  Of the three H2L1 triggers, two were found to have R>1 and were 
therefore kept.
