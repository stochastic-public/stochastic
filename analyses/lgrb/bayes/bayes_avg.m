function [ul, ul_distance, F, a, posterior] = bayes_avg(matappspath)
% function [ul, ul_distance, F, a, posterior] = bayes_avg(matappspath)
% Eric Thrane: combine 

% temporary settings
root = '/home/stamp/upperlimits_new/lgrb/plots/';
str = 'lgrb_v2';
%wave = 'adi_B';
%wave = 'adi_C';
wave = 'adi_D';

% where the alpha posterior plots live for this waveform
dirout = dir([root wave '/']);

% define domain for alpha calculation
astep = 1e-09;
amax = 1e-5;
a = 0:astep:amax;

% diagnostic plot
figure;

% ignore the first two directories, which are always . and ..
for ii=1:size(dirout,1)-2
  gps(ii) = str2num(dirout(ii+2).name);
  % load data
  q = load([root wave '/' num2str(gps(ii)) '/limits_' str '.mat']);
  % extract PDF
  x = q.alpha;
  fx = q.prob;
  % resample PDF
  fa = interp1([x amax], [fx' 0], a);
  fa = fa/sum(fa);
  % diagnostic plot
  plot(a, fa, 'b');
  hold on;
  % make sure the prior is defined
  if ii==1
    posterior = fa;
  % calculate running posterior
  else
    posterior = prior .* fa;
    posterior = posterior / sum(posterior);
  end
  % the current posterior becomes the prior for the next measurement
  prior = posterior;
end

% finish diagnostic plot
plot(a, posterior, 'r', 'linewidth', 2);
axis([0 6e-7 0 1.1*max(posterior)]);
pretty;
print('-dpng', ['bayes_avg_' wave]);

% calculate upper limit
cl = 0.90;
cdf = cumsum(posterior);
ul = min(a(cdf>cl));
ul_distance = sqrt(1/ul);

% waveforms created for 10 kpc -- convert to Mpc
fprintf('scaling distance assuming 10 kpc waveform\n');
ul_distance = ul_distance * (10 / 1000);

% calculate fluence (units = ergs/cm^2)
F = cal_fluence([matappspath '/analyses/lgrb/benchmark/' wave '.dat'], ul);

% save output file
save(['bayes_avg_' wave '.mat']);

return
