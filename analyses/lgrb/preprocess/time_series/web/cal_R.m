function R = cal_R(params, trigger_gps)
% function R = cal_R(params, trigger_gps)

% calculate epsilon (cross-power sensitivity), which we will use below to scale
% the injection amptlitudes
[det1 det2] = ifo2dets(params);
g = calF(det1, det2, trigger_gps, [params.ra params.dec], params);
epsilon = abs(g.F1p .* g.F2p + g.F1x .* g.F2x)/2;

% While we have this information handy, calculate R, which is LHS/RHS of
% Eq. A4 in the Feb 12 draft of the paper.  R is a measure of how the cross-
% power compares to the auto-power.  This code does not do anything with R, so 
% this is for diagnostic purposes.
eps11 = abs(g.F1p.^2 + g.F1x.^2);
eps22 = abs(g.F2p.^2 + g.F2x.^2);
R = 4 * epsilon / sqrt(eps11*eps22);

return
