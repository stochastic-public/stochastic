#!/usr/bin/perl
#
$NNodes=599;
$postfix = "";
$user="ethrane";
$prefix = "HL-SID_nspi9_df1_dt1_zl";
$destination = "/home/ethrane/HL-SID_nspi9_df1_dt1_zl/";

$n=1;
while ($n<=$NNodes) {
    print "searching node $n\n";
    foreach $file (</data/node$n/$user/$prefix*$postfix>) {
	$filestub=$file;
	$filestub =~ s/.*($prefix.*)/\1/;
	$oldfile=$destination.$filestub;
	print "\n$oldfile\n";
	if (!(-e $oldfile)) {
	    print "cp -r $file $destination\n";
	    system "cp -r $file $destination";
	}
	else {
        # If the directory already exists copy contents of directory
        chomp($file);
        $stub = $file;
        $stub =~ s/.*$user(.*)/\1/;
        print "cp $file/*.mat $destination$stub/\n";
        system "cp $file/*.mat $destination$stub/";
	}
    }
    $n=$n+1;
}

print "Done.\n";
    
