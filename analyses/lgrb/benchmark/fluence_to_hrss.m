function fluence_to_hrss
% function fluence_to_hrss
% hrss ~ sqrt(alpha) ~ sqrt(F)
% Thus, hrss(alpha) = sqrt( F(alpha)/F(1) ) * hrss(1)

% obtain fluence and hrss for alpha=1
[F_B hrss_B] = cal_fluence('adi_B.dat', 1, false, false);
[F_C hrss_C] = cal_fluence('adi_C.dat', 1, false, false);
[F_D hrss_D] = cal_fluence('adi_D.dat', 1, false, false);

% hard-code best fluence limits from paper (updated April 12, 2013)
F_B0 = 3.43;
F_C0 = 4.25;
F_D0 = 14.6;

% write fluence limits as = factor * sqrt( F(alpha)/F(alpha0) )
factor_B = hrss_B * sqrt(F_B0 / F_B);
factor_C = hrss_C * sqrt(F_C0 / F_C);
factor_D = hrss_D * sqrt(F_D0 / F_D);

% print results
fprintf('write fluence limits as = factor * sqrt( F(alpha)/F(alpha0)\n');
fprintf('factor = %1.2e, %1.2e, %1.2e\n', factor_B, factor_C, factor_D);

return
