function stoch_out = run_clustermap_ul(job, trial, waveform, matappspath)
% function stoch_out = run_clustermap_ul(job, trial, waveform, matappspath)
%
% Eric Thrane: this script is a wrapper for clustermap.  It performs power
% injections.  The output files are saved in a directory marked by the trigger
% GPS.

% condorize
stoch_out.start=true;
job = strassign(job);
trial = strassign(trial);
if trial==0
  return;
end

% diagnostic print
fprintf('********\nWAVEFORM = %s\n********\n', waveform);

% output file prefix and location
% lgrb_v3 run out of ethrane/ after review changes
str ='lgrb_v3';
output_dir = '/home/ethrane/stamp_results/upperlimits';

% initialize default parameters
params = stampDefaults;

% point to injection file
if strcmp(waveform,'none')
   params.powerinj = false;
else
   params.powerinj = true;
   params.injfile = [matappspath '/analyses/lgrb/benchmark/' waveform '.mat'];
end

% jobfile and triggerfile for Swift triggers with no missing data
jobsFile = [matappspath '/analyses/lgrb/triggers/jobfile_0.txt'];

% load trigger file for triggers with no missing data
q = load([matappspath '/analyses/lgrb/triggers/triggers_0.txt']);

% get ra and dec from trigger file
params.ra = q(job, 3);
params.dec = q(job, 4);
fprintf('(ra,dec)=(%1.2f,%1.2f)\n', params.ra, params.dec);

% get trigger time from trigger file
trigger_gps = q(job, 2);

% input mat directory
% time-shifted data
%params.inmats = ...
%  '/home/ethrane/HL-SID_nspi9_df1_dt1_ts1/HL-SID_nspi9_df1_dt1_ts1';
% zero-lag data
params.inmats = ...
  '/home/ethrane/HL-SID_nspi9_df1_dt1_zl/HL-SID_nspi9_df1_dt1_zl';

% frequency range--------------------------------------------------------------
params.fmin = 100;
params.fmax = 1200;

% do not save mat files or plots
params.saveMat=false;
params.savePlots=false;

% make sure output directories exist
% e.g., ~/stamp_results/upperlimits/adi_D/829887393/
% first, check the 'base" directory named with the waveform
output_file_path_base = [output_dir '/'  waveform];
if ~exist(output_file_path_base)
 system(['mkdir ' output_file_path_base]);
end
% next check the subdirectory named with the GPS time
output_file_path = [output_file_path_base '/'  num2str(trigger_gps)];
if ~exist(output_file_path)
 system(['mkdir ' output_file_path]);
end

% notches
params = mask_S5H1L1_1s1Hz(params);

% remove notches outside of observation band
params.StampFreqsToRemove = ...
  params.StampFreqsToRemove(params.StampFreqsToRemove>=params.fmin);
params.StampFreqsToRemove = ...
  params.StampFreqsToRemove(params.StampFreqsToRemove<=params.fmax);

% random seed set with GPS time
params.seed = -1;
params.jobNumber=job;

% search parameters
params = burstegardDefaults(params);

% glitch cut
params.glitchCut = true;
params.doCoincidentCut = true;

% load and parse jobfile
f = load(jobsFile);
dur = f(job, 4);
start = f(job, 2);
stop = start+dur;
fprintf('trigger GPS = %i\n', trigger_gps);
fprintf('job start GPS = %i\n', start);

% one trial per alpha value
params.inj_trials = 1;

% calculate expected antenna factors in order to get a good range of values
% for injection amplitude
params.ifo1 = 'H1';
params.ifo2 = 'L1';
% set physical constant for use in calF (this will be defined again in 
% clustermap once paramCheck is called, but that is unwieldy to do here)
params.c = 299792458;
% calculate epsilon (cross-power sensitivity), which we will use below to scale
% the injection amptlitudes
[det1 det2] = ifo2dets(params);
g = calF(det1, det2, trigger_gps, [params.ra params.dec], params);
epsilon = abs(g.F1p .* g.F2p + g.F1x .* g.F2x)/2;

% While we have this information handy, calculate R, which is LHS/RHS of
% Eq. A4 in the Feb 12 draft of the paper.  R is a measure of how the cross-
% power compares to the auto-power.  This code does not do anything with R, so
% this is for diagnostic purposes.
eps11 = abs(g.F1p.^2 + g.F1x.^2);
eps22 = abs(g.F2p.^2 + g.F2x.^2);
R = 4 * epsilon / sqrt(eps11*eps22);
fprintf('epsilon = %1.2e\n', epsilon);
fprintf('R = %1.2e\n', R);

% set the range for injection amplitudes based on antenna factors
% also each waveform gets its own correction
if strcmp(waveform,'adi_B')
  params.alpha_min = 2 * (1e-8 / epsilon);
  params.alpha_max = 2 * (5e-7 / epsilon);
elseif strcmp(waveform,'adi_C')
  params.alpha_min = 0.15 * (1e-8 / epsilon);
  params.alpha_max = 0.15 * (5e-7 / epsilon);
elseif strcmp(waveform,'adi_D')
  params.alpha_min = 0.9 * (1e-8 / epsilon);
  params.alpha_max = 0.9 * (5e-7 / epsilon);
end

% define output file and make sure it doesn't already exist
ofile = [output_file_path '/' str '_' num2str(trigger_gps) '_' ...
  num2str(trial) '.mat'];
if exist(ofile)
  fprintf('%s already exists; moving on\n', ofile);
else
  % run clustermap
  fprintf('running clustermap...\n');
  stoch_out=clustermap(params, start, stop);
  % save output files
  save(ofile, 'stoch_out');
end

return;
