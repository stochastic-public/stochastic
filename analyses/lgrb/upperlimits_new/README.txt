#######
This code has been superseded by the code here: 
     analyses/lgrb/preprocess/time_series/
#######

%--First, perform the injection study------------------------------------------
* Generate mat files that record SNR for different values of injection 
amplitude alpha

  call_run_clustermap_ul (formerly stamp_run_all)

which calls

  run_clustermap_ul (formerly) stamp_clustermap_wrapper.m

The output mat files are kept in directories labeled by the waveform and the 
trigger GPS time, e.g.,

        /home/ethrane/stamp_results/upperlimits/none/833758378/

%--Convert injection results into upper limits---------------------------------
* Next, harvest the mat files from the injection code.  Then,

  run_cal_limit (formerly stamp_run_limits_all.m)

which calls

  cal_limit     (formerly stamp_limits.m)

for each job and waveform.  The output files are saved in directories named 
after the start time of the trigger analyzed.  These output files are recorded
here:

  /home/ethrane/stamp_results/upperlimits/plots/

In order to generate the upper-limit table for the paper, run

   ul_table_for_paper.m

which makes ul_table.tex

%--Make a webpage with the results---------------------------------------------
* run quick_summary.html in web2/

%------------------------------------------------------------------------------
% OLD!!!!!
%--Make a webpage with the results---------------------------------------------
* stamp_run_summary_page.m: calls stamp_lgrb_page for each waveform before
calling stamp_summary_page_all.  This is the webpage routine that calls all the
others.

* stamp_lgrb_page.m: makes the webpage for a specific job and waveform

* stamp_summary_page_all.m: makes a summary of the summary pages (the grand
summary page)

* stamp_summary_page.m: makes a summary for each waveform

