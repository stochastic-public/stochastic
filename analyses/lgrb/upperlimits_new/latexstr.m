function str = latexstr(x)
% function str = latexstr(x)

prefactor = x / 10^floor(log10(x));
exponent = floor(log10(x));
str = sprintf('$%1.1f\\times10^{%i}$', prefactor, exponent);
return
