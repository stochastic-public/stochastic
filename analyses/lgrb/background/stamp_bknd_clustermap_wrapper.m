function stoch_out = stamp_bknd_clustermap_wrapper(job, matappspath);
% function stoch_out = stamp_bknd_clustermap_wrapper(job, matappspath);
% Eric Thrane: this script is a wrapper for clustermap.
% mods by Michael Coughlin

% output file prefix
str ='lgrb_v2';

% output directory
output_dir = '/home/stamp/background';

% initialize default parameters
params = stampDefaults;
params.powerinj = false;

% initialize variables
stoch_out.start=true;
job = strassign(job);
if job==0
  return;
end

% load pseudo job file
jobsFile = [matappspath '/analyses/lgrb/background/pseudojobs.txt'];

% it does not matter what direction we look for the background study, so use
% our standard default direction
params.ra = 6;
params.dec = 30;

% input mat directory
params.inmats = '/home/stamp/stampmatfiles/HL-SID_nspi9_df1_dt1_ts1';

% frequency range
params.fmin = 100;
params.fmax = 1200;

% override default parameters--------------------------------------------------
params.saveMat=false;
output_mat_path = [output_dir '/mats'];
params.outputfilename = [output_mat_path '/' str '_' num2str(job)];
params.savePlots=false;
output_plot_path_base = [output_dir '/plots'];
% make sure output directories exist
if ~exist(output_plot_path_base)
 system(['mkdir ' output_plot_path_base]);
end
params.plotdir = [output_plot_path_base '/' num2str(job) '/'];
if ~exist(params.plotdir)
   system(['mkdir ' params.plotdir]);
end

% use S5 mask
params = mask_S5H1L1_1s1Hz(params);

% set the seed
params.seed = job*1e5;

% clustering parameters
params = burstegardDefaults(params);
params.burstegard.findtrack = true;
params.burstegard.rr = 50;

% glitch cuts
params.glitchCut = true;
params.glitchCut = 8;
params.DQmatfile = [matappspath '/stamp/input'];
params.doCoincidentCut = 1;

% tell clustermap to run in background study mode
params.bkndstudy = 'true';
params.bkndstudydur = 1500;

% fixed sensitivity: this does not matter for a background study because
% we only care about SNR, not Y and sigma.  The same will not be true for the
% sensitivity study.
params.fixAntennaFactors = true;
fprintf('params.fixAntennaFactors = %i\n', params.fixAntennaFactors);

% load and parse job file
jobsFileDataAll = load(jobsFile);
jobsFileData = jobsFileDataAll(job,:);
index = jobsFileData(1); start = jobsFileData(2);
stop = jobsFileData(3); duration = jobsFileData(4);

% run clustermap
fprintf('running clustermap...\n');
stoch_out=clustermap(params, start, stop);

% save output files
output_file_path = [output_dir '/results'];
save([output_file_path '/' str '_' num2str(job) '.mat'],'stoch_out');

return;
