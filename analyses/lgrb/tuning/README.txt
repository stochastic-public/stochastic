
* First, run run_clustermap_noise to perform a background study.  This wrapper
code will generate a bunch of mat files with stoch_out.max_SNR.  Harvest these
files into subdirectories in mats/.  This is performed for several choices of
parameters.

* Once the mat files are created using run_clustermap_noise, run find_threshold
in order to determine the FAP=0.1% SNR threshold.  find_threshold.m will save a
mat file recording the result.

* Then, run detection_distance.m.  This script will read in the mat file output
by find_threshold and then perform a series of injections to determine the
distance at which a signal can be detected with FAP=0.1% and FDP=50%.  Note 
that detection distance pulls the clustering parameter being tested from the
first mat file created by run_clustermap_noise.m.  Before studying a new 
parameter it is *essential* to modify the script *in two places so that the R
variable refers to the current clustering variable.
