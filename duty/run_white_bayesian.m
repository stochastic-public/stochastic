function run_duty(gpsStart,gpsEnd,amp,T,doPSDCut)

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

fs = 256;
fref = 50;
fmin = 10;
fmax = 100;

frames = gpsStart:(T/2):gpsEnd;

outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_white/%.10f/%d/%d',amp,T,doPSDCut);
if ~exist(outputDir)
   system(['mkdir -p ' outputDir])
end

params.outputDir = outputDir;
params.spectraFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/flat.txt';
params.waveformFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/flat.txt';
params.T = T;
params.amp = amp;
params.gpsStart = gpsStart;
params.gpsEnd = gpsEnd;
params.fs = fs;
params.fref = fref;
params.fmin = fmin;
params.fmax = fmax;
params.doPlots = 1;
params.doPSDCut = doPSDCut;

matFile = [params.outputDir '/data.mat'];
data_out = load(matFile,'data','ys','sigmas2','pgws');
data = data_out.data;

N = length(data_out.ys);

likelihood = @bayesian_logL_multi;
model = @powerlawPSD_model;
prior = {'a', 'uniform', -10, 10, 'fixed'; ...
         'k', 'uniform', -6, 6, 'fixed'};
num = 100;
liktot = zeros(num,num);
%N = 1;

extraparams = {};
[size_prior_x,size_prior_y] = size(prior);

parnames = prior(:,1);
parvalsmin = cell2mat(prior(:,3));
parvalsmax = cell2mat(prior(:,4));

for i = 1:length(parnames)
   prior{i,6} = linspace(parvalsmin(i),parvalsmax(i),num);
end

doRun = 1;
matFile = [params.outputDir '/multinest.mat'];
if doRun
   count = 0;
   for ii = 1:N
   
      if mod(ii,100) == 1
         fprintf('%d/%d\n',ii,N);
      end
   
      ff = data.f;
      deltaF = ff(2) - ff(1);
      y1 = data.y1_all(:,ii);
      y2 = data.y2_all(:,ii);
      y1y2 = data.y1y2_all(:,ii);
      psd1 = data.psd1_all(:,ii);
      psd2 = data.psd2_all(:,ii);
      psd1_ave = data.psd1_ave;
      psd2_ave = data.psd2_ave;
      psd1_signal = data.psd1_signal_all(:,ii);
      psd2_signal = data.psd2_signal_all(:,ii);
      w1w2bar = data.w1w2bar;
      w1w2squaredbar = data.w1w2squaredbar;
      w1w2ovlsquaredbar = data.w1w2ovlsquaredbar;
   
      y1y1 = y1.*conj(y1);
      y1y2 = y1.*conj(y2);
      y2y1 = y2.*conj(y1);
      y2y2 = y2.*conj(y2);
      orf = ones(size(ff));
   
      data_bayesian = {};
      data_bayesian{1} = T*psd1_ave;
      data_bayesian{2} = T*psd2_ave;
      data_bayesian{3} = y1y1';
      data_bayesian{4} = y1y2';
      data_bayesian{5} = y2y1';
      data_bayesian{6} = y2y2';
      data_bayesian{7} = orf;
      data_bayesian{8} = ff;
   
      lik = zeros(num,num);
      for i = 1:num
         %fprintf('%d\n',i);
         for j = 1:num
            parvals = {prior{1,6}(i) prior{2,6}(j)};
            %logL = bayesian_logL(data, model, parnames, parvals);
            logL = bayesian_logL_multi(data_bayesian, model, parnames, parvals);
            lik(i,j) = logL;
         end
      end
      liktot = liktot + lik;
   
      count = count + 1;
   end
   
   lik = liktot;

   save(matFile,'prior','lik');
else
   %load(matFile);
   load(matFile,'prior','lik');
end

if params.doPlots
  
   [C,I] = max(lik(:));
   [I1,I2] = ind2sub(size(lik),I);
   lik_max = lik(I1,I2);

   a = prior{1,6}(I1);
   k = prior{2,6}(I2);
   powerlaw_data = powerlaw(ff,a,k);

   [junk,index] = max(lik(:));
   fprintf('a: %.5f, k: %.5f, likelihood: %.5e\n',a,k,lik(index));

   plotName = [params.outputDir '/omega_best'];
   figure;
   %loglog(ff,real(rbartilde1.*rbartilde2),'k--');
   loglog(ff,psd1_signal,'k--');
   hold on
   loglog(ff,psd1,'r:');
   loglog(ff,powerlaw_data,'b');
   hold off
   xlim([0 256]);
   %ylim([1e5 1e15]);
   xlabel('Frequency [Hz]');
   ylabel('S_{h}');
   print('-dpng',plotName);
   print('-depsc2',plotName);
   print('-dpdf',plotName);
   close;

   xlab = get_name(prior{1,1});
   ylab = get_name(prior{2,1});

   %lik = lik-max(lik(:));
   lik = real(lik);
   vmax = max(lik(:));
   if vmax > 0
      vmin = 0.1*vmax;
   else
      vmin = 2*vmax;
   end

   plotName = [params.outputDir '/contour'];
   figure()
   imagesc(prior{1,6},prior{2,6},lik');
   set(gca,'YDir','normal')
   set(gca,'XScale','lin');
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   set(get(cbar,'title'),'String','log10(Likelihood)');
   caxis([vmin vmax]);
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   print('-dpdf',plotName);
   close;

   [Xq, Yq] = meshgrid(prior{1,6},prior{2,6});
   Vq = lik';

   plotName = [params.outputDir '/loglikelihood'];
   figure()
   pcolor(Xq,Yq,Vq);
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   caxis([vmin vmax]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   print('-dpdf',plotName);
   close;

   norm = max(lik(:));
   lik = exp(lik - norm);
   Vq = lik';

   plotName = [params.outputDir '/likelihood'];
   figure()
   pcolor(Xq,Yq,Vq);
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   caxis([vmin vmax]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   print('-dpdf',plotName);
   close;

   plotName = [params.outputDir '/likelihood_cbar'];
   figure()
   pcolor(Xq,Yq,Vq);
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   caxis([1-1e-11 1.0]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   print('-dpdf',plotName);
   close;

   zmax = max(lik(:));
   zvals = 0:zmax/2000:zmax;
   znorm = sum(lik(:));
   for ll=1:length(zvals)
      prob(ll) = sum(sum(lik(lik>=zvals(ll))));
   end
   prob = prob/znorm;
   %prob = fliplr(prob);
   zvals68 = zvals(abs(prob-0.68)==min(abs(prob-0.68)));
   zvals95 = zvals(abs(prob-0.95)==min(abs(prob-0.95)));
   zvals99 = zvals(abs(prob-0.99)==min(abs(prob-0.99)));
   zcontours = [zvals68(1) zvals95(1) zvals99(1)];
   zvals10 = zvals(abs(prob-0.10)==min(abs(prob-0.10)));
   zcontours = [zvals68(1) zvals95(1) zvals99(1)];
   %[C, h1] = contour(aa, epsilon, lik, zvals95);

   plotName = [params.outputDir '/likecontour'];
   figure;
   [C, h] = contour(Xq, Yq, Vq, zcontours(1),'k');
   hold on
   [C, h] = contour(Xq, Yq, Vq, zcontours(2),'b');
   [C, h] = contour(Xq, Yq, Vq, zcontours(3),'r');
   %[C, h] = contour(Xq, Yq, Vq, zcontours(4),'m');
   hold off
   xlabel(xlab);
   ylabel(ylab);
   %cbar = colorbar;
   %set(get(cbar,'ylabel'),'String','Probability');

   %bayesian_get_injplot(analysisType,dataSet);

   legend('68 CL','95 CL','99 CL','True value','Location','northeast','Orientation','horizontal')
   %legend('10 CL','68 CL','95 CL','True value','Location','northeast','Orientation','horizontal')
   pretty;
   print('-dpng',plotName);
   print('-depsc2',plotName);
   print('-dpdf',plotName);
   close;
 
end

