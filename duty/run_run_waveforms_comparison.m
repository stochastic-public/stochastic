
m1s = 5:5:100;
m2s = 5:5:100;

fref = 40;
vals_nospin = NaN*ones(length(m1s),length(m2s));
vals_0p95 = NaN*ones(length(m1s),length(m2s));
for ii = 1:length(m1s)
   for jj = 1:length(m2s)
      m1 = m1s(ii); m2 = m2s(jj);

      if m1 > m2
         continue
      end

      outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_waveforms/bbh/%d_%d',m1,m2);

      filename = [outputDir '/data.mat'];
      if ~exist(filename)
         continue
      end
      data_out = load(filename);
      ff = data_out.ff; psdgw = data_out.psdgw;
      [junk,index] = min(abs(ff - fref));
      vals_nospin(ii,jj) = psdgw(index);

      outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_waveforms/bbh/%d_%d_0p95',m1,m2);

      filename = [outputDir '/data.mat'];
      if ~exist(filename)
         continue
      end
      data_out = load(filename);
      ff = data_out.ff; psdgw = data_out.psdgw;
      [junk,index] = min(abs(ff - fref));
      vals_0p95(ii,jj) = psdgw(index);

   end
end

outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_waveforms/bbh/comparison/');
if ~exist(outputDir)
   system(['mkdir -p ' outputDir])
end

[X,Y] = meshgrid(m1s,m2s);

figname = 'nospin';
figure;
pcolor(X,Y,vals_nospin);
xlabel('Mass [solar masses]');
ylabel('Mass [solar masses]');
h = colorbar;
ylabel(h, 'PSD');
%caxis([0 0.25]);
print('-dpng',[outputDir '/' figname '.png']);
print('-depsc2',[outputDir '/' figname '.eps']);
print('-dpdf',[outputDir '/' figname '.pdf']);
close;

figname = '0p95';
figure;
pcolor(X,Y,vals_0p95);
h = colorbar;
%caxis([0 0.25]);
ylabel(h, 'PSD');
xlabel('Mass [solar masses]');
ylabel('Mass [solar masses]');
print('-dpng',[outputDir '/' figname '.png']);
print('-depsc2',[outputDir '/' figname '.eps']);
print('-dpdf',[outputDir '/' figname '.pdf']);
close;

vals_ratio = vals_0p95 ./ vals_nospin;
figname = 'ratio';
figure;
pcolor(X,Y,vals_ratio);
h = colorbar;
caxis([1 1.75]);
ylabel(h, 'Ratio (\chi = 0.95 / \chi = 0)');
xlabel('Mass [solar masses]');
ylabel('Mass [solar masses]');
print('-dpng',[outputDir '/' figname '.png']);
print('-depsc2',[outputDir '/' figname '.eps']);
print('-dpdf',[outputDir '/' figname '.pdf']);
close;

index1 = find(m1s == 100);
index2 = find(m2s == 100);
vals_ratio(index1,index2)

