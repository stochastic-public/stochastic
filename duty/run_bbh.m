function run_duty(gpsStart,gpsEnd,rate,T,doPSDCut)

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

fs = 256;
fref = 50;
fmin = 10;
fmax = 100;

frames = gpsStart:(T/2):gpsEnd;

outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_bbh/%.10f/%d/%d',rate,T,doPSDCut);
if ~exist(outputDir)
   system(['mkdir -p ' outputDir])
end

params.outputDir = outputDir;
params.spectraFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd.txt';
params.waveformFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/duty/waveforms/SEOBNRv2_30_30.dat';
params.waveformFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/duty/waveforms/IMRPhenomB_30_30.dat';
%params.waveformFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/duty/waveforms/BNS.dat';
params.T = T;
params.rate = rate;
params.gpsStart = gpsStart;
params.gpsEnd = gpsEnd;
params.fs = fs;
params.fref = fref;
params.fmin = fmin;
params.fmax = fmax;
params.doPlots = 1;
params.doPSDCut = doPSDCut;

spectra = load(params.spectraFile);
spectra_f = spectra(:,1); spectra_h = spectra(:,2);
mc.transfer(:,1) = spectra_f;
mc.transfer(:,2) = spectra_h;

[t_all,r1_all,r2_all,r1_signal_all,r2_signal_all] = gen_data_bbh(params);

data = gen_psd(params,t_all,r1_all,r2_all,r1_signal_all,r2_signal_all);

ff = data.f;
deltaF = ff(2) - ff(1);
psd1_ave = data.psd1_ave;
psd2_ave = data.psd2_ave;
psd1_signal_ave = data.psd1_signal_ave;
psd2_signal_ave = data.psd2_signal_ave;
w1w2bar = data.w1w2bar;
w1w2squaredbar = data.w1w2squaredbar;
w1w2ovlsquaredbar = data.w1w2ovlsquaredbar;

z = 0.1;
m1 = 30.0;
m2 = 30.0;
%m1 = 1.4;
%m2 = 1.4;

Mtot = m1 + m2;
etha=m1*m2/(Mtot*Mtot);
nu1 = 404*((0.66389*etha*etha-0.10321*etha+0.10979)/0.125481)*(20./Mtot);
nu2 = 807.*((1.3278*etha*etha-0.20642*etha+0.21957)/0.250953)*(20./Mtot);
nu3 = 1153.*((1.7086*etha*etha-0.26592*etha+0.28236)/0.322668)*(20./Mtot);
sigma = 237.*((1.1383*etha*etha-0.177*etha+0.046834)/0.0737278)*(20./Mtot);
w1 = 1./nu1;
w2 = nu2^(-4/3)/nu1;
Kb = 7.8085e-20;

orientationFactor = 4/5;
orientationFactor = 1;

nu1
xi = 0;

LAL_MTSUN_SI = 4.9254923218988636432342917247829673e-6; %/**< Geometrized solar mass, s. = LAL_MSUN_SI / LAL_MPL_SI * LAL_TPL_SI */

dL = 1;
psdgw = zeros(size(ff));
for ii = 1:length(ff)
   f = ff(ii);
   v = (pi*Mtot*f*LAL_MTSUN_SI)^(1/3);
   if (f<nu1)
      g = f^(2./3);
      g = f^(2./3) * (1 + (-323/224 + 451*etha/168)*v^2 + xi*(27/8 - 11*etha/6)*v^3); 
   elseif (f<nu2)
      g = w1*f^(5/3);
   elseif (f<=nu3)
      g = w2*f*(f/(1.+((f-nu2)/(sigma/2.))*((f-nu2)/(sigma/2.))))^(2);
   else
      g = 0;
   end
   Mc   = ((m1*m2)^(3./5))/Mtot^(1/5);
   fLSO = 4397 / Mtot;
   psdgw(ii) = orientationFactor*((Mc^(5/3))/(dL*dL))*1*g/f^3;
end

dur = params.gpsEnd - params.gpsStart;
numinj = floor(dur * params.rate);

psdgw = numinj * psdgw * Kb * Kb / dur;

if params.doPlots

   figure;
   loglog(ff,psd1_signal_ave,'b')
   grid;
   xlim([10 1024]);
   hold on
   %loglog(spectra_f,spectra_h,'k--')
   loglog(ff,psdgw,'k--');
   hold off
   %ylim([1e-50 1e-35]);
   xlabel('Frequency [Hz]');
   ylabel('PSD [strain/rtHz]');
   print('-dpng',[params.outputDir '/waveform_psd.png'])
   print('-depsc2',[params.outputDir '/waveform_psd.eps'])
   print('-dpdf',[params.outputDir '/waveform_psd.pdf'])
   close;

   figure;
   semilogx(ff,psdgw./psd1_signal_ave,'b')
   grid;
   xlim([10 1024]);
   %ylim([1e-50 1e-35]);
   xlabel('Frequency [Hz]');
   ylabel('Ratio');
   print('-dpng',[params.outputDir '/waveform_psd_ratio.png'])
   print('-depsc2',[params.outputDir '/waveform_psd_ratio.eps'])
   print('-dpdf',[params.outputDir '/waveform_psd_ratio.pdf'])
   close;

   ratio = 2*psdgw./psd1_signal_ave;

   p=polyfit(log(ff),log(ratio),1);

   figure;
   semilogx(ff,2*psdgw./psd1_signal_ave,'b')
   m = p(1);
   b = exp(p(2));
   fprintf('m: %.5f, b: %.5f\n',m,b);

   hold on
   semilogx(ff,b*ff.^m,'k--')
   hold off
   grid;
   xlim([10 1024]);
   %ylim([1e-50 1e-35]);
   xlabel('Frequency [Hz]');
   ylabel('Ratio');
   print('-dpng',[params.outputDir '/waveform_psd_ratio2.png'])
   print('-depsc2',[params.outputDir '/waveform_psd_ratio2.eps'])
   print('-dpdf',[params.outputDir '/waveform_psd_ratio2.pdf'])
   close;
end


