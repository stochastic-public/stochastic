function [params, det1, det2] = fast_params(map)
% function [params, det1, det2] = fast_params(map)

% turn on new parameter
params.stochtrack.vlong = true;

% needed for calF
params.ifo1 = 'H1';
params.ifo2 = 'L1';
[det1 det2] = ifo2dets(params);
params.c = 3e8;

% start with default parameters
params = stampDefaults(params);

% needed for stochsky
params.doStochtrack = true;
params.stochtrack.norm = 'npix';
params.stochtrack.T = 10000;
%params.stochtrack.F = 200;
params.stochtrack.F = 20;
params.stochtrack.mindur = 80;
params.stochtrack.stochsky = true;
%params.doGPU = true;
params.stochtrack.demo = false;
% 18 May 2015: fixing bugging after param added by Michael
params.stochtrack.doSeed = false;
params.doParallel = true;
% direction needed?
params.ra = 0;
params.dec = 0;
% random seed set with GPS time
params.seed = -1;
params.jobNumber=1;
% save plots
params.savePlots = true;
% frequency range
try
  params.fmin = map.f(1);
  params.fmax = map.f(end);
  params.segmentDuration = map.segDur;
  params.startGPS = map.segstarttime(1);
catch
  fprintf('map struct unavailable\n');
end

% mask
%params = mask_S5H1L1_1s1Hz(params);
% remove notches outside of observation band
%params.StampFreqsToRemove = ...
%  params.StampFreqsToRemove(params.StampFreqsToRemove>=params.fmin);
%params.StampFreqsToRemove = ...
%  params.StampFreqsToRemove(params.StampFreqsToRemove<=params.fmax);

return
