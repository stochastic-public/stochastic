function max_cluster = asaf_v2(map, params)
% function max_cluster = asaf_v2(map, params)
%
% INPUT:
%    map  - STAMP map struct.
%    params - struct containing parameters for STAMP search.
%
% OUTPUT:
%    max_cluster (STRUCT)
%        max_cluster.snr_gamma -> SNR of the largest cluster
%        max_cluster.reconMax -> 2D array containing weight numbers for
%                                pixels contained the largest cluster and 0's
%                                at the location of other pixels
% Written by E Thrane and M Coughlin
% Contact: ethrane@ligo.caltech.edu, michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% state if parallelization is on
if params.doParallel
  fprintf(' (parallel on)...\n');
else
  fprintf(' (parallel off)...\n');
end

% map of NaNs: zeros where NaNs are present
nanmap0 = ~isnan(map.snr);

% NaNs occur due to missing data, vetoes, and notches.  When a NaN is
% encountered, y and snr are set to zero (so they do not add to the cluster SNR
% and sigma is set to one (much larger than typical strain power) so that the 
% cluster sigma does not depend significantly on the NaN pixels.  Just to be 
% safe, a check is employed to make sure that map.sigma << 1 in case anyone
% uses this with some funny units of strain.
if max(map.sigma(:)) > 1e-10
  error('Surprisingly large value of strain power encountered.');
end
map.snr(isnan(map.snr)) = 0;
map.y(isnan(map.y)) = 0;
map.z(isnan(map.z)) = 0;
map.sigma(isnan(map.sigma)) = 1;
map.sigma(map.sigma==0) = 1;

% map is MxN: M is the # of frequency bins and N is the number of segments
[M,N] = size(map.snr);

% index function: one unique index value for every (x,y) pair
% follow matlab's scheme, which looks like this:
%   tmp = [[1 2 3]' [4 5 6]'];
idx = @(xi, yi) (xi-1)*M + yi;

% stochtrack parameters-------------------------------------------------------
% number of trials
%T = params.stochtrack.T;
% number of trials determined by healpix
params.stochtrack.T = 3072;
T = params.stochtrack.T;
% number of trials in for loop is always the number of frequency bins
% and record this value in the param struct
params.stochtrack.F = length(map.f);
F = params.stochtrack.F;
%-----------------------------------------------------------------------------

% define GPU arrays
map_y = gArray(map.y, params);
map_z = gArray(map.z, params);
map_sigma = gArray(map.sigma, params);
nanmap = gArray(nanmap0, params);

% initialize m array(s) (associated with max snr track)
mtemplates = NaN*gZeros(N ,F, params);

% if stochsky search is performed prepare the necessary variables
% in fact, get them no matter what to prevent parfor errors
% obtain detector structs%
[det1 det2] = ifo2dets(params);

% create complex-valued map_snr
map_snr = (map_y + i*map_z) ./ map_sigma;

% generate random search directions
%  ra = 24*gRand(T, 1, params);
%  dec = acos(2*gRand(T, 1, params) - 1);
%  dec = (180/pi)*dec - 90;
%  source = [ra dec];
% ...using healpix
vec = HealpixGenerateSampling(2^4, 'scoord');
theta = vec(:,1);
phi = vec(:,2);
ra = phi*(12/pi);
dec = theta*(180/pi) - 90;
source = [ra dec];

% for loop to manage the memory
parfor aa=1:F
  % calculate time delay as a function of segment start time
  % note that caltau is identical to the older function calF except it has 
  % been stripped down to only calculate time delay.  tau does not need a 
  % gArray initialization because it is defined in terms of source, which is
  % already a gArray.
  tau = caltau(det1, det2, map.segstarttime, source, params)';
  % frequency array
  ff = map.f(aa) * ones(size(tau));
  % phase factor
  phi = exp(-i*2*pi*ff.*tau);

  % calculate the weight
  epsilon = calF_stochtrack(det1, det2, map.segstarttime, source, params);
  % this definition is solely for plotting purposes.  we follow the npix
  % normalization scheme in which snr = sum(weight.*snr).  since epsilon 
  % behaves like weight, we identify it as the weight, even though we are doing
  % something than npix normalization.
  weight = abs(epsilon);

  % carry out integration over a sidereal day
  xvals = repmat([1:N]', 1, T);
  yvals = aa*ones(size(xvals));
  templates = idx(xvals, yvals);

% incorrect epsilon weighting (OLD)
%  snr = mean(phi.*map_snr(templates),1);
  % scale by the number of time bins
%  snr = snr * sqrt(N);

  % implementing the correct epsilon weighting
  num = sum( epsilon.*phi.*map_snr(templates), 1);
  den = sqrt(sum( epsilon.^2, 1));
  snr = num./den;

  % take the real part of SNR
  snr_gamma = real(snr);

  % find the loudest cluster and the associated trial number (for this time 
  % through the for loop)
  [snr_max0 trial0] = max(snr_gamma);

  % keep track of snr_max for each time through the for-loop
  snr_max(aa) = gather(snr_max0);
  trial(aa) = gather(trial0);

  % capital T Trial allows us to assign a unique number to every trial in the
  % for loop
  Trial(aa) = gather(trial0 + (aa-1)*T);

  % record the best fit direction
  mra(aa) = gather(ra(trial0));
  mdec(aa) = gather(dec(trial0));

  % record best fit cluster
  mtemplates(:,aa) = gather(templates(:,trial0));
  mweight(:,aa) = gather(weight(:,trial0));

  % perodically print aa to stdout to track progress
  if mod(aa, 100)==0
    fprintf('%i\n', aa)
  end
end

% record array of max SNR as a function of frequency for diagnostic purposes
max_cluster.snrf_gamma = snr_max;

% find the max for all times through the for loop
[snr_max tmp_idx] = max(snr_max);
trial = trial(tmp_idx);
Trial = Trial(tmp_idx);

% print the SNR to the screen
fprintf('max SNR = %1.1f, trial=%i/%i\n', snr_max, Trial, T*F);

% reconstructed track map (shows the loudest track) 
rmap = gZeros(size(map.snr,1), size(map.snr,2), params);
rmap(mtemplates(:,tmp_idx)) = mweight(:,tmp_idx);
rmap = rmap.*nanmap0;
max_cluster.reconMax = gather(rmap);

% record properties of the loudest cluster
max_cluster.snr_gamma = snr_max;
max_cluster.ra = mra(tmp_idx);
max_cluster.dec = mdec(tmp_idx);

return
