function mcc_fast(fname)
% function mcc_fast(fname)
% figure out the user (usr will include a \n character at the end!)
[junk,HOME] = system('echo $HOME');
% path to stamp2
HOME = HOME(1:end-1);
stamp2 = [HOME '/matapps/packages/stochastic/trunk/stamp2/'];
stamp_pem = [HOME '/matapps/packages/stochastic/trunk/stamp-pem/'];
% get hostname
[no_use,h] = system('hostname -d');
h = h(1:end-1); % trim space from end
% path to signals toolbox
if (strcmp(h,'ligo.caltech.edu') | strcmp(h,'ligo-wa.caltech.edu') | ...
    strcmp(h,'ligo-la.caltech.edu'))
  sigs = '/ldcg/matlab_r2012a/toolbox/signal/signal';
  images = '/ldcg/matlab_r2012a/toolbox/images/iptutils/';
  stats = '/ldcg/matlab_r2012a/toolbox/stats/stats/';
elseif strcmp(h,'atlas.aei.uni-hannover.de')
  sigs = '/opt/matlab/2012a/toolbox/signal/signal';
else
  error('mcc_fast is currently set up to work only at CIT, LHO, LLO, and ATLAS');
end

% -I flags
includes = { stamp2 stamp_pem sigs images stats}; 

% -R flags
options = { '-nodisplay','-nojvm','-singleCompThread' };

% compilation command
comp_cmd = 'mcc -v -N';
for ii=1:numel(includes)
  comp_cmd = [comp_cmd ' -I ' includes{ii}];
end

for ii=1:numel(options)
  comp_cmd = [comp_cmd ' -R ' options{ii}];
end

comp_cmd = [comp_cmd ' -m ' fname];

% compile
fprintf('%s\n',comp_cmd);
eval(comp_cmd);

% remove annoying files
system(['rm mccExcludedFiles.log readme.txt run_' fname '.sh']);
system(['mv ' fname ' ~/STAMP/STAMP-PEM/bin']);
return;
