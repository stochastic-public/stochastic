function stamp_pem_summary_page(pemParamsFile)
% function stamp_pem_summary_page(pemParamsFile,segmentDuration,startGPS,endGPS)
% Given a STAMP-PEM parameter file, segment duration, and start and end 
% GPS times, creates a HTML summary page for the run for completed channels.
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Turn warnings off
warning off all;
%{
if isstr(startGPS)
   startGPS=str2num(startGPS);
end
if isstr(endGPS)
   endGPS=str2num(endGPS);
end

if isstr(segmentDuration)
   segmentDuration=str2num(segmentDuration);
end

if isstr(fmin)
   fmin = str2num(fmin)
end
if isstr(fmax)
   fmax = str2num(fmax)
end
%}
%
% Read in STAMP-PEM parameter file
params = readParamsFromFile(pemParamsFile);
%params.segmentDuration = segmentDuration;
%params.startGPS = startGPS; params.endGPS = endGPS;
%params.fmin = fmin; params.fmax = fmax;
params.darmChannelUnderscore = strrep(params.darmChannel,':','_');
%read in stamp pem personal information
%personal = readParamsFromFile([matappsPath '/stamp-pem/input/stamp_pem_personal.txt']);
%params.dirPath           = personal.dirPath;
%params.publicPath        = personal.publicPath;
%params.matappsPath       = personal.matappsPath;
%params.executableDir     = personal.executableDir;
%
% Define a vector containing frequencie we wanted to run over
if params.segmentDuration<=10
  frequencies = [params.fmin:1/(params.segmentDuration):params.fmax];
else
  frequencies = [params.fmin:0.1:params.fmax];
end
% Read in channel list
if exist([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo2 '-' params.frameType2 '-channel_list_' params.runName '.txt'])
[channelNames,channelSampleRates] = ...
   textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo2 '-' ...
	     params.frameType2 '-channel_list_' params.runName '.txt'],'%s %f');
else
  [channelNames,channelSampleRates] = ...
   textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo2 '-' ...
	     params.frameType2 '-channel_list.txt'],'%s %f');

end

% Output path for HTML file
params.path = ...
   [params.dirPath '/' params.ifo1 '/' params.runName '-' num2str(params.startGPS) ...
    '-' num2str(params.endGPS) '/' params.darmChannelUnderscore '/'  num2str(params.segmentDuration) '/' num2str(params.fmin) '-' num2str(params.fmax)];
% Open HTML file for writing
fid=fopen([params.path '/summary.html'],'w+');
% HTML Header
fprintf(fid,'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"\n');
fprintf(fid,'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n');
fprintf(fid,'\n');
fprintf(fid,'<html>\n');
fprintf(fid,'\n');
fprintf(fid,'<head>\n');
fprintf(fid,'<title>PEM Summary Page for %d - %d DARM Channel: %s</title>\n',params.startGPS,params.endGPS,params.darmChannel);
fprintf(fid,'<link rel="stylesheet" type="text/css" href="../../../../../style/main.css">\n');
fprintf(fid,'<script src="../../../../../style/sorttable.js"></script>\n');
fprintf(fid,'</head>\n');

fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">PEM Summary Page for %d - %d Darm Channel: %s</span></big></big></big></big><br>\n',params.startGPS,params.endGPS,params.darmChannel);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>');

% HTML table header
fprintf(fid,'<table class="sortable" id="sort" style="text-align: center; width: 1260px; height: 67px; margin-left:auto; margin-right: auto; background-color: white;" border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<thead>\n');
fprintf(fid,'<tr><th>PEM Channel</th><th>loudest SNR</th><th>loudest GPS</th><th>GPS SNR</th><th>loudest frequency</th><th>frequency SNR</th><th>max coherence frequency</th><th>max coherence</th></tr>\n');
fprintf(fid,'</thead>\n');

% Loop over channels
for i=1:length(channelNames)

   channelNameUnderscore = strrep(channelNames{i},':','_');

   % Check if there is data available for this channel
   if exist([params.path '/' channelNameUnderscore '/pem_' channelNameUnderscore '.html']); 

      params.doComparison = 0;
      if params.doComparison 
         % Generate arrays of ft-map SNRs for color-coding purposes
         output = stamp_pem_compare_channel(params,channelNames{i});
      else
         % Arrays of search algorithm results
         output.tt = [];
         output.burstegardSNR = [];
         output.gpsSNR = [];
         output.frequencySNR = [];
         output.coherence = [];
      end

      % Read in mat file associated with this channel
      try
         mat_data = load([params.path '/' channelNameUnderscore '/' channelNameUnderscore '_cluster.mat']);
      catch
         mat_data.cluster_out.max_SNR = NaN;
      end

      % Return maximum gps and frequency SNRs
      try

         [gpsSNR,gpsSNRIndex] = max(mat_data.cluster_out.line_glitch.t_snr);
         loudestGPS = mat_data.cluster_out.line_glitch.t(gpsSNRIndex(1));
         [frequencySNR,frequencySNRIndex] = max(mat_data.cluster_out.line_glitch.f_snr);
         loudestFrequency = mat_data.cluster_out.line_glitch.f(frequencySNRIndex(1));

      catch
         loudestGPS = NaN; gpsSNR = NaN; loudestFrequency = NaN; frequencySNR = NaN;
      end

      % Generate html color code based on this run's SNR
      %burstegardColor = stamp_pem_bgcolor(params,mat_data.cluster_out.max_SNR(1),output.burstegardSNR);
      burstegardColor = stamp_pem_bgcolor(params,mat_data.cluster_out.P2_cluster.snr_gamma,output.burstegardSNR);
      gpsColor = stamp_pem_bgcolor(params,loudestGPS,output.gpsSNR); 
      frequencyColor = stamp_pem_bgcolor(params,loudestFrequency,output.frequencySNR);

      % Read in mat file associated with this channel
      try
         coherence_data = load([params.path '/' channelNameUnderscore '/' channelNameUnderscore '_1.mat']);

         indexes = intersect(find(~isnan(max(coherence_data.map_pem.fft1))),find(~isnan(max(coherence_data.map_pem.fft2))));

         a1 = coherence_data.map_pem.fft1(:,indexes);
         psd1 = mean(abs(a1).^2,2);
         a2 = coherence_data.map_pem.fft2(:,indexes);
         psd2 = mean(abs(a2).^2,2);
         csd12 = mean(a1.*conj(a2),2);
         coherence = abs(csd12)./sqrt(psd1.*psd2);
         ff = coherence_data.map_pem.f;

         coherence = NaN * ones(length(ff),1);
         for j = 1:length(ff)
            a1 = coherence_data.map_pem.fft1(j,indexes);
            psd1 = mean(abs(a1).^2,2);
            a2 = coherence_data.map_pem.fft2(j,indexes);
            psd2 = mean(abs(a2).^2,2);
            csd12 = mean(a1.*conj(a2),2);
            %map.coherence(i) = abs(csd12)./sqrt(psd1.*psd2);
            coherence(j) = abs(xcorr(a1-mean(a1),a2-mean(a2),0,'coeff'));
         end

         %end_mask=floor(params.fmax/60);
         %start_mask=ceil(params.fmin/60);
         %bins = 5;
         %deltaF = ff(2) - ff(1);
         %for j = start_mask:end_mask;
         %   FreqToRemoveMin=60*j - bins*deltaF; FreqToRemoveMax = 60*j + bins*deltaF;
         %   indexes = find(~(ff >= FreqToRemoveMin & ff <= FreqToRemoveMax));
         %   coherence = coherence(indexes); ff = ff(indexes);
         %end

         [maxCoherence,maxCoherenceIndex] = max(coherence);
         maxCoherenceFrequency = ff(maxCoherenceIndex(1));

         channel_coherences(i).ff = ff;
         channel_coherences(i).coherence = coherence;
         channel_coherences(i).channelName = channelNames{i};
         channel_coherences(i).channelNameUnderscore = strrep(channelNames{i},'_','\_');
      catch
         maxCoherenceFrequency = NaN; maxCoherence = NaN;
      end

      coherenceColor = stamp_pem_bgcolor(params,maxCoherence,0:0.01:1);

      % Create HTML row containing channel and ft-map SNR information
      fprintf(fid,'<tr>');
      fprintf(fid,'<td><a href="./%s/pem_%s.html">%s</a></td>',...
         channelNameUnderscore,channelNameUnderscore,channelNameUnderscore);
      fprintf(fid,'<td style="background-color: %s">%.2f</td>',burstegardColor,mat_data.cluster_out.max_SNR(1));
      fprintf(fid,'<td>%.2f</td>',loudestGPS);
      fprintf(fid,'<td style="background-color: %s">%.2f</td>',gpsColor,gpsSNR);
      fprintf(fid,'<td>%.2f</td>',loudestFrequency);
      fprintf(fid,'<td style="background-color: %s">%.2f</td>',frequencyColor,frequencySNR);
      fprintf(fid,'<td>%.2f</td>',maxCoherenceFrequency);
      fprintf(fid,'<td style="background-color: %s">%.2f</td>',coherenceColor,maxCoherence);
   end
   fprintf(fid,'</tr>\n');
end

% Close HTML table
fprintf(fid,'</table>\n');

fprintf(fid,'<br>\n');
fprintf(fid,'<br>\n');

% Show colorbar indicating color significance
fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 200px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Coherence</span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Coherence Matrix</span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="./coherence.png"><img alt="" src="./coherence.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="./coherence_matrix.png"><img alt="" src="./coherence_matrix.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');

% Show user and time information
[junk,user] = system('echo $USER');
[junk,time] = system('date');

fprintf(fid,'<br>\n');
fprintf(fid,'<br>\n');
fprintf(fid,'Created by user %s on %s \n',user,time);

% Close HTML
fprintf(fid,'</html>\n');
fclose(fid);

figure('Position', [100, 100, 2049, 2049])
check_init = 0;
legend_labels = {};
for i = 1:length(channel_coherences)
   if isempty(channel_coherences(i).ff)
      continue
   end
   plot(channel_coherences(i).ff,channel_coherences(i).coherence);
   legend_labels{length(legend_labels)+1} = channel_coherences(i).channelNameUnderscore;
   if check_init == 0
      hold all;
      check_init = 1;
   end
end
hleg1 = legend(legend_labels);
set(hleg1,'Location','NorthEastOutside')
set(hleg1,'FontSize',10);
%set(hleg1,'Interpreter','none')
xlabel('Frequency [Hz]')
ylabel('Coherence')
print('-dpng',[params.path '/coherence']);
print('-depsc2',[params.path '/coherence']);
close;

legend_labels = {};
coherence_matrix = [];

for i = 1:length(channel_coherences)
   if isempty(channel_coherences(i).ff)
      continue
   end
   if isnan(sum(channel_coherences(i).coherence))
      continue
   end
   ff = channel_coherences(i).ff;
   % sometimes a channel can't sample at a high enough rate to get all frequencies in a given band
   % when this happens we fill in the extra coherences with zeros so we can still include those channels
   % in the coherence matrix.
   if ~(length(ff)==length(frequencies))
     fprintf('channel %s does not span the frequency band. Filling in extra coherences with zeros.\n',channel_coherences(i).channelName);
     extra_coh = zeros(length(channel_coherences(1).ff)-length(channel_coherences(i).ff),1);
     channel_coherences(i).coherence = [channel_coherences(i).coherence;extra_coh];
   end
   coherence_matrix = [coherence_matrix channel_coherences(i).coherence];
   legend_labels{length(legend_labels)+1} = channel_coherences(i).channelName;
end

if log10(max(ff)) - log10(min(ff)) > 1
   doLog = 1;
else
   doLog = 0;
end
% reset ff to be length it should be instead of 
% length of frequency series of last channel
% we looked at.
%ff = [params.fmin:ff(2)-ff(1):params.fmax];
[numFreq,numChannels] = size(coherence_matrix);
channelIndexes = 1:numChannels;
[X,Y] = meshgrid(frequencies,channelIndexes);
figure;
set(gcf,'PaperUnits','inches')
set(gcf,'PaperSize',[30 24])
set(gcf,'PaperPositionMode','manual')
set(gcf,'PaperPosition',[0 0 40 34])
hC = pcolor(X,Y,coherence_matrix');
set(gcf,'Renderer','zbuffer');
colormap(jet)
%shading interp
t = colorbar('peer',gca);
set(get(t,'ylabel'),'String','Coherence');
set(t,'fontsize',20);
set(hC,'LineStyle','none');
grid
set(gca,'clim',[0 1])
if doLog
   set(gca,'xscale','log');
%   set(gca,'yscale','log');
end
if doLog
   ticks = logspace(log10(params.fmin),log10(params.fmax),10);
   set(gca,'XTick',ticks)
else
   set(gca,'XTick',min(frequencies):20:max(frequencies))
end
set(gca,'YTick',channelIndexes+0.5)
set(gca,'YTickLabel',legend_labels)
pretty;
h_xlabel = get(gca, 'XLabel');
set(h_xlabel, 'FontSize', 20);
h_ylabel = get(gca, 'YLabel');
set(h_ylabel, 'FontSize', 20);
xlabel('Frequency [Hz]')
ylabel('Coherence')
print('-dpng',[params.path '/coherence_matrix']);
print('-depsc2',[params.path '/coherence_matrix']);
close;

save([params.path '/coherence.mat'],'channel_coherences');

% Public HTML output path
params.outputPath = ...
   [params.publicPath '/' params.ifo1 '/' params.runName '/' params.darmChannelUnderscore ...
    '/' num2str(params.segmentDuration) '/' num2str(params.fmin) '-' num2str(params.fmax)];

stamp_pem_createpath(params.outputPath);

rm_command = sprintf('rm -rf %s\n',params.outputPath);
system(rm_command);
cp_command = sprintf('cp -r %s %s\n',params.path,params.outputPath);
system(cp_command);

delete([params.path '/' channelNameUnderscore '/' channelNameUnderscore '_1.mat']);
