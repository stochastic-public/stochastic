function stamp_pem_page(params, cluster_out)
% function stamp_pem_page(params, cluster_out)
% Given a STAMP params struct and clustermap output struct, 
% creates a HTML summary page
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Output path for the HTML page
webpage_path = params.plotdir;
% Plot directory with respect to HTML page location
plot_path = '.';

% Return maximum gps and frequency SNRs
try
   [gpsSNR,gpsSNRIndex] = max(cluster_out.line_glitch.t_snr);
   loudestGPS = cluster_out.line_glitch.t(gpsSNRIndex(1));
   [frequencySNR,frequencySNRIndex] = max(cluster_out.line_glitch.f_snr);
   loudestFrequency = cluster_out.line_glitch.f(frequencySNRIndex(1));
catch
   loudestGPS = NaN; gpsSNR = NaN; loudestFrequency = NaN; frequencySNR = NaN;
end

% Open HTML page for writing
fid=fopen([webpage_path '/pem_' params.pemChannelUnderscore '.html'],'w+');

% HTML Header
fprintf(fid,'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"\n');
fprintf(fid,'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n');
fprintf(fid,'\n');
fprintf(fid,'<html>\n');
fprintf(fid,'\n');
fprintf(fid,'<head>\n');
fprintf(fid,'<title>PEM Page for %s: %d - %d</title>\n',params.pemChannelUnderscore,params.startGPS,params.endGPS);
fprintf(fid,'<link rel="stylesheet" type="text/css" href="../../main.css">\n');
fprintf(fid,'<script src="../../sorttable.js"></script>\n');
fprintf(fid,'</head>\n');

fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">PEM Page for %s: %d - %d</span></big></big></big></big><br>\n',params.pemChannelUnderscore,params.startGPS,params.endGPS);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>');

fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');

% HTML table of channel information
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">GPS Time of Block: %d - %d </span></big></big></big><br>\n',params.startGPS,params.endGPS);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">loudest SNR: %.2f</span></big></big></big><br>\n',cluster_out.max_SNR(1));
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">loudest GPS: %.2f</span></big></big></big><br>\n',loudestGPS);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">GPS SNR: %.2f</span></big></big></big><br>\n',gpsSNR);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">loudest Frequency: %.2f</span></big></big></big><br>\n',loudestFrequency);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Frequency SNR: %.2f</span></big></big></big><br>\n',frequencySNR);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');

% HTML table of channel plots
fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Cross-power spectrogram </span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Coherence </span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');

fprintf(fid,'<a href="%s/snr.png"><img alt="" src="%s/snr.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',plot_path,plot_path);
fprintf(fid,'</td>');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="%s/coherence.png"><img alt="" src="%s/coherence.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',plot_path,plot_path);
fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');

fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">%s PSD spectrogram</span></big></big><br>\n',params.darmChannel);
fprintf(fid,'</td>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">%s PSD spectrogram</span></big></big><br>\n',params.pemChannel);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');

fprintf(fid,'<a href="%s/P1.png"><img alt="" src="%s/P1.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',plot_path,plot_path);
fprintf(fid,'</td>');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="%s/P2.png"><img alt="" src="%s/P2.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',plot_path,plot_path);
fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');

fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Bicoherence</span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Bicoherence p-value</span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="%s/bicoherence.png"><img alt="" src="%s/bicoherence.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',plot_path,plot_path);
fprintf(fid,'</td>');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="%s/bicoherence_pvalue.png"><img alt="" src="%s/bicoherence_pvalue.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',plot_path,plot_path);
fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');

fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');

% Close HTML
fprintf(fid,'</html>\n');
fclose(fid);
