function stamp_pem_summary_page(pemParamsFile,matappsPath,segmentDuration,startGPS,endGPS,fmin,fmax)
% function stamp_pem_summary_page(pemParamsFile,segmentDuration,startGPS,endGPS)
% Given a STAMP-PEM parameter file, segment duration, and start and end 
% GPS times, creates a HTML summary page for the run for completed channels.
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Turn warnings off
warning off all;

if isstr(startGPS)
   startGPS=str2num(startGPS);
end
if isstr(endGPS)
   endGPS=str2num(endGPS);
end

if isstr(segmentDuration)
   segmentDuration=str2num(segmentDuration);
end

if isstr(fmin)
   fmin = str2num(fmin)
end
if isstr(fmax)
   fmax = str2num(fmax)
end
% Read in STAMP-PEM parameter file
params = readParamsFromFile(pemParamsFile);
% Read in stamp pem personal information
personal = readParamsFromFile([ matappsPath '/stamp-pem/input/stamp_pem_personal.txt']);
params.dirPath           = personal.dirPath;
params.publicPath        = personal.publicPath;
params.matappsPath       = personal.matappsPath;
params.executableDir     = personal.executableDir;


params.segmentDuration = segmentDuration;
params.startGPS = startGPS; params.endGPS = endGPS;
params.fmin = fmin; params.fmax = fmax;

darmChannelUnderscore=strrep(params.darmChannel, ':', '_');

% Read in channel list
if exist([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo2 '-' params.frameType2 '-channel_list_' params.runName '.txt'])
[channelNames,channelSampleRates] = ...
   textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo2 '-' ...
	     params.frameType2 '-channel_list_' params.runName '.txt'],'%s %f');
else
  [channelNames,channelSampleRates] = ...
   textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo2 '-' ...
	     params.frameType2 '-channel_list.txt'],'%s %f');

end

% Output path for HTML file
params.path = ...
   [params.dirPath '/' params.ifo1 '/' params.runName '-' num2str(params.startGPS) ...
    '-' num2str(params.endGPS) '/' darmChannelUnderscore '/'  num2str(params.segmentDuration) '/' num2str(params.fmin) '-' num2str(params.fmax)];

% Loop over channels
for i=1:length(channelNames)

   channelNameUnderscore = strrep(channelNames{i},':','_');

   coherence_data = load([params.path '/' channelNameUnderscore '/' channelNameUnderscore '_1.mat']);

   channel_coherences(i).ff = coherence_data.map0.f;
   channel_coherences(i).tt = coherence_data.map0.segstarttime;
   channel_coherences(i).fft = coherence_data.map0.fft2;
   channel_coherences(i).channelName = channelNames{i};
   channel_coherences(i).channelNameUnderscore = strrep(channelNames{i},'_','\_');
end

channelIndexes = [];
for i = 1:length(channel_coherences)
   if isempty(channel_coherences(i).ff)
      continue
   end

   indexes = find(~isnan(max(channel_coherences(i).fft)));
   a1 = channel_coherences(i).fft(:,indexes);
   if sum(a1(:)) == 0
      continue
   end

   channelIndexes = [channelIndexes i];
end

channel_coherences = channel_coherences(channelIndexes);

ff = channel_coherences(1).ff;
tt = channel_coherences(1).tt;
coherence_matrix = NaN*ones(length(channel_coherences),length(channel_coherences),length(tt),length(ff));

for i = 1:length(channel_coherences)
   for j = 1:length(channel_coherences)
      %indexes = intersect(find(~isnan(max(channel_coherences(i).fft))),find(~isnan(max(channel_coherences(j).fft))));
      %a1 = channel_coherences(i).fft(:,indexes);
      %psd1 = mean(abs(a1).^2,2);
      %a2 = channel_coherences(j).fft(:,indexes);
      %psd2 = mean(abs(a2).^2,2);
      %csd12 = mean(a1.*conj(a2),2);
      %coherence = abs(csd12)./sqrt(psd1.*psd2);

      %coherence_matrix(i,j,:) = coherence;

      a1 = channel_coherences(i).fft;
      psd1 = abs(a1).^2;
      a2 = channel_coherences(j).fft;
      psd2 = abs(a2).^2;
      csd12 = a1.*conj(a2);
      coherence = csd12./sqrt(psd1.*psd2);

      coherence_matrix(i,j,:,:) = squeeze(coherence)';
   end
end

% Minimal speed used for making kf-grid: this decides the size of the map
c = 500; % m/s
nk = 101; % No.of points in each dimesnion (kx, ky, kz); decides size of R matrix
SN = length(channel_coherences);
SP = randn(length(channel_coherences),2);

plotDir = [params.path '/kfmaps'];
stamp_pem_createpath(plotDir);

methodType = 'bartlett';
methodType = 'capon';
methodType = 'music';
methodType = 'radiometer';

%for fi = 1:length(ff)
for fi = 1
   map(fi).f = ff(fi);
   k0 = 2*pi*ff(fi)/c; % maximum of k vector magnitude

   map(fi).kx = linspace(-k0,k0,nk);
   map(fi).ky = linspace(-k0,k0,nk);
   map(fi).kz = linspace(-k0,k0,nk);
   %[kX, kY, kZ] = meshgrid(map(fi).kx, map(fi).ky, map(fi).kz);
   [kX, kY] = meshgrid(map(fi).kx, map(fi).ky);
   map(fi).spectrum = zeros(nk, nk);

   for k = 1:length(tt)

      tmpmap = zeros(nk, nk, nk);
      tmpmap = zeros(nk, nk);

      csd = coherence_matrix(:,:,k,fi);
      csd = csd - eye(SN); % removing the auto-correlation terms, prone to noise transients

      if strcmp(methodType,'bartlett')
      elseif strcmp(methodType,'capon')
          csd = inv(csd);
      elseif strcmp(methodType,'music')
          % MUSIC estimate %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

          csd(isnan(csd)) = 0;

          [V, D] = eig(csd);
          eig_vals = diag(D);
          [sort_eig_vals, idx] = sort(eig_vals);
          V = V(:, idx);

          % find out which are significant eigen values (outliers) probably
          % correspond to signal(s);
          tmpeig = abs(sort_eig_vals - median(sort_eig_vals));
          % the factor 10 might need to changed depending on the situation
          cutS = tmpeig >= median(tmpeig)*10; % might correspond to signal
          fprintf('No. of possible signals %d \n', sum(cutS));
          if(sum(cutS) >= length(sort_eig_vals)*0.6)
            fprintf('Too many signal like eigen values, change the number 10 to some higher value \n');
          end
          Us = V(:, cutS); % eigen vectors correspond to possible signals
          Un = V(:, ~cutS); % eigen vectors correspond to noise
          PIn = Un*Un';

          csd = PIn;
      elseif strcmp(methodType,'radiometer')

          % Radiometer method %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
          kX = kX(:)';
          kY = kY(:)';
          S = zeros(1,nk*nk);
          B = zeros(nk*nk);
      end

      kk = 1;
      for c1 = 1:SN
         for c2 = 1:SN
            if strcmp(methodType,'radiometer')
               if(c1~=c2)
                  %p1 = abs(a(c1)).^2;
                  %p2 = abs(a(c2)).^2;
                  p1 = 1;
                  p2 = 1;
                  p12 = csd(c1,c2);
                  % here S = \gamma*T Y, B = \gamma*T \gamma (in Vuk's notes)
                  S = S + (p12/(p1*p2))*exp(-i*((SP(c2,1)-SP(c1,1))*kX+(SP(c2,2)-SP(c1,2))*kY));
                  const_phase = exp(-i*((SP(c2,1)-SP(c1,1))*kX+(SP(c2,2)-SP(c1,2))*kY));
                  B = B + (1/(p1*p2))*const_phase'*const_phase;
               end
            else
               tmpmap = tmpmap + csd(c1,c2)*exp(-i*((SP(c2,1)-SP(c1,1))*kX+(SP(c2,2)-SP(c1,2))*kY));
            end
         end
      end

      if isnan(sum(tmpmap(:))) || sum(tmpmap(:)) == 0
         continue
      end

      % excess in real part of tmpmap corresponds to actual signal
      if strcmp(methodType,'bartlett')
         map(fi).spectrum = map(fi).spectrum + abs(real(tmpmap));
      elseif strcmp(methodType,'capon')
         map(fi).spectrum = map(fi).spectrum + 1./abs(real(tmpmap));
      elseif strcmp(methodType,'music')
         map(fi).spectrum = map(fi).spectrum + 1./abs(real(tmpmap));
      elseif strcmp(methodType,'radiometer')
         % We are solving eq. of the type B zz = S (or zz = B^{-1} S)
         zz = lsqr(real(B),real(S)'); % here zz is S in Vuk's notes;
         P = reshape(zz, nk, nk, nk);
         map(fi).spectrum = map(fi).spectrum + abs(real(P));
      end
   end

   map(fi).spectrum = map(fi).spectrum/max(map(fi).spectrum(:));% normalizing

   [X,Y] = meshgrid(map(fi).kx, map(fi).ky);

   figure;
   set(gcf,'PaperUnits','inches')
   set(gcf,'PaperSize',[30 24])
   set(gcf,'PaperPositionMode','manual')
   set(gcf,'PaperPosition',[0 0 30 24])
   hC = pcolor(X,Y,map(fi).spectrum');
   set(gcf,'Renderer','zbuffer');
   colormap(jet)
   %shading interp
   t = colorbar('peer',gca);
   set(get(t,'ylabel'),'String','Coherence');
   set(t,'fontsize',20);
   set(hC,'LineStyle','none');
   grid
   %set(gca,'clim',[0 1])
   pretty;
   h_xlabel = get(gca, 'XLabel');
   set(h_xlabel, 'FontSize', 20);
   h_ylabel = get(gca, 'YLabel');
   set(h_ylabel, 'FontSize', 20);
   xlabel('Wavenumber k_x [1/m]')
   ylabel('Wavenumber k_y [1/m]')
   plotName = strrep(num2str(ff(fi)),'.','_');
   print('-dpng',[plotDir '/' methodType '_' plotName]);
   print('-depsc2',[plotDir '/' methodType '_' plotName]);
   close;

end

