function map0 = stamp_pem_map_cuts(params,map0);

% Save PEM info
%map0.snr = abs(map0.snr); map0.y = abs(map0.y);
pem.P1 = map0.P1; pem.P2 = map0.P2;
pem.f = map0.f; pem.segstarttime = map0.segstarttime;
save([params.outputfilename '_psd.mat'],'pem');

% Normalize PSDs for comparison
map0.P1_log10 = log10(map0.P1);
map0.P2_log10 = log10(map0.P2);

map0.P1_log10(isnan(map0.P1_log10)) = 0;
map0.P2_log10(isnan(map0.P2_log10)) = 0;

map0_P1_log10 = mean(map0.P1_log10,2); map0_P2_log10 = mean(map0.P2_log10,2);

for i = 1:length(map0.f)
   if map0_P1_log10(i) ~= 0
      map0.P1_log10(i,:) = map0.P1_log10(i,:) / map0_P1_log10(i);

   end
   if map0_P2_log10(i) ~= 0
      map0.P2_log10(i,:) = map0.P2_log10(i,:) / map0_P2_log10(i);
   end
end

% Remove lines
rejectPercentageThreshold = 0.9;
%rejectPercentageThreshold = 100;
rejectThreshold = 0.5;
for i = 1:length(map0.f)
   rejectPercentage = length(find(abs(map0.P1_log10(i,:)-1) >= rejectThreshold)) / length(map0.P1_log10(i,:));
   if rejectPercentage >= rejectPercentageThreshold
      %map0.P1_log10(i,:) = 0;
   end
   rejectPercentage = length(find(abs(map0.P2_log10(i,:)-1) >= rejectThreshold)) / length(map0.P2_log10(i,:));
   if rejectPercentage >= rejectPercentageThreshold
      %map0.P2_log10(i,:) = 0;
   end
end

% Remove glitches
rejectPercentageThreshold = 0.9;
%rejectPercentageThreshold = 100;
rejectThreshold = 0.5;
for i = 1:length(map0.segstarttime)
   rejectPercentage = length(find(abs(map0.P1_log10(:,i)-1) >= rejectThreshold)) / length(map0.P1_log10(:,i));
   if rejectPercentage >= rejectPercentageThreshold
      %map0.P1_log10(:,i) = 0;
   end
   rejectPercentage = length(find(abs(map0.P2_log10(:,i)-1) >= rejectThreshold)) / length(map0.P2_log10(:,i));
   if rejectPercentage >= rejectPercentageThreshold
      %map0.P2_log10(:,i) = 0;
   end
end

segmentsFile = [params.path '/segments/segments.mat'];
if ~exist(segmentsFile)
   return
end

load(segmentsFile);
segments = segments.segments;

indexes = [];
for i = 1:length(map0.segstarttime)
   gps = map0.segstarttime(i);
   if any(gps >= segments(:,1) & gps <= segments(:,2))
      indexes = [indexes i];
   end
end

cut_col = setdiff(1:length(map0.segstarttime),indexes);

% fill cut columns with NaNs
map0.snr(:,cut_col) = NaN;
map0.y(:,cut_col) = NaN;
map0.sigma(:,cut_col) = NaN;
map0.fft1(:,cut_col) = NaN;
map0.fft2(:,cut_col) = NaN;
map0.P1(:,cut_col) = NaN;
map0.P2(:,cut_col) = NaN;

