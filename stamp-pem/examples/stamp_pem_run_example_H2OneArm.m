%run_name corresponds to the params file we'll choose. see the
%/stamp-pem/input directory for available params file. 
run_name='H2OneArm';
%concatenate params file for this run with your personal params file.
pemParamsFile = stamp_pem_createParamsFile(run_name);
pemChannel = 'H2:PEM-EY_MAG_EBAY_SEIRACK_Z_DQ';
%pemChannel = 'H2:PEM-CS_ACC_EBAY_FLOOR_Z_DQ';
segmentDuration = '10';
startGPS = '1029391218';
endGPS =   '1029391418';
fmin= '10';
fmax='200';
stamp_pem_run(pemParamsFile,pemChannel,segmentDuration,startGPS,endGPS,fmin,fmax);
stamp_pem_summary_page(pemParamsFile,segmentDuration,startGPS,endGPS,fmin,fmax);

