function pemParamsFile=stamp_pem_createParamsFile(run_name);

%%%%%%%%%%%%%%%%%%
% Creates Param File to be used in stamp-pem analysis
% concatenates the 
cd ~/matapps/packages/stochastic/trunk/stamp-pem/input;
%%%% Put the params for the given run in the params file
[junk, user_name]=textread('./stamp_pem_personal.txt', '%s %s', 1);
%%% create the file path for the params file
pemParamsFile=strcat('/home1/02786/', user_name(1), '/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_params_', run_name, '.txt');
pemParamsFile=char(pemParamsFile);
cd ~/matapps/packages/stochastic/trunk/stamp-pem/stampede;
