function stamp_pem_summary_compare(pemParamsFile,matappsPath,segmentDuration,startGPS,endGPS,fmin,fmax,startGPS2,endGPS2)
% function stamp_pem_summary_page(pemParamsFile,segmentDuration,startGPS,endGPS)
% Given a STAMP-PEM parameter file, segment duration, and start and end 
% GPS times, creates a HTML summary page for the run for completed channels.
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Turn warnings off
warning off all;

if isstr(startGPS)
   startGPS=str2num(startGPS);
end
if isstr(endGPS)
   endGPS=str2num(endGPS);
end

if isstr(segmentDuration)
   segmentDuration=str2num(segmentDuration);
end

if isstr(fmin)
   fmin = str2num(fmin)
end
if isstr(fmax)
   fmax = str2num(fmax)
end

% Read in STAMP-PEM parameter file
params = readParamsFromFile(pemParamsFile);
params.segmentDuration = segmentDuration;
params.startGPS = startGPS; params.endGPS = endGPS;
params.fmin = fmin; params.fmax = fmax;
params.darmChannelUnderscore = strrep(params.darmChannel,':','_');
%read in stamp pem personal information
personal = readParamsFromFile([matappsPath '/stamp-pem/input/stamp_pem_personal.txt']);
params.dirPath           = personal.dirPath;
params.publicPath        = personal.publicPath;
params.matappsPath       = personal.matappsPath;
params.executableDir     = personal.executableDir;

params.startGPS2 = startGPS2; params.endGPS2 = endGPS2;


% Read in channel list
if exist([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo2 '-' params.frameType2 '-channel_list_' params.runName '.txt'])
[channelNames,channelSampleRates] = ...
   textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo2 '-' ...
	     params.frameType2 '-channel_list_' params.runName '.txt'],'%s %f');
else
  [channelNames,channelSampleRates] = ...
   textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo2 '-' ...
	     params.frameType2 '-channel_list.txt'],'%s %f');

end
%{
coherenceMat1 = ...
   [params.dirPath '/' params.ifo1 '/' params.runName '-' num2str(params.startGPS) ...
    '-' num2str(params.endGPS) '/' params.darmChannelUnderscore '/'  num2str(params.segmentDuration) '/' num2str(params.fmin) '-' num2str(params.fmax) '/coherence.mat'];

coherenceMat2 = ...
   [params.dirPath '/' params.ifo1 '/' params.runName '-' num2str(params.startGPS2) ...
    '-' num2str(params.endGPS2) '/' params.darmChannelUnderscore '/'  num2str(params.segmentDuration) '/' num2str(params.fmin) '-' num2str(params.fmax) '/coherence.mat'];

if ~exist(coherenceMat1)
   fprintf('%s does not exist... returning.\n',coherenceMat1);
end

if ~exist(coherenceMat2)
   fprintf('%s does not exist... returning.\n',coherenceMat2);
end

data1 = load(coherenceMat1);
data2 = load(coherenceMat2);

channel_coherences_1 = data1.channel_coherences;
channel_coherences_2 = data2.channel_coherences;
%}
coherenceMat1 = ...
   [params.dirPath '/' params.ifo1 '/' params.runName '-' num2str(params.startGPS) ...
    '-' num2str(params.endGPS) '/' params.darmChannelUnderscore '/'  num2str(params.segmentDuration) '/' num2str(params.fmin) '-' num2str(params.fmax) '/coherence.mat'];

if ~exist(coherenceMat1)
   fprintf('%s does not exist... returning.\n',coherenceMat1);
   return
end

data1 = load(coherenceMat1);
channel_coherences_1 = data1.channel_coherences;

channel_coherences_2 = [];
coherenceFolders = dir([params.dirPath '/' params.ifo1 '/' params.runName '-*']);
for i = 1:length(coherenceFolders)
   coherenceFolderSplit = regexp(coherenceFolders(i).name,'-','split');
try
   gpsStartFolder = str2num(coherenceFolderSplit{2});
   gpsEndFolder = str2num(coherenceFolderSplit{3});
catch
keyboard
end
   if (params.startGPS2 <= gpsStartFolder) & (params.startGPS2 <= gpsEndFolder)
      coherenceMat2 = ...
         [params.dirPath '/' params.ifo1 '/' coherenceFolders(i).name ...
      '/' params.darmChannelUnderscore '/'  num2str(params.segmentDuration) '/' num2str(params.fmin) '-' num2str(params.fmax) '/coherence.mat'];
      if ~exist(coherenceMat2)
         continue
      end

      data_out = load(coherenceMat2);
      if isempty(channel_coherences_2)
         channel_coherences_2 = data_out.channel_coherences;
      else
         channel_coherences_3 = data_out.channel_coherences;
         for j = 1:length(channel_coherences_2)
            for k = 1:length(channel_coherences_3)
               if strcmp(channel_coherences_2(j).channelName,channel_coherences_3(k).channelName);
                  channel_coherences_2(j).coherence = ...
                     [channel_coherences_2(j).coherence channel_coherences_3(k).coherence];
               end
            end
         end
      end
   end
end
for j = 1:length(channel_coherences_2)
   channel_coherences_2(j).coherence = median(channel_coherences_2(j).coherence,2);
end
keyboard

% Output path for HTML file
params.path = ...
   [params.dirPath '/' params.ifo1 '/' params.runName '-' num2str(params.startGPS) ...
    '-' num2str(params.endGPS) '/' params.darmChannelUnderscore '/'  num2str(params.segmentDuration) '/' num2str(params.fmin) '-' num2str(params.fmax) '/comparison/' num2str(params.startGPS2) '-' num2str(params.endGPS2)];
stamp_pem_createpath(params.path);

% Open HTML file for writing
fid=fopen([params.path '/summary.html'],'w+');
% HTML Header
fprintf(fid,'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"\n');
fprintf(fid,'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n');
fprintf(fid,'\n');
fprintf(fid,'<html>\n');
fprintf(fid,'\n');
fprintf(fid,'<head>\n');
fprintf(fid,'<title>PEM Summary Page for %d - %d DARM Channel: %s</title>\n',params.startGPS,params.endGPS,params.darmChannel);
fprintf(fid,'<link rel="stylesheet" type="text/css" href="../../../../../style/main.css">\n');
fprintf(fid,'<script src="../../../../../style/sorttable.js"></script>\n');
fprintf(fid,'</head>\n');

fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">PEM Summary Page for %d - %d Darm Channel: %s</span></big></big></big></big><br>\n',params.startGPS,params.endGPS,params.darmChannel);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>');

% HTML table header
fprintf(fid,'<table class="sortable" id="sort" style="text-align: center; width: 1260px; height: 67px; margin-left:auto; margin-right: auto; background-color: white;" border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<thead>\n');
fprintf(fid,'<tr><th>PEM Channel</th><th>max coherence frequency</th><th>max coherence</th></tr>\n');
fprintf(fid,'</thead>\n');

channel_coherences = [];

% Loop over channels
for i=1:length(channelNames)
   
   channelNameUnderscore = strrep(channelNames{i},':','_');

   for j = 1:length(channel_coherences_1)
      if strcmp(channelNames{i},channel_coherences_1(j).channelName);
         channel_coherences(i).ff = channel_coherences_1(j).ff;
         channel_coherences(i).coherence1 = channel_coherences_1(j).coherence;
         channel_coherences(i).channelName = channelNames{i};
         channel_coherences(i).channelNameUnderscore = strrep(channelNames{i},'_','\_');
      end
   end

   for j = 1:length(channel_coherences_2)
      if strcmp(channelNames{i},channel_coherences_2(j).channelName);
         channel_coherences(i).coherence2 = channel_coherences_2(j).coherence;
      end
   end

   try
      channel_coherences(i);
   catch
      continue
   end

   try
         channel_coherences(i).coherenceRatio = ...
            channel_coherences(i).coherence1 ./ channel_coherences(i).coherence2;

         [maxCoherence,maxCoherenceIndex] = max(channel_coherences(i).coherenceRatio);
         maxCoherenceFrequency = channel_coherences(i).ff(maxCoherenceIndex(1));

   catch
      maxCoherenceFrequency = NaN; maxCoherence = NaN;
   end

      coherenceColor = stamp_pem_bgcolor(params,maxCoherence,0.1:0.1:10);

      % Create HTML row containing channel and ft-map SNR information
      fprintf(fid,'<tr>');
      fprintf(fid,'<td><a href="./%s/pem_%s.html">%s</a></td>',...
         channelNameUnderscore,channelNameUnderscore,channelNameUnderscore);
      fprintf(fid,'<td>%.2f</td>',maxCoherenceFrequency);
      fprintf(fid,'<td style="background-color: %s">%.2f</td>',coherenceColor,maxCoherence);
      fprintf(fid,'</tr>\n');
end

% Close HTML table
fprintf(fid,'</table>\n');

fprintf(fid,'<br>\n');
fprintf(fid,'<br>\n');

% Show colorbar indicating color significance
fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 200px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Coherence</span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Coherence Matrix</span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="./coherence.png"><img alt="" src="./coherence.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="./coherence_matrix.png"><img alt="" src="./coherence_matrix.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');

% Show user and time information
[junk,user] = system('echo $USER');
[junk,time] = system('date');

fprintf(fid,'<br>\n');
fprintf(fid,'<br>\n');
fprintf(fid,'Created by user %s on %s \n',user,time);

% Close HTML
fprintf(fid,'</html>\n');
fclose(fid);

figure('Position', [100, 100, 2049*1.5, 1895*1.5])
check_init = 0;
legend_labels = {};
for i = 1:length(channel_coherences)
   if (isempty(channel_coherences(i).ff) | isempty(channel_coherences(i).coherence2) | isempty(channel_coherences(i).coherence1))
      continue
   end
try
   plot(channel_coherences(i).ff,channel_coherences(i).coherenceRatio); % plot coherence vs. frequency for each channel
   legend_labels{length(legend_labels)+1} = channel_coherences(i).channelNameUnderscore;
catch
keyboard
end
   if check_init == 0
      hold all;
      check_init = 1;
   end
end
hleg1 = legend(legend_labels);
set(hleg1,'Location','NorthEastOutside')
set(hleg1,'FontSize',10);
%set(hleg1,'Interpreter','none')
xlabel('Frequency [Hz]')
ylabel('Coherence Ratio')
print('-dpng',[params.path '/coherence']);
print('-depsc2',[params.path '/coherence']);
close;

legend_labels = {};
coherence_matrix = []; % initialize coherence matrix
for i = 1:length(channel_coherences)
   if (isempty(channel_coherences(i).ff) | isempty(channel_coherences(i).coherence2) | isempty(channel_coherences(i).coherence1))
      continue
   end
   if isnan(sum(channel_coherences(i).coherenceRatio))
      continue
   end
   ff = channel_coherences(i).ff;
   coherence_matrix = [coherence_matrix log10(channel_coherences(i).coherenceRatio)];
   legend_labels{length(legend_labels)+1} = channel_coherences(i).channelName;
end

if log10(max(ff)) - log10(min(ff)) > 1
   doLog = 1;
else
   doLog = 0;
end

[numFreq,numChannels] = size(coherence_matrix);
channelIndexes = 1:numChannels;

[X,Y] = meshgrid(ff,channelIndexes);
figure;
set(gcf,'PaperUnits','inches')
set(gcf,'PaperSize',[60 48])
set(gcf,'PaperPositionMode','manual')
set(gcf,'PaperPosition',[0 0 40 34])
hC = pcolor(X,Y,coherence_matrix');
set(gcf,'Renderer','zbuffer');
colormap(jet)
%shading interp
t = colorbar('peer',gca);
set(get(t,'ylabel'),'String','Coherence Ratio (log10)');
set(t,'fontsize',20);
set(hC,'LineStyle','none');
grid
set(gca,'clim',[-2 2])
if doLog
   set(gca,'xscale','log');
%   set(gca,'yscale','log');
end
if doLog
   ticks = logspace(log10(fmin),log10(fmax),10);
   set(gca,'XTick',ticks)
else
   set(gca,'XTick',min(ff):20:max(ff))
end
set(gca,'YTick',channelIndexes+3.0)
set(gca,'YTickLabel',legend_labels)
pretty;
h_xlabel = get(gca, 'XLabel');
set(h_xlabel, 'FontSize', 14);
h_ylabel = get(gca, 'YLabel');
set(h_ylabel, 'FontSize', 14);
xlabel('Frequency [Hz]')
ylabel('Coherence Ratio')
print('-dpng',[params.path '/coherence_matrix']);
print('-depsc2',[params.path '/coherence_matrix']);
close;

