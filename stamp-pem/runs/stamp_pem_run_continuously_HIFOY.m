segmentDuration = 1;
fmin = 1; fmax = 128;

[junk,gpsEnd] = system('tconvert now - 30 minutes');
gpsEnd = str2num(gpsEnd);

run_name='HIFOY';
pemParamsFile = stamp_pem_createParamsFile(run_name);
params = readParamsFromFile(pemParamsFile);

framesMethod = 0;
gpsMethod = 1;

run = 1;
while run
  gps10min=0;
  [junk, gps10min]=system('tconvert now - 10 minutes');
  gps10min=str2num(gps10min);
  if (gps10min<gpsEnd)
    pause(256);
  end
   if framesMethod
      [junk,gpsStart] = system('tconvert now - 1000 minutes');
      [junk,gpsEnd] = system('tconvert now');
 
      gpsStart = str2num(gpsStart); gpsEnd = str2num(gpsEnd);
      
      params.startGPS = gpsStart; params.endGPS = gpsEnd;

      ligo_data_find_command = sprintf('ligo_data_find -o %s -t %s -s %d -e %d -u file',...
         params.ifo1(1),params.frameType1,params.startGPS,params.endGPS);
      [junk,frames1] = system(ligo_data_find_command);

      % Only return non-empty frame lines
      frames1 = regexp(frames1,'\n','split');
      frames1 = frames1(find( ~strcmpi(frames1,'')));

      % Return arrays of frame gps and duration
      gps1 = zeros(size(frames1));
      dur1 = zeros(size(frames1));

      for i = 1:length(frames1);
         frame1{i} = strrep(frames1{i},'file://localhost','');
         frame_split = regexp(strrep(frame1{i},'.gwf',''),'-','split');
         gps1(i) = str2num(frame_split{end-1});
         dur1(i) = str2num(frame_split{end});
      end

      ligo_data_find_command = sprintf('ligo_data_find -o %s -t %s -s %d -e %d -u file',...
         params.ifo2(1),params.frameType2,params.startGPS,params.endGPS);
      [junk,frames2] = system(ligo_data_find_command);

      % Only return non-empty frame lines
      frames2 = regexp(frames2,'\n','split');
      frames2 = frames2(find( ~strcmpi(frames2,'')));

      % Return arrays of frame gps and duration
      gps2 = zeros(size(frames2));
      dur2 = zeros(size(frames2));

      for i = 1:length(frames2);
         frame2{i} = strrep(frames2{i},'file://localhost','');
         frame_split = regexp(strrep(frame2{i},'.gwf',''),'-','split');
         gps2(i) = str2num(frame_split{end-1});
         dur2(i) = str2num(frame_split{end});
      end

      framesBack = 50;
      %gpsStart = gps1(end-framesBack); gpsEnd = gps1(end) + dur1(end);
      gpsEnd = gps1(end-framesBack) + dur1(end-framesBack);
      gpsStart = gpsEnd - 256;  

   elseif gpsMethod

      gpsStart = gpsEnd-4;
      gpsEnd = gpsStart + 256;

   end

   fprintf('Running STAMP-PEM on %d-%d\n',gpsStart,gpsEnd);
   stamp_pem_run_all(pemParamsFile,segmentDuration,gpsStart,gpsEnd,fmin,fmax)

end

