%run_name corresponds to the params file we'll choose. see the
%/stamp-pem/input directory for available params file. 
run_name='L1PEM';
pemParamsFile = stamp_pem_createParamsFile(run_name);
segmentDuration = 10;
startGPS = 1026288016;
endGPS = 1026892816;

GPS = startGPS:3600:endGPS;

for i = 1:length(GPS)-1
   try
      stamp_pem_run_all(pemParamsFile,segmentDuration,GPS(i),GPS(i+1))
      %system('sleep 5m')
   catch
   end
end


