#!/bin/bash

source ${HOME}/.bashrc
source ${HOME}/.bash_profile

cd ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/

/ldcg/matlab_r2012a/bin/matlab -nodisplay -nojvm<<EOF
 stamp_startup; 
 H1HIFOX_condor_summary_page
EOF


