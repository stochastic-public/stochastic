function output = stamp_pem_line_glitch(map, params)
% function output = stamp_pem_line_glitch(map, params)
% Given a STAMP map struct and STAMP params struct, returns an
% output struct containing the sum of rows and columns.
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Replace NaN's in map with zeroes
map.snr(isnan(map.snr)) = 0;

% Create gps array
output.t = map.segstarttime;
output.t_snr = sum(map.snr);

% Create frequency array
output.f = map.f;
output.f_snr = sum(map.snr,2);

return;
