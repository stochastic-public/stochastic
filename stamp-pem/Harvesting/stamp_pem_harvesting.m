function stamp_pem_harvesting(pemParamsFile, startGPS, endGPS, segmentDuration, challenger_channel, ranklistname)
% This is function is called in one of two cases: (1) clustermap has just been
% run on a single channel, the result ('challenger') are to be compared with previously
% produced files that have been identified as 'loud' events. (2) clustermap
% has just been run on a set of channels and this is used to compare events
% from those channels that were just run with one another, and then
% subsequently called again to compare the loudest events
% from that set of channels to previously identified 'loud' events. 
%
% pemParamsFile ------------BE SURE TO USE THE SAME PARAMS FILE USED IN
% --------------------------CREATING THE CHALLENGER CHANNEL RESULTS!!!
% --------------------------if you use a different one then the data won't be
% --------------------------found.
% params.webpage_path ------path to where webpage information is stored
% params.ranked_files_path--path to where directories containing ranked files
% --------------------------can be found. Those directories are today_ranks and alltime_ranks (and,
% --------------------------though it won't be used, yesterday_ranks)
% ranklistname -------------'today' or 'alltime' specifies whether we're
% --------------------------comparing to today's events or all time's events
%
% Written by Pat Meyers
if isstr(startGPS)
str2num(startGPS);
end
if isstr(endGPS)
str2num(endGPS);
end

%challenger_channel is channel that is a candidate for a 'loud' event. 
challenger_channel = strrep(challenger_channel, ':', '_');

params = readParamsFromFile(pemParamsFile);

params.path = [params.dirPath '/' params.ifo1 '/' params.runName '-' num2str(startGPS) '-'...
	       num2str(endGPS) '/' num2str(segmentDuration) '/' num2str(fmin) ...
	       '-' num2str(fmax) '/' challenger_channel];

challenger_file = [params.path , '/' challenger_channel '_cluster.mat'];
challenger = load(challenger_file);
%%Set path to pem Channel run's information

%%%%%%%%%Compare challenger to todays ranked competition%%%%%%%%%%%

%%% finds ranked files for today %%%%%%%%%
%%% Should eventually move today_ranks directory! %%%%%%%%%


ranked_files = dir([params.ranked_files_path '/' ranklistname '_ranks/*.mat']);

index=length(ranked_files);
if length(ranked_files)>10
error('Too many ranked files!!!');
end

%% If there are no ranked files today then the challenger is the top ranked
%% event for the day
if length(ranked_files)==0; 

save([params.ranked_files_path '/' ranklistname '_ranks/' challenger_channel '_rank1.mat'], '-struct', 'challenger');
%% write the information to our text file on webpage!


% replace snr plot
% snr plot is located in (for example) STAMP-PEM_WebPage/SP_today_events/today1/today_snr_1.png
copyfile([params.path '/snr.png'],...
	 [params.webpage_path '/SP_' ranklistname '_events/' ranklistname ...
	  '1/' ranklistname '_snr_1.png']);

% replace large cluster plot
% large cluster plot is located in (for example)
% STAMP-PEM_WebPage/SP_today_events/today1/today_cluster_1.png
copyfile([params.path '/large_cluster.png'],...
       	 [params.webpage_path '/SP_' ranklistname '_events/' ranklistname ...
	  '1/' ranklistname '_cluster_1.png']);

% replace text file to post necessary information
% text file is located in (for example)
% STAMP-PEM_WebPage/SP_today_events/today1/today1.txt

f = fopen([params.webpage_path '/SP_' ranklistname '_events/' ranklistname ...
	   '1/' ranklistname '1.txt'], 'w+');

fprintf(f, '%s\n', 'Channel Name');
fprintf(f, '%s\n', challenger_channel);
fprintf(f, '%s\n', 'Loudest SNR');
fprintf(f, '%f\n', challenger.cluster_out.max_SNR);
fprintf(f, '%s\n', 'Start GPS');
fprintf(f, '%i\n', startGPS);
fprintf(f, '%s\n', 'End GPS');
fprintf(f, '%i', endGPS);

fclose(f); 

end

if length(ranked_files)>0 & length(ranked_files)<=10
  for J= 1:length(ranked_files)
%%load the Jth ranked file so far
       champion_file=dir([params.ranked_files_path,'/' ranklistname '_ranks/*rank',num2str(J),'.mat']);
       champion = load([params.ranked_files_path,'/' ranklistname '_ranks/', champion_file.name]);

%%if  pem_rank(challenger, champion)
%%for now Ill use max SNR

if challenger.cluster_out.max_SNR >=  champion.cluster_out.max_SNR

%% function that moves everything down one rank
%% how it works is detailed in function itself
stamp_pem_reranking(params, ranked_files, J, ranklistname);

      g =  fopen([params.webpage_path '/SP_' ranklistname '_events/' ranklistname ...
		  ,num2str(J), '/' ranklistname , num2str(J), '.txt'], 'w+');
      fprintf(g, '%s\n', 'Channel Name');
      fprintf(g, '%s\n', challenger_channel);
      fprintf(g, '%s\n', 'Loudest SNR');
      fprintf(g, '%f\n', challenger.cluster_out.max_SNR);
      fprintf(g, '%s\n', 'Start GPS');
      fprintf(g, '%i\n', startGPS);
      fprintf(g, '%s\n', 'End GPS');
      fprintf(g, '%i', endGPS);
      fclose(g);

copyfile([params.path '/snr.png'],...
	 [params.webpage_path '/SP_' ranklistname '_events/' ranklistname ...
	  num2str(J) '/' ranklistname '_snr_' num2str(J) '.png']);
copyfile([params.path '/large_cluster.png'],...
	 [params.webpage_path '/SP_' ranklistname '_events/' ranklistname ...
	  num2str(J) '/' ranklistname  '_cluster_' num2str(J) '.png']);


save([params.ranked_files_path, '/' ranklistname '_ranks/', challenger_channel, '_rank', mat2str(J),'.mat'], '-struct', 'challenger');
%see below
fprintf('%s%s%s%s\n', challenger_channel, ' has produced a ranked event for ', ranklistname, '!')
index=10;
	  break;
 end %if
		 
  		
end %loop

 if index<10;
    
  %% save file in rank of index+1, since we dont have
  %% ten ranked files yet, and this one doesnt displace
  %% any of the other previously ranked ones


   k =  fopen([params.webpage_path,'/SP_' ranklistname '_events/' ranklistname ...
	       num2str(index+1) '/' ranklistname num2str(index+1) '.txt'], 'w+');
   fprintf(k, '%s\n', 'Channel Name');
fprintf(k, '%s\n', challenger_channel);
fprintf(k, '%s\n', 'Loudest SNR');
fprintf(k, '%f\n', challenger.cluster_out.max_SNR);
fprintf(k, '%s\n', 'Start GPS');
fprintf(k, '%i\n', startGPS);
fprintf(k, '%s\n', 'End GPS');
fprintf(k, '%i',   endGPS);
fclose(k);

copyfile([params.path '/snr.png'],...
         [params.webpage_path '/SP_' ranklistname '_events/' ranklistname ...
	  num2str(index+1) '/' ranklistname '_snr_', num2str(index+1), '.png']);
copyfile([params.path '/large_cluster.png'],...
         [params.webpage_path '/SP_' ranklistname '_events/' ranklistname ...
	  num2str(index+1) '/' ranklistname '_cluster_' num2str(index+1) '.png']);

save([params.ranked_files_path, '/' ranklistname '_ranks/', challenger_channel, '_rank',num2str(index+1),'.mat'], '-struct', 'challenger');

end %if statement


end %if statement