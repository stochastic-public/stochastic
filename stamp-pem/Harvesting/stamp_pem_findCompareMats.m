function stamp_pem_findCompareMats(pemParamsFile, startGPS, endGPS, segmentDuration, channelList, fmin, fmax);
% This function finds the myriad .mat files output by the automated pem
% analysis done over multiple channels (for now run, by mcoughlin). It sorts them,
% for now by max SNR, and then reads them in to stamp_pem_harvesting to be
% compared to the already ranked files
%
% pemParamsFile -------parameters defined and used for pem. MUST BE SAME
% ---------------------PARAMS FILE USED IN CREATING .MAT FILES TO BE
% ---------------------RETRIEVED
% channelList ---------list of channels over which we will be looking to get
% ---------------------.mat files for. Include this input if you are calling  
%
% 
%-----------------------------

%Read in our pem parameters
params = readParamsFromFile(pemParamsFile);


%if you want to input a file that has the channel information it will read
%from that text file. This way you can run the function on its own, or call
%it from another function.
try 
 [channelNames, channelSampleRates]=textread([channelList]);
catch %if you have input a cell array with the names of channels, then we'll
      %just set that to channelNames instead.
  channelNames = channelList;
end



index=0;

%Load the matfile for a channel at the given start and end GPS times
%specified in wrapper
%put max_SNR for that run into an array
for ii=1:length(channelNames)
  
  %index=index+1;
  
  channelNames(ii)=strrep(channelNames(ii), ':', '_');       
  
  try
    A =load([params.dirPath '/' params.ifo1 '/' params.runName '-' num2str(startGPS) '-' num2str(endGPS) '/' num2str(segmentDuration) '/'...
	   '/' num2str(fmin) '-' num2str(fmax) '/'  channelNames{ii} '/' channelNames{ii} '_cluster.mat']);
    %% We will just take max_SNR for now
    index=index+1
    B(index)=A.cluster_out.max_SNR;
  catch
  end
  
  
end %ii loop

%% Sort by max_SNR, C is sorted array
%% the number in the ith row of D is the row in B of the ith row of C
%% This way after sorting we can relate the correct channel to the correct
%% SNR. This is done in the loop that follows (kk loop) 
try
  [C, D]=sort(B, 'descend');
catch 
  error('No .mat files found!');
end

PotentialEvents = [];

%% just take the first ten
%% Channel Names

for kk=1:10
  PotentialEvents{kk} = channelNames{D(kk)};
  
  %% Call the harvesting file that relates the kk'th potentially important
  %% event to the already ranked events
  %% Eventually may want to pull out information from 10th ranked event and get
  %% rid of all potential events that won't make the list at all.
  
end %kkloop


fprintf('%s\n', 'Comparing to ranked events today...');

for jj = 1:length(PotentialEvents)
  stamp_pem_harvesting(pemParamsFile, startGPS, endGPS, segmentDuration, PotentialEvents{jj}, ...
		       'today');
end %jjloop

fprintf('%s\n', 'Done comparing to today...now comparing to all time...');

for mm = 1:length(PotentialEvents)
  stamp_pem_harvesting(pemParamsFile, startGPS, endGPS, segmentDuration, ...
		       PotentialEvents{mm}, 'alltime');
end
