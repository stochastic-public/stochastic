function pem_segments(channel_name,folder,user,ifo,frame_size)

gps_times_file=load('jobfile.txt');
gps_begin=gps_times_file(2);
gps_end=gps_times_file(3);

%channel_name='H0_PEM-LVEA_MIC';
%folder='2010_01_01';
%user='mcoughlin';
%frame_size='4';
%ifo='H1';

path=['/archive/home/' user '/STAMP/STAMP-PEM/' ifo '/daily/' ... 
      folder '/' num2str(frame_size)];

output=['/archive/home/' user '/public_html/STAMP/STAMP-PEM/' ifo '/daily/' ...
      folder '/' num2str(frame_size)];

folder_path=[path '/files/' channel_name];

airplane_stats_all=[];

all_data_files = dir([folder_path '/*']);

[nd_all,holder] = size(all_data_files);

for d=1:nd_all
 try
   airplane_stats=load([folder_path '/' all_data_files(d).name '/snr_value.txt']);
   airplane_stats_all = [airplane_stats_all; str2num(all_data_files(d).name) airplane_stats];
 catch
 end
end

A=airplane_stats_all;
[x, j] = sort(A(:,6),'descend');
B = A(j,:);
folder_number=B(:,1);
start_gps_times=B(:,2);
end_gps_times=B(:,3);
start_airplane_times=B(:,4);
end_airplane_times=B(:,5);
radon_values=B(:,6);

fid=fopen([path '/files/' channel_name '/segments.txt'],'w+');
for i=1:length(start_gps_times)
   fprintf(fid,'%.0f %.0f %.5f\n',start_gps_times(i),end_gps_times(i),radon_values(i)); 
end
fclose(fid);

fid=fopen([path '/files/' channel_name '/' channel_name '_radon.html'],'w+');

fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n')
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">%s Radon Page for %s</span></big></big></big></big><br>\n',channel_name,folder);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>');

for i=1:length(start_gps_times)
   fprintf(fid,'<table\n');
   fprintf(fid,'style="text-align: left; width: 1260px; height: 30px; margin-left:auto; margin-right: auto;"\n');
   fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n')
   fprintf(fid,'<tbody>\n');
   fprintf(fid,'<tr>\n');
   fprintf(fid,'<td\n');
   fprintf(fid,'style="text-align: center; vertical-align: top;"><big><big><big><big><span\n');
   fprintf(fid,'style="font-weight: bold;">Event %s</span></big></big></big></big><br>\n',num2str(i));
   fprintf(fid,'</td>\n');
   fprintf(fid,'</tr>\n');
   fprintf(fid,'</tbody>\n');
   fprintf(fid,'</table>\n');
   fprintf(fid,'<br>');

   fprintf(fid,'<table\n');
   fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
   fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n')
   fprintf(fid,'<tbody>\n');

   fprintf(fid,'<tr>\n');
   fprintf(fid,'<td\n');
   fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
   fprintf(fid,'style="font-weight: bold;">GPS Time of Block: %.1f - %.1f </span></big></big><br>\n',start_gps_times(i),end_gps_times(i));
   fprintf(fid,'</td>\n');
   fprintf(fid,'</tr>\n');

   fprintf(fid,'<tr>\n');
   fprintf(fid,'<td\n');
   fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
   fprintf(fid,'style="font-weight: bold;">Max Radon Power: %.5f </span></big></big><br>\n',radon_values(i));
   fprintf(fid,'</td>\n');
   fprintf(fid,'</tr>\n');

   fprintf(fid,'<tr>\n');
   fprintf(fid,'<td\n');
   fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
   fprintf(fid,'style="font-weight: bold;">Proposed Veto Times: %.1f - %.1f </span></big></big><br>\n',start_airplane_times(i),end_airplane_times(i));
   fprintf(fid,'</td>\n');
   fprintf(fid,'</tr>\n');

   fprintf(fid,'</tbody>\n');
   fprintf(fid,'</table>\n');
   fprintf(fid,'<br>');

   fprintf(fid,'<table\n');
   fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
   fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n')
   fprintf(fid,'<tbody>\n');
   fprintf(fid,'<tr>\n');
   fprintf(fid,'<td\n');
   fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
   fprintf(fid,'style="font-weight: bold;">Veto Map </span></big></big><br>\n');
   fprintf(fid,'</td>\n');
   fprintf(fid,'<td\n');
   fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
   fprintf(fid,'style="font-weight: bold;">Radon Reconstruction </span></big></big><br>\n');
   fprintf(fid,'</td>\n');
   fprintf(fid,'</tr>\n');
   fprintf(fid,'<tr>\n');
   fprintf(fid,'<td style="vertical-align: top;">\n');
   fprintf(fid,'<a href="%s/snr.eps"><img alt="" src="%s/snr.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',num2str(folder_number(i)),num2str(folder_number(i)));
   fprintf(fid,'</td>');
   fprintf(fid,'<td style="vertical-align: top;">\n');
   fprintf(fid,'<a href="%s/large_cluster_plot.eps"><img alt="" src="%s/large_cluster_plot.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',num2str(folder_number(i)),num2str(folder_number(i)));
   fprintf(fid,'</td>');
   fprintf(fid,'</tr>\n');
   fprintf(fid,'</tbody>\n');
   fprintf(fid,'</table>\n');
   fprintf(fid,'<br>');

end
