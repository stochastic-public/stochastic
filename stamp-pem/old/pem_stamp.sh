#!/bin/bash

offset=$1
ifo=$2
frame_size=$3

if [ $ifo == 'H1' ]; then
frame_type=H1_RDS_R_L1
ifo1=H
end=64
fi

if [ $ifo == 'L1' ]; then
frame_type=L1_RDS_R_L1
ifo1=L
end=64
fi

if [ $ifo == 'V1' ]; then
frame_type=CW_RDS
ifo1=V
end=5000
fi

source /opt/lscsoft/lscsoft-user-env.sh
source ${HOME}/MatlabSetup_R2007a_glnxa64.sh
source ${HOME}/.bashrc
source ${HOME}/.bash_profile

day=`date --date="$offset days ago" +'%b %d %Y'`
folder=`date --date="$offset days ago" +%Y_%m_%d`
folder_dif=`date --date="$offset days ago" +%y-%m-%d`
gps_start=`tconvert "$day 00:00:00 UTC"`
#gps_end=$(($gps_start+4752000))
#gps_end=$(($gps_start+86400))
gps_end=$(($gps_start+2592000))

rm -r ${HOME}/STAMP/STAMP-PEM/${ifo}/daily/${folder}
rm -r ${HOME}/public_html/STAMP/STAMP-PEM/${ifo}/daily/${folder}
mkdir ${HOME}/STAMP/STAMP-PEM/${ifo}/daily/${folder}
mkdir ${HOME}/STAMP/STAMP-PEM/${ifo}/daily/${folder}/${frame_size}
mkdir ${HOME}/public_html/STAMP/STAMP-PEM/${ifo}/daily/${folder}
mkdir ${HOME}/public_html/STAMP/STAMP-PEM/${ifo}/daily/${folder}/${frame_size}


path=${HOME}/STAMP/STAMP-PEM/${ifo}/daily/${folder}/${frame_size}
output_dir=${HOME}/public_html/STAMP/STAMP-PEM/${ifo}/daily/${folder}/${frame_size}

cd $path

cp -r ${HOME}/STAMP/STAMP-PEM/compiled_radon_code/* .
channel_list=pem_${ifo}_channel_list.txt
cp $channel_list pem_channel_list.txt

time=`date`
octave -q<<EOF
 pem_summary_page('${USER}','${time}','${folder}')
EOF

cp pem_summary_page.html ${output_dir}

perl pem_quick_segs.pl ${gps_start} ${gps_end} ${frame_type} ${ifo1} ${end}

matlab -q<<EOF
 pem_cache('${gps_start}','${gps_end}','${ifo}');
 pem_make_dag('${folder}','${USER}','${frame_size}','${ifo}')
EOF

source ${HOME}/matlab_script_64bit.sh

#condor_submit_dag -f -maxjobs 1000 pem_pipe.dag
