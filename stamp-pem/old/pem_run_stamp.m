function pem_run_stamp(channel_name,lineNumber,folder,user,frame_size,ifo)

if isstr(lineNumber)
   lineNumber=str2num(lineNumber);
end

if isstr(frame_size)
   frame_size=str2num(frame_size);
end

jobs=load('jobnumbers.txt');

if (sum(ismember(jobs,lineNumber))>0)
 try
   pem_lines_combine(channel_name,folder,user,ifo,frame_size)
 catch
 end
elseif lineNumber==0
   return
elseif lineNumber==jobs(end)+1
   pem_make_flag(folder,user,ifo)
   system_command=['cp -r files/ /archive/home/' user ... 
      '/public_html/STAMP/STAMP-PEM/' ifo '/daily/' folder '/4/'];
   system(system_command);
else
 try
   gps_times_file=load('jobfile_all.txt');
   gps_times_start=gps_times_file(:,2);
   gps_times_end=gps_times_file(:,3);

   which_jobs=1:100:length(gps_times_start);
   if lineNumber==length(which_jobs)
      these_jobs=which_jobs(lineNumber):length(gps_times_start);
   else
      these_jobs=which_jobs(lineNumber):(which_jobs(lineNumber+1)-1);
   end

   for i=1:length(these_jobs)
    try
      index = these_jobs(i);
      pem_params(folder,ifo,num2str(frame_size),user, ...
         gps_times_start(index), ...
         gps_times_end(index),channel_name);
      preproc(['files/' channel_name '/' channel_name '.txt'], ...
         'jobfile_all.txt',num2str(index))
      close all;
    catch
    end
   end

 catch
 end
end

