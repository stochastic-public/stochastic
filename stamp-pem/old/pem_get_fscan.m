function data_output = pem_get_fscan(which_days,channel_name,gw_freq)

fscan_location = '/archive/home/pulsar/public_html/fscan/dailyVSR4S6E/VirgoRDS/VirgoRDS';

data_output = NaN*ones(length(gw_freq),3);

which_folder = dir([fscan_location '/fscans_' which_days '*']);
file_location = [fscan_location '/' which_folder(1).name '/' channel_name];
which_files = dir([file_location '/spec_*.txt']);
which_files_split = regexp({which_files.name},'_','split');
file_freq = [];
file_indexes = [];
for j=1:length(which_files_split)
   if length(which_files_split{j})==6
      file_freq = [file_freq str2num(which_files_split{j}{2})];
      file_indexes = [file_indexes j];
   end
end

[which_file,indexes] = sort(file_freq,'ascend');

fscan_data = [];
for j=1:length(indexes)
   fscan_data_ind = load([file_location '/' which_files(indexes(j)).name]);
   fscan_data = [fscan_data; fscan_data_ind];
end

for j=1:length(gw_freq)
   which_freq = find(min(abs(fscan_data(:,1)-gw_freq(j))) == abs(fscan_data(:,1)-gw_freq(j)));
   data_output(j,1) = min(fscan_data(which_freq,2));

   which_freq = find(abs(fscan_data(:,1)-gw_freq(j)) <= 0.5);
   this_fscan_data = fscan_data(which_freq,:);

   which_freq_max = find(max(this_fscan_data(:,2)) == this_fscan_data(:,2));
   data_output(j,2) = min(this_fscan_data(which_freq_max,1));
   data_output(j,3) = min(this_fscan_data(which_freq_max,2));
end

