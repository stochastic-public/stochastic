function pem_saveplots(params, map)

  printmap(map.snr, map.xvals, map.yvals, 'time (s)', 'f (Hz)', 'SNR', [-5 5]);
  print('-dpng',[params.which_folder 'snr.png']);
  print('-depsc2',[params.which_folder 'snr.eps']);

%  printmap(map.y, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
%    'y (strain^2)', [-params.yMapScale params.yMapScale]);
%  print('-dpng',[params.which_folder 'y_map.png']);
%  print('-depsc2',[params.which_folder 'y_map.eps']);

%  map.w = map.snr./map.sigma;
%  printmap(map.w, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
%         'weight', [min(min(map.w)) max(max(map.w))]);
%  print('-dpng',[params.which_folder 'weight_map.png']);

%  missing_data_cut=(map.sigma==1);
%  logSigMap=zeros(size(map.sigma));
%  logSigMap(~missing_data_cut)=-log10(map.sigma(~missing_data_cut));
%  printmap(logSigMap, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
%    '-log_{10}(\sigma)', [min(-log10(map.sigma(~missing_data_cut))) ...
%    max(-log10(map.sigma(~missing_data_cut)))]);
%  print('-dpng',[params.which_folder 'sig_map.png']);
%  print('-depsc2',[params.which_folder 'sig_map.eps']);

  % fluence maps
%  printmap(map.F, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
%    'F (ergs/cm^2)', [-params.FMapScale params.FMapScale]);
%  print('-dpng',[params.which_folder 'F_map.png']);
%  print('-depsc2',[params.which_folder 'F_map.eps']);

%  logSigFMap=zeros(size(map.sigma));
%  logSigFMap(~missing_data_cut)=-log10(map.sigF(~missing_data_cut));
%  printmap(logSigFMap, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
%    '-log_{10}(\sigma)', [min(-log10(map.sigF(~missing_data_cut))) ...
%    max(-log10(map.sigF(~missing_data_cut)))]);
%  print('-dpng',[params.which_folder 'sigF_map.png']);
%  print('-depsc2',[params.which_folder 'sigF_map.eps']);

return
