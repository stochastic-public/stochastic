function d = getbarresponse(loc,axis);

%GETBARRESPONSE - Construct Cartesian response tensor from
%                 geographic coordinates & bar orientation
%
%  getbarresponse(loc,axis) calculates the Cartesian response tensor
%  associated with a resonant bar gravitational wave detector at a
%  location loc with arms whose long axis has an orientation described
%  by the structure's axis.
%
%SYNOPSIS
%   D = getbarresponse(LOC, AXIS)
%
%INPUT
%   LOC - input location structure with fields:
%      LAT: geodetic latitude (measured North from the Equator) in radians
%      LON: geodetic longitude (measured East from the Prime
%           Meridian) in radians
%      HEIGHT: elevation in meters above the WGS-84 reference ellipsoid 
%  Such a structure can be created from the geographic coordinates (in
%  degrees) using the function CREATELOCATION
%   AXIS - structure with the fields:
%      AZ: azimuth in radians East (clockwise) of North
%      ALT: altitude (tilt) angle in radians above the local tangent plane
%  Such a structure can be created from the local angles (in degrees)
%  using the function CREATEORIENTATION
% 
%OUTPUT
%   D - response tensor
%
%  The function calls GETCARTESIANDIRECTION to convert the
%  orientation angles for the axis into a Cartesian unit vector
%  pointing along the axis, then constructs the response tensor as
%  the outer product of this unit vector with itself.
%
%AUTHOR
%   John T. Whelan <john.whealn@ligo.org>

%  $Id$

u = getcartesiandirection(axis,loc);

d = u * transpose(u);
