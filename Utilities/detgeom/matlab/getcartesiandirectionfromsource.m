function n = getcartesiandirectionfromsource(dec,minusha);

%GETCARTESIANDIRECTIONFROMSOURCE -- Construct Cartesian unit vector 
%                                   for wave propagation from
%                                   declination and minus hour angle
%                                    
%  getcartesiandirectionfromsource(dec,minusha) calculates the unit
%  vector n in Earth-based Cartesian coordinates corresponding to a
%  given declination and minus hour angle (which is equal to the
%  source's right ascension minus the Greenwich mean sidereal time
%  (GMST) at which it is observed).
%
%SYNOPSIS 
%   N = getcartesiandirectionfromsource(DEC, MINUSHA)
%
%INPUTS
%   DEC - declination in degrees north of the celestial equator
%   MINUSHA -  minus the hour angle (i.e., right ascension minus GMST)
%             in hours EAST of Greenwich
%  
%OUTPUT
%  U - Earth-fixed Cartesian unit vector point to ORI located at LOC.
%      Define coordinate system whose first, second, and third axes
%      pierce the surface of the Earth at
%         1. The intersection of the Equator and the Prime Meridian
%         2. The intersection of the Equator and the meridian at 90
%            degrees East longitude
%         3. The North Pole
%
%  The function first converts the angles into radians and then
%  calculates the components of the unit vector pointed from the
%  source to the center of the Earth-based coordinate system
%
%AUTHOR
%  John Whelan <john.whelan@ligo.org>
%
%  See also GETCARTESIANDIRECTION

%  $Id$

cosdelta = cos(dec*pi/180);
  
n = [-cosdelta.*cos(minusha*pi/12);...
     -cosdelta.*sin(minusha*pi/12);...
     -sin(dec*pi/180)];
return;
  
