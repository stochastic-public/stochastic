function [fs1ovl,fs2ovl] = findoverlap(fs1,fs2);
% FINDOVERLAP -- find overlapping portions of frequency series
%
% [fs1ovl,fs2ovl] = findoverlap(fs1,fs2) returns the overlapping parts
%        of two frequency series
% 
% The inputs are two frequency series structures which must have the
% same frequency resolution (deltaF).  The outputs are two frequency
% series structures which are subsets of the two input series, both
% covering the same range of frequencies.  This range contains all the
% frequencies at which both input series are defined.
%
%  Routine written by John T. Whelan.
%  Contact john.whelan@ligo.org
%  $Id$
%
%  See also CONSTRUCTFREQSERIES
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (fs1.deltaF ~= fs2.deltaF)
  error('frequency step mismatch');
end;

f1 = getfreqs(fs1);
f2 = getfreqs(fs2);

fmin = max(f1(1),f2(1));
fmax = min(f1(end),f2(end));

i1min = find(f1==fmin);
i1max = find(f1==fmax);
i2min = find(f2==fmin);
i2max = find(f2==fmax);

frange = f1(i1min:i1max);
fs1ovl_data = fs1.data(i1min:i1max);
fs2ovl_data = fs2.data(i2min:i2max);

fs1ovl = constructFreqSeries(fs1ovl_data,frange(1),fs1.deltaF);
fs2ovl = constructFreqSeries(fs2ovl_data,frange(1),fs2.deltaF);
