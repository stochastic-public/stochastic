function str = setStrDef(str,strDef)
% SETSTRDEF recursively set empty or missing structure fields to defaults
%
% str = setStrDef(str,strDef)
%
% str     structure whose fields need a check/set
% strDef  default structure
%
% For fields f in both str and strDef: if str.(f) is empty set to
% strDef.(f); otherwise leave unchanged
% For fields f in str but not strDef: leave unchanged
% For fields in strDef but not in str: set to strDef.(f)
%
% Author: Lee Samuel Finn
% Copyright 2010-2011


fNames = fieldnames(strDef);
for k = 1:numel(fNames)
  fn = fNames{k};
  str.(fn) = setDef(str,fn,strDef.(fn));
end

return

function x = setDef(s,f,d)
% SETDEF recursively set struct field defaults
%
% x = setDef(s,f,d)
%
% s    struct
% f    field (string)
% d    default if s has no field f or s.(f) unset
%
% x    fully-set field f
%
% if d is a struct then operates recursively on all fields of d

if isfield(s,f)
  if isempty(s.(f))
    x = d;
  elseif isstruct(d)
    x = setStrDef(s.(f),d);
  else
    x = s.(f);
  end
else
  x = d;
end

return