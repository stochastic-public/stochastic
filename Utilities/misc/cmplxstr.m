function string = cmplxstr(format, number);

%CMPLSTR - converts a complex number into a printable string
%
%SYNOPSIS
% STRING = cmpxstr(FORTMAT, NUMBER)
%
%INPUT
%  FORMAT - string format in sprintf form
%  NUMBER - complex number to be converted to a string
%
%OUTPUT
%  STRING - string version of the complex NUMBER input
%
%USAGE EXAMPLES
%  >> cmplxstr('%.4f',sqrt(i))
%  ans = 0.7071 + 0.7071i
%  >> cmplxstr('%.4f',-sqrt(i))
%  ans = -0.7071 - 0.7071i
%  >> cmplxstr('%d',sqrt(-4))
%  ans = 2i
%  >> cmplxstr('%d',37)
%  ans = 37
%  >> cmplxstr('%f',0)
%  ans = 0.000000
%
%AUTHOR
%  John Whelan <john.whelan@ligo.org>

% $Id$

% Backwards-compatibility in case someone reverses format and number
if ~isnumeric(number)
  foo = number;
  number = format;
  format = foo;
end
  
if imag(number) == 0
  string = sprintf(format,number);
elseif real(number) == 0
  string = sprintf([format 'i'],imag(number));
elseif imag(number) < 0
  string = sprintf([format ' - ' format 'i'],real(number),-imag(number));
else
  string = sprintf([format ' + ' format 'i'],real(number),imag(number));
end;

return;
