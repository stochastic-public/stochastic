function customverctrl(fileNames, arguments)
% CUSTOMVERCTRL
%
% customverctrl(FILENAMES, 'Param1','Value1',...) 
%
%   FILENAMES     full path of the file or a cell array of files.
%   Param/Value   pairs identify the version control action to be performed
%
%   Param         Value
%   'action'      'checkin', 'checkout', 'undocheckout'
%   'lock'        'on', 'off' -- no-op for svn
%   'comments'    comment string (required for checkin action)
%   'revision'    revision number
%   'outputfile'  file name
%   'force'       no-op
%
% Performs the requested action on the specified files. If 'revision' is
% specified the action is performed on the indicated revision. If
% 'outputfile' is specified the results of the version control command are
% written to the designated file. 
%
% See also CHECKIN, CHECKOUT, UNDOCHECKOUT, CMOPTS, CUSTOMVERCTRL,
% SOURCESAFE, CLEARCASE, CVS, and PVCS. 
%
% Copyright 1998-2008 The MathWorks, Inc.
% $Revision: 1.2.4.2 $  $Date: 2008/02/29 12:46:57 $

% Mandatory argument
action     = arguments(find(strcmp(arguments, 'action')), 2);
% Mandatory if checkin is the action
comments   = arguments(find(strcmp(arguments, 'comments')), 2);    
revision   = arguments(find(strcmp(arguments, 'revision')), 2);
outputfile = arguments(find(strcmp(arguments, 'outputfile')), 2);
force      = arguments(find(strcmp(arguments, 'force')), 2);

msgUnk = [mfilename ':unknownAction'];
msgSysFail = [mfilename ':sysCmdFail'];

% Checking for mandatory arguments
if (isempty(action))
  error('MATLAB:sourceControl:noActionSpecified','No action specified.');
else
  action = action{1};
end

% Create space delimitted string of file names
files = '';
for i = 1 : length(fileNames)
  files = [files ' ' fileNames{i}];
end

switch action
  case 'checkin'
    % Checking for mandatory arguments
    if (isempty(comments))
      error(...
        'MATLAB:sourceControl:noCommentSpecified',...
        'Comments not specified');
    else
      comments = comments{1};
      comments = cleanupcomment(comments);
    end
    % Check to see if this is a new file or an existing file.
    try
      command = ['svn --quiet log ' files];
      [s, ~] = system(command);
      if (s == 1) % New file
        command = ['svn --quiet add ' files];
        [s, ~] = system(command);
        if (s == 1)
          error(msgSysFail,'%s %s',command);
        end
      end
      command = ['svn --quiet commit -m "' comments '" ' files];
      [s, ~] = system(command);
    catch ME
      error(ME.identifier,'%s %s',ME.identifier,ME.cause);
    end
    if (s == 1)
      error(msgSysFail,'%s %s',command);
    end
    
  case 'checkout'
    if (isempty(revision))
      revision = '';
    else
      revision = revision{1};
    end
    
    % Building the command string.
    command = 'svn --quiet ';
    if isempty(outputfile)
      command = [command 'checkout '];
    else
      command = [command 'cat'];
    end
    if ~isempty(revision)
      command = [command ' -r' revision];
    end
    
    command = [command ' ' files];
    
    % SHOULD BE THE LAST OPTION AS REDIRECTING STD OUTPUT
    if ~isempty(outputfile)
      command = [command ' > ' outputfile{1}];
    end
    
    [s, ~] = system(command);
    if (s == 1)
      error(msgSysFail,'%s %s',msgSysFail,command);
    end
    
  case 'undocheckout'
    command = ['svn --quiet revert ' files];
    [s, ~] = system(command);
    if (s == 1)
      error(msgSysFail,'%s %s',msgSysFail,command);
    end
    
  otherwise,
    error(msgUnk,'%s',msgUnk);
end
