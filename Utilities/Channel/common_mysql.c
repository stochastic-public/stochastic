/*-------------------------------------------------------------------------
 *
 * File Name: common_mysql.c
 *
 * Author: M. Foster, K. Thorne
 *
 * Revision: $Id: common_mysql.c,v 1.1 2004-08-11 18:43:34 kathorne Exp $
 *
 *-------------------------------------------------------------------------
 *
 * FUNCTIONS
 * print_dashes
 * process_result_set
 * process_result_set_original
 * process_result_set_with_dashes
 * process_query
 * print_error
 * do_connect
 * do_disconnect
 *
 * DESCRIPTION
 * Functions to do mySQL queries
 *
 *-------------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common_mysql.h"

/*
 * In older MySQL versions, mysql_field_count is not defined.
 *  in these cases, set it = to the number of fields
 */

#if !defined(MYSQL_VERSION_ID) || MYSQL_VERSION_ID<32224
#define mysql_field_count mysql_num_fields
#endif

/*-------------------------------------------------------------------------
 *
 * NAME
 * print_dashes
 *
 * SYNPOSIS
 * void print_dashes (MYSQL_RES *res_set);
 *
 * DESCRIPTION
 * Print dashes aligned with field names
 *-------------------------------------------------------------------------
 */
void print_dashes (MYSQL_RES *res_set)
{
    MYSQL_FIELD     *field;
    unsigned int    i, j;

    mysql_field_seek (res_set, 0);
    fputc ('+', stdout);
    for (i = 0; i < mysql_num_fields (res_set); i++)
    {
        field = mysql_fetch_field (res_set);
        for (j = 0; j < field->max_length + 2; j++)
        {
            fputc ('-', stdout);
        }
        fputc ('+', stdout);
    }
    fputc ('\n', stdout);
}

/*-------------------------------------------------------------------------
 *
 * NAME
 * process_result_set
 *
 * SYNPOSIS
 * void process_result_set (MYSQL *conn, MYSQL_RES *res_set, struct Fnlist *Fnlist);
 *
 * DESCRIPTION
 *  Fill Fnlist structure with results in each row
 *      field #1  file name
 *      field #2  gps start time
 *      field #3  duration
 *-------------------------------------------------------------------------
 */
int process_result_set (MYSQL *conn, MYSQL_RES *res_set, struct FNList *fnlist)
{
    MYSQL_ROW       row;
    int    i,j=0;
    int numRows = 0;
/*
 * Note that there is known to be only one field (row[0]) per row for this query
 * Additional fields would be represented as row[1],row[2], etc.
*/

/*P SET # of files to 0 (so failure is seen in #files = 0     */
/*P GET # of rows in query result                             */
/*P IF memory has not been allocated for the Fnlist structure */
/*P     ALLOCATE memory for pointers to file names            */
/*P     IF allocation fails                                   */
/*P         Throw error                                       */
/*P     ENDIF                                                 */
/*P     ALLOCATE memory for pointers to gps Starts            */
/*P     IF allocation fails                                   */
/*P         Throw error                                       */
/*P     ENDIF                                                 */
/*P     ALLOCATE memory for pointers to duration              */
/*P     IF allocation fails                                   */
/*P         Throw error                                       */
/*P     ENDIF                                                 */
/*P ELSE                                                      */
/*P     IF # of query rows > allocated rows                   */
/*P         Throw error                                       */
/*P         Exit                                              */
/*P     ENDIF                                                 */
/*P ENDIF                                                     */

    fnlist->nfiles = 0;
    numRows = mysql_num_rows (res_set);
    if (NULL == fnlist->namlist || 0 == fnlist->maxfiles)
    {
        fnlist->namlist = malloc(numRows * sizeof(char *));
        if (NULL == fnlist->namlist)
        {
            fprintf(stderr, "PROCESS_RESULT_SET: malloc failed for namlist\n");
            return 8;
        }
        fnlist->gpslist = malloc(numRows * sizeof(char *));
        if (NULL == fnlist->gpslist)
        {
            fprintf(stderr, "PROCESS_RESULT_SET: malloc failed for gpslist\n");
            return 9;
        }
        fnlist->durlist = malloc(numRows * sizeof(char *));
        if (NULL == fnlist->namlist)
        {
            fprintf(stderr, "PROCESS_RESULT_SET: malloc failed for durlist\n");
            return 10;
        }
    }
    else
    {
        if (numRows > fnlist->maxfiles)
        {
            fprintf(stderr, "PROCESS_RESULT_SET: query %d exceeds # of allocated rows %d\n",numRows,fnlist->maxfiles);
            return 11;
        }
    }

/* Fetch all the  rows in order to assign them to the Fnlist structure. */
/*P LOOP over rows                                                      */
/*P   IF Row Pointer is NOT null                                        */
/*P     FIND length of string in first field (File Name)                */
/*P     ALLOCATE memory for local copy of string                        */
/*P     COPY first field string to local pointer location               */
/*P     SET nth pointer of file name array to this location             */
    while ((row = mysql_fetch_row (res_set)) != NULL)
    {
        char* fileptr;
        int filelen;
        filelen = strlen(row[0]);
        fileptr = malloc((filelen+1)*sizeof(char));
        if(NULL == fileptr)
        {
            fprintf(stderr, "PROCESS_RESULT_SET malloc failed for fileptr\n");
            return 12;
        }
        memset(fileptr,0,filelen+1);
        memcpy(fileptr,row[0],filelen);
        fnlist->namlist[j] = fileptr;

/*P     IF there is a second field (assumed to be GPS start)            */
/*P         ALLOCATE memory for string                                  */
/*P         COPY second field to this memory location                   */
/*P         SET n-th pointer of GPS start array to this location        */
/*P     ELSE                                                            */
/*P         SET n-th pointer of GPS start array to NULL                 */
/*P     ENDIF                                                           */
        if(mysql_num_fields(res_set) > 1)
        {
            char* gpsptr;
			int gpslen;
			gpslen = strlen(row[1]);
			gpsptr = malloc((gpslen+1)*sizeof(char));
            if(NULL == gpsptr)
            {
                fprintf(stderr, "PROCESS_RESULT_SET malloc failed for gpsptr\n");
                return 13;
            }
			memset(gpsptr,0,gpslen+1);
			memcpy(gpsptr,row[1],gpslen);
			fnlist->gpslist[j] = gpsptr;
			}
		else
		{
		    fnlist->gpslist[j] = NULL;
		}

/*P     IF there is a third field (assumed to be duration)              */
/*P         ALLOCATE memory for string                                  */
/*P         COPY second field to this memory location                   */
/*P         SET n-th pointer of duration array to this location         */
/*P     ELSE                                                            */
/*P         SET n-th pointer of duration array to NULL                  */
/*P     ENDIF                                                           */
/*P   ENDIF                                                             */
/*P END LOOP                                                            */
        if(mysql_num_fields(res_set) > 2)
        {
            char* durptr;
			int durlen;
			durlen = strlen(row[2]);
			durptr = malloc((durlen+1)*sizeof(char));
            if(NULL == durptr)
            {
                fprintf(stderr, "PROCESS_RESULT_SET malloc failed for durptr\n");
                return 14;
            }
			memset(durptr,0,durlen+1);
			memcpy(durptr,row[2],durlen);
			fnlist->durlist[j] = durptr;
			}
		else
		{
		    fnlist->durlist[j] = NULL;
		}
        j++;
    }

/*P set # of files to the # of rows                    */
    fnlist->nfiles = numRows;
    return 0;
}

/*-------------------------------------------------------------------------
 *
 * NAME
 * process_result_set_original
 *
 * SYNPOSIS
 * void process_result_set_original (MYSQL *conn, MYSQL_RES *res_set)
 *
 * DESCRIPTION
 * original version of process_result_set
 *-------------------------------------------------------------------------
 */
void process_result_set_original (MYSQL *conn, MYSQL_RES *res_set)
{
    MYSQL_ROW       row;
    unsigned int    i;

    while ((row = mysql_fetch_row (res_set)) != NULL)

    {
        for (i = 0; i < mysql_num_fields (res_set); i++)
        {

            if (i > 0)
                fputc ('\t', stdout);
            printf ("%s", row[i] != NULL ? row[i] : "NULL");
        }
        fputc ('\n', stdout);
    }
    if (mysql_errno (conn) != 0)
        print_error (conn, "mysql_fetch_row() failed");
    else
        printf ("%lu rows returned\n", (unsigned long) mysql_num_rows (res_set));
}

/*-------------------------------------------------------------------------
 *
 * NAME
 * process_result_set_with_dashes
 *
 * SYNPOSIS
 * void process_result_set_with_dashes (MYSQL *conn, MYSQL_RES *res_set);
 *
 * DESCRIPTION
 *
 *-------------------------------------------------------------------------
 */
void process_result_set_with_dashes (MYSQL *conn, MYSQL_RES *res_set)
{
    MYSQL_FIELD     *field;
    MYSQL_ROW       row;
    unsigned int    i, col_len;

    /* determine column display widths */
    mysql_field_seek (res_set, 0);


    for (i = 0; i < mysql_num_fields (res_set); i++)
    {
        field = mysql_fetch_field (res_set);
        col_len = strlen (field->name);
        if (col_len < field->max_length)
            col_len = field->max_length;
        if (col_len < 4 &&!IS_NOT_NULL (field->flags))
            col_len = 4;    /* 4 = length of the word "NULL" */
        field->max_length = col_len;    /* reset column info */
    }

    print_dashes (res_set);
    fputc ('|', stdout);
    mysql_field_seek (res_set, 0);
    for (i = 0; i < mysql_num_fields (res_set); i++)
    {
        field = mysql_fetch_field (res_set);
        printf (" %-*s |", field->max_length, field->name);
    }
    fputc ('\n', stdout);
    print_dashes (res_set);

    while ((row = mysql_fetch_row (res_set)) != NULL)
    {
        mysql_field_seek (res_set, 0);
        fputc ('|', stdout);
        for (i = 0; i < mysql_num_fields (res_set); i++)
        {
            field = mysql_fetch_field (res_set);
            if (row[i] == NULL)
                printf (" %-*s |", field->max_length, "NULL");
            else if (IS_NUM (field->type))
                printf (" %*s |", field->max_length, row[i]);
            else
                printf (" %-*s |", field->max_length, row[i]);
        }
        fputc ('\n', stdout);
    }
    print_dashes (res_set);
    printf ("%lu rows returned\n", (unsigned long) mysql_num_rows (res_set));
}

/*-------------------------------------------------------------------------
 *
 * NAME
 * process_query
 *
 * SYNPOSIS
 * void process_query (MYSQL *conn, char *query, struct Fnlist *Fnlist)
 *
 * DESCRIPTION
 * Process mySQL query and return list of records
 *-------------------------------------------------------------------------
 */
int process_query (MYSQL *conn, char *query, struct FNList *fnlist)
{
    MYSQL_RES *res_set;
    unsigned int field_count;
    int status = 0;
    
    if (mysql_query (conn, query) != 0) /* the query failed */
    {
        print_error (conn, "PROCESS_QUERY: mysql query failed");
        return(3);
    }

    /* the query succeeded; determine whether or not it returns data */

    res_set = mysql_store_result (conn);
    if (res_set == NULL)    /* no result set was returned */
    {
        /*
         * does the lack of a result set mean that an error
         * occurred or that no result set was returned?
         */
        if (mysql_field_count (conn) > 0)
        {
            /*
             * a result set was expected, but mysql_store_result()
             * did not return one; this means an error occurred
             */
            print_error (conn, "Problem processing result set");
            return(4);
        }
        else
        {
            /*
             * no result set was returned; query returned no data
             * (it was not a SELECT, SHOW, DESCRIBE, or EXPLAIN),
             * so just report number of rows affected by query
             */
            printf ("%lu rows affected\n",
                        (unsigned long) mysql_affected_rows (conn));
            return(5);
        }
    }
    else    /* a result set was returned */
    {
        /* process rows, then free the result set */
        status = process_result_set (conn, res_set, fnlist);
        mysql_free_result (res_set);
        if(status > 0)
        {
            return(status);
        }
        else
        {
            return(0);
        }
    }
}


/*-------------------------------------------------------------------------
 *
 * NAME
 * print_error
 *
 * SYNPOSIS
 * void print_error (MYSQL *conn, char *message);
 *
 * DESCRIPTION
 * Prints error messages
 *-------------------------------------------------------------------------
 */
void print_error (MYSQL *conn, char *message)
{
    fprintf (stderr, "%s\n", message);
    if (conn != NULL)
    {
        fprintf (stderr, "Error %u (%s)\n",
                mysql_errno (conn), mysql_error (conn));
    }
}

/*-------------------------------------------------------------------------
 *
 * NAME
 * do_connect
 *
 * SYNPOSIS
 * MYSQL *do_connect (char *host_name, char *user_name, char *password, char *db_name,
 *           unsigned int port_num, char *socket_name, unsigned int flags)

 *
 * DESCRIPTION
 * Create a connection to a mySQL database
 *-------------------------------------------------------------------------
 */
MYSQL *do_connect (char *host_name, char *user_name, char *password, char *db_name,
            unsigned int port_num, char *socket_name, unsigned int flags)
{
    MYSQL   *conn;  /* pointer to connection handler */

    conn = mysql_init (NULL);  /* allocate, initialize connection handler */
    if (conn == NULL)
    {
        print_error (NULL, "mysql_init() failed (probably out of memory)");
        return (NULL);
    }
#if defined(MYSQL_VERSION_ID) &&MYSQL_VERSION_ID >= 32200  /* 3.22 and up */
    if (mysql_real_connect (conn, host_name, user_name, password,
                db_name, port_num, socket_name, flags) == NULL)
    {
        print_error (conn, "mysql_real_connect() failed");
        return (NULL);
    }
#else                           /* pre-3.22 */
    if (mysql_real_connect (conn, host_name, user_name, password,
                    port_num, socket_name, flags) == NULL)
    {
        print_error (conn, "mysql_real_connect() failed");
        return (NULL);
    }

    if (db_name != NULL)        /* simulate effect of db_name parameter */
    {
        if (mysql_select_db (conn, db_name) != 0)
        {
            print_error (conn, "mysql_select_db() failed");
            mysql_close (conn);
            return (NULL);
        }
    }
#endif
    return (conn);          /* connection is established */
}

/*-------------------------------------------------------------------------
 *
 * NAME
 * do_disonnect
 *
 * SYNPOSIS
 * void do_disconnect (MYSQL *conn)
 *
 * DESCRIPTION
 * Disconnect from mySQL database
 *-------------------------------------------------------------------------
 */
void do_disconnect (MYSQL *conn)
{
    mysql_close (conn);
}
