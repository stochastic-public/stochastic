function alignOk = framealigntest(frInst,frType,frChan,testGps,rptFile)
% FRAMEALIGNTEST - tests sample alignment when reading frames
% alignOk = framealigntest(frInst,frType,frChan,testGps[,rptFile])
%       frInst - instrument field in frame file name
%       frType - type field in frame file name
%       frChan - channel to read from frames
%       testGps - GPS time to conduct test for
%       rptFile - [OPTIONAL] report file
%
%   alignOk = 0 passes tests
%           > 0 fails tests
%
%  - Tests that frame reading grabs correct fragement of 
%  frame by reading frame starting at different times and
%  checking for sample matches
%
%  $Id: framealigntest.m,v 1.3 2008-05-15 13:35:39 kathorne Exp $

%  Setup to read frames
%  Define GPS interval around test time
%  Get list of frames
%  Using first frame, set up test reads
%  1) Read whole frame
%  2) Start reading 1 second in from ends
%      Check that samples match those from whole frame
%  3) Start reading 2 seconds from ends
%      Check that samples match those from whole frame
%  4) Start read 3.5 seconds from frame start
%      Check that samples match those from whole frame
alignOk = 2;
error(nargchk(4,5,nargin),'struct');
if(nargin > 4)
    fid = fopen(rptFile,'w');
    fprintf(fid,' Frame-Reading Alignment Test\n');
    fprintf(fid,' Test Frames: %s-%s-##-##.gwf\n',frInst,frType);
    fprintf(fid,' Test Channel: %s\n',frChan);
    fprintf(fid,' Requested GPS time: %g\n',testGps);
else
    fid = [];
end
gpsStart = testGps - 200;
gpsDur = 400;
frameList = framelist(frInst,frType,gpsStart,gpsDur);
gpsTimes = frameList.gpsTimes;
if(numel(gpsTimes) < 1)
    msgId = 'framealigntest.noFrames';
    if(~isempty(fid))
        fprintf(fid,' && No frames found !!\n');
        fclose(fid);
    end
    error(msgId,' No frames found');
end
frameDur = frameList.frameDurs;
fName = frameList.frameFiles;
testFrame = char(fName{1,1});
[fPath,testFrame,feExt] = fileparts(testFrame);
startGps = gpsTimes(1);
startDur = frameDur(1);
if(~isempty(fid))
    fprintf(fid,' Frame to test: %s\n',testFrame);
    fprintf(fid,' Starts at %09d duration %f\n',startGps,startDur);
end

tChan = chanstruct(frChan);
tChan.instrument = frInst;
tChan.type = frType;
[vect1,rate1,err1] = chanvector(tChan,startGps,startDur);
if(err1 ~= 0)
    msgId = 'framealigntest:readFail';
    if(~isempty(fid))
        fprintf(fid,' && Frame read with no offset failed!!\n');
        fclose(fid);
    end
    error(msgId,' Frame read with no offset failed with error %d',...
        err1);
end
%
%  Test with 1 second offset
%
fprintf(' -- Start test with 1 sec offset --\n');
if(~isempty(fid))
    fprintf(fid,' -- Start test with 1 sec offset --\n');
end
start2 = startGps + 1;
dur2 = startDur - 2;
[vect2,rate2,err2] = chanvector(tChan,start2,dur2);
if(err2 ~= 0)
    msgId = 'framealigntest:readFail';
    if(~isempty(fid))
        fprintf(fid,' && Frame read with 1 sec offset failed!!\n');
        fclose(fid);
    end
    error(msgId,' Frame read with 1 sec offset failed with error %d',...
        err2);
end
if(rate2 ~= rate1)
    msgId = 'framealigntest:badRate';
    if(~isempty(fid))
        fprintf(fid,...
      ' Rate with 1 sec offset %f does not match full rate %f\n',...
        rate2,rate1);
    end
    warning(msgId,...
      ' Rate with 1 sec offset %f does not match full rate %f',...
        rate2,rate1);
end

% Start by checking the front end
beg1 = 1 + rate1;
end1 = beg1 + rate1;
beg2 = 1;
end2 = 1 + rate1;
test1 = vect1(beg1:end1);
test2 = vect2(beg2:end2);
diffOk = diffcheck(test1,test2);
if(diffOk ~= true)
    msgId = 'framealigntest:badMatch';
    warning(msgId,' Read with 1 sec offset not aligned at start');
    alignOk = 1;
    if(~isempty(fid))
        fprintf(fid,'Read with 1 sec offset not aligned at start - FAIL\n');
    end
    % now try to determine offset
    chk1A = test1(1:end-1);
    chk2A = test2(2:end);
    diffA = diffcheck(chk1A,chk2A);
    chk1B = test1(2:end);
    chk2B = test2(1:end-1);
    diffB = diffcheck(chk1B,chk2B);
    if(diffA == true && diffB ~= true)
        fprintf(' New data is offset by +1 sample!\n');
        if(~isempty(fid))
            fprintf(fid,'Data is offset by +1 sample!\n');    
        end
    end
    if(diffA ~= true && diffB == true)
        fprintf(' New data is offset by -1 sample!\n');
        if(~isempty(fid))
            fprintf(fid,'Data is offset by -1 sample!\n');    
        end
    end    
else
    fprintf('Read with 1 sec offset aligned at start - OK\n');
    if(~isempty(fid))
        fprintf(fid,'Read with 1 sec offset aligned at start - OK\n');
    end
end

%  ** Check the back end
last1 = numel(vect1) - round(1*rate1);
frst1 = last1 - floor(0.5*rate1);
last2 = numel(vect2);
frst2 = last2 - floor(0.5*rate1);
test1 = vect1(frst1:last1);
test2 = vect2(frst2:last2);
diffOk = diffcheck(test1,test2);
if(diffOk ~= true)
    msgId = 'framealigntest:badMatch';
    warning(msgId,' Read with 1 sec offset not aligned at end');
    alignOk = 1;
    if(~isempty(fid))
        fprintf(fid,'Read with 1 sec offset not aligned at end- FAIL\n');
    end
else
    fprintf('Read with 1 sec offset aligned at end - OK\n');
    if(~isempty(fid))
        fprintf(fid,'Read with 1 sec offset aligned at end - OK\n');
    end
end

%
% Test with 2 second offset
%
fprintf(' -- Start test with 2 sec offset --\n');
if(~isempty(fid))
    fprintf(fid,' -- Start test with 2 sec offset --\n');
end
start3 = startGps + 2;
dur3 = startDur - 4;
[vect3,rate3,err3] = chanvector(tChan,start3,dur3);
if(err3 ~= 0)
    msgId = 'framealigntest:readFail';
    if(~isempty(fid))
        fprintf(fid,' && Frame read with 2 sec offset failed!!\n');
        fclose(fid);
    end
    error(msgId,' Frame read with 2 sec offset failed with error %d',...
        err3);
end
if(rate3 ~= rate1)
    msgId = 'framealigntest:badRate';
    if(~isempty(fid))
        fprintf(fid,...
      ' Rate with 2 sec offset %f does not match full rate %f\n',...
        rate3,rate1);
    end    
    warning(msgId,...
        ' Rate with 2 sec offset %f does not match full rate %f',...
        rate3,rate1);
end

% * Check the front end
beg1 = 1 + 2*rate1;
end1 = beg1 + rate1;
beg3 = 1;
end3 = 1 + rate1;
test1 = vect1(beg1:end1);
test3 = vect3(beg3:end3);
diffOk = diffcheck(test1,test3);
if(diffOk ~= true)
    msgId = 'framealigntest:badMatch';
    warning(msgId,' Read with 2 sec offset not aligned at start');
    if(~isempty(fid))
        fprintf(fid,'Read with 2 sec offset not aligned at start - FAIL\n');
    end
    alignOk = 1;
    % now try to determine offset
    chk1A = test1(1:end-1);
    chk3A = test3(2:end);
    diffA = diffcheck(chk1A,chk3A);
    chk1B = test1(2:end);
    chk3B = test3(1:end-1);
    diffB = diffcheck(chk1B,chk3B);
    if(diffA == true && diffB ~= true)
        fprintf(' New data is offset by +1 sample!\n');
        if(~isempty(fid))
            fprintf(fid,'Data is offset by +1 sample!\n');    
        end
    end
    if(diffA ~= true && diffB == true)
        fprintf(' New data is offset by -1 sample!\n');
        if(~isempty(fid))
            fprintf(fid,'Data is offset by -1 sample!\n');    
        end
    end    
else
    fprintf('Read with 2 sec offset aligned at start - OK\n');
    if(~isempty(fid))
        fprintf(fid,'Read with 2 sec offset aligned at start - OK\n');
    end
end

%  ** Check the back end
last1 = numel(vect1) - round(2*rate1);
frst1 = last1 - floor(0.5*rate1);
last3 = numel(vect3);
frst3 = last3 - floor(0.5*rate1);
test1 = vect1(frst1:last1);
test3 = vect3(frst3:last3);
diffOk = diffcheck(test1,test3);
if(diffOk ~= true)
    msgId = 'framealigntest:badMatch';
    warning(msgId,' Read with 2 sec offset not aligned at end');
    alignOk = 1;
    if(~isempty(fid))
        fprintf(fid,'Read with 2 sec offset not aligned at end- FAIL\n');
    end
else
    fprintf('Read with 2 sec offset aligned at end - OK\n');
    if(~isempty(fid))
        fprintf(fid,'Read with 2 sec offset aligned at end - OK\n');
    end
end
%  
%  check offset by 3.5 sec
%
fprintf(' -- Start test with 3.5 sec offset --\n');
if(~isempty(fid))
    fprintf(fid,' -- Start test with 3.5 sec offset --\n');
end
start4 = startGps + 3.5;
dur4 = startDur - 5;
[vect4,rate4,err4] = chanvector(tChan,start4,dur4);
if(err4 ~= 0)
    msgId = 'framealigntest:readFail';
    if(~isempty(fid))
        fprintf(fid,' && Frame read with 3.5 sec offset failed!!\n');
        fclose(fid);
    end
    error(msgId,' Frame read with 3.5 sec offset failed with error %d',...
        err4);
end
if(rate4 ~= rate1)
    msgId = 'framealigntest:badRate';
    if(~isempty(fid))
        fprintf(fid,...
      ' Rate with 3.5 sec offset %f does not match full rate %f\n',...
        rate4,rate1);
    end
    warning(msgId,...
        ' Rate with 3.5 sec offset %f does not match full rate %f',...
        rate4,rate1)
end
beg1 = 1 + round(3.5*rate1);
end1 = beg1 + rate1;
beg4 = 1;
end4 = 1 + rate1;
test1 = vect1(beg1:end1);
test4 = vect4(beg4:end4);
diffOk = diffcheck(test1,test4);
if(diffOk ~= true)
    msgId = 'framealigntest:badMatch';
    warning(msgId,' Read with 3.5 sec offset not aligned');
    alignOk = 1;
    if(~isempty(fid))
        fprintf(fid,'Read with 3.5 sec offset not aligned - FAIL\n');
    end
    % now try to determine offset
    chk1A = test1(1:end-1);
    chk4A = test4(2:end);
    diffA = diffcheck(chk1A,chk4A);
    chk1B = test1(2:end);
    chk4B = test4(1:end-1);
    diffB = diffcheck(chk1B,chk4B);
    if(diffA == true && diffB ~= true)
        fprintf(' New data is offset by +1 sample!\n');
        if(~isempty(fid))
            fprintf(fid,'Data is offset by +1 sample!\n');    
        end
    end
    if(diffA ~= true && diffB == true)
        fprintf(' New data is offset by -1 sample!\n');
        if(~isempty(fid))
            fprintf(fid,'Data is offset by -1 sample!\n');    
        end
    end    
else
    fprintf('Read with 3.5 sec offset aligned - OK\n');
    if(~isempty(fid))
        fprintf(fid,'Read with 3.5 sec offset aligned - OK\n');
    end
end

%  ** Check the back end
last1 = numel(vect1) - round(1.5*rate1);
frst1 = last1 - floor(0.5*rate1);
last4 = numel(vect4);
frst4 = last4 - floor(0.5*rate1);
test1 = vect1(frst1:last1);
test4 = vect4(frst4:last4);
diffOk = diffcheck(test1,test4);
if(diffOk ~= true)
    msgId = 'framealigntest:badMatch';
    warning(msgId,' Read with 3.5 sec offset not aligned at end');
    alignOk = 1;
    if(~isempty(fid))
        fprintf(fid,'Read with 3.5 sec offset not aligned at end- FAIL\n');
    end
else
    fprintf('Read with 3.5 sec offset aligned at end - OK\n');
    if(~isempty(fid))
        fprintf(fid,'Read with 3.5 sec offset aligned at end - OK\n');
    end
end

%  set error flag
if(alignOk == 2)
    alignOk = 0;
end

return


function diffOk = diffcheck(ray1,ray2)
% DIFFCHECK - checks that all elements match
if(numel(ray1)~=numel(ray2))
    msgId = 'diffcheck:badLen';
    warning(msgId,' array lengths of %d and %d do not match',...
        numel(ray1),numel(ray2));
    diffOk = false;
    return
end
diffRay = ray1-ray2;
maxDiff = max(diffRay);
minDiff = min(diffRay);
if(maxDiff == 0 && minDiff == 0)
    diffOk = true;
else
    diffOk = false;
end
return
