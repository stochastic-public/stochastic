function errorCode = ldrtests
% LDRTESTS - test setup of LDR for MATLAB Channel class
%   errorCode = function ldrtests
%       errorCode = 0 passed, > 0 otherwise
%           1 - LDR_SQL_HOST not defined
%           2 - LDR_METADATA not defined
%           3 - LDR_SQL_USER not defined
%           4 - LDR_SQL_PWD not defined
%
% $Id: ldrtests.m,v 1.1 2004-08-11 18:44:13 kathorne Exp $

% GET LDR host environment variable
% IF undefined
%   CREATE error
%   SET error code
% ENDIF
% GET LDR metadata environment variable
% IF undefined
%   CREATE error
%   SET error code
% ENDIF
% GET LDR account environment variable
% IF undefined
%   CREATE error
%   SET error code
% ENDIF
% GET LDR password variable
% IF undefined
%   CREATE error
%   SET error code
% ENDIF
errorCode = 0;
sqlHost = getenv('LDR_SQL_HOST');
if(isempty(sqlHost))
    fprintf('** LDR_SQL_HOST needs to be defined\n');
    errorCode = 1;
end
ldrMeta = getenv('LDR_METADATA');
if(isempty(ldrMeta))
    fprintf('** LDR_METADATA needs to be defined\n');
    errorCode = 2;
end
ldrUser = getenv('LDR_SQL_USER');
if(isempty(ldrUser))
    fprintf('LDR_SQL_USER needs to be defined\n');
    errorCode = 3;
end
ldrPwd = getenv('LDR_SQL_PWD');
if(isempty(ldrPwd))
    fprintf('LDR_SQL_PWD needs to be defined\n');
    errorCode = 4;
end
if(0 == errorCode)
    fprintf('** All LDR variables are defined!\n');
end
return
