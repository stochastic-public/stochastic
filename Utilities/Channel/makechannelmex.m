function [errorCode] = makechannelmex
% MAKECHANNELMEX - create mlframequery MEX file for Channel software
%   errorCode = makechannelmex()
%       errorCode - 0 if successful, > 0 if failed
%               0 - MEX file build successful!
%               1 - MYSQL_LIB not defined
%               2 - MYSQL_INC not defined
%               3 - mysql.h not in MYSQL_INC directory
%               4 - MEX file not built
%
%  - Requires
%       MATLAB installation (invoked with 'matlab' command)
%       MYSQL_LIB environment variable - points to MySQL libraries
%       MYSQL_INC environment variable - points to MySQL includes
%
% - On Linux, it creates file mlframequery.mexglx
%
% $Id: makechannelmex.m,v 1.1 2004-08-11 18:43:34 kathorne Exp $

% IF MEX file exists
%   DELETE existing MEX file
% ENDIF
% GET mysql library path from environment variable
% IF library path is undefined
%   CREATE error
%   EXIT
% ENDIF
% GET mysql include path from environment variable
% IF include path is undefined
%   CREATE error
%   EXIT
% ENDIF
% CHECK that mysql.h is on include path
% IF mysql.h is not on include path
%   CREATE error
%   EXIT
% ENDIF
errorCode = 4;
extStr = mexext;
mexFile = sprintf('mlframequery.%s',extStr);
mexExist = exist(mexFile);
if(3 == mexExist)
    delete(mexFile);
end
mysqlLibPath = getenv('MYSQL_LIB');
if(isempty(mysqlLibPath))
    errorCode = 1;
    warning('MySQL library path MYSQL_LIB needs to be defined!');
    return
end
mysqlIncPath = getenv('MYSQL_INC');
if(isempty(mysqlIncPath))
    errorCode = 2;
    warning('MySQL include path MYSQL_INC needs to be defined!');
    return
end
incFile = strcat(mysqlIncPath,'/mysql.h');
incExist = exist(incFile);
if(incExist ~= 2)
    errorCode = 3;
    warning('path %s does not have MySQL includes!',mysqlIncPath);
    return
end

% CREATE mex command string
% RUN mex command string to make MEX file
% IF MEX file not created
%   CREATE error
%   EXIT
% ENDIF
libStr = sprintf(' -L%s -lmysqlclient',mysqlLibPath);
incStr = sprintf(' -I%s',mysqlIncPath);
cFiles = ' mlframequery.c framequery.c common_mysql.c';
mexStr = strcat('mex',incStr,libStr,cFiles);
fprintf(' Creating mlframequery MEX file\n');
eval(mexStr);
mexExist = exist(mexFile);
if(3 == mexExist)
    fprintf('**Succesfully created MEX file %s\n',mexFile);
    errorCode = 0;
else
    errorCord =4;
    warning(' Failed to create MEX file %s\n',mexFile);
end
return
