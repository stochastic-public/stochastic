function propVal = get(chanObj,propName)
% GET Get channel property from the specified object
% and return the value
switch propName
    case 'name'
        propVal = chanObj.name;
    case 'statusCode'
        propVal = chanObj.statusCode;
    case 'site'
        propVal = chanObj.site;
    case 'instrument'
        propVal = chanObj.instrument;
    case 'type'
        propVal = chanObj.type;
    case 'gpsStart'
        propVal = chanObj.gpsStart;
    case 'rate'
        propVal = chanObj.rate;
    otherwise
        warning ([propName,' is not a Channel property']);
end
return