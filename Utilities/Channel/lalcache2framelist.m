function [frameListStruct] = lalcache2framelist(lalCacheFile)
% LALCACHE2FRAMELIST - parse LAL Cache file to get frame list
% [frameListStruct] = lalcache2framelist(lalCacheFile)
%
% Given a path to a LAL cache file (from gw_data_find), read the file and
% return a structure with the following
%   gpsTimes  - vector of gps start times
%   frameFiles  - cell-array of paths to corresponding frames
%   listError - Possible error code - 0 if no error
%   frameDurs  - vector of frame file durations
%
%   listError values
%      1 no frame files in list
%      11 LAL cache file input was bad
%      12 LAL cache file bad format
%
%   error/warning messages
%       lalcache2frameList:noFiles
%       lalcache2frameList:badCache
%
% $Id: lalcache2framelist.m,v 1.1 2005-09-21 19:17:26 kathorne Exp $

% IF # of inputs is wrong
%   PRINT error message
%   EXIT
% ENDIF
% IF # of outputs is wrong
%   PRINT error message
%   EXIT
% ENDIF
% INITIALIZE outputs
error(nargchk(1,1,nargin),'struct');
error(nargchk(1,1,nargout),'struct');
listError = 0;
gpsTimes = [];
frameFiles = [];
frameDurs = [];
frameListStruct = [];

% IF LAL cache file does not exist
%   SET error code
%   EXIT
% ENDIF
% READ in LAL cache file
%  -- Assumed format --
% instrument type gpsStart frameDur filePath
if(iscell(lalCacheFile))
    lalCacheFile = char(lalCacheFile);
end
if(isempty(lalCacheFile) || ~ischar(lalCacheFile))
    listError = 11;
    frameListStruct = mkframeliststruct(gpsTimes,frameFiles,...
            listError,frameDurs);
    msgId = 'lalcache2framelist:badCache';
    warning(msgId,'%s: LAL cache file input was bad',msgId);
    return
end
if(exist(lalCacheFile,'file') ~= 2)
    listError = 11;
    frameListStruct = mkframeliststruct(gpsTimes,frameFiles,...
            listError,frameDurs);
    msgId = 'lalcache2framelist:badCache';
    warning(msgId,'%s: LAL cache file %s not found',...
        msgId,lalCacheFile);
    return
end    
[instrumentDum,typeDum,cacheGps,cacheDurs,cacheFrames]=...
    textread(lalCacheFile,'%s %s %d %d %s');
numCache = numel(cacheGps);
if(numCache < 1)
    listError = 1;
    frameListStruct = mkframeliststruct(gpsTimes,frameFiles,...
            listError,frameDurs);
    msgId = 'lalcache2framelist:noFiles';
    warning(msgId,'%s: no files in LAL cache file %s',...
        msgId,lalCacheFile);
    return
end
% NOTE: the physical file list that comes from
% LAL cache will be of the format
%   'file://name.of.node/local/file/path'
%   - We need to strip off the file: header as well as the node name to
%       get the local file path
%   - we also need to skip the duplicate gsiftp:
%
% LOOP over list of files
%	IF file: header is present
%       REMOVE file: header from file path
%       IF // is present in file path
%           REMOVE // from path (leaving node-name)
%       ENDIF
%       FIND the next / in path (indicating end of node-name
%           or beginning of local path)
%       IF a / exists in path
%           SET file path to string starting at this next /
%       ENDIF
%       ADD frame to gps,duration,frame lists
%   ENDIF
% END LOOP
filSrch = 'file:';
lenFil = length(filSrch);
dslSrch = '//';
lenDsl = length(dslSrch);
numFiles = 0;
frameList = [];
for k = 1:numCache
    fileName = char(cacheFrames{k});
	filPos = findstr(filSrch,fileName);
	if(~isempty(filPos))
        numFiles = numFiles + 1;
		fileName = fileName((filPos+lenFil):end);
        dslPos = findstr('//',fileName);
        if(~isempty(dslPos))
            fileName = fileName((dslPos+lenDsl):end);
        end
        slPos = findstr('/',fileName);
        if(~isempty(slPos))
            fileName = fileName(slPos(1):end);
        end
        if(isempty(frameList))
            frameList = cell(1,1);
        end
        gpsList(numFiles) = cacheGps(k);
        frameList{numFiles} = fileName;
        durList(numFiles) = cacheDurs(k);
    end
end

% SET GPS output to array from query
% SET frame duration output to array from query
% SORT GPS,file,duration arrays by ascending GPS Start Time
% REMOVE duplicate frame files in GPS,file,duration arrays
[gpsTimes,ndx] = unique(gpsList);
frameFiles = {frameList{ndx}};
frameDurs = durList(ndx);

% IF there are no frame files left
%   SET error code
%   RETURN
% ENDIF
% CLEAR error code
% CREATE output as structure
numFrame = numel(gpsTimes);
if(numFrame < 1)
    listError = 1;
    frameListStruct = mkframeliststruct(gpsTimes,frameFiles,...
            listError,frameDurs);
    msgId = 'lalcache2framelist:noFiles';
    warning(msgId,'%s: no local frames in LAL cache file %s',...
        msgId,lalCacheFile);
    return
end
listError = 0;    
frameListStruct = mkframeliststruct(gpsTimes,frameFiles,...
            listError,frameDurs);
return
